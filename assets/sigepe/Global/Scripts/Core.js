jQuery(document).ready(function() {    
//    alert("sigepe-core.js");

	$("#link-logout").click(function(){
		window.location.replace(BaseUrl+"logout");
	});

	$(".link-redirect").click(function(e){
		e.preventDefault();
		Url 	=	$(this).data('redirect');
		window.location.href = BaseUrl + Url;
	});


	(function ($) {
	    $.fn.serializeFormJSON = function () {

	        var o = {};
	        var a = this.serializeArray();
	        $.each(a, function () {
	            if (o[this.name]) {
	                if (!o[this.name].push) {
	                    o[this.name] = [o[this.name]];
	                }
	                o[this.name].push(this.value || '');
	            } else {
	                o[this.name] = this.value || '';
	            }
	        });
	        return o;
	    };
	})(jQuery);

});