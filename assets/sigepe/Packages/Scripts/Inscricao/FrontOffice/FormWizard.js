var FormWizard = function () {


    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().bootstrapWizard) {
                return;
            }


            var form        =   $('#form-animal');
            var error       =   $('.alert-danger', form);
            var success     =   $('.alert-success', form);

            form.validate({
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {

                    /* TAB 1 ( VINCULOS ) */
                    'proprietario-flag'         : {required: true },
                    'cpf-cnpj-proprietario'     : {required: true, cpfBR: true },
                    'nome-proprietario'         : {required: true, minlength: 999 },
                    'federacao'                 : {required: true },
                    'entidade-filiada'          : {required: true },
                    'escola-equitacao'          : {required: true },

                    /* TAB 2 ( INFORMACOES DO ANIMAL ) */
//                    'chip'                      : {required: true },
//                    'passaporte'                : {required: true },
                    'data-nascimento'           : {required: true },
                    'nome-completo'             : {required: true },
/*
                    'raca'                      : {required: true },
                    'pelagem'                   : {required: true },
                    'genero'                    : {required: true },
                    'genero-tipo'               : {required: true },
*/                    
                },
                messages: {
                        
                    /* TAB 1 ( VINCULOS ) */
                    'proprietario-flag'         : {required: "Selecione uma opção." },
                    'cpf-cnpj-proprietario'     : {required: "Infome o CPF do proprietário.", cpfBR: 'Informe um CPF válido' },
                    'nome-proprietario'         : {required: "Selecione o nome de um proprietário na lista.", minlength: 'Selecione o nome de um proprietário na lista.' },
                    'federacao'                 : {required: "Qual federação seu animal está filiado?" },
                    'entidade-filiada'          : {required: "Selecione uma opção." },
                    'escola-equitacao'          : {required: "Selecione uma opção." },

                    /* TAB 2 ( INFORMACOES DO ANIMAL ) */
                    'chip'                      : {required: "Qual o CHIP do animal?", },
                    'passaporte'                : {required: "Informe o passaporte do animal.", },
                    'data-nascimento'           : {required: "Informe a data de nascimento do animal.", },
                    'nome-completo'             : {required: "Qual o nome completo do animal?", },
                    'raca'                      : {required: "Selecione qual a raça do animal.", },
                    'pelagem'                   : {required: "Selecione qual a pelagem do animal.", },
                    'genero'                    : {required: "Qual o gênero do animal.", },
                    'genero-tipo'               : {required: "Seu animal é inteiro ou castrado?", },

                },

                errorPlacement: function (error, element) { // render error placement for each input type
                  
                    if (element.attr("name") == "genero") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_gender_error");
                    }
                    else if (element.attr("name") == "proprietario-flag") { // for uniform checkboxes, insert the after the given container
                        error.insertAfter("#form-proprietario-animal-error");
                    }
                    else if (element.attr("name") == "proprietario-flag") { // for uniform checkboxes, insert the after the given container
                        error.insertAfter("#form-proprietario-animal-error");
                    }
                    else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                        label
                            .closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid') // mark the current input as valid and display OK icon
                        .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },

                submitHandler: function (form) {
                    success.show();
                    error.hide();
                    form[0].submit();
                    //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
                }

            });

            var displayConfirm = function() {
           

                $('#tab3 .form-control-static', form).each(function(){

                    var input = $('[name="'+$(this).attr("data-display")+'"]', form);
                    

                    if (input.is(":radio")) {
                        input = $('[name="'+$(this).attr("data-display")+'"]:checked', form);
                    }


                    if (input.is(":text") || input.is("textarea")) {
                        $(this).html(input.val());
                    }
                    else if (input.is("select")) {
                        $(this).html(input.find('option:selected').text());
                    }
                    else if (input.is(":radio") && input.is(":checked")) {
                        $(this).html(input.attr("data-title"));
                    }
                    else if ($(this).attr("data-display") == 'payment[]') {
                        var payment = [];
                        $('[name="payment[]"]:checked', form).each(function(){ 
                            payment.push($(this).attr('data-title'));
                        });
                        $(this).html(payment.join("<br>"));
                    }

                    if ( input.is(":text")  ) {
                        if(input.val().length === 0)
                            $(this).html('-');
                    }

                    if (input.is("select")) {
                        if(input.val().length === 0)
                            $(this).html('-');
                    }

                });


                // Flag Proprietario
                if( $('input[name=proprietario-flag]:checked', '#form-animal').val() == '2' ){
                    $("#form-group-resume-cpf-cnpj-proprietario").slideDown();
                    $("#form-group-resume-cpf-cnpj-proprietario .id").html( $("#proprietario-id").val() );
                    $("#form-group-resume-cpf-cnpj-proprietario .nome").html( $("#proprietario-nome").val() );
                    $("#form-group-resume-cpf-cnpj-proprietario .cpf-cnpj").html( $("#proprietario-cpf-cnpj").val() );
                }else{
                    $("#form-group-resume-cpf-cnpj-proprietario").slideUp();
                }

                
                // Flag Genero Tipo
                if( $('input[name=genero]:checked', '#form-animal').val() == '1' ){
                    $("#form-group-tipo-macho").slideDown();
                }else{
                    $("#form-group-tipo-macho").slideUp();
                }


                // Federacao
                if($("#federacao").val() == 141){ // 141 id de FHB r( tb_pessoa )
                    
                    if($("#entidade-filiada").val().length > 1){
                        $("#form-group-resume-entidade-filiada").show();                        
                        $("#form-group-resume-escola-equitacao").hide();                        
                    }else{
                        $("#form-group-resume-escola-equitacao").show();                        
                        $("#form-group-resume-entidade-filiada").hide();                        
                    }
                }else{
                    $("#form-group-resume-entidade-filiada, #form-group-resume-escola-equitacao").hide();
                }


            }

            var SubmitForm = function() {

                /*
                    // DESCOMENTAR ESSE BLOCO APOS LIBERACAO DA CARLA. BLOQUEAR CAMPOS OBRIGATORIOS.

                if( NumeroChipExiste( $("#form-animal #chip").val() ) == true )
                    return AvisoNumeroChipExiste();
                */


                $.ajax({

                    url : BaseUrl + 'animal/FrontOffice/Cadastro/AjaxProcessar/',
                    type : 'POST',
                    data: { DataSerialized: $("#form-animal").serialize() },
                    async: false,
                    dataType:'json',
                    success : function(data) {            

                        console.log(data);

                        StringErros     =   ''; 

                        if(data.Status == true){
                            $("#form-animal").slideUp();
                            sweetAlert(
                                    "Cadastro de animal realizado com sucesso!",
                                    "Em alguns instantes você será direcionado para o sistema.",
                                    "success");                            
                           setTimeout(function () {
                               window.location.href = BaseUrl + "FrontOffice/Animal/Dashboard/" + data.AnimalId; //will redirect to your blog page (an ex: blog.html)
                            }, 2000); //will call the function after 2 secs.
                        }else{

                            sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");

                        }


                    },
                    error : function(request,error)
                    {
                        alert("Request: "+JSON.stringify(request));
                        sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");
                    }
                });


                $('#form-atleta .button-submit').removeClass('disabled');

	        }
	
	        var AvisoNumeroChipExiste = function() {
				form.bootstrapWizard('show',1);
				form.find('.button-previous').hide();
				form.find('.button-submit').hide();
                form.find('.button-next').show();
				$('#form-animal #chip').val('');

                // selecionar 
                $( "ul.nav-pills li:eq(1)" ).removeClass('done');

				// show here pretty lightbox
                sweetAlert("Oops... Animal já cadastrado.", "O Nº Chip informado já está cadastrado! Ligue para FHBr: (61) 3245-5870 para mais detalhes", "error");
				
                App.scrollTo($('.page-title'));

                $('#form-animal .button-submit').removeClass('disabled');

            }


            var handleTitle = function(tab, navigation, index) {
                var total = navigation.find('li').length;
                var current = index + 1;
                // set wizard title
                $('.step-title', $('#form-animal')).text('Etapa ' + (index + 1) + ' de ' + total);
                // set done steps
                jQuery('li', $('#form-animal')).removeClass("done");
                var li_list = navigation.find('li');
                for (var i = 0; i < index; i++) {
                    jQuery(li_list[i]).addClass("done");
                }

                if (current == 1) {
                    $('#form-animal').find('.button-previous').hide();
                } else {
                    $('#form-animal').find('.button-previous').show();
                }

                if (current >= total) {
                    $('#form-animal').find('.button-next').hide();
                    $('#form-animal').find('.button-submit').show();
                    displayConfirm();
                } else {
                    $('#form-animal').find('.button-next').show();
                    $('#form-animal').find('.button-submit').hide();
                }
                App.scrollTo($('.page-title')); 
            }



            // default form wizard
            $('#form-animal').bootstrapWizard({

                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',

                onTabClick: function (tab, navigation, index, clickedIndex) {
                    return false;
                    
                    success.hide();
                    error.hide();
                    if (form.valid() == false) {
                        return false;
                    }
                    
                    handleTitle(tab, navigation, clickedIndex);
                },

                onNext: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    if (form.valid() == false) {
                        return false;
                    }

                    handleTitle(tab, navigation, index);
                },

                onPrevious: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    handleTitle(tab, navigation, index);
                },

                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#form-animal').find('.progress-bar').css({
                        width: $percent + '%'
                    });
                }

            });


            $('#form-animal').find('.button-previous').hide();
            $('#form-animal .button-submit').click(function () {

                $('#form-animal .button-submit').addClass('disabled');

            	SubmitForm();
//                alert('Finished! Hope you like it :)');

            }).hide();

        }
        // end init

    }; // return

}(); // end function FormWizard()

FormWizard.init();
