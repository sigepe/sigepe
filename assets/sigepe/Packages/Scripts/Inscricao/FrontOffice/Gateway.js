/**
 * SIGEPE
 *
 * @package 	Vanguarda
 * @author      Gustavo Botega
 * @copyright   Copyright (c) 2017, Gustavo Botega
 * @link        http://gustavobotega.com
 * @link        http://agenciavanguarda.com.br
 *
 */



    /*

        BLOCO PAGAR.ME
        - HandleSuccessCheckout
        - GetErrorMessage

    ========================================================================*/
    function CreateTransactionBoleto () {

//        $("#layer-loading-registro").fadeIn();

        SigepeFatura      =   $("input[name='sigepe-transacao']").val();
//        SigepeRegistro    =   $("input[name='sigepe-inscricao-']").val();
        PagarMePreco      =   $("#pagarme-preco").val();

        pagarme.client.connect({ encryption_key: 'ek_live_IIpzTxJcAkbyzgjbBDXSsYiG73GsWM'  })
        .then(

            client => client.transactions.create(
                {
                    amount: PagarMePreco,
                    external_id: '#' + PagarmeExternalId,
                    payment_method: 'boleto',
                    postback_url: BaseUrl + 'financeiro/FrontOffice/PagarmePostbackUrl/Processar',
                    boleto_instructions: 'Boleto referente a inscrição ',
//                    boleto_expiration_date: GetVencimentoBoleto(),
                    boleto_expiration_date: $("#pagarme-vencimento-boleto").val(),
                    customer: {
                      type: 'individual',
                      name: PagarmeNome,
                      country: 'br',
                      email: PagarmeEmail,
                      documents: [
                        {
                          type: 'cpf',
                          number: PagarmeCpf,
                        },
                      ],
                      phone_numbers: ['+55' + PagarmeTelefonePrincipal],
                      birthday: PagarmeDataNascimento
                    },
                    billing: {
                      name: PagarmeNome,
                      address: {
                        country: 'br',
                        state: PagarmeEstadoSigla,
                        city: PagarmeCidade,
                        neighborhood: PagarmeBairro,
                        street: PagarmeLogradouro,
                        street_number: PagarmeNumero,
                        zipcode: PagarmeCep
                      }
                    },
                    items: [
                      {
                        id: '1',
                        title: 'Inscrição - Fatura Simples',
//                        unit_price: PagarMePreco,
                        unit_price: PagarMePreco,
                        quantity: 1,
                        tangible: false
                      }
                    ]
                }
            )

        ).then(

            transaction => HandleBoleto(transaction, PagarMePreco)
        )

    }
    // END CreateTransactionBoleto


    function GetVencimentoBoleto(){

        startdate = moment().format('DD-MM-YYYY');;
        var new_date = moment(startdate, "DD-MM-YYYY").add('days', 1);
        var day = new_date.format('DD');
        var month = new_date.format('MM');
        var year = new_date.format('YYYY');
        return year + '-' + month + '-' + day;

    }



    function HandleBoleto(Transaction, PagarMePreco){

        console.log("Transaction a seguir e do boleto");
        console.log(">" + Transaction);

        // Se a transacao do boleto for autorizada executa a captura
        if(Transaction.object == 'transaction' && Transaction.status == 'authorized'){

          $.ajax({
            type: "POST",
            url: BaseUrl + "financeiro/FrontOffice/PagarmeVanguarda/HandleNaturezaOperacao",
            data: {
                    DatasetPagarme: Transaction,
                    PagarmeAmount: PagarMePreco,
                    AtletaId : $("#atleta-id").val(),
                    AtletaCategoriaId : $("#atleta-categoria-id").val(),
                    AnimalId : $("#animal-id").val(),
                    SerieId : $("#combobox-serie").val(),
                    EventoId : $("#evento-id").val(),

                    FaturaGlobalId :          $("#fatura-global-id").val(),
                    FaturaGlobalControle :    $("#fatura-global-controle").val(),
                    FaturaSimplesId :         $("#fatura-simples-id").val(),
                    FaturaSimplesControle :   $("#fatura-simples-controle").val(),

                    SigepeTransacao: $("input[name='sigepe-transacao']").val(),
                    //SigepeRegistroId: $("input[name='sigepe-registro']").val(),
                    SigepeNaturezaOperacao: 5
                },
            dataType:'json',
            success: function( data ){
              console.log(data);
                sweetAlert(
                        "Inscrição Gerada!",
                        "Sua inscrição só terá validade após compensação do boleto bancário. Você será redirecionado para o boleto em 5 segundos.",
                        "success");

                    setTimeout(function(){
                        window.location.replace( BaseUrl + 'evento/FrontOffice/FaturaGlobal/Dashboard/' + $("#evento-id").val() );
                    }, 1500);
            },
            error: function (jqXHR, exception) {
                $("#layer-loading-registro").fadeOut();
                console.log(jqXHR);
                GetErrorMessage(jqXHR, exception);
                sweetAlert(
                        "Oops.. aconteceu algum problema!",
                        "Sua inscrição NÃO foi processado. Entre em contato com a secretaria da federação para mais detalhes.",
                        "error");
            },
           complete: function (data) {
              $("#layer-loading-registro").fadeOut();
            }

          });


        }else{
            alert("ERRO! CONSULTE A FEDERACAO. CODIGO DO ERRO: GMB4628");
        }

    }
    //













    // This function is used to get error message for all ajax calls
    function GetErrorMessage(jqXHR, exception) {
        var msg = '';
        if (jqXHR.status === 0) {
            msg = 'Not connect.\n Verify Network.';
        } else if (jqXHR.status == 404) {
            msg = 'Requested page not found. [404]';
        } else if (jqXHR.status == 500) {
            msg = 'Internal Server Error [500].';
        } else if (exception === 'parsererror') {
            msg = 'Requested JSON parse failed.';
        } else if (exception === 'timeout') {
            msg = 'Time out error.';
        } else if (exception === 'abort') {
            msg = 'Ajax request aborted.';
        } else {
            msg = 'Uncaught Error.\n' + jqXHR.responseText;
        }
        alert(msg);
    }
