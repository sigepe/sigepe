jQuery(document).ready(function() {



	/**
	* Easy Autocomplete - Busca de proprietario pelo nome
	*
	* @author Gustavo Botega
	*/
	var options = {
    url: BaseUrl + "inscricao/FrontOffice/Inscricao/JsonAtleta",

    getValue: "name",

    list: {
        match: {
            enabled: true
        },

		showAnimation: {
			type: "slide", //normal|slide|fade
			time: 400,
			callback: function() {}
		},

		hideAnimation: {
			type: "slide", //normal|slide|fade
			time: 400,
			callback: function() {}
		},

		onClickEvent: function() {

			var Objeto 	= $("#nome-proprietario").getSelectedItemData();
			var Id 		= Objeto.id;
			var Nome 	= Objeto.name


			VerificaConjuntoSelecionado();

			$("#camada-proprietario-localizar").slideUp();
			$("#camada-proprietario-localizado").slideDown();

			$("#camada-proprietario-localizado span.nome").html(Nome);
			$("#camada-proprietario-localizado span.id").html(Id);

			$("#atleta-id").val(Id);


			$("#proprietario-id").val(Id);
			$("#proprietario-nome").val(Nome);

			$("#static").modal('show');

		}

    },

    theme: "plate-dark"
};


	function VerificaConjuntoSelecionado() {

		if( ( $("#camada-proprietario-localizado").css('display') == 'block' ) &&  ( $("#camada-animal-localizado").css('display') == 'block' ) ){

			$(".aviso-selecione-conjunto").slideUp();
			$(".aviso-baia-qs-desabilitado").slideDown();


			$("#box-serie-prova").slideDown();
			$("#bloco-provas").slideDown();
			$("#box-resumo").slideDown();

			liberarSeriesEspecificas();
			liberarProvasEspecificas();
		} else {
			bloquearTodasAsSeries();
			bloquearTodasAsProvas();
			$(".aviso-baia-qs-desabilitado").slideUp();
			$(".aviso-selecione-conjunto").slideDown();
			$("#box-serie-prova").slideUp();
			$("#bloco-provas").slideUp();
			$("#box-resumo").slideUp();


		}

	}







	function bloquearTodasAsSeries(){
		$("#combobox-serie > option").each(function(){
			SerieId = $(this).attr('data-serie-id');
			if (SerieId > 0){
			console.log(SerieId);
				$(DatasetSeriePorCategoria[SerieId].SerieCategorias).each(function(index, element){
					if(CategoriaAtleta == element){
						$("#combobox-serie option[value='"+SerieId+"'").prop("disabled", true); // mudar propriedade "disable" para true;
						//$("#combobox-serie option[value='"+SerieId+"'").css('background-color', '#ccc');
						$("#combobox-serie option").css('background-color', '#ccc');
						$("#combobox-serie option[value='"+SerieId+"'").css('color', '#666');
					}
				});
			}
		});
	}

	function liberarSeriesEspecificas(){
		$("#combobox-serie > option").each(function(){
			SerieId = $(this).attr('data-serie-id');
			if (SerieId > 0){
				$(DatasetSeriePorCategoria[SerieId].SerieCategorias).each(function(index, element){
					if(CategoriaAtleta == element){
						$("#combobox-serie option[value='"+SerieId+"'").prop("disabled", false); // mudar propriedade "disable" para false;
						$("#combobox-serie option[value='"+SerieId+"'").css('background-color', 'white');
						$("#combobox-serie option[value='"+SerieId+"'").css('color', 'black');
					}
				});
			}
		});
	}






	function bloquearTodasAsProvas(){

		$("tr.linha-prova").each(function(){
			$(this).find('input').prop('checked', false);
			$(this).removeClass('linha-prova-selecionado');
			$(this).removeClass('linha-prova-selecionado-serie');
			$(this).addClass('linha-prova-inabilitado');
		});



	}



	function liberarProvasEspecificas(){
	/*
		$("#combobox-serie > option").each(function(){
			SerieId = $(this).attr('data-serie-id');
			if (SerieId > 0){
				$(DatasetSeriePorCategoria[SerieId].SerieCategorias).each(function(index, element){
					if(CategoriaAtleta == element){
						$("#combobox-serie option[value='"+SerieId+"'").prop("disabled", false); // mudar propriedade "disable" para false;
						$("#combobox-serie option[value='"+SerieId+"'").css('background-color', 'white');
						$("#combobox-serie option[value='"+SerieId+"'").css('color', 'black');
					}
				});
			}
		});
	*/



		$("tr.linha-prova").each(function(){

			ProvaId = $(this).attr('data-prova-id');
			$(DatasetProvas[ProvaId].categorias).each(function(index, element){
				if(CategoriaAtleta == element)
					$("tr[data-prova-id="+ProvaId+"]").removeClass('linha-prova-inabilitado');
			});

		});



	//DatasetProvas

	}




	function selecionarProvasDaSerie(Dataset){

		$("tr.linha-prova").each(function(){
			ProvaId 	=	$(this).data('prova-id');
			ProvaId 	=	String(ProvaId);
			if( (jQuery.inArray( ProvaId, Dataset)) != '-1'){
				$(this).find('input').prop('checked', true);
				$(this).addClass('linha-prova-selecionado-serie');
				$(this).removeClass('linha-prova-selecionado');
				$(this).removeClass('linha-prova-inabilitado');
			}
		});

	}



	function removerSelecaoProvasDaSerie(Dataset){

		$("tr.linha-prova").each(function(){
			ProvaId 	=	$(this).data('prova-id');
			ProvaId 	=	String(ProvaId);
			if( (jQuery.inArray( ProvaId, Dataset)) != '-1'){
				$(this).find('input').prop('checked', false);
				$(this).removeClass('linha-prova-selecionado-serie');
				$(this).removeClass('linha-prova-selecionado');
				$(this).removeClass('linha-prova-inabilitado');
			}
		});

	}









	/*function DesbloquearSeriesPorCategoria() {


			$("#combobox-serie > option").each(function() {

				SerieId 		=	 $(this).attr('data-serie-id');

				if(SerieId > 0){

					//<option value="110" data-serie-id="110" style="background-color: rgb(238, 238, 238); color: rgb(204, 204, 204);" disabled="disabled">CBrS CN 04 Anos - R$ 180.00                    </option>

					// DatasetSeriePorCategoria[110].SerieCategorias = ARRAY 
//					$( DatasetSeriePorCategoria[110].SerieCategorias ).each(function(index, element) {


					
					$( DatasetSeriePorCategoria[SerieId].SerieCategorias ).each(function(index, element) {


						if(CategoriaAtleta == element){
							//$("#combobox-serie option[value='"+SerieId+"'").attr('disabled', 'disabled');
							$("#combobox-serie option[value='"+SerieId+"'").prop("disabled", false); // Element(s) are now enabled.

							$("#combobox-serie option[value='"+SerieId+"'").css('background-color', '#fff');
							$("#combobox-serie option[value='"+SerieId+"'").css('color', '#666');

						}else{
						}

					});

				}


			$("#combobox-serie").find('option').css('background-color', 'transparent');
			$("#combobox-serie").find('option:disabled').css('background-color', '#eee');
			$("#combobox-serie").find('option:disabled').css('color', '#ccc');

		});

	}*/




		/*
		function DELETARFUNCAODesbloquearSeriesPorCategoria() {
			$('#combobox-serie').prop('selectedIndex',0);
			$("#combobox-serie").find('option:disabled').css('background-color', '#fff');
			$("#combobox-serie").find('option:disabled').css('color', '#666');
			$("#combobox-serie").find('option').prop("disabled", false); ;
		}
		*/






	$("#nome-proprietario").easyAutocomplete(options);

		/**
		* Click - Trocar Proprietario
		*
		* @author Gustavo Botega
		*/
		$("#btn-trocar-proprietario").click(function(e){
			removerSerie();
			e.preventDefault();

	    	$("#camada-proprietario-localizar").show();
	    	$("#camada-proprietario-localizado").hide();

	    	$("#nome-proprietario").val('');
	    	$("input[name='proprietario-id']").val('');
	    	$("input[name='proprietario-nome']").val('');

			VerificaConjuntoSelecionado();

		});


		/**
		* Click - Trocar Proprietario
		*
		* @author Gustavo Botega
		*/
		$("#btn-trocar-animal").click(function(e){
			e.preventDefault();



	    	$("#camada-animal-localizar").show();
	    	$("#camada-animal-localizado").hide();

	    	$("#nome-animal").val('');
	    	$("input[name='animal-id']").val('');
	    	$("input[name='animal-nome']").val('');

			VerificaConjuntoSelecionado();

		});







		/**
		* Easy Autocomplete - Busca de proprietario pelo nome
		*
		* @author Gustavo Botega
		*/
		var options2 = {
			url: BaseUrl + "inscricao/FrontOffice/Inscricao/JsonAnimal",

	    getValue: "name",

	    list: {
	        match: {
	            enabled: true
	        },

			showAnimation: {
				type: "slide", //normal|slide|fade
				time: 400,
				callback: function() {}
			},

			hideAnimation: {
				type: "slide", //normal|slide|fade
				time: 400,
				callback: function() {}
			},

			onClickEvent: function() {

				var Objeto 	= $("#nome-animal").getSelectedItemData();
				var Id 		= Objeto.id;
				var Nome 	= Objeto.name


				$("#camada-animal-localizar").slideUp();
				$("#camada-animal-localizado").slideDown();

				$("#camada-animal-localizado span.nome").html(Nome);
				$("#camada-animal-localizado span.id").html(Id);

				$("#animal-id").val(Id);
				VerificaConjuntoSelecionado();

			}

	    },

	    theme: "plate-dark"
		};

		$("#nome-animal").easyAutocomplete(options2);













		/*
		$("#btn-adicionar-serie").click(function(){

		});

		*/

		$("#combobox-serie").change(function(){
			if( $(this).val() > 0 ){



				SerieSelected 	=	$('#combobox-serie').val();
				$(DatasetSeriePorCategoria[SerieSelected].SerieProvas).each(function(index, element) {
						$("#table-provas-da-serie tr[data-srp-id='"+element+"']").slideDown();
						$("#table-provas-da-serie tr[data-srp-id='"+element+"']").show();
				});


				$("#nome-serie-selecionada").text(DatasetSeriePorCategoria[SerieSelected].SerieNome);
				$("#serie-preco-promocional").text(DatasetSeriePorCategoria[SerieSelected].SeriePrecoPromocional);
				$("#serie-preco-cheio").text(DatasetSeriePorCategoria[SerieSelected].SeriePrecoCheio);


				SeriePrecoPromocionalDecimal 			=   DatasetSeriePorCategoria[SerieSelected].SeriePrecoPromocional;
				$("#valor-serie").val(SeriePrecoPromocionalDecimal);

				SeriePrecoPromocional 					=		SeriePrecoPromocionalDecimal.replace('.', "");
				$("#pagarme-preco").val(SeriePrecoPromocional);

				$("#table-provas-da-serie").slideDown();
				$("#cabecalho-provas-da-serie").slideDown();

				$("#row-combobox-serie").slideUp();
				$("#aviso-limite-serie-inscricao").slideDown();

				$("#flag-serie").val('1');

				// Ativar provas que fazem parte da serie
				selecionarProvasDaSerie(DatasetSeriePorCategoria[SerieSelected].SerieProvas);

			}
		});




		$("#btn-remover-serie").click(function(){

			SerieSelected 	=	$('#combobox-serie').val();
			removerSelecaoProvasDaSerie(DatasetSeriePorCategoria[SerieSelected].SerieProvas);

			removerSerie();
		});


		function removerSerie(){
			$("#cabecalho-provas-da-serie").slideUp();
			$("#row-combobox-serie").slideDown();
			$("#aviso-limite-serie-inscricao").slideUp();
			$("#table-provas-da-serie tr").css('display', 'none');
			$("#table-provas-da-serie").slideUp();
			$('#combobox-serie').prop('selectedIndex',0);
			$("#flag-serie").val('');
		}


		$(".categoria-do-atleta").click(function(){
			// Definindo variavel
			$("#atleta-categoria-id").val($(this).attr('data-id'));
			window.CategoriaAtleta 	=	$(this).attr('data-id');
			$("#categoria-selecionada-atleta").text( $(this).attr('data-sigla') + ' - ' + $(this).attr('data-categoria')  );
			$("#static").modal('hide');


			// 

			VerificaConjuntoSelecionado();
		});









  		$("#btn-boleto").click(function(e){



	        // atualizando preco de baia + qs 

			if($("#valor-baia").val() == null || $("#quantidade-baia").val() == null){
		        ValorBaia   =   0;
		        QtdeBaia    =   0;
			}else{
		        ValorBaia   =   $("#valor-baia").val();
		        QtdeBaia    =   $("#quantidade-qs").val();
			}


			if($("#valor-qs").val() == null || $("#quantidade-qs").val() == null){
		        ValorQs   =   0;
		        QtdeQs    =   0;
			}else{
		        ValorQs   =   $("#valor-qs").val();
		        QtdeQs    =   $("#quantidade-qs").val();
			}


	        ValorTotalBaia  = ValorBaia * QtdeBaia;
	        ValorTotalQs    = ValorQs * QtdeQs;
	        TotalOpcional   = ValorTotalBaia + ValorTotalQs;
	        ValorSerie      = 0.00;
	        ValorProva      = 0.00;



	        // Verificar se Serie esta ativo 
	        if($("#flag-serie").val() > 0)
		        ValorSerie      =   parseFloat($("#valor-serie").val());



	        // Verificar se existe Provas 

	        ArrProvas 	=	[];
	        if ( $( ".linha-prova-selecionado" ).length ){
	        	$("#flag-prova").val('1');
				$("tr.linha-prova-selecionado").each(function(index, element){
					ProvaId 	=	$(this).attr('data-prova-id');
					ValorProva 	+=	parseFloat(DatasetProvas[ProvaId].valor);
					ArrProvas[index] =	ProvaId;
				});
				ValorProva 	=	parseFloat(ValorProva);
		    }



		    FlagSerie 	=	$("#flag-serie").val();
		    FlagProva 	=	$( ".linha-prova-selecionado" ).length;
		    if( FlagSerie == 0 && FlagProva == 0){
		        sweetAlert("Atenção", "Selecione uma série ou pelo menos uma prova para avançar.", "warning");
		        return false;
		    }


		    console.log("PROVAS");
		    console.log(ArrProvas);


  			$(this).slideUp();
			swal({
			  title: 'Inscrição sendo processada...',
			  type: 'warning',
			  text: 'Aguarde alguns instantes enquanto processamos a sua inscrição. Esse procedimento pode demorar até 1 minuto.',
			  showCancelButton: false,
			  showConfirmButton: false
			});








	        // Setando no pagarme o preco
	        
	        $("#pagarme-preco").val( (parseFloat(ValorSerie) + parseFloat(ValorProva) + TotalOpcional) * 100 );

	        /*
	        alert('Serie: ' + ValorSerie);
	        alert('Prova: ' + ValorProva);
	        alert('Opcional: ' + TotalOpcional);
	        alert( parseFloat(ValorSerie) + parseFloat(ValorProva) + TotalOpcional );
	        alert("OPERACAO CANCELADA!");
	        return false;
	        */



	        SigepeCriarInscricao();


          /*
  		      $.ajax({

  		          url : BaseUrl + 'inscricao/FrontOffice/Inscricao/AjaxProcessar/',
  		          type : 'POST',
  		          data: {
  								AtletaId : $("#atleta-id").val(),
  								AtletaCategoriaId : $("#atleta-categoria-id").val(),
  								AnimalId : $("#animal-id").val(),
  								SerieId : $("#combobox-serie").val(),
  								EventoId : $("#evento-id").val()
  							},
  		          async: false,
  		          dataType:'json',
  		          success : function(data) {


  		              StringErros     =   '';
  									alert("Inscrição gerada com sucesso! Aguarde redirecionamento para boleto.");
  									setTimeout(function () {
  											window.location.href = BaseUrl; //will redirect to your blog page (an ex: blog.html)
  									 }, 2000); //will call the function after 2 secs.


  		          },
  		          error : function(request,error)
  		          {
  		              alert("Request: "+JSON.stringify(request));
  		              sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");
  		          }
  		      });
            */


  		});




    /*
        SigepeCriarInscricao
        - Cria inscricao e financeiro
    ========================================================================*/
    function SigepeCriarInscricao () {

      //alert("primeiro pass!");

        $.ajax({
            type: "POST",
            url: BaseUrl + "inscricao/FrontOffice/Inscricao/Processar",
            data: {
              AtletaId :            $("#atleta-id").val(),
              AtletaCategoriaId :   $("#atleta-categoria-id").val(),
              AnimalId :            $("#animal-id").val(),
              SerieId :             $("#combobox-serie").val(),
              EventoId :            $("#evento-id").val(),
              QtdeBaia :            $("#quantidade-baia").val(),
              QtdeQs:               $("#quantidade-qs").val(),
              ValorBaia:            $("#valor-baia").val(),
              ValorQs:              $("#valor-qs").val(),
              FlagSerie:            $("#flag-serie").val(),
              FlagProva:            $("#flag-prova").val(),
              ArrProvas:            ArrProvas
            },
            dataType:'json',
            success: function( data ){

//                alert("cadastrando faturasimplesid: " + data.FaturaSimplesId);

                $("#fatura-global-controle").val(data.FaturaGlobalControle);
                $("#fatura-global-id").val(data.FaturaGlobalId);
                $("#fatura-simples-id").val(data.FaturaSimpleslId);
                $("#fatura-simples-controle").val(data.FaturaSimplesControle);
                // $("input[name='sigepe-registro']").val(data.RegistroId);

                //alert("Segundo Passo - Criando transacao");
                //alert("retornando dados. gravar fatura global/simples na view");
                console.log(data);
                ////alert("console-log");
                $("#fatura-global-id").val(data.FaturaGlobalId);
                $("input[name='sigepe-transacao']").val(data.Transacao);
                CreateTransactionBoleto();
            },
            error: function (jqXHR, exception) {
              $("#layer-loading-registro").fadeOut();
                console.log(jqXHR);
                GetErrorMessage(jqXHR, exception);
                sweetAlert(
                        "Oops.. aconteceu algum problema!",
                        "Sua inscrição NÃO foi processado. Entre em contato com a secretaria da federação para mais detalhes.",
                        "error");
            },
            complete: function (data) {
              $("#layer-loading-registro").fadeOut();
            }
        });


        setTimeout(function(){ $("#layer-loading-registro").fadeOut() }, 75 *1000);

    }
    // end SigepeCriarRegistro()






});
