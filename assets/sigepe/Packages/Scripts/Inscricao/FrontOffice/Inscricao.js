/**
 * SIGEPE
 *
 * @package 	Vanguarda
 * @author      Gustavo Botega
 * @copyright   Copyright (c) 2017, Gustavo Botega
 * @link        http://gustavobotega.com
 * @link        http://agenciavanguarda.com.br
 *
 */

$(document).ready(function(){


	$(".linha-prova").click(function(){

		if($(this).hasClass("linha-prova-inabilitado")){
	        sweetAlert("Atenção", "Essa prova não está disponível para a categoria selecionada", "warning");
	        return false;
		}

		if($(this).hasClass("linha-prova-selecionado-serie")){
	        sweetAlert("Atenção", "Essa prova foi selecionado via série. Para remover essa prova remover antes a série.", "warning");
	        return false;
		}


		if($(this).hasClass("linha-prova-selecionado")){
			$(this).removeClass('linha-prova-selecionado');
			$(this).find('input').prop('checked', false);
	        return false;
		}

		

		// Ativando linha
		$(this).addClass('linha-prova-selecionado');
		$(this).find('input').prop('checked', true);


	});

});