jQuery(document).ready(function() {    


	/**
	* Federacao
	*
	* @author Gustavo Botega 
	*/
	$('select#federacao').on('change', function() {
		if(this.value == 141){ // 143 e o ID da FHBr
			$("#form-group-entidade-filiada, #form-group-escola-equitacao").slideDown();
		}else{
			$("#form-group-entidade-filiada, #form-group-escola-equitacao").slideUp();
			$("#form-group-entidade-filiada select, #form-group-escola-equitacao select").prop('selectedIndex',0);
		}
 	})


	/**
	* Entidade Filiada
	*
	* @author Gustavo Botega 
	*/
	$('select#entidade-filiada').on('change', function() {
		
		if(this.value.length==0){
			ResetarEntidadeEquestre();
		}else{
			$('#form-group-escola-equitacao').slideUp();
			$("#form-group-link-remover-entidade").slideDown();
		}

	});


	/**
	* Escola Equitacao
	*
	* @author Gustavo Botega 
	*/
	$('select#escola-equitacao').on('change', function() {
		
		if(this.value.length==0){
			ResetarEntidadeEquestre();
		}else{
			$('#form-group-entidade-filiada').slideUp();
			$("#form-group-link-remover-entidade").slideDown();
		}

	});


	/**
	* Link Trocar Entidade
	*
	* @author Gustavo Botega 
	*/
	$('#link-trocar-entidade').on('click', function(event) {
	    event.preventDefault();
	    ResetarEntidadeEquestre();
	});


	/**
	* ResetarEntidadeEquestre
	*
	* @author Gustavo Botega 
	*/
	function ResetarEntidadeEquestre(){
		$("#form-group-entidade-filiada select, #form-group-escola-equitacao select").prop('selectedIndex',0);
		$("#form-group-entidade-filiada, #form-group-escola-equitacao").slideDown();
		$('#form-group-link-remover-entidade').slideUp();
	}




	/**
	* FocusOut - N Chip
	*
	* @author Gustavo Botega 
	*/
	$Chip       = $("#chip");
	$Chip.focusout(function() {

	  $ChipValue  = $("#chip").val();

	  if( $ChipValue.length <= 6){ 
	    ResetChip();
	    return false;
	  }


	});



	$CpfCnpjProprietario 	=	$("#cpf-cnpj-proprietario");
	$CpfCnpjProprietario.focusout(function(){
		$("#btn-localizar-proprietario").click();
	});



	/**
	* ResetChip
	*
	* @author Gustavo Botega 
	*/
	 ResetChip = function(){
		$chip  = $("#chip");
		$chip.val('');
		$chip.focus();
	} 


	/**
	* Change - Genero Animal
	*
	* @author Gustavo Botega 
	*/
	$("input:radio[name='genero']").on("change", function() {
		if($(this).val() == '1'){
			$("#camada-genero-tipo-macho").slideDown();
		}else{
			$('input[name=genero-tipo]').attr('checked',false);
			$("#camada-genero-tipo-macho").slideUp();
		}
	});


	/**
	* Change - Proprietario Animal
	*
	* @author Gustavo Botega 
	*/
	$("input:radio[name='proprietario-flag']").on("change", function() {
		if($(this).val() != '1'){
			$("#camada-cpf-cnpj-proprietario").slideDown();
		}else{
			$("#camada-cpf-cnpj-proprietario").slideUp();
			$("#cpf-cnpj-proprietario").val('');
	    	$("#camada-cpf-cnpj-proprietario-localizado").slideUp();
	    	$("#camada-cpf-cnpj-proprietario-localizar").slideDown();
	    	$("#cpf-cnpj-proprietario").val('');
        	$("input[name='proprietario-cpf-cnpj']").val('');
        	$("input[name='proprietario-id']").val('');
        	$("input[name='proprietario-nome']").val('');

		}
	});


	/**
	* Click - Localizar Proprietario
	*
	* @author Gustavo Botega 
	*/
	$("#btn-localizar-proprietario").click(function(){
	  
	    $.ajax({

	        url : BaseUrl + 'animal/FrontOffice/Cadastro/ConsultarProprietario/',
	        type : 'POST',
	        data: { CpfCnpjProprietario: $("#cpf-cnpj-proprietario").val() },
	        async: false,
	        dataType:'json',
	        success : function(data) {            

	            console.log(data);

	            StringErros     =   ''; 


	            /* Proprietario Localizado */	
	            if(data.StatusSlug == 'ProprietarioEncontrado'){
	            	$("#camada-cpf-cnpj-proprietario-localizar").slideUp();
	            	$("#camada-cpf-cnpj-proprietario-localizado").slideDown();
	            	$("#camada-cpf-cnpj-proprietario-localizado .nome").html(data.PessoaNomeCompleto);
	            	$("#camada-cpf-cnpj-proprietario-localizado .cpf-cnpj").html(data.PessoaCpfCnpj);
	            	$("#camada-cpf-cnpj-proprietario-localizado .id").html(data.PessoaId);
	            	$("input[name='proprietario-cpf-cnpj']").val(data.PessoaCpfCnpj);
	            	$("input[name='proprietario-id']").val(data.PessoaId);
	            	$("input[name='proprietario-nome']").val(data.PessoaNomeCompleto);
	            }


	            /* Nao foi encontrado nenhuma pessoa com o CPF Informado */	
	            if(data.StatusSlug == 'ProprietarioNaoEncontrado'){
	                sweetAlert("Oops... Proprietário não cadastrado!", "O CPF do proprietário informado não consta em nossa base de dados. Antes de prosseguir solicite ao proprietário que se cadastre no sistema para continuar o cadastro do animal.", "error");
			    	$("#cpf-cnpj-proprietario").val('');
	            	$("input[name='proprietario-cpf-cnpj']").val('');
	            	$("input[name='proprietario-id']").val('');
	            }

	            	
	            /* CPF do proprietario informado e o mesmo do autor */	
	            if(data.StatusSlug == 'ProprietarioIgualAutor'){

			    	$("#camada-cpf-cnpj-proprietario").slideUp();
					$("input[name='proprietario-flag'][value='1']").prop("checked",true);
			    	$("#cpf-cnpj-proprietario").val('');
	            	$("input[name='proprietario-cpf-cnpj']").val('');
	            	$("input[name='proprietario-id']").val('');
				
				}

	            


	        },
	        error : function(request,error)
	        {
	            alert("Request: "+JSON.stringify(request));
	            sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");
	        }
	    });


	});



	/**
	* Click - Localizar Proprietario
	*
	* @author Gustavo Botega 
	$("#btn-trocar-proprietario").click(function(e){
		e.preventDefault();

		alert("aaa");

    	$("#camada-cpf-cnpj-proprietario-localizado").slideUp();
    	$("#camada-cpf-cnpj-proprietario-localizar").slideDown();
    	$("#cpf-cnpj-proprietario").val('');
    	$("input[name='proprietario-cpf-cnpj']").val('');
    	$("input[name='proprietario-id']").val('');

	});
	*/

	$("#btn-trocar-proprietario").click(function(e){
		e.preventDefault();

    	$("#camada-proprietario-localizar").slideDown();
    	$("#camada-proprietario-localizado").slideUp();

    	$("#nome-proprietario").val('');
    	$("input[name='proprietario-id']").val('');
    	$("input[name='proprietario-nome']").val('');

	});



	/**
	* Easy Autocomplete - Busca de proprietario pelo nome
	*
	* @author Gustavo Botega 
	*/
	var options = {
    url: BaseUrl + "animal/FrontOffice/Cadastro/JsonProprietario",

    getValue: "name",

    list: {
        match: {
            enabled: true
        },

		showAnimation: {
			type: "slide", //normal|slide|fade
			time: 400,
			callback: function() {}
		},

		hideAnimation: {
			type: "slide", //normal|slide|fade
			time: 400,
			callback: function() {}
		},

		onClickEvent: function() {

			var Objeto 	= $("#nome-proprietario").getSelectedItemData();
			var Id 		= Objeto.id;
			var Nome 	= Objeto.name

			$("#camada-proprietario-localizar").slideUp();
			$("#camada-proprietario-localizado").slideDown();

			$("#camada-proprietario-localizado span.nome").html(Nome);
			$("#camada-proprietario-localizado span.id").html(Id);

			$("#proprietario-id").val(Id);
			$("#proprietario-nome").val(Nome);

		}

    },

    theme: "plate-dark"
};

	$("#nome-proprietario").easyAutocomplete(options);






});