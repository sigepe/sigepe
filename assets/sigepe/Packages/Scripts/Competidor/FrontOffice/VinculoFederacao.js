/**
 * SIGEPE
 * 
 * @package 	Vanguarda
 * @author      Gustavo Botega
 * @copyright   Copyright (c) 2017, Gustavo Botega
 * @link        http://gustavobotega.com
 * @link        http://agenciavanguarda.com.br
 * 
 */

$(document).ready(function(){
    
    
	$('#btn-trocar-federacao').on('click', function(event) {
		event.preventDefault();
        $("body, html").animate({ 
	      scrollTop: $( "#portlet-vinculos" ).offset().top 
	    }, 600);
        $("#tablerow-trocar-federacao").slideDown();
	});
    
	$('#form-trocar-federacao .btn-submit').on('click', function(event) {
		event.preventDefault();
        $("#form-trocar-federacao").submit();
    });
    
    $('#form-trocar-federacao .btn-cancelar').on('click', function(event) {
		event.preventDefault();
        $("#tablerow-trocar-federacao").hide();
        $("body, html").animate({ 
	      scrollTop: $( "#portlet-vinculos" ).offset().top 
	    }, 600);
        $('#form-trocar-federacao .form-group').removeClass('has-error');
        $('#form-trocar-federacao')[0].reset();
    });   

    var FormTrocarFederacao        =   $('#form-trocar-federacao');
    var error       =   $('.alert-danger', FormTrocarFederacao);
    var success     =   $('.alert-success', FormTrocarFederacao);

    FormTrocarFederacao.validate({
        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        rules: {
            
            'federacao': {
                required: true
            },
            'federacao-entidade-equestre': {
                required: true
            },

        },
        messages: {
            'federacao': {
                required: "Selecione sua nova federação.",
            },
            'federacao-entidade-equestre': {
                required: "Todos os animais da FHBr devem estar vinculados a alguma entidade equestre.",
            },
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("name") == "federacao") { // for uniform radio buttons, insert the after the given container
                error.insertAfter("#form-federacao-erro");
            } else if (element.attr("name") == "federacao-entidade-equestre") { // for uniform checkboxes, insert the after the given container
                error.insertAfter("#form-federacao-entidade-equestre-erro");
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success.hide();
            error.show();
            App.scrollTo(error, -85);
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                label
                    .closest('.form-group').removeClass('has-error').addClass('has-success');
                label.remove(); // remove error label here
            } else { // display success icon for other inputs
                label
                    .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            }
        },

        submitHandler: function (FormTrocarFederacao) {
            success.show();
            error.hide();
            ProcessarTrocaFederacao();
            return false;
            //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
        }


    });    

    


    function ProcessarTrocaFederacao() {

        FederacaoSelecionada    =   $("#federacao option:selected").text();

        $("#tablerow-trocar-federacao").hide();
        $("body, html").animate({ 
          scrollTop: $( "#portlet-vinculos" ).offset().top 
        }, 600);
        $('#btn-trocar-federacao').remove();

        $.ajax({

            url : BaseUrl + 'perfil/FrontOffice/Animal/AjaxTrocaFederacao/',
            type : 'POST',
            data: { DataSerialized: $("#form-trocar-federacao").serialize() },
            async: true,
            dataType:'json',
            success : function(data) {            

                console.log(data);

                
                if(data == true){

                    sweetAlert(
                            "Federaçao atualizada!",
                            "Sua solicitaçao foi realizada com sucesso. Aguarde a aprovaçao do vínculo pela federação selecionada.",
                            "success");
                    
                    setTimeout(function () { 
                      location.reload();
                    }, 1 * 1000);

                }else{

                    sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");

                }


            },
            error : function(request,error)
            {
                alert("Request: "+JSON.stringify(request));
                sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");
            }
        });
     
        
        
    }

    

    /**
    * Federacao
    *
    * @author Gustavo Botega 
    */
    $('select#federacao').on('change', function() {
        if(this.value == 141){ // 143 e o ID da FHBr
            $("#form-group-federacao-entidade-equestre").show();
        }else{
            $("#form-group-federacao-entidade-equestre").hide();
            $('#form-group-federacao-entidade-equestre select').prop('selectedIndex',0);
        }
    })
    
    

});
// end jquery 
