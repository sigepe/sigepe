/**
 * SIGEPE
 * 
 * @package 	Vanguarda
 * @author      Gustavo Botega
 * @copyright   Copyright (c) 2017, Gustavo Botega
 * @link        http://gustavobotega.com
 * @link        http://agenciavanguarda.com.br
 * 
 */

$(document).ready(function(){


    jQuery("#btn-detalhes-vinculo").click(function(){
        $("a[href='#tab-vinculo-registro']").click();
//        $("body, html").animate({scrollTop: $( '#tab-vinculo-registro' ).offset().top - 60 }, 600);
    });


	$('.vinculo-detalhe-link').on('click', function(event) {

		event.preventDefault();

		IdVinculo 		=	$(this).data('id');
		NomeEmpresa 	=	$(this).data('nome-empresa');
		IdEmpresa 		=	$(this).data('id-empresa');
		$("#vinculo-detalhe-tabela").slideUp();
		$("#vinculo-detalhe-carregando").slideDown();
		$("#vinculo-detalhe").modal('show');


		$("h4.modal-title").html(NomeEmpresa);

		$("#vinculo-detalhe-id").html(IdVinculo);
		$("#vinculo-detalhe-id-empresa").html(IdEmpresa);

		setTimeout(function(){ 

				// Ajax Success
		        $.ajax({

		            url : BaseUrl + 'cadastro/Consulta/ConsultarHistoricoVinculo/' + IdVinculo,
		            type : 'POST',
		//            data: { DataSerialized: $("#form-registro-atleta").serialize() },
		            async: true,
		            dataType:'json',
		            success : function(data) {            

		                console.log(data);
		                if(typeof(data) == "undefined") { // erro
		                    sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitaÃ§Ã£o.", "error");
		                }else{ // sucesso

							$("#vinculo-detalhe-carregando").slideUp();
							$("#vinculo-detalhe-tabela tbody").html(data);
							$("#vinculo-detalhe-tabela").slideDown();
		                }

		            },
		            error : function(request,error)
		            {
		                sweetAlert("Erro de RequisiÃ§Ã£o!", JSON.stringify(request), "error");
		            }
		        });


		 }, 250);


	});

    
});
// end jquery 


