

/**
 * SIGEPE
 * 
 * @package 	Vanguarda
 * @author      Gustavo Botega
 * @copyright   Copyright (c) 2017, Gustavo Botega
 * @link        http://gustavobotega.com
 * @link        http://agenciavanguarda.com.br
 * 
 */

$(document).ready(function(){







  
    /*

        BLOCO FORM-DADOS-ATLETA
        - Trigger btns
        - Validate

    ========================================================================*/

    var FormDadosAtleta =   $("#form-dados-atleta");

    jQuery("#form-dados-atleta .btn-submit").click(function(){
        $(FormDadosAtleta).submit();
    });

	jQuery("#form-dados-atleta .btn-editar-informacoes").click(function(){

		$(this).hide();

		jQuery("#form-dados-atleta .form-actions").show();
		jQuery(FormDadosAtleta).addClass('form-edit');

    	$("#form-dados-atleta").find('.form-control-static').hide();
    	$("#form-dados-atleta").find('input, select').show();

	    $("body, html").animate({ 
	      scrollTop: $( FormDadosAtleta ).offset().top - 40 
	    }, 600);

        
	});

    // validate signup form on keyup and submit
    $("#form-dados-atleta").validate({
        rules: {'nome-competicao': "required", },
        messages: {'nome-competicao': "Preencha esse campo", },
        submitHandler: function() {

            $.ajax({

                url : BaseUrl + 'perfil/FrontOffice/Atleta/AjaxDadosAtletaProcessar/',
                type : 'POST',
                data: { DataSerialized: $("#form-dados-atleta").serialize() },
                async: false,
                dataType:'json',
                success : function(data) {            

                    console.log(data);

                    StringErros     =   ''; 

                    if(data == true){

                        // Setar value label, Fechar formulario e Aparecer botao.

                        sweetAlert(
                                "Meu perfil atualizado!",
                                "Mantenha seu perfil sempre atualizado. Verifique também e-mail, telefone e endereço se estão atualizados.",
                                "success");                           


                        // Resetando formulario
                        $("#fcs-nome-competicao").html( $('input[name="nome-competicao"]').val() );
                        $("#form-dados-atleta").find('.form-control-static').show();
                        $("#form-dados-atleta").find('input, select').hide();
                        $("#form-dados-atleta .btn-editar-informacoes").show();
                        jQuery("#form-dados-atleta .form-actions").hide();

                        $("body, html").animate({ 
                            scrollTop: $( FormDadosAtleta ).offset().top - 40 
                        }, 600);

                    }else{

                        sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");

                    }


                },
                error : function(request,error)
                {
                    alert("Request: "+JSON.stringify(request));
                    sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");
                }
            });

        }

    });









    /* ---------------------------- */







    /*

        BLOCO NOVO REGISTRO
        - Trigger Click ( Novo Registro )
        - Change - Modalidade
        - Change - Tipo de Registro
        - Change - Tipo de Pagamento
        - Validate Form

    ========================================================================*/

    var form        =   $('#form-registro');
    var error       =   $('.alert-danger', form);
    var success     =   $('.alert-success', form);

    jQuery(".btn-novo-registro").click(function(){
        $("#portlet-formulario-registro").slideDown();
        $("body, html").animate({ scrollTop: $( '#portlet-formulario-registro' ).offset().top - 25 }, 600);
    });

    $("#form-registro-submit").click(function(){
        $("#form-registro").submit();
    });

    /* Validate */
    form.validate({
        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        rules: {
            'modalidade': {
                required: true
            },
            'tipo-registro': {
                required: true
            },
            'tipo-pagamento': {
                required: true
            },
        },
        messages: {
            'modalidade': {
                required: "Informe uma modalidade.",
            },
            'tipo-registro': {
                required: "Selecione um tipo de registro.",
            },
            'tipo-pagamento': {
                required: "Selecione um tipo de pagamento.",
            }
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("name") == "modalidade") { // for uniform radio buttons, insert the after the given container
                error.insertAfter("#form-modalidade-erro");
            } else if (element.attr("name") == "tipo-registro") { // for uniform checkboxes, insert the after the given container
                error.insertAfter("#form-tipo-registro-erro");
            } else if (element.attr("name") == "tipo-pagamento") { // for uniform checkboxes, insert the after the given container
                error.insertAfter("#form-tipo-pagamento-erro");
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success.hide();
            error.show();
            App.scrollTo(error, -85);
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                label
                    .closest('.form-group').removeClass('has-error').addClass('has-success');
                label.remove(); // remove error label here
            } else { // display success icon for other inputs
                label
                    .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            }
        },

        submitHandler: function (form) {
            success.show();
            error.hide();
            SigepeCriarRegistro();
            return false;
        }

    });    






    /*

        VINCULOS
        - Trigger Click ( Detalhe )
        - Troca de Federacao
        - Troca de Entidade

    ========================================================================*/
	$('.vinculo-detalhe-link').on('click', function(event) {

		event.preventDefault();

		IdVinculo 		=	$(this).data('id');
		NomeEmpresa 	=	$(this).data('nome-empresa');
		IdEmpresa 		=	$(this).data('id-empresa');
		$("#vinculo-detalhe-tabela").slideUp();
		$("#vinculo-detalhe-carregando").slideDown();
		$("#vinculo-detalhe").modal('show');


		$("h4.modal-title").html(NomeEmpresa);

		$("#vinculo-detalhe-id").html(IdVinculo);
		$("#vinculo-detalhe-id-empresa").html(IdEmpresa);

		setTimeout(function(){ 


				// Ajax Success
		        $.ajax({

		            url : BaseUrl + 'cadastro/Consulta/ConsultarHistoricoVinculo/' + IdVinculo,
		            type : 'POST',
		//            data: { DataSerialized: $("#form-registro-atleta").serialize() },
		            async: true,
		            dataType:'json',
		            success : function(data) {            


		                console.log(data);
		                if(typeof(data) == "undefined") { // erro
		                    sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitaÃ§Ã£o.", "error");
		                }else{ // sucesso

							$("#vinculo-detalhe-carregando").slideUp();
							$("#vinculo-detalhe-tabela tbody").html(data);
							$("#vinculo-detalhe-tabela").slideDown();
		                }

		            },
		            error : function(request,error)
		            {
		                sweetAlert("Erro de RequisiÃ§Ã£o!", JSON.stringify(request), "error");
		            }
		        });


		 }, 250);


	});

    
    
    /*
        TROCA DE FEDERACAO
    -------------------------------------*/
	$('#btn-trocar-federacao').on('click', function(event) {
		event.preventDefault();
        $("body, html").animate({ 
	      scrollTop: $( "#portlet-vinculos-atleta" ).offset().top 
	    }, 600);
        $("#tablerow-trocar-federacao").slideDown();
	});
    
    
	$('#form-trocar-federacao .btn-submit').on('click', function(event) {
		event.preventDefault();
        $("#form-trocar-federacao").submit();
    });
    
    $('#form-trocar-federacao .btn-cancelar').on('click', function(event) {
		event.preventDefault();
        $("#tablerow-trocar-federacao").hide();
        $("body, html").animate({ 
	      scrollTop: $( "#portlet-vinculos-atleta" ).offset().top 
	    }, 600);
        $('#form-trocar-federacao .form-group').removeClass('has-error');
        $('#form-trocar-federacao')[0].reset();
    });
    
    
    

    var FormTrocarFederacao        =   $('#form-trocar-federacao');
    var error       =   $('.alert-danger', form);
    var success     =   $('.alert-success', form);

    FormTrocarFederacao.validate({
        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        rules: {

            
            'federacao': {
                required: true
            },
            'federacao-entidade-equestre': {
                required: true
            },

        },
        messages: {
            'federacao': {
                required: "Selecione sua nova federação.",
            },
            'federacao-entidade-equestre': {
                required: "Todos os atletas da FHBr devem estar vinculados a alguma entidade equestre.",
            },
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("name") == "federacao") { // for uniform radio buttons, insert the after the given container
                error.insertAfter("#form-federacao-erro");
            } else if (element.attr("name") == "federacao-entidade-equestre") { // for uniform checkboxes, insert the after the given container
                error.insertAfter("#form-federacao-entidade-equestre-erro");
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success.hide();
            error.show();
            App.scrollTo(error, -85);
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                label
                    .closest('.form-group').removeClass('has-error').addClass('has-success');
                label.remove(); // remove error label here
            } else { // display success icon for other inputs
                label
                    .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            }
        },

        submitHandler: function (FormTrocarFederacao) {
            success.show();
            error.hide();
            ProcessarTrocaFederacao();
            return false;
            //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
        },





    });    

    


    function ProcessarTrocaFederacao() {

        FederacaoSelecionada    =   $("#federacao option:selected").text();

        $("#tablerow-trocar-federacao").hide();
        $("body, html").animate({ 
          scrollTop: $( "#portlet-vinculos-atleta" ).offset().top 
        }, 600);
        $('#btn-trocar-federacao').remove();

        $.ajax({

            url : BaseUrl + 'perfil/FrontOffice/Atleta/AjaxTrocaFederacao/',
            type : 'POST',
            data: { DataSerialized: $("#form-trocar-federacao").serialize() },
            async: true,
            dataType:'json',
            success : function(data) {            

                console.log(data);

                
                if(data == true){

                    sweetAlert(
                            "Federaçao atualizada!",
                            "Sua solicitaçao foi realizada com sucesso. Aguarde a aprovaçao do vínculo pela federação selecionada.",
                            "success");
                    
                    setTimeout(function () { 
                      location.reload();
                    }, 1 * 1000);

                }else{

                    sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");

                }


            },
            error : function(request,error)
            {
                alert("Request: "+JSON.stringify(request));
                sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");
            }
        });
     
        
        
    }

    
    
    

        /**************************************
        TROCA ENTIDADE 
        ***************************************/
        $('#btn-trocar-entidade').on('click', function(event) {
            event.preventDefault();
            $("body, html").animate({ 
              scrollTop: $( "#portlet-vinculos-atleta" ).offset().top 
            }, 600);
            $("#tablerow-trocar-entidade").slideDown();
        });


        $('#form-trocar-entidade .btn-submit').on('click', function(event) {
            event.preventDefault();
            $("#form-trocar-entidade").submit();
        });

        $('#form-trocar-entidade .btn-cancelar').on('click', function(event) {
            event.preventDefault();
            $("#tablerow-trocar-entidade").hide();
            $("body, html").animate({ 
              scrollTop: $( "#portlet-vinculos-atleta" ).offset().top 
            }, 600);
            $('#form-trocar-entidade .form-group').removeClass('has-error');
            $('#form-trocar-entidade')[0].reset();
        });


        var FormTrocarEntidade        =   $('#form-trocar-entidade');
        var error       =   $('.alert-danger', form);
        var success     =   $('.alert-success', form);

        FormTrocarEntidade.validate({
            doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {

                'entidade-equestre': {
                    required: true
                },

            },
            messages: {
                'entidade-equestre': {
                    required: "Todos os atletas da FHBr devem estar vinculados a alguma entidade equestre.",
                },
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.attr("name") == "entidade-equestre") { // for uniform radio buttons, insert the after the given container
                    error.insertAfter("#form-entidade-equestre-erro");
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit   
                success.hide();
                error.show();
                App.scrollTo(error, -85);
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                    label
                        .closest('.form-group').removeClass('has-error').addClass('has-success');
                    label.remove(); // remove error label here
                } else { // display success icon for other inputs
                    label
                        .addClass('valid') // mark the current input as valid and display OK icon
                    .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                }
            },

            submitHandler: function (FormTrocarEntidade) {
                success.show();
                error.hide();
                ProcessarTrocaEntidade();
                return false;
                //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
            },





        });    




        function ProcessarTrocaEntidade() {

            EntidadeSelecionada    =   $("#entidade-equestre option:selected").text();

            $("#tablerow-trocar-entidade").hide();
            $("body, html").animate({ 
              scrollTop: $( "#portlet-vinculos-atleta" ).offset().top 
            }, 600);
            $('#btn-trocar-entidade').remove();

            $.ajax({

                url : BaseUrl + 'perfil/FrontOffice/Atleta/AjaxTrocaEntidade/',
                type : 'POST',
                data: { DataSerialized: $("#form-trocar-entidade").serialize() },
                async: true,
                dataType:'json',
                success : function(data) {            

                    console.log(data);

                    
                    if(data == true){

                        sweetAlert(
                                "Entidade Equestre atualizada!",
                                "Sua solicitaçao foi realizada com sucesso. Aguarde a aprovaçao do vínculo pela entidade selecionada.",
                                "success");
                        
                        setTimeout(function () { 
                          location.reload();
                        }, 1 * 1000);

                    }else{

                        sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");

                    }


                },
                error : function(request,error)
                {
                    alert("Request: "+JSON.stringify(request));
                    sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");
                }
            });
         
            
            
        }

    
    
        /**
        * Federacao
        *
        * @author Gustavo Botega 
        */
        $('select#federacao').on('change', function() {
            if(this.value == 141){ // 143 e o ID da FHBr
                $("#form-group-federacao-entidade-equestre").show();
            }else{
                $("#form-group-federacao-entidade-equestre").hide();
                $('#form-group-federacao-entidade-equestre select').prop('selectedIndex',0);
            }
        })








    jQuery("#btn-detalhes-vinculo").click(function(){
        $("a[href='#tab-vinculo-registro']").click();
        $("body, html").animate({scrollTop: $( '#tab-vinculo-registro' ).offset().top - 60 }, 600);
    });




    
    
    
    

});
// end jquery 
