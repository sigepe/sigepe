/**
 * SIGEPE
 *
 * @package 	Vanguarda
 * @author      Gustavo Botega
 * @copyright   Copyright (c) 2017, Gustavo Botega
 * @link        http://gustavobotega.com
 * @link        http://agenciavanguarda.com.br
 *
 */



    /*

        BLOCO PAGAR.ME
        - CheckoutCartaoCredito
        - HandleSuccessCheckout
        - GetErrorMessage

    ========================================================================*/
    function CheckoutCartaoCredito () {

      PagarMePreco      =   $("#pagarme-preco").val();

      var checkout = new PagarMeCheckout.Checkout({
        encryption_key: 'ek_live_IIpzTxJcAkbyzgjbBDXSsYiG73GsWM',
        success: HandleSuccessCheckoutCartaoCredito,
        error: function (data) {
            console.log(data);
        },
        close: function() {

            console.log('The modal has been closed.');

            SigepeRegistroId = $("input[name='sigepe-registro']").val();

            $.ajax({
              type: "POST",
              url: BaseUrl + "registro/FrontOffice/Registro/SetStatusCanceladoJson/",
              data: {SigepeRegistroId: SigepeRegistroId },
              success: function( data ){

                  $("#layer-loading-registro").fadeOut();

                  // Cancelar transacao. Realizar ajax. Levar esse bloco close pra fora.
                  sweetAlert(
                          "Transação Cancelada!",
                          "Seu registro NÃO foi processado.",
                          "error");


                  setTimeout(function(){
                     window.location.href    =   BaseUrl + 'FrontOffice/Perfil/Atleta';
                  }, 5000);


              },
              error: function (jqXHR, exception) {
                  console.log(jqXHR);
                  //GetErrorMessage(jqXHR, exception);
                  sweetAlert(
                          "Aconteceu algum problema!",
                          "Entre em contato com a secretaria da federação para mais detalhes.",
                          "error");
              },
             complete: function (data) {
                $("#layer-loading-registro").fadeOut();
              }

            });





        }
      });

      checkout.open({
        amount: PagarMePreco,
        maxInstallments: 6,
        createToken: 'true',
        paymentMethods: 'credit_card',
        postback_url: BaseUrl + 'financeiro/FrontOffice/PagarmePostbackUrl/Processar',
        customerData: false,
        customer: {
          external_id: '#' + PagarmeExternalId,
          name: PagarmeNome,
          type: 'individual',
          country: 'br',
          email: PagarmeEmail,
          documents: [
            {
              type: 'cpf',
              number: PagarmeCpf,
            },
          ],
          phone_numbers: ['+55' + PagarmeTelefonePrincipal],
          birthday: PagarmeDataNascimento,
        },
        billing: {
          name: PagarmeNome,
          address: {
            country: 'br',
            state: PagarmeEstadoSigla,
            city: PagarmeCidade,
            neighborhood: PagarmeBairro,
            street: PagarmeLogradouro,
            street_number: PagarmeNumero,
            zipcode: PagarmeCep
          }
        },
        items: [
          {
            id: $("input[name='sigepe-taxa']").val(), // ret_id
            title: 'Registro de Competidor',
            unit_price: PagarMePreco,
            quantity: 1,
            category: $("input[name='tipo-competidor']").val() == '1' ? 'Atleta' : 'Animal', // Tipo do Competidor
            tangible: false
          }
        ]
      })


    }
    // CheckoutCartaoCredito




  function HandleSuccessCheckoutCartaoCredito (data) {

      PagarMePreco      =   $("#pagarme-preco").val();

      $("#layer-loading-registro").fadeIn();

      $.ajax({
        type: "POST",
        url: BaseUrl + "financeiro/FrontOffice/PagarmeVanguarda/HandleNaturezaOperacao",
        data: {
                DatasetPagarme: data,
                PagarmeAmount: PagarMePreco,
                FormSerialized: $( "#form-registro" ).serialize(),
                SigepeTransacao: $("input[name='sigepe-transacao']").val(),
                SigepeRegistroId: $("input[name='sigepe-registro']").val(),
                SigepeNaturezaOperacao: 1
            },
        success: function( data ){

            $("#layer-loading-registro").fadeOut();


                sweetAlert(
                        "Registro ativo!",
                        "Seu registro foi realizado com sucesso. Em 5 segundos você será redirecionado para a fatura de pagamento.",
                        "success");

            setTimeout(function(){
                window.location.href    =   BaseUrl + 'financeiro/FrontOffice/Fatura/Fatura/' + $("input[name='sigepe-transacao']").val();
            }, 5000);


        },
        error: function (jqXHR, exception) {
            console.log(jqXHR);
            GetErrorMessage(jqXHR, exception);
            sweetAlert(
                    "Oops.. aconteceu algum problema!",
                    "Seu registro NÃO foi processado. Entre em contato com a secretaria da federação para mais detalhes.",
                    "error");
        },
       complete: function (data) {
          $("#layer-loading-registro").fadeOut();
        }

      });

        setTimeout(function(){
          $("#layer-loading-registro").fadeOut();
            sweetAlert(
                    "Erro!",
                    "Timeout solicitação de registro. Seu registro NÃO foi processado. Entre em contato com a secretaria da federação para mais detalhes.",
                    "error");

        }, 75 *1000);

  }











    function CreateTransactionBoleto () {

        $("#layer-loading-registro").fadeIn();

        SigepeFatura      =   $("input[name='sigepe-transacao']").val();
        SigepeRegistro    =   $("input[name='sigepe-registro']").val();
        PagarMePreco      =   $("#pagarme-preco").val();
        pagarme.client.connect({ encryption_key: 'ek_live_IIpzTxJcAkbyzgjbBDXSsYiG73GsWM'  })
        .then(

            client => client.transactions.create(
                {
                    amount: PagarMePreco,
                    external_id: '#' + PagarmeExternalId,
                    payment_method: 'boleto',
                    postback_url: BaseUrl + 'financeiro/FrontOffice/PagarmePostbackUrl/Processar',
                    boleto_instructions: 'Registro de Atleta ',
                    boleto_expiration_date: GetVencimentoBoleto(),
                    customer: {
                      type: 'individual',
                      name: PagarmeNome,
                      country: 'br',
                      email: PagarmeEmail,
                      documents: [
                        {
                          type: 'cpf',
                          number: PagarmeCpf,
                        },
                      ],
                      phone_numbers: ['+55' + PagarmeTelefonePrincipal],
                      birthday: PagarmeDataNascimento
                    },
                    billing: {
                      name: PagarmeNome,
                      address: {
                        country: 'br',
                        state: PagarmeEstadoSigla,
                        city: PagarmeCidade,
                        neighborhood: PagarmeBairro,
                        street: PagarmeLogradouro,
                        street_number: PagarmeNumero,
                        zipcode: PagarmeCep
                      }
                    },
                    items: [
                      {
                        id: '1',
                        title: 'Registro de Atleta',
                        unit_price: PagarMePreco,
                        quantity: 1,
                        tangible: false
                      }
                    ]
                }
            )

        ).then(

            transaction => HandleBoleto(transaction, PagarMePreco)
        )

    }
    // END CreateTransactionBoleto


    function GetVencimentoBoleto(){

        startdate = moment().format('DD-MM-YYYY');;
        var new_date = moment(startdate, "DD-MM-YYYY").add('days', 1);
        var day = new_date.format('DD');
        var month = new_date.format('MM');
        var year = new_date.format('YYYY');
        return year + '-' + month + '-' + day;

    }



    function HandleBoleto(Transaction, PagarMePreco){

        console.log(Transaction);

        // Se a transacao do boleto for autorizada executa a captura
        if(Transaction.object == 'transaction' && Transaction.status == 'authorized'){

          $.ajax({
            type: "POST",
            url: BaseUrl + "financeiro/FrontOffice/PagarmeVanguarda/HandleNaturezaOperacao",
            data: {
                    DatasetPagarme: Transaction,
                    PagarmeAmount: PagarMePreco,
                    FormSerialized: $( "#form-registro" ).serialize(),
                    SigepeTransacao: $("input[name='sigepe-transacao']").val(),
                    SigepeRegistroId: $("input[name='sigepe-registro']").val(),
                    SigepeNaturezaOperacao: 1
                },
            dataType:'json',
            success: function( data ){
              console.log(data);
                $("#layer-loading-registro").fadeOut();
                sweetAlert(
                        "Solicitação de Registro criada!",
                        "Seu registro só terá validade após compensação do boleto bancário. Você será redirecionado para o boleto em 5 segundos.",
                        "success");

                    setTimeout(function(){
                        window.location.replace(data.BoletoUrl);
                    }, 5000);
            },
            error: function (jqXHR, exception) {
                $("#layer-loading-registro").fadeOut();
                console.log(jqXHR);
                GetErrorMessage(jqXHR, exception);
                sweetAlert(
                        "Oops.. aconteceu algum problema!",
                        "Seu registro NÃO foi processado. Entre em contato com a secretaria da federação para mais detalhes.",
                        "error");
            },
           complete: function (data) {
              $("#layer-loading-registro").fadeOut();
            }

          });


        }else{
            alert("ERRO! CONSULTE A FEDERACAO. CODIGO DO ERRO: GMB4628");
        }

    }
    //













    // This function is used to get error message for all ajax calls
    function GetErrorMessage(jqXHR, exception) {
        var msg = '';
        if (jqXHR.status === 0) {
            msg = 'Not connect.\n Verify Network.';
        } else if (jqXHR.status == 404) {
            msg = 'Requested page not found. [404]';
        } else if (jqXHR.status == 500) {
            msg = 'Internal Server Error [500].';
        } else if (exception === 'parsererror') {
            msg = 'Requested JSON parse failed.';
        } else if (exception === 'timeout') {
            msg = 'Time out error.';
        } else if (exception === 'abort') {
            msg = 'Ajax request aborted.';
        } else {
            msg = 'Uncaught Error.\n' + jqXHR.responseText;
        }
        alert(msg);
    }
