jQuery(document).ready(function() {    




	/**
	 * Change - CPF Isento
	 *
	 * @author Gustavo Botega 
	 * @return 
	 */
	$('#cpf-isento').change(function() {
	    $('#cpf').attr('disabled', this.checked)
	    $('#cpf').val('');
	});


	$("#cpf-isento").change(function() {
	    if(this.checked) {
			$("#layer-cpf").slideUp();
			$("#usuario-aviso").slideDown();
	    }else{
			$("#layer-cpf").slideDown();
			$("#usuario-aviso").slideUp();
	    }
	});




	/**
	 * Data Nascimento
	 *
	 * @author Gustavo Botega 
	 * @return 
	 */
	$( "#data-nascimento-fundacao" ).focusout(function() {
		
		
		$DataNascimento  =   $("#data-nascimento-fundacao");

	  if( $DataNascimento.val().length == 10 ){

	      DateBirthPiece  =   $DataNascimento.val().split('/');
	      DateBirthIso    =   DateBirthPiece[2] + '-' + DateBirthPiece[1] + '-' + DateBirthPiece[0];
	      dob             =   new Date(DateBirthIso);
	      var today       =   new Date();
	      var age         =   Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));

	      if(age > 100){
	          $DataNascimento.val('');
	          $DataNascimento.focus();
	      }

	      if(age >= 18 && age < 100){
	          $("#layer-cpf-isento").slideUp();
			  $('#cpf').prop('disabled', false);
	          $("html, body").animate({ scrollTop: $('#tab1').offset().top }, 1000);
	      }

	      if(age < 18){
	          $("#layer-cpf-isento").slideDown();
	      }

	      if(isNaN(age)){
	          //toastr["error"]("<b>Erro!</b> <br /> Preencha o campo corretamente. A data deve ser no formato: dd/mm/aaaa  ");
	          $DataNascimento.val('');
	          $DataNascimento.focus();
	      }

	  }else{

	    //  toastr["error"]("<b>Erro!</b> <br /> Preencha o campo corretamente. A data deve ser no formato: dd/mm/aaaa  ");
	      $DataNascimento.val('');
	      $DataNascimento.focus();
	  }
	  

	});








});