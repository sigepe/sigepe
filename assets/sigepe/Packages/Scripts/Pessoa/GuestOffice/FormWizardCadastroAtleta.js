var FormWizard = function () {


    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().bootstrapWizard) {
                return;
            }


            var form        =   $('#form-atleta');
            var error       =   $('.alert-danger', form);
            var success     =   $('.alert-success', form);

            form.validate({
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {

                    
                    'nome-competicao': {
                        required: true
                    },
                    'registro-cbh': {
                        number: true
                    },
                    'registro-fei': {
                        number: true
                    },
                    'federacao': {
                        required: true,
                    },
                    'entidade-filiada': {
                        required: true,
                    },
                    'escola-equitacao': {
                        required: true,
                    },

                    'data-nascimento-fundacao': {
                        required: true
                    },
                    'cpf-cnpj': {
                        required: true,
                        cpfBR: true
                    },
                    'nome-razao-social': {
                        minlength: 6,
                        required: true,
                        lettersonlywithspace: true
                    },
                    sexo: {
                        required: true
                    },
                    numero: {
                        minlength: 15,
                        required: true
                    },
                    email: {
                        minlength: 5,
                        required: true,
                        email: true
                    },
                    senha: {
                        minlength: 6,
                        required: true
                    },
                    'confirmar-senha': {
                        minlength: 6,
                        required: true,
                        equalTo: "#senha"
                    }
                    
                },
                messages: {
                    'nome-competicao': {
                        required: "Qual o seu nome de competição?",
                    },
                    'registro-cbh': {
                        number: "Registro CBH deve conter apenas números.",
                    },
                    'registro-fei': {
                        number: "Registro FEI deve conter apenas números.",
                    },
                    'federacao': {
                        required: "Selecione uma federação"
                    },
                    'entidade-filiada': {
                        required: "Selecione uma entidade filiada."
                    },
                    'escola-equitacao': {
                        required: "Selecione uma escola de equitação."
                    },
                    'data-nascimento-fundacao': {
                        required: "Digite sua data de nascimento"
                    },
                    'cpf-cnpj': {
                        required: "Insira seu CPF",
                        cpfBR: 'Informe um CPF válido'
                    },
                    'nome-razao-social': {
                        required: "Digite seu nome",
                        minlength: 'Digite seu nome completo',
                        lettersonlywithspace: 'Digite um nome válido. Apenas letras.'
                    },
                    sexo: {
                        required: "Informe seu sexo"
                    },
                    numero: {
                        minlength: 'Digite seu celular com DDD',
                        required: 'Digite seu celular'
                    },
                    email: {
                        email: 'Informe um e-mail válido',
                        required: 'Digite seu e-mail'
                    },
                    senha: {
                        required: 'Digite uma senha para sua conta',
                        minlength: 'Sua senha deve ter pelo menos 6 dígitos'
                    },
                    'confirmar-senha': {
                        equalTo: "As senhas não conferem",
                        minlength: '',
                        required: 'Confirme a senha'
                    },
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_gender_error");
                    } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                        error.insertAfter("#form_payment_error");
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                        label
                            .closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid') // mark the current input as valid and display OK icon
                        .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },

                submitHandler: function (form) {
                    success.show();
                    error.hide();
                    form[0].submit();
                    //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
                }

            });

            var displayConfirm = function() {
                $('#tab3 .form-control-static', form).each(function(){
                    var input = $('[name="'+$(this).attr("data-display")+'"]', form);
                    if (input.is(":radio")) {
                        input = $('[name="'+$(this).attr("data-display")+'"]:checked', form);
                    }
                    if (input.is(":text") || input.is("textarea")) {
                        $(this).html(input.val());
                    } else if (input.is("select")) {
                        $(this).html(input.find('option:selected').text());
                    } else if (input.is(":radio") && input.is(":checked")) {
                        $(this).html(input.attr("data-title"));
                    } else if ($(this).attr("data-display") == 'payment[]') {
                        var payment = [];
                        $('[name="payment[]"]:checked', form).each(function(){ 
                            payment.push($(this).attr('data-title'));
                        });
                        $(this).html(payment.join("<br>"));
                    }
                });
                if($('#cpf-isento').is(':checked')){
				    $("p[data-display='cpf-cnpj']").text('isento');
                    // Inserir aqui o CPF Usuario Gerado
                    $("#resumo-form-group-cpf label").html('Usuário:');
                    $("#resumo-form-group-cpf p.form-control-static").html( $("input[name='usuario-menor-idade']").val() );
                }else{
                    $("#resumo-form-group-cpf label").html('CPF:');
                }


                if($("#federacao").val() == 141){ // 141 id de FHBr ( tb_pessoa )
                    
                    if($("#entidade-filiada").val().length > 1){
                        $("#form-group-resume-entidade-filiada").show();                        
                        $("#form-group-resume-escola-equitacao").hide();                        
                    }else{
                        $("#form-group-resume-escola-equitacao").show();                        
                        $("#form-group-resume-entidade-filiada").hide();                        
                    }

                }else{
                    $("#form-group-resume-entidade-filiada, #form-group-resume-escola-equitacao").hide();
                }


            }

            var SubmitForm = function() {


                if( $('#cpf-isento').is(':checked') == false ){
                    if( CpfCnpjExiste( $("#form-atleta #cpf-cnpj").val() ) == true )
                        return AvisoCpfExiste();
                }



                $.ajax({

                    url : BaseUrl + 'pessoa/GuestOffice/CadastroAtleta/AjaxProcessar/',
                    type : 'POST',
                    data: { DataSerialized: $("#form-atleta").serialize() },
                    async: false,
                    dataType:'json',
                    success : function(data) {            


                        console.log("----");
                        console.log(data);


                        StringErros     =   ''; 

                        if(data == true){
                            $("#form-atleta").slideUp();
                            sweetAlert(
                                    "Cadastro realizado com sucesso!",
                                    "Seja bem-vindo! Em alguns instantes você será direcionado para a página de identificação.",
                                    "success");                            
                           setTimeout(function () {
                               window.location.href = BaseUrl + "login"; //will redirect to your blog page (an ex: blog.html)
                            }, 2000); //will call the function after 2 secs.
                        }else{

                            /*
                            $('#form-atleta').bootstrapWizard('show',0);
                            $('#form-atleta').find('.button-previous').hide();
                            $('#form-atleta').find('.button-submit').hide();
                            $('#form-atleta').find('.button-next').show();

                            TamanhoErros    =   data.length;
                            Increment       =   1;
                            $.each(data,function(index, value){
                                StringErros += value;
                                    
                                if(Increment!=TamanhoErros)
                                    StringErros += " | ";


                                Increment++;
                            });
                            */

                            sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");

                        }


                    },
                    error : function(request,error)
                    {
                        alert("Request: "+JSON.stringify(request));
                        sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");
                    }
                });


                $('#form-atleta .button-submit').removeClass('disabled');

	        }
	
	        var AvisoCpfExiste = function() {
				$('#form-atleta').bootstrapWizard('show',1);
				$('#form-atleta').find('.button-previous').hide();
				$('#form-atleta').find('.button-submit').hide();
                $('#form-atleta').find('.button-next').show();
				$('#form-atleta #cpf-cnpj').val('');

                // selecionar 
                $( "ul.nav-pills li:eq(1)" ).removeClass('done');

				// show here pretty lightbox
                sweetAlert("Oops... CPF já existe", "O CPF informado já está cadastrado! Ligue para FHBr: (61) 3245-5870 para mais detalhes", "error");
				
                App.scrollTo($('.page-title'));

                $('#form-atleta .button-submit').removeClass('disabled');

            }


            var handleTitle = function(tab, navigation, index) {
                var total = navigation.find('li').length;
                var current = index + 1;
                // set wizard title
                $('.step-title', $('#form-atleta')).text('Etapa ' + (index + 1) + ' de ' + total);
                // set done steps
                jQuery('li', $('#form-atleta')).removeClass("done");
                var li_list = navigation.find('li');
                for (var i = 0; i < index; i++) {
                    jQuery(li_list[i]).addClass("done");
                }

                if (current == 1) {
                    $('#form-atleta').find('.button-previous').hide();
                } else {
                    $('#form-atleta').find('.button-previous').show();
                }

                if (current >= total) {
                    $('#form-atleta').find('.button-next').hide();
                    $('#form-atleta').find('.button-submit').show();
                    displayConfirm();
                } else {
                    $('#form-atleta').find('.button-next').show();
                    $('#form-atleta').find('.button-submit').hide();
                }
                App.scrollTo($('.page-title')); 
            }



            // default form wizard
            $('#form-atleta').bootstrapWizard({

                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',

                onTabClick: function (tab, navigation, index, clickedIndex) {
                    return false;
                    
                    success.hide();
                    error.hide();
                    if (form.valid() == false) {
                        return false;
                    }
                    
                    handleTitle(tab, navigation, clickedIndex);
                },

                onNext: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    if (form.valid() == false) {
                        return false;
                    }

                    handleTitle(tab, navigation, index);
                },

                onPrevious: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    handleTitle(tab, navigation, index);
                },

                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#form-atleta').find('.progress-bar').css({
                        width: $percent + '%'
                    });
                }

            });


            $('#form-atleta').find('.button-previous').hide();
            $('#form-atleta .button-submit').click(function () {

                $('#form-atleta .button-submit').addClass('disabled');

            	SubmitForm();
//                alert('Finished! Hope you like it :)');

            }).hide();

        }
        // end init

    }; // return

}(); // end function FormWizard()

FormWizard.init();
