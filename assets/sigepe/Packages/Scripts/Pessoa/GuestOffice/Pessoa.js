/**
 * SIGEPE
 * 
 * @package 	Vanguarda
 * @author      Gustavo Botega
 * @copyright   Copyright (c) 2015, Gustavo Botega
 * @link        http://gustavobotega.com
 * @link        http://agenciavanguarda.com.br
 * 
 */
var Pessoa  = function () {



	/*
	CpfExiste()
	------------------------------------------*/
    CpfCnpjExiste = function(CpfCnpj) {


    	console.log(CpfCnpj);
    	CpfCnpj = RemoverPontuacaoCpfCnpj(CpfCnpj);
    	RetornoDeDados 	=	'';
    	Erro  =	undefined;

		$.ajax({
		  url : BaseUrl + "pessoa/Pessoa/CpfCnpjExiste/",
		  type : 'post',
          dataType: "json",
          async:false,
		  data : {
		       CpfCnpj : CpfCnpj
		  },
//		  beforeSend : function(){}
		})
		.done(function(result){

			RetornoDeDados = result;

		})
		.fail(function(jqXHR, textStatus, msg){
		  alert(jqXHR);
		  alert(textStatus);
		  alert(msg);
		}); 

		if(Erro != undefined)
			return 'Erro';

		return RetornoDeDados;

    }




	/*
	RemoverPontuacaoCpfCnpj()
	------------------------------------------*/
    RemoverPontuacaoCpfCnpj = function(CpfCnpj) {

    	console.log(CpfCnpj);

    	CpfCnpjFormatado = CpfCnpj.split('.').join("");
    	CpfCnpjFormatado = CpfCnpjFormatado.split('-').join("");
    	CpfCnpjFormatado = CpfCnpjFormatado.split('/').join("");

    	return CpfCnpjFormatado;
    }



	/*
	CnpjExiste()
	------------------------------------------*/
    window.CnpjExiste = function() {
		$("#modalNoPermission").modal('show');
    }





	/*
	handleGeneralFunctions
	------------------------------------------*/
    var handleGeneralFunctions = function() {

		/*
		preventDefault	
		------------------------------------------*/
		$(".preventDefault").click(function(e){
			e.preventDefault();
		});


    }



    return {
        init: function () {
            handleGeneralFunctions();
        }
    };

}();

