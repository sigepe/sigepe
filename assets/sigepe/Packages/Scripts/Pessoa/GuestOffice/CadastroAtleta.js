jQuery(document).ready(function() {    


	$('select#federacao').on('change', function() {
		if(this.value == 141){ // 143 e o ID da FHBr
			$("#form-group-entidade-filiada, #form-group-escola-equitacao").slideDown();
//			alert("exibir box");
		}else{
			$("#form-group-entidade-filiada, #form-group-escola-equitacao").slideUp();
			$("#form-group-entidade-filiada select, #form-group-escola-equitacao select").prop('selectedIndex',0);
		}
 	})



	$('select#entidade-filiada').on('change', function() {
		
		if(this.value.length==0){
			ResetarEntidadeEquestre();
		}else{
			$('#form-group-escola-equitacao').slideUp();
			$("#form-group-link-remover-entidade").slideDown();
		}

	});


	$('select#escola-equitacao').on('change', function() {
		
		if(this.value.length==0){
			ResetarEntidadeEquestre();
		}else{
			$('#form-group-entidade-filiada').slideUp();
			$("#form-group-link-remover-entidade").slideDown();
		}

	});

	$('#link-trocar-entidade').on('click', function(event) {
	    event.preventDefault();
	    ResetarEntidadeEquestre();
	});


	function ResetarEntidadeEquestre(){
		$("#form-group-entidade-filiada select, #form-group-escola-equitacao select").prop('selectedIndex',0);
		$("#form-group-entidade-filiada, #form-group-escola-equitacao").slideDown();
		$('#form-group-link-remover-entidade').slideUp();
	}



});