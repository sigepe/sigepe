var FormValidation = function () {

    var handleValidation1 = function(){

        $(".btn-salvar").click(function(){

            var form1 = $('#form-serie');
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "", // validate all fields including form hidden input
                rules: {
                    'pes-cpf': { required: true },
                    'pes-nome-completo': { required: true },
                    'pes-data-nascimento': { required: true },
                    'pes-status': { required: true },
                    'pes-genero': { required: true },
                    'pes-apelido': { required: true },
                    'pes-estado-civil': { required: true },
                    'pes-nacionalidade': { required: true },
                    'pes-naturalidade': { required: true },
                    'pes-tipo-sanguineo': { required: true },
                    'pes-escolaridade': { required: true },
                    'pes-nome-mae': { required: true },
                },
                messages: {
                    'pes-cpf': { required: "Informe o CPF" },
                    'pes-nome-completo': { required: "Informe o Nome Completo" },
                    'pes-data-nascimento': { required: "Informe a Data de Nascimento" },
                    'pes-status': { required: "Informe o Status" },
                    'pes-genero': { required: "Informe o Gênero" },
                    'pes-apelido': { required: "Informe o Apelido" },
                    'pes-estado-civil': { required: "Informe o Estado Civil" },
                    'pes-nacionalidade': { required: "Informe a Nacionalidade" },
                    'pes-naturalidade': { required: "Informe a Naturalidade" },
                    'pes-tipo-sanguineo': { required: "Informe o Tipo Sanguíneo" },
                    'pes-escolaridade': { required: "Informe a Escolaridade" },
                    'pes-nome-mae': { required: "Informe o Nome da Mãe" },
                },

                invalidHandler: function(event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                errorPlacement: function(error, element) { // render error placement for each input type
                    var cont = $(element).parent('.input-group');


                    if (cont.size() > 0) {
                        cont.after(error);
                    } else {
                        element.after(error);
                    }

                    if (element.attr("type") == "radio") {
                        error.insertAfter(element);
                    } else {
                        error.insertAfter(element);
                    }

                },

                highlight: function(element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function(element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function(label) {
                    label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function(form) {
                    success1.show();
                    error1.hide();
                    formSubmitAjax();
                }
            }); // form1.validate


            function formSubmitAjax() {
            
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: BaseUrl + 'pessoa/BackOffice/Pessoa/processar/',
                    data: {
                        dataJson: form1.serializeFormJSON()
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR);
                        console.log(textStatus);
                        console.log(errorThrown);
                        sweetAlert("Erro!", "Solicitação não processada. Solicite suporte técnico.", "error"); //alert("Server down. Try Later");
                    },
                    success: function(data) {

                        console.log(data);
                        if (data == true) {
                            if ($('input[name=id]').val() == undefined) {
                                sweetAlert("Sucesso!", "Prova cadastrada.", "success");
                                document.getElementById("form-prova").reset(); // limpa formulário
                            } else {
                                sweetAlert("Sucesso!", "Prova atualizada.", "success");
                                setTimeout(function() {
                                    //window.history.back();
                                }, 500);
                            }
                        } else {
                            sweetAlert("Erro!", "Solicitação não processada. Solicite suporte técnico.", "error");
                        }
                    }
                });
            } // formSubmitAjax

        }); // click

    } // handleValidation1


    return {
        init: function(){
            handleValidation1();
        }
    }; //return

}();




var FormHide = function() {

    var hideFormDados = function() {
        $(".btn-edit").click(function(){
            $(".form-control-static").hide();
            $(".form-control-editable").show();
        });

        $(".btn-cancel").click(function(){        
            $(".form-control-static").show();
            $(".form-control-editable").hide();
        });
    }

    return {
        init: function() {
            hideFormDados();
        }
    };
}();

jQuery(document).ready(function() {
    FormHide.init();
    FormValidation.init();

    $(".form-control-editable").each(function(){
        $(this).hide();
    });

});