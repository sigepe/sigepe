/**
 * SIGEPE
 * 
 * @package 	Vanguarda
 * @author      Gustavo Botega
 * @copyright   Copyright (c) 2017, Gustavo Botega
 * @link        http://gustavobotega.com
 * @link        http://agenciavanguarda.com.br
 * 
 */

$(document).ready(function(){


        



    $('#estado').change(function(){

        if( $(this).val() ) {
            $('#cidade').hide();
            $('.carregando').show();

            $.ajax({
                url : BaseUrl + 'perfil/FrontOffice/Endereco/ConsultaCidadeAjax',
                type : 'POST',
                data: {
                    EstadoId: $('#estado').find(":selected").data('id'),
                    EstadoSigla: $(this).data('sigla'),
                    ajax: true
                },
                async: false,
                dataType:'json',
                success : function(data) {            

                    var options = '<option value=""></option>'; 
                    for (var i = 0; i < data.length; i++) {
                        options += '<option value="' + data[i].CidadeId + '"  data-cidade="'+data[i].CidadeNome+'" >' + data[i].CidadeNome + '</option>';
                    }   
                    $('#cidade').html(options).show();
                    $('.carregando').hide();

                },
                error : function(request,error) { alert("Request: "+JSON.stringify(request)); }
            });





        } else {
            $('#cidade').html('<option value="">– Escolha um estado –</option>');
        }
    });












    var options =  {
      onComplete: function(cep) {
        console.log(cep);

        var cep = cep.replace(/-/g, "");

        // Validação do CEP; caso o CEP não possua 8 números, então cancela
        // a consulta
        if(cep.length != 8){
            return false;
        }
        
        // A url de pesquisa consiste no endereço do webservice + o cep que
        // o usuário informou + o tipo de retorno desejado (entre "json",
        // "jsonp", "xml", "piped" ou "querty")
        var url = "http://viacep.com.br/ws/"+cep+"/json/";
        
        $.getJSON(url, function(dadosRetorno){
            try{

                console.log();

                if (   typeof dadosRetorno.cep == 'undefined'){
                    sweetAlert("Oops... Não encontramos esse CEP!", " O CEP informado não consta em nossa base de dados. Tente novamente. Se persistir o problema entre em contato com a secretaria da federação. ", "error");
                    $("#cep").val('');
                    $("#cep").focus();
                    return false;
                }



                // Carregando Estado Selecionada
                jQuery('#estado option[data-sigla="'+dadosRetorno.uf+'"]').prop('selected', true);


   

                // Carregando Cidade Selecionada
                $.ajax({
                url : BaseUrl + 'perfil/FrontOffice/Endereco/ConsultaCidadeAjax',
                type : 'POST',
                data: { 
                    EstadoId: $('#estado').find(":selected").data('id'),
                    EstadoSigla: $('#estado').data('sigla'),
                    ajax: true
                },
                async: false,
                dataType:'json',
                success : function(data) {         


                    var options = '<option value=""></option>'; 
                    for (var i = 0; i < data.length; i++) {
                        options += '<option value="' + data[i].CidadeId + '"  data-cidade="'+data[i].CidadeNome+'" >' + data[i].CidadeNome + '</option>';
                    }   
                    $('#cidade').html(options).show();
                    $('.carregando').hide();



                    CidadeNome   =   dadosRetorno.localidade.toUpperCase();
                    jQuery('#cidade option[data-cidade="'+CidadeNome+'"]').attr('selected', 'selected');



                    console.log(dadosRetorno.localidade);

                },
                error : function(request,error) {
                    alert("Request: "+JSON.stringify(request));
                }
            });



                // Preenche os campos de acordo com o retorno da pesquisa
                $("input[name='logradouro']").val(dadosRetorno.logradouro);
                $("input[name='bairro']").val(dadosRetorno.bairro);

                $("#cep").blur(); 


            }catch(ex){
                console.log("teste");
                alert("teste");
            }
        });


      },
    };

    $('#cep').mask('00000-000', options);    





    /*
        TRIGGER 
        BTN-DELETAR-ENDERECO
    ===========================================================*/
    $('body').on('click', '.btn-deletar-endereco', function (){

    	TableRowId 		=	$(this).parent().parent().data('id');

        swal({
          title: "Você tem certeza?",
          text: "Esse procedimento irá deletar o endereço associado a sua conta. Esse procedimento é irreversível.",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Sim, deletar endereço!",
          closeOnConfirm: false
        },
        function(){
            $.ajax({
                url : BaseUrl + 'perfil/FrontOffice/Endereco/Deletar',
                type : 'POST',
                data: { EnderecoId: TableRowId },
                async: false,
                dataType:'json',
                success : function(data) {            
                    if(data != false){
                        $("#table-row-" + TableRowId).fadeOut();
                        $("body, html").animate({ 
                          scrollTop: $( "#perfil-endereco" ).offset().top 
                        }, 600);
                        swal("Endereço deletado!", "Ação realizada com sucesso.", "success");
                    }else{
                        sweetAlert("Oops... Aconteceu algo de errado!", " Houve algum erro ao processar sua solicitação de deletar endereço. Tente novamente. ", "error");
                        return;                                    
                    }
                },
                error : function(request,error) { alert("Request: "+JSON.stringify(request)); }
            });
        });

	});





    /*
        TRIGGER 
        BTN-ENDERECO-PRINCIPAL
    ===========================================================*/
    $('body').on('click', '.btn-endereco-principal', function (){

        TableRowId      =   $(this).parent().parent().data('id');

        swal({
          title: "Você tem certeza?",
          text: "Esse procedimento irá transformar esse endereço como principal.",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-warning",
          confirmButtonText: "Sim!",
          closeOnConfirm: false
        },
        function(){
            $.ajax({
                url : BaseUrl + 'perfil/FrontOffice/Endereco/EnderecoPrincipal',
                type : 'POST',
                data: { EnderecoId: TableRowId },
                async: false,
                dataType:'json',
                success : function(data) {            

                    if(data != false){
                        
                        // Remover todos os Flag P que houver.
                        $("#table-endereco").find('.flag-endereco-principal').remove();


                        // HTML dos btns
                        HtmlBtnDeletarEndereco   =   "<a href='javascript:;' class='btn btn-circle btn-sm btn-danger btn-deletar-endereco'> <i class='fa fa-close' aria-hidden='true'></i> Deletar Endereço </a>";
                        HtmlBtnEnderecoPrincipal    =   "<a href='javascript:;' class='btn btn-circle btn-sm btn-warning btn-endereco-principal tooltips' data-original-title='Transformar esse endereço como principal.'> <i class='fa fa-pencil' aria-hidden='true'></i> Endereço Principal </a>";


                        // Inserindo Botão de Endereco Principal e Deletar Endereco para o antigo endereco principal
                        $("#table-endereco tbody tr").each(function(){

                            if( ! $(this).find('.btn-endereco-principal').length )
                                $(this).find('td:eq(1)').append(HtmlBtnEnderecoPrincipal);

                            if( ! $(this).find('.btn-deletar-endereco').length )
                                $(this).find('td:eq(1)').append(HtmlBtnDeletarEndereco);

                        });                        


                        // HTML flag Endereco Principal
                        HtmlTagEnderecoPrincipal    =   "<a href='javascript:;' class='tooltips badge badge-warning bold flag-endereco-principal' title='' data-original-title='Endereço Principal. Preferencialmente vamos utilizar esse número para entrar em contato com você.'> P </a>";


                        // Atualizando na linha
                        $("#table-row-" + TableRowId).find('.btn-endereco-principal').remove();
                        $("#table-row-" + TableRowId).find('.btn-deletar-endereco').remove();
                        $("#table-row-" + TableRowId).children("td:first").prepend(HtmlTagEnderecoPrincipal);


                        // Resetando componentes
                         $(".popovers").popover();  // Hack para reinicializar popover. Popover nao funciona para elementos carregados dinamicamente via ajax.
                         $(".tooltips").tooltip();  // Hack para reinicializar 

                        // Alerta
                        swal("Endereço alterado como principal!", "Ação realizada com sucesso.", "success");

                    }else{
                        sweetAlert("Oops... Aconteceu algo de errado!", " Houve algum erro ao processar sua solicitação de deletar endereço. Tente novamente. ", "error");
                        return;                                    
                    }

                },
                error : function(request,error)
                {
                    alert("Request: "+JSON.stringify(request));
                }
            });

        });

    });





    /*
        TRIGGER 
        BTN-CADASTRAR-ENDERECO
    ===========================================================*/
    jQuery("#perfil-endereco .portlet-title .btn-cadastrar-endereco, #perfil-endereco #aviso-nenhum-endereco").click(function(e){
        e.preventDefault();
        $("#form-endereco").slideDown();
        $("body, html").animate({ 
          scrollTop: $( "#form-endereco" ).offset().top 
        }, 600);
    });

    $("#perfil-endereco .form-actions .btn-submit").click(function(){
        $("#form-endereco").submit();
    });



    /*
        FORM VALIDATION
    ===========================================================*/
    var form        =   $('#form-endereco');
    var error       =   $('.alert-danger', form);
    var success     =   $('.alert-success', form);

    form.validate( {

        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        rules: {
            cep: {required: true, minlength: 9 },
            estado: {required: true, },
            cidade: {required: true, },
            bairro: {required: true, },
            logradouro: {required: true, },
            numero: {required: true, },
            tipo: {required: true, }
        },
        messages: {
            cep: {required: "Informe o CEP.", minlength: "Digite o CEP completo.", },
            estado: {required: "Informe o estado.", },
            cidade: {required: "Informe a cidade.", },
            bairro: {required: "Informe o bairro.", },
            logradouro: {required: "Informe o logradouro. Exemplo: Rua, Avenida, Viela, Praça, etc.", },
            numero: {required: "Informe o numero.", },
            tipo: {required: "Informe qual o tipo do endereço.", },
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                error.insertAfter("#form_gender_error");
            } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                error.insertAfter("#form_payment_error");
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success.hide();
            error.show();
            App.scrollTo(error, -200);
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                label
                    .closest('.form-group').removeClass('has-error').addClass('has-success');
                label.remove(); // remove error label here
            } else { // display success icon for other inputs
                label
                    .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            }
        },

        submitHandler: function (form) {
            success.show();
            error.hide();

            $.ajax({

                url : BaseUrl + 'perfil/FrontOffice/Endereco/AjaxProcessar',
                type : 'POST',
                data: { DataSerialized: $("#form-endereco").serialize() },
                async: false,
                dataType:'json',
                success : function(data) {            

                    if(data != false){

                        $("#aviso-nenhum-endereco").remove();
                        $("#table-scrollable-endereco").slideDown();

                        LineEndereco	=	$("#form-endereco input[name='endereco']").val();
                        LineTipo 		=	$("#form-endereco input[name='tipo']").val();
                        LineId 			=	data;

                        $("#form-endereco .alert").hide();                        
                        $("#form-endereco input").val('');                        
                        $('#form-endereco select').prop('selectedIndex',0);
						$("#form-endereco").slideUp();

						/*
                            Inserindo Dinamicamente o endereco cadastrado.
                            E realizado uma verificacao pra checar se sera o primeiro endereco ou nao. 
                        */ 
                        if($("#table-endereco tbody tr").length > 0){
                            $("#table-endereco tbody tr:first").after(data.TableRow);
                        }else{
                            $("#table-endereco tbody").prepend(data.TableRow);
                        }


                        // Hack para reinicializar popover. Popover nao funciona para elementos carregados dinamicamente via ajax.
                        $(".popovers").popover();  

					    $("body, html").animate({ 
					      scrollTop: $( "#perfil-endereco" ).offset().top 
					    }, 600);

                        swal("Endereço cadastrado!", "Ação realizada com sucesso.", "success");

                    }else{
                        sweetAlert("Oops... Aconteceu algo de errado!", " Houve algum erro ao processar sua solicitação de cadastrar endereço. Tente novamente. ", "error");
                        $("#form-endereco input").val('');                        
                        $('#form-endereco select').prop('selectedIndex',0);
                        return;                                    
                    }

                },
                error : function(request,error)
                {
                    alert("Request: "+JSON.stringify(request));
                }
            });

        }
        // end submitHandler

    }); // end form.validate

    
});