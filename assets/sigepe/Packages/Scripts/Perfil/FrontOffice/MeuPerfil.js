/**
 * SIGEPE
 * 
 * @package 	Vanguarda
 * @author      Gustavo Botega
 * @copyright   Copyright (c) 2017, Gustavo Botega
 * @link        http://gustavobotega.com
 * @link        http://agenciavanguarda.com.br
 * 
 */

$(document).ready(function(){


	jQuery("#meu-perfil .portlet-title .actions .btn-edit").click(function(){

		$(this).hide();

		jQuery("#meu-perfil .form-actions").show();
		jQuery("#meu-perfil form").addClass('form-edit');

	    $("#meu-perfil .fg-edit").each(function(){
	    	$(this).find('.form-control-static').hide();
	    	$(this).find('input, select').show();
	    });


	    $("body, html").animate({ 
	      scrollTop: $( "#meu-perfil-divisao" ).offset().top 
	    }, 600);


	});


	$("#commentForm").validate();


	// validate signup form on keyup and submit
	$("#form-meu-perfil").validate({
		rules: {
			'nome-completo': "required",
			'data-nascimento': "required",
			apelido: "required",
			'estado-civil': "required",
			'genero': "required",
			nacionalidade: "required",
			naturalidade: "required",
			'tipo-sanguineo': "required",
			escolaridade: "required",
		},
		messages: {
			'nome-completo': "Informe seu nome completo",
			'data-nascimento': "Informe sua data de nascimento",
			apelido: "Como quer ser chamado pelo sistema?",
			'genero': "Informe seu gênero",
			'estado-civil': "Qual seu estado civil?",
			nacionalidade: "Informe sua nacionalidade",
			naturalidade: "Qual cidade você nasceu?",
			'tipo-sanguineo': "Qual seu tipo sanguíneo?",
			escolaridade: "Informe sua escolaridade",
		},
		submitHandler: function() {

		    $.ajax({

		        url : BaseUrl + 'perfil/FrontOffice/MeuPerfil/AjaxProcessar/',
		        type : 'POST',
		        data: { DataSerialized: $("#form-meu-perfil").serialize() },
		        async: false,
		        dataType:'json',
		        success : function(data) {            

		            console.log(data);

		            StringErros     =   ''; 

		            if(data == true){

		            	// Setar value label, Fechar formulario e Aparecer botao.

		                sweetAlert(
		                        "Meu perfil atualizado!",
		                        "Mantenha seu perfil sempre atualizado. Verifique também e-mail, telefone e endereço se estão atualizados.",
		                        "success");                           


		                // Resetando formulario


		                $("#fcs-data-nascimento").html( $('input[name="data-nascimento"]').val() );
		                $("#fcs-nome-completo").html( $('input[name="nome-completo"]').val() );
		                $("#fcs-genero").html( $('select[name="genero"] option:selected').text() );

		                $("#fcs-apelido").html( $('input[name="apelido"]').val() );
		                $("#fcs-estado-civil").html( $('select[name="estado-civil"] option:selected').text() );
		                $("#fcs-nacionalidade").html( $('select[name="nacionalidade"] option:selected').text() );
		                $("#fcs-naturalidade").html( $('input[name="naturalidade"]').val() );
		                $("#fcs-tipo-sanguineo").html( $('select[name="tipo-sanguineo"] option:selected').text() );
		                $("#fcs-escolaridade").html( $('select[name="escolaridade"] option:selected').text() );
		                $("#fcs-nome-pai").html( $('input[name="nome-pai"]').val() );
		                $("#fcs-nome-mae").html( $('input[name="nome-mae"]').val() );

		                jQuery("#meu-perfil .portlet-title .actions .btn-edit").show();

						jQuery("#meu-perfil .form-actions").hide();

					    $("body, html").animate({ 
					      scrollTop: $( ".page-header" ).offset().top 
					    }, 600);

					    $("#meu-perfil .fg-edit").each(function(){
					    	$(this).find('.form-control-static').show();
					    	$(this).find('input, select').hide();
					    });



		            }else{

		                sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");

		            }


		        },
		        error : function(request,error)
		        {
		            alert("Request: "+JSON.stringify(request));
		            sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");
		        }
		    });

		}

	});




});