

/**
 * SIGEPE
 * 
 * @package 	Vanguarda
 * @author      Gustavo Botega
 * @copyright   Copyright (c) 2017, Gustavo Botega
 * @link        http://gustavobotega.com
 * @link        http://agenciavanguarda.com.br
 * 
 */

$(document).ready(function(){

  
    var FormDadosAtleta =   $("#form-dados-atleta");
    
    jQuery("#form-dados-atleta .btn-submit").click(function(){
        $(FormDadosAtleta).submit();
    });

	jQuery("#form-dados-atleta .btn-editar-informacoes").click(function(){

		$(this).hide();

		jQuery("#form-dados-atleta .form-actions").show();
		jQuery(FormDadosAtleta).addClass('form-edit');

    	$("#form-dados-atleta").find('.form-control-static').hide();
    	$("#form-dados-atleta").find('input, select').show();

	    $("body, html").animate({ 
	      scrollTop: $( FormDadosAtleta ).offset().top - 40 
	    }, 600);

        
	});

    // validate signup form on keyup and submit
    $("#form-dados-atleta").validate({
        rules: {'nome-competicao': "required", },
        messages: {'nome-competicao': "Preencha esse campo", },
        submitHandler: function() {

            $.ajax({

                url : BaseUrl + 'perfil/FrontOffice/Atleta/AjaxDadosAtletaProcessar/',
                type : 'POST',
                data: { DataSerialized: $("#form-dados-atleta").serialize() },
                async: false,
                dataType:'json',
                success : function(data) {            

                    console.log(data);

                    StringErros     =   ''; 

                    if(data == true){

                        // Setar value label, Fechar formulario e Aparecer botao.

                        sweetAlert(
                                "Meu perfil atualizado!",
                                "Mantenha seu perfil sempre atualizado. Verifique também e-mail, telefone e endereço se estão atualizados.",
                                "success");                           


                        // Resetando formulario
                        $("#fcs-nome-competicao").html( $('input[name="nome-competicao"]').val() );
                        $("#form-dados-atleta").find('.form-control-static').show();
                        $("#form-dados-atleta").find('input, select').hide();
                        $("#form-dados-atleta .btn-editar-informacoes").show();
                        jQuery("#form-dados-atleta .form-actions").hide();

                        $("body, html").animate({ 
                            scrollTop: $( FormDadosAtleta ).offset().top - 40 
                        }, 600);

                    }else{

                        sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");

                    }


                },
                error : function(request,error)
                {
                    alert("Request: "+JSON.stringify(request));
                    sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");
                }
            });

        }

    });


});
// end jquery 
