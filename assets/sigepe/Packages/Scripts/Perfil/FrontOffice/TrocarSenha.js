/**
 * SIGEPE
 * 
 * @package 	Vanguarda
 * @author      Gustavo Botega
 * @copyright   Copyright (c) 2017, Gustavo Botega
 * @link        http://gustavobotega.com
 * @link        http://agenciavanguarda.com.br
 * 
 */

$(document).ready(function(){


    $("#perfil-trocar-senha .form-actions .btn-submit").click(function(){
        $("#form-trocar-senha").submit();
    });



    /*
        FORM VALIDATION
    ===========================================================*/
    var form        =   $('#form-trocar-senha');
    var error       =   $('.alert-danger', form);
    var success     =   $('.alert-success', form);

    form.validate( {

        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        rules: {
            'senha-atual': {required: true, },
            'nova-senha': {required: true, },
            'confirmar-senha': {required: true, equalTo: "#nova-senha" },
        },
        messages: {
            'senha-atual': {
                required: "Esse campo é obrigatório.",
            },
            'nova-senha': {
                required: "Digite a nova senha.", 
            },
            'confirmar-senha': {
                required: "Preencha esse campo. Digite novamente a nova senha.",
                equalTo: "Senhas não conferem."
            },
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                error.insertAfter("#form_gender_error");
            } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                error.insertAfter("#form_payment_error");
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success.hide();
            error.show();
            App.scrollTo(error, -200);
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                label
                    .closest('.form-group').removeClass('has-error').addClass('has-success');
                label.remove(); // remove error label here
            } else { // display success icon for other inputs
                label
                    .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            }
        },

        submitHandler: function (form) {
            success.show();
            error.hide();

            $.ajax({

                url : BaseUrl + 'perfil/FrontOffice/TrocarSenha/AjaxProcessar',
                type : 'POST',
                data: { DataSerialized: $("#form-trocar-senha").serialize() },
                async: false,
                dataType:'json',
                success : function(data) {            

                    if(data != false){
					    $("body, html").animate({ 
					      scrollTop: $( "#perfil-trocar-senha" ).offset().top 
					    }, 600);

                        swal("Senha alterada com sucesso!", "Em instantes você será redirecionado para autenticação.", "success");
						window.location.href = BaseUrl + "logout";

                    }else{

						swal({ html:true, title:'<i>TITLE</i>', text:'<b>TEXT</b>'});

                        sweetAlert("Oops... Aconteceu algo de errado!", " Houve algum erro ao processar sua solicitação. <hr> <b>Possíveis motivos:</b> <br><br> - A nova senha tem que ser diferente da senha atual. <br> - A nova senha deve conter pelo menos 6 e no máximo 12 caracteres. <br> A senha atual informada não está correta. . ", "error");
                        $("#form-trocar-senha input").val('');                        
                        return;                                    
                    }

                },
                error : function(request,error)
                {
                    alert("Request: "+JSON.stringify(request));
                }
            });

        }
        // end submitHandler

    }); // end form.validate

    


});