

/**
 * SIGEPE
 * 
 * @package 	Vanguarda
 * @author      Gustavo Botega
 * @copyright   Copyright (c) 2017, Gustavo Botega
 * @link        http://gustavobotega.com
 * @link        http://agenciavanguarda.com.br
 * 
 */

$(document).ready(function(){





  function handleSuccess (data) {

      PagarMePreco      =   $("#pagarme-preco").val();

      $("#perfil-atleta").slideUp();
      $("#layer-loading-registro").fadeIn();


      $.ajax({
        type: "POST",
        url: BaseUrl + "financeiro/FrontOffice/PagarmeVanguarda/CreateTransaction",
        data: { DatasetPagarme: data, PagarmeAmount: PagarMePreco, FormSerialized: $( "#form-registro-atleta" ).serialize() },
        success: function( data ){
            $("#layer-loading-registro").fadeOut();

            TipoPagamentoSelecionado    =   $('input[type=radio][name="tipo-pagamento"]:checked').val();

            if(TipoPagamentoSelecionado == '1'){
                sweetAlert(
                        "Solicitação de Registro gerada!",
                        "Em alguns segundos você será direcionado pro boleto. A operação somente se dá como concluída após a compesação do boleto. Você será redirecionado para a fatura de pagamento em 5 segundos.",
                        "success");                           
            }

            if(TipoPagamentoSelecionado == '2'){
                sweetAlert(
                        "Registro ativo!",
                        "Seu registro foi realizado com sucesso. Em 5 segundos você será redirecionado para a fatura de pagamento.",
                        "success");                           
            }

            setTimeout(function(){
                window.location.href    =   BaseUrl + 'financeiro/FrontOffice/Transacao/Fatura/1518967007';
//                window.location.href    =   BaseUrl + 'financeiro/FrontOffice/Transacao/Fatura/' = data.Fatura;
            }, 5000);


        },
        error: function (jqXHR, exception) {
            console.log(jqXHR);
            getErrorMessage(jqXHR, exception);
            sweetAlert(
                    "Oops.. aconteceu algum problema!",
                    "Seu registro NÃO foi processado. Entre em contato com a secretaria da federação para mais detalhes.",
                    "error");                           
        },
       complete: function (data) {
          $("#layer-loading-registro").fadeOut();
        }

      });

        setTimeout(function(){
          $("#layer-loading-registro").fadeOut();
            sweetAlert(
                    "Erro!",
                    "Timeout solicitação de registro. Seu registro NÃO foi processado. Entre em contato com a secretaria da federação para mais detalhes.",
                    "error");                           

        }, 75 *1000);



  }



    // This function is used to get error message for all ajax calls
    function getErrorMessage(jqXHR, exception) {
        var msg = '';
        if (jqXHR.status === 0) {
            msg = 'Not connect.\n Verify Network.';
        } else if (jqXHR.status == 404) {
            msg = 'Requested page not found. [404]';
        } else if (jqXHR.status == 500) {
            msg = 'Internal Server Error [500].';
        } else if (exception === 'parsererror') {
            msg = 'Requested JSON parse failed.';
        } else if (exception === 'timeout') {
            msg = 'Time out error.';
        } else if (exception === 'abort') {
            msg = 'Ajax request aborted.';
        } else {
            msg = 'Uncaught Error.\n' + jqXHR.responseText;
        }
        alert(msg);
    }


  function handleError (data) {
    console.log(data);
  }



  function CheckoutCartaoCredito (data) {

      PagarMePreco      =   $("#pagarme-preco").val();

      var checkout = new PagarMeCheckout.Checkout({
        encryption_key: 'ek_test_2E7rIVgIqzo4dgpFEvCMuoDLy0GlE2',
        success: handleSuccess,
        error: handleError,
        close: function() {
            console.log('The modal has been closed.');
            $('input[name="tipo-pagamento"]').prop('checked', false);
        }
      });

      checkout.open({
        amount: PagarMePreco,
        maxInstallments: 6,
        createToken: 'true',
        paymentMethods: 'credit_card',
        postback_url: BaseUrl + 'financeiro/FrontOffice/PagarmePostbackUrl/Processar',
        customerData: false,
        customer: {
          external_id: '#' + PagarmeExternalId,
          name: PagarmeNome,
          type: 'individual',
          country: 'br',
          email: PagarmeEmail,
          documents: [
            {
              type: 'cpf',
              number: PagarmeCpf,
            },
          ],
          phone_numbers: ['+55' + PagarmeTelefonePrincipal],
          birthday: PagarmeDataNascimento,
        },
        billing: {
          name: PagarmeNome,
          address: {
            country: 'br',
            state: PagarmeEstadoSigla,
            city: PagarmeCidade,
            neighborhood: PagarmeBairro,
            street: PagarmeLogradouro,
            street_number: PagarmeNumero,
            zipcode: PagarmeCep
          }
        },
        items: [
          {
            id: '1',
            title: 'Registro de Atleta',
            unit_price: PagarMePreco,
            quantity: 1,
            tangible: false
          }
        ],

        "metadata": {
            'ID Financeiro': "--",
            'ID Financeiro Histórico': "--",
            'ID Registro': "--",
            'ID Registro Histórico': "--",
        },        
      })


    }
    // CheckoutCartaoCredito


















    var FormDadosAtleta =   $("#form-dados-atleta");


    jQuery("#form-dados-atleta .btn-submit").click(function(){
        $(FormDadosAtleta).submit();
    });


	jQuery("#form-dados-atleta .btn-editar-informacoes").click(function(){

		$(this).hide();

		jQuery("#form-dados-atleta .form-actions").show();
		jQuery(FormDadosAtleta).addClass('form-edit');

    	$("#form-dados-atleta").find('.form-control-static').hide();
    	$("#form-dados-atleta").find('input, select').show();

	    $("body, html").animate({ 
	      scrollTop: $( FormDadosAtleta ).offset().top - 40 
	    }, 600);

        
	});

    // validate signup form on keyup and submit
    $("#form-dados-atleta").validate({
        rules: {'nome-competicao': "required", },
        messages: {'nome-competicao': "Preencha esse campo", },
        submitHandler: function() {

            $.ajax({

                url : BaseUrl + 'perfil/FrontOffice/Atleta/AjaxDadosAtletaProcessar/',
                type : 'POST',
                data: { DataSerialized: $("#form-dados-atleta").serialize() },
                async: false,
                dataType:'json',
                success : function(data) {            

                    console.log(data);

                    StringErros     =   ''; 

                    if(data == true){

                        // Setar value label, Fechar formulario e Aparecer botao.

                        sweetAlert(
                                "Meu perfil atualizado!",
                                "Mantenha seu perfil sempre atualizado. Verifique também e-mail, telefone e endereço se estão atualizados.",
                                "success");                           


                        // Resetando formulario
                        $("#fcs-nome-competicao").html( $('input[name="nome-competicao"]').val() );
                        $("#form-dados-atleta").find('.form-control-static').show();
                        $("#form-dados-atleta").find('input, select').hide();
                        $("#form-dados-atleta .btn-editar-informacoes").show();
                        jQuery("#form-dados-atleta .form-actions").hide();

                        $("body, html").animate({ 
                            scrollTop: $( FormDadosAtleta ).offset().top - 40 
                        }, 600);

                    }else{

                        sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");

                    }


                },
                error : function(request,error)
                {
                    alert("Request: "+JSON.stringify(request));
                    sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");
                }
            });

        }

    });




    /* ---------------------------- */





    jQuery("#btn-detalhes-vinculo").click(function(){
        $("a[href='#tab-vinculo-registro']").click();

        $("body, html").animate({scrollTop: $( '#tab-vinculo-registro' ).offset().top - 60 }, 600);

    });





    /*  ---------------------------- */

    jQuery(".btn-novo-registro").click(function(){

        $("#portlet-formulario-registro").slideDown();


        $("body, html").animate({scrollTop: $( '#portlet-formulario-registro' ).offset().top - 25 }, 600);

    });









    $('input[type=radio][name=modalidade]').change(function() {

        $(".tipo-registro-item").hide();
        $(".loading-tipo-registro").slideDown();
        $("input[name='tipo-registro']:radio").removeAttr("checked");

        $(".aviso-dependencia").slideDown();
        $("#portlet-valores").slideUp();
        $("#form-groupo-tipo-pagamento").slideUp();


        ModalidadeId = this.value;

        $.ajax({

            url : BaseUrl + 'perfil/FrontOffice/Atleta/GetTipoRegistro/',
            type : 'POST',
            data: { ModalidadeId: ModalidadeId },
            async: false,
            dataType:'json',
            success : function(data) {            

                if(data.length === 0){
	                sweetAlert("Modalidade", "A modalidade selecionada não possui nenhum tipo de registro associado. Para maiores esclarecimentos entre em contato com a secretaria da federação.", "error");
                   throw new Error('This is not an error. This is just to abort javascript');
                }

                if(data.length > 0){
                    $.each(data, function(index, value) {
                        $("#tipo-registro-item-" + value).show();
                    });
                }


                setTimeout(function(){ 
                    $("body, html").animate({scrollTop: $( '#formulario-registro-tipo-registro' ).offset().top }, 600);
                }, 600);

                $(".loading-tipo-registro").slideUp();
                $("#formulario-registro-tipo-registro .alert-warning").slideUp();
                $("#formulario-registro-tipo-registro .form-group").slideDown();

            },
            error : function(request,error)
            {
                alert("Request: "+JSON.stringify(request));
                sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");
            }
        });



    });




    $('input[type=radio][name="tipo-registro"]').change(function() {

        $(".aviso-dependencia").slideUp();
        $("#portlet-valores").slideDown();
        $("#form-groupo-tipo-pagamento").slideDown();
        $('input[name="tipo-pagamento"]').prop('checked', false);


        TipoRegistroId  = this.value;
        ModalidadeId    = $('input[name="modalidade"]:checked').val();  
        TipoRegistro    = $('input[name="tipo-registro"]:checked').data('title');
        Modalidade      = $('input[name="modalidade"]:checked').data('title');  

        
        $.ajax({

            url : BaseUrl + 'perfil/FrontOffice/Atleta/GetValoresRegistro/',
            type : 'POST',
            data: { ModalidadeId: ModalidadeId, TipoRegistroId: TipoRegistroId },
            async: false,
            dataType:'json',
            success : function(data) {            


                /*
                Resgatando valores
                ---------------------------------*/
                /* Taxas */
                TaxaDataLimitePromocional   =   data.Taxa.DataLimitePromocional;
                TaxaValor                   =   data.Taxa.Valor;
                TaxaValorMascara            =   'R$ ' + TaxaValor.replace(".", ",");
                TaxaValorPromocional        =   data.Taxa.ValorPromocional;
                if(TaxaValorPromocional != null)
                    TaxaValorPromocionalMascara =   'R$ ' + TaxaValorPromocional.replace(".", ",");


                TaxaCbhObrigatorio          =   data.Taxa.CbhObrigatorio;
                TaxaCbhHabilitarVenda       =   data.Taxa.CbhHabilitarVenda;
                TaxaCbh                     =   data.Taxa.CbhTaxa;
                TaxaCbhMascara              =   'R$ ' + TaxaCbh.replace(".", ",");



                /* Desconto */
                DescontoFlag                =   data.Desconto.Flag;
                DescontoPromocionalFlag     =   data.Desconto.DescontoPromocionalFlag;
                DescontoPromocionalValor    =   data.Desconto.DescontoPromocionalValor;

                if (typeof data.Desconto.Desconto != "undefined")
                    Desconto                    =   data.Desconto.Desconto;

                /* Total */
                TotalBruto                  =   data.Total.TotalBruto;
                TotalLiquido                =   data.Total.TotalLiquido;


                /*
                Label e valor da Taxa de Registro 
                ---------------------------------*/
                if(TipoRegistroId == '1')
                    TableRowTaxaItemFirst   =   'Registro ' + Modalidade + ' Anual - FHBr';

                if(TipoRegistroId == '2')
                    TableRowTaxaItemFirst   =   'Registro ' + Modalidade + ' Copa - FHBr';

                if(TipoRegistroId == '3')
                    TableRowTaxaItemFirst   =   'Registro ' + Modalidade + ' Participação Única - FHBr';

                $("#tablerow-taxa-item .first").html(TableRowTaxaItemFirst);
                $("#tablerow-taxa-item .second").html(TaxaValorMascara);


                /*
                Label e valor - Taxa CBH
                ---------------------------------*/
                if(TaxaCbhHabilitarVenda){
                    if(ModalidadeId == '1' && TipoRegistroId == '1'){
                        $("#tablerow-taxa-cbh").show();
                        $("#tablerow-taxa-cbh .second").html(TaxaCbhMascara);
                    }else{
                        $("#tablerow-taxa-cbh").hide();
                    }
                }


                /*
                Bloco Desconto
                ---------------------------------*/
                if(!DescontoFlag)
                    $("#tablerow-desconto, #tablerow-desconto-item").hide();

                if(DescontoFlag){

                    $("#tablerow-desconto, #tablerow-desconto-item").show();

                    // Promocional
                    if(DescontoPromocionalFlag){
                        $("#tablerow-desconto-item .first").html('Desconto até '+ TaxaDataLimitePromocional +' <a href="javascript:;" class="popovers" data-container="body" data-trigger="hover" data-content="---" data-original-title="Detalhes"> <i class="fa fa-info-circle" aria-hidden="true"></i> </a>');
                        $("#tablerow-desconto-item .second").html('R$ ' + DescontoPromocionalValor.replace(".", ","));
                    }                   


                    // Mes a Mes
                    if(!DescontoPromocionalFlag){
                        $("#tablerow-desconto-item .first").html('Desconto especial mês <a href="javascript:;" class="popovers" data-container="body" data-trigger="hover" data-content="---" data-original-title="Detalhes"> <i class="fa fa-info-circle" aria-hidden="true"></i> </a>');
                        $("#tablerow-desconto-item .second").html('R$ ' + Desconto.replace(".", ","));
                    }

                }


                /*
                Bloco Total
                ---------------------------------*/
                if(DescontoFlag){
                    $("#preco-total-promocional").show();
                    $("#preco-total-promocional").html('R$ ' + TotalBruto.replace(".", ","));
                }

                if(!DescontoFlag){
                    $("#preco-total-promocional").hide();
                }

                $("#preco-total").html('R$ ' +  TotalLiquido.replace(".", ","));
                $("#pagarme-preco").val( TotalLiquido.replace(".", "") );

                $("#portlet-valores").slideDown();
                $("#form-groupo-tipo-pagamento").slideDown();


                 $(".popovers").popover();  // Hack para reinicializar popover. Popover nao funciona para elementos carregados dinamicamente via ajax.



                setTimeout(function(){ 
                    $("body, html").animate({scrollTop: $( '#formulario-registro-valores' ).offset().top }, 600);
                }, 600);


            },
            error : function(request,error)
            {
                alert("Request: "+JSON.stringify(request));
                sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");
            }
        });
        



    });



    $('input[type=radio][name="tipo-pagamento"]').change(function() {

        $('.tipo-pagamento-item-active').removeClass('tipo-pagamento-item-active');
        $(this).parent().addClass('tipo-pagamento-item-active');

        $("#tipo-pagamento-boleto-content").slideUp();
        //$("#tipo-pagamento-cartao-credito-content").slideUp();

        TipoPagamentoId     =   this.value;

        // Boleto bancario selecionado
        if(TipoPagamentoId == '1'){ 
            $("#tipo-pagamento-boleto-content").slideDown();
        } 

        // Cartao de credito selecionado
        if(TipoPagamentoId == '2'){ 
            
            CheckoutCartaoCredito();
            //$("#tipo-pagamento-cartao-credito-content").slideDown();

        } 


        setTimeout(function(){ 
            $("body, html").animate({scrollTop: $( '#formulario-registro-pagamento' ).offset().top }, 600);
        }, 600);




    });











	$('.vinculo-detalhe-link').on('click', function(event) {

		event.preventDefault();

		IdVinculo 		=	$(this).data('id');
		NomeEmpresa 	=	$(this).data('nome-empresa');
		IdEmpresa 		=	$(this).data('id-empresa');
		$("#vinculo-detalhe-tabela").slideUp();
		$("#vinculo-detalhe-carregando").slideDown();
		$("#vinculo-detalhe").modal('show');


		$("h4.modal-title").html(NomeEmpresa);

		$("#vinculo-detalhe-id").html(IdVinculo);
		$("#vinculo-detalhe-id-empresa").html(IdEmpresa);

		setTimeout(function(){ 


				// Ajax Success
		        $.ajax({

		            url : BaseUrl + 'cadastro/Consulta/ConsultarHistoricoVinculo/' + IdVinculo,
		            type : 'POST',
		//            data: { DataSerialized: $("#form-registro-atleta").serialize() },
		            async: true,
		            dataType:'json',
		            success : function(data) {            


		                console.log(data);
		                if(typeof(data) == "undefined") { // erro
		                    sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitaÃ§Ã£o.", "error");
		                }else{ // sucesso

							$("#vinculo-detalhe-carregando").slideUp();
							$("#vinculo-detalhe-tabela tbody").html(data);
							$("#vinculo-detalhe-tabela").slideDown();
		                }

		            },
		            error : function(request,error)
		            {
		                sweetAlert("Erro de RequisiÃ§Ã£o!", JSON.stringify(request), "error");
		            }
		        });


		 }, 250);






	});

    
    
    
    
    /*
        TROCA DE FEDERACAO
    -------------------------------------*/
	$('#btn-trocar-federacao').on('click', function(event) {
		event.preventDefault();
        $("body, html").animate({ 
	      scrollTop: $( "#portlet-vinculos-atleta" ).offset().top 
	    }, 600);
        $("#tablerow-trocar-federacao").slideDown();
	});
    
    
	$('#form-trocar-federacao .btn-submit').on('click', function(event) {
		event.preventDefault();
        $("#form-trocar-federacao").submit();
    });
    
    $('#form-trocar-federacao .btn-cancelar').on('click', function(event) {
		event.preventDefault();
        $("#tablerow-trocar-federacao").hide();
        $("body, html").animate({ 
	      scrollTop: $( "#portlet-vinculos-atleta" ).offset().top 
	    }, 600);
        $('#form-trocar-federacao .form-group').removeClass('has-error');
        $('#form-trocar-federacao')[0].reset();
    });
    
    
    

            var FormTrocarFederacao        =   $('#form-trocar-federacao');
            var error       =   $('.alert-danger', form);
            var success     =   $('.alert-success', form);

            FormTrocarFederacao.validate({
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {

                    
                    'federacao': {
                        required: true
                    },
                    'federacao-entidade-equestre': {
                        required: true
                    },

                },
                messages: {
                    'federacao': {
                        required: "Selecione sua nova federação.",
                    },
                    'federacao-entidade-equestre': {
                        required: "Todos os atletas da FHBr devem estar vinculados a alguma entidade equestre.",
                    },
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "federacao") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form-federacao-erro");
                    } else if (element.attr("name") == "federacao-entidade-equestre") { // for uniform checkboxes, insert the after the given container
                        error.insertAfter("#form-federacao-entidade-equestre-erro");
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success.hide();
                    error.show();
                    App.scrollTo(error, -85);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                        label
                            .closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid') // mark the current input as valid and display OK icon
                        .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },

                submitHandler: function (FormTrocarFederacao) {
                    success.show();
                    error.hide();
                    ProcessarTrocaFederacao();
                    return false;
                    //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
                },





            });    
    
    
    

            function ProcessarTrocaFederacao() {

                FederacaoSelecionada    =   $("#federacao option:selected").text();

                $("#tablerow-trocar-federacao").hide();
                $("body, html").animate({ 
                  scrollTop: $( "#portlet-vinculos-atleta" ).offset().top 
                }, 600);
                $('#btn-trocar-federacao').remove();

                $.ajax({

                    url : BaseUrl + 'perfil/FrontOffice/Atleta/AjaxTrocaFederacao/',
                    type : 'POST',
                    data: { DataSerialized: $("#form-trocar-federacao").serialize() },
                    async: true,
                    dataType:'json',
                    success : function(data) {            

                        console.log(data);

                        
                        if(data == true){

                            sweetAlert(
                                    "Federaçao atualizada!",
                                    "Sua solicitaçao foi realizada com sucesso. Aguarde a aprovaçao do vínculo pela federação selecionada.",
                                    "success");
                            
                            setTimeout(function () { 
                              location.reload();
                            }, 1 * 1000);

                        }else{

                            sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");

                        }


                    },
                    error : function(request,error)
                    {
                        alert("Request: "+JSON.stringify(request));
                        sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");
                    }
                });
             
                
                
            }

    
    
    

            /**************************************
            TROCA ENTIDADE 
            ***************************************/
            $('#btn-trocar-entidade').on('click', function(event) {
                event.preventDefault();
                $("body, html").animate({ 
                  scrollTop: $( "#portlet-vinculos-atleta" ).offset().top 
                }, 600);
                $("#tablerow-trocar-entidade").slideDown();
            });


            $('#form-trocar-entidade .btn-submit').on('click', function(event) {
                event.preventDefault();
                $("#form-trocar-entidade").submit();
            });

            $('#form-trocar-entidade .btn-cancelar').on('click', function(event) {
                event.preventDefault();
                $("#tablerow-trocar-entidade").hide();
                $("body, html").animate({ 
                  scrollTop: $( "#portlet-vinculos-atleta" ).offset().top 
                }, 600);
                $('#form-trocar-entidade .form-group').removeClass('has-error');
                $('#form-trocar-entidade')[0].reset();
            });


            var FormTrocarEntidade        =   $('#form-trocar-entidade');
            var error       =   $('.alert-danger', form);
            var success     =   $('.alert-success', form);

            FormTrocarEntidade.validate({
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {

                    'entidade-equestre': {
                        required: true
                    },

                },
                messages: {
                    'entidade-equestre': {
                        required: "Todos os atletas da FHBr devem estar vinculados a alguma entidade equestre.",
                    },
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "entidade-equestre") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form-entidade-equestre-erro");
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success.hide();
                    error.show();
                    App.scrollTo(error, -85);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                        label
                            .closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid') // mark the current input as valid and display OK icon
                        .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },

                submitHandler: function (FormTrocarEntidade) {
                    success.show();
                    error.hide();
                    ProcessarTrocaEntidade();
                    return false;
                    //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
                },





            });    
    
    
    

            function ProcessarTrocaEntidade() {

                EntidadeSelecionada    =   $("#entidade-equestre option:selected").text();

                $("#tablerow-trocar-entidade").hide();
                $("body, html").animate({ 
                  scrollTop: $( "#portlet-vinculos-atleta" ).offset().top 
                }, 600);
                $('#btn-trocar-entidade').remove();

                $.ajax({

                    url : BaseUrl + 'perfil/FrontOffice/Atleta/AjaxTrocaEntidade/',
                    type : 'POST',
                    data: { DataSerialized: $("#form-trocar-entidade").serialize() },
                    async: true,
                    dataType:'json',
                    success : function(data) {            

                        console.log(data);

                        
                        if(data == true){

                            sweetAlert(
                                    "Entidade Equestre atualizada!",
                                    "Sua solicitaçao foi realizada com sucesso. Aguarde a aprovaçao do vínculo pela entidade selecionada.",
                                    "success");
                            
                            setTimeout(function () { 
                              location.reload();
                            }, 1 * 1000);

                        }else{

                            sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");

                        }


                    },
                    error : function(request,error)
                    {
                        alert("Request: "+JSON.stringify(request));
                        sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");
                    }
                });
             
                
                
            }
    
    
    
    
    
    
    
    
    

            var form        =   $('#form-registro-atleta');
            var error       =   $('.alert-danger', form);
            var success     =   $('.alert-success', form);

            form.validate({
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {

                    
                    'modalidade': {
                        required: true
                    },
                    'tipo-registro': {
                        required: true
                    },
                    'tipo-pagamento': {
                        required: true
                    },
                    
                },
                messages: {
                    'modalidade': {
                        required: "Informe uma modalidade.",
                    },
                    'tipo-registro': {
                        required: "Selecione um tipo de registro.",
                    },
                    'tipo-pagamento': {
                        required: "Selecione um tipo de pagamento.",
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "modalidade") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form-modalidade-erro");
                    } else if (element.attr("name") == "tipo-registro") { // for uniform checkboxes, insert the after the given container
                        error.insertAfter("#form-tipo-registro-erro");
                    } else if (element.attr("name") == "tipo-pagamento") { // for uniform checkboxes, insert the after the given container
                        error.insertAfter("#form-tipo-pagamento-erro");
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success.hide();
                    error.show();
                    App.scrollTo(error, -85);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                        label
                            .closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid') // mark the current input as valid and display OK icon
                        .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },

                submitHandler: function (form) {
                    success.show();
                    error.hide();
                    ProcessarPagamentoBoleto();
                    return false;
                    //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
                },





            });    
    
    
            /**
            * Federacao
            *
            * @author Gustavo Botega 
            */
            $('select#federacao').on('change', function() {
                if(this.value == 141){ // 143 e o ID da FHBr
                    $("#form-group-federacao-entidade-equestre").show();
                }else{
                    $("#form-group-federacao-entidade-equestre").hide();
                    $('#form-group-federacao-entidade-equestre select').prop('selectedIndex',0);
                }
            })



    
    
    
    

});
// end jquery 









        function ProcessarPagamentoBoleto() {

            
            var promiseTransacao =   pagarme.client.connect({ api_key: 'ak_live_DOwxAmD3S45SpXF3GV7t5oI9Whu648' })
              .then(client => client.transactions.create({
                amount: 51700,
                "installments": 1,
                payment_method: 'boleto',
                postback_url: 'http://requestb.in/pedtvype',
                customer: {
                  external_id: "#1",
                  type: 'individual',
                  country: 'br',
                  email: "crdpaula@gmail.com",
                  name: 'Carla Rosana de Paula',
                  documents: [
                    {
                      type: 'cpf',
                      number: '04730815122',
                    },
                  ],
                  phone_numbers: [
                    "+5511999998888",
                    "+5511888889999"
                  ],
                  birthday: "1968-11-09"
                },

                billing: {
                  name: "Trinity Moss",
                  address: {
                    country: "br",
                    state: "df",
                    city: "Brasília",
                    neighborhood: "Rio Cotia",
                    street: "Rua Matrix",
                    street_number: "9999",
                    zipcode: "06714360"
                  }
                },



                items: [
                  {
                    id: "b123",
                    title: "Inscrição + Quarto de Sela",
                    unit_price: 51700,
                    quantity: 1,
                    tangible: false,
                    category: 'Registro - Atleta'
                  }
                ],


                "metadata": {
                    'ID Financeiro': "--",
                    'ID Financeiro Histórico': "--",
                    'ID Registro': "--",
                    'ID Registro Histórico': "--",
                },
                "antifraud_metadata": {},


              }));


            promiseTransacao.then(function(Transacao) {

              var PromiseResgate = pagarme.client.connect({ api_key: 'ak_live_DOwxAmD3S45SpXF3GV7t5oI9Whu648' })
                .then(client => client.transactions.find({ id: Transacao.id }));

              PromiseResgate.then(function(Resgate) {
                console.log();
                window.location.href = Resgate.boleto_url;
              });

            });



        }








