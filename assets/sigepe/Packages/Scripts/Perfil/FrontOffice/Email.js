/**
 * SIGEPE
 * 
 * @package 	Vanguarda
 * @author      Gustavo Botega
 * @copyright   Copyright (c) 2017, Gustavo Botega
 * @link        http://gustavobotega.com
 * @link        http://agenciavanguarda.com.br
 * 
 */

$(document).ready(function(){


    /*
        TRIGGER 
        BTN-DELETAR-EMAIL
    ===========================================================*/
    $('body').on('click', '.btn-deletar-email', function (){

    	TableRowId 		=	$(this).parent().parent().data('id');

        swal({
          title: "Você tem certeza?",
          text: "Esse procedimento irá deletar o email associado a sua conta. Esse procedimento é irreversível.",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Sim, deletar email!",
          closeOnConfirm: false
        },
        function(){
            $.ajax({
                url : BaseUrl + 'perfil/FrontOffice/Email/Deletar',
                type : 'POST',
                data: { EmailId: TableRowId },
                async: false,
                dataType:'json',
                success : function(data) {            
                    if(data != false){
                        $("#table-row-" + TableRowId).fadeOut();
                        $("body, html").animate({ 
                          scrollTop: $( "#perfil-email" ).offset().top 
                        }, 600);
                        swal("Email deletado!", "Ação realizada com sucesso.", "success");
                    }else{
                        sweetAlert("Oops... Aconteceu algo de errado!", " Houve algum erro ao processar sua solicitação de deletar email. Tente novamente. ", "error");
                        return;                                    
                    }
                },
                error : function(request,error) { alert("Request: "+JSON.stringify(request)); }
            });
        });

	});





    /*
        TRIGGER 
        BTN-EMAIL-PRINCIPAL
    ===========================================================*/
    $('body').on('click', '.btn-email-principal', function (){

        TableRowId      =   $(this).parent().parent().data('id');

        swal({
          title: "Você tem certeza?",
          text: "Esse procedimento irá transformar esse email como principal.",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-warning",
          confirmButtonText: "Sim!",
          closeOnConfirm: false
        },
        function(){
            $.ajax({
                url : BaseUrl + 'perfil/FrontOffice/Email/EmailPrincipal',
                type : 'POST',
                data: { EmailId: TableRowId },
                async: false,
                dataType:'json',
                success : function(data) {            

                    if(data != false){
                        
                        // Remover todos os Flag P que houver.
                        $("#table-email").find('.flag-email-principal').remove();


                        // HTML dos btns
                        HtmlBtnDeletarEmail      =   "<a href='javascript:;' class='btn btn-circle btn-sm btn-danger btn-deletar-email'> <i class='fa fa-close' aria-hidden='true'></i> Deletar Email </a>";
                        HtmlBtnEmailPrincipal    =   "<a href='javascript:;' class='btn btn-circle btn-sm btn-warning btn-email-principal tooltips' data-original-title='Transformar esse email como principal.'> <i class='fa fa-pencil' aria-hidden='true'></i> Email Principal </a>";


                        // Inserindo Botão de Email Principal e Deletar Email para o antigo email principal
                        $("#table-email tbody tr").each(function(){

                            if( ! $(this).find('.btn-email-principal').length )
                                $(this).find('td:eq(1)').append(HtmlBtnEmailPrincipal);

                            if( ! $(this).find('.btn-deletar-email').length )
                                $(this).find('td:eq(1)').append(HtmlBtnDeletarEmail);

                        });                        


                        // HTML flag Email Principal
                        HtmlTagEmailPrincipal    =   "<a href='javascript:;' class='tooltips badge badge-warning bold flag-email-principal' title='' data-original-title='Email Principal. Preferencialmente vamos utilizar esse número para entrar em contato com você.'> P </a>";


                        // Atualizando na linha
                        $("#table-row-" + TableRowId).find('.btn-email-principal').remove();
                        $("#table-row-" + TableRowId).find('.btn-deletar-email').remove();
                        $("#table-row-" + TableRowId).children("td:first").prepend(HtmlTagEmailPrincipal);


                        // Resetando componentes
                         $(".popovers").popover();  // Hack para reinicializar popover. Popover nao funciona para elementos carregados dinamicamente via ajax.
                         $(".tooltips").tooltip();  // Hack para reinicializar 

                        // Alerta
                        swal("Email alterado como principal!", "Ação realizada com sucesso.", "success");

                    }else{
                        sweetAlert("Oops... Aconteceu algo de errado!", " Houve algum erro ao processar sua solicitação de deletar email. Tente novamente. ", "error");
                        return;                                    
                    }

                },
                error : function(request,error)
                {
                    alert("Request: "+JSON.stringify(request));
                }
            });

        });

    });





    /*
        TRIGGER 
        BTN-CADASTRAR-EMAIL
    ===========================================================*/
    jQuery("#perfil-email .portlet-title .btn-cadastrar-email, #perfil-email #aviso-nenhum-email").click(function(e){
        e.preventDefault();
        $("#form-email").slideDown();
        $("body, html").animate({ 
          scrollTop: $( "#form-email" ).offset().top 
        }, 600);
    });

    $("#perfil-email .form-actions .btn-submit").click(function(){
        $("#form-email").submit();
    });



    /*
        FORM VALIDATION
    ===========================================================*/
    var form        =   $('#form-email');
    var error       =   $('.alert-danger', form);
    var success     =   $('.alert-success', form);

    form.validate( {

        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        rules: {
            email: {
                required: true,
            },
            tipo: {
                required: true,
            }
        },
        messages: {
            email: {
                required: "Informe um email.",
            },
            tipo: {
                required: "Informe qual o tipo do email.",
            },
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                error.insertAfter("#form_gender_error");
            } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                error.insertAfter("#form_payment_error");
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success.hide();
            error.show();
            App.scrollTo(error, -200);
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                label
                    .closest('.form-group').removeClass('has-error').addClass('has-success');
                label.remove(); // remove error label here
            } else { // display success icon for other inputs
                label
                    .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            }
        },

        submitHandler: function (form) {
            success.show();
            error.hide();

            $.ajax({

                url : BaseUrl + 'perfil/FrontOffice/Email/AjaxProcessar',
                type : 'POST',
                data: { DataSerialized: $("#form-email").serialize() },
                async: false,
                dataType:'json',
                success : function(data) {            

                    if(data != false){

                        $("#aviso-nenhum-email").remove();
                        $("#table-scrollable-email").slideDown();

                        LineEmail 	 	=	$("#form-email input[name='email']").val();
                        LineTipo 		=	$("#form-email input[name='tipo']").val();
                        LineId 			=	data;

                        $("#form-email .alert").hide();                        
                        $("#form-email input").val('');                        
                        $('#form-email select').prop('selectedIndex',0);
						$("#form-email").slideUp();

						/*
                            Inserindo Dinamicamente o email cadastrado.
                            E realizado uma verificacao pra checar se sera o primeiro email ou nao. 
                        */ 
                        if($("#table-email tbody tr").length > 0){
                            $("#table-email tbody tr:first").after(data.TableRow);
                        }else{
                            $("#table-email tbody").prepend(data.TableRow);
                        }


                        // Hack para reinicializar popover. Popover nao funciona para elementos carregados dinamicamente via ajax.
                        $(".popovers").popover();  

					    $("body, html").animate({ 
					      scrollTop: $( "#perfil-email" ).offset().top 
					    }, 600);

                        swal("Email cadastrado!", "Ação realizada com sucesso.", "success");

                    }else{
                        sweetAlert("Oops... Aconteceu algo de errado!", " Houve algum erro ao processar sua solicitação de cadastrar email. Tente novamente. ", "error");
                        $("#form-email input").val('');                        
                        $('#form-email select').prop('selectedIndex',0);
                        return;                                    
                    }

                },
                error : function(request,error)
                {
                    alert("Request: "+JSON.stringify(request));
                }
            });

        }
        // end submitHandler

    }); // end form.validate

    
});