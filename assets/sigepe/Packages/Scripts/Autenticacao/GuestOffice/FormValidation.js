

var FormValidation = function () {

    // basic validation
    var handleValidation1 = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation










            var form        =   $('#form-authentication');
            var error       =   $('.alert-danger', form);
            var success     =   $('.alert-success', form);

            form.validate( {

                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    cpf: {
                        required: true,
                        cpfBR: true
                    },
                    usuario: {
                        required: true,
                        cpfBR: true
                    },
                    senha: {
                        minlength: 6,
                        required: true
                    }
                },
                messages: {
                    cpf: {
                        required: "Insira seu CPF",
                        cpfBR: 'Informe um CPF válido'
                    },
                    usuario: {
                        required: "Preencha seu usuário",
                        cpfBR: "Usuário inválido"
                    },
                    senha: {
                        required: 'Digite sua senha',
                        minlength: 'Sua senha deve ter pelo menos 6 dígitos'
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_gender_error");
                    } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                        error.insertAfter("#form_payment_error");
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                        label
                            .closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid') // mark the current input as valid and display OK icon
                        .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },

                submitHandler: function (form) {
                    success.show();
                    error.hide();
                   // form[0].submit();
                    //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax


                    if( !CpfCnpjExiste( $("#form-authentication #cpf").val() ) == true ){
                        sweetAlert("Oops... CPF/Usuário não existe", "O CPF/Usuário informado não consta em nossa base de dados ! Ligue para FHBr: (61) 3245-5870 para mais detalhes", "error");
                        $("#form-authentication input").val('');                        
                        $('#form-authentication input').blur()

                    }else{
                        
                        $.ajax({

                            url : BaseUrl + 'autenticacao/autenticacao/Processar/',
                            type : 'POST',
                            data: { DataSerialized: $("#form-authentication").serialize() },
                            async: false,
                            dataType:'json',
                            success : function(data) {            

                                console.log(data);

                                if(data.Status){
                                    sweetAlert("Usuário autenticado!", "Você será redirecionado para sua conta.", "success");


                                    var Url = window.location.href;
                                    var Inscricao = Url.includes("inscricao");

                                    if(Inscricao){
                                        var parts = Url.split("/");
                                        EventoId    =   parts[parts.length-1]
                                        setTimeout(function () {
                                            window.location.href = BaseUrl + "evento/FrontOffice/FaturaGlobal/Dashboard/" + EventoId; 
                                        }, 500); 
                                    }else{
                                        setTimeout(function () {
                                            window.location.href = BaseUrl + "dashboard"; 
                                        }, 500); 
                                    }



                                }else{
                                    sweetAlert("Oops... Dados inválidos!", " CPF/Senha não conferem em nosso sistema. Tente novamente. Para dúvidas ligue para FHBr: (61) 3245-5870 e solicite suporte para mais informações.", "error");
                                    $("#form-authentication input").val('');                        
                                    $('#form-authentication input').blur()
                                    return;                                    
                                }
                                

                                if(data.fk_sta_id=='2'){
                                    sweetAlert("Usuário bloqueado!", "Ligue para FHBr: (61) 3245-5870 e solicite suporte para mais informações.", "error");
                                    return;
                                }


                                if(data.FlagUsuario != '1'){
                                    sweetAlert("Usuário sem permissão!", " Ligue para FHBr: (61) 3245-5870 e solicite suporte para mais informações.", "warning");
                                    return;
                                }


                            },
                            error : function(request,error)
                            {
                                alert("Request: "+JSON.stringify(request));
                            }
                        });



                    }

                }
                // end submitHandler

            }); // end form.validate


    }
    // end handleValidation1


    return {
        //main function to initiate the module
        init: function () {

            handleValidation1();

        }

    };

}();



jQuery(document).ready(function() {
    FormValidation.init();
});