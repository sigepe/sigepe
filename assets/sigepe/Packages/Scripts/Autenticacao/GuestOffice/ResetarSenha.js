

var FormValidationResetarSenha = function () {

    // basic validation
    var handleValidation1 = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation




            var form        =   $('#form-resetar-senha');
            var error       =   $('.alert-danger', form);
            var success     =   $('.alert-success', form);

            form.validate( {

                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    'cpf-resetar-senha': {
                        required: true,
                        cpfBR: true
                    }
                },
                messages: {
                    'cpf-resetar-senha': {
                        required: "Insira seu CPF2",
                        cpfBR: 'Informe um CPF válido'
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_gender_error");
                    } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                        error.insertAfter("#form_payment_error");
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                        label
                            .closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid') // mark the current input as valid and display OK icon
                        .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },

                submitHandler: function (form) {
                    success.show();
                    error.hide();
                   // form[0].submit();
                    //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax


                    $.ajax({

                        url : BaseUrl + 'autenticacao/ResetarSenha/Processar/',
                        type : 'POST',
                        data: { CpfCnpj: $("#cpf-resetar-senha").val() },
                        async: false,
                        dataType:'json',
                        success : function(data) {            

                        	alert("retorno");
                        	console.log(data);
                            if(data.Status == 'true' || data.Status == true){
 
                                sweetAlert("Nova senha gerada!", "E-mail enviado para: " + data.Email, "success");
                                $("#form-resetar-senha input").val('');                        
                                $('#form-resetar-senha input').blur();
								$("#back-btn").trigger('click');

                            }else{
                                sweetAlert("Oops... Algum problema!",  data.Mensagem +  " <br> Para dúvidas ligue para FHBr: (61) 3245-5870 e solicite suporte para mais informações.", "error");
                                $("#form-resetar-senha input").val('');                        
                                $('#form-resetar-senha input').blur()
                                return;                                    
                            }
                            

                        },
                        error : function(request,error)
                        {
                            alert("Request: "+JSON.stringify(request));
                        }
                    });


                }
                // end submitHandler

            }); // end form.validate


    }
    // end handleValidation1


    return {
        //main function to initiate the module
        init: function () {

            handleValidation1();

        }

    };

}();



jQuery(document).ready(function() {
    FormValidationResetarSenha.init();
});