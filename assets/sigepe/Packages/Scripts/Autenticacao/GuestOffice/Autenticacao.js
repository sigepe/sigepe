/**
 * SIGEPE
 * 
 * @package 	Vanguarda
 * @author      Gustavo Botega
 * @copyright   Copyright (c) 2015, Gustavo Botega
 * @link        http://gustavobotega.com
 * @link        http://agenciavanguarda.com.br
 * 
 */
var Authentication  = function () {


	$("#cpf-isento").click(function(){
		$("#cpf").attr('placeholder', 'Usuário');
		$("#cpf").attr('name', 'usuario');
		$("#cpf").css('border', '2px solid #467c9e');
		$("#cpf").val('');
		$("#cpf").removeClass('mask-cpf');
		$("#cpf").unmask();
	});


}();

