var FormValidation = function () {

    // basic validation
    var handleValidation1 = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1 = $('#form-evento');
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    'modalidade': {required: true },
                    'nome-evento': {required: true },
                    'quantidade-prova': {required: true },
                    'tipo-evento[]': {required: true },
                    'data-inicio': {required: true },
                    'data-fim': {required: true },
                    'data-limite-sem-acrescimo': {required: true },
                    'desenhador-percurso[]': {required: true },
                    'tipo-venda': {required: true },
                    'baia': {required: true },
                    'quarto-de-sela': {required: true },
                    'ativar-site': {required: true },
                    'local': {required: true },
                    'abertura-inscricoes': {required: true },
                    'encerramento-inscricoes': {required: true },
                    'programa': {required: true },
                    'logotipo': {required: true },

                },
                messages: {
                    'modalidade': {required: "Selecione uma modalidade"},
                    'nome-evento': {required: "Entre com o nome do evento"},
                    'quantidade-prova': {required: "Entre com a quantidade de provas"},
                    'tipo-evento[]': {required: "Informe o tipo do evento"},
                    'data-inicio': {required: "Informe a data de início do evento"},
                    'data-fim': {required: "Informe a data fim do evento"},
                    'data-fim': {required: "Informe a data fim do evento"},
                    'data-limite-sem-acrescimo': {required: "Informe a Data Limite sem Acréscimo"},
                    'tipo-venda': {required: "Informe o tipo de inscrição do evento."},
                    'desenhador-percurso[]': {required: "Informe o(s) desenhador(es) de percurso do evento"},
                    'baia': {required: "Selecione uma opção."},
                    'quarto-de-sela': {required: "Selecione uma opção."},
                    'ativar-site': {required: "Selecione uma opção."},
                    'local': {required: "Selecione o local onde o evento vai acontecer."},
                    'abertura-inscricoes': {required: "Informe a data e hora que as inscrições devem ser abertas."},
                    'encerramento-inscricoes': {required: "Informe a data e hora que as inscrições devem ser encerradas."},
                    'programa': {required: "Informe a data e hora que as inscrições devem ser encerradas."},
                    'logotipo': {required: "Informe a data e hora que as inscrições devem ser encerradas."},
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var cont = $(element).parent('.input-group');


                    if (cont.size() > 0) {
                        cont.after(error);
                    } else {
                        element.after(error);
                    }

                    if (element.attr("type") == "radio") {
                        error.insertAfter(element);
                    } else {
                        error.insertAfter(element);
                    }

                    // PROGRAMA 
                    if (element.attr("name") == "programa")
                        error.insertAfter("#programa-error-camada");

                    // LOGOTIPO 
                    if (element.attr("name") == "logotipo")
                        error.insertAfter("#logotipo-error-camada");



                },

                highlight: function (element) { // hightlight error inputs

                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    success1.show();
                    error1.hide();
                    form.submit();
                }
            });


    }


    return {
        //main function to initiate the module
        init: function () {
            handleValidation1();
        }

    };

}();

jQuery(document).ready(function() {
    FormValidation.init();
});