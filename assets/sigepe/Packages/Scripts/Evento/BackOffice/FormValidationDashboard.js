/* FormValidation */
var FormValidation = function() {

    // basic validation
    var handleValidation1 = function() {



        $(".btn-salvar").click(function(){ 
            
            //$('#form-evento').css( "background", "red" );
            var form1 = $('#form-evento');
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);


            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "", // validate all fields including form hidden input
                rules: {
                    'eve-nome-evento': { required: true },
                    'eve-controle': { required: true },
                    'eve-data-inicio': { required: true },
                    'eve-data-fim': { required: true },
                    'eve-quantidade-prova': { required: true },
                    'eve-data-limite-sem-acrescimo': { required: true },
                    'eve-local': { required: true },
                    'eve-ativo-no-site': { required: true },
                    'eve-site-inscricao-inicio': { required: true },
                    'eve-site-inscricao-fim': { required: true },
                    'eve-tipo-venda-inscricoes': { required: true },
                    'eve-baia-ativo': { required: true },
                    'eve-quarto-sela': { required: true }
                },
                messages: {
                    'eve-nome-evento': { required: "Informe o nome do evento" },
                    'eve-controle': { required: "Informe o CONTROLE do evento" },
                    'eve-data-inicio': { required: "Informe a Data do Início" },
                    'eve-data-fim': { required: "Informe a Data do Fim" },
                    'eve-quantidade-prova': { required: "Informe a Quantidade de Provas" },
                    'eve-data-limite-sem-acrescimo': { required: "Informe a Data Limite" },
                    'eve-local': { required: "Informe o Local" },
                    'eve-ativo-no-site': { required: "Informe se está ativo no site" },
                    'eve-site-inscricao-inicio': { required: "Informe a Data do Início no Site" },
                    'eve-site-inscricao-fim': { required: "Informe a Data do Fim no Site" },
                    'eve-tipo-venda-inscricoes': { required: "Informe o tipo de venda das inscrições" },
                    'eve-baia-ativo': { required: "Informe se estão ativas" },
                    'eve-quarto-sela': { required: "Informe se estão ativos" }
                },

                invalidHandler: function(event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                errorPlacement: function(error, element) { // render error placement for each input type
                    var cont = $(element).parent('.input-group');


                    if (cont.size() > 0) {
                        cont.after(error);
                    } else {
                        element.after(error);
                    }

                    if (element.attr("type") == "radio") {
                        error.insertAfter(element);
                    } else {
                        error.insertAfter(element);
                    }

                },

                highlight: function(element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function(element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function(label) {
                    label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function(form) {
                    success1.show();
                    error1.hide();
                    formSubmitAjax();
                }
            }); // form1.validate

            function formSubmitAjax() {
                console.log(form1.serializeFormJSON());
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: BaseUrl + 'evento/BackOffice/Dashboard/processar/',
                    data: {
                        dataJson: form1.serializeFormJSON()
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR);
                        console.log(textStatus);
                        console.log(errorThrown);
                        alert("Server down. Try Later");
                    },
                    success: function(data) {

                        console.log("data down");
                        console.log(data);
                        if (data == true) {
                            if ($('input[name=id]').val() == undefined) {
                                sweetAlert("Sucesso!", "Evento Cadastrado.", "success");
                                document.getElementById("form-evento").reset(); // limpa formulário
                            } else {
                                sweetAlert("Sucesso!", "Evento Atualizado.", "success");
                                setTimeout(function() {
                                    //window.history.back();
                                }, 500);
                            }
                        } else {
                            sweetAlert("Erro!", "Solicitação não processada. Solicite suporte técnico.", "error");
                        }
                    }
                });
            } // formSubmitAjax
        }); // click

    } // handleValidation1


    return {
        //main function to initiate the module
        init: function() {
            handleValidation1();
        }
    };

}();
/* FormValidation */


/* FormHide */
var FormHide = function() {

    var hideFormDados = function() {
        $(".btn-editar").click(function(){
            //$(this).closest("td-show").slideUp();
            $(".td-show").hide();
            $(".td-edit").show();
        });

        $(".btn-cancelar").click(function(){        
            //$(this).closest("td-show").slideUp();
            $(".td-show").show();
            $(".td-edit").hide();
        });
    }

    return {
        init: function() {
            hideFormDados();
        }
    };
}();
/* FormHide */


$(document).ready(function() {
    FormHide.init();
    FormValidation.init();

    $(".td-edit").each(function(){
        $(this).hide();
    });

    $(document).on('change', '#file', function() {
        document.getElementById("uploaded_image").innerHTML = "";
        sweetAlert("Imagem Alterada!", "", "success");
        var name = document.getElementById("file").files[0].name;
        var form_data = new FormData();
        var ext = name.split('.').pop().toLowerCase();
        if (jQuery.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
            alert("Invalid Image File");
        }
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("file").files[0]);
        var f = document.getElementById("file").files[0];
        var fsize = f.size || f.fileSize;
        if (fsize > 2000000) {
            alert("Image File Size is very big");
        } else {
            form_data.append("file", document.getElementById('file').files[0]);
            form_data.append("idEvento", document.getElementById('id'));
            $.ajax({
                url: BaseUrl + 'evento/BackOffice/Dashboard/uploadLogo/',
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $('#uploaded_image').html("<label class='text-success'>Image Uploading...</label>");
                },
                success: function(data) {
                    $('#uploaded_image').html(data);
                }
            });
        }
    });

    /*$(".btn-upload").click(function(){ 
        alert("mudou a imagem");
        var name = document.getElementById("file").files[0].name;
        var form_data = new FormData();
        var ext = name.split('.').pop().toLowerCase();
        if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1)  {
            alert("Invalid Image File");
        }
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("file").files[0]);
        var f = document.getElementById("file").files[0];
        var fsize = f.size||f.fileSize;
        if(fsize > 2000000) {
            alert("Image File Size is very big");
        } else {
            form_data.append("file", document.getElementById('file').files[0]);
            $.ajax({
                url: BaseUrl + 'evento/BackOffice/Dashboard/uploadLogo/',
                method:"POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                beforeSend:function(){
                    $('#uploaded_image').html("<label class='text-success'>Image Uploading...</label>");
                },   
                success:function(data) {
                    $('#uploaded_image').html(data);
                }
            });
        }
    });*/

});

