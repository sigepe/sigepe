$(document).ready(function(){


	$("#modalidade").change(function(){

		ModalidadeId  	 =	 $(this).val();
		$("#tipo-evento option").remove();
		if($(this).val() > 0){



			// Ajax Success
	        $.ajax({

		            url : BaseUrl + 'evento/BackOffice/Evento/ObterTipoEventoPorModalidade/' + $(this).val() + '/1',
		            type : 'POST',
		//            data: { DataSerialized: $("#form-registro-atleta").serialize() },
		            async: true,
		            dataType:'json',
		            success : function(data) {            

		            	if(data.length == 0){
			                sweetAlert("Atenção!", "Está Modalidade não possui tipo de evento!", "warning");
		            	}

						$.each(data, function (i, item) {
						    $('#tipo-evento').append($('<option>', { 
						        value: item.evt_id,
						        text: item.evt_sigla + ' - ' + item.evt_tipo
						    }));
						});


		            },
		            error : function(request,error)
		            {
		                sweetAlert("Erro de RequisiÃ§Ã£o!", JSON.stringify(request), "error");
		            }
	        });

		}else{
            sweetAlert("Atenção!", 'Selecione uma modalidade válida!', "warning");
		}

	});


});