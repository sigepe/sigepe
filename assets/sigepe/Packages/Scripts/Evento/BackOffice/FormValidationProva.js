//$(this).children().css({"color": "red", "border": "2px solid red"});


        $(".btn-salvar-prova").click(function(){

            var form1 = $(this).closest("form");
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);


            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "", // validate all fields including form hidden input
                rules: {
                    'serie': { required: true },
                    'nome-prova': { required: true },
                    'numero-prova': { required: true },
                    'tipo-pista': { required: true },
                    'tipo-sorteio': { required: true },
                    'fk-status': { required: true },
                    'caracteristica': { required: true },
                    'valor-prova': { required: true },
                    'valor-prova-promocional': { required: true },
                    'limite-inscricao-prova': { required: true },
                    'limite-inscricao-atleta': { required: true },
                    'prova-dependente': { required: true },
                    'dia': { required: true },
                    'hora': { required: true },
                },
                messages: {
                    'serie': { required: "Selecione qual série essa prova faz parte" },
                    'nome-prova': { required: "Informe o nome da prova" },
                    'numero-prova': { required: "Informe o número da prova" },
                    'tipo-pista': { required: "Informe o tipo da pista" },
                    'tipo-sorteio': { required: "Informe o tipo do sorteio" },
                    'fk-status': { required: "Informe o Status" },
                    'caracteristica': { required: "Informe a característica da prova" },
                    'valor-prova': { required: "Preencha o valor da prova" },
                    'valor-prova-promocional': { required: "Preencha o valor promocional" },
                    'limite-inscricao-prova': { required: "Preencha o campo" },
                    'limite-inscricao-atleta': { required: "Preencha o campo" },
                    'prova-dependente': { required: "Selecione uma opção" },
                    'dia': { required: "Selecione o dia da prova" },
                    'hora': { required: "Informe o horário de início" },
                },

                invalidHandler: function(event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                errorPlacement: function(error, element) { // render error placement for each input type
                    var cont = $(element).parent('.input-group');


                    if (cont.size() > 0) {
                        cont.after(error);
                    } else {
                        element.after(error);
                    }

                    if (element.attr("type") == "radio") {
                        error.insertAfter(element);
                    } else {
                        error.insertAfter(element);
                    }

                },

                highlight: function(element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function(element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function(label) {
                    label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function(form) {
                    success1.show();
                    error1.hide();
                    formSubmitAjax();
                    //  form.submit();
                }
            }); // form1.validate

            function formSubmitAjax() {
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: BaseUrl + 'evento/BackOffice/Prova/processar/',
                    data: {
                        dataJson: form1.serializeFormJSON()
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR);
                        console.log(textStatus);
                        console.log(errorThrown);
                        alert("Server down. Try Later");
                    },
                    success: function(data) {

                        console.log("data down");
                        console.log(data);

                        if (data == true) {
                            if ($('input[name=id]').val() == undefined) {
                                sweetAlert("Sucesso!", "Prova cadastrada.", "success");
                                document.getElementById("form-prova").reset(); // limpa formulário
                                App.scrollTo(error1, -200);
                            } else {
                                sweetAlert("Sucesso!", "Prova atualizada.", "success");
                                setTimeout(function() {
                                    //window.history.back();
                                }, 500);
                            }
                        } else {
                            sweetAlert("Erro!", "Solicitação não processada. Solicite suporte técnico.", "error");
                        }
                    }
                });
            } // formSubmitAjax
        }); // click


var FormHide = function() {

    var hideFormDados = function() {
        $(".btn-editar-prova").click(function(){        
            $(this).parent().parent().hide();
            $(this).parent().parent().next(".form-prova-editar").show();
        });

        $(".btn-cancelar-prova").click(function(){        
            $(this).parent().parent().hide();
            $(this).parent().parent().prev(".form-prova-exibir").show();
        });
    }

    return {
        init: function() {
            hideFormDados();
        }
    };
}();

jQuery(document).ready(function() {
    FormHide.init();

    $(".form-prova-editar").each(function(){
        $(this).hide();
    });


});