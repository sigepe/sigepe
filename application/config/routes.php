<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */





/*
    _____                        _        ____     __    __   _
   / ____|                      | |      / __ \   / _|  / _| (_)
  | |  __   _   _    ___   ___  | |_    | |  | | | |_  | |_   _    ___    ___
  | | |_ | | | | |  / _ \ / __| | __|   | |  | | |  _| |  _| | |  / __|  / _ \
  | |__| | | |_| | |  __/ \__ \ | |_    | |__| | | |   | |   | | | (__  |  __/
   \_____|  \__,_|  \___| |___/  \__|    \____/  |_|   |_|   |_|  \___|  \___|


  #  Environment Guest Office

  ========================================================================= */

/* AUTENTICACAO */
$route['login'] = 'autenticacao/autenticacao/Login';
$route['login/inscricao/(:num)'] = 'autenticacao/autenticacao/Login/$1';
$route['logout'] = 'autenticacao/autenticacao/Logout';

/* CADASTRO */
$route['FrontOffice/Pessoa/CadastroPessoaFisica'] = 'pessoa/GuestOffice/CadastroPessoaFisica/Formulario';
$route['FrontOffice/Pessoa/CadastroAtleta'] = 'pessoa/GuestOffice/CadastroAtleta';




/*

   ______                          _        ____     __    __   _
  |  ____|                        | |      / __ \   / _|  / _| (_)
  | |__     _ __    ___    _ __   | |_    | |  | | | |_  | |_   _    ___    ___
  |  __|   | '__|  / _ \  | '_ \  | __|   | |  | | |  _| |  _| | |  / __|  / _ \
  | |      | |    | (_) | | | | | | |_    | |__| | | |   | |   | | | (__  |  __/
  |_|      |_|     \___/  |_| |_|  \__|    \____/  |_|   |_|   |_|  \___|  \___|


  #  Environment Front Office User

  ========================================================================= */

/* DASHBOARD */
$route['FrontOffice/Dashboard'] = 'dashboard/FrontOffice/Dashboard';
$route['FrontOffice'] = 'dashboard/FrontOffice/Dashboard';
$route['Dashboard'] = 'dashboard/FrontOffice/Dashboard';
$route['dashboard'] = 'dashboard/FrontOffice/Dashboard';


/* EVENTO */
$route['FrontOffice/Evento/InscricoesEmAberto'] = 'evento/FrontOffice/EventosComInscricoesEmAberto';
$route['FrontOffice/Evento/(:num)'] = 'evento/FrontOffice/Evento/Dashboard/$1';
$route['FrontOffice/FaturaSimples/(:num)/(:num)'] = 'evento/FrontOffice/FaturaSimples/Dashboard/$1/$2';
$route['FrontOffice/Evento/Calendario'] = 'evento/FrontOffice/Listar/Calendario';

/* ANIMAL */
$route['FrontOffice/Animal/MeusAnimais'] = 'animal/FrontOffice/Listagem';
$route['FrontOffice/Animal/Cadastro'] = 'animal/FrontOffice/Cadastro';
$route['FrontOffice/Animal/(:num)'] = 'animal/FrontOffice/Dashboard/Dashboard/$1';
$route['FrontOffice/Animal/Dashboard/(:num)'] = 'animal/FrontOffice/Dashboard/Dashboard/$1';
$route['FrontOffice/Animal/PerfilCompleto/(:num)'] = 'animal/FrontOffice/Animal/animal_read_re/$1';

/* MEU PERFIL */
$route['FrontOffice/Perfil'] = 'perfil/FrontOffice/Perfil';
$route['FrontOffice/Perfil/Dashboard'] = 'perfil/FrontOffice/Perfil/Dashboard';
$route['FrontOffice/MeuPerfil'] = 'perfil/FrontOffice/MeuPerfil/Index';
$route['FrontOffice/Perfil/Inicio'] = 'perfil/FrontOffice/Perfil';
$route['FrontOffice/Perfil/MeuPerfil'] = 'perfil/FrontOffice/MeuPerfil/Index';
$route['FrontOffice/ResetarSenha'] = 'perfil/FrontOffice/Perfil/ResetarSenha';
$route['FrontOffice/Perfil/Telefone'] = 'perfil/FrontOffice/Telefone';
$route['FrontOffice/Perfil/Email'] = 'perfil/FrontOffice/Email';
$route['FrontOffice/Perfil/Endereco'] = 'perfil/FrontOffice/Endereco';
$route['FrontOffice/Perfil/TrocarSenha'] = 'perfil/FrontOffice/TrocarSenha';
$route['FrontOffice/Perfil/Duvidas'] = 'perfil/FrontOffice/Duvidas';
$route['FrontOffice/Perfil/Atleta'] = 'perfil/FrontOffice/Atleta';


/* FINANCEIRO */
$route['FrontOffice/Financeiro/Transacao/(:num)'] = 'financeiro/FrontOffice/Transacao/Fatura/$1';





/*

   ____                   _       ____     __    __   _
  |  _ \                 | |     / __ \   / _|  / _| (_)
  | |_) |   __ _    ___  | | __ | |  | | | |_  | |_   _    ___    ___
  |  _ <   / _` |  / __| | |/ / | |  | | |  _| |  _| | |  / __|  / _ \
  | |_) | | (_| | | (__  |   <  | |__| | | |   | |   | | | (__  |  __/
  |____/   \__,_|  \___| |_|\_\  \____/  |_|   |_|   |_|  \___|  \___|

  ========================================================================= */

/* MODULO DASHBOARD */
/* INICIO PACOTE DASHBOARD */
$route['BackOffice/Dashboard'] = 'dashboard/BackOffice/Dashboard/Index';
$route['BackOffice'] = 'dashboard/BackOffice/Dashboard/Index';
/* FIM PACOTE DASHBOARD */



/* MODULO REGISTRO */
/* INICIO PACOTE ATLETA */
$route['BackOffice/Atleta/Todos/Listar'] = 'atleta/BackOffice/Atleta/atleta_browse_bread';
$route['BackOffice/Atleta/Federados/Listar'] = 'atleta/BackOffice/Atleta/atleta_federado_browse';
$route['BackOffice/Atleta/OutrasFederacoes/Listar'] = 'atleta/BackOffice/Atleta/atleta_outra_federacao_browse';
$route['BackOffice/Registro/Atleta'] = 'registro/BackOffice/Atleta';
/* FIM PACOTE ATLETA */
/* INICIO PACOTE ANIMAL */
$route['BackOffice/Registro/Animal'] = 'registro/BackOffice/Animal';
$route['BackOffice/Animal/Todos/Listar'] = 'animal/BackOffice/Animal';
$route['BackOffice/Animal/Fhbr/Listar'] = 'animal/BackOffice/Animal/animal_fhbr_browse';
$route['BackOffice/Animal/OutrasFederacoes/Listar'] = 'animal/BackOffice/Animal/animal_outra_federacao';
/* FIM PACOTE ANIMAL */



/* MODULO EVENTO */
$route['BackOffice/Evento/Calendario'] = 'evento/BackOffice/Evento/calendario';
/* INICIO PACOTE DASHBOARD */
$route['BackOffice/Evento/Todos/Listar'] = 'evento/BackOffice/ConsultaEvento/Todos'; // $1 - Id do Evento     
$route['BackOffice/Evento/Dashboard/Detalhar/(:num)'] = 'evento/BackOffice/Dashboard/dashboard_read_read/$1'; // $1 - Id do Evento     
$route['BackOffice/Evento/Cadastrar'] = 'evento/BackOffice/CadastroEvento/index';
/* INICIO FIM DASHBOARD */

/* INICIO PACOTE INSCRICOES */
$route['BackOffice/Evento/Inscricao/Listar/(:num)'] = 'evento/BackOffice/Inscricao/Dashboard/$1'; // $1 - Id do Evento     
/* INICIO FIM INSCRICOES */

/* INICIO PACOTE SERIE */
/* B */ $route['BackOffice/Evento/Serie/Listar/(:num)'] = 'evento/BackOffice/Serie/serie_browse_bread/$1'; // $1 - Id do Evento     
/* R */
/* E */ $route['BackOffice/Evento/Serie/Editar/(:num)/(:num)'] = 'evento/BackOffice/Serie/serie_edit/$1/$2'; // $1 - Id do Evento   /   $2 - Id da Serie a ser editada     
/* A */ $route['BackOffice/Evento/Serie/Cadastrar/(:num)'] = 'evento/BackOffice/Serie/serie_add/$1'; // $1 - Id do Evento     
/* D */ $route['BackOffice/Evento/Serie/Desativar/(:num)'] = 'evento/BackOffice/Serie/serie_disable/$1'; // $1 - Id do Evento     
/* FIM PACOTE SERIE */

/* INICIO PACOTE PROVA */
/* B */ $route['BackOffice/Evento/Prova/Listar/(:num)'] = 'evento/BackOffice/Prova/prova_browse_bread/$1'; // $1 - Id do Evento     
/* R */
/* E */ $route['BackOffice/Evento/Prova/Editar/(:num)/(:num)'] = 'evento/BackOffice/Prova/prova_edit/$1/$2'; // $1 - Id do Evento   /   $2 - Id da Serie a ser editada     
/* A */ $route['BackOffice/Evento/Prova/Cadastrar/(:num)'] = 'evento/BackOffice/Prova/prova_add/$1'; // $1 - Id do Evento     
/* D */# $route['BackOffice/Evento/Serie/Desativar/(:num)']                = 'evento/BackOffice/Serie/serie_disable/$1'; // $1 - Id do Evento     
/* FIM PACOTE PROVA */

/* INICIO PACOTE BAIA */
/* FIM PACOTE BAIA */

/* INICIO PACOTE QUARTOSELA */
/* FIM PACOTE QUARTOSELA */

/* INICIO PACOTE OFICIAL */
/* FIM PACOTE OFICIAL */

/* INICIO PACOTE PATROCINADOR */
/* FIM PACOTE PATROCINADOR */

/* INICIO PACOTE PATROCINADOR/APOIADOR */
/* FIM PACOTE PATROCINADOR/APOIADOR */

/* INICIO PACOTE DOCUMENTO */
/* BREAD */ $route['BackOffice/Evento/Documento/Listar/(:num)'] = 'evento/BackOffice/Documento/documento_bread/$1'; // $1 - Id do Evento     
/* FIM PACOTE DOCUMENTO */

/* INICIO PACOTE ORDEMENTRADA */
/* FIM PACOTE ORDEMENTRADA */

/* INICIO PACOTE RESULTADO */
/* FIM PACOTE RESULTADO */

/* INICIO PACOTE RANKING */
/* FIM PACOTE RANKING */

/* INICIO PACOTE FINANCEIRO */
/* FIM PACOTE FINANCEIRO */

/* INICIO PACOTE FOTOS */
/* FIM PACOTE FOTOS */

/* INICIO PACOTE CONFIGURACAO */
/* FIM PACOTE CONFIGURACAO */



/* MODULO DESENHADOR */
$route['BackOffice/Desenhador/Todos/Listar'] = 'desenhador/BackOffice/Desenhador/desenhador_browse_bread';

/* MODULO PESSOA */
/* INICIO PACOTE DASHBOARD */
$route['BackOffice/Pessoa/PessoaFisica/Listar'] = 'pessoa/BackOffice/Pessoa/pessoa_browse/';
$route['BackOffice/Pessoa/Dashboard/Detalhar/(:num)'] = 'pessoa/BackOffice/Pessoa/pessoa_dashboard/$1';
$route['BackOffice/Pessoa/CpfDuplicado/Listar'] = 'pessoa/BackOffice/Pessoa/pessoa_cpf_duplicado';
$route['BackOffice/Pessoa/Perfil/Detalhar/(:num)'] = 'pessoa/BackOffice/Pessoa/pessoa_read_ed/$1';
$route['BackOffice/Pessoa/Telefone/Listar/(:num)'] = 'pessoa/BackOffice/Telefone/telefone_browse_bread/$1';
$route['BackOffice/Pessoa/Email/Listar/(:num)'] = 'pessoa/BackOffice/Email/email_browse_bread/$1';
$route['BackOffice/Pessoa/Endereco/Listar/(:num)'] = 'pessoa/BackOffice/Endereco/endereco_browse_bread/$1';
$route['BackOffice/Pessoa/Configuracao/Detalhar/(:num)'] = 'pessoa/BackOffice/Configuracao/configuracao_browse_bread/$1';
/* INICIO FIM DASHBOARD */

/* INICIO PACOTE EMAIL */
/* FIM PACOTE EMAIL */


/* MODULO FAQ */
/* INICIO PACOTE CATEGORIA */
$route['BackOffice/Faq/Categoria/Listar'] = 'faq/BackOffice/Categoria/categoria_browse_bread';
/* FIM PACOTE CATEGORIA */



/*
   _          __  _____   _____   _____   _____   _____    _     _   _   _____   _____  
  | |        / / | ____| |  _  \ /  ___/ | ____| |  _  \  | |   / / | | /  ___| | ____| 
  | |  __   / /  | |__   | |_| | | |___  | |__   | |_| |  | |  / /  | | | |     | |__   
  | | /  | / /   |  __|  |  _  { \___  \ |  __|  |  _  /  | | / /   | | | |     |  __|  
  | |/   |/ /    | |___  | |_| |  ___| | | |___  | | \ \  | |/ /    | | | |___  | |___  
  |___/|___/     |_____| |_____/ /_____/ |_____| |_|  \_\ |___/     |_| \_____| |_____| 

  ========================================================================= */
// arrumar essa rota
//$route['Webservice/Desenhador/listarComOrdem/(:any)/(:any)'] = 'desenhador/GuestOffice/DesenhadorController/listarComOrdem/$1/$2/$3';





/*
   ____    _     _                             _____                    _
   / __ \  | |   | |                           |  __ \                  | |
  | |  | | | |_  | |__     ___   _ __   ___    | |__) |   ___    _   _  | |_    ___   ___
  | |  | | | __| | '_ \   / _ \ | '__| / __|   |  _  /   / _ \  | | | | | __|  / _ \ / __|
  | |__| | | |_  | | | | |  __/ | |    \__ \   | | \ \  | (_) | | |_| | | |_  |  __/ \__ \
   \____/   \__| |_| |_|  \___| |_|    |___/   |_|  \_\  \___/   \__,_|  \__|  \___| |___/


  #  Routes

  ========================================================================= */
$route['default_controller'] = 'autenticacao/autenticacao/Login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
