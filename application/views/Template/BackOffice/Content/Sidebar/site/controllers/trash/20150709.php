<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class cSite extends CI_Controller
{

	/**
	 * Variavel que contem as regras de validacao do formulario 
	 * @var array
	 */
	function __construct()
	{


		parent::__construct();

	}
	

	/*-----------------------------------------------	
	@ MODALIDADE - HISTÓRICO
	-----------------------------------------------*/
	public function modalidade_historico()
	{

		$this->data['current_page']						=	'modalidade_historico';
		$this->data['current_page_titulo']				=	'Histórico';

		$this->loadtemplate( 'modalidade', true, 'modalidade');
	}


	/*-----------------------------------------------	
	@ MODALIDADE - RANKING
	-----------------------------------------------*/
	public function modalidade_ranking()
	{

		$this->data['current_page']						=	'modalidade_ranking';
		$this->data['current_page_titulo']				=	'Ranking';

		$this->loadtemplate( 'modalidade', true, 'modalidade');
	}


	/*-----------------------------------------------	
	@ MODALIDADE - REGULAMENTO
	-----------------------------------------------*/
	public function modalidade_regulamento()
	{
		$this->data['current_page']						=	'modalidade_regulamento';
		$this->data['current_page_titulo']				=	'Regulamento';

		$this->loadtemplate( 'modalidade', true, 'modalidade');
	}



	/*-----------------------------------------------	
	@ MODALIDADE - TAXAS
	-----------------------------------------------*/
	public function modalidade_taxas()
	{
		$this->data['current_page']						=	'modalidade_taxas';
		$this->data['current_page_titulo']				=	'Taxas';

		$this->loadtemplate( 'modalidade', true, 'modalidade');
	}




}
/* END CLASS */

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */



