<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class cSite extends CI_Controller
{

	/**
	 * Variavel que contem as regras de validacao do formulario 
	 * @var array
	 */
	function __construct()
	{


		parent::__construct();


		/**
		 * Setando a timezone do sistema para America / Sao Paulo
		 */
	 	date_default_timezone_set('America/Sao_Paulo');
	

	 	$this->data['base_url']	=	base_url();

		/**
		 * Carregando library que serao utilizadas em toda a classe
		 */
		$this->load->library('my_date');


		/**
		 * Carregando helpers que serao utilizadas em toda a classe
		 */
		$this->load->helper('cookie');



		/**
		 * Chamada do metodo environmentVariables() responsavel por
		 * setar as variaveis que serao utilizadas no template
		 */
		$this->environmentVariables();

	}
	



	/*-----------------------------------------------	
	@ INDEX
	-----------------------------------------------*/
	public function index()
	{
		$this->inicio();
	}



	/*-----------------------------------------------	
	@ INDEX
	-----------------------------------------------*/
	public function estatuto()
	{
		$data = file_get_contents(base_url() . "uploads/estatuto/estatuto.pdf"); // Read the file's contents
		$name = 'estatuto.pdf';

		force_download($name, $data);
	}


	/*-----------------------------------------------	
	@ PROGRAMA ( EVENTO )
	-----------------------------------------------*/
	public function force_download_programa($slugEvento)
	{

		$nomeArquivo	=	$this->m_crud->get_rowSpecific('tb_site_evento', 'eve_slug', $slugEvento, 1, 'eve_programa'); 
		$data 			= file_get_contents(base_url() . "uploads/evento/programa/" . $nomeArquivo); // Read the file's contents
		$name 			= '[programa] - ' . $slugEvento . '.pdf';

		force_download($name, $data);
	}


	/*-----------------------------------------------	
	@ INÍCIO
	-----------------------------------------------*/
	public function inicio()
	{
		$this->data['title_page']	=	'';
		$this->data['current_page']	=	'inicio';
		$this->data['current_nav']	=	'inicio';


		$this->data['ultimas_galerias']	=	$this->m_crud->get_allWhereTwoParametersOrderbyLimit('tb_blog', 'fk_tip_id', 2, 'blo_flagStatus', 1, 'blo_id', 'desc', 2);
		$this->data['proximos_eventos']	=	$this->db->query("SELECT * FROM tb_site_evento 
															WHERE DATE(eve_datafim) > DATE(NOW() + INTERVAL -1 DAY) AND
															fk_sta_id IN (1,2,3,4,5)
															ORDER BY eve_datainicio ASC
															LIMIT 4
															; ")->result();


		$this->data['ultimos_destaques']	=	$this->db->query("SELECT * FROM tb_blog 
															WHERE fk_tip_id in (1,2)
															AND blo_flagDestaque IS NOT NULL
															AND blo_posicao_destaque IS NOT NULL
															AND blo_flagStatus IS NOT NULL
															ORDER BY blo_posicao_destaque ASC
															LIMIT 4
															; ")->result();


		$this->loadtemplate( 'inicio' );
	}


	/*-----------------------------------------------	
	@ A FEDERAÇÃO - HISTORIA
	-----------------------------------------------*/
	public function afederacao($slugPagina)
	{

		$secondSegment 	=	$this->uri->segment(2);
		$thirdSegment 	=	$this->uri->segment(3);

		if($secondSegment=='entidades-filiadas' || $secondSegment=='escolas-de-equitacao'){
			redirect(base_url().'site/cSite/entidadeescola/'. $secondSegment . '/' . $thirdSegment);
		}



		$this->data['title_page']						=	'';
		$this->data['current_nav']						=	'afederacao';

		if($slugPagina==null)
		{
			redirect(base_url() . 'afederacao/' . 'historia');
		}

		if(
			$slugPagina != 'historia' &&
			$slugPagina != 'tribunal-de-justica-desportiva' &&
			$slugPagina != 'prestacao-de-contas' &&
			$slugPagina != 'contratacoes-e-licitacoes'
		)
		{
			redirect(base_url() . 'a-federacao/' . 'historia');
		}
		else
		{

/*
			// ENTIDADES FILIADAS
			if($slugPagina=='noticias')
			{
				redirect(base_url());
			}
*/

			$query 								=	$this->m_crud->get_allWhere('tb_pagina_categoria', 'cat_slug', $slugPagina);
			$idCategoria						=   $query[0]->cat_id;

			$this->data['tituloPagina']			=	$this->m_crud->get_rowSpecific('tb_pagina_categoria', 'cat_slug', $slugPagina, 1, 'cat_titulo');

			$this->data['paginas']				=	$this->db->query('select * from tb_pagina_categoria where cat_id in (1,2,3,4)')->result();
			$this->data['slugPagina'] 			= 	$slugPagina;
			$this->data['current_page']			=	'modalidade_' . $slugPagina;

			$this->data['conteudo']				=	$this->m_crud->get_rowSpecific('tb_pagina', 'fk_cat_id', $idCategoria, 1, 'pag_conteudo');

			$this->loadtemplate( 'federacao', 'true', 'federacao' );

		}
	}



	/*-----------------------------------------------	
	@ A FEDERAÇÃO - (LISTAGEM) ENTIDADES FILIADAS
	-----------------------------------------------*/
	public function entidadesescolas()
	{

		$slugTipoPagina = $this->uri->segment(2);
		$this->data['slugTipoPagina']	=	$slugTipoPagina;
		$this->data['slugItem']			=	NULL;

		if($slugTipoPagina=='entidades-filiadas')
		{
			$flag_selecionado 	=	'ent_flag_entidade';
			$this->data['tituloTipoPagina']	=	'Entidades Filiadas';
			$this->data['current_page']	=	'federacao_entidades';
		}
		else
		{
			$flag_selecionado 	=	'ent_flag_escola';
			$this->data['tituloTipoPagina']	=	'Escolas de Equitação';
			$this->data['current_page']	=	'federacao_escolas';
		}

		$this->data['entidadesescolas']	=	$this->m_crud->get_allWhereTwoParametersOrderby('tb_entidadeescola', $flag_selecionado, 1, 'ent_status', 1,'ent_titulo', 'asc');

		$this->data['title_page']	=	'';

		$this->data['current_nav']	=	'afederacao';

		$this->loadtemplate( 'entidadesescolas', 'true', 'federacao' );
	}



	/*-----------------------------------------------	
	@ Processa contato
	-----------------------------------------------*/
	public function processaContato()
	{
		$post = serialize($_POST);
		$data = array(
		   'con_conteudo' => $post,
		);

		$this->db->insert('tb_site_contato', $data);




		$keySubmit = $this->input->post('keySubmit');
		if(strlen($keySubmit)>0):

			if($this->form_validation->run()==TRUE):


				/* CONSULTA - ESTADOS */
/*				$estadoPost = $this->input->post('cod_estados');
				$estadoQuery = $this->crud->get_allWhereLimit('estados','cod_estados',$estadoPost,1);
				$estadoQueryRow = $estadoQuery->row();
				$estado = $estadoQueryRow->nome;*/

				/* CONSULTA - CIDADES */
/*				$cidadePost = $this->input->post('cod_cidades');
				$cidadeQuery = $this->crud->get_allWhereLimit('cidades','cod_cidades',$cidadePost,1);
				$cidadeQueryRow = $cidadeQuery->row();
				$cidade = $cidadeQueryRow->nome;*/


				$nome = $this->input->post('nome');
				$email = $this->input->post('email');
				$telefone = $this->input->post('telefone');
				$celular = $this->input->post('telefone');
				$departamento = $this->input->post('departamento');
				$departamento_titulo = $this->m_crud->get_rowSpecific('tb_site_contato_departamento', 'com_titulo', $departamento, 1, 'dep_departamento');
				$departamento_email = $this->m_crud->get_rowSpecific('tb_site_contato_departamento', 'com_titulo', $departamento, 1, 'dep_email');
				$filiado = $this->input->post('filiado');
				$mensagemOriginal = $this->input->post('mensagem');
					
				if($filiado==1){
					$filiado == 'Sim';
				}
				else{
					$filiado == 'Não';
				}

				$comonosencontrou_titulo = $this->m_crud->get_rowSpecific('tb_site_contato_comonosencontrou', 'com_id', $comonosencontrou, 1, 'com_titulo');

				$mensagem = 
				"
				Nome: ".$nome." <br />
				E-mail: ".$email." <br />
				Telefone: ".$telefone." <br />
				Celular: ".$celular." <br />
				Como nós Encontrou: ".$comonosencontrou_titulo." <br />
				Assunto: ".$assunto." <br />
				Filiado: ".$filiado." <br />
				Mensagem: ".$mensagemOriginal." <br />
				";

				/*-------------------------------------------
					ENVIANDO E-MAIL 
				--------------------------------------------*/
			    $mail = new PHPMailer();
				$mail->IsSMTP(); 
				$mail->SMTPAuth   	 = true; 
				$mail->SMTPSecure 	 = "ssl";  
				$mail->Host       	 = "smtp.lcgas.com.br";      
				$mail->Port       	 = 465;                   
				$mail->Username   	 = "site@lcgas.com.br";  
				$mail->Password   	 = "des1008d";            
				$mail->SetFrom("gustavo@avantdigital.com.br", utf8_decode("FHBr - Site"));
				$mail->Subject    	 = utf8_decode("[FHBr-WEBSITE]  - " . $nome);
				$mail->Body      	 = $mensagem;
				$mail->AltBody    	 = $mensagem;

				$emailDestino 		  = $this->crud->get_rowSpecific('contato_departamento','id',$departamento, 1, 'email');
				$departamentoDestino  = $this->crud->get_rowSpecific('contato_departamento','id',$departamento, 1, 'departamento');

				$mail->AddAddress($emailDestino, $departamentoDestino);
				$mail->AddAddress('contato@fhbr.com.br', 'Contato');


				if(!$mail->Send()) {
					$data["message"] = "Error en el envío: " . $mail->ErrorInfo;
					redirect("erro",'refresh');
				}
				else {
					$this->session->set_flashdata('sucessook',' <strong>Olá '.$nome.'.</strong> <br /><br />	 Recebemos sua mensagem. <br /> <br />	Entraremos em contato até 72 horas úteis.');
					redirect(base_url()."contato",'refresh');
				}

			endif;

		endif;


		redirect(base_url() . 'contato'); 
	}







	/*-----------------------------------------------	
	@ EnviarEmail
	-----------------------------------------------*/
	public function EnviarEmail()
	{

		// the message
		$msg = "First line of text\nSecond line of text";

		// use wordwrap() if lines are longer than 70 characters
		$msg = wordwrap($msg,70);

		// send email
		mail("gustavobotega@gmail.com","My subject",$msg);


		var_dump("enviar emai2l");

	}



















	/*-----------------------------------------------	
	@ A FEDERAÇÃO - (PARTICULAR) ENTIDADE / ESCOLA
	-----------------------------------------------*/
	public function entidadeescola()
	{
		$slugTipoPagina	=	$this->uri->segment(2);
		$slugItem		=	$this->uri->segment(3);

		$this->data['slugTipoPagina']	=	$slugTipoPagina;
		$this->data['slugItem']			=	$slugItem;


		if($slugTipoPagina=='entidades-filiadas')
		{
			$flag_selecionado 	=	'ent_flag_entidade';
			$this->data['tituloTipoPagina']	=	'Entidades Filiadas';
		}
		else
		{
			$flag_selecionado 	=	'ent_flag_escola';
			$this->data['tituloTipoPagina']	=	'Escolas de Equitação';
		}

		$this->data['entidadesescolas']	=	$this->m_crud->get_allWhereLimitOrderby('tb_entidadeescola', $flag_selecionado, 1, 100, 'ent_titulo', 'asc');


		$idItem 	=	$this->m_crud->get_rowSpecific('tb_entidadeescola', 'ent_slug', $slugItem, 1, 'ent_id');

		$query 								=	$this->m_crud->get_allWhereLimitOrderby('tb_entidadeescola', 'ent_slug', $slugItem, 1, 'ent_id', 'asc');
		$this->data['query']				=	$query[0];
		$this->data['fotos']				=	$this->m_crud->get_allWhereLimitOrderby('tb_entidadeescola_foto', 'fk_ent_id', $idItem, 100, 'fot_id', 'asc');
		/*var_dump($this->data['fotos']);*/


		/* CONFIGURAÇÕES DO MAPA */
		$this->data['global_map_title']			=	$query[0]->ent_titulo;
		$this->data['global_map_content']		=	$query[0]->ent_endereco;
		$this->data['global_map_latitude']		=	$query[0]->ent_eixox;
		$this->data['global_map_longitude']		=	$query[0]->ent_eixoy;


		$this->data['nomeEntidadeEscola']	=	$query[0]->ent_titulo;
		$this->data['current_nav']	=	'afederacao';



		$this->loadtemplate( 'entidadeescola', 'true', 'federacao' );

	}


	/**
	* Metodo responsavel por exibir uma listagem com foto de todas as modalidades
	*/
	public function entidade()
	{

		$this->data['title_page']			=	'';
		$this->data['tipoPagina']			=	'Entidades Filiadas';
		$this->data['current_nav']			=	'afederacao';
		$this->data['current_page']			=	'federacao_entidades';
		$this->data['nomeEntidadeEscola']	=	'Sociedade Hípica de Brasília';

		$this->loadtemplate( 'federacao_itemEntidadeEscola', 'true', 'federacao' );
	}


	/**
	* Metodo responsavel por exibir uma listagem com foto de todas as modalidades
	*/
	public function modalidades()
	{
		$this->data['title_page']			=	'';
		$this->data['current_nav']			=	'modalidades';
		$this->data['current_page']			=	'-';

		$this->loadtemplate( 'modalidades');
	}


	/**
	* Metodo responsavel por exibir a pagina de uma modalidade especifica.
	* A funcao recebe qual a modalidade e qual a pagina deve ser exibida
	* Modalidades aceitas: 'atrelagem','adestramento','cce','enduro','paraequestre','redeas','salto','volteio'
	* Paginas aceitas: 'historico', 'noticias', 'galeria', 'ranking' 'regulamento' 'taxas' 'quadro-juizes' 'reprises' 'cursos'
	*/
	public function modalidade($slugModalidade, $slugPagina=null)
	{

		/**
		* Setando variaveis 
		*/
		$this->data['title_page']						=	'';
		$this->data['current_nav']						=	'modalidades';


		/**
		* Checa se $slugPagina esta vazio e redireciona para a pagina de historico da modalidade informada
		*/
		if($slugPagina==null)
		{
			redirect(base_url() . 'modalidade/' . $slugModalidade . '/historico');
		}


		/**
		* Se o $slugModalidade for diferente de algum da instrucao o usuario
		* e direcionado para a pagina de listagem de modalidades. 
		*/
		if(
			$slugModalidade != 'atrelagem'&&
			$slugModalidade != 'adestramento' &&
			$slugModalidade != 'cce' &&
			$slugModalidade != 'enduro' &&
			$slugModalidade != 'paraequestre' &&
			$slugModalidade != 'redeas' &&
			$slugModalidade != 'salto' &&
			$slugModalidade != 'volteio'
		)
			redirect(base_url() . 'modalidades');


		/*
		Caso $slugPagina seja algum valor diferente do informado na instrucao e direcionado o usuario para a pagina de historico da modalidade informada.
		*/
		if(
			$slugPagina != 'historico' &&
			$slugPagina != 'noticias' &&
			$slugPagina != 'galeria' &&
			$slugPagina != 'ranking' &&
			$slugPagina != 'regulamento' &&
			$slugPagina != 'taxas' &&
			$slugPagina != 'quadro-juizes' &&
			$slugPagina != 'reprises' &&
			$slugPagina != 'cursos' 
		){
			redirect(base_url() . 'modalidade/' . $slugModalidade . '/historico');
		}
		

		/**
		* Definicao de Variaveis
		*/
		$idModalidade 			= $this->m_crud->get_rowSpecific('tb_modalidade', 'mod_slug', $slugModalidade, 1, 'mod_id');	
		$idCategoria 			= $this->m_crud->get_rowSpecific('tb_pagina_categoria', 'cat_slug', $slugPagina, 1, 'cat_id');	
		$tituloModalidade 		= $this->m_crud->get_rowSpecific('tb_modalidade', 'mod_slug', $slugModalidade, 1, 'mod_modalidade');
		$tituloPagina 			= $this->m_crud->get_rowSpecific('tb_pagina_categoria', 'cat_slug', $slugPagina, 1, 'cat_titulo');
		$conteudo 	            =	$this->m_crud->get_rowSpecificTwoParameters('tb_pagina', 'fk_mod_id', $idModalidade, 'fk_cat_id', $idCategoria, 1, 'pag_conteudo');


		/**
		* Array com todas as paginas que são de alguma modalidade
		*/
		$this->data['paginas']	=	$this->db->query('select * from tb_pagina_categoria where cat_id in (5,6,7,8,9,10,11) and cat_flagStatus IS NOT NULL ')->result();


		/**
		* Valores para serem enviados para a View
		*/
		$this->data['current_page']				=	'modalidade_' . $slugPagina;
		$this->data['conteudo']					=	$conteudo;
		$this->data['tituloModalidade']			=	$tituloModalidade;
		$this->data['tituloPagina']				=	$tituloPagina;
		$this->data['slugModalidade'] 			=	$slugModalidade;
		$this->data['slugPagina'] 				=	$slugPagina;


		/**
		* Carrega a View
		*/
		$this->loadtemplate( 'modalidade', true, 'modalidade');

	}





	/**
	* Metodo responsavel por exibir a listagem de eventos de todos os registros
	* Refatorar - só exibir eventos do ano atual - colocar botoes para anos anteriores
	*/
	public function calendario()
	{

		$this->data['arrEvento']	=	$this->db->query("


			
(
											
											SELECT * 
											FROM (
													SELECT * FROM fhbr01.tb_site_evento
													WHERE 	YEAR(eve_datainicio) = YEAR('2017-01-01') AND 
															fk_sta_id IN (5)
													ORDER BY date(eve_datainicio) DESC
													LIMIT 2
												) t
											ORDER BY eve_datainicio asc

											)
										UNION
										(

											SELECT * FROM fhbr01.tb_site_evento
											WHERE 	YEAR(eve_datainicio) = YEAR('2017-01-01') AND 
													fk_sta_id IN (1,2,3,4)
											ORDER BY date(eve_datainicio) asc
											LIMIT 100
										)

								
								

										")->result();			

        $this->data['current_page'] 	=	'calendario';
		$this->data['current_nav']		=	'calendario';
		$this->loadtemplate( 'calendario', false);
	}




	/**
	*/
	public function handleCalendarFilter()
	{

		$modality 		=	$_POST['modality'] ;
		$year 			=	$_POST['year'];
		$status 		=	$_POST['status'];
		
			
		$where  =  $this->handleCalendarFilterResponse($modality, $status, $year);
		if(!empty($where)){
			$where 	=	"WHERE " . $where;
		}


		setlocale (LC_ALL, 'pt_BR','ptb');
		mysql_query('SET lc_time_names = "pt_BR"');
		 

		$query	=						$this->db->query("

		SELECT
			e.eve_id,
			e.eve_evento,
			/* YEAR(e.eve_datainicio) as 'year', */
			CONCAT(UCASE(MID(MONTHNAME(e.eve_datainicio),1,1)),MID(MONTHNAME(e.eve_datainicio),2)) as 'months',
			CONCAT(	e.eve_datainicio, ' a ' ,e.eve_datafim	) as 'periodo',
			m.mod_modalidade,
			n.ent_sigla,
			s.sta_id


		FROM tb_site_evento as e

		INNER JOIN tb_modalidade as m ON (m.mod_id=e.fk_mod_id)
		INNER JOIN tb_site_evento_status as s ON (s.sta_id=e.fk_sta_id)
		INNER JOIN tb_entidadeescola as n ON (n.ent_id=e.fk_ent_id)

		".$where."

		ORDER BY date(eve_datainicio) asc

											;
										")->result();
		$countQuery 	=	count($query);
        echo json_encode( array( 'draw' => '1', 'recordsTotal' => count($query), 'recordsFiltered' => count($query), 'data' => $query ) );

	}


	public function handleCalendarFilterResponse($modality, $status, $year){

		$validate 	=	$this->handleCalendarFilterValidate($modality, $status, $year);

		$query 		=	'';
		$countData 	=	count($validate);
		$countForeach 	=	1;
		foreach ($validate as $key => $value) {
			$query .= $value;

			/* se nao for o ultimo nao poe a query*/

			if($countData != $countForeach){
				$query .= " AND ";
			}

			$countForeach++;
		}
		return $query;

	}



	public function handleCalendarFilterValidate($modality, $status, $year){

		$valid 	=	array();

		if(!empty($modality)){
			$valid['modality']	=	" fk_mod_id = '".$modality."' ";
		}

		if(!empty($status)){
			$valid['status']	=	" fk_sta_id = '".$status."' ";
		}

		if(!empty($year)){
			$valid['year']		=	" YEAR(eve_datainicio) = ".$year." ";
		}

		return $valid;

	}



	/*-----------------------------------------------	
	@ EVENTO
	-----------------------------------------------*/
	public function evento($eve_slug=NULL)
	{

		/* Setando principais variaveis 
		============================================= */

		/* Capturando o ID do evento a partir do slug recido como parametro da funcao */
		$idEvento 								=	$this->m_crud->get_rowSpecific(
														'tb_site_evento',
														'eve_slug',
														$eve_slug,
														1,
														'eve_id'
													);

		/* Dias */
		$dias 									=	$this->m_crud->get_allWhereOrderby(
														'tb_site_evento_dia',
														'fk_eve_id',
														$idEvento,
														'dia_posicao',
														'asc'
													)->result();

		/* Evento */
		$evento 			 					=	$this->m_crud->get_allWhere('														tb_site_evento',
														'eve_slug',
														$eve_slug
													);
		$evento 								=	$evento[0];

		/* Countdown */
		$evento_countdown 						=	$this->m_crud->get_rowSpecific(
														'tb_site_evento',
														'eve_slug',
														$eve_slug,
														1,
														'eve_datainicio'
													);


		/* Adendo */
		$adendo			 						=	$this->m_crud->get_rowSpecific(
														'tb_site_evento',
														'eve_slug',
														$eve_slug,
														1,
														'eve_adendo'
													);


		/* Localizacao ( Entidade Filiada )
		============================================= */
		$latitude 	=	$this->m_crud->get_rowSpecific(
							'tb_entidadeescola',
							'ent_id',
							$evento->fk_ent_id,
							1,
							'ent_eixox'
						);

		$longitude 	=	$this->m_crud->get_rowSpecific(
							'tb_entidadeescola',
							'ent_id',
							$evento->fk_ent_id,
							1,
							'ent_eixoy'
						);

		(empty($latitude) || empty($longitude)) ? $this->data['global_map']	=	FALSE :  $this->data['global_map']	=	TRUE;

		$global_map_title			=	$this->m_crud->get_rowSpecific(
														'tb_entidadeescola',
														'ent_id',
														$evento->fk_ent_id,
														1,
														'ent_titulo'
													);

		$global_map_content		=	$this->m_crud->get_rowSpecific(
														'tb_entidadeescola',
														'ent_id',
														$evento->fk_ent_id,
														1,
														'ent_endereco'
													);


		/* Status do Evento
		============================================= */
		switch ($evento->fk_sta_id) {
			
			case 1: //Inscricoes em Breve
				$this->processaStatusEvento('eve_data_abertura_inscricao', $eve_slug, 1);
				break;

			case 2: //Inscricoes Abertas
				$this->processaStatusEvento('eve_data_encerra_inscricao', $eve_slug, 2);
				break;

			case 3: //Inscricoes Encerradas
				$this->processaStatusEvento('eve_datainicio', $eve_slug, 3);
				break;

		}


		/* Setando $this->data
		============================================= */
		/* Info Gerais */
		$this->data['evento']										=	$evento;
		$this->data['eve_evento']									=	$evento->eve_evento;
		$this->data['fk_sta_id']									=	$evento->fk_sta_id;
		$this->data['periodo']										=	$evento->eve_datainicio;
		$this->data['eve_data_limite_pagamento_desconto_especial']	=	$evento->eve_data_limite_pagamento_desconto_especial;
		$this->data['eve_data_limite_pagamento_com_desconto']		=	$evento->eve_data_limite_pagamento_com_desconto;
		$this->data['eve_data_encerra_inscricao']					=	$evento->eve_data_encerra_inscricao;
		$this->data['eve_juri_presidente']							=	$evento->eve_juri_presidente;
		$this->data['eve_juri_membros']								=	$evento->eve_juri_membros;
		$this->data['eve_delegado_tecnico']							=	$evento->eve_delegado_tecnico;
		$this->data['eve_armador']									=	$evento->eve_armador;
		$this->data['dias']											=	$dias;
		/* Map */
		$this->data['global_map_title']								=	$global_map_title;
		$this->data['global_map_content']							=	$global_map_content;
		$this->data['global_map_latitude']							=	$latitude;
		$this->data['global_map_longitude']							=	$longitude;
		/* Slug */
		$this->data['eve_slug']										=	$eve_slug;
		/* Template */
        $this->data['current_page'] 								= 	'evento';
		$this->data['current_nav']									=	'calendario';
		$this->data['realizarInscricao']							=	FALSE;
		$this->data['programa']										=	FALSE;
		$this->data['adendo']										=	FALSE;
		#$this->data['ordensDeEntrada']								=	$this->processaBtns($evento, 'ordensDeEntrada');
		$this->data['ordensDeEntrada']								=	$evento->eve_flagOrdem;
		$this->data['quadroHorario']								=	TRUE;
		$this->data['resultados']									=	$evento->eve_flagResultado;
		$this->data['provas']										=	$this->processaBtns($evento, 'provas');
		$this->data['comprovanteInscricao']							=	FALSE;
		$this->data['segundaViaBoleto']								=	FALSE;
		$this->data['status_titulo']								=	$this->m_crud->get_rowSpecific('tb_site_evento_status', 'sta_id', $evento->fk_sta_id, 1, 'sta_titulo');
		$this->data['status_botao']									=	$this->m_crud->get_rowSpecific('tb_site_evento_status', 'sta_id', $evento->fk_sta_id, 1, 'sta_botao');
		$this->data['status_bootstrap']								=	$this->processaStatusBootstrap($evento->fk_sta_id);
		$this->data['eve_resultado']								=	$this->processaBtns($evento, 'resultado');
		$this->data['eve_quadrohorarios']							=	$this->processaBtns($evento, 'quadroHorario');
		$this->data['programa']										=	$this->processaBtns($evento, 'programa');
		$this->data['adendo']										=	$this->processaBtns($evento, 'adendo');


		/* Carrega estrutura do Template
		============================================= */
		$this->loadtemplate(
			array(
				'evento/header',
				'evento/generalInformation',
				'evento/sponsors',
				'evento/supporters',
				'evento/exam',
				'evento/footer',
				'evento/map',
			)
			, false);
	}


	/* Processa Status Evento 
	============================================= */
	public function processaStatusBootstrap($statusEvento){


		switch ($statusEvento) {

			case 1: //Inscricoes em Breve
				$status_bootstrap  = 'btn-primary';
				break;

			case 2: //Inscricoes Abertas
				$status_bootstrap  	= 'btn-success';
				$this->data['provas'] 				= TRUE;
				break;

			case 3: //Inscricoes Encerradas
				$status_bootstrap  	= 'btn-danger';
				$this->data['ordensdeentrada']   	= TRUE;
				$this->data['provas'] 				= TRUE;
				break;

			case 4: //Evento Acontecendo
				$status_bootstrap  	= 'btn-primary';
				$provas 			= TRUE;
				break;

			case 5: //Evento já Aconteceu
				$status_bootstrap  	= 'btn-warning';
				$provas 			= TRUE;
				break;
			
			case 6: //Evento Cancelado
				$status_bootstrap  = 'btn-default';
				break;

			//Default
			default:
				$status_bootstrap  = 'btn-default';
				break;
		}

		return $status_bootstrap;

	}


	/* Processa Btns 
	============================================= */
	public function processaBtns($evento, $tipo){

		$eve_resultado 			=	FALSE;
		$eve_quadrohorarios 	=	FALSE;
		$programa 				=	FALSE;
		$adendo 				=	FALSE;
		$provas 				=	FALSE;

		switch ($tipo) {

			case 'resultados':
				$eve_resultado = $this->m_crud->get_rowSpecific('tb_site_evento', 'eve_id', $evento->eve_id, 1, 'eve_resultado');
				if(is_null($eve_resultado)){
					$eve_resultado = 0;
				}else{
					$eve_resultado = 1;
				}
				return $eve_resultado;
				break;

			case 'quadroHorario':
				$eve_quadrohorarios = $evento->eve_quadrohorarios;
				if(is_null($eve_quadrohorarios)){
					$eve_quadrohorarios = 0;
				}else{
					$eve_quadrohorarios = 1;
				}
				return $eve_quadrohorarios;
				break;
			
			case 'programa':
				if(!empty($evento->eve_programa)){
					$programa 			= TRUE;
				}
				return $programa;
				break;

			case 'adendo':
				if(!empty($evento->eve_adendo)){
					$adendo 			= TRUE;
				}
				return $adendo;
				break;

			case 'provas':
				if($evento->fk_sta_id==4 || $evento->fk_sta_id==5){
					$provas		=	TRUE;
				}
				return $provas;
				break;

			case 'ordensDeEntrada':
				if($evento->fk_sta_id){
					$ordensDeEntrada   	= TRUE;
				}
				return $ordensDeEntrada;
				break;
		}

	}

	/* Processa Status Evento 
	============================================= */
	public function processaStatusEvento($tablefield, $eve_slug, $statusEvento){

		$data	=	$this->m_crud->get_rowSpecific('tb_site_evento', 'eve_slug', $eve_slug, 1, $tablefield);
		$ano 	=	$this->my_date->datetime($data, 'justYear');
		$mes 	=	$this->my_date->datetime($data, 'justMonth');
		$dia 	=	$this->my_date->datetime($data, 'justDay');

		$this->data['counterAno']		=	$ano;
		$this->data['counterMes']		=	$mes - 1;
		$this->data['counterDia']		=	$dia;
		if(!empty($data))
			$this->data['counter']		=	TRUE;
		
	}



    public function contactProcess(){

        //====================================================
        // Resgatar variaveis do formulario de contato 
        //====================================================
        $arrayData      	=	$_POST['arrayData'];
        parse_str($arrayData, $dataBulk);

        $nome       		=   $dataBulk['formNome'];
        $email      		=   $dataBulk['formEmail'];
        $telefone   		=   $dataBulk['formTelefone'];
        $celular    		=   $dataBulk['formCelular'];
        $comoencontrou      =   $dataBulk['formComoNosEncontrou'];
        $departamento       =   $dataBulk['formDepartamento'];
        $filiado 			=   $dataBulk['formFiliado'];
        $mensagem           =   $dataBulk['formMensagem'];
        
        $emailDepartamento	=   $this->m_crud->get_rowSpecific('tb_site_contato_departamento', 'dep_id', $departamento, 1, 'dep_email');


        // Caso - Primeiro Nome
        //====================================================
        $firstNameCustomer = explode(' ',trim($nome));
        $firstNameCustomer = $firstNameCustomer[0];
        //====================================================


        // Caso - Departamento
        //====================================================
        $departamento = $this->m_crud->get_rowSpecific('tb_site_contato_departamento', 'dep_id', $departamento, 1, 'dep_departamento');
        //====================================================


        // Caso - Como Encontrou
        //====================================================
        $comoencontrou = $this->m_crud->get_rowSpecific('tb_site_contato_comonosencontrou', 'com_id', $comoencontrou, 1, 'com_titulo');
        //====================================================


        //====================================================
        $email_remetente = "site@fhbr.com.br"; // deve ser um email do dominio
        //====================================================
  

        //Configurações do email, ajustar conforme necessidade
        //====================================================
        $email_destinatario = "$emailDepartamento"; // qualquer email pode receber os dados
        $email_reply = "$email";
        $email_assunto =  $departamento . " - " . date("dmYHis");
        //====================================================
 

        //Monta o Corpo da Mensagem
        //====================================================
        $email_conteudo =   "Nome: $nome \n";
        $email_conteudo .=  "Email: $email \n";
        $email_conteudo .=  "Telefone: $telefone \n";
        $email_conteudo .=  "Celular: $celular \n";
        $email_conteudo .=  "Como nos encontrou: $comoencontrou \n";
        $email_conteudo .=  "Departamento: $departamento \n";
        $email_conteudo .=  "Filiado: $filiado \n";
        $email_conteudo .=  "Mensagem: $mensagem \n";
        $email_conteudo .=  "Data/Hora: ".date("d/m/Y - H:i:s")." \n";
        //====================================================
 
 
        //Seta os Headers (Alerar somente caso necessario)
        //====================================================
        $email_headers = implode ( "\n",array ( "From: $email_remetente", "Reply-To: $email_reply", "Subject: $email_assunto","Return-Path:  $email_remetente","MIME-Version: 1.0","X-Priority: 3","Content-Type: text/html; charset=UTF-8" ) );
        //====================================================
 
 
        //Enviando o email
        //====================================================
        $triggerMail = mail ($email_destinatario, $email_assunto, nl2br($email_conteudo), $email_headers);
        //====================================================


        if($triggerMail)
        {
            $array = array(
                'status'                =>  'processed',
                'firstNameCustomer'     =>  ucfirst(strtolower($firstNameCustomer)),
            );
        }
        else
        {
            $array = array(
                'status'    =>  'unprocessed'
            );
        }

        echo json_encode($array);

    }


	/*
	 * Funcao e responsavel por exibir a listagem de posts. O metodo blog recebe o apontamento de duas rotas: noticias e galerias ( para mais detalhes veja arquivo routes.php do framework ). Um post e qualquer registro na tabela tb_blog. A funcao recebe dois parametros $slugTipoPagina e $slugCategoria

	 * $slugTipoPagina pode ser apenas 'noticias' ou 'galerias'.
	 * $slugCategoria e a categoria do tipo de pagina. 
	 
	 * Exemplo: A categoria novidades faz parte do tipo de pagina 'galerias'. Para mais detalhes ver o diagrama de entidade relacionada do banco.
	 
	 * Caso o $slugCategoria seja NULL e exibido a listagem de todos os posts daquele tipo de pagina

	 * $slugCategoria pode receber um valor slug tanto da tabela tb_blog_categoria quanto da tabela tb_modalidade.  
	*/
	public function blog($slugTipoPagina = NULL, $slugCategoria = NULL)
	{

		// Capturando o id do tipo de pagina a partir do $slugTipoPagina, recebido na funcao.
		$idTipoPagina	=	$this->m_crud->get_rowSpecific(
								'tb_blog_tipo',
								'tip_slug',
								$slugTipoPagina,
								1,
								'tip_id'
							);

		/*
		 * Condicional verifica se $slugCategoria esta vazio.
		 * Se tiver algum conteudo em $slugCategoria a instrucao vai para o else.
		 * E realizado uma consulta na tabela tb_blog_categoria com o $slugCategoria informado. Se retornar NULL indica que $slugCategoria nao e valido ou nao existe, logo deve procurar na tabela de modalidade. Se retornar algum valor procura o valor na tabela de noticias.
		 */
		if(empty($slugCategoria)){
			$query		=	$this->m_crud->get_allWhereTwoParametersOrderby(
								'tb_blog',
								'blo_flagStatus',
								'1',
								'fk_tip_id',
								$idTipoPagina,
								'blo_id',
								'desc'
							);
		}
		else{
			$query	=	$this->m_crud->get_rowSpecificTwoParameters(
							'tb_blog_categoria',
							'fk_tip_id',
							$idTipoPagina,
							'cat_slug',
							$slugCategoria,
							1,
							'cat_categoria'
						);

			if(is_null($query))
			{
				$idModalidade 		=	$this->m_crud->get_rowSpecific(
											'tb_modalidade',
											'mod_slug',
											$slugCategoria,
											1,
											'mod_id'
										);

				$query				=	$this->m_crud->get_allWhereThreeParametersOrderby(
											'tb_blog',
											'blo_flagStatus',
											'1',
											'fk_tip_id',
											$idTipoPagina,
											'fk_mod_id',
											$idModalidade,
											'blo_id',
											'desc'
										);
			}
			else
			{
				$idCategoria 		=	$this->m_crud->get_rowSpecificTwoParameters(
											'tb_blog_categoria',
											'fk_tip_id',
											$idTipoPagina,
											'cat_slug',
											$slugCategoria,
											1,
											'cat_id'
										);

				$query				=	$this->m_crud->get_allWhereThreeParametersOrderby(
											'tb_blog',
											'blo_flagStatus',
											'1',
											'fk_tip_id',
											$idTipoPagina,
											'fk_cat_id',
											$idCategoria,
											'blo_id',
											'desc'
										);


			}
		}


		/* Query para selecionar todos os itens da categoria associados ao tipo da pagina */
		$categoriasSidebar	=	$this->db->query(
									"
									SELECT * FROM tb_blog_categoria
									WHERE 	fk_tip_id=$idTipoPagina
									"
								)->result();



		/* Titulo da Pagina */
		switch ($slugTipoPagina) {
			case 'noticias':
				$tituloPagina			=	'Notícias';
				break;
			
			case 'galerias':
				$tituloPagina			=	'Galerias';
				break;
		}


		// PROCURA EM CATEGORIAS, SE NÃO ACHAR PROCURA EM MODALIDADES
		$tituloCategoria	=	$this->m_crud->get_rowSpecific(
									'tb_blog_categoria',
									'cat_slug',
									$slugCategoria,
									1,
									'cat_categoria'
								);
		if(is_null($tituloCategoria))
		{
			$tituloCategoria	=	$this->m_crud->get_rowSpecific(
										'tb_modalidade',
										'mod_slug',
										$slugCategoria,
										1,
										'mod_modalidade'
									);
			if(is_null($tituloCategoria))
				$tituloCategoria 	=	'Geral';
		}

		$this->data['tituloCategoria']		=	$tituloCategoria;

		/* CATEGORIAS SIDEBAR */
		$this->data['categoriasSidebar']	=	$categoriasSidebar;

		// SLUGS
		$this->data['slugTipoPagina']		=	$slugTipoPagina;
		$this->data['slugCategoria']		=	$slugCategoria;

		// DADOS
		$this->data['arrData']				=	$query;
		$this->data['tituloPagina']			=	$tituloPagina;
	

		// CARREGA VIEW		
		$this->loadtemplate( 'blog', true, 'blog' );
	}


	/**
	* Metodo responsavel por exibir um post especifico do blog.
	* @param string @slugTipoPagina - pode receber dois valores: 'noticias' ou 'galerias'
	* @param string @slugCategoria - recebe o slug da categoria referente ao post
	* @param string @slugPost - recebe o slug do post selecionado
	*/
	public function blogitem($slugTipoPagina=null, $slugCategoria=null, $slugPost=null)
	{

		// $idTipoPagina = retorna 1(noticias) ou 2(galerias).
		$idTipoPagina	=	$this->m_crud->get_rowSpecific('tb_blog_tipo', 'tip_slug', $slugTipoPagina, 1, 'tip_id');


		// Identificando se a categoria é uma categoria ou modalidade
		$tipoSlugCategoria  =  $this->m_crud->get_rowSpecific('tb_blog_categoria', 'cat_slug', $slugCategoria, 1, 'cat_id'); 
		if(is_null($tipoSlugCategoria)){
			// MODALIDADE
			$var	=	 'modalidade';
		}else{
			// CATEGORIA
			$var	=	 'categoria';
		}


		/* ARRAY DAS CATEGORIAS */
		$categoriasSidebar						=	$this->db->query("SELECT * FROM tb_blog_categoria WHERE fk_tip_id=$idTipoPagina AND cat_slug<>'geral'")->result();
		$this->data['categoriasSidebar']		=	$categoriasSidebar;


		// PROCURA EM CATEGORIAS, SE NÃO ACHAR PROCURA EM MODALIDADES
		$tituloCategoria	=	$this->m_crud->get_rowSpecific('tb_blog_categoria', 'cat_slug', $slugCategoria, 1, 'cat_categoria');
		if(is_null($tituloCategoria))
		{
			$tituloCategoria	=	$this->m_crud->get_rowSpecific('tb_modalidade', 'mod_slug', $slugCategoria, 1, 'mod_modalidade');
		}
		$this->data['tituloCategoria']		=	$tituloCategoria;


		// QUERY
		$query 	=	$this->m_crud->get_allWhereTwoParameters('tb_blog', 'blo_slug', $slugPost, 'fk_tip_id', $idTipoPagina);
		$this->data['query']	=	$query;

		// SE O POST NÃO EXISTIR REDIRECIONA PARA O TIPO E A CATEGORIA QUE FOI RECEBIDO NA URL
		if(empty($query))
		{
			redirect(base_url() . $slugTipoPagina . '/' . $slugCategoria);
		}


		// SLUGS
		$this->data['slugTipoPagina']		=	$slugTipoPagina;
		$this->data['slugCategoria']		=	$slugCategoria;
		$this->data['slugPost']				=	$slugPost;

		// id categoria
		$idPost 		= $this->m_crud->get_rowSpecificTwoParameters('tb_blog', 'fk_tip_id', $idTipoPagina, 'blo_slug', $slugPost, 1, 'blo_id');

		// RELACIONADAS
		// TRATAR CATEGORIA/MODALIDADE

		if($var=='categoria'){
			$idCategoria 	= $this->m_crud->get_rowSpecificTwoParameters('tb_blog_categoria', 'fk_tip_id', $idTipoPagina, 'cat_slug', $slugCategoria, 1, 'cat_id');
			$this->data['postsRelacionados']	=	$this->db->query("SELECT * FROM tb_blog
																	WHERE fk_tip_id=$idTipoPagina AND
																	fk_cat_id=$idCategoria AND
																	blo_id<>$idPost ORDER BY blo_id DESC LIMIT 3")->result();
		}else{
			$idModalidade 	= $this->m_crud->get_rowSpecific('tb_modalidade', 'mod_slug', $slugCategoria, 1, 'mod_id');
			$this->data['postsRelacionados']	=	$this->db->query("SELECT * FROM tb_blog
																	WHERE fk_tip_id=$idTipoPagina AND
																	fk_mod_id=$idModalidade AND
																	blo_id<>$idPost ORDER BY blo_id DESC LIMIT 3")->result();
		}


		// TIPO PAGINA ( breadcrumbs )
		if($slugTipoPagina=='noticias')
		{
			$this->data['tipoPagina']			=	'Notícias';
		}
		else
		{
			$this->data['arrFotos']				=	$this->m_crud->get_allWhere('tb_blog_foto', 'fk_blo_id', $idPost);
			$this->data['tipoPagina']			=	'Galerias';
		}

		$this->data['blo_titulo']		=		$query[0]->blo_titulo;
		$this->data['blo_data']			=		$this->my_date->datetime($query[0]->blo_data, 'justDate');
		$this->data['blo_conteudo']		=		$query[0]->blo_conteudo;
		$this->data['blo_fonte']		=		$query[0]->blo_fonte;
		$this->data['blo_folder']		=		$query[0]->blo_folder;
		$this->data['blo_slug']			=		$query[0]->blo_slug;
		
		$this->data['urlPost']			=		base_url() . $slugTipoPagina . '/' . $slugCategoria . '/' . $query[0]->blo_slug;
		$this->data['urlRelativoFoto']	=		'./manager/uploads/blog/galerias/' . $query[0]->blo_folder . '/';
		$this->data['urlAbsolutoFoto']	=		base_url() . 'manager/uploads/blog/galerias/' . $query[0]->blo_folder . '/';
		
		$this->data['modificado']		=		$this->my_date->datetime($query[0]->modificado, 'justDate');




		// CARREGA VIEW		
		$this->loadtemplate( 'blogitem', true, 'blog' );
	}

	

	/**
	 * Contato - Metodo responsavel por exibir a pagina de contato
	 */
	public function contato()
	{

		/**
		* Setando variaveis que serao utilizadas para exibir o googlemaps na pagina
		*/
		$this->data['global_map_title']			=	'<b>Federação Hípica de Brasília.</b>';
		$this->data['global_map_content']		=	'Setor Hípico - Lote 08 - Brasília - DF<br/>Próximo ao Jardim Zoológico de Brasília<br>';
		$this->data['global_map_latitude']		=	'-15.835553';
		$this->data['global_map_longitude']		=	'-47.942326';
		 

		/**
		* Como nos encontrou - Realizando a consulta no banco e armazenando no array data para enviar para view
		*/
		$comonosencontrou 		=	$this->m_crud->get_allLimitOrderby('tb_contato_comoencontrou', 20, 'com_posicao', 'asc');
		$this->data['comonosencontrou']	=	$this->montarDropdown($comonosencontrou, 'Selecione uma opção', 'com_id', 'com_titulo');


		/**
		* Departamentos - Realizando a consulta no banco e armazenando no array data para enviar para view
		*/
		$departamentos 		=	$this->m_crud->get_allOrderby('tb_contato_departamento', 'dep_posicao', 'asc');
		$this->data['departamentos']	=	$this->montarDropdown($departamentos, 'Selecione uma opção', 'dep_id', 'dep_departamento');


		/**
		* Setando variaveis que serao utilizadas para exibir o googlemaps na pagina
		*/
		$this->data['title_page']		=	'';
		$this->data['current_page']		=	'contato';


		/**
		* Setando variaveis que serao utilizadas para exibir o googlemaps na pagina
		*/
		$this->loadtemplate( 'contato' );

	}


	/**
	 * Metodo responsavel por carregar as view do template
	 * @param string @pathView - recebe qual arquivo deve ser carregado. Caso o arquivo nao esteja na raiz da pasta view do module site, informar o diretorio antes de informar o arquivo
	 */
	protected function loadTemplate( $pathView, $sidebar=false, $sidebar_type=false )
	{
		$this->load->library('parser');
		$this->load->library('sf_parser'); // http://www.lantean.co/extending-codeigniter-parser-to-support-objects/

		$data = $this->data;

		$this->sf_parser->parse('header', $data);


		if(is_array($pathView))
		{
			foreach ($pathView as $view) {
				$this->sf_parser->parse( $view, $data );
			}
		}
		else
		{
			$this->sf_parser->parse( $pathView, $data );
		}


		if($sidebar==true)
		{
			$sidebar_type = 'sidebar_' . $sidebar_type;
			$this->sf_parser->parse($sidebar_type, $data);
		}
		
		$this->sf_parser->parse('footer', $data);
	}



	/**
	 * Montar dropdown
 	 * @param array @dados
 	 * @param string @primeiroOption
 	 * @param string @indice
 	 * @param string @valor
 	 *
	 */
	function montarDropdown($dados, $primeiroOption, $indice, $valor)
	{
		$arrAux = array(''=>$primeiroOption);
		foreach ($dados as $key => $value) {
			$arrAux[$value->$indice]	=	$value->$valor;
		}
		return $arrAux;
	}




	/**
	 * O metodo environmentVariables() e chamado pelo metodo construtor da classe
	 * E nesse metodo onde sao setadas as variaveis que serao utilizadas em todo template
	 */
	public function environmentVariables(){

		$this->data['current_nav']				=	'';
		$this->data['title_page']				=	'';
		$this->data['use_wizard']				=	'';
		$this->data['use_mask']					=	'';
		$this->data['use_owlslider']			=	'';
		$this->data['use_maps']					=	'';
		
		$this->data['global_map']				=	FALSE;
		$this->data['global_map_title']			=	'Federação Hípica de Brasília';
		$this->data['global_map_content']		=	'<b>Federação Hípica de Brasília.</b><br/> Setor Hípico - Lote 08 - Brasília - DF<br/>Em frente ao Jardim Zoológico de Brasília<br>';
		$this->data['global_map_latitude']		=	48.852722;
		$this->data['global_map_longitude']		=	2.348527;

		$this->data['evento_countdown']			=	'';
		$this->data['counterAno']					=	NULL;
		$this->data['counterMes']					=	NULL;
		$this->data['counterDia']					=	NULL;
		$this->data['counter']						=	FALSE;


		/*
			Banner Publicitários
		*/
		$this->data['banners']		=	$this->m_crud->get_allWhereLimitOrderby('tb_banner', 'ban_flagStatus', '1',  1, 'ban_id', 'random');
		$this->data['parceiros']	=	$this->m_crud->get_allWhereLimitOrderby('tb_parceiro', 'par_flagStatus', '1', 20, 'par_posicao', 'asc');


		$this->data['enquete_id_pergunta']  =	$this->m_crud->get_rowSpecific('tb_site_enquete_pergunta', 'per_status', 1, 1, 'per_id');
		$this->data['enquete_pergunta']		=	$this->m_crud->get_rowSpecific('tb_site_enquete_pergunta', 'per_status', 1, 1, 'per_pergunta');




		/*
			Modalidades são usadas em todo o site:
			- Menu
			- Listagem de modalidades na index
			- Caledario
			- Noticias
			- Galerias
		*/
		$this->data['slugTipoPagina']		=	NULL;
		$this->data['slugCategoria']		=	NULL;
		$this->data['modalidades']				=	$this->m_crud->get_allWhereOrderby('tb_modalidade', 'mod_status', '1', 'mod_modalidade', 'asc')->result();
		$this->data['categoriasDeNoticias']		=	$this->db->query("SELECT * FROM tb_blog_categoria WHERE fk_tip_id=1 AND cat_slug<>'geral'")->result();
		$this->data['categoriasDeGalerias']		=	$this->db->query("SELECT * FROM tb_blog_categoria WHERE fk_tip_id=2 AND cat_slug<>'geral'")->result();

        $this->data['current_page'] 	= NULL;


		$this->data['entidadesescolas_rodape']	=	$this->db->query("SELECT * FROM tb_entidadeescola WHERE ent_flag_entidade=1 AND ent_logo<>'nophoto.jpg' AND ent_status=1 ")->result();

	}



	/**
	 * Metodo responsavel por realizar o processamento dos dados submetidos
	 * na enquete
	 */
	public function processaVoto()
	{
		/*
		1ª Ler variaveis recebidas
		*/
		$idPergunta 			=	$_POST['idPergunta'];
		if(isset($_POST['idRespostaSelecionada'])){
			$idRespostaSelecionada 	= 	$_POST['idRespostaSelecionada'];
		}
		$idBtnSelecionado 		=	$_POST['idBtnSelecionado'];


		/*
		2ª Verificar se já existe cookie.
		*/
		$nomeDoCookie = 'cookie_enquete_'.$idPergunta;
		if (!get_cookie($nomeDoCookie)) {
		    /* 	
			    Cookie nao registrado, retorna false
		    */

			// Inserindo voto no banco
			$dados = array(
				'fk_res_id'	=> $idRespostaSelecionada,
				'vot_ip'	=> $_SERVER['REMOTE_ADDR'],
				'criado'	=> date("Y-m-d H:i:s")
			);
			$this->m_crud->insert('tb_site_enquete_voto', $dados);

			// Setando Cookie
			$cookie = array(
			    'name'   => $nomeDoCookie,
			    'value'  => $this->my_date->datetime(date("Y-m-d H:i:s"), 'justDate') . 'x' . $_SERVER['REMOTE_ADDR'],
			    'expire' => 86500,
			    'secure' => false
			);
			$this->input->set_cookie($cookie); 

			// Retornando dados por json
			$array = array(
				'status'=>  'voto_computado',
				'btn-origem'=> $idBtnSelecionado
			);
			sleep(2);
			echo json_encode($array);
		}
		else{
			/*
			 Cookie registrado
			 */
			$cookieRegistrado = $this->input->cookie($nomeDoCookie, TRUE);
			$spiceCookieRegistrado = explode('x', $cookieRegistrado);
			$array = array(
				'status'			=>  'voto_nao_computado',
				'datahora_origem'	=>  $spiceCookieRegistrado[0],
				'ip_origem'			=>  $spiceCookieRegistrado[1],
				'btn_origem'		=> $idBtnSelecionado
			);
			sleep(2);
			echo json_encode($array);
		}


	}




}
/* END CLASS */

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */



