<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class cSite extends CI_Controller
{

	/**
	 * Variavel que contem as regras de validacao do formulario 
	 * @var array
	 */
	function __construct()
	{


		parent::__construct();


		/**
		 * Setando a timezone do sistema para America / Sao Paulo
		 */
	 	date_default_timezone_set('America/Sao_Paulo');
	

		/**
		 * Carregando library que serao utilizadas em toda a classe
		 */
		$this->load->library('my_date');


		/**
		 * Carregando helpers que serao utilizadas em toda a classe
		 */
		$this->load->helper('cookie');



		/**
		 * Chamada do metodo environmentVariables() responsavel por
		 * setar as variaveis que serao utilizadas no template
		 */
		$this->environmentVariables();

	}
	



	/*-----------------------------------------------	
	@ INDEX
	-----------------------------------------------*/
	public function index()
	{
		$this->inicio();
	}



	/*-----------------------------------------------	
	@ INDEX
	-----------------------------------------------*/
	public function estatuto()
	{
		$data = file_get_contents(base_url() . "uploads/estatuto/estatuto.pdf"); // Read the file's contents
		$name = 'estatuto.pdf';

		force_download($name, $data);
	}


	/*-----------------------------------------------	
	@ PROGRAMA ( EVENTO )
	-----------------------------------------------*/
	public function force_download_programa($slugEvento)
	{

		$nomeArquivo	=	$this->m_crud->get_rowSpecific('tb_site_evento', 'eve_slug', $slugEvento, 1, 'eve_programa'); 
		$data 			= file_get_contents(base_url() . "uploads/evento/programa/" . $nomeArquivo); // Read the file's contents
		$name 			= '[programa] - ' . $slugEvento . '.pdf';

		force_download($name, $data);
	}


	/*-----------------------------------------------	
	@ INÍCIO
	-----------------------------------------------*/
	public function inicio()
	{
		$this->data['title_page']	=	'';
		$this->data['current_page']	=	'inicio';
		$this->data['current_nav']	=	'inicio';


		$this->data['ultimas_galerias']	=	$this->m_crud->get_allWhereTwoParametersOrderbyLimit('tb_site_blog', 'fk_tip_id', 2, 'blo_status', 1, 'blo_id', 'desc', 2);
		$this->data['proximos_eventos']	=	$this->db->query("SELECT * FROM tb_site_evento 
															WHERE DATE(eve_datafim) > DATE(NOW() + INTERVAL -1 DAY) AND
															fk_sta_id IN (1,2,3,4,5)
															ORDER BY eve_datainicio ASC
															LIMIT 4
															; ")->result();


		$this->data['ultimos_destaques']	=	$this->db->query("SELECT * FROM tb_site_blog 
															WHERE fk_tip_id=1
															AND blo_posicao_destaque IS NOT NULL
															ORDER BY blo_posicao_destaque ASC
															LIMIT 4
															; ")->result();


		$this->loadtemplate( 'inicio' );
	}


	/*-----------------------------------------------	
	@ A FEDERAÇÃO - HISTORIA
	-----------------------------------------------*/
	public function afederacao($slugPagina)
	{

		$secondSegment 	=	$this->uri->segment(2);
		$thirdSegment 	=	$this->uri->segment(3);

		if($secondSegment=='entidades-filiadas' || $secondSegment=='escolas-de-equitacao'){
			redirect(base_url().'site/cSite/entidadeescola/'. $secondSegment . '/' . $thirdSegment);
		}



		$this->data['title_page']						=	'';
		$this->data['current_nav']						=	'afederacao';

		if($slugPagina==null)
		{
			redirect(base_url() . 'afederacao/' . 'historia');
		}

		if(
			$slugPagina != 'historia' &&
			$slugPagina != 'tribunal-de-justica-desportiva' &&
			$slugPagina != 'prestacao-de-contas' &&
			$slugPagina != 'contratacoes-e-licitacoes'
		)
		{
			redirect(base_url() . 'a-federacao/' . 'historia');
		}
		else
		{

/*
			// ENTIDADES FILIADAS
			if($slugPagina=='noticias')
			{
				redirect(base_url());
			}
*/

			$query 								=	$this->m_crud->get_allWhere('tb_site_pagina_categoria', 'cat_slug', $slugPagina);
			$idCategoria						=   $query[0]->cat_id;

			$this->data['tituloPagina']			=	$this->m_crud->get_rowSpecific('tb_site_pagina_categoria', 'cat_slug', $slugPagina, 1, 'cat_titulo');

			$this->data['paginas']				=	$this->db->query('select * from tb_site_pagina_categoria where cat_id in (1,2,3,4)')->result();
			$this->data['slugPagina'] 			= 	$slugPagina;
			$this->data['current_page']			=	'modalidade_' . $slugPagina;

			$this->data['conteudo']				=	$this->m_crud->get_rowSpecific('tb_site_pagina', 'fk_cat_id', $idCategoria, 1, 'pag_conteudo');

			$this->loadtemplate( 'federacao', 'true', 'federacao' );

		}
	}



	/*-----------------------------------------------	
	@ A FEDERAÇÃO - (LISTAGEM) ENTIDADES FILIADAS
	-----------------------------------------------*/
	public function entidadesescolas()
	{

		$slugTipoPagina = $this->uri->segment(2);
		$this->data['slugTipoPagina']	=	$slugTipoPagina;
		$this->data['slugItem']			=	NULL;

		if($slugTipoPagina=='entidades-filiadas')
		{
			$flag_selecionado 	=	'ent_flag_entidade';
			$this->data['tituloTipoPagina']	=	'Entidades Filiadas';
			$this->data['current_page']	=	'federacao_entidades';
		}
		else
		{
			$flag_selecionado 	=	'ent_flag_escola';
			$this->data['tituloTipoPagina']	=	'Escolas de Equitação';
			$this->data['current_page']	=	'federacao_escolas';
		}

		$this->data['entidadesescolas']	=	$this->m_crud->get_allWhereTwoParametersOrderby('tb_site_entidadeescola', $flag_selecionado, 1, 'ent_status', 1,'ent_titulo', 'asc');

		$this->data['title_page']	=	'';

		$this->data['current_nav']	=	'afederacao';

		$this->loadtemplate( 'entidadesescolas', 'true', 'federacao' );
	}



	/*-----------------------------------------------	
	@ Processa contato
	-----------------------------------------------*/
	public function processaContato()
	{
		$post = serialize($_POST);
		$data = array(
		   'con_conteudo' => $post,
		);

		$this->db->insert('tb_site_contato', $data);




		$keySubmit = $this->input->post('keySubmit');
		if(strlen($keySubmit)>0):

			if($this->form_validation->run()==TRUE):


				/* CONSULTA - ESTADOS */
/*				$estadoPost = $this->input->post('cod_estados');
				$estadoQuery = $this->crud->get_allWhereLimit('estados','cod_estados',$estadoPost,1);
				$estadoQueryRow = $estadoQuery->row();
				$estado = $estadoQueryRow->nome;*/

				/* CONSULTA - CIDADES */
/*				$cidadePost = $this->input->post('cod_cidades');
				$cidadeQuery = $this->crud->get_allWhereLimit('cidades','cod_cidades',$cidadePost,1);
				$cidadeQueryRow = $cidadeQuery->row();
				$cidade = $cidadeQueryRow->nome;*/


				$nome = $this->input->post('nome');
				$email = $this->input->post('email');
				$telefone = $this->input->post('telefone');
				$celular = $this->input->post('telefone');
				$departamento = $this->input->post('departamento');
				$departamento_titulo = $this->m_crud->get_rowSpecific('tb_site_contato_departamento', 'com_titulo', $departamento, 1, 'dep_departamento');
				$departamento_email = $this->m_crud->get_rowSpecific('tb_site_contato_departamento', 'com_titulo', $departamento, 1, 'dep_email');
				$filiado = $this->input->post('filiado');
				$mensagemOriginal = $this->input->post('mensagem');
					
				if($filiado==1){
					$filiado == 'Sim';
				}
				else{
					$filiado == 'Não';
				}

				$comonosencontrou_titulo = $this->m_crud->get_rowSpecific('tb_site_contato_comonosencontrou', 'com_id', $comonosencontrou, 1, 'com_titulo');

				$mensagem = 
				"
				Nome: ".$nome." <br />
				E-mail: ".$email." <br />
				Telefone: ".$telefone." <br />
				Celular: ".$celular." <br />
				Como nós Encontrou: ".$comonosencontrou_titulo." <br />
				Assunto: ".$assunto." <br />
				Filiado: ".$filiado." <br />
				Mensagem: ".$mensagemOriginal." <br />
				";

				/*-------------------------------------------
					ENVIANDO E-MAIL 
				--------------------------------------------*/
			    $mail = new PHPMailer();
				$mail->IsSMTP(); 
				$mail->SMTPAuth   	 = true; 
				$mail->SMTPSecure 	 = "ssl";  
				$mail->Host       	 = "smtp.lcgas.com.br";      
				$mail->Port       	 = 465;                   
				$mail->Username   	 = "site@lcgas.com.br";  
				$mail->Password   	 = "des1008d";            
				$mail->SetFrom("gustavo@avantdigital.com.br", utf8_decode("FHBr - Site"));
				$mail->Subject    	 = utf8_decode("[FHBr-WEBSITE]  - " . $nome);
				$mail->Body      	 = $mensagem;
				$mail->AltBody    	 = $mensagem;

				$emailDestino 		  = $this->crud->get_rowSpecific('contato_departamento','id',$departamento, 1, 'email');
				$departamentoDestino  = $this->crud->get_rowSpecific('contato_departamento','id',$departamento, 1, 'departamento');

				$mail->AddAddress($emailDestino, $departamentoDestino);
				$mail->AddAddress('contato@fhbr.com.br', 'Contato');


				if(!$mail->Send()) {
					$data["message"] = "Error en el envío: " . $mail->ErrorInfo;
					redirect("erro",'refresh');
				}
				else {
					$this->session->set_flashdata('sucessook',' <strong>Olá '.$nome.'.</strong> <br /><br />	 Recebemos sua mensagem. <br /> <br />	Entraremos em contato até 72 horas úteis.');
					redirect(base_url()."contato",'refresh');
				}

			endif;

		endif;


		redirect(base_url() . 'contato'); 
	}



	/*-----------------------------------------------	
	@ A FEDERAÇÃO - (PARTICULAR) ENTIDADE / ESCOLA
	-----------------------------------------------*/
	public function entidadeescola()
	{
		$slugTipoPagina	=	$this->uri->segment(2);
		$slugItem		=	$this->uri->segment(3);

		$this->data['slugTipoPagina']	=	$slugTipoPagina;
		$this->data['slugItem']			=	$slugItem;


		if($slugTipoPagina=='entidades-filiadas')
		{
			$flag_selecionado 	=	'ent_flag_entidade';
			$this->data['tituloTipoPagina']	=	'Entidades Filiadas';
		}
		else
		{
			$flag_selecionado 	=	'ent_flag_escola';
			$this->data['tituloTipoPagina']	=	'Escolas de Equitação';
		}
		$this->data['entidadesescolas']	=	$this->m_crud->get_allWhereLimitOrderby('tb_site_entidadeescola', $flag_selecionado, 1, 100, 'ent_titulo', 'asc');


		$idItem 	=	$this->m_crud->get_rowSpecific('tb_site_entidadeescola', 'ent_slug', $slugItem, 1, 'ent_id');

		$query 								=	$this->m_crud->get_allWhereLimitOrderby('tb_site_entidadeescola', 'ent_slug', $slugItem, 1, 'ent_id', 'asc');
		$this->data['query']				=	$query[0];
		$this->data['fotos']				=	$this->m_crud->get_allWhereLimitOrderby('tb_site_entidadeescola_foto', 'fk_ent_id', $idItem, 100, 'fot_id', 'asc');
		/*var_dump($this->data['fotos']);*/


		/* CONFIGURAÇÕES DO MAPA */
		$this->data['global_map_title']			=	$query[0]->ent_titulo;
		$this->data['global_map_content']		=	$query[0]->ent_endereco;
		$this->data['global_map_latitude']		=	$query[0]->ent_eixox;
		$this->data['global_map_longitude']		=	$query[0]->ent_eixoy;


		$this->data['nomeEntidadeEscola']	=	$query[0]->ent_titulo;
		$this->data['current_nav']	=	'afederacao';



		$this->loadtemplate( 'entidadeescola', 'true', 'federacao' );

	}


	/*-----------------------------------------------	
	@ A FEDERAÇÃO - (ITEM) ENTIDADE
	-----------------------------------------------*/
	public function entidade()
	{

		$this->data['title_page']			=	'';
		$this->data['tipoPagina']			=	'Entidades Filiadas';
		$this->data['current_nav']			=	'afederacao';
		$this->data['current_page']			=	'federacao_entidades';
		$this->data['nomeEntidadeEscola']	=	'Sociedade Hípica de Brasília';

		$this->loadtemplate( 'federacao_itemEntidadeEscola', 'true', 'federacao' );
	}



	/*-----------------------------------------------	
	@ A FEDERAÇÃO - (ITEM) ENTIDADE
	-----------------------------------------------*/
	public function modalidades()
	{
		$this->data['title_page']			=	'';
		$this->data['current_nav']			=	'modalidades';
		$this->data['current_page']			=	'-';


		$this->loadtemplate( 'modalidades');
	}


	/*-----------------------------------------------	
	@ MODALIDADE
	-----------------------------------------------*/
	public function modalidade($slugModalidade, $slugPagina=null)
	{


		$this->data['title_page']						=	'';
		$this->data['current_nav']						=	'modalidades';

		if($slugPagina==null)
		{
			redirect(base_url() . 'modalidade/' . $slugModalidade . '/historico');
		}

		if(
			$slugModalidade != 'atrelagem' &&
			$slugModalidade != 'adestramento' &&
			$slugModalidade != 'cce' &&
			$slugModalidade != 'enduro' &&
			$slugModalidade != 'paraequestre' &&
			$slugModalidade != 'redeas' &&
			$slugModalidade != 'salto' &&
			$slugModalidade != 'volteio'
		)
		{
			redirect(base_url().'modalidade');
		}
		else
		{

			/*
			- Verificando qual a página da categoria
			*/
			if(
				$slugPagina != 'historico' &&
				$slugPagina != 'noticias' &&
				$slugPagina != 'galeria' &&
				$slugPagina != 'ranking' &&
				$slugPagina != 'regulamento' &&
				$slugPagina != 'taxas' &&
				$slugPagina != 'quadro-juizes' &&
				$slugPagina != 'reprises' &&
				$slugPagina != 'cursos' 
			)
			{
				// Se houver um valor diferente da condição, redireciona para histórico
				redirect(base_url() . 'modalidade/' . $slugModalidade . '/historico');
			}
			else
			{

				// NOTICIAS
				if($slugPagina=='noticias')
				{
					redirect(base_url());
				}

				// GALERIA
				if($slugPagina=='galeria')
				{
					redirect(base_url() );
				}

			}
			
			

			/* Array com todas as paginas que são de alguma modalidade */	
			$this->data['paginas']	=	$this->db->query('select * from tb_site_pagina_categoria where cat_id in (5,6,7,8,9,10,11)')->result();


			$idModalidade 					= $this->m_crud->get_rowSpecific('tb_site_modalidade', 'mod_slug', $slugModalidade, 1, 'mod_id');	
			$tituloModalidade 				= $this->m_crud->get_rowSpecific('tb_site_modalidade', 'mod_slug', $slugModalidade, 1, 'mod_modalidade');
			$this->data['slugModalidade'] 	= $slugModalidade;
		
			$idCategoria 					= $this->m_crud->get_rowSpecific('tb_site_pagina_categoria', 'cat_slug', $slugPagina, 1, 'cat_id');	
			$tituloPagina 					= $this->m_crud->get_rowSpecific('tb_site_pagina_categoria', 'cat_slug', $slugPagina, 1, 'cat_titulo');
			$this->data['slugPagina'] 		= $slugPagina;



			$this->data['tituloModalidade']					=	$tituloModalidade;
			$this->data['tituloPagina']						=	$tituloPagina;
		
			$this->data['modalidade_pictograma']			=	$slugModalidade;
			$this->data['current_page']						=	'modalidade_' . $slugPagina;

			$conteudo 	                                    = $this->m_crud->get_rowSpecificTwoParameters('tb_site_pagina', 'fk_mod_id', $idModalidade, 'fk_cat_id', $idCategoria, 1, 'pag_conteudo');
			$this->data['conteudo']							= $conteudo;

			$this->loadtemplate( 'modalidade', true, 'modalidade');

		}

	}



	function image_thumb( $image_path='http://fhbr.com.br/vanguarda/implantacao/uploads/entidadeescola/centro-hipico-lago-sul/CHLS001.jpg', $height=50, $width=50 ) {

	    // Get the CodeIgniter super object
	    $CI =& get_instance();

	    // Path to image thumbnail
	    $image_thumb = dirname( $image_path ) . '/' . $height . '_' . $width . '.jpg';

	    if ( !file_exists( $image_thumb ) ) {
	        // LOAD LIBRARY
	        $CI->load->library( 'image_lib' );

	        // CONFIGURE IMAGE LIBRARY
	        $config['image_library']    = 'gd2';
	        $config['source_image']     = $image_path;
	        $config['new_image']        = $image_thumb;
	        $config['maintain_ratio']   = TRUE;
	        $config['height']           = $height;
	        $config['width']            = $width;
	        $CI->image_lib->initialize( $config );
	        $CI->image_lib->resize();
	        $CI->image_lib->clear();
	    }

	    echo "	<img src='".$config['new_image']."'>	";
	    echo '<img src="' . dirname( $_SERVER['SCRIPT_NAME'] ) . '/' . $image_thumb . '"  class="img-responsive" />';
	}


	function ajustar()
	{

		$src_img = './uploads/entidadeescola/centro-hipico-lago-sul/CHLS001.jpg';
		echo thumb($src_img, 200, 100); // outputs image_thumb.jpg 

	}



	/**
	* Metodo responsavel por exibir a listagem de eventos de todos os registros
	* Refatorar - só exibir eventos do ano atual - colocar botoes para anos anteriores
	*/
	public function calendario($mod_slug=NULL)
	{

		$this->data['current_nav']	=	'calendario';
		$idModalidade 		=	$this->m_crud->get_rowSpecific('tb_site_modalidade', 'mod_slug', $mod_slug, 1, 'mod_id');

		if($mod_slug==NULL || $mod_slug == 'geral')
		{
			$mod_slug = 'geral';
			$this->data['arrEvento']	=	$this->db->query("SELECT * FROM tb_site_evento 
											ORDER BY DATE(eve_datainicio) ASC
									; ")->result();			
		}
		else
		{
			$this->data['arrEvento']	=	$this->db->query("SELECT * FROM tb_site_evento 
											WHERE fk_mod_id='$idModalidade'
											ORDER BY DATE(eve_datainicio) ASC
									; ")->result();			
//			$this->data['arrEvento']	=	$this->m_crud->get_allWhereOrde('tb_site_evento', 'fk_mod_id', $idModalidade);
		}


		// TRATAR SE MOD_SLUG NÃO EXISTIR

		$this->data['mod_slug']	=	$mod_slug;
		$this->loadtemplate( 'calendario', false);
	}





	/*-----------------------------------------------	
	@ EVENTO
	-----------------------------------------------*/
	public function evento($eve_slug=NULL)
	{
		$idEvento 								=	$this->m_crud->get_rowSpecific('tb_site_evento', 'eve_slug', $eve_slug, 1, 'eve_id');
		$this->data['dias'] 					=	$this->m_crud->get_allWhereOrderby('tb_site_evento_dia', 'fk_eve_id', $idEvento, 'dia_posicao', 'asc')->result();

		$this->data['evento'] 					=	$this->m_crud->get_allWhere('tb_site_evento', 'eve_slug', $eve_slug);
		$this->data['evento_countdown'] 		=	$this->m_crud->get_rowSpecific('tb_site_evento', 'eve_slug', $this->data['evento'][0]->eve_slug, 1, 'eve_datainicio');
		$this->data['adendo']			 		=	$this->m_crud->get_rowSpecific('tb_site_evento', 'eve_slug', $this->data['evento'][0]->eve_slug, 1, 'eve_adendo');
		$this->data['realizarInscricao'] 		=	$this->m_crud->get_rowSpecific('tb_site_evento', 'eve_slug', $this->data['evento'][0]->eve_slug, 1, 'eve_inscricao');

		/* LOCALIZAÇÃO DA ENTIDADE */
		$latitude 	= $this->m_crud->get_rowSpecific('tb_site_entidadeescola', 'ent_id', $this->data['evento'][0]->fk_ent_id, 1, 'ent_eixox');
		$longitude 	= $this->m_crud->get_rowSpecific('tb_site_entidadeescola', 'ent_id', $this->data['evento'][0]->fk_ent_id, 1, 'ent_eixoy');
		if(empty($latitude) || empty($longitude)){
			$this->data['global_map']				=	FALSE;
		}else{
			$this->data['global_map']				=	TRUE;
		}
		$this->data['global_map_title']			=	$this->m_crud->get_rowSpecific('tb_site_entidadeescola', 'ent_id', $this->data['evento'][0]->fk_ent_id, 1, 'ent_titulo');
		$this->data['global_map_content']		=	$this->m_crud->get_rowSpecific('tb_site_entidadeescola', 'ent_id', $this->data['evento'][0]->fk_ent_id, 1, 'ent_endereco');
		$this->data['global_map_latitude']		=	$latitude;
		$this->data['global_map_longitude']		=	$longitude;

		/* INFORMAÇÕES GERAIS DO EVENTO */
		$periodo					=	$this->data['evento'][0]->eve_datainicio;
		$eve_data_limite_pagamento_desconto_especial	=	$this->data['evento'][0]->eve_data_limite_pagamento_desconto_especial;
		$eve_data_limite_pagamento_com_desconto			=	$this->data['evento'][0]->eve_data_limite_pagamento_com_desconto;
		$eve_data_encerra_inscricao	=	$this->data['evento'][0]->eve_data_encerra_inscricao;
		$eve_juri_presidente		=	$this->data['evento'][0]->eve_juri_presidente;
		$eve_juri_membros			=	$this->data['evento'][0]->eve_juri_membros;
		$eve_delegado_tecnico		=	$this->data['evento'][0]->eve_delegado_tecnico;
		$eve_armador				=	$this->data['evento'][0]->eve_armador;
		$fk_sta_id					=	$this->data['evento'][0]->fk_sta_id;


		/*
			// Inscricoes Abertas
			case 2:
		*/
		switch ($this->data['evento'][0]->fk_sta_id) {

			//Inscricoes em Breve
			case 1:
				$this->processaStatusEvento('eve_data_abertura_inscricao', $eve_slug, 1);
				break;

			//Inscricoes Abertas
			case 2:
				$this->processaStatusEvento('eve_data_encerra_inscricao', $eve_slug, 2);
				break;

			//Inscricoes Encerradas
			case 3:
				$this->processaStatusEvento('eve_datainicio', $eve_slug, 3);
				break;

			//Default
			default:
				$status_bootstrap 			= 'btn-default';
				break;

		}




		/* ENVIANDO DADOS PRA VIEW */
		$this->data['periodo']		=	$periodo;
		$this->data['eve_data_limite_pagamento_desconto_especial']	=	$eve_data_limite_pagamento_desconto_especial;
		$this->data['eve_data_limite_pagamento_com_desconto']		=	$eve_data_limite_pagamento_com_desconto;
		$this->data['eve_data_encerra_inscricao']					=	$eve_data_encerra_inscricao;
		$this->data['eve_juri_presidente']							=	$eve_juri_presidente;
		$this->data['eve_juri_membros']								=	$eve_juri_membros;
		$this->data['eve_delegado_tecnico']							=	$eve_delegado_tecnico;
		$this->data['eve_armador']									=	$eve_armador;
		$this->data['fk_sta_id']									=	$fk_sta_id;



		/* ENVIANDO VARIÁVEIS DE TEMPLATE PRA VIEW */
        $this->data['current_page'] 	= 'evento';
		$this->data['current_nav']	=	'calendario';
		$this->data['eve_slug']	=	$eve_slug;
		$this->loadtemplate( 'evento', false);
	}


	function processaStatusEvento($tablefield, $eve_slug, $statusEvento){

		$data	= $this->m_crud->get_rowSpecific('tb_site_evento', 'eve_slug', $eve_slug, 1, $tablefield);
		$ano 	= $this->my_date->datetime($data, 'justYear');
		$mes 	= $this->my_date->datetime($data, 'justMonth');
		$dia 	= $this->my_date->datetime($data, 'justDay');

		$this->data['counterAno']					=	$ano;
		$this->data['counterMes']					=	$mes - 1;
		$this->data['counterDia']					=	$dia + 1;
		if(!empty($data)){
			$this->data['counter']					=	TRUE;
		}


	}



	/*-----------------------------------------------	
	@ NOTICIAS
	-----------------------------------------------*/
	public function noticias()
	{
		$this->data['title_page']		=	'';

		$this->data['arrNoticias']	=	array(

			/*array(
				'thumb'=> '2.jpg',
				'titulo'=> 'Giovanna Lara de Freitas estreia em nova categoria na temporada 2015',
				'data'=> '25/07/2014',
				'descricao'=> 'A amazona cubatense Giovanna de Lara Freitas estreia na temporada 2015 em uma nova categoria. Depois de conquistar títulos em 2014, pela categoria mirim (12/14 anos), em saltos com obstáculos de 1,25 metros, aos 14 anos, ela passará a competir pela categoria pré-júnior (14/16 anos), para saltos de 1,30 metros. ',
			),
			array(
				'thumb'=> '3.jpg',
				'titulo'=> 'Barbacena será sede da abertura do Campeonato Mineiro de Hipismo',
				'data'=> '25/07/2014',
				'descricao'=> 'A cidade de Barbacena irá sediar neste ano a abertura do Campeonato Estadual de Hipismo. A disputa está agendada para os dias 13, 14 e 15 de março, no Parque de Exposições da cidade. De acordo com o coordenador técnico da Federação Hípica, Carlos Alberto Sá Grise, nos outros dois anos anteriores, a cidade sediou uma das etapas. Porém, pela primeira vez será palco da abertura do evento.',
			),*/
		);
		$this->data['current_page']	=	'noticias';
		$this->loadtemplate( 'noticias', true, 'noticias');
	}


    /************************************************** 
      NOTÍCIA
    ************************************************* */
    public function noticia($idNoticia) {

        $this->data['current_class'] 	= 'noticias';
		$this->data['current_page']	=	'noticias';
		
		$this->data['titulo']	=	'Nos EUA, Rodrigo Pessoa quer evitar hepta feminino na "Guera dos Sexos"';
		$this->data['idNoticia']	=	$idNoticia;
		$this->data['criado']	=	'0000-00-00 00:00:00';
		$this->data['conteudo']	=	'<p>O hipismo &eacute; a &uacute;nica modalidade ol&iacute;mpica em que homens e mulheres disputam a mesma prova, em igualdade de condi&ccedil;&otilde;es. Mas, na primeira semana do Festival de Inverno, que ser&aacute; realizada a partir da noite desta quarta-feira em Wellington, nos Estados Unidos, o duelo ser&aacute; entre os sexos. Um duelo entre homens e mulheres promete incendiar a competi&ccedil;&atilde;o, que contar&aacute; com a participa&ccedil;&atilde;o do brasileiro Rodrigo Pessoa, campe&atilde;o ol&iacute;mpico em 2004.&nbsp;Nos &uacute;ltimos seis anos, as mulheres levaram a melhor contra os homens no evento, que conta com uma premia&ccedil;&atilde;o de US$ 75 mil, cerca de R$ 180 mil.&nbsp;</p> <div class="foto componente-conteudo" id="3ecff07c-e57c-052a-22f6-5ebf639e3c5f" style="font-family: arial, helvetica, freesans, sans-serif; font-size: 12px; margin: 0px 25px 30px 0px; outline: 0px; padding: 0px; -webkit-font-smoothing: antialiased; border-radius: 3px; overflow: hidden; position: relative; zoom: 1; float: left; color: rgb(0, 0, 0); line-height: 12px; width: 690px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">></div> <table border="0" cellpadding="1" cellspacing="1"> <tbody> <tr> <td> <table border="0" cellpadding="1" cellspacing="1" style="line-height:20.7999992370605px"> <tbody> <tr> <td><img alt="Guerra dos Sexos - Hipismo saltos (Foto: Divulgação)" src="http://s2.glbimg.com/53TFQ5qKi-e17zxTqjMgIzWRHSo=/29x0:719x475/690x476/s.glbimg.com/es/ge/f/original/2015/01/07/hipismo_1.jpg" style="-webkit-font-smoothing:antialiased; background:transparent; border:0px; color:rgb(0, 0, 0); display:block; font-family:inherit; font-size:12px; height:476px; line-height:12px; margin:0px; outline:0px; padding:0px; width:690px" title="Guerra dos Sexos - Hipismo saltos (Foto: Divulgação)" /></td> </tr> </tbody> </table> </td> </tr> </tbody> </table> <p>O Festival Equestre de Inverno tem a dura&ccedil;&atilde;o de 12 semanas, com a participa&ccedil;&atilde;o de quase tr&ecirc;s mil cavaleiros de 31 pa&iacute;ses. O principal evento individual da primeira etapa do evento ser&aacute; o Grande Pr&ecirc;mio, no domingo, com a premia&ccedil;&atilde;o de US$ 30 mil, cerca de R$ 80 mil.&nbsp;</p> <div class="foto componente-conteudo" id="d76892da-b775-ff88-1711-5a39c074fd31" style="font-family: arial, helvetica, freesans, sans-serif; font-size: 12px; margin: 0px 25px 30px 0px; outline: 0px; padding: 0px; -webkit-font-smoothing: antialiased; border-radius: 3px; overflow: hidden; position: relative; zoom: 1; float: left; color: rgb(0, 0, 0); line-height: 12px; width: 300px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><img alt="Rodrigo Pessoa, Copa das Nações de Hipismo (Foto: Agência EFE)" src="http://s2.glbimg.com/tfJ3_PvPglywfl05YhqZHExi5w4=/0x21:950x759/300x233/s.glbimg.com/es/ge/f/original/2014/10/09/rodrigopessoa-hipismo-efe.jpg" style="-webkit-font-smoothing:antialiased; background:transparent; border:0px; display:block; font-family:inherit; height:233px; margin:0px; outline:0px; padding:0px; width:300px" title="Rodrigo Pessoa, Copa das Nações de Hipismo (Foto: Agência EFE)" /><span style="font-family:inherit"><span style="font-size:10px"><strong>Rodrigo Pessoa &eacute; um dos favoritos ao t&iacute;tulo</strong><br /> (Foto: Ag&ecirc;ncia EFE)</span></span> <hr /></div> <p>Rodrigo Pessoa &eacute; um dos favoritos ao t&iacute;tulo da competi&ccedil;&atilde;o. No ano passado, o campe&atilde;o ol&iacute;mpico venceu cinco provas ao longo da competi&ccedil;&atilde;o na Fl&oacute;rida, incluindo o Grande Pr&ecirc;mio disputado na nona semana do Festival.</p> <p>&Eacute; o primeiro passo de Rodrigo no ano de 2015, importante pois o brasileiro participar&aacute; dos Jogos Pan-Americanos e de algumas das principais competi&ccedil;&otilde;es internacionais. No ano passado, ele ajudou a sele&ccedil;&atilde;o nacional a terminar em quinto lugar os Jogos Equestres.</p>';
		$this->data['comentarios']	=	true;

		$this->data['noticias']	=	array(
			array(
				'not_thumb'=> '3.jpg',
				'not_thumb'=> '3.jpg',
				'not_titulo'=> 'Barbacena será sede da abertura do Campeonato Mineiro de Hipismo',
				'criado'=> '25/07/2014',
				'descricao'=> 'A cidade de Barbacena irá sediar neste ano a abertura do Campeonato Estadual de Hipismo. A disputa está agendada para os dias 13, 14 e 15 de março, no Parque de Exposições da cidade. De acordo com o coordenador técnico da Federação Hípica, Carlos Alberto Sá Grise, nos outros dois anos anteriores, a cidade sediou uma das etapas. Porém, pela primeira vez será palco da abertura do evento.',
			),
		);


		$this->loadtemplate( 'noticia', true, 'noticias');
    }


	/*-----------------------------------------------	
	@ BLOG
	// $slugTipoPagina	= pode ser 'noticias' ou 'galerias'
	// $slugCategoria	= categoria selecionada de alguma pagina
	-----------------------------------------------*/
	public function blog($slugTipoPagina=null, $slugCategoria=null)
	{

		// $idTipoPagina = retorna 1(noticias) ou 2(galerias).
		$idTipoPagina	=	$this->m_crud->get_rowSpecific('tb_site_blog_tipo', 'tip_slug', $slugTipoPagina, 1, 'tip_id');

		/*
			$idTipoCategoria é uma variavel que servirá para indicar em qual tabela a
			query deverá ser feita, se será na categoria ou na modalidade
		*/
		$query	=	$this->m_crud->get_rowSpecificTwoParameters('tb_site_blog_categoria', 'fk_tip_id', $idTipoPagina, 'cat_slug', $slugCategoria, 1, 'cat_categoria');

		if(empty($slugCategoria)){
			$query		=	$this->m_crud->get_allWhereOrderby('tb_site_blog', 'fk_tip_id', $idTipoPagina, 'blo_id', 'desc')->result();
			$slugCategoria = NULL;
		}
		else{
			if(is_null($query))
			{
				$idTipoCategoria 	= 	1;	//modalidade
				$idModalidade 		=	$this->m_crud->get_rowSpecific('tb_site_modalidade', 'mod_slug', $slugCategoria, 1, 'mod_id');
				$query				=	$this->m_crud->get_allWhereTwoParametersOrderby('tb_site_blog', 'fk_tip_id', $idTipoPagina, 'fk_mod_id', $idModalidade, 'blo_id', 'desc');
				$str = $this->db->last_query();
			}
			else
			{
				$idTipoCategoria 	= 	2;	//categoria
				$idCategoria 		=	$this->m_crud->get_rowSpecificTwoParameters('tb_site_blog_categoria', 'fk_tip_id', $idTipoPagina, 'cat_slug', $slugCategoria, 1, 'cat_id');
				$query				=	$this->m_crud->get_allWhereTwoParametersOrderby('tb_site_blog', 'fk_tip_id', $idTipoPagina, 'fk_cat_id', $idCategoria, 'blo_id', 'desc');
				$str = $this->db->last_query();
			}
		}


		// Tratamento de caso quando a categoria for nula
		/*if(empty($slugCategoria)) 
		{
			redirect( base_url() . $slugTipoPagina . '/geral');
		}*/

		/* ARRAY DAS CATEGORIAS */
		$categoriasSidebar						=	$this->db->query("SELECT * FROM tb_site_blog_categoria WHERE fk_tip_id=$idTipoPagina AND cat_slug<>'geral'")->result();
		$this->data['categoriasSidebar']		=	$categoriasSidebar;



		// #####################################################
		// Tratamento caso quando slugcategoria não existir
		// #####################################################
		//
		//

		// NOTICIAS
		if($slugTipoPagina=='noticias')
		{	
			$tipoPagina			=	'Notícias';
		}


		// GALERIAS
		if($slugTipoPagina=='galerias')
		{	
			$tipoPagina			=	'Galerias';
		}


		// PROCURA EM CATEGORIAS, SE NÃO ACHAR PROCURA EM MODALIDADES
		$tituloCategoria	=	$this->m_crud->get_rowSpecific('tb_site_blog_categoria', 'cat_slug', $slugCategoria, 1, 'cat_categoria');
		if(is_null($tituloCategoria))
		{
			$tituloCategoria	=	$this->m_crud->get_rowSpecific('tb_site_modalidade', 'mod_slug', $slugCategoria, 1, 'mod_modalidade');
		}
		$this->data['tituloCategoria']		=	$tituloCategoria;


		// SLUGS
		$this->data['slugTipoPagina']		=	$slugTipoPagina;
		$this->data['slugCategoria']		=	$slugCategoria;


		// DADOS
		$this->data['arrData']				=	$query;
		$this->data['tipoPagina']			=	$tipoPagina;
	

		// CARREGA VIEW		
		$this->loadtemplate( 'blog', true, 'blog' );
	}


	/**
	* Metodo responsavel por exibir um post especifico do blog.
	* @param string @slugTipoPagina - pode receber dois valores: 'noticias' ou 'galerias'
	* @param string @slugCategoria - recebe o slug da categoria referente ao post
	* @param string @slugPost - recebe o slug do post selecionado
	*/
	public function blogitem($slugTipoPagina=null, $slugCategoria=null, $slugPost=null)
	{

		// $idTipoPagina = retorna 1(noticias) ou 2(galerias).
		$idTipoPagina	=	$this->m_crud->get_rowSpecific('tb_site_blog_tipo', 'tip_slug', $slugTipoPagina, 1, 'tip_id');


		// Identificando se a categoria é uma categoria ou modalidade
		$tipoSlugCategoria  =  $this->m_crud->get_rowSpecific('tb_site_blog_categoria', 'cat_slug', $slugCategoria, 1, 'cat_id'); 
		if(is_null($tipoSlugCategoria)){
			// MODALIDADE
			$var	=	 'modalidade';
		}else{
			// CATEGORIA
			$var	=	 'categoria';
		}


		/* ARRAY DAS CATEGORIAS */
		$categoriasSidebar						=	$this->db->query("SELECT * FROM tb_site_blog_categoria WHERE fk_tip_id=$idTipoPagina AND cat_slug<>'geral'")->result();
		$this->data['categoriasSidebar']		=	$categoriasSidebar;


		// PROCURA EM CATEGORIAS, SE NÃO ACHAR PROCURA EM MODALIDADES
		$tituloCategoria	=	$this->m_crud->get_rowSpecific('tb_site_blog_categoria', 'cat_slug', $slugCategoria, 1, 'cat_categoria');
		if(is_null($tituloCategoria))
		{
			$tituloCategoria	=	$this->m_crud->get_rowSpecific('tb_site_modalidade', 'mod_slug', $slugCategoria, 1, 'mod_modalidade');
		}
		$this->data['tituloCategoria']		=	$tituloCategoria;


		// QUERY
		$query 	=	$this->m_crud->get_allWhereTwoParameters('tb_site_blog', 'blo_slug', $slugPost, 'fk_tip_id', $idTipoPagina);
		$this->data['query']	=	$query;

		// SE O POST NÃO EXISTIR REDIRECIONA PARA O TIPO E A CATEGORIA QUE FOI RECEBIDO NA URL
		if(empty($query))
		{
			redirect(base_url() . $slugTipoPagina . '/' . $slugCategoria);
		}


		// SLUGS
		$this->data['slugTipoPagina']		=	$slugTipoPagina;
		$this->data['slugCategoria']		=	$slugCategoria;
		$this->data['slugPost']				=	$slugPost;

		// id categoria
		$idPost 		= $this->m_crud->get_rowSpecificTwoParameters('tb_site_blog', 'fk_tip_id', $idTipoPagina, 'blo_slug', $slugPost, 1, 'blo_id');

		// RELACIONADAS
		// TRATAR CATEGORIA/MODALIDADE

		if($var=='categoria'){
			$idCategoria 	= $this->m_crud->get_rowSpecificTwoParameters('tb_site_blog_categoria', 'fk_tip_id', $idTipoPagina, 'cat_slug', $slugCategoria, 1, 'cat_id');
			$this->data['postsRelacionados']	=	$this->db->query("SELECT * FROM tb_site_blog
																	WHERE fk_tip_id=$idTipoPagina AND
																	fk_cat_id=$idCategoria AND
																	blo_id<>$idPost ORDER BY blo_id DESC LIMIT 3")->result();
		}else{
			$idModalidade 	= $this->m_crud->get_rowSpecific('tb_site_modalidade', 'mod_slug', $slugCategoria, 1, 'mod_id');
			$this->data['postsRelacionados']	=	$this->db->query("SELECT * FROM tb_site_blog
																	WHERE fk_tip_id=$idTipoPagina AND
																	fk_mod_id=$idModalidade AND
																	blo_id<>$idPost ORDER BY blo_id DESC LIMIT 3")->result();
		}


		// TIPO PAGINA ( breadcrumbs )
		if($slugTipoPagina=='noticias')
		{
			$this->data['tipoPagina']			=	'Notícias';
		}
		else
		{
			$this->data['arrFotos']				=	$this->m_crud->get_allWhere('tb_site_blog_foto', 'fk_blo_id', $idPost);
			$this->data['tipoPagina']			=	'Galerias';
		}


		// CARREGA VIEW		
		$this->loadtemplate( 'blogitem', true, 'blog' );
	}

	

	/**
	 * Contato - Metodo responsavel por exibir a pagina de contato
	 */
	public function contato()
	{

		/**
		* Setando variaveis que serao utilizadas para exibir o googlemaps na pagina
		*/
		$this->data['global_map_title']			=	'<b>Federação Hípica de Brasília.</b>';
		$this->data['global_map_content']		=	'Setor Hípico - Lote 08 - Brasília - DF<br/>Próximo ao Jardim Zoológico de Brasília<br>';
		$this->data['global_map_latitude']		=	'-15.835553';
		$this->data['global_map_longitude']		=	'-47.942326';
		 

		/**
		* Como nos encontrou - Realizando a consulta no banco e armazenando no array data para enviar para view
		*/
		$comonosencontrou 		=	$this->m_crud->get_allLimitOrderby('tb_site_contato_comonosencontrou', 20, 'com_posicao', 'asc');
		$this->data['comonosencontrou']	=	$this->montarDropdown($comonosencontrou, 'Selecione uma opção', 'com_id', 'com_titulo');


		/**
		* Departamentos - Realizando a consulta no banco e armazenando no array data para enviar para view
		*/
		$departamentos 		=	$this->m_crud->get_allOrderby('tb_site_contato_departamento', 'dep_posicao', 'asc');
		$this->data['departamentos']	=	$this->montarDropdown($departamentos, 'Selecione uma opção', 'dep_id', 'dep_departamento');


		/**
		* Setando variaveis que serao utilizadas para exibir o googlemaps na pagina
		*/
		$this->data['title_page']		=	'';
		$this->data['current_page']		=	'contato';


		/**
		* Setando variaveis que serao utilizadas para exibir o googlemaps na pagina
		*/
		$this->loadtemplate( 'contato' );

	}


	/**
	 * Metodo responsavel por carregar as view do template
	 * @param string @pathView - recebe qual arquivo deve ser carregado. Caso o arquivo nao esteja na raiz da pasta view do module site, informar o diretorio antes de informar o arquivo
	 */
	protected function loadTemplate( $pathView, $sidebar=false, $sidebar_type=false )
	{
		$this->load->view('header', $this->data);
		$this->load->view( $pathView );

		if($sidebar==true)
		{
			$sidebar_type = 'sidebar_' . $sidebar_type;
			$this->load->view($sidebar_type);
		}
		
		$this->load->view('footer');
	}



	/**
	 * Montar dropdown
 	 * @param array @dados
 	 * @param string @primeiroOption
 	 * @param string @indice
 	 * @param string @valor
 	 *
	 */
	function montarDropdown($dados, $primeiroOption, $indice, $valor)
	{
		$arrAux = array(''=>$primeiroOption);
		foreach ($dados as $key => $value) {
			$arrAux[$value->$indice]	=	$value->$valor;
		}
		return $arrAux;
	}




	/**
	 * O metodo environmentVariables() e chamado pelo metodo construtor da classe
	 * E nesse metodo onde sao setadas as variaveis que serao utilizadas em todo template
	 */
	public function environmentVariables(){

		$this->data['current_nav']				=	'';
		$this->data['title_page']				=	'';
		$this->data['use_wizard']				=	'';
		$this->data['use_mask']					=	'';
		$this->data['use_owlslider']			=	'';
		$this->data['use_maps']					=	'';
		
		$this->data['global_map']				=	FALSE;
		$this->data['global_map_title']			=	'Federação Hípica de Brasília';
		$this->data['global_map_content']		=	'<b>Federação Hípica de Brasília.</b><br/> Setor Hípico - Lote 08 - Brasília - DF<br/>Em frente ao Jardim Zoológico de Brasília<br>';
		$this->data['global_map_latitude']		=	48.852722;
		$this->data['global_map_longitude']		=	2.348527;

		$this->data['evento_countdown']			=	'';
		$this->data['counterAno']					=	NULL;
		$this->data['counterMes']					=	NULL;
		$this->data['counterDia']					=	NULL;
		$this->data['counter']						=	FALSE;


		/*
			Banner Publicitários
		*/
		$this->data['banners']		=	$this->m_crud->get_allLimitOrderby('tb_site_banner', 1, 'ban_id', 'random');
		$this->data['parceiros']	=	$this->m_crud->get_allLimitOrderby('tb_site_parceiro', 20, 'par_posicao', 'asc');


		$this->data['enquete_id_pergunta']  =	$this->m_crud->get_rowSpecific('tb_site_enquete_pergunta', 'per_status', 1, 1, 'per_id');
		$this->data['enquete_pergunta']		=	$this->m_crud->get_rowSpecific('tb_site_enquete_pergunta', 'per_status', 1, 1, 'per_pergunta');




		/*
			Modalidades são usadas em todo o site:
			- Menu
			- Listagem de modalidades na index
			- Caledario
			- Noticias
			- Galerias
		*/
		$this->data['slugTipoPagina']		=	NULL;
		$this->data['slugCategoria']		=	NULL;
		$this->data['modalidades']				=	$this->m_crud->get_allOrderby('tb_site_modalidade', 'mod_modalidade', 'asc');
		$this->data['categoriasDeNoticias']		=	$this->db->query("SELECT * FROM tb_site_blog_categoria WHERE fk_tip_id=1 AND cat_slug<>'geral'")->result();
		$this->data['categoriasDeGalerias']		=	$this->db->query("SELECT * FROM tb_site_blog_categoria WHERE fk_tip_id=2 AND cat_slug<>'geral'")->result();

        $this->data['current_page'] 	= NULL;


		$this->data['entidadesescolas_rodape']	=	$this->db->query("SELECT * FROM tb_site_entidadeescola WHERE ent_flag_entidade=1 AND ent_logo<>'nophoto.jpg' AND ent_status=1 ")->result();

	}



	/**
	 * Metodo responsavel por realizar o processamento dos dados submetidos
	 * na enquete
	 */
	public function processaVoto()
	{
		/*
		1ª Ler variaveis recebidas
		*/
		$idPergunta 			=	$_POST['idPergunta'];
		if(isset($_POST['idRespostaSelecionada'])){
			$idRespostaSelecionada 	= 	$_POST['idRespostaSelecionada'];
		}
		$idBtnSelecionado 		=	$_POST['idBtnSelecionado'];


		/*
		2ª Verificar se já existe cookie.
		*/
		$nomeDoCookie = 'cookie_enquete_'.$idPergunta;
		if (!get_cookie($nomeDoCookie)) {
		    /* 	
			    Cookie nao registrado, retorna false
		    */

			// Inserindo voto no banco
			$dados = array(
				'fk_res_id'	=> $idRespostaSelecionada,
				'vot_ip'	=> $_SERVER['REMOTE_ADDR'],
				'criado'	=> date("Y-m-d H:i:s")
			);
			$this->m_crud->insert('tb_site_enquete_voto', $dados);

			// Setando Cookie
			$cookie = array(
			    'name'   => $nomeDoCookie,
			    'value'  => $this->my_date->datetime(date("Y-m-d H:i:s"), 'justDate') . 'x' . $_SERVER['REMOTE_ADDR'],
			    'expire' => 86500,
			    'secure' => false
			);
			$this->input->set_cookie($cookie); 

			// Retornando dados por json
			$array = array(
				'status'=>  'voto_computado',
				'btn-origem'=> $idBtnSelecionado
			);
			sleep(2);
			echo json_encode($array);
		}
		else{
			/*
			 Cookie registrado
			 */
			$cookieRegistrado = $this->input->cookie($nomeDoCookie, TRUE);
			$spiceCookieRegistrado = explode('x', $cookieRegistrado);
			$array = array(
				'status'			=>  'voto_nao_computado',
				'datahora_origem'	=>  $spiceCookieRegistrado[0],
				'ip_origem'			=>  $spiceCookieRegistrado[1],
				'btn_origem'		=> $idBtnSelecionado
			);
			sleep(2);
			echo json_encode($array);
		}


	}




}
/* END CLASS */

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */



