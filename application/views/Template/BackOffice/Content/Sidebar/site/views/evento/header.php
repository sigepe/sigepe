<!-- BEGIN Evento -->
<div class="col-sm-12 padding-left-0 page_evento">
	

	<!-- Breadcrumb
	============================================= -->
  	<ul class="breadcrumb">
	    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a></li>
	    <li><a href="<?php echo base_url() . 'calendario'; ?>">Calendário</a></li>
	    <li class="active"><?php echo strip_tags($evento->eve_evento); ?></li>
	</ul>	


	<h1>{eve_evento}</h1>


    <!-- Info Header
    ============================================= -->
    <?php if($global_map==true): ?>
	<div class="row infoheader">
		<div class="col-sm-12 local"><b>Local:</b> <?php echo strip_tags($global_map_content); ?></div>
		<div class="col-sm-12 vernomapa"><a href="#ancora-localizacao" class="locationpage" title=""><i class="fa fa-map-marker"></i> Confira localização no Mapa</a></div>
	</div>	
	<?php endif; ?>


	<div class="gridline"></div>


    <!-- Botoes Header
    ============================================= -->
	<div class="row ">

		<div class="col-sm-12 btn_cabecalho">

			<!-- PROGRAMA -->
			<?php if($programa): ?>
			<a href="<?php echo base_url() . 'uploads/evento/programa/' . $evento->eve_programa; ?>" class="btn btn-default" target="_blank"> Programa </a> <?php endif; ?>

			<!-- ADENDO -->
			<?php if($adendo): ?>
			<a href="<?php echo base_url() . 'uploads/evento/adendo/' . $evento->eve_adendo; ?>" target="_blank" class="btn btn-primary" target="_blank"> Adendo </a> <?php endif; ?>

			<!-- ORDENS DE ENTRADA -->
			<?php if($ordensDeEntrada): ?> <a href="#ordensresultados" class="btn btn-info locationpage"> Ordens de Entrada </a> <?php endif; ?>

			<!-- QUADRO DE HORARIOS -->
			<?php if($eve_quadrohorarios): ?>
			<a href="<?php echo base_url() . 'uploads/evento/quadrodehorario/' . $evento->eve_quadrohorarios; ?>" class="btn btn-primary" target="_blank">Quadro de Horários</a><?php endif; ?>

			<!-- RESULTADOS -->
			<?php if($resultados): ?>
				<?php if($eve_resultado==0): ?><a href="#provas" class="btn btn-warning locationpage">Resultados</a><?php endif; ?>
				<?php if($eve_resultado==1): ?><a href="<?php echo base_url() . 'uploads/evento/resultado/' . $evento->eve_resultado; ?>" class="btn btn-warning" target="_blank">Resultados</a><?php endif; ?>
			<?php endif; ?>

			<!-- PROVAS -->
			<?php if($provas): ?>
			<a href="#provas" class="btn btn-info locationpage">
				Provas
			</a>
			<?php endif; ?>

		</div>

	</div>


	<div class="gridline"></div>




