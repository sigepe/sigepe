<!-- BEGIN LEFT SIDEBAR -->            
<div class="col-md-9 col-sm-9 blog-posts padding-left-0" id="page_blog">
  
  <ul class="breadcrumb visible-md visible-lg">
      <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a></li>
      <li><a href="<?php echo base_url() . $slugTipoPagina; ?>"><?php echo $tituloPagina; ?></a></li>
      <li class="active"><?php echo $tituloCategoria; ?></li>
  </ul> 


  <h1><?php echo $tituloPagina ?></h1>


  <!-- BEGIN Loop -->
  <?php foreach ($arrData as $item): ?>


  <?php


    if(is_null($item->fk_mod_id)){
      // CATEGORIA
      $slugCategoria = $this->m_crud->get_rowSpecific('tb_blog_categoria', 'cat_id', $item->fk_cat_id, 1, 'cat_slug');
    }
    else{
      // MODALIDADE
      $slugCategoria = $this->m_crud->get_rowSpecific('tb_modalidade', 'mod_id', $item->fk_mod_id, 1, 'mod_slug');
    }

    if(!is_null($item->blo_flagLinkExterno) && $item->blo_flagLinkExterno == 1){
      $link   =   $item->blo_link_externo;
      $target =   '_blank';
    }else{
      $link   =   base_url() . $slugTipoPagina . '/' . $slugCategoria . '/' . $item->blo_slug;
      $target =   '_self';
    }

  ?>


  <div class="row">
    <div class="col-md-4 col-sm-4">
      <a href="<?php echo $link; ?>" target="<?php echo $target; ?>">
        <img class="img-responsive" alt="<?php echo $item->blo_titulo; ?>" src="<?php echo base_url() . 'manager/uploads/blog/thumbs/' . $item->blo_thumb; ?>" />
      </a>
    </div>
    <div class="col-md-8 col-sm-8">
      <h2>
        <a href="<?php echo $link; ?>" target="<?php echo $target; ?>">
          <?php echo $item->blo_titulo; ?>
        </a>
      </h2>
      <ul class="blog-info">
        <li><i class="fa fa-calendar"></i> <?php echo $this->my_date->datetime($item->criado, 'justDate'); ?></li>
      </ul>
      <p><?php echo $item->blo_descricao_curta; ?></p>
    </div>
  </div>
  <hr class="blog-post-sep">

  <?php endforeach; ?>
  <!-- END Loop -->




  <?php if(empty($arrData)): ?>
  Não existe nenhum item cadastrado para essa categoria.
  <?php endif; ?>


<!--       
  <hr class="blog-post-sep">
  <ul class="pagination">
    <li><a href="#">Prev</a></li>
    <li><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li class="active"><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li><a href="#">Next</a></li>
  </ul>                -->

</div>
<!-- END LEFT SIDEBAR -->

