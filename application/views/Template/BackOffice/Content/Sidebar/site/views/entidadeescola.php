<!-- BEGIN Historia -->
<div class="col-sm-9 padding-left-0 federacao" id="federacao_itemEntidadesEscolas">
	
	<ul class="breadcrumb">
	    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a></li>
	    <li><a href="<?php echo base_url() . 'a-federacao/historia'; ?>">A Federação</a></li>
	    <li><a href="<?php echo base_url() . 'a-federacao/' . $slugTipoPagina; ?>"><?php echo $tituloTipoPagina; ?></a></li>
	    <li class="active"><?php echo $nomeEntidadeEscola; ?></li>
	</ul>	

	<h1><?php echo $nomeEntidadeEscola; ?></h1>
	
	<div class="conteudo">

		<div class="col-sm-4 thumb">
			<div class="item">
				<img src="<?php echo base_url() . 'manager/uploads/entidade/thumbs/' . $query->ent_logo; ?>" alt="" class="img-responsive" style="width:200px;" />
			</div>
			<?php if(!empty($query->ent_eixox) && !empty($query->ent_eixoy)): ?>
			<a href="#ancora-localizacao" class="btn btn-block btn-success scrolltop"><span class="glyphicon glyphicon-map-marker"></span> Confira localização no mapa</a>
			<?php endif; ?>
		</div>

		<div class="col-sm-8 box">

			<ul>
				<li class="descricao">
					<?php
					if($slugTipoPagina=='entidades-filiadas'){
						echo $query->ent_entidade_descricao;
						if(empty($query->ent_entidade_descricao))
							echo "Conteúdo indisponível.";
					}
					else{
						echo $query->ent_escola_descricao;						
						if(empty($query->ent_escola_descricao))
							echo "Conteúdo indisponível.";
					}

					?>
				</li>
				<hr />
				
				<!-- SITE -->
				<li class="site margin-top-20">
					<b>Site:</b>
					<br />
					<?php
					if(empty($query->ent_site))
							echo "Conteúdo indisponível.";
					?>
					<a href="<?php echo $query->ent_site; ?>" title="<?php echo $query->ent_titulo; ?>" target="_blank"><?php echo $query->ent_site; ?>
					</a>
				</li>
			
				<hr />

				<li class="endereco margin-top-20">
					<b>Endereço:</b>
					<br />
					<?php
					if(empty($query->ent_endereco))
							echo "Conteúdo indisponível.";
					?>
					<?php echo $query->ent_endereco; ?>
				</li>
				
				<hr />
				
				<li class="telefone margin-top-20">
					<b>Telefone:</b>
					<br />
					<?php
					if(empty($query->ent_telefone))
							echo "Conteúdo indisponível.";
					?>
					<?php echo $query->ent_telefone; ?>​
				</li>
				
				<hr />
				
				<li class="email margin-top-20">
					<b>E-mail:</b>
					<br />
					<?php
					if(empty($query->ent_email))
							echo "Conteúdo indisponível.";
					?>
					<?php echo $query->ent_email; ?>
				</li>

				<hr />

			</ul>


			<?php if(!empty($fotos)): ?>
			<div class="fotos">

				<h4>Fotos</h4>

				<?php foreach ($fotos as $foto): ?>
                <div class="col-md-3 col-sm-3 gallery-item">
                  <a data-rel="fancybox-button" title="" href="<?php echo base_url() . 'manager/uploads/entidade/galerias/' . $query->ent_folder . '/' . $foto->fot_thumb; ?>" class="fancybox-button">
  	               
  	               	<?php $urlRelativoFoto 	= './manager/uploads/entidade/galerias/' . $query->ent_folder . '/' . $foto->fot_thumb; ?>
  	               	<?php $urlDiretorioFoto = base_url() . 'manager/uploads/entidade/galerias/' . $query->ent_folder . '/' ; ?>
                    <img src="<?php echo $urlDiretorioFoto . thumb($urlRelativoFoto, 200, 200); ?>" class="img-responsive" />
                   
                    <div class="zoomix"><i class="fa fa-search"></i></div>
                  </a> 
                </div>
	            <?php endforeach; ?>

			</div>
			<?php endif; ?>

			<div class="col-sm-12 no-padding">
				<?php if(!empty($query->ent_eixox) && !empty($query->ent_eixoy)): ?>
				<a href="#ancora-localizacao" class="btn btn-block btn-success scrolltop"><span class="glyphicon glyphicon-map-marker"></span> Confira localização no mapa</a>
				<?php endif; ?>
			</div>

		</div>

	</div>

</div>
<!-- END Historia -->
