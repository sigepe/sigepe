<!-- BEGIN Contato -->
<div class="col-sm-12 padding-left-0" id="page_contato">
	
	<ul class="breadcrumb">
	    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a></li>
	    <li class="active">Contato</li>
	</ul>	

	<h1>Contato</h1>

	<!-- BEGIN Formulario -->
	<div class="col-sm-7 padding-right-25" id="formulario">

		<form id="formContato" method="post" >

			<!-- NOME -->
			<div class="row"> 
				<div class="col-sm-3">
					<label>Nome:</label>
				</div>
				<div class="col-sm-9">
					<input type="text" class="form-control rounded-4" placeholder="Digite seu nome" name="formNome" />
				</div>			
			</div>

			<!-- E-MAIL -->
			<div class="row"> 
				<div class="col-sm-3">
					<label>E-mail:</label>
				</div>
				<div class="col-sm-9">
					<input type="text" class="form-control rounded-4" placeholder="Digite seu e-mail" name="formEmail">
				</div>			
			</div>

			<!-- TELEFONE -->
			<div class="row"> 
				<div class="col-sm-3">
					<label>Telefone:</label>
				</div>
				<div class="col-sm-9">
					<div class="col-sm-5 padding-left-0">
						<input type="tel" class="form-control rounded-4 sp_celphones" placeholder="Digite seu telefone"  name="formTelefone">
					</div>
					<div class="col-sm-2 text-right padding-left-0">
						<label>Celular:</label>
					</div>	
					<div class="col-sm-5 no-padding">
						<input type="tel" class="form-control rounded-4 sp_celphones" placeholder="Digite seu celular"  name="formCelular">
					</div>
				</div>			
			</div>


			<!-- COMO NOS ENCONTROU -->
			<div class="row"> 
				<div class="col-sm-3">
					<label class="margin-top--5">Como nos encontrou:</label>
				</div>
				<div class="col-sm-9">
					<?php echo form_dropdown('formComoNosEncontrou', $comonosencontrou, '', 'class="form-control"'); ?>
				</div>			
			</div>

			<!-- ASSUNTO -->
			<div class="row"> 
				<div class="col-sm-3">
					<label>Assunto:</label>
				</div>
				<div class="col-sm-9">
					<?php echo form_dropdown('formDepartamento', $departamentos, '', 'class="form-control"'); ?>
				</div>			
			</div>

			<!-- FILIADO -->
			<div class="row"> 
				<div class="col-sm-3">
					<label>Filiado:</label>
				</div>
				<div class="col-sm-9">
					<label class="radio-inline">
						<input type="radio" name="formFiliado" id="inlineRadio1" value="sim"> Sim
					</label>
					<label class="radio-inline">
						<input type="radio" name="formFiliado" id="inlineRadio2" value="nao"> Não
					</label>
				</div>			
			</div>

			<!-- MENSAGEM -->
			<div class="row"> 
				<div class="col-sm-3">
					<label>Mensagem:</label>
				</div>
				<div class="col-sm-9">
					<textarea class="form-control" rows="5" name="formMensagem"></textarea>
				</div>			
			</div>

			<!-- SUBMIT -->
			<div class="row"> 
				<div class="col-sm-3"></div>
				<div class="col-sm-9">
					<a href="#" class="btn btn-success submit">Enviar mensagem</a>
				</div>			
			</div>

			<input type="hidden" name="keySubmit" value="keySubmit" />

		</form>

	</div>
	<!-- END Formulario -->


	<!-- BEGIN Informacoes -->
	<div class="col-sm-5" id="informacoes">
		
		<!-- 
		<div class="note note-info col-sm-12">
			<h4 class="block">Dica:</h4>
			<p>
			 Se voce for uma entidade filiada, escola de equitação, atleta ou propritário de animal, dê preferência para fazer contato pelo Portal FHBr.
			</p>
		</div>
		-->

		<div class="row telefone">
			<div class="col-sm-12">
				<span>61</span> 3245-5870
			</div>
		</div>

		<div class="row telefone">
			<div class="col-sm-12">
				<span>61</span> 3445-1864
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12 atendimento">
				Atendimento de terça a sexta-feira das 08h às 18h e sábado das 08h às 12h.
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12 email">
				<i class="fa fa-envelope-o"></i>
				contato@fhbr.com.br
			</div>
		</div>

		<div class="row redessociais">
			<div class="col-sm-12">
				<div class="title_template margin-bottom-20" id="title">NOSSAS <b>REDES</b></div>
			</div>
		</div>

		<div class="row redessociais_item">
			<div class="col-sm-12 ">
				<a href="https://www.facebook.com/pages/Federa%C3%A7%C3%A3o-H%C3%ADpica-de-Bras%C3%ADlia/914503305251147" title="Facebook" target="_blank">
					<div class="col-sm-2"><i class="fa fa-facebook"></i></div>
					<div class="col-sm-10">Facebook</div>
				</a>
			</div>
		</div>

		<div class="row redessociais_item">
			<div class="col-sm-12">
				<a href="https://instagram.com/fhbroficial/" title="Instagram" target="_blank">
					<div class="col-sm-2"><i class="fa fa-instagram"></i></div>
					<div class="col-sm-10">Instagram</div>
				</a>
			</div>
		</div>

	</div>
	<!-- END Informacoes -->

</div>
<!-- END Contato -->

</div>
<!-- END Container -->

		<!-- MAPA -->
		<div class="localizacao col-sm-12" id="ancora-localizacao" style="padding-top:25px;position:relative;z-index:200;">
			<div class="container">
				<div class="col-sm-1"><i class="fa fa-map-marker"></i></div>
				<div class="col-sm-11 red-title">
					<span class="titulo">Venha nos visitar</span>
					
					<span class="local">
						Setor Hípico - Lote 08 - Em frente ao Jardim Zoológico de Brasília / Brasília - DF
					</span>
				</div>
			</div>
		</div>

		<div class="col-sm-12 no-padding" style="position:relative;z-index:100;">
          <div id="map" class="gmaps" style="height:400px;"></div>
        </div>

<div class="container">
