
<!-- BEGIN Sidebar -->
<div class="col-sm-3 federacao_sidebar">

	<h2 class="no-top-space"><?php echo $tituloModalidade; ?></h2>
	<ul class="nav sidebar-categories margin-bottom-40">

		<?php $contador = 0; ?>
		<?php foreach ($paginas as $pagina): ?>

			<?php
				if($slugPagina==$pagina->cat_slug)
				{
					$navPaginaAtivo	=	"active";
				}
				else
				{
					$navPaginaAtivo	=	"";
				}
			?>

			<?php if($contador==1): ?>

			<li>
				<a href="<?php echo base_url() . 'noticias/' . $slugModalidade; ?>" title="Notícias da modalidade: <?php echo $tituloModalidade; ?>">
					Notícias
				</a>
			</li>

			<li>
				<a href="<?php echo base_url() . 'galerias/' . $slugModalidade; ?>" title="Galerias da modalidade: <?php echo $tituloModalidade; ?>">
					Galerias
				</a>
			</li>

			<?php endif; ?>


			<?php
				if($pagina->cat_id==10 ){

					if($slugModalidade=='paraequestre' || $slugModalidade=='adestramento')
					{

			?>
					<li class="<?php echo $navPaginaAtivo; ?>">
						<a href="<?php echo base_url() . 'modalidade/' . $slugModalidade . '/' . $pagina->cat_slug; ?>" title="<?php echo $tituloModalidade . ' - ' . $pagina->cat_titulo; ?>">
							<?php echo $pagina->cat_titulo; ?>
						</a>
					</li>

			<?php 
					}

				}
				else{
			?>

			<li class="<?php echo $navPaginaAtivo; ?>">
				<a href="<?php echo base_url() . 'modalidade/' . $slugModalidade . '/' . $pagina->cat_slug; ?>" title="<?php echo $tituloModalidade . ' - ' . $pagina->cat_titulo; ?>">
					<?php echo $pagina->cat_titulo; ?>
				</a>
			</li>


			<?php } ?>


		<?php $contador++; ?>
		<?php endforeach; ?>


	</ul>
	<!-- CATEGORIES END -->

	<h2 class="no-top-space">Modalidades</h2>
	<ul class="nav sidebar-categories margin-bottom-40">

		<?php foreach ($modalidades as $modalidade): ?>

			<?php
				if($slugModalidade==$modalidade->mod_slug)
				{
					$navModalidadeAtivo	=	"active";
				}
				else
				{
					$navModalidadeAtivo	=	"";
				}
			?>

			<li class="<?php echo $navModalidadeAtivo; ?>">
				<a href="<?php echo base_url() . 'modalidade/' . $modalidade->mod_slug; ?>" title="<?php echo $modalidade->mod_modalidade; ?>">
					<?php echo $modalidade->mod_modalidade; ?>
				</a>
			</li>

		<?php endforeach; ?>

	</ul>
	<!-- CATEGORIES END -->



</div>
<!-- END Sidebar -->

