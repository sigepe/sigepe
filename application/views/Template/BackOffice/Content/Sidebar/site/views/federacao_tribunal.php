<!-- BEGIN Historia -->
<div class="col-sm-9 padding-left-0 federacao" id="federacao_tribunal">
	
	<ul class="breadcrumb">
	    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a></li>
	    <li><a href="<?php echo base_url() . 'afederacao/historia'; ?>">A Federação</a></li>
	    <li class="active">Tribunal de Justiça Desportiva</li>
	</ul>	

	<h1>Tribunal de Justiça Desportiva</h1>
	
	<div class="conteudo margin-bottom-30">
		This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, “Lorem ipsum dolor sit amet..”, comes from a line in section 1.10.32.
		The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from “de Finibus Bonorum et Malorum” by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.or those interested. Sections 1.10.32 and 1.10.33 from “de Finibus Bonorum et Malorum” by Cicero.
	</div>

	<div class="gestao">
		<h3><b>Atual Gestão</b></h3>
		<h4><b>Gestão:</b> Triênio 2015/2017</h4>
        <table class="tabela no-margin margin-top-20" cellspacing='0'>
            <tr><th>Cargo</th><th>Nome</th></tr><!-- Table Header -->
            <tr class=""><td>Presidente</td><td>Luiz Roberto Giugni</td></tr>
            <tr class="even"><td>Vice-Presidente</td><td>Ronaldo Bittencourt Filho </td></tr>
            <tr class=""><td>Secretario Geral</td><td>Ronaldo Bittencourt Filho></tr>
            <tr class="even"><td>Primeiro Secretário</td><td>Carla Rosana de Paula</td></tr>
            <tr class=""><td>Diretor Financeiro</td><td>João Gilberto Cominese Freire</tr>
            <tr class="even"><td>Diretor Veterinário</td><td>Thomas Walter Wolff</td></tr>
            <tr class=""><td>Diretor Jurídico</td><td>Sandra M. M. Neves Piva</tr>
        </table>
	</div>

</div>
<!-- END Historia -->
