<?php
	
	$realizarInscricao 		= FALSE;
	$programa 				= FALSE;
	$adendo 				= FALSE;
	$ordensDeEntrada 		= FALSE;
	$resultados 			= FALSE;
	$provas 				= FALSE;
	$comprovanteInscricao 	= FALSE;
	$segundaViaBoleto 		= FALSE;




	$status_titulo 		   = $this->m_crud->get_rowSpecific('tb_site_evento_status', 'sta_id', $evento[0]->fk_sta_id, 1, 'sta_titulo');
	$status_botao 		   = $this->m_crud->get_rowSpecific('tb_site_evento_status', 'sta_id', $evento[0]->fk_sta_id, 1, 'sta_botao');

	$status = $evento[0]->fk_sta_id;
	switch ($status) {

		//Inscricoes em Breve
		case 1:
			$status_bootstrap  = 'btn-primary';
			break;

		//Inscricoes Abertas
		case 2:
			$status_bootstrap  	= 'btn-success';
			$provas 			= TRUE;
			break;

		//Inscricoes Encerradas
		case 3:
			$status_bootstrap  	= 'btn-danger';
			break;

		//Evento Acontecendo
		case 4:
			$status_bootstrap  	= 'btn-primary';
			$resultados 	   	= $resultados;
			$provas 			= TRUE;
			break;

		//Evento já Aconteceu
		case 5:
			$status_bootstrap  	= 'btn-warning';
			$provas 			= TRUE;
			break;

		//Evento Cancelado
		case 6:
			$status_bootstrap  = 'btn-default';
			break;

		//Default
		default:
			$status_bootstrap  = 'btn-default';
			break;

	}

	/* ORDEM DE ENTRADA */	
	/*if($evento[0]->fk_sta_id==3 || $evento[0]->fk_sta_id==4 || $evento[0]->fk_sta_id==5){
		$ordensdeentrada = TRUE;
		$arr_ordensdeentrada 	=	$this->m_crud->get_allWhere('tb_site_evento_ordensdeentrada', 'fk_eve_id', $evento[0]->eve_id);
	}*/

	/* RESULTADOS */
	/*if($evento[0]->fk_sta_id==4 || $evento[0]->fk_sta_id==5){
		$resultados = TRUE;
		$arr_resultados = array();
	}*/




	/* INSCRICAO */
	if(!empty($evento[0]->eve_inscricao) && $evento[0]->fk_sta_id==2){
		$realizarInscricao  = TRUE;
	}

	/* PROGRAMA */
	if(!empty($evento[0]->eve_programa)){
		$programa 			= TRUE;
	}

	/* ADENDO */
	if(!empty($evento[0]->eve_adendo)){
		$adendo 			= TRUE;
	}


?>

<!-- BEGIN Evento -->
<div class="col-sm-12 padding-left-0 page_evento">
	
	<ul class="breadcrumb">
	    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a></li>
	    <li><a href="<?php echo base_url() . 'calendario'; ?>">Calendário</a></li>
	    <li class="active"><?php echo strip_tags($evento[0]->eve_evento); ?></li>
	</ul>	

	<h1><?php echo $evento[0]->eve_evento; ?></h1>

	<?php if($global_map==true): ?>
	<div class="row infoheader">
		<div class="col-sm-12 local"><b>Local:</b> <?php echo strip_tags($global_map_content); ?></div>
		<div class="col-sm-12 vernomapa"><a href="#ancora-localizacao" class="locationpage" title=""><i class="fa fa-map-marker"></i> Confira localização no Mapa</a></div>
	</div>	
	<?php endif; ?>

	<div class="gridline"></div>

	<div class="row">

		<div class="col-sm-12">

			<!-- REALIZAR INSCRIÇÃO -->
			<?php if($realizarInscricao): ?>
			<a href="<?php echo $evento[0]->eve_inscricao ?>" class="btn btn-success" target="_blank">
				Realizar Inscrição
			</a>
			<?php endif; ?>

			<!-- PROGRAMA -->
			<?php if($programa): ?>
			<a href="<?php echo base_url() . 'uploads/evento/programa/' . $evento[0]->eve_programa; ?>" class="btn btn-primary" target="_blank">
				Programa
			</a>
			<?php endif; ?>

			<!-- ADENDO -->
			<?php if($adendo): ?>
			<a href="<?php echo base_url() . 'uploads/evento/adendo/' . $evento[0]->eve_adendo; ?>" target="_blank" class="btn btn-primary" target="_blank">
				Adendo
			</a>
			<?php endif; ?>

			<!-- ORDENS DE ENTRADA -->
			<?php if($ordensDeEntrada): ?>
			<a href="#ordensresultados" class="btn btn-primary locationpage">
				Ordens de Entrada
			</a>
			<?php endif; ?>

			<!-- RESULTADOS -->
			<?php if($resultados): ?>
			<a href="#ordensresultados" class="btn btn-primary locationpage">
				Resultados
			</a>
			<?php endif; ?>

			<!-- PROVAS -->
			<?php if($provas): ?>
			<a href="#" class="btn btn-primary">
				Provas
			</a>
			<?php endif; ?>

			<!-- COMPROVANTE DE INSCRIÇÃO -->
			<?php if($comprovanteInscricao): ?>
			<a href="#" class="btn btn-primary">
				Comprovante de Inscrição
			</a>
			<?php endif; ?>

			<!-- 2º VIA DE BOLETO -->
			<?php if($comprovanteInscricao): ?>
			<a href="#" class="btn btn-primary">
				2º Via de Boleto
			</a>
			<?php endif; ?>

		</div>

	</div>

	<div class="gridline"></div>


	<div class="row informacoesgerais margin-bottom-30">

		<div class="col-sm-12">
		    <div class="title_template">INFORMAÇÕES <b>GERAIS</b></div>
		    <div class="logo col-sm-3"><div class="box"><img src="<?php echo base_url() . 'uploads/evento/' . $evento[0]->eve_logo; ?>" class="img-responsive" /></div></div>
		    <div class="descricao col-sm-9">
		    	<?php 
		    		if(is_null($evento[0]->eve_descricao)){
						echo "Nenhuma descrição para este evento";		    			
		    		}else{
		    			echo $evento[0]->eve_descricao;
		    		}
		    	?>
		    </div>
		</div>

		<div class="col-sm-12">
			
			<div class="col-sm-<?php echo ($counter==true) ? '5' : '12'; ?> no-padding detalhes">
				<div class="row">

					<!-- PERÍODO -->
					<?php if($periodo): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Período:</div>
							<div class="col-sm-6 value">
				        		<?php echo $this->my_date->datetime($evento[0]->eve_datainicio, 'justDate'); ?>
		        				<?php if(!empty($evento[0]->eve_datafim)): ?>
				        		a <?php echo $this->my_date->datetime($evento[0]->eve_datafim, 'justDate'); ?>
					        	<?php endif; ?>
							</div>
						</div>
					</div>
					<?php endif; ?>

					<!-- DATA LIMITE DE PAGAMENTO COM DESCONTO ESPECIAL -->
					<?php if($eve_data_limite_pagamento_desconto_especial): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Data Limite de Pagamento com Desconto Especial:</div>
							<div class="col-sm-6 value"><?php echo $this->my_date->datetime($evento[0]->eve_data_limite_pagamento_desconto_especial, 'justDate'); ?></div>
						</div>
					</div>
					<?php endif; ?>
					
					<!-- DATA LIMITE DE PAGAMENTO COM DESCONTO -->
					<?php if($eve_data_limite_pagamento_com_desconto): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Data Limite de Pagamento com Desconto:</div>
							<div class="col-sm-6 value"><?php echo $this->my_date->datetime($evento[0]->eve_data_limite_pagamento_com_desconto, 'justDate'); ?></div>
						</div>
					</div>
					<?php endif; ?>

					<!-- DATA ENCERRAMENTO INSCRIÇÃO ( DATA FIM ) -->
					<?php if($eve_data_encerra_inscricao): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Data Encerramento de Inscrição:</div>
							<div class="col-sm-6 value"><?php echo $this->my_date->datetime($evento[0]->eve_data_encerra_inscricao, 'justDate'); ?></div>
						</div>
					</div>
					<?php endif; ?>

					<!-- PRESIDENTE DO JURI -->
					<?php if($eve_juri_presidente): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Presidente do Juri:</div>
							<div class="col-sm-6 value"><?php echo $evento[0]->eve_juri_presidente; ?></div>
						</div>
					</div>
					<?php endif; ?>

					<!-- MEMBROS DO JURI DE CAMPO -->
					<?php if($eve_juri_membros): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Membro Juri de Campo:</div>
							<div class="col-sm-6 value"><?php echo $evento[0]->eve_juri_membros; ?></div>
						</div>
					</div>
					<?php endif; ?>

					<!-- DELEGADO TÉCNICO -->
					<?php if($eve_delegado_tecnico): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Delegado Técnico:</div>
							<div class="col-sm-6 value"><?php echo $evento[0]->eve_delegado_tecnico; ?></div>
						</div>
					</div>
					<?php endif; ?>

					<!-- DESENHADOR DE PERCURSO -->
					<?php if(!is_null($eve_armador)): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Desenhador de Percurso:</div>
							<div class="col-sm-6 value"><?php echo $evento[0]->eve_armador; ?></div>
						</div>
					</div>
					<?php endif; ?>

					<!-- STATUS DO EVENTO -->
					<?php if(!is_null($fk_sta_id)): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Status do Evento:</div>
							<div class="col-sm-6 value"><span class="btn btn-xs <?php echo $status_bootstrap; ?>"><?php echo $status_titulo; ?></span></div>
						</div>
					</div>
					<?php endif; ?>

				</div>
			</div>

			<?php if($counter==TRUE): ?>
			<div class="col-sm-7 contador regressivoInscricao">
				
				<h3>INSCRIÇÕES <b>ENCERRAM</b> EM</h3>
				<div id="counter"></div>
				<div class="desc">
					<div>Dia(s)</div>
					<div>Hora(s)</div>
					<div>Minuto(s)</div>
					<div>Segundo(s)</div>
				</div>
				<div class="btninscricao col-sm-12">
					<a href="<?php echo $evento[0]->eve_inscricao ?>" class="btn btn-success btn-block" target="_blank">
						Inscrever agora
					</a>
				</div>

			</div>
			<?php endif; ?>

		</div>


	</div>


	<!-- BEGIN OrdensResultados -->
	<?php if($ordensDeEntrada || $resultados): ?>
	<div class="row margin-bottom-30 margin-top-20" id="ordensresultados">


			<!-- ORDENS DE ENTRADA -->
			<div class="col-sm-6">
			    <div class="title_template">ORDENS DE <b>ENTRADA</b></div>
			    <div class="descricao col-sm-12">
			    	
			    	<?php if(empty($arr_ordensDeEntrada)): ?>
			    		<div class="margin-top-15">Nenhuma ordem de entrada para esse evento. Aguarde atualização.</div>
			    	<?php endif; ?>
					
			    	<?php if(!empty($arr_ordensDeEntrada)): ?>
			    		<ul>
				    		<?php foreach ($arr_ordensDeEntrada as $arr_ordem): ?>
				    		<li class="margin-top-15">
				    			<a href="<?php echo base_url() . 'uploads/evento/ordem/' . $arr_ordem->ord_arquivo; ?>" title="<?php echo $arr_ordem->ord_titulo; ?>" target="_blank">
				    				<span class="label label-warning"><?php echo $arr_ordem->ord_titulo; ?></span>
				    			</a>
				    		</li>
					    	<?php endforeach; ?>
			    		</ul>
				    <?php endif; ?>
			    </div>
			</div>


			<!-- RESULTADO -->
			<div class="col-sm-6">
			    <div class="title_template">RESULTADOS DO <b>EVENTO</b></div>
			    <div class="descricao col-sm-12">
			    	
			    	<?php if(empty($arr_resultados)): ?>
			    		<div class="margin-top-15">Nenhuma resultado para esse evento. Aguarde atualização.</div>
			    	<?php endif; ?>
					
			    	<?php if(!empty($arr_resultados)): ?>
			    		<ul>
				    		<?php foreach ($arr_resultados as $arr_resultado): ?>
				    		<li>
				    			<a href="<?php echo base_url() . 'uploads/evento/resultado/' . $arr_resultado->res_arquivo; ?>" title="<?php echo $arr_resultado->res_titulo; ?>" target="_blank">
				    				<?php echo $arr_resultado->res_titulo; ?>
				    			</a>
				    		</li>
					    	<?php endforeach; ?>
			    		</ul>
				    <?php endif; ?>
			    </div>
			</div>



	</div>
	<!-- END Ordens de Entrada -->
	<?php endif; ?>



	<?php
	/*
	<!-- PATROCINADORES GOLD -->
	<div class="row patrocinadores">
		<div class="col-sm-12">
		    <div class="title_template">PATROCINADORES <b>GOLD</b></div>
		</div>
	</div>


	<!-- PATROCINADORES GOLD -->
	<div class="row patrocinadores">
		<div class="col-sm-12">
		    <div class="title_template">APOIADORES DO <b>EVENTO</b></div>
		</div>
	</div>
	*/
	?>


</div>
<!-- END Evento -->


<?php if($global_map==TRUE): ?>

</div>
<!-- END Container ( opened header.php ) -->


		<!-- MAPA -->
		<div class="localizacao col-sm-12" id="ancora-localizacao" style="padding-top:25px;position:relative;z-index:200;">
			<div class="container">
				<div class="col-sm-1"><i class="fa fa-map-marker"></i></div>
				<div class="col-sm-11 red-title">
					<span class="titulo">Venha nos visitar</span>
					
					<span class="local">
						<?php echo $global_map_content; ?>
					</span>
				</div>
			</div>
		</div>

		<div class="col-sm-12 no-padding" style="position:relative;z-index:100;">
          <div id="map" class="gmaps" style="height:400px;"></div>
        </div>


<div class="container no-padding">

<?php endif; ?>