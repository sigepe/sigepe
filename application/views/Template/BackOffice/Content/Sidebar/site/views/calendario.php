<!-- BEGIN Evento -->
<div class="col-sm-12 padding-left-0 padding-right-0" id="page_calendario">
		
	<div class="col-sm-12">
		<ul class="breadcrumb">
		    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a></li>
		    <li class="active"><a href="<?php echo base_url() . 'calendario'; ?>">Calendário</a></li>
		</ul>
	</div>	

	<div class="col-sm-12"> <h1>Calendário</h1> </div>


	<div class="col-sm-12">


		<div class="table-responsive">          
		    <table class="table tabela no-margin" cellspacing='0' id="tableCalendar">
		        
		     	<!-- Table Head -->
		        <thead>
			        <tr>
			        	<th>Evento</th>
			        	<th>Ano</th>
			        	<th>Mês</th>
			        	<th>Data</th>
			        	<th>Modalidade</th>
			        	<th>Local</th>
			        	<th>Status</th>
			        </tr>
		        </thead>

		        <tbody>

			        <?php
			     		$contador=0;
			        	foreach ($arrEvento as $evento):
			       
			        		$ano			=	$this->my_date->datetime($evento->eve_datainicio, 'justYear');
			        		$programa		=	base_url() . 'evento/' . $evento->eve_slug;
			        		$mesExtenso 	=	$this->my_date->mes_extenso($evento->eve_datainicio);
			        		$dataInicio 	=	$this->my_date->datetime($evento->eve_datainicio, 'justDayAndMonth');
			        		$dataFim 		=	$this->my_date->datetime($evento->eve_datafim, 'justDayAndMonth');

							switch ($evento->fk_sta_id) {
								case 1:
									$status_bootstrap  = 'primary'; //Inscricoes em Breve
									break;
								case 2:
									$status_bootstrap  	= 'success'; //Inscricoes Abertas
									break;
								case 3:
									$status_bootstrap  	= 'danger'; //Inscricoes Encerradas
									break;
								case 4:
									$status_bootstrap  	= 'primary'; //Evento Acontecendo
									break;							
								case 5:
									$status_bootstrap  	= 'warning'; //Evento já Aconteceu
									break;
								case 6:
									$status_bootstrap  = 'default'; //Evento Cancelado
									break;
								default:
									$status_bootstrap  = 'default'; //Default
									break;
							}


			        ?>
			        <tr class="<?php echo ($contador % 2==0) ? 'even' : ''; ?>" data-row="item">


			        	<!-- Nome do Evento -->
			        	<td>
				        		<a href="<?php echo $programa; ?>" title="<?php echo $evento->eve_evento; ?>" class="btn btn-block btn-<?php echo $status_bootstrap; ?>"><?php echo $evento->eve_evento; ?></a>
				        </td>
			        	

			        	<!-- Mes -->
			        	<td><?php echo $ano; ?></td>

			        	<!-- Mes -->
			        	<td><?php echo $mesExtenso; ?></td>

			        	<!-- Data -->
			        	<td class="text-center">
			        		<?php

								$date = DateTime::createFromFormat("Y-m-d", $evento->eve_datafim);
								#$year =  $date->format("Y");

			        			echo $dataInicio;
			        			echo (!empty($evento->eve_datafim)) ? ' a ' . $dataFim : '';
			        			/*echo "<span class='label label-warning date'>".$year."</span>"*/
			        		 ?>
			        	</td> 
			        	
			        	<!-- Modalidade -->
			        	<td>
			        		<?php echo $this->m_crud->get_rowSpecific(
															'tb_site_modalidade',
															'mod_id',
															$evento->fk_mod_id,
															1,
															'mod_modalidade'
														);
			        		?>
			        	</td>

			        	<!-- Local -->
			        	<td><?php echo $this->m_crud->get_rowSpecific('tb_entidadeescola', 'ent_id', $evento->fk_ent_id, 1, 'ent_sigla'); ?></td>

			        	<!-- Status -->
			        	<td>
			        		<span class="label label-<?php echo $status_bootstrap; ?>">
			        			<?php echo $this->m_crud->get_rowSpecific('tb_site_evento_status', 'sta_id', $evento->fk_sta_id, 1, 'sta_titulo'); ?>
			        		</span>
			        	</td>

			        </tr>	
			        <?php $contador++;  ?>
				    <?php endforeach; ?>
				
				</tbody>


		    </table>
		</div>



		<div class="row">
			<div class="col-sm-12">
				<div class="col-xs-6 col-md-3" id="calendario_cbh">
					<a href="http://www.cbh.org.br/calendarios.html" target="_blank" title="" class="link_calendario_externo">
						<div class="col-sm-4 bandeira"><img src="<?php echo base_url(); ?>assets/frontend/layout/img/calendario/cbh.jpg" /></div>
						<div class="col-sm-8 texto"><span>calendário CBH</span></div>
					</a>
				</div>		
				<div class="col-xs-6 col-md-3" id="calendario_fei">
					<a href="https://data.fei.org/Calendar/Search.aspx" target="_blank" title="" class="link_calendario_externo">
						<div class="col-sm-4 bandeira"><img src="<?php echo base_url(); ?>assets/frontend/layout/img/calendario/fei.jpg" /></div>
						<div class="col-sm-8 texto"><span>calendário FEI</span></div>
					</a>
				</div>	
			</div>
		</div>

		<div class="col-sm-12">
			<div class="gridline"></div>
		</div>




	</div>



</div>
<!-- END Calendário -->



<!-- Modal Bootstrap - Procurar Evento
============================================= -->
<div class="modal fade" id="modalSearchEvent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><b>Filtro de Busca</b></h4>
      </div>

	<form id="formSearchEvent">
    
      <div class="modal-body">
        
		<div class="alert alert-info">
			<strong></strong> <b>Vamos ajudar você a encontrar o evento que procura.</b> <br /><br /> Selecione os filtros em seguida clique em buscar.
		</div>





			<!-- Modalidade
			============================================= -->
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-md-3 text-right">Modalidade</label>
						<div class="col-md-9">
							<select class="form-control" name="modality" id="modality">
								<option value="">- Selecione uma Modalidade - </option>
								<option value="7">Salto</option>
								<option value="1">Atrelagem</option>
								<option value="2">Adestramento</option>
								<option value="3">CCE</option>
								<option value="4">Enduro</option>
								<option value="5">Paraequestre</option>
								<option value="7">Rédeas</option>
								<option value="8">Volteio</option>
							</select>
							<span class="help-block">
							 </span>
						</div>
					</div>
				</div>
			</div>





			<!-- Status
			============================================= -->
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-md-3 text-right">Status</label>
						<div class="col-md-9">
							<select class="form-control" name="status" id="status">
								<option value="">- Selecione um Status -</option>
								<option value="1">Inscrições em breve</option>
								<option value="2">Inscrições Abertas</option>
								<option value="3">Inscrições Encerradas</option>
								<option value="4">Evento Acontecendo</option>
								<option value="5">Evento Encerrado</option>
								<option value="6">Evento Cancelado</option>
							</select>
							<span class="help-block">
							 </span>
						</div>
					</div>
				</div>
			</div>


			<!-- Ano
			============================================= -->
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-md-3 text-right">Ano</label>
						<div class="col-md-9">
							<select class="form-control" name="year" id="year">
								<option value="">- Selecione o Ano -</option>
								<option value="2015">2015</option>
								<option value="2016">2016</option>
							</select>
							<span class="help-block">
							 </span>
						</div>
					</div>
				</div>
			</div>



      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success"><i class="fa fa-search"></i>Buscar</button>
      </div>

	</form>

    </div>
  </div>
</div>
