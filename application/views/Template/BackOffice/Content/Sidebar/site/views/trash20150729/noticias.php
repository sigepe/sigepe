<!-- BEGIN Historia -->
<div class="col-sm-12 padding-left-0" id="page_noticias">
  
  <ul class="breadcrumb visible-md visible-lg">
      <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a></li>
      <li class="active">Noticias</li>
  </ul> 


  <h1>Notícias</h1>

</div>

    <!-- BEGIN LEFT SIDEBAR -->            
    <div class="col-md-9 col-sm-9 blog-posts">

      <?php foreach ($arrNoticias as $noticia): ?>
      <div class="row">
        <div class="col-md-4 col-sm-4">
          <a href="#" title=""> 
            <img class="img-responsive" alt="" src="<?php echo base_url() . 'uploads/noticia/' . $noticia['thumb']; ?>" />
          </a>
        </div>
        <div class="col-md-8 col-sm-8">
          <h2><a href="#"><?php echo $noticia['titulo']; ?></a></h2>
          <ul class="blog-info">
            <li><i class="fa fa-calendar"></i> <?php echo $noticia['data']; ?></li>
          </ul>
          <p><?php echo $noticia['descricao']; ?></p>
        </div>
      </div>
      <hr class="blog-post-sep">
      <?php endforeach; ?>

      <?php if(empty($arrNoticias)): ?>
      Não existe nenhuma notícia cadastrada
      <?php endif; ?>


<!--       <hr class="blog-post-sep">
      <ul class="pagination">
        <li><a href="#">Prev</a></li>
        <li><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li class="active"><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li><a href="#">Next</a></li>
      </ul>                -->

    </div>
    <!-- END LEFT SIDEBAR -->

