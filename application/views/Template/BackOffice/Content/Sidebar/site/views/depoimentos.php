<!-- Rodape - Parte Inferior ( Copyright ) -->
<div class="container-fluid" id="pagina-beneficios">      

  <div class="container">
      
    <!-- BREADCRUMBS -->
    <ul class="breadcrumb">
        <li><a href="<?php echo base_url() . 'inicio'; ?>">Site</a></li>
        <li class="active">Depoimentos</li>
    </ul>


    <!-- BENEFÍCIOS -->
    <?php foreach ($depoimentos as $depoimento): ?>

      <div class="col-sm-6 box">
          <div class="col-sm-3">
            <img src="<?php echo base_url() . 'uploads/depoimento/' . $depoimento->dep_thumb; ?>" class="img-circle" />
          </div>
          <div class="col-sm-8">
            <ul class="item">
                <li class="titulo"><?php echo $depoimento->dep_titulo; ?></li>
                <li class="conteudo">
                  <?php
                    $string = strip_tags($depoimento->dep_conteudo);
                    echo $string = (strlen($string) > 153) ? substr($string,0,150)."... <a href='#' title='' data-toggle='modal' data-target='#myModal".$depoimento->dep_id."'>continue lendo</a> " : $string;
                  ?>
                </li>
            </ul>
          </div>
          <div class="col-sm-1"></div>
      </div>

      <!-- Modal -->
      <?php if(strlen($string) > 125): ?>
      <div class="modal fade" id="myModal<?php echo $depoimento->dep_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title" id="myModalLabel"><?php echo $depoimento->dep_titulo; ?></h4>
            </div>
            <div class="modal-body">

              <div class="row">

                <div class="col-sm-3" style="text-align:center;">
                  <img src="<?php echo base_url() . 'uploads/depoimento/' . $depoimento->dep_thumb; ?>" style="display:inline;" style="border-radius:50%;" />
                </div>
                <div class="col-sm-8" style="color:#888;">
                  <?php echo $depoimento->dep_conteudo; ?>
                </div>

              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
          </div>
        </div>
      </div>
      <?php endif; ?>

    <?php endforeach; ?>


  </div>

</div>