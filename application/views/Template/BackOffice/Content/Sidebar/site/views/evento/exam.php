<!-- Begin Provas
============================================= -->
<?php if($provas): ?>
<div class="row col-sm-12 margin-bottom-30 margin-top-30" id="provas">


    <div class="title_template margin-bottom-20">PROVAS</div>


	<?php foreach ($dias as $dia) : ?>

	    <!-- Botoes - Dia
	    ============================================= -->
		<div class="col-sm-12 no-padding">
			<a href="#<?php echo "provas_" . $dia->dia_id; ?>" title="" class="dia_title text-center col-sm-12" id="<?php echo "linkprova_" . $dia->dia_id; ?>">
				<?php $diasemana = $this->my_date->datetime($dia->dia_dia, 'justDate'); ?>
				<?php echo $dia->dia_semana . ' - ' . $diasemana; ?>
				<i class="fa fa-plus-circle"></i>
			</a>
		</div>


	    <!-- Box Day
	    ============================================= -->
		<div class="col-sm-12 no-padding margin-bottom-30 box-table-responsive" id="<?php echo "provas_" . $dia->dia_id; ?>">


		    <!-- Table-responsive - Bootstrap
		    ============================================= -->
			<div class="table-responsive">


			    <!-- Table
			    ============================================= -->
			    <table class="table">


				    <!-- Table Head
				    ============================================= -->
	    		    <thead>
				        <tr>
				        	<th>Nome</th>
				        	<th>Pista</th>
				        	<th>Altura</th>
				        	<th>Categorias</th>
				        	<th>Tipo</th>
				        	<th>Hora</th>
				        	<th>Croqui</th>
				        	<th>Status</th>
				        </tr>
				    </thead>


				    <!-- Table Body
				    ============================================= -->
				    <tbody>
			     	<?php
						$contador=0;
						$provas = $this->m_crud->get_allWhere('tb_site_evento_prova', 'fk_dia_id', $dia->dia_id); 
						if(is_array($provas)): 
							foreach ($provas as $prova): 
					?>

							    <!-- Table Row
							    ============================================= -->					        
						        <tr class="<?php echo ($contador % 2==0) ? 'even' : ''; ?>">


								    <!-- Nome
								    ============================================= -->					        
						        	<td>
						        		<?php echo $prova->pro_nome; ?>
						        	</td>


								    <!-- Pista
								    ============================================= -->					        
						        	<td>
						        		<?php echo $prova->pro_pista; ?>
						        	</td>


								    <!-- Altura
								    ============================================= -->					        
						        	<td>
						        		<?php echo $prova->pro_altura; ?>
						        	</td>


								    <!-- Categorias
								    ============================================= -->					        
						        	<td>
						        		<?php echo $prova->pro_categorias; ?>
						        	</td>


								    <!-- Tipo
								    ============================================= -->					        
						        	<td>
						        		<?php echo $prova->pro_tipo; ?>
						        	</td>


								    <!-- Hora
								    ============================================= -->					        
						        	<td>
						        		<?php echo $prova->pro_hora; ?>
						        	</td>


								    <!-- Croqui
								    ============================================= -->					        
						        	<td>
						        		<?php if(!empty($prova->pro_croqui)): ?>
						        		<a href="<?php echo base_url() . 'uploads/evento/croqui/' . $prova->pro_croqui; ?>" class="label label-warning btn-block btn-croqui" target="_blank">Ver Croqui</a>
							        	<?php endif; ?>
						        		<?php echo (empty($prova->pro_croqui)) ? '-' : ''; ?>
						        	</td>


								    <!-- Status
								    ============================================= -->					        
						        	<?php

						        		$statusProva = $this->m_crud->get_rowSpecific(
						        							'tb_site_evento_prova_status',
						        							'sta_id',
						        							$prova->fk_sta_id,
						        							1,
						        							'sta_titulo'
						        						);
										
										switch ($prova->fk_sta_id):
											case 1: //A Realizar
												$statusProva_bootstrap  	= 'btn-primary';
												break;
											case 2: //Em Andamento
												$statusProva_bootstrap  	= 'btn-success';
												break;
											case 3: //Encerrada
												$statusProva_bootstrap  	= 'btn-danger';
												break;
											case 4: //Cancelada
												$statusProva_bootstrap  	= 'btn-primary';
												break;
											default: //Default
												$status_bootstrap 			= 'btn-default';
												break;
										endswitch;

						        	?>


								    <!-- Botoes
								    ============================================= -->					
						        	<td>
										<div class="btns">
											
											<span class="item label btn-block <?php echo $statusProva_bootstrap; ?>">
												Prova <?php echo $statusProva; ?>
											</span>
					
											<!-- ORDEM DE ENTRADA -->
											<?php if($prova->pro_ordem_status==1): ?>
											<div>
												<a href="<?php echo base_url() . 'uploads/evento/ordem/' . $prova->pro_ordem; ?>" title="Fazer download da ordem de entrada da prova: <?php echo $prova->pro_nome; ?>" class="item label label-warning btn-block" target="_blank">Ordem de Entrada</a>
											</div>
											<?php endif; ?>

											<!-- RESULTADO ONLINE -->
											<?php if($prova->pro_resultado_link_status==1): ?>
											<div>
												<a href="<?php echo $prova->pro_resultado_link ?>" title="Acessar resultado online da prova: <?php echo $prova->pro_nome; ?>" class="item label label-warning btn-block" target="_blank">Resultado Online</a>
											</div>
											<?php endif; ?>

											<!-- RESULTADO EQUIPE -->
											<?php if($prova->pro_resultado_equipe_status==1): ?>
											<div>
												<a href="<?php echo base_url() . 'uploads/evento/equipe/' . $prova->pro_resultado_equipe_pdf; ?>" title="Confira resultado final da prova: <?php echo $prova->pro_nome; ?>" class="item label label-warning btn-block" target="_blank">Resultado Equipe</a>
											</div>
											<?php endif; ?>

											<!-- RESULTADO CAMPEONATO -->
											<?php if($prova->pro_resultado_campeonato_status==1): ?>
											<div>
												<a href="<?php echo base_url() . 'uploads/evento/resultado/campeonato/' . $prova->pro_resultado_campeonato_pdf; ?>" title="Confira resultado final da prova: <?php echo $prova->pro_nome; ?>" class="item label label-warning btn-block" target="_blank">Resultado Campeonato</a>
											</div>
											<?php endif; ?>

											<!-- RESULTADO FINAL -->
											<?php if($prova->pro_resultado_pdf_status==1): ?>
											<div>
												<a href="<?php echo base_url() . 'uploads/evento/resultado/final/' . $prova->pro_resultado_pdf; ?>" title="Confira resultado final da prova: <?php echo $prova->pro_nome; ?>" class="item label label-warning btn-block" target="_blank">Resultado Final</a>
											</div>
											<?php endif; ?>

										</div>
						        	</td>

						        </tr>	
						        <?php $contador++;  ?>
								
								<?php
									if(empty($provas))
									echo '<div class="btn btn-warning margin-top-20" style="display:table;">Nenhuma prova para <span style="text-decoration:underline;"><b><i>este dia</i></b></span>.</div>';
								?>

						    <?php endforeach; ?>
						<?php endif; ?>

				    </tbody>

				</table>

			</div>
			<!-- /.table-responsive -->

		</div>
		<!-- /.box-table-responsive -->

	<?php endforeach; ?>


    <!-- Dias
    ============================================= -->	
	<?php echo (empty($dias)) ? '<div class="btn btn-warning margin-top-20" style="display:table;">Nenhuma prova para <span style="text-decoration:underline;"><b><i>este evento</i></b></span>.</div>' : '' ?>


</div>
<?php endif; ?>
<!-- /#provas -->