<div class="col-sm-12 page_evento">
	
	
	<ul class="breadcrumb hidden-xs">
	    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a></li>
	    <li><a href="<?php echo base_url() . 'calendario'; ?>">Calendário</a></li>
	    <li class="active"><?php echo strip_tags($evento[0]->eve_evento); ?></li>
	</ul>	


	<h1><?php echo $evento[0]->eve_evento; ?></h1>

	<?php if($global_map==true): ?>
	<div class="col-sm-12"><b>Local:</b> <?php echo strip_tags($global_map_content); ?></div>
	<div class="col-sm-12"><a href="#ancora-localizacao" class="locationpage" title=""><i class="fa fa-map-marker"></i> Confira localização no Mapa</a></div>
	<?php endif; ?>
	<div class="gridline"></div>

	<div class="row float-left">

		<div class="col-sm-12">

			<!-- PROGRAMA --><?php if($programa): ?><a href="<?php echo base_url() . 'uploads/evento/programa/' . $evento[0]->eve_programa; ?>" class="btn btn-default" target="_blank">Programa</a><?php endif; ?>
			<!-- ADENDO --><?php if($adendo): ?><a href="<?php echo base_url() . 'uploads/evento/adendo/' . $evento[0]->eve_adendo; ?>" target="_blank" class="btn btn-primary" target="_blank">Adendo</a><?php endif; ?>
			<!-- ORDENS DE ENTRADA --><?php if($ordensDeEntrada): ?><a href="#ordensresultados" class="btn btn-info locationpage">Ordens de Entrada</a><?php endif; ?>
			<!-- QUADRO DE HORARIOS --><?php if($eve_quadrohorarios): ?><a href="<?php echo base_url() . 'uploads/evento/quadrodehorario/' . $evento[0]->eve_quadrohorarios; ?>" class="btn btn-primary" target="_blank">Quadro de Horários</a><?php endif; ?>
			<!-- RESULTADOS 0 --><?php if($eve_resultado==0): ?><a href="#provas" class="btn btn-warning locationpage">Resultados</a><?php endif; ?>
			<!-- RESULTADO 1 --><?php if($eve_resultado==1): ?><a href="<?php echo base_url() . 'uploads/evento/resultado/' . $evento[0]->eve_resultado; ?>" class="btn btn-warning" target="_blank">Resultados</a><?php endif; ?>
			<!-- PROVAS --><?php if($provas): ?><a href="#provas" class="btn btn-info locationpage">Provas</a><?php endif; ?>

		</div>

	</div>

	<div class="gridline"></div>

