<!-- BEGIN Historia -->
<div class="col-sm-9 padding-left-0 federacao" id="federacao_listagemEntidadesEscolas">
	
	<ul class="breadcrumb">
	    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a></li>
	    <li><a href="<?php echo base_url() . 'afederacao/historia'; ?>">A Federação</a></li>
	    <li class="active"><?php echo $tipoPagina; ?></li>
	</ul>	

	<h1><?php echo $tipoPagina; ?></h1>
	
	<div class="conteudo">

		<div class="col-sm-6 item padding-left-0">
			<div class="box">
				<div class="col-sm-5 thumb">
					<a href="#" title=""><img src="<?php echo base_url(); ?>assets/frontend/layout/img/entidades/rcg.jpg" alt="" class="img-responsive" /></a>
				</div>
				<div class="col-sm-7">
					<a href="#" title="" class="titulo">1º Regimento da Cavalaria de Guarda - RCG</a>
					<a href="#" class="btn btn-sm btn-primary">Mais Detalhes</a>
				</div>
			</div>
		</div>

		<div class="col-sm-6 item padding-left-0">
			<div class="box">
				<div class="col-sm-5 thumb">
					<a href="#" title=""><img src="<?php echo base_url(); ?>assets/frontend/layout/img/entidades/chls.png" alt="" class="img-responsive" /></a>
				</div>
				<div class="col-sm-7">
					<a href="#" title="" class="titulo">Centro Hípico Lago Sul “CHLS”</a>
					<a href="#" class="btn btn-sm btn-primary">Mais Detalhes</a>
				</div>
			</div>
		</div>

		<div class="col-sm-6 item padding-left-0">
			<div class="box">
				<div class="col-sm-5 thumb">
					<a href="#" title=""><img src="<?php echo base_url(); ?>assets/frontend/layout/img/entidades/shbr.png" alt="" class="img-responsive" /></a>
				</div>
				<div class="col-sm-7">
					<a href="#" title="" class="titulo">Sociedade Hípica de Brasília</a>
					<a href="#" class="btn btn-sm btn-primary">Mais Detalhes</a>
				</div>
			</div>
		</div>

		<div class="col-sm-6 item padding-left-0">
			<div class="box">
				<div class="col-sm-5 thumb">
					<a href="#" title=""><img src="<?php echo base_url(); ?>assets/frontend/layout/img/entidades/cittac.png" alt="" class="img-responsive" /></a>
				</div>
				<div class="col-sm-7">
					<a href="#" title="" class="titulo">CITTAC</a>
					<a href="#" class="btn btn-sm btn-primary">Mais Detalhes</a>
				</div>
			</div>
		</div>

		<div class="col-sm-6 item padding-left-0">
			<div class="box">
				<div class="col-sm-5 thumb">
					<a href="#" title=""><img src="<?php echo base_url(); ?>assets/frontend/layout/img/entidades/bcc.png" alt="" class="img-responsive" /></a>
				</div>
				<div class="col-sm-7">
					<a href="#" title="" class="titulo">Brasília Country Club - BCC</a>
					<a href="#" class="btn btn-sm btn-primary">Mais Detalhes</a>
				</div>
			</div>
		</div>

					


	</div>

</div>
<!-- END Historia -->
