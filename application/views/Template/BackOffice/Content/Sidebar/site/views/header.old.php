
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title><?php echo $title_page; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/template.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/mediaqueries.css" rel="stylesheet">

    <!-- Importando fontes ( Google Webfonts ): ROBOTO e MONT SERRAT -->
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700|Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>

    <!-- Slidebars -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/slidebars/distribution/0.10.2/slidebars.min.css">
    
    <?php if($use_owlslider): ?>
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/owlcarousel/owl-carousel/owl.carousel.css'; ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/owlcarousel/owl-carousel/owl.theme.css'; ?>">
    <?php endif; ?>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <?php if($use_wizard): ?>
      
      <link href="<?php echo base_url(); ?>assets/plugins/wizard/src/bootstrap-wizard.css" rel="stylesheet" />
      <link href="<?php echo base_url(); ?>assets/plugins/wizard/chosen/chosen.css" rel="stylesheet" />
      <link href="<?php echo base_url(); ?>assets/plugins/wizard/css/template.css" rel="stylesheet" />
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="<?php echo base_url(); ?>assets/plugins/wizard/js/html5shiv-3.7.0.js"></script>
      <script src="<?php echo base_url(); ?>assets/plugins/wizard/js/respond-1.3.0.min.js"></script>
      <![endif]-->
    <?php endif; ?>


  </head>

  <body>

  <div class="sb-slidebar sb-left sb-style-push">
    <div class="sb-slidebar-logo">
       <img src="<?php echo base_url(); ?>assets/images/logo.png" />
    </div>
    <div class="sb-slidebar-conteudomenu">

    </div>
  </div>



  <!-- Begin Slidebars -->
  <div id="sb-site">

    <!-- Static navbar -->
    <nav class="navbar navbar-default navbar-static-top" id="menu">
      <div class="container">
        <div class="navbar-header" id="identification_navbar-header">
          <button type="button" class="navbar-toggle collapsed toggle-menu my-button">
            <div class="box_iconbar">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </div>
            <div class="box_sronly">
              <span>MENU</span>
            </div>
          </button>
          <a class="navbar-brand" href="<?php echo base_url() . 'inicio' ; ?>">
            <img src="<?php echo base_url(); ?>assets/images/logo.png" alt="" />
          </a>
        </div>

        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="<?php echo ($current_page == 'inicio') ? 'active':'' ; ?>" id="nav-inicio"><a href="<?php echo base_url() . 'inicio'; ?>">INÍCIO</a></li>
            <li class="<?php echo ($current_page == 'associacao') ? 'active':'' ; ?>" id="nav-aassociacao"><a href="<?php echo base_url() . 'associacao'; ?>">A ASSOCIAÇÃO</a></li>
            <li class="<?php echo ($current_page == 'beneficios') ? 'active':'' ; ?>" id="nav-beneficios"><a href="<?php echo base_url() . 'beneficios'; ?>">BENEFÍCIOS</a></li>
            <li class="<?php echo ($current_page == 'agenda') ? 'active':'' ; ?>" id="nav-agenda"><a href="<?php echo base_url() . 'agenda'; ?>">AGENDA</a></li>
            <li class="<?php echo ($current_page == 'planodetrabalho') ? 'active':'' ; ?>" id="nav-planodetrabalho"><a href="<?php echo base_url() . 'planodetrabalho'; ?>">PLANO DE TRABALHO</a></li>
            <li class="<?php echo ($current_page == 'noticias') ? 'active':'' ; ?>" id="nav-noticias"><a href="<?php echo base_url() . 'noticias'; ?>">NOTÍCIAS</a></li>
            <!-- <li class="<?php echo ($current_page == 'meuaers') ? 'active':'' ; ?>" id="nav-meuaers"><a href="<?php echo base_url() . 'meuaers'; ?>">MEU AERS</a></li> -->
            <li class="<?php echo ($current_page == 'contato') ? 'active':'' ; ?>" id="nav-contato"><a href="<?php echo base_url() . 'contato'; ?>">CONTATO</a></li>
          </ul>

        </div><!--/.nav-collapse -->
      </div>
    </nav>

