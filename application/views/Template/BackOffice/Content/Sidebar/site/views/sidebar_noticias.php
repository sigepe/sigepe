<!-- BEGIN RIGHT SIDEBAR -->            
<div class="col-md-3 col-sm-3 blog-sidebar">
<!-- CATEGORIES START -->
<h2 class="no-top-space">Categorias</h2>
<ul class="nav sidebar-categories margin-bottom-40">
  <li class="active"><a href="#">Geral</a></li>
  <li><a href="#">Atrelagem</a></li>
  <li><a href="#">Adestramento</a></li>
  <li><a href="#">CCE</a></li>
  <li><a href="#">Enduro</a></li>
  <li><a href="#">Paraequestre</a></li>
  <li><a href="#">Rédeas</a></li>
  <li><a href="#">Salto</a></li>
  <li><a href="#">Volteio</a></li>
</ul>
<!-- CATEGORIES END -->

</div>
<!-- END RIGHT SIDEBAR -->     