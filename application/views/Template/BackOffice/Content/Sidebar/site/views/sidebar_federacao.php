
<!-- BEGIN Sidebar -->
<div class="col-sm-3 federacao_sidebar">




	<?php if(isset($tituloTipoPagina)): ?>
	<h2 class="no-top-space"><?php echo $tituloTipoPagina; ?></h2>
	<ul class="nav sidebar-categories margin-bottom-40">

		<?php foreach ($entidadesescolas as $menu_entidadeescola): ?>

		<?php

		$sidebar_active = NULL;

		if(!isset($slugItem)){
			$slugItem=='A';
		}
		if($menu_entidadeescola->ent_slug == $slugItem)
		{
			$sidebar_active = 'active';
		}
		?>

		<?php 
			$menu_titulo 	=	$menu_entidadeescola->ent_titulo;
			$menu_sigla 	=	$menu_entidadeescola->ent_sigla;
			$menu_link 		=	base_url() . 'a-federacao/' . $slugTipoPagina . '/' . $menu_entidadeescola->ent_slug;
		?>
		<li class="<?php echo $sidebar_active; ?>">
			<a href="<?php echo $menu_link; ?>"><?php echo $menu_titulo . ' - ' . $menu_sigla; ?></a>
		</li>
		<?php endforeach; ?>

	</ul>
	<?php endif; ?>
	<!-- CATEGORIES END -->

	<h2 class="no-top-space">A Federação</h2>
	<ul class="nav sidebar-categories margin-bottom-40">
		<li <?php echo ($current_page=='federacao_historia') ? 'class="active"' : ''; ?>><a href="<?php echo base_url() . 'a-federacao/historia'; ?>">História</a></li>
		<li <?php echo ($slugTipoPagina=='entidades-filiadas') ? 'class="active"' : ''; ?>><a href="<?php echo base_url() . 'a-federacao/entidades-filiadas'; ?>">Entidades Filiadas</a></li>
		<li <?php echo ($slugTipoPagina=='escolas-de-equitacao') ? 'class="active"' : ''; ?>><a href="<?php echo base_url() . 'a-federacao/escolas-de-equitacao'; ?>">Escolas de Equitação</a></li>
		<li <?php echo ($current_page=='federacao_tribunal') ? 'class="active"' : ''; ?>><a href="<?php echo base_url() . 'a-federacao/tribunal-de-justica-desportiva'; ?>">Tribunal de Justiça Desportiva</a></li>
	</ul>
	<!-- CATEGORIES END -->

	<h2 class="no-top-space">Portal da Transparência</h2>
	<ul class="nav sidebar-categories margin-bottom-40">
	    <li><a href="<?php echo base_url() . 'a-federacao/estatuto' ?>">Estatuto</a></li>
		<li <?php echo ($current_page=='federacao_prestacao') ? 'class="active"' : ''; ?>><a href="<?php echo base_url() . 'a-federacao/prestacao-de-contas'; ?>">Prestação de Contas</a></li>
		<li <?php echo ($current_page=='federacao_contratacoes') ? 'class="active"' : ''; ?>><a href="<?php echo base_url() . 'a-federacao/contratacoes-e-licitacoes'; ?>">Contratações e Licitações</a></li>
	</ul>
	<!-- CATEGORIES END -->



</div>
<!-- END Sidebar -->


	<?php if(isset($nomeEntidadeEscola) && !empty($query->ent_eixox) && !empty($query->ent_eixoy)): ?>

      </div>
      <!-- End Container ( opened header.php )  -->

    </div>
    <!-- End Main ( opened header.php )  -->

	<!-- MAPA -->
	<div class="localizacao" id="ancora-localizacao">
		<div class="container">
			<div class="col-sm-1"><img src="<?php echo base_url() . 'assets/frontend/layout/img/localizacao.png' ?>" /></div>
			<div class="col-sm-11 red-title">Localização <span> <?php echo $query->ent_endereco; ?></span></div>
		</div>
	</div>


	<div class="template-mapa">
		<div id="map" class="gmaps" style="height:400px;"></div>
	</div>


    <!-- Begin Main -->
    <div class="main">

      <!-- Begin Container -->
      <div class="container no-padding">



	<?php endif; ?>
