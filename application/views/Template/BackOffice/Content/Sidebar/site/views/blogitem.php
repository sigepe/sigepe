<div class="col-md-9 col-sm-9 blog-item padding-left-0" id="page_blogitem">


  <!-- Breadcrumb
  ============================================= -->
  <ul class="breadcrumb visible-md visible-lg">
      <li><a href="{base_url}"><i class="fa fa-home"></i></a></li>
      <li><a href="{base_url}{slugTipoPagina}">{tipoPagina}</a></li>
      <li><a href="{base_url}{slugTipoPagina}/{slugCategoria}">{tituloCategoria}</a></li>
      <li class="active">{blo_titulo}</li>
  </ul> 


  <!-- Cabecalho
  ============================================= -->
  <div class="cabecalho col-sm-12">

    <div><h1 class="titulo">{blo_titulo}</h1></div>
      
    <div class="cabecalho_info">
      <b>Publicado em: </b> {blo_data} 
      <?php if(!is_null($query[0]->modificado)): ?>
      <span>- <b>Atualizado em:</b> {modificado} </span>
      <?php endif; ?>
    </div>

    <div class="row visible-md visible-lg">
      <div class="col-sm-12 redessociais">
        <div id="ferramentas">
          <div id="social">

            <!-- FACEBOOK -->
            <div class="item facebook">
              <div class="fb-share-button" data-href="<?php echo $urlPost; ?>" data-layout="button_count"></div>
            </div>

            <!-- TWEET -->
            <div class="item">
              <a class="twitter-share-button" href="<?php echo $urlPost; ?>"
              data-related="twitterdev">
              Tweet
              </a>
            </div>

            <!-- GOOGLE PLUS -->
            <div class="item">
              <div class="g-plusone" data-size="medium" style="width:30px;"></div>
            </div>
            
          </div>
        </div>
      </div>
    </div>

  </div>


  <!-- Conteudo
  ============================================= -->
  <div class="both">


    <!-- Texto
    ============================================= -->
    <div class="col-sm-12">
      <div class="conteudo">{blo_conteudo}</div>
    </div>


    <!-- Galerias
    ============================================= -->
    <?php if ($slugTipoPagina=='galerias'): ?>
      <div class="row galeria">
        <h3 class="margin-top-20 col-sm-12">Fotos</h3>
        <?php
          if(is_null($arrFotos))
            echo '<div class="col-sm-12"><h3 style="font-weight:300;color:red;">Nenhuma foto cadastrada para essa publicação!</h3></div>';

          if(!is_null($arrFotos)):
            foreach ($arrFotos as $foto):
        ?> 
          <div class="col-md-3 col-sm-3 gallery-item">
            <a data-rel="fancybox-button" title="{blo_titulo}" href="{base_url}manager/uploads/blog/galerias/{blo_folder}/<?php echo $foto->fot_thumb; ?>" class="fancybox-button">
              <img src="{urlAbsolutoFoto}<?php echo thumb($urlRelativoFoto . $foto->fot_thumb, 200, 200); ?>" class="img-responsive" />
              <div class="zoomix"><i class="fa fa-search"></i></div>
            </a> 
          </div>
        <?php
            endforeach;
          endif;
        ?>
      </div>
    <?php endif; ?>

  </div>


  <!-- Blog Info
  ============================================= -->
  <ul class="blog-info">
    <li><i class="fa fa-user"></i> Por: FHBr</li>
    <li><i class="fa fa-calendar"></i> {blo_data}</li>
    <li><i class="fa fa-search"></i> Fonte: {blo_fonte}</li>
  </ul>
  <!-- END Info -->


  <div class="gridline"></div>


  <!-- Comentarios
  ============================================= -->
  <?php if($query[0]->blo_flagComentario==1): ?>
  <div class="comentarios">
    <div class="title_template margin-bottom-20">COMENTÁRIOS DA <b><?php echo strtoupper(singular($slugTipoPagina)); ?></b></div>
    <div class="box">
        <div class="fb-comments" data-href="<?php echo $urlPost; ?>" data-width="100%" data-numposts="5" data-colorscheme="light"></div>  
    </div>
  </div>
  <?php endif; ?>
  <!-- END Comentários -->


  <div class="gridline"></div>


  <!-- Posts Relacionados
  ============================================= -->
  <?php if(!empty($postsRelacionados)): ?>
  <div class="relacionadas">

    <div class="row">
      <div class="col-sm-12">
        <div class="title_template margin-bottom-20"><?php echo strtoupper($slugTipoPagina); ?> <b>RELACIONADAS</b></div>
      </div>
    </div>

    <div class="box">
      <?php foreach ($postsRelacionados as $postRelacionado): ?>
      <div class="col-sm-4 padding-left-0">
        <a href="<?php echo base_url() . $slugTipoPagina . '/' . $slugCategoria . '/' . $postRelacionado->blo_slug; ?>" title="<?php echo $postRelacionado->blo_titulo; ?>" class="thumb">
          <img src="<?php echo base_url() . 'manager/uploads/blog/thumbs/' . $postRelacionado->blo_thumb; ?>" class="img-responsive" />
        </a>
        <div class="col-sm-6 data padding-left-0"><span><?php echo $this->my_date->datetime($postRelacionado->criado, 'justDate'); ?></span></div>
        <a href="<?php echo base_url() . $slugTipoPagina . '/' . $slugCategoria . '/' . $postRelacionado->blo_slug; ?>" title="<?php echo $postRelacionado->blo_titulo; ?>" class="titulo">
          <?php echo $postRelacionado->blo_titulo; ?>
        </a>
      </div>
      <?php endforeach; ?>
    </div>

  </div>
  <?php endif; ?>

</div>
