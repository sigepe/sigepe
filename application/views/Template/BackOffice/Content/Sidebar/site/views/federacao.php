<!-- BEGIN Federacao -->
<div class="col-sm-9 padding-left-0 federacao">
	
	<ul class="breadcrumb">
	    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a></li>
	    <li><a href="<?php echo base_url() . 'afederacao/historia'; ?>">A Federação</a></li>
	    <li class="active"><?php echo $tituloPagina; ?></li>
	</ul>	

	<h1><?php echo $tituloPagina; ?></h1>
	
	<div class="conteudo margin-bottom-30">
		<?php echo $conteudo; ?>
	</div>


</div>
<!-- END Federacao -->
