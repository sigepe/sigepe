

    <!-- Informacoes Gerais
    ============================================= -->
	<div class="row informacoesgerais margin-bottom-30 float-left">

		<div class="col-sm-12">
		    <div class="title_template">INFORMAÇÕES <b>GERAIS</b></div>
		    <div class="logo col-sm-3"><div class="box"><img src="<?php echo base_url() . 'uploads/evento/' . $evento->eve_logo; ?>" class="img-responsive" /></div></div>
		    <div class="descricao col-sm-9">
		    	<?php 
		    		if(is_null($evento->eve_descricao)){
						echo "Nenhuma descrição para este evento";		    			
		    		}else{
		    			echo $evento->eve_descricao;
		    		}
		    	?>
		    </div>
		</div>


		<div class="col-sm-12">
			
			<div class="col-sm-<?php echo ($counter==true) ? '5' : '12'; ?> no-padding detalhes">
				<div class="row">

				    <!-- Periodo
				    ============================================= -->
					<?php if($periodo): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Período:</div>
							<div class="col-sm-6 value">
				        		<?php echo $this->my_date->datetime($evento->eve_datainicio, 'justDate'); ?>
		        				<?php if(!empty($evento->eve_datafim)): ?>
				        		a <?php echo $this->my_date->datetime($evento->eve_datafim, 'justDate'); ?>
					        	<?php endif; ?>
							</div>
						</div>
					</div>
					<?php endif; ?>


				    <!-- Data limite de pagamento com desconto especial
				    ============================================= -->
					<?php if($eve_data_limite_pagamento_desconto_especial): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Data Limite de Pagamento com Desconto Especial:</div>
							<div class="col-sm-6 value"><?php echo $this->my_date->datetime($evento->eve_data_limite_pagamento_desconto_especial, 'justDate'); ?></div>
						</div>
					</div>
					<?php endif; ?>


				    <!-- Data limite de pagamento com desconto
				    ============================================= -->
					<?php if($eve_data_limite_pagamento_com_desconto): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Data Limite de Pagamento com Desconto:</div>
							<div class="col-sm-6 value"><?php echo $this->my_date->datetime($evento->eve_data_limite_pagamento_com_desconto, 'justDate'); ?></div>
						</div>
					</div>
					<?php endif; ?>


				    <!-- Data encerramento inscrição ( datafim )
				    ============================================= -->
					<?php if($eve_data_encerra_inscricao): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Data Encerramento de Inscrição:</div>
							<div class="col-sm-6 value"><?php echo $this->my_date->datetime($evento->eve_data_encerra_inscricao, 'justDate'); ?></div>
						</div>
					</div>
					<?php endif; ?>


				    <!-- Presidente do Juri Presidente
				    ============================================= -->
					<?php if($eve_juri_presidente): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Presidente do Juri:</div>
							<div class="col-sm-6 value"><?php echo $evento->eve_juri_presidente; ?></div>
						</div>
					</div>
					<?php endif; ?>


				    <!-- Membros do Juri de Campo
				    ============================================= -->
					<?php if($eve_juri_membros): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Membro Juri de Campo:</div>
							<div class="col-sm-6 value"><?php echo $evento->eve_juri_membros; ?></div>
						</div>
					</div>
					<?php endif; ?>


				    <!-- Delegado Tecnico
				    ============================================= -->
					<?php if($eve_delegado_tecnico): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Delegado Técnico:</div>
							<div class="col-sm-6 value"><?php echo $evento->eve_delegado_tecnico; ?></div>
						</div>
					</div>
					<?php endif; ?>


				    <!-- Desenhador de Percurso
				    ============================================= -->
					<?php if(!is_null($eve_armador)): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Desenhador de Percurso:</div>
							<div class="col-sm-6 value"><?php echo $evento->eve_armador; ?></div>
						</div>
					</div>
					<?php endif; ?>


				    <!-- Status do Evento
				    ============================================= -->
					<?php if(!is_null($fk_sta_id)): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Status do Evento:</div>
							<div class="col-sm-6 value"><span class="btn btn-xs <?php echo $status_bootstrap; ?>"><?php echo $status_titulo; ?></span></div>
						</div>
					</div>
					<?php endif; ?>

				</div>
				<!-- /.row -->

			</div>
			<!-- /.col-sm-5/12 -->


		    <!-- Counter
		    ============================================= -->
			<?php if($counter==TRUE): ?>

				<!-- Inscricoes em Breve -->
				<?php if($evento->fk_sta_id==1): ?>
				<div class="col-sm-7 contador status1">
					
					<h3>INSCRIÇÕES <b>ABREM</b> EM</h3>
					<div id="counter"></div>
					<div class="desc">
						<div>Dia(s)</div>
						<div>Hora(s)</div>
						<div>Minuto(s)</div>
						<div>Segundo(s)</div>
					</div>
				</div>
				<?php endif; ?>


				<!-- Inscricoes Abertas -->
				<?php if($evento->fk_sta_id==2): ?>
				<div class="col-sm-7 contador status2">
					<h3>INSCRIÇÕES <b>ENCERRAM</b> EM</h3>
					<div id="counter"></div>
					<div class="desc">
						<div>Dia(s)</div>
						<div>Hora(s)</div>
						<div>Minuto(s)</div>
						<div>Segundo(s)</div>
					</div>
					<div class="btninscricao col-sm-12">
						<a href="<?php echo $evento->eve_inscricao ?>" class="btn btn-success btn-block" target="_blank">
							Inscrever agora
						</a>
					</div>
				</div>
				<?php endif; ?>


				<!-- Inscricoes Encerradas -->
				<?php if($evento->fk_sta_id==3): ?>
				<div class="col-sm-7 contador status3">

					<div class="box">
						<span class="label label-default">Inscriçõe Encerradas</span>
					</div>

					<h3><b>CONTAGEM REGRESSIVA</b> PARA INICIAR O EVENTO:</h3>
					<div id="counter"></div>
					<div class="desc">
						<div>Dia(s)</div>
						<div>Hora(s)</div>
						<div>Minuto(s)</div>
						<div>Segundo(s)</div>
					</div>
	<!-- 				<div class="btninscricao col-sm-12">
						<a href="<?php echo $evento->eve_inscricao ?>" class="btn btn-success btn-block" target="_blank">
							Inscrever agora
						</a>
					</div> -->
				</div>
				<?php endif; ?>


				<!-- Evento Acontecendo -->
				<?php if($evento->fk_sta_id==4): ?>
				<div class="col-sm-7 contador status4">

					<div class="box">
						<span class="label label-default">Evento acontecendo</span>
					</div>

					<div class="content">
						<h3>ACOMPANHE A <b>PROVA ONLINE</b></h3>
					</div>
				
				</div>
				<?php endif; ?>


				<!-- Evento Encerrado -->
				<?php if($evento->fk_sta_id==5): ?>
				<div class="col-sm-7 contador status5">

					<div class="box">
						<span class="label label-default">Evento Encerrado</span>
					</div>

					<div class="content">
						<h3>CONFIRA O <b>RESULTADO DO EVENTO:</b> <a href="#" class="btn btn-default">Clique Aqui</a></h3>
					</div>
				
				</div>
				<?php endif; ?>

			<?php endif; ?>

		</div>
		<!-- /.col-sm-12 -->

	</div>
	<!-- /.informacoesgerais -->

