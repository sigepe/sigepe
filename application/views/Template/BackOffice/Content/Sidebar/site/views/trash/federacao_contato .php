<!-- BEGIN Contato -->
<div class="col-sm-9 padding-left-0" id="page_contato">
	
	<ul class="breadcrumb">
	    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a></li>
	    <li class="active">Contato</li>
	</ul>	

	<h1>Contato</h1>
	
	<div class="conteudo margin-bottom-30">
	    <img src="<?php echo base_url(); ?>assets/frontend/layout/img/afederacao/historia.jpg" alt="" class="img-responsive" style="float:left;margin-right:20px;margin-bottom:20px;border:1px solid #ccc;padding:2px;" />
		Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of “de Finibus Bonorum et Malorum” (The Extremes of Good and Evil) by Cicero, written in 45 BC. 
		<br />
		<br />
		This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, “Lorem ipsum dolor sit amet..”, comes from a line in section 1.10.32.
		The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from “de Finibus Bonorum et Malorum” by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.or those interested. Sections 1.10.32 and 1.10.33 from “de Finibus Bonorum et Malorum” by Cicero.
	</div>

	<div class="gestao">
		<h3><b>Atual Gestão</b></h3>
		<h4><b>Lema:</b> Empreender e Inovar</h4>
		<h4><b>Gestão:</b> Triênio 2015/2017</h4>
        <table class="tabela no-margin margin-top-20" cellspacing='0'>
            <tr><th>Cargo</th><th>Nome</th></tr><!-- Table Header -->
            <tr class=""><td>Presidente</td><td>Luiz Roberto Giugni</td></tr>
            <tr class="even"><td>Vice-Presidente</td><td>Ronaldo Bittencourt Filho </td></tr>
            <tr class=""><td>Secretario Geral</td><td>Ronaldo Bittencourt Filho></tr>
            <tr class="even"><td>Primeiro Secretário</td><td>Carla Rosana de Paula</td></tr>
            <tr class=""><td>Diretor Financeiro</td><td>João Gilberto Cominese Freire</tr>
            <tr class="even"><td>Diretor Veterinário</td><td>Thomas Walter Wolff</td></tr>
            <tr class=""><td>Diretor Jurídico</td><td>Sandra M. M. Neves Piva</tr>
        </table>
	</div>

</div>
<!-- END Contato -->
