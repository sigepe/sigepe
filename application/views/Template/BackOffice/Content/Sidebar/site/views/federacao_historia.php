<!-- BEGIN Historia -->
<div class="col-sm-9 padding-left-0 federacao" id="federacao_historia">
	
	<ul class="breadcrumb">
	    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a></li>
	    <li><a href="<?php echo base_url() . 'afederacao/historia'; ?>">A Federação</a></li>
	    <li class="active">História</li>
	</ul>	

	<h1>História</h1>
	
	<div class="conteudo margin-bottom-30">

		<p>Com a mudan&ccedil;a da capital federal para Bras&iacute;lia houve um crescimento do hipismo cl&aacute;ssico e o surgimento de diversos Clubes equestres na Capital Federal. Ocorreu assim, o aprimoramento da equita&ccedil;&atilde;o pelos praticantes, alcan&ccedil;ado elevados n&iacute;veis no esporte, tornando necess&aacute;ria e indispens&aacute;vel a exist&ecirc;ncia de um &oacute;rg&atilde;o regulador, filiado &agrave; Confedera&ccedil;&atilde;o Brasileira de Hipismo no Distrital Federal. Tal iniciativa, deu-se por um grupo de pessoas abnegadas pela arte equestre, que representavam os clubes: Bras&iacute;lia Country Clube, Sociedade H&iacute;pica de Bras&iacute;lia, Jockey Clube de Bras&iacute;lia e o Clube das Na&ccedil;&otilde;es. Em 23 de fevereiro de 1967 foi fundada a Federa&ccedil;&atilde;o H&iacute;pica de Bras&iacute;lia, como uma entidade sem fins lucrativos com a finalidade de promover e dirigir, no mesmo territ&oacute;rio, provas, competi&ccedil;&otilde;es e campeonatos do referido desporto, em todas as modalidades, respeitando, cumprindo e fazendo cumprir as leis e regulamentos internacionais adotados pela pr&oacute;pria FHBr, Confedera&ccedil;&atilde;o Brasileira de Hipismo - CBH e Federa&ccedil;&atilde;o Equestre Internacional - FEI.</p> <p><strong>RELA&Ccedil;&Atilde;O DOS EX PRESIDENTES DA FHBr.</strong><br /> 70/72 &ndash; Gabriel A. Botafogo Ribeiro<br /> 72/74 &ndash; Valdoir Menezes Ferreira<br /> 74/75 &ndash; Adir Cunha<br /> 75/76 &ndash; Fidelis Chaves da Silveira<br /> 76/78 &ndash; Antenor Santa Cruz Abreu<br /> 78/81 &ndash; Heitor Cesar Pimenta<br /> 81/13 &ndash; Paulo Azambuja de Oliveira<br /> 83/85 &ndash; Luiz Fernandes Marcondes Albuquerque<br /> 86/87 - Valdoir Menezes Ferreira<br /> 88/89 &ndash; Paulo Azambuja de Oliveira<br /> 90/91 &ndash; Jose Luiz Spino<br /> 92/93 &ndash; Jose Luiz Spino<br /> 94/95 &ndash; Ant&ocirc;nio Joao Azambuja<br /> 96/97 &ndash; Ant&ocirc;nio Joao Azambuja<br /> 98/99 &ndash; Paulo Gustavo Magalhaes Pinto<br /> 00/01 &ndash; Ant&ocirc;nio Joao Azambuja<br /> 02/03 &ndash; Orlando Rodrigues da Cunha<br /> 04/05 &ndash; Orlando Rodrigues da Cunha<br /> 06/07 &ndash; Ronaldo Bittencourt Filho<br /> 08/09 &ndash; Ronaldo Bittencourt Filho<br /> 10/11 &ndash; Luiz Felipe Ribeiro Coelho<br /> 2012 &ndash; Ronaldo Bittencourt Filho<br /> 2013 - Flavio Grillo Ara&uacute;jo</p> <p>&nbsp;</p> <p><u>ENTIDADES FILIADAS ATUALMENTE</u></p> <ul> <li>Sociedade H&iacute;pica De Bras&iacute;lia;</li> <li>Bras&iacute;lia Country Club;</li> <li>Centro H&iacute;pico Do Parque;</li> <li>Associa&ccedil;&atilde;o Nacional De Equoterapia;</li> <li>Parque H&iacute;pico De Bras&iacute;lia;</li> <li>Centro H&iacute;pico Lago Sul.</li> <li>Centro De Prepara&ccedil;&atilde;o H&iacute;pico Do Gama;</li> <li>Centro De Treinamento Do Trotador De &Aacute;guas Claras;</li> <li>Manege Cabral &ndash; MC</li> <li>Manege Alonso Equestres - MAE</li> <li>Regimento De Cavalaria De Guardas (1&ordm; RCG);</li> <li>Regimento De Pol&iacute;cia Montada (RPMon).</li> </ul> <p>&Oacute;rg&atilde;o m&aacute;ximo do esporte nacional, a Confedera&ccedil;&atilde;o Brasileira de Hipismo (CBH) &eacute; respons&aacute;vel pela regulamenta&ccedil;&atilde;o, coordena&ccedil;&atilde;o, promo&ccedil;&atilde;o e fomento de 8 dos esportes h&iacute;picos praticados no Pa&iacute;s: Adestramento, Atrelagem, Concurso Completo de Equita&ccedil;&atilde;o, Enduro, Equita&ccedil;&atilde;o Especial (Paraequestre), R&eacute;deas, Volteio e Salto.</p> <p>Respondendo por estes esportes junto a FEI &ndash; Federa&ccedil;&atilde;o Equestre Internacional e aos &oacute;rg&atilde;os governamentais, a CBH &eacute; respons&aacute;vel, ainda, pela forma&ccedil;&atilde;o das equipes brasileiras que representam o Pa&iacute;s em competi&ccedil;&otilde;es internacionais, pela realiza&ccedil;&atilde;o de campeonatos, seletivas e cursos, pela chancela de eventos promovidos por federa&ccedil;&otilde;es estaduais, pela capta&ccedil;&atilde;o e administra&ccedil;&atilde;o de verbas junto a &oacute;rg&atilde;os governamentais e COB &ndash; Comit&ecirc; Ol&iacute;mpico Brasileiro.</p> <p>Hoje, 20 federa&ccedil;&otilde;es estaduais, al&eacute;m da CDE - Comiss&atilde;o de Desportos do Ex&eacute;rcito - Vila Militar - Deodoro - Rio de Janeiro (RJ) respondem diretamente &agrave; CBH.</p>
		
	</div>

	<div class="gestao">
		<h3><b>Atual Gestão</b></h3>
		<h4><b>Lema:</b> Empreender e Inovar</h4>
		<h4><b>Gestão:</b> Triênio 2015/2017</h4>
        <table class="tabela no-margin margin-top-20" cellspacing='0'>
            <tr><th>Cargo</th><th>Nome</th></tr><!-- Table Header -->
            <tr class=""><td>Presidente</td><td>Luiz Roberto Giugni</td></tr>
            <tr class="even"><td>Vice-Presidente</td><td>Ronaldo Bittencourt Filho </td></tr>
            <tr class=""><td>Secretario Geral</td><td>Ronaldo Bittencourt Filho></tr>
            <tr class="even"><td>Primeiro Secretário</td><td>Carla Rosana de Paula</td></tr>
            <tr class=""><td>Diretor Financeiro</td><td>João Gilberto Cominese Freire</tr>
            <tr class="even"><td>Diretor Veterinário</td><td>Thomas Walter Wolff</td></tr>
            <tr class=""><td>Diretor Jurídico</td><td>Sandra M. M. Neves Piva</tr>
        </table>
	</div>

</div>
<!-- END Historia -->
