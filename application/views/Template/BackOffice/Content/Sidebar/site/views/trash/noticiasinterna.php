<?php include("header.php"); ?>

<div class="container-fluid" id="pagina-noticiasinterna">      

  <div class="container">
      
    <!-- BREADCRUMBS -->
    <ul class="breadcrumb">
        <li><a href="index.html">Site</a></li>
        <li><a href="index.html">Agenda</a></li>
        <li class="active">3º Copa de Futebol da AERS</li>
    </ul>


    <!-- HEADER DA AGENDA -->
    <div id="pagina-noticiasinterna-header">

      <div class="col-sm-7">
        <h1>3º Copa de Futebol da AERS</h1>
      </div>

      <!-- INÍCIO: REDESSOCIAIS -->
      <div class="col-sm-5 redessociais">

        <div id="ferramentas">
          
          <div id="social">
              
              <!-- FACEBOOK -->
              <div 
                  class="fb-like" 
                  data-href="http://www.go.senac.br/portal/noticia/8404-servidores-do-senac-aparecida-de-goiania-arrecadam-e-doam-produtos"
                  data-width="150" 
                  data-layout="button_count" 
                  data-action="like" 
                  data-show-faces="true"
                  data-share="false">
              </div>
              
              <!-- TWITTER -->
              <a 
              href="http://www.go.senac.br/portal/noticia/8404-servidores-do-senac-aparecida-de-goiania-arrecadam-e-doam-produtos" 
              class="twitter-share-button" 
              data-lang="en">Tweet</a>
              
              <!-- GOOGLE PLUS -->
              <div class="g-plusone" data-size="medium"></div>
              
              <!-- ADDTHIS -->
              <div class="addthis_toolbox addthis_default_style ">
                 <a href="http://www.addthis.com/bookmark.php" class="addthis_button"  style="text-decoration:none;"> + compartilhar</a> 
              </div>
              <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=xa-4efa1b0c111a32e5"></script>
              
          </div>
          
        </div>

      </div>
      <!-- FIM: REDESSOCIAIS -->

      <div class="col-sm-12 info">
          <span> Publicado em: 03/10/2014 às 15:04:10 <br />
      </div>

    </div>
    <!-- FIM: HEADER -->  




    <!-- CONTEÚDO -->
    <div class="conteudo">

      <img src="assets/images/noticias/4.jpg" class="foto-principal" />

      Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of “de Finibus Bonorum et Malorum”.
      <br />
      <br />
      The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, “Lorem ipsum dolor sit amet..”, comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from “de Finibus Bonorum et Malorum.
      <br />
      <br />
      There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don’t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn’t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.
        
      <div class="btn-fotos">
        <a href="#" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-camera"></span> CONFIRA AS FOTOS</a>      
      </div>


    </div>
    <!-- FIM CONTEÚDO -->

    <div class="espacamento">
      <hr class="separador" />
    </div>


    <!-- COMENTÁRIOS -->
    <div class="comentarios">
      <div class="col-sm-1"><img src="assets/images/agenda/chat.png" /></div>
      <div class="col-sm-11 red-title">Comentários</div>
      
      <div class="box">
        <div class="col-sm-1"></div>     
        <div class="col-sm-11">
          <div class="fb-comments" data-href="http://developers.facebook.com/docs/plugins/comments/" data-width="1035" data-numposts="5" data-colorscheme="light"></div>  
        </div>
      </div>

    </div>

    <!-- NOTICIAS RELACIONADAS -->
    <div class="relacionadas">

      <div class="row">
        <div class="col-sm-1"><img src="assets/images/noticias/relacionadas.png" /></div>
        <div class="col-sm-11 red-title">Notícias Relacionadas</div>
      </div>

      <div class="box">
        <div class="col-sm-3">
          <a href="#" title="" class="thumb"><img src="http://extrema.mg.gov.br/site/wp-content/uploads/2011/09/alunos.jpg" /></a>
          <div class="col-sm-6 data">03/10/2014</div>
          <div class="col-sm-6 categoria"><a href="#" title="">PARCERIA</a></div>
          <a href="#" title="" class="titulo">Escola Cidadã une Cmeis, Senac Caldas Novas e Cebrom</a>
        </div>
        <div class="col-sm-3">
          <a href="#" title="" class="thumb"><img src="http://www.rccsc.com.br/area/img/noticias/a04fbada35d20afa25f9aa94cd4e8535.jpg" /></a>
          <div class="col-sm-6 data">03/10/2014</div>
          <div class="col-sm-6 categoria"><a href="#" title="">PARCERIA</a></div>
          <a href="#" title="" class="titulo">Escola Cidadã une Cmeis, Senac Caldas Novas e Cebrom</a>
        </div>
        <div class="col-sm-3">
          <a href="#" title="" class="thumb"><img src="http://www.i-decoracao.com/Uploads/i-decoracao.com/ImagensGrandes/imagens-arvores-natal.jpg" /></a>
          <div class="col-sm-6 data">03/10/2014</div>
          <div class="col-sm-6 categoria"><a href="#" title="">PARCERIA</a></div>
          <a href="#" title="" class="titulo">Escola Cidadã une Cmeis, Senac Caldas Novas e Cebrom</a>
        </div>
        <div class="col-sm-3">
          <a href="#" title="" class="thumb"><img src="http://extrema.mg.gov.br/site/wp-content/uploads/2011/09/alunos.jpg" /></a>
          <div class="col-sm-6 data">03/10/2014</div>
          <div class="col-sm-6 categoria"><a href="#" title="">PARCERIA</a></div>
          <a href="#" title="" class="titulo">Escola Cidadã une Cmeis, Senac Caldas Novas e Cebrom</a>
        </div>
      </div>


    </div>


  </div>
  <!-- FIM CONTAINER -->

</div>
<!-- FIM CONTAINER-FLUID -->



<?php include("footer.php"); ?>
