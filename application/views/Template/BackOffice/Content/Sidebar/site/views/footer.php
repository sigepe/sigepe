
      <div class="row">
        
        <div class="col-sm-12" style="float:left;">

        <!-- BEGIN parceiros -->
        <div class="col-sm-6 footer-nossosparceiros padding-left-0">

          <div class="col-sm-12">
            <div class="title_template margin-bottom-20">
              NOSSOS <b>PARCEIROS</b>
            </div>
          </div>

          <div id="owl-parceiros" class="owl-carousel footer-owl" style="float:left;">

    
            <?php foreach ($parceiros as $parceiro): ?>

            <?php 
              $titulo_parceiro_footer      = $parceiro->par_parceiro;
              $descricao_parceiro_footer   = $parceiro->par_descricao;
              $img_parceiro_footer         = base_url() . 'manager/uploads/parceiro/' . $parceiro->par_thumb;
              $link_parceiro_footer        = $parceiro->par_link;
            ?>

            <div>

              <?php if(!is_null($link_parceiro_footer)): ?>    
              <a href="<?php echo $link_parceiro_footer; ?>" title="<?php echo $titulo_parceiro_footer; ?>" target="_blank">
              <?php endif; ?>    
  
                <img src="<?php echo $img_parceiro_footer; ?>" alt="" class="img-responsive" />
  
              <?php if(!is_null($link_parceiro_footer)): ?>    
              </a>
              <?php endif; ?>    
  
            </div>

            <?php endforeach; ?>


          </div>
          <!-- ./end owl-parceiros -->

          </div>
          <!-- ./end footer-nossosparceiros -->


        <!-- BEGIN entidades -->
        <div class="col-sm-6 footer-nossasentidades padding-right-0 hidden-xs">

          <div class="col-sm-12 no-padding">
            <div class="title_template margin-bottom-20">NOSSAS <b>ENTIDADES</b></div>
          </div>

          <div id="owl-entidades" class="owl-carousel footer-owl" style="float:left;">
         
            <?php foreach ($entidadesescolas_rodape as $entidadeescola_rodape): ?>

            <?php 
            $titulo_footer   = $entidadeescola_rodape->ent_titulo;
            $img_footer    = base_url() . 'manager/uploads/entidade/thumbs/' . $entidadeescola_rodape->ent_logo;
            $link_footer     = base_url() . 'a-federacao/entidades-filiadas/' . $entidadeescola_rodape->ent_slug;
            ?>

            <div>
              <a href="<?php echo $link_footer; ?>" title="<?php echo $titulo_footer; ?>">
                <img src="<?php echo $img_footer; ?>" alt="" class="img-responsive" />
              </a>
            </div>

            <?php endforeach; ?>

          </div>

        </div>
        <!-- END entidades -->

        </div>
        <!-- END col-sm-12 -->

        </div>
        <!-- END row -->
      </div>
      <!-- End Container ( opened header.php )  -->

    </div>
    <!-- End Main ( opened header.php )  -->

    <!-- BEGIN PRE-FOOTER -->
    <div class="pre-footer">
 
      <div class="container">

        <div class="row">

          <!-- BEGIN BOTTOM ABOUT BLOCK -->
          <div class="col-md-4 col-sm-6 pre-footer-col padding-left-0 visible-md visible-lg">

            <div class="col-sm-12 title padding-left-0">
              <img src="<?php echo base_url(); ?>assets/frontend/layout/img/footer/facebook.png" alt="" class="img-responsive" />
            </div>

            <div class="col-sm-12 box">
              <div class="fb-page" data-href="https://www.facebook.com/pages/Federa%C3%A7%C3%A3o-H%C3%ADpica-de-Bras%C3%ADlia/914503305251147?fref=ts" data-height="250" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"></div>
            </div>

          </div>
          <!-- END BOTTOM ABOUT BLOCK -->

          <!-- BEGIN BOTTOM CONTACTS -->
          <div class="col-md-4 col-sm-6 pre-footer-col visible-md visible-lg">

            <div class="col-sm-12 title padding-left-0">
              <img src="<?php echo base_url(); ?>assets/frontend/layout/img/footer/instagram.png" alt="" class="img-responsive" />
            </div>

            <div class="col-sm-12 box">
              
              <div class="widget_instagram">
                <iframe src="http://www.intagme.com/in/?u=Zmhicl9oaXBpc21vfGlufDc3fDR8Mnx8eWVzfDV8dW5kZWZpbmVkfHllcw==" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:368px; height: 184px" ></iframe>
              </div>

            </div>

          </div>
          <!-- END BOTTOM CONTACTS -->

          
          <!-- BEGIN SOCIALNETWORKS -->
          <div class="col-sm-12 visible-xs footer_socialnetworks">

            <div class="row">
              <div class="col-sm-12">
                <a href="https://www.facebook.com/pages/Federa%C3%A7%C3%A3o-H%C3%ADpica-de-Bras%C3%ADlia/914503305251147?ref=ts&fref=ts" target="_blank" class="item">
                  <i class="fa fa-facebook"></i>
                  Facebook
                </a>
                <a href="http://instagram.com/fhbr_hipismo" class="item instagram" target="_blank">
                  <i class="fa fa-instagram"></i>
                  Instagram
                </a>
              </div>
            </div>

          </div>
          <!-- END SOCIALNETWORKS -->

          <!-- BEGIN TWITTER BLOCK --> 
          <div class="col-md-4 col-sm-6 pre-footer-col enquete padding-right-0">

            <div class="col-sm-12 title padding-left-0">
              <img src="<?php echo base_url(); ?>assets/frontend/layout/img/footer/enquete.png" alt="" class="img-responsive" />
              <span>Enquete</span>
            </div>

            <div class="col-sm-12 box enquete_box">
  
              <form action="#" method="post" id="form_enquete">

                <!-- Perguntas -->
                <p class="pergunta">
                  <?php echo $enquete_pergunta; ?>
                </p>

                <!-- Respostas -->
                <?php
                  $respostas = $this->m_crud->get_allWhereTwoParametersOrderby('tb_site_enquete_resposta', 'fk_per_id', $enquete_id_pergunta, 'res_status', '1', 'res_posicao', 'asc');
                  foreach ($respostas as $resposta):
                ?>
                <ul id="enquete_resposta">
                  <li>
                    <input type="radio" name="enquete" value="<?php echo $resposta->res_id; ?>" id="<?php echo $resposta->res_id; ?>" />
                    <label for="<?php echo $resposta->res_id; ?>"><?php echo $resposta->res_resposta; ?></label>
                  </li>
                </ul>
                <?php endforeach; ?>

                <!-- Botões -->
                <div id="enquete_botoes">
                  <a href="#" class="btn btn-sm btn-primary" id="enquete_votar">Votar</a>
                  <!-- <a href="#" class="btn btn-sm btn-default" id="enquete_btnresultado">Ver Resultado</a> -->
                </div>

              </form>

            </div>

          </div>
          <!-- END TWITTER BLOCK -->
        </div>
      </div>
    </div>
    <!-- END PRE-FOOTER -->

    <!-- BEGIN FOOTER -->
    <div class="footer">
      <div class="container">
        <div class="row">
          
          <!-- BEGIN COPYRIGHT -->
          <div class="col-md-8 col-sm-8 esquerdo">

            <div class="col-sm-3">
              <a class="" href="<?php echo base_url(); ?>">
                <img src="<?php echo base_url(); ?>assets/frontend/layout/img/logos/logo-header.png" alt="" class="img-responsive" />
              </a>
            </div>

            <div class="copyright col-sm-9">
              <div><b>Endereço:</b> SHIP Sul Lote nº 08, Brasília - DF - CEP 70610-900  <span><b>Telefone:</b> (61) 3245-5870</span></div>
              <div>Copyright © FHBr- Federação Hípica de Brasília</div>
            </div>

          </div>
          <!-- END COPYRIGHT -->

          <!-- BEGIN PAYMENTS -->
          <div class="col-md-4 col-sm-4 direito vanguarda">

            <span>produzido por:</span>
            <a href="http://souvanguarda.com.br" title="" target="_blank">
              <img src="<?php echo base_url(); ?>assets/frontend/layout/img/footer/vanguarda.png" alt="" class="img-responsive" />
            </a>

          </div>
          <!-- END PAYMENTS -->

        </div>
      </div>
    </div>
    <!-- END FOOTER -->

  </div>
  <!-- END SB-SLIDERS -->

    <div class="sb-slidebar sb-left">
      <!-- Your left Slidebar content. -->
      <div class="sb-slidebar-conteudomenu">

      </div>
    </div>


  <!-- Modal -->
  <div class="modal fade" id="modal-resultado-enquete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Enquete</h4>
        </div>
        <div class="modal-body">

              <!-- BEGIN enquete_loading -->
              <div id="enquete_loading" style="display: block; float: left; width: 100%; height: auto; text-align: center; padding: 50px 0;">
                <i class="fa fa-spinner fa-pulse fa-5x"></i>
              </div>
              <!-- END enquete_loading -->              

              <!-- BEGIN enquete_resultados -->
              <div id="enquete_resultados" style="display:none;">

                <div class="alert alert-success" role="alert" id="voto_computado" style="display:none;">
                  <b>Voto computado!</b> <br /> Parabéns, seu voto foi processado com sucesso em <?php echo $this->my_date->datetime(date("Y-m-d H:i:s"), 'justDate'); ?>. <br /><br />
                  <small><b>IP Origem:</b> <?php echo $_SERVER['REMOTE_ADDR']; ?></small>
                </div>

                <div class="alert alert-danger" role="alert" id="voto_nao_computado" style="display:none;">
                  <b>Voto NÃO computado!</b> <br />
                  Você votou nessa enquete em: <span id="voto_nao_computado_datahora"></span>. <br /> É válido apenas um voto por dispositivo. Fique atento a próxima.<br /><br />
                  <small><b>IP Origem:</b> <span id="voto_nao_computado_ip_origem"></span></small>
                </div>

                <h3>O que você achou do evento de premiação do Encerramento do Ranking 2014 de Salto e Adestramento da FHBr?</h3>
                <hr />

              <?php
                $enquete_contador = 4;
                $resultados = $this->db->query(
                              'SELECT res_id, res_resposta,
                              COUNT(*) as qtdeVotos
                              FROM tb_site_enquete_resposta as resposta
                              /*WHERE fk_per_id = {per_pergunta} */
                              LEFT JOIN tb_site_enquete_voto as voto
                              ON voto.fk_res_id = resposta.res_id
                              GROUP BY res_resposta
                              ORDER BY resposta.res_posicao ASC;')->result();
                
                $somaQtdeVotos = 0;
                foreach ($resultados as $foreachSoma) {
                  
                  $somaQtdeVotos += $foreachSoma->qtdeVotos;

                }

                foreach ($resultados as $resultado):
                $porcentagemVoto = round(( $resultado->qtdeVotos / $somaQtdeVotos ) * 100, 2);
                $porcentagemItem = $porcentagemVoto . "%";
              ?>

              <?php

              switch ($enquete_contador) {
                case 1:
                  $enquete_color = 'progress-bar-success'; 
                  break;
                case 2:
                  $enquete_color = 'progress-bar-info'; 
                  break;
                case 3:
                  $enquete_color = 'progress-bar-warning'; 
                  break;
                case 4:
                  $enquete_color = 'progress-bar-danger'; 
                  break;
              }
              ?>
                <?php echo $resultado->res_resposta; ?>
                <div class="progress">
                  <div class="progress-bar <?php echo $enquete_color; ?>" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $porcentagemVoto; ?>%"><?php echo $porcentagemItem; ?></div>
                </div>

              <?php $enquete_contador--; ?>
              <?php endforeach; ?>

              </div>
              <!-- END enquete_resultados -->

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>






    <!-- Modal Loading Default
    ============================================= -->
    <div id="modalLoading" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
        <div class="modal-body" style="text-align:center;">
            <i class="fa fa-refresh fa-spin" style="margin: 80px auto; text-align: center; font-size: 10em; color: #fff;"></i>
             <h2 style="font-weight:900;color:white;">Aguarde,</h2>
             <div style="text-align: center; width: 80%; color: #fff; font-size: 15px; margin: 10px auto 30px auto;">estamos processando as informações. Pode demorar alguns instantes.</div>
        </div>
    </div>


    <!-- Modal Successfully Contact
    ============================================= -->
    <div class="modal fade " tabindex="-1" role="dialog"  aria-hidden="true" id="modalContactSucessfully">
        <div class="modal-dialog modal-md">
            <div class="modal-body">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Atendimento</h4>
                    </div>

                    <div class="modal-body">

                        <h1> <span id="leadNameContact"></span>, </h1>
                       
                        <blockquote>
                            <div class="row">
                                <div class="col-sm-12">
                                    obrigado pelo seu contato, em até 48 horas entraremos em contato.
                                </div>
                            </div>
                        </blockquote>

                        <hr />

                    </div>

                </div>
            </div>
        </div>
    </div>











    <!-- Variable Environments
    =========================================== -->
    <script type="text/javascript">
    /* Baseurl */
    var baseUrl               = '<?php echo base_url(); ?>';

    /* Map */
    var global_map_title      = '<?php echo $global_map_title; ?>';
    var global_map_content    = '<?php echo $global_map_content; ?>';
    var global_map_latitude   = '<?php echo $global_map_latitude; ?>';
    var global_map_longitude  = '<?php echo $global_map_longitude; ?>';

    /* Counter */
    var counterAno            = '<?php echo $counterAno; ?>';
    var counterMes            = '<?php echo $counterMes; ?>';
    var counterDia            = '<?php echo $counterDia; ?>';

    /* Enquete */
    var  enquete_id_pergunta  = '<?php echo $enquete_id_pergunta; ?>';
    </script>


    <!--[if lt IE 9]>
    <script src="<?php echo base_url(); ?>assets/global/plugins/respond.min.js"></script>
    <![endif]-->


    <!-- Core Components Javascript
    ============================================= -->
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>      
    <script src="<?php echo base_url(); ?>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>


    <!-- Fancybox
    ============================================= -->
    <script src="<?php echo base_url(); ?>assets/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->


    <!-- OWL Carousel
    ============================================= -->
    <script src="<?php echo base_url(); ?>assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->


    <!-- Google Maps
    ============================================= -->
    <script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/gmaps/gmaps.js" type="text/javascript"></script>


    <!-- Slidebars
    ============================================= -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/override/slidebars/development/slidebars.css">
    <script src="<?php echo base_url(); ?>assets/override/slidebars/development/slidebars.js"></script>


    <!-- jQuery Validate 1.14.0
    ============================================= -->
    <script src="{base_url}assets/sigepe/plugins/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
    <script src="{base_url}assets/sigepe/plugins/jquery-validation-1.14.0/dist/additional-methods.min.js"></script>
    <script src="{base_url}assets/sigepe/plugins/jquery-validation-1.14.0/trigger.js"></script>


    <!-- jQuery - Datatables
    ============================================= -->
    <script type="text/javascript" charset="utf8" src="{base_url}assets/sigepe/plugins/DataTables-1.10.8/media/js/jquery.dataTables.js"></script>


    <!-- Owl Slider
    ============================================= -->
    <script src="<?php echo base_url(); ?>assets/frontend/layout/plugins/owlslider/owl-carousel/owl.carousel.js"></script>


    <!-- Back to Top
    ============================================= -->
    <script src="<?php echo base_url(); ?>assets/frontend/layout/scripts/back-to-top.js" type="text/javascript"></script>


    <!-- Layout
    ============================================= -->
    <script src="<?php echo base_url(); ?>assets/frontend/layout/scripts/layout.js" type="text/javascript"></script>


    <!-- Countdown
    ============================================= -->
    <script src="<?php echo base_url() . 'assets/override/countdown/js/jquery.countdown.js'; ?>" type="text/javascript"></script>


    <!-- jQuery Mask Plugin
    ============================================= -->
    <script src="{base_url}assets/override/plugins/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js"></script>
    <script src="{base_url}assets/override/plugins/jQuery-Mask-Plugin-master/trigger.js"></script>


    
    <!-- Front Event
    ============================================= -->
    <script src="{base_url}assets/sigepe/scripts/frontEvent.js"></script>


    <!-- Template
    ============================================= -->
    <script type="text/javascript" src="{base_url}assets/sigepe/scripts/template.js"></script>


    <!-- Triggers
    =========================================== -->
    <script type="text/javascript">
        jQuery(document).ready(function() {


            /*
            Layout
            ---------------------*/
            Layout.init();    
            Layout.initFixHeaderWithPreHeader(); /* Switch On Header Fixing (only if you have pre-header) */
            Layout.initNavScrolling();

            /*
            Template
            ---------------------*/
            Template.init(); 
            <?php echo ($current_page=='inicio') ? 'Template.initHome();' : '';  ?> 
            <?php echo ($current_page=='contato') ? 'Template.initContact();' : '';  ?> 


            /*
            Event
            ---------------------*/
            <?php echo ($current_page=='calendario') ? 'FrontEvent.initCalendar();' : '';  ?> /* Calendar */
            <?php echo ($current_page=='evento') ? 'FrontEvent.initEventSpecifc();' : '';  ?> /* Event */    

        });
    </script>
    <!-- END PAGE LEVEL JAVASCRIPTS -->


</body>
</html>