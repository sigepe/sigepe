<div class="container-fluid" id="pagina-noticiasinterna">      

  <div class="container">
      
    <!-- BREADCRUMBS -->
    <ul class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Site</a></li>
        <li><a href="<?php echo base_url() . 'noticias'; ?>">Notícias</a></li>
        <li class="active"><?php echo $titulo; ?></li>
    </ul>

    <!-- HEADER DA AGENDA -->
    <div id="pagina-noticiasinterna-header">

      <div class="col-sm-7 no-padding-left">
        <h1><?php echo $titulo; ?></h1>
      </div>

      <!-- INÍCIO: REDESSOCIAIS -->
      <div class="col-sm-5 redessociais">

        <div id="ferramentas">
          
          <div id="social">
               
              <!-- FACEBOOK -->
              <div 
                  class="fb-like" 
                  data-href="<?php echo base_url() . 'noticia/' . $idNoticia; ?>"
                  data-width="150" 
                  data-layout="button_count" 
                  data-action="like" 
                  data-show-faces="true"
                  data-share="false">
              </div>
              
              <!-- TWITTER -->
              <a 
              href="<?php echo base_url() . 'noticia/' . $idNoticia; ?>" 
              class="twitter-share-button" 
              data-lang="en">Tweet</a>
              
              <!-- GOOGLE PLUS -->
              <div class="g-plusone" data-size="medium"></div>
              
              <!-- ADDTHIS -->
              <div class="addthis_toolbox addthis_default_style ">
                 <a href="http://www.addthis.com/bookmark.php" class="addthis_button"  style="text-decoration:none;"> + compartilhar</a> 
              </div>
              <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=xa-4efa1b0c111a32e5"></script>
              
          </div>
          
        </div>

      </div>
      <!-- FIM: REDESSOCIAIS -->

      <div class="col-sm-12 info no-padding-left">
          <span> Publicado em: <?php echo $this->my_date->datetime($criado, 'justDate'); ?> <br />
      </div>

    </div>
    <!-- FIM: HEADER -->  


    <!-- CONTEÚDO -->
    <div class="conteudo">

<!-- Large modal -->


      
      <?php if($thumb!='nophotoavailable.png'): ?>
        <?php if( $thumb != NULL ) :?>

      <!-- Button trigger modal -->
      <button type="button" data-toggle="modal" data-target=".bs-example-modal-lg" style="padding: 0; margin: 0; background: none; border: 0; float: left;">
        <img src="<?php echo base_url() . 'uploads/noticia/' . $thumb; ?>" class="foto-principal" />
      </button>

      <!-- Modal -->
      <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content" style="text-align:center;">
            <img src="<?php echo base_url() . 'uploads/noticia/' . $thumb; ?>" style="max-width:100%;width:100%;" />
          </div>
        </div>
      </div>

        <?php endif; ?>
      <?php endif; ?>




      <div class="conteudo_box">
        <?php echo $conteudo; ?>
      </div>
        
      <!-- <div class="btn-fotos">
        <a href="#" class="btn btn-primary btn-lg no-float"><span class="glyphicon glyphicon-camera"></span> CONFIRA AS FOTOS</a>      
      </div>
      -->

    </div>
    <!-- FIM CONTEÚDO -->

    <div class="espacamento">
      <hr class="separador" />
    </div>


    <!-- COMENTÁRIOS -->
    <?php if($comentarios==1): ?>
    <div class="comentarios">

      <div class="col-sm-1"><img src="<?php echo base_url(); ?>assets/images/agenda/chat.png" /></div>

      <div class="col-sm-11 red-title">Comentários</div>
      
      <div class="box">
        <div class="col-sm-1"></div>     
        <div class="col-sm-11">
          <div class="fb-comments" data-href="http://www.aers.org.br/?pub=70" data-width="1035" data-numposts="5" data-colorscheme="light"></div>  
        </div>
      </div>

    </div>
    <?php endif; ?>



    <!-- NOTICIAS RELACIONADAS -->
    <div class="relacionadas">

      <div class="row">
        <div class="col-sm-12 red-title">Notícias Relacionadas</div>
      </div>

      <div class="box">
        <?php foreach ($noticias as $noticia): ?>
        <div class="col-sm-3">
          <a href="<?php echo base_url() . 'noticia/' . $noticia->not_id; ?>" title="<?php echo $noticia->not_titulo; ?>" class="thumb"><img src="<?php echo base_url() . 'uploads/noticia/' . $noticia->not_capa; ?>" /></a>
          <div class="col-sm-6 data"><?php echo $this->my_date->datetime($noticia->criado, 'justDate'); ?></div>
          <div class="col-sm-6 categoria"><a href="#" title=""><?php ?></a></div>
          <a href="<?php echo base_url() . 'noticia/' . $noticia->not_id; ?>" title="<?php echo $noticia->not_titulo; ?>" class="titulo"><?php echo $noticia->not_titulo; ?></a>
        </div>
        <?php endforeach; ?>
      </div>


    </div>



  </div>
  <!-- FIM CONTAINER -->

</div>
<!-- FIM CONTAINER-FLUID -->