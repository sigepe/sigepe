<!-- BEGIN Historia -->
<div class="col-sm-12 padding-left-0" id="pagina-modalidades">
	
	<ul class="breadcrumb">
	    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a></li>
	    <li class="active">Modalidades</li>
	</ul>	

	<h1>Modalidades</h1>
	
	<div class="conteudo">

        <div class="col-sm-3">
          <a href="#" title="">
            <img src="<?php echo base_url() . 'uploads/modalidades/b/' . 'atrelagem.png'; ?>" alt="atrelagem" class="img-responsive" />
            <span>Atrelagem</span>
          </a>
        </div>

        <div class="col-sm-3">
          <a href="#" title="">
            <img src="<?php echo base_url() . 'uploads/modalidades/b/' . 'adestramento.png'; ?>" alt="adestramento" class="img-responsive" />
            <span>Adestramento</span>
          </a>
        </div>

        <div class="col-sm-3">
          <a href="#" title="">
            <img src="<?php echo base_url() . 'uploads/modalidades/b/' . 'cce.png'; ?>" alt="cce" class="img-responsive" />
            <span>CCE</span>
          </a>
        </div>

        <div class="col-sm-3">
          <a href="#" title="">
            <img src="<?php echo base_url() . 'uploads/modalidades/b/' . 'enduro.png'; ?>" alt="enduro" class="img-responsive" />
            <span>Enduro</span>
          </a>
        </div>

        <div class="col-sm-3">
          <a href="#" title="">
            <img src="<?php echo base_url() . 'uploads/modalidades/b/' . 'paraequestre.png'; ?>" alt="paraequestre" class="img-responsive" />
            <span>Paraequestre</span>
          </a>
        </div>

        <div class="col-sm-3">
          <a href="#" title="">
            <img src="<?php echo base_url() . 'uploads/modalidades/b/' . 'redeas.png'; ?>" alt="redeas" class="img-responsive" />
            <span>Rédeas</span>
          </a>
        </div>

        <div class="col-sm-3">
          <a href="#" title="">
            <img src="<?php echo base_url() . 'uploads/modalidades/b/' . 'salto.png'; ?>" alt="salto" class="img-responsive" />
            <span>Salto</span>
          </a>
        </div>

        <div class="col-sm-3">
          <a href="#" title="">
            <img src="<?php echo base_url() . 'uploads/modalidades/b/' . 'volteio.png'; ?>" alt="volteio" class="img-responsive" />
            <span>Volteio</span>
          </a>
        </div>


	</div>

</div>
<!-- END Historia -->
