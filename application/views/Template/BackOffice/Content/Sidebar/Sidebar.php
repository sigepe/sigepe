
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu <?php echo ($ShowSidebar == false) ? 'page-sidebar-menu-closed' : ''; ?>  " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <li class="nav-item start ">
                            <a href="{base_url}BackOffice/Dashboard" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Dashboard</span>
                            </a>
                        </li>


                        <li class="heading">
                            <h3 class="uppercase">Pessoa & Perfil</h3>
                        </li>
                        <li class="nav-item">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <span class="title">Física</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="{base_url}BackOffice/Pessoa/PessoaFisica/Listar" class="nav-link ">
                                        <span class="title">Todos</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="{base_url}BackOffice/Pessoa/CpfDuplicado/Listar" class="nav-link ">
                                        <span class="title">CPF Duplicados</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="javascript:;" class="nav-link nav-toggle">
                                        <span class="title">Atletas</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item ">
                                            <a href="{base_url}BackOffice/Atleta/Todos/Listar" class="nav-link "> Todos </a>
                                        </li>
                                        <li class="nav-item ">
                                            <a href="{base_url}BackOffice/Atleta/Federados/Listar" class="nav-link "> FHBr </a>
                                        </li>
                                        <li class="nav-item ">
                                            <a href="{base_url}BackOffice/Atleta/OutrasFederacoes/Listar" class="nav-link "> Outras Federações </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item  ">
                                    <a href="{base_url}BackOffice/Desenhador/Todos/Listar" class="nav-link ">
                                        <span class="title">Desenhador de Percurso <br> <small>(Armador)</small></span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="#" class="nav-link ">
                                        <span class="title">Juiz</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="#" class="nav-link ">
                                        <span class="title">Juiz Externo</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="#" class="nav-link ">
                                        <span class="title">Delegado Técnico</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="#" class="nav-link ">
                                        <span class="title">Comissário</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="#" class="nav-link ">
                                        <span class="title">Veterinário</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="#" class="nav-link ">
                                        <span class="title">Instrutor</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="#" class="nav-link ">
                                        <span class="title">Locutor</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="#" class="nav-link ">
                                        <span class="title">BackOffice</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-briefcase" aria-hidden="true"></i>
                                <span class="title">Jurídica</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="{base_url}pessoajuridica/BackOffice/Confederacao/" class="nav-link "><span class="title">Confederação</span></a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="{base_url}pessoajuridica/BackOffice/Federacao/" class="nav-link "><span class="title">Federação</span></a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="javascript:;" class="nav-link nav-toggle">
                                        <span class="title">Clube</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item  ">
                                            <a href="{base_url}pessoajuridica/BackOffice/Entidade/" class="nav-link "><span class="title">Consultar</span></a>
                                        </li>
                                        <li class="nav-item ">
                                            <a href="#" class="nav-link "> Cadastrar </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item  ">
                                    <a href="#" class="nav-link ">
                                        <span class="title">Associação</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
       
       
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-gear" aria-hidden="true"></i>
                                <span class="title">Configuração</span>
                            </a>
                        </li>                        



                        <li class="heading">
                            <h3 class="uppercase">Animal</h3>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-daiamond"></i>
                                <span class="title">Consultar</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item ">
                                    <a href="{base_url}BackOffice/Animal/Todos/Listar" class="nav-link "> Todos </a>
                                </li>
                                <li class="nav-item ">
                                    <a href="{base_url}BackOffice/Animal/Fhbr/Listar" class="nav-link "> FHBr </a>
                                </li>
                                <li class="nav-item ">
                                    <a href="{base_url}BackOffice/Animal/OutrasFederacoes/Listar" class="nav-link "> Outras Federações </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-sdiaamond"></i>
                                <span class="title">Configuração</span>
                            </a>
                        </li>


                        <li class="heading">
                            <h3 class="uppercase">Evento</h3>
                        </li>
                        <li class="nav-item  ">
                            <a href="{base_url}BackOffice/Evento/Cadastrar" class="nav-link nav-toggle">
                                <i class="icon-diaamond"></i>
                                <span class="title">Cadastrar</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="{base_url}BackOffice/Evento/Todos/Listar" class="">
                                <i class="icon-daiamond"></i>
                                <span class="title">Consultar</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="{base_url}BackOffice/Evento/Calendario" class="nav-link nav-toggle">
                                <i class="icon-diaamond"></i>
                                <span class="title">Calendário</span>
                            </a>
                        </li>



                        <li class="heading">
                            <h3 class="uppercase">Registro</h3>
                        </li>
                        <li class="nav-item  ">
                            <a href="{base_url}BackOffice/Registro/Atleta" class="nav-link nav-toggle">
                                <i class="icon-diaamond"></i>
                                <span class="title">Atletas Federados</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="{base_url}BackOffice/Registro/Animal" class="nav-link nav-toggle">
                                <i class="icon-diaamond"></i>
                                <span class="title">Animais Federados</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-diaamond"></i>
                                <span class="title">Taxas</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="ui_metronic_grid.html" class="nav-link ">
                                        <span class="title">Taxas</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="ui_colors.html" class="nav-link ">
                                        <span class="title">Desconto</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="ui_general.html" class="nav-link ">
                                        <span class="title">Adicional</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-diaamond"></i>
                                <span class="title">Configuração</span>
                            </a>
                        </li>


                        <li class="heading">
                            <h3 class="uppercase">Financeiro</h3>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-usd" aria-hidden="true"></i>
                                <span class="title">Ordem</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="ui_colors.html" class="nav-link ">
                                        <span class="title">Inscrição</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="ui_metronic_grid.html" class="nav-link ">
                                        <span class="title">Passaporte</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="ui_colors.html" class="nav-link ">
                                        <span class="title">Selo</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="ui_colors.html" class="nav-link ">
                                        <span class="title">Registro Atleta</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="ui_colors.html" class="nav-link ">
                                        <span class="title">Registro Animal</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-bar-chart" aria-hidden="true"></i>
                                <span class="title">Relatório</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-exchange" aria-hidden="true"></i>
                                <span class="title">Gateway - Pagar.me</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-cog" aria-hidden="true"></i>
                                <span class="title">Configuração</span>
                            </a>
                        </li>


                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
