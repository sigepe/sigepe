<!-- Begin Destaques -->
<div class="col-sm-6 padding-left-0" id="inicio_destaques">

  <div class="col-sm-12">
    <div class="title_template margin-bottom-20">ÚLTIMOS <b>DESTAQUES</b></div>
  </div>



   <!-- BEGIN Owl -->
   <div class="col-sm-12">
     <div id="destaque-home" class="owl-carousel">

      <?php
        foreach ($ultimos_destaques as $destaque):

          /* TRATAR SLUGCATEGORIA QUANDO FOR MODALIDADE */  
          if(is_null($destaque->fk_mod_id))
          {
            // Categoria
            $slugCategoria  = $this->m_crud->get_rowSpecificTwoParameters('tb_blog_categoria', 'fk_tip_id', 1, 'cat_id', $destaque->fk_cat_id, 1, 'cat_slug');
          }
          else
          {
            // Modalidade
            $slugCategoria  = $this->m_crud->get_rowSpecific('tb_modalidade', 'mod_id', $destaque->fk_mod_id,  1, 'mod_slug');
          }

          $dataNoticia      = $this->my_date->datetime($destaque->blo_data, 'justDate');
          $diasemana        = $this->my_date->diasemana($destaque->blo_data);
          $titulo           = $destaque->blo_titulo;
          $img              = base_url() . 'manager/uploads/blog/thumbs/' . $destaque->blo_thumb;
        
          $link_externo     = $destaque->blo_flagLinkExterno;  
          if(!is_null($link_externo)){
            $link           = $destaque->blo_link_externo;
            $target         = '_blank';
          }      
          else{
            $tipoPagina     = $destaque->fk_tip_id;
            ($tipoPagina==1) ? $tipoPagina='noticias' : $tipoPagina='galerias';

            $link           = base_url() . $tipoPagina . '/' . $slugCategoria . '/' . $destaque->blo_slug;
            $target         = '_self';
          }     
        
        ?>

      <div class="item">
        <a href="<?php echo $link; ?>" title="<?php echo $titulo; ?>" target="<?php echo $target; ?>" >
          <div>
              <span><?php echo $dataNoticia . ' - ' . $diasemana; ?></span>
              <span><?php echo $titulo; ?></span>
          </div>
          <img src="<?php echo $img; ?>" />
        </a>
      </div>


    <?php endforeach ?>

    </div> 
  </div>
  <!-- END Owl -->

</div>
<!-- End Destaques -->

<!-- Begin Agenda -->
<div class="col-sm-6" id="inicio_eventos">

  <div class="title_template margin-bottom-20">PRÓXIMOS <b>EVENTOS</b></div>

    <?php
    var_dump($proximos_eventos);
    foreach ($proximos_eventos as $proximo_evento): ?>

    <div class="item col-sm-12">

      <div class="data col-sm-3 no-padding">
        <div class="data">
          <span class="dia"><?php echo $this->my_date->datetime($proximo_evento->eve_datainicio, 'justDay'); ?></span>
          <span class="mes">
            <?php
              $mes = $this->my_date->mes_extenso($proximo_evento->eve_datainicio);
              $mes = substr($mes, 0, 3);
              echo strtoupper($mes);
            ?>
          </span>
        </div>
      </div>

      <div class="col-sm-9 informacoes">
        <div class="titulo">
          <a href="<?php echo base_url() . 'evento/' . $proximo_evento->eve_slug; ?>" title="<?php echo $proximo_evento->eve_evento; ?>">
            <?php echo strip_tags($proximo_evento->eve_evento); ?>
          </a>
        </div>
      </div>

      <div class="col-sm-12 no-padding">
        <?php if(is_string($proximo_evento->eve_programa) && is_string($proximo_evento->eve_inscricao)): ?>
        <div class="btns">
        <?php endif; ?>

          <?php

            var_dump($proximo_evento);
            if(is_string($proximo_evento->eve_programa))
            {
              #echo "<a href=' ". base_url() . "site/cSite/force_download_programa/" . $proximo_evento->eve_slug ."' class='btn btn-xs btn-default'>Programa</a>";
              echo "<a href=' ". base_url() . "uploads/evento/programa/" . $proximo_evento->eve_programa ."' class='btn btn-sm btn-primary btn_programa' target='_blank'> <span class='glyphicon glyphicon-download-alt'> </span>  &nbsp;&nbsp; Programa</a>";
            }
            if($proximo_evento->fk_sta_id == 2)
            {
              #echo "<a href=' ". base_url() . "site/cSite/force_download_programa/" . $proximo_evento->eve_slug ."' class='btn btn-xs btn-default'>Programa</a>";
              echo "<a href=' ". $proximo_evento->eve_inscricao ."' class='btn btn-sm btn-success' target='_blank'> <span class='glyphicon glyphicon-ok'></span> &nbsp;&nbsp; Inscrição</a>";
            }

            if($proximo_evento->eve_flagOrdem == '1' && !is_null($proximo_evento->eve_flagOrdem)  && $proximo_evento->fk_sta_id == '3' || $proximo_evento->fk_sta_id == '4'  ){
              echo "<a href=' ". base_url() . 'evento/' . $proximo_evento->eve_slug ."/#scrollProvas ' class='btn btn-sm btn-warning'> <span class='glyphicon glyphicon-ok'></span> &nbsp;&nbsp; Ordem de Entrada</a>";
            }

            if( $proximo_evento->fk_sta_id == '4'  ){
              echo "<a href=' ". base_url() . 'evento/' . $proximo_evento->eve_slug ."/#scrollProvas ' class='btn btn-sm btn-success'> <span class='glyphicon glyphicon-ok'></span> &nbsp;&nbsp; Resultado</a>";
            }

	
            if($proximo_evento->eve_flagResultado == '1' && !is_null($proximo_evento->eve_flagResultado) && $proximo_evento->fk_sta_id == '5' ){
              echo "<a href='". base_url() . 'uploads/evento/resultado/resultado/' . $proximo_evento->eve_resultado . "'  class='btn btn-sm btn-success'> <span class='glyphicon glyphicon-ok'></span> &nbsp;&nbsp; Resultado Final</a>";
            }
       

 


            if($proximo_evento->fk_sta_id == 4 && strlen($proximo_evento->eve_quadrohorarios) > 1 ){
              echo "<a href=' ". base_url() . "uploads/evento/quadrodehorario/" . $proximo_evento->eve_quadrohorarios ."' class='btn btn-sm btn-primary btn_programa' target='_blank'> <span class='glyphicon glyphicon-download-alt'> </span>  &nbsp;&nbsp; Quadro de Horários</a>";
            }

          ?> 
        
        <?php if(is_string($proximo_evento->eve_programa) && is_string($proximo_evento->eve_inscricao)): ?>
        </div>
        <?php endif; ?>
      </div>
    </div>
    <?php endforeach; ?>

    <a href="{base_url}calendario" class="btn btn-block btn-info" style="float:left;border-radius:15px;"><span class="glyphicon glyphicon-plus"></span> veja todos os eventos da FHBr</a>

</div>
<!-- End Agenda -->


  <!-- Begin Modalidades -->
  <div class="col-sm-12 margin-top-30 inicio_modalidades visible-md visible-lg">

    <div class="title_template">MODALIDADES <b>FHBr</b></div>

  </div>


  </div>
  <!-- End Container ( opened header.php )  -->

</div>
<!-- End Main ( opened header.php )  -->


  <div class="inicio_modalidades modalidades_fullwidth margin-bottom-30 padding-left-0 visible-md visible-lg">

    <div class="container">

      <ul class="box">

        <?php foreach ($modalidades as $modalidade): ?>
        <li class="item">
          <a href="<?php echo base_url() . 'modalidade/' . $modalidade->mod_slug; ?>" title="<?php echo $modalidade->mod_modalidade; ?>">
            <img src="<?php echo base_url() . 'manager/uploads/modalidade/' . $modalidade->mod_thumb; ?>" alt="<?php echo $modalidade->mod_slug; ?>" />
            <span><?php echo $modalidade->mod_modalidade; ?></span>
          </a>
        </li>
        <?php endforeach; ?>

      </ul>

    </div>

  </div>


  <!-- Begin Main -->
  <div class="main">

    <!-- Begin Container -->
    <div class="container no-padding">

      <!-- BEGIN Galeria -->
      <div class="col-sm-12 margin-bottom-30 inicio_galeria padding-left-0">

        <div class="col-sm-12">
          <div class="title_template margin-bottom-20">ÚLTIMAS <b>GALERIAS</b></div>
        </div>

        <div class="col-sm-12">
        <?php foreach ($ultimas_galerias as $galeria): ?>

        <?php
          $nomeCategoria = $this->m_crud->get_rowSpecificTwoParameters('tb_blog_categoria', 'fk_tip_id', 2, 'cat_id', $galeria->fk_cat_id, 1, 'cat_categoria');
          $nomeModalidade = $this->m_crud->get_rowSpecific('tb_modalidade', 'mod_id', $galeria->fk_mod_id, 1, 'mod_modalidade');

          
          /* TRATAR SLUGCATEGORIA QUANDO FOR MODALIDADE */  
          if(is_null($galeria->fk_mod_id))
          {
            // Categoria
            $slugCategoria  = $this->m_crud->get_rowSpecificTwoParameters('tb_blog_categoria', 'fk_tip_id', 1, 'cat_id', $destaque->fk_cat_id, 1, 'cat_slug');
          }
          else
          {
            // Modalidade
            $slugCategoria  = $this->m_crud->get_rowSpecific('tb_modalidade', 'mod_id', $galeria->fk_mod_id,  1, 'mod_slug');
          }


          $link = base_url() . 'galerias/' . $slugCategoria . '/' . $galeria->blo_slug;
        ?>

          <div class="col-sm-6 item padding-left-0">
            <div class="col-sm-4 no-padding">
              <a href="<?php echo $link; ?>" title="<?php echo $galeria->blo_titulo; ?>">
                <img src="<?php echo base_url() . 'manager/uploads/blog/thumbs/' . $galeria->blo_thumb; ?>" alt="" class="img-responsive img-rounded" />
              </a>
            </div>
            <div class="col-sm-8 info">
              
              <a href="<?php echo $link; ?>" title="<?php echo $galeria->blo_titulo; ?>" class="titulo">
                <?php echo $galeria->blo_titulo; ?>
              </a>

              <div class="data"><?php echo $this->my_date->datetime($galeria->blo_data, 'justDate'); ?> - <?php echo $this->my_date->diasemana($galeria->blo_data); ?></div>
            
              <?php if(!is_null($nomeCategoria)): ?>
              <div class="categoria">
                <b>Categoria:</b>
                <?php echo $nomeCategoria; ?>
              </div>
              <?php endif; ?>        

              <?php if(!is_null($nomeModalidade)): ?>
              <div class="categoria">
                <b>Modalidade:</b>
                <?php echo $nomeModalidade; ?>
              </div>
              <?php endif; ?>
           
              <div class="botao visible-md visible-lg"><a href="<?php echo $link; ?>" class="btn btn-primary btn-primary"><span class="glyphicon glyphicon-camera"></span>   Ver Fotos</a></div>
            </div>
          </div>
        <?php endforeach; ?>
        </div>

      </div>
      <!-- End Galeria -->





