
<style type="text/css">
    .profile-usermenu-perfis{
        margin-top: 0px;
    }
</style>

<div class="row">
    
    <div class="col-sm-12">

        <!-- BEGIN PROFILE SIDEBAR -->
        <div class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet ">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                    <img src="http://images.instagram.com/profiles/anonymousUser.jpg" class="img-responsive" alt=""> </div>
                <!-- END SIDEBAR USERPIC -->


                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name"> <?php echo $PrimeiroNome . " " . $UltimoNome; ?> </div>
                    <!-- <div class="profile-usertitle-job"> Developer </div> -->
                </div>
                <!-- END SIDEBAR USER TITLE -->

                <!-- SIDEBAR BUTTONS -->
    <!-- 
                <div class="profile-userbuttons">
                    <button type="button" class="btn btn-circle green btn-sm">Follow</button>
                    <button type="button" class="btn btn-circle red btn-sm">Message</button>
                </div>
    -->
                <!-- END SIDEBAR BUTTONS -->

                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'Dashboard' ) ? 'active' : ''; ?>">
                            <a href="{base_url}FrontOffice/Perfil/Dashboard"> <i class="fa fa-home"></i> Início </a>
                        </li>
                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'MeuPerfil') ? 'active' : ''; ?>">
                            <a href="{base_url}FrontOffice/Perfil/MeuPerfil"> <i class="fa fa-user"></i> Meu Perfil </a>
                        </li>
                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'Telefone') ? 'active' : ''; ?>">
                            <a href="{base_url}FrontOffice/Perfil/Telefone"> <i class="fa fa-phone"></i> Telefone </a>
                        </li>
                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'Email') ? 'active' : ''; ?>">
                            <a href="{base_url}FrontOffice/Perfil/Email"> <i class="fa fa-envelope"></i> Email </a>
                        </li>
                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'Endereco') ? 'active' : ''; ?>">
                            <a href="{base_url}FrontOffice/Perfil/Endereco"> <i class="fa fa-map-marker"></i> Endereço </a>
                        </li>
                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'TrocarSenha') ? 'active' : ''; ?>">
                            <a href="{base_url}FrontOffice/Perfil/TrocarSenha"> <i class="fa fa-key"></i> Trocar Senha </a>
                        </li><!-- 
                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'Duvidas') ? 'active' : ''; ?>">
                            <a href="{base_url}FrontOffice/Perfil/Duvidas"> <i class="fa fa-info"></i> Dúvidas </a>
                        </li> -->
                    </ul>
                </div>
                <!-- END MENU -->
            </div>
            <!-- END PORTLET MAIN -->
            <!-- PORTLET MAIN -->
            <div class="portlet light" style="padding: 10px 0 0 0; border-top: 2px solid #5b9bd1;">
                <!-- STAT -->
                <div class="row list-separated profile-stat text-left" style="padding-left: 20px;">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="uppercase profile-stat-text" style="text-align: left;"> MEUS PERFIS </div>
                    </div>
                </div>
                <!-- END STAT -->

                <div class="profile-usermenu profile-usermenu-perfis" style="padding-bottom: 0px;">
                    <ul class="nav">

                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'Atleta') ? 'active' : ''; ?>">
                            <a href="{base_url}FrontOffice/Perfil/Atleta"> 
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                Atleta
                            </a>
                        </li>

<!-- 
                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'DesenhadorDePercurso') ? 'active' : ''; ?>">
                            <a href="#"> 
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                Desenhador de Percurso
                            </a>
                        </li>
                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'BackOffice') ? 'active' : ''; ?>">
                            <a href="#"> 
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                Backoffice
                            </a>
                        </li>
 -->                        
                    </ul>
                </div>


            </div>
            <!-- END PORTLET MAIN -->
        </div>
        <!-- END BEGIN PROFILE SIDEBAR -->
