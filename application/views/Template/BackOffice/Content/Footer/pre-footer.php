                    <!-- BEGIN PRE-FOOTER -->
                    <div class="page-prefooter">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-9 col-sm-6 col-xs-12 footer-block">
                                    <h2>SIGEPE Brasil</h2>
                                    <p> Sistema Integrado para Gestão de Provas Equestres. </p>
                                </div>

                                <?php 
                                /*
                                <div class="col-md-3 col-sm-6 col-xs12 footer-block">
                                    <h2>Subscribe Email</h2>
                                    <div class="subscribe-form">
                                        <form action="javascript:;">
                                            <div class="input-group">
                                                <input type="text" placeholder="mail@email.com" class="form-control">
                                                <span class="input-group-btn">
                                                    <button class="btn" type="submit">Submit</button>
                                                </span>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 footer-block">
                                    <h2>Follow Us On</h2>
                                    <ul class="social-icons">
                                        <li>
                                            <a href="javascript:;" data-original-title="rss" class="rss"></a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-original-title="facebook" class="facebook"></a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-original-title="twitter" class="twitter"></a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-original-title="googleplus" class="googleplus"></a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-original-title="linkedin" class="linkedin"></a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-original-title="youtube" class="youtube"></a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-original-title="vimeo" class="vimeo"></a>
                                        </li>
                                    </ul>
                                </div>
                                */
                                ?>
                                <div class="col-md-3 col-sm-6 col-xs-12 footer-block">
                                    <h2>Contato</h2>
                                    <address class="margin-bottom-40"> Telefone: (61) 3245-5870
                                        <br> Email:
                                        <a href="#">sigepe@fhbr.com.br</a>
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PRE-FOOTER -->

