
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <!-- BEGIN CONTAINER -->
                    <div class="page-container">
                        <!-- BEGIN CONTENT -->
                        <div class="page-content-wrapper">
                            <!-- BEGIN CONTENT BODY -->
                            <!-- BEGIN PAGE HEAD-->
                            <div class="page-head">
                                <div class="container-fluid">
                                    <!-- BEGIN PAGE TITLE -->
                                    <div class="page-title">
                                        <h1>Empresas
                                            <small>Relação das empresas que você tem algum vínculo</small>
                                        </h1>
                                    </div>
                                    <!-- END PAGE TITLE -->
                                </div>
                            </div>
                            <!-- END PAGE HEAD-->
                            <!-- BEGIN PAGE CONTENT BODY -->
                            <div class="page-content">
                                <div class="container-fluid">
                                    <!-- BEGIN PAGE BREADCRUMBS -->
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="index.html">SIGEPE</a>
                                            <i class="fa fa-circle"></i>
                                        </li>
                                        <li>
                                            <span>Empresas</span>
                                        </li>
                                    </ul>
                                    <!-- END PAGE BREADCRUMBS -->
                                    <!-- BEGIN PAGE CONTENT INNER -->
                                    <div class="page-content-inner">
                                        <!-- 
                                        <div class="m-heading-1 border-green m-bordered">
                                            <h3>DataTables Buttons Extension</h3>
                                            <p> A common UI paradigm to use with interactive tables is to present buttons that will trigger some action - that may be to alter the table's state, modify the data in the table, gather the data from the table
                                                or even to activate some external process. Showing such buttons is an interface that end users are comfortable with, making them feel at home with the table. </p>
                                            <p> For more info please check out
                                                <a class="btn red btn-outline" href="http://datatables.net/extensions/buttons/" target="_blank">the official documentation</a>
                                            </p>
                                        </div>
                                         -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <!-- Begin: life time stats -->
                                                <div class="portlet light portlet-fit portlet-datatable ">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                            <i class="icon-settings font-green"></i>
                                                            <span class="caption-subject font-green sbold uppercase">Empresas</span>
                                                        </div>
                                                        <div class="actions">

                                                            <div class="btn-group">
                                                                <a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                                                                    <i class="fa fa-share"></i>
                                                                    <span class="hidden-xs"> Exportar Dados </span>
                                                                    <i class="fa fa-angle-down"></i>
                                                                </a>
                                                                <ul class="dropdown-menu pull-right" id="sample_3_tools">
                                                                    <li>
                                                                        <a href="javascript:;" data-action="0" class="tool-action">
                                                                            <i class="icon-printer"></i> Imprimir</a>
                                                                    </li>
                                                                    <li class="divider"> </li>
                                                                    <li>
                                                                        <a href="javascript:;" data-action="1" class="tool-action">
                                                                            <i class="icon-check"></i> Copiar</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="javascript:;" data-action="2" class="tool-action">
                                                                            <i class="icon-doc"></i> PDF</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="javascript:;" data-action="3" class="tool-action">
                                                                            <i class="icon-paper-clip"></i> Excel</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="javascript:;" data-action="4" class="tool-action">
                                                                            <i class="icon-cloud-upload"></i> CSV</a>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="portlet-body">
                                                        <div class="table-container">
                                                            <table class="table table-striped table-bordered table-hover" id="sample_3">
                                                                <thead>
                                                                    <tr>
                                                                        <th> ID </th>
                                                                        <th> Empresa </th>
                                                                        <th> Vínculo </th>
                                                                        <th> Permissão</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td> 84 </td>
                                                                        <td> <a href="#" title="" class="show label label-primary">Federação Hípica de Brasília</a> </td>
                                                                        <td> Presidente </td>
                                                                        <td> Administrador </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> 169 </td>
                                                                        <td> <a href="#" title="" class="show label label-primary">Haras do Morro</a> </td>
                                                                        <td> Secretário(a) </td>
                                                                        <td> Editor </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> 214 </td>
                                                                        <td> <a href="#" title="" class="show label label-primary">Vanguarda Tecnologia</a> </td>
                                                                        <td> Sócio-proprietário </td>
                                                                        <td> Administrador </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- End: life time stats -->
                                            </div>
                                        </div>

                                    </div>
                                    <!-- END PAGE CONTENT INNER -->
                                </div>
                            </div>
                            <!-- END PAGE CONTENT BODY -->
                            <!-- END CONTENT BODY -->
                        </div>
                        <!-- END CONTENT -->

                    </div>
                    <!-- END CONTAINER -->
                </div>
            </div>
            