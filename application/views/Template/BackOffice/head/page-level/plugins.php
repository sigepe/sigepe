<!-- BEGIN PAGE LEVEL PLUGINS -->
<?php
	if(isset($StylesFile['Plugins']) && !is_null($StylesFile['Plugins'])):
		foreach ($StylesFile['Plugins'] as $key => $value):
?>
    <link href="<?php echo base_url() . LoadTemplateComponent('StylesFile', 'PageLevelPlugins', $key); ?>" rel="stylesheet" type="text/css" />
<?php
		endforeach;
    	endif;
    ?>
<!-- END PAGE LEVEL PLUGINS -->
