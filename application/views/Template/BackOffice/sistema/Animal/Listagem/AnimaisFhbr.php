<style type="text/css">
.h4, .h5, .h6, h4, h5, h6 {
    margin-top: 0px;
    margin-bottom: 5px;
}
hr, p {
    margin: 10px 0;
}
.portlet.light.bordered {
    border-bottom: 3px solid rgba(204, 204, 204, 0.45) !important;
}
</style>


    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet light bordered portlet-modalidade-tabela" style="">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <i class="icon-speech font-green-sharp"></i>
                <span class="caption-subject bold uppercase"> Tabela</span>
                <span class="caption-helper">Relação de todas as pessoas</span>
            </div>
            <div class="actions">
                <!-- <a style="margin-bottom: 50px;" class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a> -->
            </div>
        </div>

        <div class="portlet-body">
            <table id="tabela-consulta-evento" class="table table-striped table-bordered table-hover">
                <?php
                $arr = array("thead", "tfoot");
                foreach ($arr as $key):
                    echo "<$key>"; ?>
                    <tr>
                        <th>ID</th>
                        <th>Nome Completo</th>
                        <th>N Chip</th>
                        <th>Entidade</th>
                        <th>Proprietário<small><br>CPF/CNPJ</small></th>
                        <th>Status</th>
                    </tr>
                    <?php echo "</$key>"; ?>
                <?php  endforeach; ?><thead>
                <tbody>
                    <?php 
                    foreach ($DataSetAnimal as $animal): ?>
                    <tr>
                        <td><a href="{base_url}BackOffice/Animal/Perfil/Detalhar/<?php echo $animal->ani_id?>" style="display: block;">#<?php echo $animal->ani_id; ?></a></td>
                        <td><a href="{base_url}BackOffice/Animal/Perfil/Detalhar/<?php echo $animal->ani_id?>"> <?php echo ucwords(strtolower($animal->ani_nome_completo)); ?> </a></td>
                        <td> <?php echo $animal->ani_numero_chip; ?> </td>

                        <td> <?php
                        try {
                            if (!$animal->ani_federacao) {
                                throw new Exception('');
                            }
                            $string = explode(" - ", $animal->ani_entidade);
                            echo $string[0]."<br>";

                        } catch (Exception $e) {

                        }
                        ?> </td>

                        <td> <?php
                        echo ucwords(strtolower($animal->ani_proprietario_nome));
                        echo "<small><br>";
                        echo ($animal->ani_proprietario_natureza == "PF") ? "CPF: " : ($animal->ani_proprietario_natureza == "PJ") ? "CNPJ: " : "";
                        echo $animal->ani_proprietario_cpf_cnpj."</small>";

                        ?> </td>
                        <td>
                            <?php 
                            $corLabel = "";
                            switch ($animal->fk_sta_id) {
                                case 1:
                                    $corLabel = "primary";
                                    break;
                                    
                                case 2:
                                default:
                                    $corLabel = "default";
                                    break;
                            }
                            echo 
                            "<span class='label label-".$corLabel."' style='font-size: 14px !important; '>". $animal->ani_status ."</span>";
                            ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->