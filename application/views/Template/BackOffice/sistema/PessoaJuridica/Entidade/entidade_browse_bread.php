<?php 
?>
    <!-- BEGIN PROFILE CONTENT -->
<div class="profile-content" id="meu-perfil">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light " style="width: 100%; float: left;">


                    <div class="portlet-title">
                        <div class="caption font-blue-madison">
                            <span class="caption-subject bold uppercase"> Todos as Entidades</span>
                            <span class="caption-helper">Informações das entidades</span>
                        </div>
                        <div class="actions">
                            
                        </div>
                    </div>

                    <div class="portlet-body">
                        <div class="tab-content">
                            <!-- PERSONAL INFO TAB -->
                            <div class="tab-pane active" id="tab_1_1">

                                <form role="form" action="#" class="form-view" id="form-perfil">
                                    <div class="table-scrollable table-scrollable-borderless">
                                        <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        
                                            <thead>
                                                <tr class="uppercase">
                                                    <th>ID</th>
                                                    <th>Nome</th>
                                                    <th>Presidente</th>
                                                    <th>CNPJ</th>
                                                    <th>Qtd. Atletas</th>
                                                    <th>Qtd. Animais</th>
                                                </tr>
                                            </thead>

                                            <tbody>

                                                <?php foreach ($dataset as $key => $pessoaje): ?>
                                                <tr>
                                                    <td><?php echo $pessoaje->pje_id; ?></td>
                                                    <td><?php echo $pessoaje->pje_acronimo; ?></td>
                                                    <td><a href="{base_url}BackOffice/Pessoa/Perfil/Detalhar/<?php echo $pessoaje->fk_pro_id; ?>" style="display: block;"><?php echo $pessoaje->pje_prop_nome_razao; ?></a></td>
                                                    <td><?php echo $pessoaje->pje_cpf_cnpj; ?></td>
                                                    <td></td>
                                                    <td></td>
                                                    
                                                </tr>
                                                <?php endforeach; ?>

                                            </tbody>

                                        </table>
                                    </div>
                                </form>
                            </div>
                            <!-- END PERSONAL INFO TAB -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>