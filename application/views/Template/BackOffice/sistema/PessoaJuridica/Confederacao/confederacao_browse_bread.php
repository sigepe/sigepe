<?php 
?>
    <!-- BEGIN PROFILE CONTENT -->
<div class="profile-content" id="meu-perfil">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light " style="width: 100%; float: left;">


                    <div class="portlet-title">
                        <div class="caption font-blue-madison">
                            <span class="caption-subject bold uppercase"> Todos as Confederações</span>
                            <span class="caption-helper">Informações das confederações</span>
                        </div>
                        <div class="actions">
                            
                        </div>
                    </div>

                    <div class="portlet-body">
                        <div class="tab-content">
                            <!-- PERSONAL INFO TAB -->
                            <div class="tab-pane active" id="tab_1_1">

                                <form role="form" action="#" class="form-view" id="form-perfil">
                                    <div class="table-scrollable table-scrollable-borderless">
                                        <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        
                                            <thead>
                                                <tr class="uppercase">
                                                    <th>ID</th>
                                                    <th>Nome</th>
                                                    <th>Presidente</th>
                                                    <th>Mandato</th>
                                                    <th>CNPJ</th>
                                                    <th>Qtd. Atletas</th>
                                                    <th>Qtd. Animais</th>
                                                </tr>
                                            </thead>

                                            <tbody>

                                                <?php foreach ($dataset as $key => $pessoajf): ?>
                                                <tr>
                                                    <td><?php echo $pessoajf->pjc_id; ?></td>
                                                    <td><?php echo $pessoajf->pjc_nome_razao_social; ?></td>
                                                    <td><a href="{base_url}BackOffice/Pessoa/Perfil/Detalhar/<?php echo $pessoajf->fk_pre_id; ?>" style="display: block;"><?php echo $pessoajf->pjc_pre_nome_razao; ?></a></td>
                                                    <td></td>
                                                    <td><?php echo $pessoajf->pjc_cpf_cnpj; ?></td>
                                                    <td></td>
                                                    <td></td>
                                                    
                                                </tr>
                                                <?php endforeach; ?>

                                            </tbody>

                                        </table>
                                    </div>
                                </form>
                            </div>
                            <!-- END PERSONAL INFO TAB -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>