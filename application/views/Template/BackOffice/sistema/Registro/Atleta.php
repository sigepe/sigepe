
<style type="text/css">
.label{
    border-radius: 30px !important;
    width: 100%;
    display: block;
} 
.tabs-left.nav-tabs>li.active a{
    background: #eee;
    font-weight: bold;
}
.tab-pane > .porlet{
    padding-bottom: 0px !important;
}
.portlet.light .portlet-body{
    padding-top: 0px !important;
}
.portlet.light .dataTables_wrapper .dt-buttons{margin-top: -55px !important; }
</style>


    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet light">


        <div class="portlet-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#ano_2018" data-toggle="tab"> 2018 </a>
                </li>
                <li>
                    <a href="#ano_2019" data-toggle="tab" onclick="swal('Pacote inativo!', 'Sua licença não possui permissão para esse pacote no SIGEPE! STS-1535378245', 'error')"> 2019 </a>
                </li>
            </ul>


            <div class="tab-content">


                <!-- INICIO TAB-CONTENT ( ANO 2018 ) -->
                <div class="tab-pane fade active in" id="ano_2018">

                    <div class="row">
                        <div class="col-md-2 col-sm-2 col-xs-2" style="padding-right: 0px;">
                            <ul class="nav nav-tabs tabs-left text-right">
                                <?php foreach ($DatasetModalidade as $key => $value): ?>
                                <li class="<?php echo ($key == 0) ? 'active' : ''; ?>">
                                    <a href="#modalidade_<?php echo $value->evm_slug; ?>" data-toggle="tab" aria-expanded="true"> <?php echo $value->evm_modalidade; ?> </a>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <div class="col-md-10 col-sm-10 col-xs-10" style="background: #eee; padding-top: 20px;">
                            <div class="tab-content">
                                
                                <?php foreach ($DatasetModalidade as $key => $ValueModalidade): ?>
                                <!-- INICIO TAB-PANE ( MODALIDADE SALTO ) -->
                                <div class="tab-pane <?php echo ($key == 0) ? 'active in' : 'fade'; ?>" id="modalidade_<?php echo $ValueModalidade->evm_slug; ?>">

                                    <div class="portlet light bordered">
                                        <div class="portlet-title tabbable-line">
                                            <!--
                                            <div class="caption">
                                                <i class="icon-share font-dark"></i>
                                                <span class="caption-subject font-dark bold uppercase">Portlet Tabs</span>
                                            </div>
                                            -->
                                            <ul class="nav nav-tabs" style="float: left;">
                                                <li class="active">
                                                    <a href="#modalidade_<?php echo $ValueModalidade->evm_slug ?>_ativo" data-toggle="tab" aria-expanded="false"> <span class="bold">Ativos </span> <br> <small>Registros Válidos</small> </a>
                                                </li>
                                                <li class="">
                                                    <a href="#modalidade_<?php echo $ValueModalidade->evm_slug ?>_cancelado" data-toggle="tab" aria-expanded="false"> <span class="bold">Cancelados </span> <br> <small>Registros Cancelados</small> </a>
                                                </li>
                                                <li class="">
                                                    <a href="#modalidade_<?php echo $ValueModalidade->evm_slug ?>_aguardando_pagamento" data-toggle="tab" aria-expanded="false"> <span class="bold">Aguardando Pagamento</span> <br> <small>Boleto Bancário</small> </a>
                                                </li>
                                                <li>
                                                    <a href="#modalidade_<?php echo $ValueModalidade->evm_slug ?>_isento" data-toggle="tab" aria-expanded="true"> <span class="bold">Isentos</span> <br> <small>Cedido por Diretoria</small> </a>
                                                </li>
                                                <li class="">
                                                    <a href="#modalidade_<?php echo $ValueModalidade->evm_slug ?>_estatisticas" data-toggle="tab" aria-expanded="false" onclick="swal('Pacote inativo!', 'Sua licença não possui permissão para esse pacote no SIGEPE!  STS-1535376875', 'error')"><span class="bold">Estatísticas</span> <br> <small>Relatório Detalhado</small></a>
                                                </li>
                                            </ul>

                                        </div>
                                        <div class="portlet-body">
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="modalidade_<?php echo $ValueModalidade->evm_slug ?>_ativo">


                                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                                    <div class="portlet light bordered portlet-modalidade-tabela" style="border: 0px !important;padding: 0px !important;margin-bottom: 0px !important">
                                                        <div class="portlet-title">
                                                            <div class="caption font-green-sharp">
                                                                <i class="icon-speech font-green-sharp"></i>
                                                                <span class="caption-subject bold uppercase"> Tabela</span>
                                                                <span class="caption-helper">Relação de todos os atletas de <?php echo $ValueModalidade->evm_slug ?> com registro ativo</span>
                                                            </div>

                                                            <div class="tools"> </div>
                                                        </div>

                                                        <div class="portlet-body">
                                                            <table class="table table-striped table-bordered table-hover datatable-buttons">
                                                                <thead>
                                                                    <tr>
                                                                        <th>ID</th>
                                                                        <th>Atleta</th>
                                                                        <th>Clube</th>
                                                                        <th>Natureza</th>
                                                                        <th>
                                                                            <a href="javascript:;" class="tooltips" data-placement="bottom" data-original-title="Para mais detalhes clique no código da transação do registro que deseja. Você será direcionado para o módulo financeiro."> <i class="fa fa-info-circle"></i> </a>
                                                                            Código da Transação <br> <small><small>(Módulo: Financeiro)</small></small>
                                                                        </th>
                                                                        <th>
                                                                            <a href="javascript:;" class="tooltips" data-placement="bottom" data-original-title="Indica a data que a solicitação de registro foi solicitada e NÃO a data do processamento do pagamento. Para detalhes do pagamento acessar o módulo financeiro."> <i class="fa fa-info-circle"></i> </a>
                                                                            Criado em
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tfoot>
                                                                    <tr>
                                                                        <th>ID</th>
                                                                        <th>Atleta</th>
                                                                        <th>Clube</th>
                                                                        <th>Natureza</th>
                                                                        <th>
                                                                            <a href="javascript:;" class="tooltips" data-placement="bottom" data-original-title="Para mais detalhes clique no código da transação do registro que deseja. Você será direcionado para o módulo financeiro."> <i class="fa fa-info-circle"></i> </a>
                                                                            Código da Transação <br> <small><small>(Módulo: Financeiro)</small></small>
                                                                        </th>
                                                                        <th>
                                                                            <a href="javascript:;" class="tooltips" data-placement="bottom" data-original-title="Indica a data que a solicitação de registro foi solicitada e NÃO a data do processamento do pagamento. Para detalhes do pagamento acessar o módulo financeiro."> <i class="fa fa-info-circle"></i> </a>
                                                                            Criado em
                                                                        </th>
                                                                    </tr>
                                                                </tfoot>
                                                                <tbody>
                                                                    <?php
                                                                        foreach ($DatasetAtleta as $key => $ValueAtleta):

                                                                            if( $ValueAtleta->evm_id != $ValueModalidade->evm_id )
                                                                                continue;

                                                                            if( $ValueAtleta->fk_sta_id != 301 )
                                                                                continue;

                                                                            /*
                                                                            echo 'EVM-ID: ' . $ValueAtleta->evm_id;
                                                                            echo '<br>MOD EVM-ID: ' . $ValueModalidade->evm_id;
                                                                            echo "<hr>";
                                                                            */

                                                                    ?>
                                                                    <tr>
                                                                        <td>
                                                                                <?php echo $ValueAtleta->reg_id; ?>
                                                                        </td>
                                                                        <td>
                                                                            <a href="#" title="Clique para selecionar o perfil do atleta">
                                                                                <?php echo $ValueAtleta->pes_nome_razao_social; ?>
                                                                            </a>
                                                                        </td>
                                                                        <td>
                                                                            <?php
                                                                            
                                                                            try {
                                                                                if (!$ValueAtleta->nomeclube) {
                                                                                    throw new Exception('Não possui.');
                                                                                }
                                                                                $Clube      =       $ValueAtleta->nomeclube;
                                                                                $Explode    =       explode("-", $Clube);
                                                                                echo $Explode[0] . '<br><small>' . $Explode[1] . "</small>";
                                                                            } catch (Exception $e) {
                                                                                echo "<p style='color: red;'>".$e->getMessage()."</p>";
                                                                            }

                                                                            ?>
                                                                        </td>
                                                                        <td><?php echo $ValueAtleta->ret_tipo; ?></td>
                                                                        <td>
                                                                            <a href="#" title="Clique para mais detalhes da transação.">
                                                                                <?php echo $ValueAtleta->fin_transacao; ?>
                                                                            </a>
                                                                        </td>
                                                                        <td>
                                                                            <?php
                                                                                $time = new DateTime($ValueAtleta->reg_criado);
                                                                                $date = $time->format('d/m/Y');
                                                                                $time = $time->format('H:i');
                                                                                echo $date;
                                                                                echo '<br><small> às ' . $time . "</small>"; 
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <?php endforeach; ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!-- END EXAMPLE TABLE PORTLET-->


                                                </div>
                                                <!-- END ATIVO -->



                                                <!-- INATIVOS -->
                                                <div class="tab-pane" id="modalidade_<?php echo $ValueModalidade->evm_slug ?>_cancelado">

                                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                                    <div class="portlet light bordered portlet-modalidade-tabela" style="border: 0px !important;padding: 0px !important;margin-bottom: 0px !important">
                                                        <div class="portlet-title">
                                                            <div class="caption font-green-sharp">
                                                                <i class="icon-speech font-green-sharp"></i>
                                                                <span class="caption-subject bold uppercase"> Tabela</span>
                                                                <span class="caption-helper">Relação de todos os atletas de <?php echo $ValueModalidade->evm_slug ?> com registro inativo</span>
                                                            </div>

                                                            <div class="tools"> </div>
                                                        </div>

                                                        <div class="portlet-body">
                                                            <table class="table table-striped table-bordered table-hover datatable-buttons">
                                                                <thead>
                                                                    <tr>
                                                                        <th>ID</th>
                                                                        <th>Atleta</th>
                                                                        <th>Clube</th>
                                                                        <th>Natureza</th>
                                                                        <th>
                                                                            <a href="javascript:;" class="tooltips" data-placement="bottom" data-original-title="Para mais detalhes clique no código da transação do registro que deseja. Você será direcionado para o módulo financeiro."> <i class="fa fa-info-circle"></i> </a>
                                                                            Código da Transação <br> <small><small>(Módulo: Financeiro)</small></small>
                                                                        </th>
                                                                        <th>
                                                                            <a href="javascript:;" class="tooltips" data-placement="bottom" data-original-title="Indica a data que a solicitação de registro foi solicitada e NÃO a data do processamento do pagamento. Para detalhes do pagamento acessar o módulo financeiro."> <i class="fa fa-info-circle"></i> </a>
                                                                            Registrado em
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tfoot>
                                                                    <tr>
                                                                        <th>ID</th>
                                                                        <th>Atleta</th>
                                                                        <th>Clube</th>
                                                                        <th>Natureza</th>
                                                                        <th>
                                                                            <a href="javascript:;" class="tooltips" data-placement="bottom" data-original-title="Para mais detalhes clique no código da transação do registro que deseja. Você será direcionado para o módulo financeiro."> <i class="fa fa-info-circle"></i> </a>
                                                                            Código da Transação <br> <small><small>(Módulo: Financeiro)</small></small>
                                                                        </th>
                                                                        <th>
                                                                            <a href="javascript:;" class="tooltips" data-placement="bottom" data-original-title="Indica a data que a solicitação de registro foi solicitada e NÃO a data do processamento do pagamento. Para detalhes do pagamento acessar o módulo financeiro."> <i class="fa fa-info-circle"></i> </a>
                                                                            Registrado em
                                                                        </th>
                                                                    </tr>
                                                                </tfoot>
                                                                <tbody>
                                                                    <?php
                                                                        foreach ($DatasetAtleta as $key => $ValueAtleta):

                                                                            if( $ValueAtleta->evm_id != $ValueModalidade->evm_id )
                                                                                continue;

                                                                            if( $ValueAtleta->fk_sta_id != 303 )
                                                                                continue;

                                                                    ?>
                                                                    <tr>
                                                                        <td>
                                                                                <?php echo $ValueAtleta->reg_id; ?>
                                                                        </td>
                                                                        <td>
                                                                            <a href="#" title="Clique para selecionar o perfil do atleta">
                                                                                <?php echo $ValueAtleta->pes_nome_razao_social; ?>
                                                                            </a>
                                                                        </td>
                                                                        <td>
                                                                            <?php
                                                                                $Clube      =       $ValueAtleta->nomeclube;
                                                                                $Explode    =       explode("-", $Clube);
                                                                                echo $Explode[0] . '<br><small>' . $Explode[1] . "</small>";
                                                                            ?>
                                                                        </td>
                                                                        <td><?php echo $ValueAtleta->ret_tipo; ?></td>
                                                                        <td>
                                                                            <a href="#" title="Clique para mais detalhes da transação.">
                                                                                <?php echo $ValueAtleta->fin_transacao; ?>
                                                                            </a>
                                                                        </td>
                                                                        <td>
                                                                            <?php
                                                                                $time = new DateTime($ValueAtleta->reg_criado);
                                                                                $date = $time->format('d/m/Y');
                                                                                $time = $time->format('H:i');
                                                                                echo $date;
                                                                                echo '<br><small> às ' . $time . "</small>"; 
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <?php endforeach; ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!-- END EXAMPLE TABLE PORTLET-->

                                                </div>
                                                <!-- FIM INATIVOS -->





                                                <div class="tab-pane" id="modalidade_<?php echo $ValueModalidade->evm_slug ?>_isento">


                                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                                    <div class="portlet light bordered portlet-modalidade-tabela" style="border: 0px !important;padding: 0px !important;margin-bottom: 0px !important">
                                                        <div class="portlet-title">
                                                            <div class="caption font-green-sharp">
                                                                <i class="icon-speech font-green-sharp"></i>
                                                                <span class="caption-subject bold uppercase"> Tabela</span>
                                                                <span class="caption-helper">Relação de todos os atletas de <?php echo $ValueModalidade->evm_slug ?> com registro que foram isentados parcial/total</span>
                                                            </div>

                                                            <div class="tools"> </div>
                                                        </div>

                                                        <div class="portlet-body">
                                                            <table class="table table-striped table-bordered table-hover datatable-buttons">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Atleta</th>
                                                                        <th>Clube</th>
                                                                        <th>Natureza</th>
                                                                        <th>
                                                                            <a href="javascript:;" class="tooltips" data-placement="bottom" data-original-title="Para mais detalhes clique no código da transação do registro que deseja. Você será direcionado para o módulo financeiro."> <i class="fa fa-info-circle"></i> </a>
                                                                            Código da Transação <br> <small><small>(Módulo: Financeiro)</small></small>
                                                                        </th>
                                                                        <th>
                                                                            <a href="javascript:;" class="tooltips" data-placement="bottom" data-original-title="Indica a data que a solicitação de registro foi solicitada e NÃO a data do processamento do pagamento. Para detalhes do pagamento acessar o módulo financeiro."> <i class="fa fa-info-circle"></i> </a>
                                                                            Registrado em
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tfoot>
                                                                    <tr>
                                                                        <th>Atleta</th>
                                                                        <th>Clube</th>
                                                                        <th>Natureza</th>
                                                                        <th>
                                                                            <a href="javascript:;" class="tooltips" data-placement="bottom" data-original-title="Para mais detalhes clique no código da transação do registro que deseja. Você será direcionado para o módulo financeiro."> <i class="fa fa-info-circle"></i> </a>
                                                                            Código da Transação <br> <small><small>(Módulo: Financeiro)</small></small>
                                                                        </th>
                                                                        <th>
                                                                            <a href="javascript:;" class="tooltips" data-placement="bottom" data-original-title="Indica a data que a solicitação de registro foi solicitada e NÃO a data do processamento do pagamento. Para detalhes do pagamento acessar o módulo financeiro."> <i class="fa fa-info-circle"></i> </a>
                                                                            Registrado em
                                                                        </th>
                                                                    </tr>
                                                                </tfoot>
                                                                <tbody>
                                                                    <?php
                                                                        foreach ($DatasetAtletaIsento as $key => $ValueAtleta):
                                                                            if( ($ValueAtleta->evm_id != $ValueModalidade->evm_id) && $ValueAtleta->fk_sta_id != 300 )
                                                                                continue;
                                                                    ?>
                                                                    <tr>
                                                                        <td>
                                                                            <a href="#" title="Clique para selecionar o perfil do atleta">
                                                                                <?php echo $ValueAtleta->pes_nome_razao_social; ?>
                                                                            </a>
                                                                        </td>
                                                                        <td>
                                                                            <?php
                                                                                $Clube      =       $ValueAtleta->nomeclube;
                                                                                $Explode    =       explode("-", $Clube);
                                                                                echo $Explode[0] . '<br><small>' . $Explode[1] . "</small>";
                                                                            ?>
                                                                        </td>
                                                                        <td><?php echo $ValueAtleta->ret_tipo; ?></td>
                                                                        <td>
                                                                            <a href="#" title="Clique para mais detalhes da transação.">
                                                                                <?php echo $ValueAtleta->fin_transacao; ?>
                                                                            </a>
                                                                        </td>
                                                                        <td>
                                                                            <?php
                                                                                $time = new DateTime($ValueAtleta->reg_criado);
                                                                                $date = $time->format('d/m/Y');
                                                                                $time = $time->format('H:i');
                                                                                echo $date;
                                                                                echo '<br><small> às ' . $time . "</small>"; 
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <?php endforeach; ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!-- END EXAMPLE TABLE PORTLET-->

                                                </div>










                                                <div class="tab-pane" id="modalidade_<?php echo $ValueModalidade->evm_slug ?>_aguardando_pagamento">


                                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                                    <div class="portlet light bordered portlet-modalidade-tabela" style="border: 0px !important;padding: 0px !important;margin-bottom: 0px !important">
                                                        <div class="portlet-title">
                                                            <div class="caption font-green-sharp">
                                                                <i class="icon-speech font-green-sharp"></i>
                                                                <span class="caption-subject bold uppercase"> Tabela</span>
                                                                <span class="caption-helper">Relação de todos os atletas de <?php echo $ValueModalidade->evm_slug ?> com registro que foram isentados parcial/total</span>
                                                            </div>

                                                            <div class="tools"> </div>
                                                        </div>

                                                        <div class="portlet-body">
                                                            <table class="table table-striped table-bordered table-hover datatable-buttons">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Atleta</th>
                                                                        <th>Clube</th>
                                                                        <th>Natureza</th>
                                                                        <th>
                                                                            <a href="javascript:;" class="tooltips" data-placement="bottom" data-original-title="Para mais detalhes clique no código da transação do registro que deseja. Você será direcionado para o módulo financeiro."> <i class="fa fa-info-circle"></i> </a>
                                                                            Código da Transação <br> <small><small>(Módulo: Financeiro)</small></small>
                                                                        </th>
                                                                        <th>
                                                                            <a href="javascript:;" class="tooltips" data-placement="bottom" data-original-title="Indica a data que a solicitação de registro foi solicitada e NÃO a data do processamento do pagamento. Para detalhes do pagamento acessar o módulo financeiro."> <i class="fa fa-info-circle"></i> </a>
                                                                            Registrado em
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tfoot>
                                                                    <tr>
                                                                        <th>Atleta</th>
                                                                        <th>Clube</th>
                                                                        <th>Natureza</th>
                                                                        <th>
                                                                            <a href="javascript:;" class="tooltips" data-placement="bottom" data-original-title="Para mais detalhes clique no código da transação do registro que deseja. Você será direcionado para o módulo financeiro."> <i class="fa fa-info-circle"></i> </a>
                                                                            Código da Transação <br> <small><small>(Módulo: Financeiro)</small></small>
                                                                        </th>
                                                                        <th>
                                                                            <a href="javascript:;" class="tooltips" data-placement="bottom" data-original-title="Indica a data que a solicitação de registro foi solicitada e NÃO a data do processamento do pagamento. Para detalhes do pagamento acessar o módulo financeiro."> <i class="fa fa-info-circle"></i> </a>
                                                                            Registrado em
                                                                        </th>
                                                                    </tr>
                                                                </tfoot>
                                                                <tbody>
                                                                    <?php
                                                                        foreach ($DatasetAtletaAguardandoPagamento as $key => $ValueAtleta):
                                                                            if( ($ValueAtleta->evm_id != $ValueModalidade->evm_id) && $ValueAtleta->fk_sta_id != 300 )
                                                                                continue;
                                                                    ?>
                                                                    <tr>
                                                                        <td>
                                                                            <a href="#" title="Clique para selecionar o perfil do atleta">
                                                                                <?php echo $ValueAtleta->pes_nome_razao_social; ?>
                                                                            </a>
                                                                        </td>
                                                                        <td>
                                                                            <?php
                                                                                $Clube      =       $ValueAtleta->nomeclube;
                                                                                $Explode    =       explode("-", $Clube);
                                                                                echo $Explode[0] . '<br><small>' . $Explode[1] . "</small>";
                                                                            ?>
                                                                        </td>
                                                                        <td><?php echo $ValueAtleta->ret_tipo; ?></td>
                                                                        <td>
                                                                            <a href="#" title="Clique para mais detalhes da transação.">
                                                                                <?php echo $ValueAtleta->fin_transacao; ?>
                                                                            </a>
                                                                        </td>
                                                                        <td>
                                                                            <?php
                                                                                $time = new DateTime($ValueAtleta->reg_criado);
                                                                                $date = $time->format('d/m/Y');
                                                                                $time = $time->format('H:i');
                                                                                echo $date;
                                                                                echo '<br><small> às ' . $time . "</small>"; 
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <?php endforeach; ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!-- END EXAMPLE TABLE PORTLET-->

                                                </div>





                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <!-- FIM TAB-PANE ( MODALIDADE  ) -->
                                <?php endforeach; ?>


                            </div>
                        </div>
                    </div>



                </div>
                <!-- FIM TAB-CONTENT ( ANO 2018 ) -->


                <!-- INICIO TAB-CONTENT ( ANO 2019 ) -->
                <div class="tab-pane fade" id="ano_2019">

                    <div class="alert alert-warning">
                        <strong>Atenção!</strong> Licença para o cliente: <b>FHBr - Federação Hípica de Brasília</b> válida apenas para o ano em exercício - 2018.
                    </div>

                </div>
                <!-- FIM TAB-CONTENT ( ANO 2019 ) -->



            </div>
            <!-- END TAB-CONTENT -->

        </div>
        <!-- END PORTLET-BODY -->

    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
