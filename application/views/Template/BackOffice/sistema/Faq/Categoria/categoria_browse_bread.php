


	<div class="portlet light">
	    <div class="portlet-title">
	        <div class="caption">
	            <i class="icon-speech"></i>
	            <span class="caption-subject bold uppercase"> Categorias</span>
	            <span class="caption-helper">Listagem de todas as categorias</span>
	        </div>
	        <div class="actions">

				<a class="btn btn-circle btn-edit btn-sm btn-cadastrar-categoria" href="javascript:;">
				    <i class="fa fa-plus"></i>
				    Cadastrar Categoria
				</a>

	        </div>
	    </div>
	    <div class="portlet-body">
	
			<?php if(empty($dataset)): ?>
			<div class="alert alert-warning">
				<strong>Atenção!</strong> Nenhuma categoria cadastrada.
			</div>
			<?php endif; ?>

			<?php if(!empty($dataset)): ?>
            <table id="tabela-consulta-evento" class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Ambiente</th>
                        <th>Categoria</th>
                        <th>Posição</th>
                        <th>Qtde. Perguntas</th>
                        <th>Ação</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Ambiente</th>
                        <th>Categoria</th>
                        <th>Posição</th>
                        <th>Qtde. Perguntas</th>
                        <th>Ação</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php 
                    foreach ($dataset as $key => $categoria): ?>
                    <tr>
                        <td> #<?php echo $categoria->fac_id;// id ?> </a> </td>
                        <td> <?php echo $categoria->fac_ambiente;// ambiente ?> </a> </td>
                        <td> <?php echo $categoria->fac_nome;// nome ?> </a> </td>
                        <td> <?php echo $categoria->fac_posicao;// posicao ?> </a> </td>
                        <td> <?php echo $categoria->fac_qtde_pergunta;// qtde pergunta ?> </a> </td>
                        <td> btn editar / btn desativar </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
			<?php endif; ?>




			<div class="portlet light bordered portlet-formulario hide-element">
			    <div class="portlet-title">
			        <div class="caption">
			            <i class="icon-speech"></i>
			            <span class="caption-subject bold uppercase"> Formulário</span>
			            <span class="caption-helper">Cadastrar Categoria</span>
			        </div>
			    </div>
			    <div class="portlet-body">
					
					<?php echo form_open('', array('id'=>'form-categoria', 'class'=>'form-horizontal form-view')); ?>
						<!-- Categoria -->
						<div class="row">
							<div class="form-group">
							    <label class="control-label col-md-3">Categoria
							        <span class="required" aria-required="true"> * </span>
							    </label>
							    <div class="col-md-4">
						            <input type="text" class="form-control" name="categoria" placeholder="">
							    </div>
							</div>
						</div>

						<!-- Ambiente -->
						<div class="row">
							<div class="form-group">
							    <label class="control-label col-md-3">Ambiente
							        <span class="required" aria-required="true"> * </span>
							    </label>
							    <div class="col-md-4">
						            <?php
										$options = array(
									        ''         			  => 'Selecione uma opção',
									        'FrontOffice'         => 'FrontOffice',
									        'BackOffice'          => 'BackOffice'
										);

										echo form_dropdown('ambiente', $options, '', array('class'=>'form-control'));
						            ?>
							    </div>
							</div>
						</div>

						<!-- Posicao -->
						<div class="row">
							<div class="form-group">
							    <label class="control-label col-md-3">Posição
							        <span class="required" aria-required="true"> * </span>
							    </label>
							    <div class="col-md-4">
						            <?php
							            $data = array(
							                    'type'  => 'number',
							                    'name'  => 'posicao',
							                    'class' => 'form-control'
							            );
							            echo form_input($data);						            ?>
							    </div>
							</div>
						</div>
						
						<div class="row">
							<div class="form-actions">
							    <input type="submit" class="btn blue" style="padding: 10px 40px;display: block;" value="Cadastrar">
							</div>
						</div>

					<?php echo form_close(); ?>

			    </div>
			</div>



	
	    </div>
	    <!-- /portlet-body -->

	</div>
    <!-- /portlet -->
