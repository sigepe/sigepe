<style type="text/css">
.active a{
    font-weight: bold !important;
}
.profile-sidebar{
    width: 100%;
}
#pessoa-sidebar{
    padding-left: 0px !important;
}
</style>



    <div class="col-sm-3" id="pessoa-sidebar">

        <div class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet " style="padding-top: 15px !important;">
                <!-- SIDEBAR USERPIC -->


                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu" style="margin-top: 0px !important;">
                    <ul class="nav">
                        <li class="<?php echo ($Class == 'Pessoa' && (isset($menuAtivo) && $menuAtivo == 'dashboard')) ? 'active' : ''; ?>">
                            <a href="{base_url}BackOffice/Pessoa/Dashboard/Detalhar/{pessoaId}">
                                <i class="icon-home"></i> Início </a>
                        </li>
                        <li class="<?php echo ($Class == 'Pessoa' && (isset($menuAtivo) && $menuAtivo == 'meuPerfil')) ? 'active' : ''; ?>">
                            <a href="{base_url}BackOffice/Pessoa/Perfil/Detalhar/{pessoaId}">
                                <i class="fa fa-user"></i> Meu Perfil </a>
                        </li>
                        <li class="<?php echo ($Class == 'Telefone' && (isset($menuAtivo) && $menuAtivo == 'telefone')) ? 'active' : ''; ?>">
                            <a href="{base_url}BackOffice/Pessoa/Telefone/Listar/{pessoaId}">
                                <i class="fa fa-phone"></i> Telefone </a>
                        </li>
                        <li class="<?php echo ($Class == 'Email' && (isset($menuAtivo) && $menuAtivo == 'email')) ? 'active' : ''; ?>">
                            <a href="{base_url}BackOffice/Pessoa/Email/Listar/{pessoaId}">
                                <i class="fa fa-envelope"></i> Email </a>
                        </li>
                        <li class="<?php echo ($Class == 'Endereco' && (isset($menuAtivo) && $menuAtivo == 'endereco')) ? 'active' : ''; ?>">
                            <a href="{base_url}BackOffice/Pessoa/Endereco/Listar/{pessoaId}">
                                <i class="fa fa-map-marker"></i> Endereço </a>
                        </li>
                        <li class="<?php echo ($Class == 'Configuracao' && (isset($menuAtivo) && $menuAtivo == 'configuracao')) ? 'active' : ''; ?>">
                            <a href="{base_url}BackOffice/Pessoa/Configuracao/Detalhar/{pessoaId}">
                                <i class="fa fa-cog"></i> Configuração </a>
                        </li>                        
                    </ul>
                </div>
                <!-- END MENU -->
            </div>
            <!-- END PORTLET MAIN -->
        </div>
        <!-- /profile-sidebar -->

    </div>
    <!-- /evento-sidebar -->
