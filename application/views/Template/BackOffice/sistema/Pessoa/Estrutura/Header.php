<style type="text/css">

.easy-autocomplete-container{
    top: 25px;
}
.easy-autocomplete{
    width: 100% !important;
}
#nome-proprietario{
    text-transform: uppercase;
}
.page-header .page-header-menu.fixed{
    position: relative;
}
.form-actions{
}
.form .form-actions, .portlet-form .form-actions{
    padding: 20px !important;
    margin: 0 !important;
    background-color: #f5f5f5 !important;
    border-top: 1px solid #e7ecf1 !important;
}
#baia-error,
#quarto-de-sela-error,
#ativar-site-error{
    display: block;
    width: 100%;
    float: left;
}

.page-head{
    display: none;
}

.profile-sidebar{
    width: 100%;
}

.dashboard-stat2{
    padding-bottom: 0px !important;
}


/* --------------- */


#nome-evento::first-line {
    font-size: 35px;
    font-weight: bold;
    letter-spacing: -1px;
}

img {
    border-color: transparent !important;
    box-shadow: 1px 1px 1px transparent !important;
}

</style>

<div class="row">

    <div class="col-sm-12 margin-top-10 margin-bottom-25" id="evento-top">
           <img src="{base_url}files/evento/{FolderId}/logotipo/{Logotipo}"
           onerror="this.onerror=null;this.src='{base_url}assets/sigepe/Global/Images/Evento/no-photo.png';"
           style="width:130px;border-radius: 10% !important;border: 2px solid #fff; box-shadow: 1px 1px 1px #999;float: left;width:200px; margin-right: 17px;">
           <div class="" style="float: left;width:80%;">
                <h3 id="nome-evento" style="margin-top: 0;"><?php echo nl2br($NomeEvento); ?></h3>
                <h4>
                    <div type="button" class="btn btn-default btn-sm" style="border-radius: 5px !important;">{Modalidade}</div>
                </h4>
           </div>
    </div>
    <!-- /evento-top -->
