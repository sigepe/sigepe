<style type="text/css">
    .h4, .h5, .h6, h4, h5, h6 {
        margin-top: 0px;
        margin-bottom: 5px;
    }
    hr, p {
        margin: 10px 0;
    }
    .portlet.light.bordered {
        border-bottom: 3px solid rgba(204, 204, 204, 0.45) !important;
    }

    img {
        max-height: 45px;
        max-width: 45px;
    }
</style>

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light bordered portlet-modalidade-tabela" style="">
    <div class="portlet-title">
        <div class="caption font-green-sharp">
            <i class="icon-speech font-green-sharp"></i>
            <span class="caption-subject bold uppercase"> Tabela</span>
            <span class="caption-helper">Relação de pessoas físicas</span>
        </div>
        <div class="actions">
            <!-- <a style="margin-bottom: 50px;" class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a> -->
        </div>
    </div>

    <div class="portlet-body">
        <table id="tabela-consulta-evento" class="table table-striped table-bordered table-hover">
            <?php 
            $arr = array("thead", "tfoot");
            foreach ($arr as $key):
                echo "<$key>"; ?>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>Status</th>
                    </tr>
                <?php echo "</$key>"; ?>
            <?php  endforeach; ?>
            
            <tbody>
                <?php 
                foreach ($DataSetPessoas as $pes): ?>
                    <tr>
                        <td><a href="{base_url}BackOffice/Pessoa/Detalhar/<?php echo $pes->pes_id?>" style="display: block;">#<?php echo $pes->pes_id; ?></a></td>
                        <td>
                            <img src="{base_url}assets/sigepe/Global/Images/Pessoa/<?php echo $pes->pes_foto; ?>"
                                onerror="this.onerror=null;this.src='{base_url}assets/sigepe/Global/Images/Pessoa/no-photo.jpg';" 
                                alt="<?php echo $pes->pes_nome_razao_social; ?>"
                            >
                            <!-- <img src="<?php echo $pes->pes_foto; ?>" alt="<?php echo $pes->pes_nome_razao_social; ?>"> -->
                            <a href="{base_url}BackOffice/Pessoa/Perfil/Detalhar/<?php echo $pes->pes_id?>"> <?php echo $pes->pes_nome_razao_social; ?> </a>
                        </td>
                        <td><?php echo $pes->pes_cpf_cnpj; ?> </td>
                        </td>
                        <td><?php echo $pes->sta_status; ?> </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->