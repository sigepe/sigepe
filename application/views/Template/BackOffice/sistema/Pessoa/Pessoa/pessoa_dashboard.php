
<style type="text/css" media="screen">

.tiles .tile{
    width: 100% !important;
}
    
</style>

<div class="col-sm-9">

    <div class="portlet light ">

        <div class="portlet-body">

            <div class="tiles">

                <div class="row">

                    <div class="col-sm-3">
                        <div class="tile bg-blue link-redirect" data-redirect="BackOffice/Pessoa/Perfil/Detalhar/{pessoaId}">
                            <div class="tile-body">
                                <i class="fa fa-address-card"></i>
                            </div>
                            <div class="tile-object">
                                <div class="name"> Editar Perfil </div>
                            </div>
                        </div>

                    </div>
                    

                    <div class="col-sm-3">
                        <div class="tile bg-blue link-redirect" data-redirect="BackOffice/Pessoa/Telefone/Listar/{pessoaId}">
                            <div class="tile-body">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="tile-object">
                                <div class="name"> Gerenciar Telefone </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-3">
                        <div class="tile bg-blue link-redirect" data-redirect="BackOffice/Pessoa/Email/Listar/{pessoaId}">
                            <div class="tile-body">
                                <i class="fa fa-envelope"></i>
                            </div>
                            <div class="tile-object">
                                <div class="name"> Gerenciar Email </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="tile bg-blue link-redirect" data-redirect="BackOffice/Pessoa/Endereco/Listar/{pessoaId}">
                            <div class="tile-body">
                                <i class="fa fa-map-marker"></i>
                            </div>
                            <div class="tile-object">
                                <div class="name"> Gerenciar Endereço </div>
                            </div>
                        </div>
                    </div>



                </div>
                <!-- /MENUS -->

            </div>

        </div>

    </div>
    <!-- /portlet-light -->




    <div class="portlet light ">

        <div class="portlet-body">

            <div class="tiles">


                <!-- 
                PERFIS
                ======================================= -->
                <?php
                $Perfis  =   $this->session->userdata('Perfis');
                if(!empty($Perfis)):
                ?>
                <div class="row tiles-perfis" style="margin: 0px;">


                    <div class="portlet light " style="padding: 0px;">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-dark bold uppercase">Perfil</span>
                            </div>
                        </div>
                        <div class="portlet-body row">

                            <?php foreach ($Perfis as $key => $value): ?>
                            <div class="col-sm-3">
                                <a class="tile bg-grey-cascade" href='{base_url}<?php echo ($value == '5') ? 'FrontOffice/Perfil/Atleta' : '';  ?>'>
                                    <div class="tile-body">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                        <?php echo $this->model_crud->get_rowSpecific('tb_vinculo_perfil', 'vip_id', $value, 1, 'vip_papel'); ?>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <?php endforeach; ?>

                        </div>
                    </div>
                    <!-- end portlet -->



                </div>
                <?php endif;  ?>
                <!-- /PERFIS -->
                    




                <!-- 
                EMPRESAS
                ======================================= -->
                <?php
                $Empresas  =   $this->session->userdata('Empresas');
                if(!empty($Empresas)):
                ?>
                <div class="row tiles-empresas">

                    <hr />


                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-dark bold uppercase">Empresas</span>
                            </div>
                        </div>
                        <div class="portlet-body row">

                            <?php foreach ($Empresas as $key => $value): ?>
                            <div class="col-sm-3">

                                <div class="tile bg-grey-cascade popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="<?php echo $this->model_crud->get_rowSpecific('tb_vinculo_tipo', 'vit_id', $value['TipoVinculo'], 1, 'vit_tipo_vinculo'); ?>" data-original-title="Vínculo">
                                    <a href="javascript:;">
                                        <div class="tile-body">
                                            <i class="fa fa-briefcase"></i>
                                        </div>
                                        <div class="tile-object">
                                            <div class="name"> 

                                            <?php

                                                if($value['TipoVinculo'] == '12' || $value['TipoVinculo'] == '16')
                                                    echo 'Confederação';

                                                if($value['TipoVinculo'] == '13' || $value['TipoVinculo'] == '17')
                                                    echo 'Federação';

                                                if($value['TipoVinculo'] == '14' || $value['TipoVinculo'] == '18')
                                                    echo 'Entidade Equestre';

                                                if($value['TipoVinculo'] == '15' || $value['TipoVinculo'] == '19')
                                                    echo 'Empresa Ordinária';

                                            ?>

                                            </div>
                                        </div>
                                    </a>
                                    <a href="javascript:;" class="btn blue"> <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value['IdEmpresa'], 1, 'pes_nome_razao_social') ?> </a>
                                </div>
                            </div>
                            <?php endforeach; ?>

                        </div>
                    </div>
                    <!-- end portlet -->

                </div>
                <?php endif; ?>
                <!-- /EMPRESAS -->
                    




            </div>
            <!-- /tiles -->


        </div>
    </div>


</div>
<!-- /evento-serie -->





