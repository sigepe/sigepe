<style type="text/css">
    .h4, .h5, .h6, h4, h5, h6 {
        margin-top: 0px;
        margin-bottom: 5px;
    }
    hr, p {
        margin: 10px 0;
    }
    .portlet.light.bordered {
        border-bottom: 3px solid rgba(204, 204, 204, 0.45) !important;
    }

    img {
        max-height: 45px;
        max-width: 45px;
    }
</style>

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light bordered portlet-modalidade-tabela" style="">
    <div class="portlet-title">
        <div class="caption font-green-sharp">
            <i class="icon-speech font-green-sharp"></i>
            <span class="caption-subject bold uppercase"> Tabela</span>
            <span class="caption-helper">Relação de pessoas físicas</span>
            <span class="caption-helper"><br>Legenda: Status do Perfil
                <br><small class="badge badge-default bold">Inativo : Data</small></span>
                <small class="badge badge-primary bold">Ativo : Data</small></span>
        </div>
        <div class="actions">
            <!-- <a style="margin-bottom: 50px;" class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a> -->
        </div>
    </div>

    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nome</th>
                    <th>Perfil</th>
                    <th>CPF</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Nome</th>
                    <th>Perfli</th>
                    <th>CPF</th>
                    <th>Status</th>
                </tr>
            </tfoot>
            <tbody>
                <?php 
                foreach ($DataSetPessoas as $pessoa): ?>
                    <tr>
                        <td><a href="{base_url}BackOffice/Pessoa/Perfil/Detalhar/<?php echo $pessoa->pes_id?>" style="display: block;">#<?php echo $pessoa->pes_id; ?></a></td>
                        <td>
                            <?php
                                $filename = "{base_url}assets/sigepe/Global/Images/Pessoa/$pessoa->pes_foto";
                                if (is_null($pessoa->pes_foto) == true || !file_exists($filename)){
                                    $filename = "{base_url}assets/sigepe/Global/Images/Pessoa/no-photo.jpg";
                                }
                            ?>

                            <img src="<?php echo $filename; ?>" alt="<?php echo ucwords(strtolower($pessoa->pes_nome_razao_social)); ?>">
                            <!-- <img src="<?php echo $pessoa->pes_foto; ?>" alt="<?php echo ucwords(strtolower($pessoa->pes_nome_razao_social)); ?>"> -->
                            <a href="{base_url}BackOffice/Pessoa/Perfil/Detalhar/<?php echo $pessoa->pes_id?>"> <?php echo ucwords(strtolower($pessoa->pes_nome_razao_social)); ?> </a>
                        </td>
                        <td><?php 
                            foreach ($pessoa->perfil as $perfil) {
                                $labelColor;
                                switch ($perfil->vin_fk_sta_id) {
                                    case 1:
                                        $labelColor = "primary";
                                        break;

                                    case 2:
                                    default:
                                        $labelColor = "default";
                                        break;
                                }

                                echo "<small class='badge badge-$labelColor bold' style='font-size: 14px !important; margin: 2px;'>".
                                $perfil->vip_papel." : ".date('m/d/Y',strtotime($perfil->vin_criado))."</small><br>";
                            }
                        ?> </td>
                        <td><?php echo $pessoa->pes_cpf_cnpj; ?> </td>
                        </td>
                        <td><?php echo $pessoa->sta_status; ?> </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->