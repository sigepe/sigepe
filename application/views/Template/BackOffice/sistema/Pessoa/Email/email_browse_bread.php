<style>
.btn-cadastrar-email {
    background: #03af03;
    border: 2px solid #11d911;
    color: white;
    transition: .5s;
}    
</style>
<div class="col-sm-9" id="evento-conteudo">

    <!-- BEGIN PROFILE CONTENT -->
    <div class="profile-content" id="meu-perfil">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light " style="width: 100%; float: left;">


                    <div class="portlet-title">
                        <div class="caption font-blue-madison">
                            <span class="caption-subject bold uppercase"> Emails</span>
                            <span class="caption-helper">Informações da conta</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-edit btn-sm btn-cadastrar-email" href="javascript:;">
                                <i class="fa fa-plus"></i>
                                Cadastrar Email
                            </a>
                        </div>
                    </div>

                    <div class="portlet-body">
                        <div class="tab-content">
                            <!-- PERSONAL INFO TAB -->
                            <div class="tab-pane active" id="tab_1_1">

                                <form role="form" action="#" class="form-view" id="form-perfil">
                                    <div class="table-scrollable table-scrollable-borderless">
                                        <table class="table table-hover table-light" id="table-email">
                                        
                                            <thead>
                                                <tr class="uppercase">
                                                    <th class="text-center">
                                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                                        Número
                                                    </th>
                                                    <th class="text-center"> Ação </th>
                                                </tr>
                                            </thead>

                                            <tbody>

                                                <?php foreach ($Datasetemail as $key => $value): ?>
                                                <tr id="table-row-<?php echo $value->ema_id; ?>" data-id="<?php echo $value->ema_id; ?>">
                                                    <td class="text-center">
                                                         
                                                        <?php if(!is_null($value->ema_principal)): ?>
                                                         <a href="javascript:;"
                                                            class="tooltips badge badge-warning bold flag-email-principal"
                                                            title=""
                                                            data-original-title="Email Principal. Preferencialmente vamos utilizar esse número para entrar em contato com você.">
                                                            P
                                                        </a>
                                                        <?php endif; ?>

                                                         <span class="bold"><?php echo $this->model_crud->get_rowSpecific('tb_contato', 'con_id', $value->fk_con_id, 1, 'con_contato'); ?>:</span>
                                                        <?php echo $value->ema_email; ?>

                                                    </td>
                                                    <td class="text-center">
                                                        <span class="btn btn-circle btn-sm btn-success btn-gerenciar-conjunto popovers" data-container="body" onclick=" " data-html="true" data-trigger="hover" data-placement="left" data-content="
                                                                <small> 

                                                                    <b>Cadastrado em:</b><br>
                                                                    <?php echo $this->my_data->datetime($value->criado, 'datetime_untilMinuts'); ?>

                                                                    <hr style='margin: 7px 0;'>

                                                                    <b>Por:</b><br>
                                                                    <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_aut_id, 1, 'pes_nome_razao_social'); ?> <br>

                                                                </small>
                                                            " data-original-title="Detalhes">
                                                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                            Detalhes
                                                        </span>

                                                        <?php if(is_null($value->ema_principal)): ?>
                                                        <a href="javascript:;"
                                                            class="btn btn-circle btn-sm btn-warning btn-email-principal tooltips"
                                                            data-original-title="Transformar esse email como principal."
                                                        >
                                                            
                                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                                            Email Principal
                                                        </a>
                                                        <?php endif; ?>

                                                        <?php if(is_null($value->ema_principal)): ?>
                                                        <a href="javascript:;" class="btn btn-circle btn-sm btn-danger btn-deletar-email">
                                                            <i class="fa fa-close" aria-hidden="true"></i>
                                                            Deletar Email
                                                        </a>
                                                        <?php endif; ?>

                                                    </td>
                                                </tr>
                                                <?php endforeach; ?>

                                            </tbody>

                                        </table>
                                    </div>
                                </form>
                            </div>
                            <!-- END PERSONAL INFO TAB -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="portlet light porlet-main" style="display: none;">

        <div class="portlet-body porlet-body-main">

            <form action="#" role="form" id="form-email" class="form-horizontal form-view">

                <div class="form-body">

                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-yellow-crusta">
                                <i class="icon-share font-yellow-crusta"></i>
                                <span class="caption-subject bold uppercase"> Formulário</span>
                                <span class="caption-helper">Preencha os campos abaixo para cadastrar um email a sua conta.</span>
                            </div>
                        </div>
                        <div class="portlet-body">

                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                Preencha todos os campos do formulário.
                            </div>

                            <div class="alert alert-success display-hide">
                                <button class="close" data-close="alert"></button>
                                Formulário validado!
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Email
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-phone"></i>
                                        </span>
                                        <input type="text" class="form-control mask-email" name="email" placeholder="Email"> </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Tipo do Email
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <select class="form-control" name="tipo">
                                        <option value="">- Selecione uma Opção -</option>
                                        <?php foreach ($DatasetTipoEmail as $key => $value): ?>
                                            <option value="<?php echo $value->con_id; ?>"><?php echo $value->con_contato; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <!-- /.portlet-body -->

                    </div>
                    <!-- /.portlet -->

                </div>
                <!-- /form-body -->

                <div class="form-actions text-center">
                    <a class="btn blue btn-submit">
                        <i class="fa fa-save" aria-hidden="true"></i>
                        Cadastrar Email
                    </a>
                </div>

            </form>
        </div>
    </div>
    <!-- END PROFILE CONTENT -->


</div>