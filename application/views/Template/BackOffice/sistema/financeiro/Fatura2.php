<style type="text/css">

.easy-autocomplete-container{
    top: 25px;
}
.easy-autocomplete{
    width: 100% !important;
}
#nome-proprietario{
    text-transform: uppercase;
}

</style>



        <div class="portlet light " id="form_wizard_1">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-red"></i>
                    <span class="caption-subject font-red bold uppercase"> MÓDULO: REGISTRO - Taxas</span>
                </div>
            </div>
            <div class="portlet-body form">


                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <ul class="nav nav-tabs tabs-left">
                            <?php foreach ($DatasetModalidades as $key => $value): ?>
                            <li class="<?php echo ($key == 0) ? 'active' : ''; ?>">
                                <a href="#tab_<?php echo $value->evm_slug; ?>" data-toggle="tab"> <?php echo $value->evm_modalidade; ?> </a>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-9">
                        <div class="tab-content">


                            <?php foreach ($DatasetModalidades as $key => $ValueModalidade): ?>
                            <div class="tab-pane <?php echo ($key == 0) ? 'active' : ''; ?>" id="tab_<?php echo $ValueModalidade->evm_slug; ?>">

                                <div class="tabbable-line">

                                    <ul class="nav nav-tabs ">
                                        <?php foreach ($DatasetAno as $key => $ValueAno): ?>
                                        <li class="<?php echo ($key == 0) ? 'active' : ''; ?>">
                                            <a
                                                href="#tab_<?php echo $ValueModalidade->evm_slug . '_' . $ValueAno->rea_ano; ?>"
                                                data-toggle="tab"
                                                aria-expanded="true"
                                                class="popovers"
                                                data-container="body"
                                                data-trigger="hover"
                                                data-content="
                                                    <small>
                                                        <span class='badge badge-warning' style='display:block;'> Atleta </span>
                                                        <b>Taxa de registro anual - CBH:</b><br>
                                                        R$ <?php echo $this->my_moeda->InserirPontuacao($ValueAno->rea_taxa_atleta_cbh); ?>
                                                        <hr style='margin:5px 0;'>
                                                        <b>Obrigatório recolher taxa anual - CBH?</b><br>
                                                        <?php echo (!is_null($ValueAno->rea_taxa_atleta_cbh_obrigatorio)) ? 'Sim' : 'Não' ?>
                                                        <hr style='margin:5px 0;'>
                                                        <b>Habilitar venda de registro anual (CBH)?</b>
                                                        <?php echo (!is_null($ValueAno->rea_habilitar_venda_registro_atleta_cbh)) ? 'Sim' : 'Não' ?>

                                                        <hr>

                                                        <span class='badge badge-warning' style='display:block;'> Animal </span>
                                                        <b>Taxa de registro anual - CBH:</b><br>
                                                        R$ <?php echo $this->my_moeda->InserirPontuacao($ValueAno->rea_taxa_animal_cbh); ?>
                                                        <hr style='margin:5px 0;'>
                                                        <b>Obrigatório recolher taxa anual - CBH?</b><br>
                                                        <?php echo (!is_null($ValueAno->rea_taxa_animal_cbh_obrigatorio)) ? 'Sim' : 'Não' ?>
                                                        <hr style='margin:5px 0;'>
                                                        <b>Habilitar venda de registro anual (CBH)?</b>
                                                        <?php echo (!is_null($ValueAno->rea_habilitar_venda_registro_animal_cbh)) ? 'Sim' : 'Não' ?>

                                                    </small>
                                                "
                                                data-html="true"
                                                data-original-title="<b>Definições de preço do registro da CBH para atletas e animais para o ano <?php echo $ValueAno->rea_ano; ?></b>"
                                            >
                                                    <?php echo $ValueAno->rea_ano; ?> <i class="fa fa-info-circle blue" style="color: #337ab7"></i> </a>
                                            </a>
                                        </li>
                                        <?php endforeach; ?>
                                    </ul>

                                    <div class="tab-content">

                                        <?php foreach ($DatasetAno as $key => $ValueAno): ?>
                                        <div class="tab-pane <?php echo ($key == 0) ? 'active' : ''; ?>" id="tab_<?php echo $ValueModalidade->evm_slug . '_' . $ValueAno->rea_ano; ?>">

                                           <?php
                                            $SqlAtleta                     = '
                                                                                    SELECT * FROM tb_registro_taxa
                                                                                    WHERE fk_rec_id = 1 AND 
                                                                                          fk_rea_id = '.$ValueAno->rea_id.' AND 
                                                                                          fk_evm_id = '.$ValueModalidade->evm_id.'      
                                                                                    ORDER BY fk_ret_id ASC
                                                                                ';
                                            $ResultadoAtleta   = $this->db->query($SqlAtleta)->result();

                                            $SqlAnimal                     = '
                                                                                    SELECT * FROM tb_registro_taxa
                                                                                    WHERE fk_rec_id = 2 AND 
                                                                                          fk_rea_id = '.$ValueAno->rea_id.' AND 
                                                                                          fk_evm_id = '.$ValueModalidade->evm_id.'      
                                                                                    ORDER BY flag_cavalos_novos ASC, fk_ret_id ASC 
                                                                                ';
                                            $ResultadoAnimal   = $this->db->query($SqlAnimal)->result();

                                            ?>

                                            <h3 style="margin-top: 0px;">Atletas</h3>
                                            <?php if (is_null($ResultadoAtleta) || empty($ResultadoAtleta)): ?>
                                            <div class="alert alert-danger"> <strong>Atenção!</strong> Não existe taxas para atletas nesta essa modalidade no ano selecionado. </div>
                                            <?php endif; ?>


                                            <?php if (!empty($ResultadoAtleta)): ?>
                                            <div class="table-scrollable">
                                                <table class="table table-hover table-light">
                                                    <thead>
                                                        <tr class="uppercase">
                                                            <th class="text-center"> # </th>
                                                            <th class="text-center"> Tipo </th>
                                                            <th class="text-center"> D.L.P. <a href="javascript:;" class="tooltips" data-original-title="Data Limite Permitida"> <i class="fa fa-info-circle"></i> </a> </th>
                                                            <th class="text-center"> Valor Promocional </th>
                                                            <th class="text-center"> Valor </th>
                                                            <th class="text-center"> Descontos </th>
                                                            <th class="text-center"> Status </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($ResultadoAtleta as $key => $value): ?>
                                                        <tr>
                                                            <td class="text-center"> <?php echo $value->ret_id; ?> </td>
                                                            <td class="text-center"> <?php echo $this->model_crud->get_rowSpecific('tb_registro_tipo', 'ret_id', $value->fk_ret_id, 1, 'ret_tipo'); ?> </td>
                                                            <td class="text-center"> <?php echo $this->my_data->ConverterData($value->ret_data_promocional, 'ISO', 'PT-BR'); ?> </td>
                                                            <td class="text-center"> <?php echo (!is_null($value->ret_preco_promocional)) ? 'R$ ' . $this->my_moeda->InserirPontuacao($value->ret_preco_promocional) : '-'; ?> </td>
                                                            <td class="text-center"> <?php echo (!is_null($value->ret_preco)) ? 'R$ ' . $this->my_moeda->InserirPontuacao($value->ret_preco) : '-'; ?> </td>
                                                            <td class="text-center"> 
                                                                <?php

                                                                    $SqlDescontos                   = '
                                                                                                            SELECT * FROM tb_registro_desconto
                                                                                                            WHERE fk_ret_id = '.$value->ret_id.'
                                                                                                            ';
                                                                    $ResultadoDescontos         = $this->db->query($SqlDescontos)->result();
                                                                    $QtdeDescontosNaTaxa        =   count($ResultadoDescontos);

                                                                    if($QtdeDescontosNaTaxa > 0):
                                                                ?>
                                                                <a data-toggle="modal" href="#descontos-<?php echo $value->ret_id; ?>" class="btn btn-circle btn-xs blue btn purple btn-outline sbold">
                                                                    Relação de Descontos
                                                                    <i class="fa fa-plus"></i>
                                                                </a>
                                                                <?php endif; ?>
 
                                                                <?php if($QtdeDescontosNaTaxa == 0): ?>
                                                                <a data-toggle="modal" href="#descontos-<?php echo $value->ret_id; ?>" class="btn btn-circle btn-xs grey btn grey btn-outline sbold">
                                                                    Taxa sem descontos
                                                                    <i class="fa fa-minus-circle"></i>
                                                                </a>
                                                                <?php endif; ?>
                                                            </td>
                                                            <td class="text-center">
                                                                <?php if($value->fk_sta_id == 1): ?>
                                                                <a href="javascript:;" class="btn btn-circle btn-xs blue tooltips" href="javascript:;" data-original-title="Taxa ativa. Essa taxa é comercializada no sistema."> Taxa ativa <i class="fa fa-info-circle"></i>  </a>
                                                                <?php endif; ?>

                                                                <?php if($value->fk_sta_id == 2): ?>
                                                                <a href="javascript:;" class="btn btn-circle btn-xs red tooltips" href="javascript:;" data-original-title="Taxa inativa. Essa taxa não é permitida a venda. No entanto não é possível excluí-la do sistema uma vez que podem haver registros de atletas/animais associados a taxas."> Taxa inativa <i class="fa fa-info-circle"></i>  </a>
                                                                <?php endif; ?>
                                                            </td>
                                                        </tr>
                                                        <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>  
                                            <?php endif; ?>

                                            <hr>

                                            <h3 style="margin-top: 0px;">Animais</h3>
                                            <?php if (is_null($ResultadoAnimal) || empty($ResultadoAnimal)): ?>
                                            <div class="alert alert-danger"> <strong>Atenção!</strong> Não existe taxas para animais nesta essa modalidade no ano selecionado. </div>
                                            <?php endif; ?>



                                            <?php if (!empty($ResultadoAnimal)): ?>
                                            <div class="table-scrollable">
                                                <table class="table table-hover table-light">
                                                    <thead>
                                                        <tr class="uppercase">
                                                            <th class="text-center"> # </th>
                                                            <th class="text-center"> Tipo </th>
                                                            <th class="text-center"> C.N. <a href="javascript:;" class="tooltips" data-original-title="Cavalos Novos 4 a 7 Anos (apenas para competirem na categoria Cavalos Novos)"> <i class="fa fa-info-circle"></i> </a> </th>
                                                            <th class="text-center"> D.L.P. <a href="javascript:;" class="tooltips" data-original-title="Data Limite Permitida"> <i class="fa fa-info-circle"></i> </a> </th>
                                                            <th class="text-center"> Valor Promocional </th>
                                                            <th class="text-center"> Valor </th>
                                                            <th class="text-center"> Descontos </th>
                                                            <th class="text-center"> Ações </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($ResultadoAnimal as $key => $value): ?>
                                                        <tr>
                                                            <td class="text-center"> <?php echo $value->ret_id; ?> </td>
                                                            <td class="text-center"> <?php echo $this->model_crud->get_rowSpecific('tb_registro_tipo', 'ret_id', $value->fk_ret_id, 1, 'ret_tipo'); ?> </td>
                                                            <td class="text-center"> <?php echo (is_null($value->flag_cavalos_novos)) ? '-' : 'Sim' ; ?> </td>
                                                            <td class="text-center"> <?php echo $this->my_data->ConverterData($value->ret_data_promocional, 'ISO', 'PT-BR'); ?> </td>
                                                            <td class="text-center"> <?php echo (!is_null($value->ret_preco_promocional)) ? 'R$ ' . $this->my_moeda->InserirPontuacao($value->ret_preco_promocional) : '-'; ?> </td>
                                                            <td class="text-center"> <?php echo (!is_null($value->ret_preco)) ? 'R$ ' . $this->my_moeda->InserirPontuacao($value->ret_preco) : '-'; ?> </td>
                                                            <td class="text-center"> 
                                                                <?php
                                                                    $SqlDescontos                   = '
                                                                                                            SELECT * FROM tb_registro_desconto
                                                                                                            WHERE fk_ret_id = '.$value->ret_id.'
                                                                                                            ';
                                                                    $ResultadoDescontos         = $this->db->query($SqlDescontos)->result();
                                                                    $QtdeDescontosNaTaxa        =   count($ResultadoDescontos);

                                                                    if($QtdeDescontosNaTaxa > 0):
                                                                ?>
                                                                <a data-toggle="modal" href="#descontos-<?php echo $value->ret_id; ?>" class="btn btn-circle btn-xs blue btn purple btn-outline sbold">
                                                                    Relação de Descontos
                                                                    <i class="fa fa-plus"></i>
                                                                </a>
                                                                <?php endif; ?>
 
                                                                <?php if($QtdeDescontosNaTaxa == 0): ?>
                                                                <a data-toggle="modal" href="#descontos-<?php echo $value->ret_id; ?>" class="btn btn-circle btn-xs grey btn grey btn-outline sbold">
                                                                    Taxa sem descontos
                                                                    <i class="fa fa-minus-circle"></i>
                                                                </a>
                                                                <?php endif; ?>
                                                            </td>
                                                            <td class="text-center">
                                                                <?php if($value->fk_sta_id == 1): ?>
                                                                <a href="javascript:;" class="btn btn-circle btn-xs blue tooltips" href="javascript:;" data-original-title="Taxa ativa. Essa taxa é comercializada no sistema."> Taxa ativa <i class="fa fa-info-circle"></i>  </a>
                                                                <?php endif; ?>

                                                                <?php if($value->fk_sta_id == 2): ?>
                                                                <a href="javascript:;" class="btn btn-circle btn-xs red tooltips" href="javascript:;" data-original-title="Taxa inativa. Essa taxa não é permitida a venda. No entanto não é possível excluí-la do sistema uma vez que podem haver registros de atletas/animais associados a taxas."> Taxa inativa <i class="fa fa-info-circle"></i>  </a>
                                                                <?php endif; ?>
                                                            </td>
                                                        </tr>
                                                        <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>  
                                            <?php endif; ?>

                                        </div>
                                        <?php endforeach; ?>


                                    </div>
                                </div>



                            </div>
                            <?php endforeach; ?>


                        </div>
                    </div>
                </div>




            </div>
        </div>



        

        <?php
            foreach ($DatasetTaxas as $key => $ValueTaxas):
                $Modalidade         =   $this->model_crud->get_rowSpecific('tb_evento_modalidade', 'evm_id', $ValueTaxas->fk_evm_id, 1, 'evm_modalidade');
                $Ano                =   $this->model_crud->get_rowSpecific('tb_registro_ano', 'rea_id', $ValueTaxas->fk_rea_id, 1, 'rea_ano');
                $TipoCompetidor     =   $this->model_crud->get_rowSpecific('tb_registro_competidor', 'rec_id', $ValueTaxas->fk_rec_id, 1, 'rec_competidor');
                $TipoRegistro       =   $this->model_crud->get_rowSpecific('tb_registro_tipo', 'ret_id', $ValueTaxas->fk_ret_id, 1, 'ret_tipo');;
        ?>
        <div class="modal fade bs-modal-lg" id="descontos-<?php echo $ValueTaxas->ret_id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h3 class="modal-title">Relação de Descontos para a <b>modalidade <?php echo $Modalidade ?></b> no <b>ano de <?php echo $Ano ?></b></h3>
                        <h5 class="modal-title"><?php echo ' Tipo de Competidor: ' . $TipoCompetidor . ' <br> Tipo de Registro: ' . $TipoRegistro; ?></h5>
                    </div>
                    <div class="modal-body">
                        
                        <?php
                            $SqlDescontos                   = '
                                                                    SELECT * FROM tb_registro_desconto
                                                                    WHERE fk_ret_id = '.$ValueTaxas->ret_id.'
                                                                    ';
                            $ResultadoDescontos         = $this->db->query($SqlDescontos)->result();
                        ?>


                        <?php if(empty($ResultadoDescontos)): ?>
                        <div class="alert alert-block alert-danger fade in">
                            <p> Não existe descontos para a taxa selecionada. </p>
                        </div>
                        <?php endif; ?>


                        <?php
                            if(!empty($ResultadoDescontos)):
                                $RegistroAnoId              =   $ValueTaxas->fk_rea_id;
                                $TaxaAtletaCbh              =   $this->model_crud->get_rowSpecific('tb_registro_ano', 'rea_id', $RegistroAnoId, 1, 'rea_taxa_atleta_cbh');
                                $TaxaAnimalCbh              =   $this->model_crud->get_rowSpecific('tb_registro_ano', 'rea_id', $RegistroAnoId, 1, 'rea_taxa_animal_cbh');
                                $TaxaFederacao              =   $ValueTaxas->ret_preco;

                                ($ValueTaxas->fk_rec_id == '1') ? $TaxaCbh = $TaxaAtletaCbh : $TaxaCbh = $TaxaAnimalCbh; // Se o fk_rec_id for 1(atleta) a $TaxaCbh assume o valor da taxa de atleta. Se nao, o valor de animal.
                        ?>
                        <div class="table-scrollable">
                            <table class="table table-hover table-light">
                                <thead>
                                    <tr>
                                        <th> # </th>
                                        <th> Período </th>
                                        <th> Desconto </th>
                                        <th> Valor Federação <a href="javascript:;" class="tooltips" data-original-title="Valor que a federação deve receber após inclusão do desconto."> <i class="fa fa-info-circle"></i> </a> </th>
                                        <th> Valor Federação + CBH <a href="javascript:;" class="tooltips" data-original-title="Valor que a federação deve receber após inclusão do desconto e a taxa da CBH."> <i class="fa fa-info-circle"></i> </a> </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($ResultadoDescontos as $key => $ValueDescontos): ?>
                                    <tr>
                                        <td> <?php echo $ValueDescontos->red_id; ?> </td>
                                        <td>
                                            <small>
                                                <b>Início:</b> <?php echo $this->my_data->ConverterData($ValueDescontos->red_data_inicio, 'ISO', 'PT-BR'); ?><br>
                                                <b>Fim:</b> <?php echo $this->my_data->ConverterData($ValueDescontos->red_data_fim, 'ISO', 'PT-BR'); ?>
                                            </small>
                                        </td>
                                        <td> 
                                            R$ <?php echo $this->my_moeda->InserirPontuacao($ValueDescontos->red_desconto); ?>
                                        </td>
                                        <td> 
                                            <?php
                                                $ValorDesconto              =   $ValueDescontos->red_desconto;
                                                $TaxaComDesconto               =   $TaxaFederacao - $ValorDesconto;   
                                                echo 'R$' . $this->my_moeda->InserirPontuacao($TaxaComDesconto);
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                $ValorDesconto              =   $ValueDescontos->red_desconto;
                                                $TaxaDesconto               =   $TaxaFederacao -$ValorDesconto + $TaxaCbh;   
                                                echo 'R$' . $this->my_moeda->InserirPontuacao($TaxaDesconto);
                                            ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <?php endif; ?>



                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <?php endforeach; ?>
        <!-- /.modal -->