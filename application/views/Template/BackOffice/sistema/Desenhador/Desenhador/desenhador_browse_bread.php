<?php ?>
<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content" id="meu-perfil">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light " style="width: 100%; float: left;">


                <div class="portlet-title">
                    <div class="caption font-blue-madison">
                        <span class="caption-subject bold uppercase"> Todos Desenhadores de Percurso</span>
                        <span class="caption-helper"></span>
                    </div>
                    <div class="actions">

                    </div>
                </div>

                <div class="portlet-body">
                    <div class="tab-content">
                        <!-- PERSONAL INFO TAB -->
                        <div class="tab-pane active" id="tab_1_1">

                            <form role="form" action="#" class="form-view" id="form-perfil">
                                <div class="table-scrollable table-scrollable-borderless">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">

                                        <thead>
                                            <tr class="uppercase">
                                                <th class="">Id Pessoa</th>
                                                <th class="">Nome</th>
                                                <th class="">Desenhador Desde</th>
                                                <th class="">Status</th>
                                            </tr>
                                        </thead>

                                        <tbody>

                                            <?php foreach ($dataSet as $desenhador): ?>
                                                <tr>
                                                    <td><a href="{base_url}BackOffice/Pessoa/Detalhar/<?php echo $desenhador->ped_fk_pes_id?>" style="display: block;">#<?php echo $desenhador->ped_fk_pes_id; ?></a></td>
                                                    <td><a href="{base_url}BackOffice/Pessoa/Detalhar/<?php echo $desenhador->ped_fk_pes_id?>" style="display: block;"><?php echo ucwords(strtolower($desenhador->ped_nome)); ?></a></td>
                                                    <td><?php echo date("d/m/Y - h:i:s", strtotime($desenhador->ped_criado)); ?></td>
                                                    <td>
                                                        <?php 
                                                        $corLabel = "";
                                                        switch ($desenhador->ped_fk_sta_id) {
                                                            case 1:
                                                                $corLabel = "primary";
                                                                break;
                                                                
                                                            case 2:
                                                            default:
                                                                $corLabel = "default";
                                                                break;
                                                        }
                                                        echo 
                                                        "<span class='label label-".$corLabel."'>". $desenhador->ped_status ."</span>";
                                                        ?>
                                                    </td>
                                                </tr>

                                            <?php endforeach; ?>

                                        </tbody>

                                    </table>
                                </div>
                            </form>
                        </div>
                        <!-- END PERSONAL INFO TAB -->

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="portlet light porlet-main" style="display: none;">

    <div class="portlet-body porlet-body-main">

        <form action="#" role="form" id="form-email" class="form-horizontal form-view">

            <div class="form-body">

                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-yellow-crusta">
                            <i class="icon-share font-yellow-crusta"></i>
                            <span class="caption-subject bold uppercase"> Formulário</span>
                            <span class="caption-helper">Preencha os campos abaixo para cadastrar um email a sua conta.</span>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button>
                            Preencha todos os campos do formulário.
                        </div>

                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button>
                            Formulário validado!
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Email
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </span>
                                    <input type="text" class="form-control mask-email" name="email" placeholder="Email"> </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Tipo do Email
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-4">
                                <select class="form-control" name="tipo">
                                    <option value="">- Selecione uma Opção -</option>
                                    <?php foreach ($DatasetTipoEmail as $key => $value): ?>
                                        <option value="<?php echo $value->con_id; ?>"><?php echo $value->con_contato; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                    </div>
                    <!-- /.portlet-body -->

                </div>
                <!-- /.portlet -->

            </div>
            <!-- /form-body -->

            <div class="form-actions text-center">
                <a class="btn blue btn-submit">
                    <i class="fa fa-save" aria-hidden="true"></i>
                    Cadastrar Email
                </a>
            </div>

        </form>
    </div>
</div>
<!-- END PROFILE CONTENT -->


</div>