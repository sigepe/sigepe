<?php 
?>
    <!-- BEGIN PROFILE CONTENT -->
    <div class="profile-content" id="meu-perfil">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light " style="width: 100%; float: left;">


                    <div class="portlet-title">
                        <div class="caption font-blue-madison">
                            <span class="caption-subject bold uppercase"> Todos Atletas</span>
                            <span class="caption-helper">Informações dos atletas</span>
                            <span class="caption-helper"><br>Legenda: Status do Registro na Modalidade
                                <br><small class="badge badge-default bold">Inativo : Data</small></span>
                                <small class="badge badge-primary bold">Ativo : Data</small></span>
                                <br><small class="badge badge-danger bold">Vencido : Data</small></span>
                                <small class="badge badge-warning bold">Cancelado : Data</small></span>

                        </div>
                        <div class="actions">
                            
                        </div>
                    </div>

                    <div class="portlet-body">
                        <div class="tab-content">
                            <!-- PERSONAL INFO TAB -->
                            <div class="tab-pane active" id="tab_1_1">

                                <form role="form" action="#" class="form-view" id="form-perfil">
                                    <div class="table-scrollable table-scrollable-borderless">
                                        <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        
                                            <thead>
                                                <tr class="uppercase">
                                                    <th class="">ID Atleta</th>
                                                    <th class="">ID Pessoa</th>
                                                    <th class="">Nome do Atleta <small><br>Nome da Pessoa</small></th>
                                                    <th class="">Federação</th>
                                                    <th class="">Entidade</th>
                                                    <th class="">modalidade</th>
                                                    <th class="">Status <small><br>Atleta</small></th>
                                                </tr>
                                            </thead>

                                            <tbody>

                                                <?php foreach ($dataset as $key => $atleta): ?>
                                                <tr>

                                                    <td><?php echo $atleta->pfa_id; ?></td>
                                                    <td><a href="{base_url}BackOffice/Pessoa/Perfil/Detalhar/<?php echo $atleta->fk_pes_id; ?>" style="display: block;">#<?php echo $atleta->fk_pes_id; ?></a></td>
                                                    <td><?php 
                                                        $nomeCompeticao = ucwords(strtolower($atleta->pfa_nome_competicao));
                                                        $nomeCompleto = ucwords(strtolower($atleta->pes_nome_razao_social));
                                                        
                                                        echo $nomeCompeticao."<small><br>".$nomeCompleto."</small>";
                                                        //echo $atleta->pfa_nome_competicao."<small><br>".$atleta->pes_nome_razao_social."</small>";
                                                    ?></td>

                                                    <td>
                                                    <?php
                                                    try {
                                                        if (!$atleta->pjf_acronimo) {
                                                            throw new Exception('');
                                                        }
                                                        $string = explode(" - ", $atleta->pjf_acronimo);
                                                    echo $string[0]."<br>";

                                                    } catch (Exception $e) {

                                                    }
                                                    // echo $atleta->pjf_acronimo;
                                                    ?>
                                                    </td>

                                                    <td>
                                                    <?php
                                                    try {
                                                        if (!$atleta->pje_entidade) {
                                                            throw new Exception('');
                                                        }
                                                        $string = explode(" - ", $atleta->pje_entidade);
                                                    echo $string[0]."<br>"."<small>".$string[1]."</small>"; 

                                                    } catch (Exception $e) {

                                                    }
                                                    // echo $atleta->pjf_acronimo;
                                                    ?>
                                                    </td>

                                                    <td><?php 
                                                        foreach ($atleta->modalidades as $modalidade) {
                                                            $labelColor;
                                                            switch ($modalidade->fk_sta_id) {
                                                                case 300:
                                                                default:
                                                                    $labelColor = "default";
                                                                    break;

                                                                case 301:
                                                                    $labelColor = "primary";
                                                                    break;

                                                                case 302:
                                                                    $labelColor = "danger";
                                                                    break;

                                                                case 303:
                                                                    $labelColor = "warning";
                                                                    break;
                                                            }

                                                            echo "<small class='badge badge-$labelColor bold'>".
                                                            $modalidade->evm_modalidade." : ".
                                                            substr ($modalidade->reg_criado, 0, 10)."</small><br>";
                                                        }

                                                    ?></td>
                                                    <td><?php echo $atleta->vin_status; ?></td>
                                                    
                                                </tr>
                                                <?php endforeach; ?>

                                            </tbody>

                                        </table>
                                    </div>
                                </form>
                            </div>
                            <!-- END PERSONAL INFO TAB -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="portlet light porlet-main" style="display: none;">

        <div class="portlet-body porlet-body-main">

            <form action="#" role="form" id="form-email" class="form-horizontal form-view">

                <div class="form-body">

                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-yellow-crusta">
                                <i class="icon-share font-yellow-crusta"></i>
                                <span class="caption-subject bold uppercase"> Formulário</span>
                                <span class="caption-helper">Preencha os campos abaixo para cadastrar um email a sua conta.</span>
                            </div>
                        </div>
                        <div class="portlet-body">

                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                Preencha todos os campos do formulário.
                            </div>

                            <div class="alert alert-success display-hide">
                                <button class="close" data-close="alert"></button>
                                Formulário validado!
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Email
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-phone"></i>
                                        </span>
                                        <input type="text" class="form-control mask-email" name="email" placeholder="Email"> </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Tipo do Email
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <select class="form-control" name="tipo">
                                        <option value="">- Selecione uma Opção -</option>
                                        <?php foreach ($DatasetTipoEmail as $key => $value): ?>
                                            <option value="<?php echo $value->con_id; ?>"><?php echo $value->con_contato; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <!-- /.portlet-body -->

                    </div>
                    <!-- /.portlet -->

                </div>
                <!-- /form-body -->

                <div class="form-actions text-center">
                    <a class="btn blue btn-submit">
                        <i class="fa fa-save" aria-hidden="true"></i>
                        Cadastrar Email
                    </a>
                </div>

            </form>
        </div>
    </div>
    <!-- END PROFILE CONTENT -->


</div>