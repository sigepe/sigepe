<?php 
?>
    <!-- BEGIN PROFILE CONTENT -->
    <div class="profile-content" id="meu-perfil">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light " style="width: 100%; float: left;">


                    <div class="portlet-title">
                        <div class="caption font-blue-madison">
                            <span class="caption-subject bold uppercase"> Todos Atletas</span>
                            <span class="caption-helper">Informações dos atletas</span>
                        </div>
                        <div class="actions">
                            
                        </div>
                    </div>

                    <div class="portlet-body">
                        <div class="tab-content">
                            <!-- PERSONAL INFO TAB -->
                            <div class="tab-pane active" id="tab_1_1">

                                <form role="form" action="#" class="form-view" id="form-perfil">
                                    <div class="table-scrollable table-scrollable-borderless">
                                        <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        
                                            <thead>
                                                <tr class="uppercase">
                                                    <th class="">ID Atleta</th>
                                                    <th class="">ID Pessoa</th>
                                                    <th class="">Nome do Atleta <small><br>Nome da Pessoa</small></th>
                                                    <th class="">Federação</th>
                                                    <th class="">Ativo desde</th>
                                                    <th class="">Status <small><br>Atleta</small></th>
                                                </tr>
                                            </thead>

                                            <tbody>

                                                <?php foreach ($dataset as $key => $atleta): ?>
                                                <tr>

                                                    <td><?php echo $atleta->pfa_id; ?></td>
                                                    <td><a href="{base_url}BackOffice/Pessoa/Perfil/Detalhar/<?php echo $atleta->fk_pes_id; ?>" style="display: block;">#<?php echo $atleta->fk_pes_id; ?></a></td>
                                                    <td><?php 
                                                        $nomeCompeticao = ucwords(strtolower($atleta->pfa_nome_competicao));
                                                        $nomeCompleto = ucwords(strtolower($atleta->pes_nome_razao_social));
                                                        
                                                        echo $nomeCompeticao."<small><br>".$nomeCompleto."</small>";
                                                    ?></td>

                                                    <td>
                                                    <?php
                                                    try {
                                                        if (!$atleta->pjf_acronimo) {
                                                            throw new Exception('');
                                                        }
                                                        $string = explode(" - ", $atleta->pjf_acronimo);
                                                    echo $string[0]; 

                                                    } catch (Exception $e) {

                                                    }
                                                    // echo $atleta->pjf_acronimo;
                                                    ?>
                                                    </td>
                                                    
                                                    <td><?php echo substr ($atleta->criado, 0, 10); ?></td>
                                                    <td><?php echo $atleta->sta_status; ?></td>
                                                    
                                                </tr>
                                                <?php endforeach; ?>

                                            </tbody>

                                        </table>
                                    </div>
                                </form>
                            </div>
                            <!-- END PERSONAL INFO TAB -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>