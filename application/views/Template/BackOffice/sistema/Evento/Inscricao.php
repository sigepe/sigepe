    <style type="text/css">

      #lista-serie-inscricao {
        float: left;
      }


        .sigepe .tiles .tile{
            overflow: initial !important;
        }

        .tiles .tile{
            border: none !important;
        }

        .tiles .tile .tile-body{
            margin-bottom: 0 !important;
        }




/*
       .sigepe .tiles .tile:after{
            content: "1";
            background: red;
            width: 100%;
            height: 30px;
        }
*/

        .tiles-empresas .tile{
            margin-bottom: 60px !important;
        }
        .sigepe .tiles .tile{
            width: 100% !important;
            margin-bottom: 0px;
        }
        .sigepe .tiles .tile .tile-object>.name{
            position: relative !important;
            margin-left: 0px !important;
            margin-right: 0px !important;
            text-align: center;
        }
        .sigepe .tiles{
            margin-right: 0px !important;
        }


        .btn.blue:not(.btn-outline) {
            color: #FFF;
            background-color: #3598dc;
            border-color: #3598dc;
            width: 100%;
            white-space: normal;
        }


    </style>

    <div class="col-sm-9 evento-conteudo" id="evento-inscricoes">


      <div class="portlet light bordered">
          <div class="portlet-title tabbable-line">
              <div class="caption">
                  <i class="icon-pin font-yellow-lemon"></i>
                  <span class="caption-subject bold font-yellow-lemon uppercase"> Inscrições </span>
                  <span class="caption-helper">Relatório dos Inscritos</span>
              </div>
              <ul class="nav nav-tabs">
                  <li class="">
                      <a href="#portlet_resumo" data-toggle="tab" aria-expanded="true"> Resumo </a>
                  </li>
                  <li class="active">
                      <a href="#portlet_todas" data-toggle="tab" aria-expanded="true"> Todas </a>
                  </li>
                  <li>
                      <a href="#portlet_serie" data-toggle="tab" aria-expanded="true"> Série </a>
                  </li>
                  <li>
                      <a href="#portlet_prova" data-toggle="tab"> Prova </a>
                  </li>
              </ul>
          </div>
          <div class="portlet-body">
              <div class="tab-content">






                  <!-- PORTLET DE INSCRICOES POR SERIE -->
                  <div class="tab-pane " id="portlet_resumo">


                      <div class="portlet-body">





                      </div>


                  </div>
                  <!-- FIM DO PORTLET POR SERIE --> 


                  <!-- PORTLET DE INSCRICOES POR SERIE -->
                  <div class="tab-pane active " id="portlet_todas">

                      <div class="portlet-body">
                          <table id="tabela-consulta-inscricoes" class="table table-striped table-bordered table-hover">
                              <thead>
                                  <tr class="">
                                      <th> Nº Inscrição </th>
                                      <th> Atleta </th>
                                      <th> Animal </th>
                                      <th> Categoria </th>
                                      <th> Entidade </th>
                                      
                                      <?php if (isset($datasetInscricoes[0]) && $datasetInscricoes[0]->flag_baia != null): ?>
                                        <th> Qtde. Baia </th>
                                      <?php endif; ?>
                                      <?php if (isset($datasetInscricoes[0]) && $datasetInscricoes[0]->flag_quarto != null): ?>
                                        <th> Qtde. Q.S. </th>
                                      <?php endif; ?>

                                      <th> Série </th>
                                      <th> Prova </th>

                                      <?php if (isset($datasetInscricoes[0]) && $datasetInscricoes[0]->flag_baia != null): ?>
                                        <th> Valor Baia </th>
                                      <?php endif; ?>
                                      <?php if (isset($datasetInscricoes[0]) && $datasetInscricoes[0]->flag_quarto != null): ?>
                                        <th> Valor Q.S. </th>
                                      <?php endif; ?>
                                      <th> Valor Inscrição </th>
                                      <th> Status <br> <small>Financeiro</small> </th>
                                      <th> Data </th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <?php
                                    foreach ($datasetInscricoes as $key => $inscricao):
                                  ?>
                                  <tr>
                                      <td><?php echo $inscricao->frf_controle; ?></td>
                                      <td><?php echo $inscricao->pes_nome_razao_social ;?></td>
                                      <td><?php echo $inscricao->ani_nome_completo ;?></td>
                                      <td><?php echo $inscricao->evc_sigla ;?></td>
                                      <td><?php echo $inscricao->entidade ;?></td>
                                      
                                      <?php if ($inscricao->flag_baia != null ): ?>
                                        <td><?php echo $inscricao->frf_quantidade_baia ;?></td>
                                      <?php endif; ?>
                                      <?php if ($inscricao->flag_quarto != null ): ?>
                                        <td><?php echo $inscricao->frf_quantidade_quarto ;?></td>
                                      <?php endif; ?>
                                      
                                      <td><?php echo $inscricao->ers_nome ;?></td>
                                      <td><?php 
                                      $sql = "SELECT * FROM tb_evento_faturaglobal_rel_faturasimples_rel_inscricao as fri
                                      join tb_evento_rel_serie_rel_prova as srp on srp.srp_id = fri.fk_srp_id
                                      where fri.fk_frf_id = $inscricao->frf_id order by srp_numero_prova asc";
                                      $arrProvas = $this->db->query($sql)->result();

                                      foreach ($arrProvas as $prova) {
                                        if (strlen($prova->srp_numero_prova) == 1){
                                          $prova->srp_numero_prova = "0". $prova->srp_numero_prova;
                                        }
                                        echo "<small>P.".$prova->srp_numero_prova."<br></small>";
                                      }


                                      ?></td>

                                      <?php if ($inscricao->flag_baia != null ): ?>
                                        <td><?php echo $inscricao->frf_valor_baia ;?></td>
                                      <?php endif; ?>
                                      <?php if ($inscricao->flag_quarto != null ): ?>
                                        <td><?php echo $inscricao->frf_valor_quarto ;?></td>
                                      <?php endif; ?>

                                      <td><?php 

                                      $SerieValor                             =   0.00;
                                      $SerieValorPromocional                  =   0.00;
                                      $ProvaAvulsaValor                       =   0.00;
                                      $ProvaAvulsaValorPromocional            =   0.00;
                                      $BaiaValor                              =   0.00;
                                      $BaiaValorPromocional                   =   0.00;
                                      $QuartoDeSelaValor                      =   0.00;
                                      $QuartoDeSelaValorPromocional           =   0.00;
                                      $ValorTotal                             =   0.00;
                                      $ValorTotalPromocional                  =   0.00;
                                      $FaturaSimplesId                        =   $inscricao->frf_id;

                                              /*
                                                   SERIE
                                              -----------------------------------------------*/
                                              $SqlValorSerie                     = "
                                                                                          SELECT *
                                                                                          FROM
                                                                                              tb_evento_faturaglobal_rel_faturasimples
                                                                                          WHERE
                                                                                              fk_ers_id IS NOT NULL AND
                                                                                              frf_controle = $inscricao->frf_controle
                                                                                          LIMIT 1
                                                                                  ";
                                              $Query  =   $this->db->query($SqlValorSerie)->result();

                                              if(!empty($Query)):
                                                  if(!is_null($Query[0]->fk_ers_id)){
                                                      $SerieValor                 =   $Query[0]->frf_valor_serie;
                                                      $SerieValorPromocional      =   $Query[0]->frf_valor_promocional_serie;
                                                  }
                                              endif;



                                              /*
                                                   PROVAS AVULSAS
                                              -----------------------------------------------*/
                                              $SqlValorProvaAvulsa                     = "
                                                                                          SELECT * FROM
                                                                                              tb_evento_faturaglobal_rel_faturasimples_rel_inscricao as fri

                                                                                          WHERE
                                                                                              fri.flag_deletado IS NULL AND
                                                                                              fri.flag_serie IS NULL AND
                                                                                              fri.fk_frf_id = ".$FaturaSimplesId."

                                                                                  ";
                                              $Query  =   $this->db->query($SqlValorProvaAvulsa)->result();


                                              if(!is_null($Query)):

                                                  $ProvaAvulsaValor   = 0;
                                                  foreach ($Query as $key => $value) {
                                                      //$ProvaAvulsaValor               += $value->fri_valor;
                                                      /*
                                                      if(!is_null($value->fri_valor_promocional)){
                                                          $ProvaAvulsaValorPromocional    += $value->fri_valor_promocional;
                                                      }else{
                                                          $ProvaAvulsaValorPromocional    += $value->fri_valor;
                                                      }*/
                                                      $ProvaAvulsaValor    += $value->fri_valor;

                                                  }


                                              endif;





                                              /*
                                                   BAIA
                                              -----------------------------------------------*/
                                              $SqlSomaValorBaia                     = "

                                                                                          SELECT * FROM tb_evento_faturaglobal_rel_faturasimples_rel_baia as frb

                                                                                          WHERE
                                                                                              frb.flag_deletado IS NULL AND
                                                                                              frb.fk_frf_id = '".$FaturaSimplesId."'
                                                                                  ";
                                              $Query  =   $this->db->query($SqlSomaValorBaia)->result();
                                              if(!is_null($Query)){

                                                  foreach ($Query as $key => $value) {
                                                      $BaiaValor               += $value->frb_valor;

                                                      if(!is_null($value->frb_valor_promocional)){
                                                          $BaiaValorPromocional    += $value->frb_valor_promocional;
                                                      }else{
                                                          $BaiaValorPromocional    += $value->frb_valor;
                                                      }
                                                  }

                                              }




                                              /*
                                                   QUARTO DE SELA
                                              -----------------------------------------------*/
                                              $SqlSomaValorQuarto                     = "

                                                                                          SELECT * FROM tb_evento_faturaglobal_rel_faturasimples_rel_quarto as frq

                                                                                          WHERE
                                                                                              frq.flag_deletado IS NULL AND
                                                                                              frq.fk_frf_id = '".$FaturaSimplesId."'
                                                                                  ";
                                              $Query  =   $this->db->query($SqlSomaValorQuarto)->result();
                                              if(!is_null($Query)){

                                                  foreach ($Query as $key => $value) {
                                                      $QuartoDeSelaValor               += $value->frq_valor;

                                                      if(!is_null($value->frq_valor_promocional)){
                                                          $QuartoDeSelaValorPromocional    += $value->frq_valor_promocional;
                                                      }else{
                                                          $QuartoDeSelaValorPromocional    += $value->frq_valor;
                                                      }
                                                  }

                                              }





                                              $ValorTotal                           =      $SerieValor + $ProvaAvulsaValor + $BaiaValor + $QuartoDeSelaValor;


                                              $ValorTotalPromocional                =      floatval($SerieValorPromocional) + floatval($ProvaAvulsaValor) + floatval($BaiaValorPromocional) + floatval($QuartoDeSelaValorPromocional);

                                      /*
                                              echo "<br>";
                                              echo " SERIE VLAOR PROMOCIONAL " . $SerieValorPromocional . "<br>";
                                              echo "PROVA AVULSA  PROMOCIONAL " . $ProvaAvulsaValorPromocional . "<br>";
                                              echo "BAIA  PROMOCIONAL " . $BaiaValorPromocional . "<br>";
                                              echo "QUARTO VALOR PROMOCIONAL " . $QuartoDeSelaValorPromocional . "<br>";

                                              echo "VALOR TOTAL" . */


                                              $ResumoValorTotal                 =   $ValorTotal;
                                              $ResumoValorTotalPromocional      =   $ValorTotalPromocional;

                                              echo number_format($ResumoValorTotalPromocional, 2, ',' , '.');


                                      ?></td>
                                      <td><?php echo ($inscricao->frf_sta_id != 1) ? $inscricao->sta_status : "" ; ?></td>
                                      <td><?php echo $inscricao->frf_criado; ?></td>

                                  </tr>
                                  <?php endforeach; ?>
                              </tbody>
                          </table>


                      </div>


                  </div>
                  <!-- FIM DO PORTLET POR SERIE --> 




                  <!-- PORTLET DE INSCRICOES POR SERIE -->
                  <div class="tab-pane " id="portlet_serie">


                      <div class="portlet-body">

                          <div class="row">
                              <div class="col-md-2 col-sm-2 col-xs-2">
                                  <ul class="nav nav-tabs tabs-left">


                                      <?php
                                          $Contador   =   1;
                                          foreach ($DatasetSerie as $key => $value):
                                      ?>

                                      <li class="<?php echo ($Contador===1) ? 'active' : ''; ?>">
                                          <a href="#tab-<?php echo $value->ers_id; ?>" data-toggle="tab" class="tooltips" data-container="body" data-placement="left" data-original-title="<?php echo $value->ers_nome; ?>">
                                              <i class="fa fa-caret-right" aria-hidden="true"></i>
                                              <?php echo $value->evs_altura . 'M'; ?> <br>
                                          </a>
                                      </li>
                                      <?php
                                          $Contador++;
                                          endforeach;
                                      ?>


                                  </ul>
                              </div>

                              <div class="col-md-10 col-sm-10 col-xs-10">

                                  <div class="tab-content">



                                      <?php
                                          $Contador = 1;
                                          foreach ($DatasetSerie as $key => $ValueSerie):
                                      ?>

                                      <div class="tab-pane" id="tab-<?php echo $ValueSerie->ers_id; ?>">
                                          <h4 style="font-weight: 600; padding-top:0px;margin-top:0px;">
                                            <?php echo $ValueSerie->evs_altura . 'M'; ?> - <?php echo $ValueSerie->ers_nome; ?>
                                          </h4>
                                          <hr>


                                          <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                                              <thead>
                                                  <tr class="">
                                                      <th> Inscrição </th>
                                                      <th> Conjunto </th>
                                                      <th> Status <br> <small>Financeiro</small> </th>
                                                      <th> Valor <br> <small>Fatura</small> </th>
                                                      <th> Valor <br> <small>Promocional Série</small> </th>
                                                      <th> Qtde Baia <br> <small>Valor Pago</small> </th>
                                                      <th> Qtde QS <br> <small>Valor Pago</small> </th>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                  <?php
                                                    foreach ($DatasetInscricoesPorSerie as $key => $Inscricao):
                                                        if($Inscricao->fk_ers_id != $ValueSerie->ers_id)
                                                          continue;
                                                  ?>
                                                  <tr>
                                                      <td> <?php echo $Inscricao->frf_controle; ?> </td>
                                                      <td>
                                                        <b> <?php echo $Inscricao->pes_nome_razao_social; ?> </b>
                                                        <br>
                                                        <small>
                                                            (
                                                              <?php
                                                                  $NomeEmpresa  =  $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $Inscricao->fk_pjf_id, 1, 'pes_nome_razao_social');
                                                                  $Arr = explode(" ", $NomeEmpresa);
                                                                  echo $Arr[0];
                                                                  if(empty($Arr[0]))
                                                                    echo "Sem Federação";
                                                              ?>
                                                              /
                                                              <?php
                                                                  $NomeEmpresa  =  $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $Inscricao->fk_pje_id, 1, 'pes_nome_razao_social');
                                                                  $Arr = explode(" ", $NomeEmpresa);
                                                                  echo $Arr[0];
                                                                  if(empty($Arr[0]))
                                                                    echo "Sem Entidade";
                                                              ?>
                                                            /
                                                            <?php echo $Inscricao->evc_sigla; ?> )
                                                        </small>
                                                        <hr style=" margin: 10px 0; ">
                                                        <b> <?php echo $Inscricao->ani_nome_completo; ?></b>
                                                        <small>
                                                          (
                                                          <?php
                                                              $NomeEmpresa  =  $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $Inscricao->AnimalEntidadeId, 1, 'pes_nome_razao_social');
                                                              $Arr = explode(" ", $NomeEmpresa);
                                                              echo $Arr[0];
                                                          ?>
                                                          )
                                                        </small>
                                                      </td>
                                                      <td>
                                                          <?php if($Inscricao->fk_sta_id != 601): ?>
                                                          <span class="badge badge-danger"> Aguardando pagamento </span>
                                                          <?php endif ?>
                                                          
                                                          <?php if($Inscricao->fk_sta_id == 601): ?>
                                                          <span class="badge badge-success"> Pago </span>
                                                          <?php endif ?>
                                                      </td>
                                                      <td> R$
                                                          <?php
                                                              $Somatorio = 0;
                                                              
                                                              if($Inscricao->frf_quantidade_baia > 0) 
                                                                $Somatorio += ($Inscricao->frf_valor_baia * $Inscricao->frf_quantidade_baia);
                                                              
                                                              if($Inscricao->frf_quantidade_quarto > 0) 
                                                                $Somatorio += ($Inscricao->frf_valor_quarto * $Inscricao->frf_quantidade_quarto);

                                                              $Somatorio += $Inscricao->frf_valor_promocional_serie;

                                                              echo $Somatorio;
                                                          ?>
                                                      </td>

                                                      <td> R$ <?php echo $Inscricao->frf_valor_promocional_serie; ?> </td>
                                                      <td> 
                                                        <?php echo ($Inscricao->frf_quantidade_baia > 0) ? $Inscricao->frf_quantidade_baia . ' Baia(s)' : '-'; ?> 
                                                        <?php if($Inscricao->frf_quantidade_baia > 0): ?>
                                                        <br>
                                                        <small>
                                                        R$ <?php echo $Inscricao->frf_valor_baia * $Inscricao->frf_quantidade_baia ; ?>
                                                        </small>
                                                        <?php endif; ?>
                                                      </td>
                                                      <td> 
                                                        <?php echo ($Inscricao->frf_quantidade_quarto > 0) ? $Inscricao->frf_quantidade_quarto . ' Q.S.' : '-'; ?> 
                                                        <?php if($Inscricao->frf_quantidade_quarto > 0): ?>
                                                        <br>
                                                        <small>
                                                        R$ <?php echo $Inscricao->frf_valor_quarto * $Inscricao->frf_quantidade_quarto ; ?>
                                                        </small>
                                                        <?php endif; ?>
                                                      </td>
                                                      <td>
                                                        <?php echo $Inscricao->frf_criado; ?>
                                                      </td>
                                                  </tr>
                                                  <?php endforeach; ?>
                                              </tbody>
                                          </table>





                                      </div>

                                      <?php
                                          $Contador++;
                                          endforeach;
                                      ?>

                                  </div>
                                  <!-- /tab-content -->

                              </div>
                              <!-- /col-md-9 -->

                          </div>
                          <!-- /row -->

                      </div>
                      <!-- /portlet-body -->











                              <!--
                              // Serie entra como parametro de where.

//                                WHERE
                                    - ID DO Evento
                                    - ID Da Serie / da Prova
                              -->



                  </div>
                  <!-- FIM DO PORTLET POR SERIE -->


                  <div class="tab-pane " id="portlet_prova">

                    PROVA PORLET

                    <div class="portlet-body">

                        <div class="row">
                            <div class="col-md-2 col-sm-2 col-xs-2">
                                <ul class="nav nav-tabs tabs-left">

                                    <?php
                                        $Contador = 1;
                                        foreach ($DatasetProva as $key => $value):
                                    ?>
                                    <li class="<?php echo ($Contador == '1') ? 'active' : ''; ?>">
                                        <a href="#tab_<?php echo $value->srp_id; ?>" data-toggle="tab">
                                            Pr. <?php echo ($value->srp_numero_prova < 10 ) ? '0' . $value->srp_numero_prova : $value->srp_numero_prova; ?>
                                        </a>
                                    </li>
                                    <?php
                                        $Contador++;
                                        endforeach;
                                    ?>

                                </ul>
                            </div>

                            <div class="col-md-10 col-sm-10 col-xs-10">

                                <div class="tab-content">

                                    <?php
                                        $Contador = 1;
                                        foreach ($DatasetProva as $key => $value):
                                    ?>
                                    <?php
                                        $Contador++;
                                        endforeach;
                                    ?>

                                </div>
                                <!-- /tab-content -->

                            </div>
                            <!-- /col-md-9 -->

                        </div>
                        <!-- /row -->

                    </div>
                    <!-- /portlet-body -->

                  </div>





              </div>
          </div>
      </div>




</div>
<!-- /evento-inscricoes -->
