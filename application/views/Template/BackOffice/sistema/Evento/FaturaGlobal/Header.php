<style type="text/css">
    .todo-tasklist-item{
        background: #eaf1f2;
    }
    .fatura-global-preco-total{

    }
    .status-aguardando-pagamento .fatura-global-preco-total{
        color: #f11a1a !important;
    }

    .boleto-todas-inscricoes{
        background: #03af03;
        border: 2px solid #11d911;
        color:white;
        transition: .5s;
    }
    .boleto-todas-inscricoes:hover{
        background: #057605;
        border-color: #084008;
        color: white;
    }


    .boleto-inscricao{
        background: #03af03;
        border: 2px solid #11d911;
        color:white;
        transition: .5s;
    }
    .boleto-inscricao:hover{
        background: #057605;
        border-color: #084008;
        color: white;
    }

    .fatura-global-btns {
        border-top: 1px dashed #ccc;
        padding: 9px 0;
        margin-top: 13px;
        padding-top: 15px;
    }
    .status-aguardando-pagamento{
        border-left: #f11a1a 7px solid;
        padding-left: 25px !important;
    }
    .status-aguardando-pagamento .todo-tasklist-item-title{
        color: #f11a1a;
    }
    .tooltips-status-aguardando-pagamento{
        background: #f11a1a;
        display: block;
        color: white;
        border: 2px solid #b70f0f;
        font-weight: bold;
    }
    .badge-status-aguardando-pagamento{
        background: #f11a1a;
        color: white;
        font-weight: bold;
    }
    .status-aguardando-pagamento .bloco-cabecalho-direita .clearfix{
        margin-top: 45px;
        text-align: center;
        color: #1f869b;
    }
</style>



    <div class="row">
        <div class="col-sm-12 margin-top-10 margin-bottom-25" id="evento-top">
               <img src="{base_url}assets/sigepe/global/images/evento/CSN-Brasilia-Indoor-logo.png" style="width:130px;border-radius: 10% !important;border: 2px solid #fff; box-shadow: 1px 1px 1px #999;margin-right: 35px;float: left;">
               <div class="" style="float: left;">
                    <h3 id="nome-evento" style="margin-top: 0;"><?php echo nl2br($DatasetEvento[0]->eve_nome); ?></h3>
                    <h4>
                        <div type="button" class="btn btn-default btn-sm" style="border-radius: 5px !important;">
                            <b>Status:</b> <?php echo $DatasetEvento[0]->fk_sta_id; ?>
                        </div>
                    </h4>
               </div>
        </div>
        <!-- /evento-top -->
    </div>



    <!-- tabbable-custom -->
    <div class="tabbable-custom ">

        <!-- ul.nav-tabs -->
        <ul class="nav nav-tabs ">

            <li class="active">
                <a href="#tab-informacoes" data-toggle="tab"> INFORMAÇÕES DO EVENTO </a>
            </li>

            <li>
                <a href="#tab-inscricoes" data-toggle="tab"> INSCRIÇÕES </a>
            </li>

            <li>
                <a href="#tab-financeiro" data-toggle="tab" style="display: none"> FINANCEIRO </a>
            </li>

<!--
            <li>
                <a href="#tab-duvidas" data-toggle="tab"> Dúvidas? </a>
            </li>
 -->

        </ul>

        <!-- tab-content -->
        <div class="tab-content">



            <!-- tab-informacoes -->
            <div class="tab-pane" id="tab-informacoes">
                <p> Informações do Evento. </p>
            </div>
            <!-- /div#tab-informacoes -->




            <!-- tab-inscricoes -->
            <div class="tab-pane active" id="tab-inscricoes">


                <a href="{base_url}inscricao/FrontOffice/Inscricao/Formulario/<?php echo $DatasetEvento[0]->eve_id; ?>" class="btn btn-circle display-block yellow-lemon  margin-top-15" target="_blank">
                    <i class="fa fa-plus"></i> NOVA INSCRIÇÃO
                </a>

                <hr>


                <div class="tabbable-line">
                    <ul class="nav nav-tabs ">
                        <li>
                            <a href="#tab-minhas-inscricoes" data-toggle="tab" title="Nessa aba é exibido as inscrições caso você possua algum vínculo com o atleta ou o animal. ">
                                <i class="fa fa-caret-right" aria-hidden="true"></i>
                                Minhas <b>Inscrições</b>
                            </a>
                        </li>
                        <li class="active">
                            <a href="#tab-inscricoes-que-realizei" data-toggle="tab" title="Toda inscrição que você realizar para você ou para os outros será listado nessa aba.">
                                <i class="fa fa-caret-right" aria-hidden="true"></i>
                                Inscrições <b>que eu realizei</b>
                            </a>
                        </li>
                    </ul>


                    <div class="tab-content" style="padding: 15px 0 10px 0 !important;">


                        <!-- tab-minhas-inscricoes -->
                        <div class="tab-pane" id="tab-minhas-inscricoes">
                            <div class="alert alert-warning" style="margin-bottom: 0px !important;">
                                <strong>Atenção!</strong>
                                Nenhuma inscrição localizada para o seu perfil.
                            </div>
                        </div>
                        <!-- /div#tab-minhas-inscricoes -->


                        <!-- tab-inscricoes-que-realizei -->
                        <div class="tab-pane active" id="tab-inscricoes-que-realizei">


                            <?php foreach ($DatasetInscricoesQueRealizei as $key => $FaturaGlobal): ?>
                            <div class="todo-tasklist-item todo-tasklist-item-border-green status-aguardando-pagamento">

                                <!-- Preco -->
                                <div class="row">
                                    <div class="col-sm-8">


                                        <div class="todo-tasklist-item-title"
                                            style="padding-top: 20px;font-size: 19px; font-weight: bold;">
                                                FATURA: #<?php echo $FaturaGlobal->evf_controle; ?>

                                                | <span style="font-weight: 500;">ATLETA: <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $FaturaGlobal->fk_pes_id, 1, 'pes_nome_razao_social'); ?></span>
                                        </div>


                                        <div class="todo-tasklist-controls pull-left fatura-global-preco-total" style="font-size: 18px; width: 100%; ">
                                                <i class="fa fa-usd"></i> Preço Total: <b> R$ <?php echo $this->my_moeda->InserirPontuacao($FaturaGlobal->evf_valor); ?> </b>


                                                <span class="todo-tasklist-badge badge badge-status-aguardando-pagamento" style="margin-left: 20px;">
                                                    Aguardando Pagamento
                                                </span>

                                        </div>


                                        <!--
                                        <div class="todo-tasklist-controls fatura-global-btns" style="float: left; width: 100%;">
                                            <a href="javascript:;" class="btn btn-circle btn-sm boleto-todas-inscricoes">
                                                <i class="fa fa-usd" aria-hidden="true"></i>
                                                Boleto todas inscrições
                                            </a>
                                            <a href="javascript:;" class="btn btn-circle btn-sm blue">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                Adicionar Conjunto a essa Fatura
                                            </a>
                                        </div>
                                        -->

                                    </div>
                                    <div class="col-sm-4 bloco-cabecalho-direita">
                                        <div class="clearfix">

                                            <i class="fa fa-calendar"></i>
                                             Gerada em <?php echo strftime('%d de %B de %Y', strtotime($FaturaGlobal->criado)); ?>
                                            <br>
                                            por <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $FaturaGlobal->fk_aut_id, 1, 'pes_nome_razao_social'); ?>

                                        </div>
                                    </div>
                                </div>

                                <div class="tab-gerenciar-conjunto todo-tasklist-item-text" style="display: block;">

                                    <div class="table-scrollable">
                                        <table class="table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th> N° Inscrição </th>
                                                    <th> Conjunto ( Atleta / Animal ) </th>
                                                    <th class="text-center"> Preço </th>
                                                    <th class="text-center"> Financeiro </th>
                                                    <th class="text-center"> Gerenciar </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    $this->load->module('evento/FrontOffice/FaturaSimples');
                                                    $FaturaGlobalId             =   $FaturaGlobal->evf_id;
                                                    $DatasetFaturaSimples       =   $this->faturasimples->GetFaturaSimples($FaturaGlobalId);
                                                    foreach ($DatasetFaturaSimples as $key => $FaturaSimples):
                                                ?>
                                                <tr>
                                                    <td>
                                                        #<?php echo $FaturaSimples->frf_controle; ?>
                                                    </td>
                                                    <td>

                                                        <!-- <img src="http://fhbr.dev/assets/pages/media/users/avatar4.jpg" class="rounded" style="height: 30px;" /> -->
                                                        <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $FaturaSimples->fk_pes_id, 1, 'pes_nome_razao_social'); ?>
                                                        <small class="tooltips" title="<?php echo $this->model_crud->get_rowSpecific('tb_evento_categoria', 'evc_id', $FaturaSimples->fk_evc_id, 1, 'evc_categoria'); ?>" data-original-title="12">
                                                            <?php echo $this->model_crud->get_rowSpecific('tb_evento_categoria', 'evc_id', $FaturaSimples->fk_evc_id, 1, 'evc_sigla'); ?>
                                                        </small>


                                                        <br>

                                                        <!-- <img src="http://4.bp.blogspot.com/_KJM7MbMirrM/TQ6n4nWN68I/AAAAAAAAG7M/Kk33IzENaq4/s400/2011%2BJockey%2BBrian%2BNyawo%2B3.jpg" class="rounded" style="height: 30px;" /> -->
                                                        <?php echo $this->model_crud->get_rowSpecific('tb_animal', 'ani_id', $FaturaSimples->fk_ani_id, 1, 'ani_nome_completo'); ?>

                                                    </td>
                                                    <td class="text-center">
                                                        R$ <?php echo $this->my_moeda->InserirPontuacao($FaturaSimples->frf_valor); ?>
                                                    </td>
                                                    <td class="text-center">
                                                        <span class="btn btn-circle btn-sm btn-default tooltips tooltips-status-aguardando-pagamento" data-original-title="Aguardando pagamento. Pague e garanta sua inscrição.">
                                                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                            Inscrição não confirmada
                                                        </span>
                                                    </td>
                                                    <td class="text-center">

                                                        <!--
                                                        <span
                                                            class="btn btn-circle btn-sm btn-success btn-gerenciar-conjunto popovers"
                                                            data-container="body"
                                                            onclick=" "
                                                            data-html="true"
                                                            data-trigger="hover"
                                                            data-placement="left"
                                                            data-content="

                                                                <small>

                                                                    <b>Inscrição N°:</b><br>
                                                                    <?php echo $FaturaSimples->frf_controle; ?> <br>

                                                                    <hr style='margin:8px 0;'>

                                                                    <b>Provas:</b><br>
                                                                    Pr. 08 ( 1.10M ) <br>
                                                                    Pr. 09 ( 1.10M ) <br>
                                                                    Pr. 12 ( 1.10M ) <br>
                                                                    Pr. 14 ( 1.35M ) <br>

                                                                    <hr style='margin:8px 0;'>

                                                                    <b>Baias:</b><br>
                                                                    Sim ( 2 )

                                                                    <hr style='margin:8px 0;'>

                                                                    <b>Quarto de Sela:</b><br>
                                                                    Não

                                                                </small>

                                                            "
                                                            data-original-title="Detalhes">
                                                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                            Detalhes da Inscrição
                                                        </span>
                                                        -->



                                                        <a href="{base_url}FrontOffice/FaturaSimples/{EventoId}/<?php echo $FaturaSimples->frf_controle; ?>" class="btn btn-circle btn-sm btn-primary btn-gerenciar-conjunto"> <i class="fa fa-pencil" aria-hidden="true"></i> Detalhes da inscrição </a>
                                                        <a href="<?php echo $FaturaSimples->frf_boleto; ?>" class="btn btn-circle btn-sm btn-primary boleto-inscricao">  <i class="fa fa-usd" aria-hidden="true"></i> Boleto Inscrição </a>
                                                    </td>
                                                </tr>
                                                <?php endforeach; ?>

                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                            </div>
                            <?php endforeach; // fim de fatura global  ?>





                            <div class="alert alert-warning" style="display: none;">
                                <strong>Atenção!</strong>
                                Você não realizou nenhuma inscrição.
                            </div>


                        </div>
                        <!-- /div#tab-inscricoes-que-realizei -->



                    </div>
                    <!-- /div.tab-content -->

                </div>
                <!-- /div.tabbable-line -->

            </div>
            <!-- /div#tab-inscricoes -->




            <!-- tab-financeiro -->
            <div class="tab-pane" id="tab-financeiro" style="display: none">
                <p> Howdy, I'm in Section 3. </p>
                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                    Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat </p>
                <p>
                    <a class="btn yellow" href="ui_tabs_accordions_navs.html#tab_1_1_3" target="_blank"> Activate this tab via URL </a>
                </p>
            </div>
            <!-- /div#tab-financeiro -->



        </div>
        <!-- /div.tab-content -->

    </div>
    <!-- /tabbable-custom -->
