<style type="text/css">

.easy-autocomplete-container{
    top: 25px;
}
.easy-autocomplete{
    width: 100% !important;
}
#nome-proprietario{
    text-transform: uppercase;
}
.page-header .page-header-menu.fixed{
    position: relative;
}
.form-actions{
}
.form .form-actions, .portlet-form .form-actions{
    padding: 20px !important;
    margin: 0 !important;
    background-color: #f5f5f5 !important;
    border-top: 1px solid #e7ecf1 !important;
}
#prova-dependente-error{
    display: block;
    width: 100%;
    float: left;
}


.ms-container{
    width: 600px;
}
</style>




    <style type="text/css">

        .sigepe .tiles .tile{
            overflow: initial !important;
        }

        .tiles .tile{
            border: none !important;
        }

        .tiles .tile .tile-body{
            margin-bottom: 0 !important;
        }




/*
       .sigepe .tiles .tile:after{
            content: "1";
            background: red;
            width: 100%;
            height: 30px;
        }
*/

        .tiles-empresas .tile{
            margin-bottom: 60px !important;
        }
        .sigepe .tiles .tile{
            width: 100% !important;
            margin-bottom: 0px;
        }
        .sigepe .tiles .tile .tile-object>.name{
            position: relative !important;
            margin-left: 0px !important;
            margin-right: 0px !important;
            text-align: center;
        }
        .sigepe .tiles{
            margin-right: 0px !important;
        }


        .btn.blue:not(.btn-outline) {
            color: #FFF;
            background-color: #3598dc;
            border-color: #3598dc;
            width: 100%;
            white-space: normal;
        }

        #my_multi_select1-error{
            color: red;
        }


    </style>



<div class="col-sm-9 evento-conteudo" id="evento-cadastro-prova">

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> Prova</span>
                <span class="caption-helper">Formulário de cadastro de prova</span>
            </div>
        </div>
        <div class="portlet-body">


            <form class="form form-horizontal" role="form" id="form-prova" name="form-prova" action="{base_url}evento/BackOffice/CadastroProva/Processar" method="post">

                <div class="form-body" style="margin:0px;padding:0px;">

                    <!-- VALORES E LIMITES -->
                    <div class="portlet light bordered">

                        <div class="portlet-title">
                            <div class="caption font-yellow-crusta">
                                <i class="icon-share font-yellow-crusta"></i>
                                <span class="caption-subject bold uppercase"> INFORMAÇÕES GERAIS</span>
                                <!-- <span class="caption-helper"></span> -->
                            </div>
                        </div>

                        <div class="portlet-body">


                            <!-- SERIE -->
                            <div class="form-group">
                                <label class="control-label col-md-4">
                                    Série
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <select class="form-control" name="serie" id="serie">
                                        <option value="">- Selecione uma Série do Evento -</option>
                                        <?php foreach ($DatasetSerie as $key => $value): ?>
                                        <option value="<?php echo $value->ers_id; ?>">
                                            <?php echo $value->ers_nome; ?>
                                        </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>


                            <!-- NOME PROVA -->
                            <div class="form-group">
                                <label class="control-label col-md-4">Nome da Prova <span class="required" aria-required="true">
                                * </span>
                                </label>
                                <div class="col-md-8">
                                    <textarea name="nome-prova" class="form-control" cols="40" rows="2" style="width:100%;"></textarea>
                                </div>
                            </div>


                            <!-- NOME TROFEU -->
                            <div class="form-group">
                                <label class="control-label col-md-4">Nome do Troféu
                                </label>
                                <div class="col-md-8">
                                    <textarea name="nome-trofeu" class="form-control" cols="40" rows="2" style="width:100%;"></textarea>
                                </div>
                            </div>


                            <!-- Dia da Prova -->
                            <div class="form-group">
                                <label class="control-label col-md-4">Dia da Prova <span class="required" aria-required="true">
                                * </span>
                                </label>
                                <div class="col-md-4">
                                    <select name="dia" class="form-control">
                                        <option value="">Selecione -</option>
                                        <?php
                                            for ($i=0; $i < $QuantidadeDiasEvento; $i++):
                                            $QuantidadeDias     =   '+' . $i . ' day';
                                            $date = strtotime($QuantidadeDias, strtotime($EventoInicio));
                                        ?>
                                        <option value="<?php echo date("Y-m-d", $date); ?>">
                                            <?php
                                                echo $this->my_data->ConverterData( date("Y-m-d", $date), 'ISO', 'PT-BR') ;
                                                echo ' - ' . $this->my_data->diasemana( date("Y-m-d", $date) );
                                             ?>
                                        </option>
                                        <?php endfor; ?>
                                    </select>
                                </div>

                                <div class="col-md-4">
                                    <div class="input-group">
                                        <input type="text" class="form-control timepicker timepicker-24 time" name="hora" placeholder="Hora da Prova HH:MM:SS">
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-clock-o"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>

                            </div>

                            <!-- NUMERO DA PROVA -->
                            <div class="form-group">
                                <label class="control-label col-md-4">
                                    Número da Prova
                                     <span class="required" aria-required="true">
                                * </span>
                                </label>
                                <div class="col-md-4">
                                    <select class="form-control" name="numero-prova" id="numero-prova">
                                        <option value="">- Selecione -</option>
                                        <?php
                                            for ($i=1; $i < $QuantidadeDeProva + 1 ; $i++):
                                                if(in_array($i, $DatasetProvasCadastradas))
                                                    continue;
                                        ?>
                                        <option value="<?php echo $i; ?>">
                                            <?php echo $i; ?>
                                        </option>
                                        <?php endfor; ?>
                                    </select>
                                </div>
                            </div>


                            <!-- TIPO DA PISTA -->
                            <div class="form-group">
                                <label class="control-label col-md-4">
                                    Tipo da Pista
                                     <span class="required" aria-required="true">
                                * </span>
                                </label>
                                <div class="col-md-8">
                                    <select class="form-control" name="tipo-pista" id="tipo-pista">
                                        <option value="">- Selecione um Tipo de Pista -</option>
                                        <?php foreach ($DatasetTipoPista as $key => $value): ?>
                                        <option value="<?php echo $value->spp_id; ?>">
                                            <?php echo $value->spp_pista; ?>
                                        </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>


                            <!-- CARACTERISTICA -->
                            <div class="form-group">
                                <label class="control-label col-md-4">
                                    Característica
                                     <span class="required" aria-required="true">
                                * </span>
                                </label>
                                <div class="col-md-8">
                                    <select class="form-control" name="caracteristica" id="caracteristica">
                                        <option value="">- Selecione a Característica de Prova -</option>
                                        <?php foreach ($DatasetCaracteristica as $key => $value): ?>
                                        <option value="<?php echo $value->spc_id; ?>">
                                            <?php echo $value->spc_caracteristica . ' - ' . $value->spc_regulamento; ?>
                                        </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>


                            <!-- TIPO DE SORTEIO -->
                            <div class="form-group">
                                <label class="control-label col-md-4">
                                    Tipo de Sorteio
                                     <span class="required" aria-required="true">
                                * </span>
                                </label>
                                <div class="col-md-8">
                                    <select class="form-control" name="tipo-sorteio" id="tipo-sorteio">
                                        <option value="">- Selecione o Tipo de Sorteio -</option>
                                        <?php foreach ($DatasetTipoSorteio as $key => $value): ?>
                                        <option value="<?php echo $value->sps_id; ?>">
                                            <?php echo $value->sps_sorteio; ?>
                                        </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>



                        </div>
                        <!-- /porlet-body (site ) -->

                    </div>
                    <!-- portlet(site) -->



                    <!-- VALORES E LIMITES -->
                    <div class="portlet light bordered">

                        <div class="portlet-title">
                            <div class="caption font-yellow-crusta">
                                <i class="icon-share font-yellow-crusta"></i>
                                <span class="caption-subject bold uppercase"> VALORES E LIMITES</span>
                                <!-- <span class="caption-helper"></span> -->
                            </div>
                        </div>

                        <div class="portlet-body">


                            <!-- VALOR DA PROVA -->
                            <div class="form-group">
                                <label class="control-label col-md-4">
                                    Valor da Prova
                                     <span class="required" aria-required="true">
                                * </span>
                                </label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <span class="input-group-addon"> R$ </span>
                                        <input type="text" class="form-control text-center money" name="valor-prova" placeholder="Digite um valor">
                                    </div>
                                </div>
                            </div>


                            <!-- VALOR DA PROVA PROMOCIONAL -->
                            <div class="form-group">
                                <label class="control-label col-md-4">
                                    Valor da Prova ( Promocional )
                                     <span class="required" aria-required="true">
                                * </span>
                                </label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <span class="input-group-addon"> R$ </span>
                                        <input type="text" class="form-control text-center money" name="valor-prova-promocional" placeholder="Digite um valor">
                                    </div>
                                </div>
                            </div>


                            <!-- LIMITE INSCRICAO PROVA -->
                            <div class="form-group">
                                <label class="control-label col-md-4">
                                    Limite de Inscrições da Prova
                                     <span class="required" aria-required="true">
                                * </span>
                                </label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input type="number" name="limite-inscricao-prova" class="form-control">
                                    </div>
                                </div>
                            </div>



                            <!-- LIMITE INSCRICAO PROVA -->
                            <div class="form-group">
                                <label class="control-label col-md-4">
                                    Limite de Inscrições por Atleta
                                     <span class="required" aria-required="true">
                                * </span>
                                </label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input type="number" name="limite-inscricao-atleta" class="form-control">
                                    </div>
                                </div>
                            </div>


                        </div>
                        <!-- /porlet-body (site ) -->

                    </div>
                    <!-- portlet(site) -->




                    <!-- SALTO -->
                    <div class="portlet light bordered">

                        <div class="portlet-title">
                            <div class="caption font-yellow-crusta">
                                <i class="icon-share font-yellow-crusta"></i>
                                <span class="caption-subject bold uppercase"> SALTO</span>
                                <!-- <span class="caption-helper"></span> -->
                            </div>
                        </div>

                        <div class="portlet-body">

                            <!-- VELOCIDADE -->
                            <div class="form-group">
                                <label class="control-label col-md-4">
                                    Velocidade
                                </label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input type="text" class="form-control text-center" name="velocidade" placeholder="">
                                        <span class="input-group-addon"> m/min </span>
                                    </div>
                                </div>
                            </div>

                            <!-- OBSTACULO -->
                            <div class="form-group">
                                <label class="control-label col-md-4">
                                    Obstáculo
                                </label>
                                <div class="col-md-8">

                                    <div class="input-group col-sm-4" style="float: left;margin-right: 10px;">
                                        <input type="text" class="form-control text-center" name="obstaculo-altura" placeholder="Altura">
                                        <span class="input-group-addon"> metros </span>
                                    </div>

                                    <div class="input-group col-sm-4" style="float: left;margin-right: 10px;">
                                        <input type="text" class="form-control text-center" name="obstaculo-largura" placeholder="Largura">
                                        <span class="input-group-addon"> metros </span>
                                    </div>

                                </div>
                            </div>


                        </div>
                        <!-- /porlet-body (site ) -->

                    </div>
                    <!-- portlet(site) -->




                    <!-- PROVA DEPENDENTE -->
                    <div class="portlet light bordered">

                        <div class="portlet-title">
                            <div class="caption font-yellow-crusta">
                                <i class="icon-share font-yellow-crusta"></i>
                                <span class="caption-subject bold uppercase"> PROVA DEPENDENTE</span>
                                <!-- <span class="caption-helper"></span> -->
                            </div>
                        </div>

                        <div class="portlet-body">

                            <!-- FLAG -->
                            <div class="form-group">
                                <label class="control-label col-md-4">
                                    Prova Dependente
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <div class="form-control" style="border:none;">

                                        <input type="radio" name="prova-dependente" value="1" id="prova-dependente-sim">
                                        <label for="prova-dependente-sim" style="margin-right: 15px;">SIM</label>

                                        <input type="radio" name="prova-dependente" value="2" id="prova-dependente-nao">
                                        <label for="prova-dependente-nao">NÃO</label>

                                    </div>
                                    <!-- /input-group -->

                                    <div id="prova-dependente-error"></div>

                                </div>
                            </div>
                            <!-- fim prova-dependente -->


                        </div>
                        <!-- /porlet-body (site ) -->

                    </div>
                    <!-- portlet(site) -->


                </div>
                <!-- /form-body -->

                <div class="form-actions">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-3">
                        <button type="submit" class="btn green">Cadastrar Prova</button>
                    </div>
                </div>
                <!-- /form-actions -->


                <!-- HIDDEN -->
                <input type="hidden" name="evento-id" value="{EventoId}">

            </form>

        </div>
        <!-- /portlet-body -->

    </div>
    <!-- /portlet -->

</div>
<!-- /evento-cadastro-serie -->
