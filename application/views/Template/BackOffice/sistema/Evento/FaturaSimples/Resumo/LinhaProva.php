

    <?php foreach ($DatasetLinhaProvaAvulsa as $key => $value): ?>
    <tr>

        <td class="text-left">
        	 <b>
                <?php echo $value->srp_nome . ' - ' . $value->srp_obstaculo_altura ?>M
            </b>
            <br>
        	   <small class="font-grey-cascade">
                <?php echo $value->srp_nome_trofeu; ?>
            </small>
        </td>


        <td class="text-left">

            <?php if($PoliticaPreco == '1' && !is_null($value->fri_valor_promocional)): ?>
                <small style="text-decoration: line-through;" class="font-grey-cascade">
                R$ <?php echo $this->my_moeda->InserirPontuacao($value->fri_valor); ?>
                </small>
                R$ <?php echo $this->my_moeda->InserirPontuacao($value->fri_valor_promocional); ?>
            <?php endif; ?>

            <?php if($PoliticaPreco == '2' || ( $PoliticaPreco=='1' && is_null($value->fri_valor_promocional) )  ): ?>
            R$ <?php echo $this->my_moeda->InserirPontuacao($value->fri_valor); ?>
            <?php endif; ?>

        </td>

        <td>
          <a href="#" class="btn btn-circle btn-danger btn-sm" style="border-radius: 28px !important;font-weight: 400 !important;">
              <i class="fa fa-trash" aria-hidden="true"></i>
              Deletar Prova
          </a>
        </td>

    </tr>
    <?php endforeach; ?>
