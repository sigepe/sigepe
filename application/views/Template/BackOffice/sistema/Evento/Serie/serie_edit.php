



<div class="col-sm-9 evento-conteudo" id="evento-cadastro-serie">

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> Série</span>
                <span class="caption-helper">Formulário de cadastro de série</span>
            </div>
        </div>
        <div class="portlet-body">


            <form class="form form-horizontal" role="form" id="form-serie" name="form-serie" method="post">

                <div class="form-body">

                    <!-- MODALIDADE -->
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            Altura do Obstáculo
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select class="form-control" name="altura" id="altura">
                                <option value="">- Selecione uma Altura -</option>
                                <?php foreach ($datasetAlturas as $key => $value): ?>
                                <option 
                                <?php echo ($value->evs_id == $serieEditar[0]->fk_evs_id) ? "selected" : "" ?>
                                    value="<?php echo $value->evs_id; ?>"><?php echo $value->evs_altura;
                                echo ($value->fk_evm_id == 1) ? "M" : ""; ?></option>
                                <?php endforeach; ?>
                            </select>      
                        </div>
                    </div>


                    <!-- NOME EVENTO -->
                    <div class="form-group">
                        <label class="control-label col-md-3">Nome da Série <span class="required" aria-required="true">
                                * </span>
                        </label>
                        <div class="col-md-9">
                            <input name="nome-serie" class="form-control" type="text"
                            value="<?php echo $serieEditar[0]->ers_nome; ?>" style="width:100%;">
                        </div>
                    </div>


                    <hr>


                    <!-- VALOR ATE INSCRICAO DEFINITIVA -->
                    <div class="form-group">
                        <label class="control-label col-md-3">Valor ATÉ Inscrição Definitiva <span class="required" aria-required="true">
                                * </span>
                        </label>
                        <div class="col-md-3">
                            <div class="input-group">
                                <span class="input-group-addon"> R$ </span>
                                <input type="text" class="form-control text-center money"
                                value="<?php echo $serieEditar[0]->ers_valor_ate_inscricao_definitiva; ?>" name="valor-ate-inscricao-definitiva" placeholder="Digite um valor">
                            </div>
                        </div>
                    </div>


                    <!-- VALOR APÓS INSCRICAO DEFINITIVA -->
                    <div class="form-group">
                        <label class="control-label col-md-3">Valor APÓS Inscrição Definitiva <span class="required" aria-required="true">
                                * </span>
                        </label>
                        <div class="col-md-3">
                            <div class="input-group">
                                <span class="input-group-addon"> R$ </span>
                                <input type="text" class="form-control text-center money"
                                value="<?php echo $serieEditar[0]->ers_valor_apos_inscricao_definitiva; ?>"name="valor-apos-inscricao-definitiva" placeholder="Digite um valor">
                            </div>
                        </div>
                    </div>




                    <!-- CATEGORIAS -->
                    <div class="portlet light bordered">

                        <div class="portlet-title">
                            <div class="caption font-yellow-crusta">
                                <i class="icon-share font-yellow-crusta"></i>
                                <span class="caption-subject bold uppercase"> Categorias</span>
                            </div>
                        </div>

                        <div class="portlet-body">
                            <div class="form-group">
                                <label class="control-label col-md-3">Indique quais são as categorias que fazem parte da série <span class="required" aria-required="true">
                                        * </span>
                                </label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <select multiple="multiple" class="multi-select" id="my_multi_select1" name="serie-categoria">
                                            <?php foreach ($categoriasDaSerie as $key => $value): ?>
                                                <option <?php echo ($value->categoriaContidaSerie > 0) ? "selected" : ""; ?> value="<?php echo $value->evc_id; ?>"><?php echo $value->evc_sigla . ' - ' . $value->evc_categoria; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /porlet-body (site ) -->

                        <div id="serie-categoria-error"></div>

                    </div>
                    <!-- portlet(site) -->


                </div>
                <!-- /form-body -->

                <div class="form-actions">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-3">
                        <button type="submit" class="btn green">Salvar Alterações</button>
                    </div>
                </div>
                <!-- /form-actions -->


                <!-- HIDDEN -->
                <input type="hidden" name="evento-id" value="{EventoId}">
                <input type="hidden" name="id" value="<?php echo $serieEditar[0]->ers_id; ?>">

            </form>

        </div>
        <!-- /portlet-body -->

    </div>
    <!-- /portlet -->

</div>
<!-- /evento-cadastro-serie -->



