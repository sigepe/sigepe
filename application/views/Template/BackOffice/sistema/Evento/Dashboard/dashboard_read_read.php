<style type="text/css">
    .h4, .h5, .h6, h4, h5, h6 {
        margin-top: 0px;
        margin-bottom: 5px;
    }
    hr, p {
        margin: 10px 0;
    }
    .portlet.light.bordered {
        border-bottom: 3px solid rgba(204, 204, 204, 0.45) !important;
    }
    .td-show {
        margin: 0px;
    }
    .td-edit {
        margin: 0px;
    }
</style>

<div class="col-sm-9" id="evento-conteudo">



    <div class="row">

        <!-- ATLETA -->
        <div class="col-sm-4">
            <div class="portlet light bordered">
                <div class="portlet-body">
                    <div class="dashboard-stat2 ">
                        <div class="display">
                            <div class="number">
                                <h3 class="font-green-sharp">
                                    <span data-counter="counterup" data-value="0">0</span>
                                </h3>
                                <small>ATLETA</small>
                            </div>
                            <div class="icon">
                                <i class="icon-pie-chart"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- ANIMAL -->
        <div class="col-sm-4">
            <div class="portlet light bordered">
                <div class="portlet-body">
                    <div class="dashboard-stat2 ">
                        <div class="display">
                            <div class="number">
                                <h3 class="font-green-sharp">
                                    <span data-counter="counterup" data-value="0">0</span>
                                </h3>
                                <small>ANIMAL</small>
                            </div>
                            <div class="icon">
                                <i class="icon-pie-chart"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- INSCRICAO -->
        <div class="col-sm-4">
            <div class="portlet light bordered">
                <div class="portlet-body">
                    <div class="dashboard-stat2 ">
                        <div class="display">
                            <div class="number">
                                <h3 class="font-green-sharp">
                                    <span data-counter="counterup" data-value="0">0</span>
                                </h3>
                                <small>INSCRIÇÕES</small>
                            </div>
                            <div class="icon">
                                <i class="icon-pie-chart"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>




    <div class="row">
        <form class="form form-horizontal form-evento" role="form" id="form-evento" name="form-evento">
        <div class="col-sm-12">
            <div class="portlet light bordered">

                <div class="portlet-title">
                    <div class="caption font-green-sharp">
                        <i class="icon-speech font-green-sharp"></i>
                        <span class="caption-subject bold uppercase"> INFORMAÇÕES GERAIS</span>
                        <span class="caption-helper">Resumo do Evento</span>
                    </div>
                    <div class="actions">
                        <div class="td-show">
                            <a href="#" class="btn btn-circle btn-editar btn-primary">
                                <i class="fa fa-pencil"></i> Editar </a>
                        </div>
                        <div class="td-edit">
                            <button type="submit" class="btn btn-default btn-circle btn-salvar"><span class="glyphicon glyphicon-edit"></span> Salvar</button>
                            <a href="#" class="btn btn-circle btn-cancelar btn-danger"><i class="fa fa-pencil"></i> Cancelar </a>
                        </div>
                    </div>
                </div>

                <div class="portlet-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-scrollable table-scrollable-borderless">                           
                                <table class="table table-hover table-light">
                                    <tbody>
                                        <tr>
                                            <td class="text-right bold">Nome do Evento</td>
                                            <td>
                                                <p class="td-show"><?php echo $evento[0]->eve_nome; ?></p>
                                                <p class="td-edit">
                                                    <textarea name="eve-nome-evento" class="wysihtml5 form-control" rows="6" data-error-container="#editor1_error"><?php echo $evento[0]->eve_nome; ?></textarea>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Status</td>
                                            <td><div class="td-show">
                                                    <?php echo (!is_null($evento[0]->fk_sta_id)) ? $evento[0]->sta_status : ""; ?>
                                                </div>
                                                <div class="td-edit">
                                                    <select class="form-control" name="eve-status-evento">
                                                        <?php foreach ($arrStatus as $key => $sta) { ?>
                                                            <option <?php echo ($evento[0]->fk_sta_id == $sta->sta_id) ? "selected" : "" ; ?>
                                                            value="<?php echo $sta->sta_id; ?>">
                                                                <?php echo str_replace("[ Evento ]", "[$sta->sta_id] - ", $sta->sta_status); ?>
                                                            </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Slug</td>
                                            <td>
                                                <p class="td-show"><?php echo $evento[0]->eve_slug; ?></p>
                                                <p class="td-edit">
                                                    <input type="text" disabled value="<?php echo $evento[0]->eve_slug; ?>" class="form-control" name="eve-slug">
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Modalidade</td>
                                            <td>
                                                <p class="td-show"><?php echo $evento[0]->evm_modalidade; ?></p>
                                                <p class="td-edit">
                                                    <?php echo $evento[0]->evm_modalidade; ?><small><b> - (Não editável)</b></small>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Tipo</td>
                                            <td>
                                                <p class="td-show">
                                                <?php
                                                foreach ($arrTipoEvento as $key => $tipoEvento){
                                                    if ($tipoEvento->tiposDoEvento > 0){
                                                        echo "<b>$tipoEvento->evt_sigla</b> - $tipoEvento->evt_tipo<br>";
                                                    }
                                                }
                                                ?>

                                                </p>
                                                <p class="td-edit">
                                                    <select multiple="multiple" class="multi-select form-control" id="my_multi_select1" name="eve-tipo-evento">
                                                        <?php
                                                        foreach ($arrTipoEvento as $key => $tipoEvento){ ?>
                                                            <option <?php echo ($tipoEvento->tiposDoEvento > 0) ? "selected" : ""; ?>
                                                                value="<?php echo $tipoEvento->evt_id; ?>">
                                                                <?php echo "$tipoEvento->evt_sigla - $tipoEvento->evt_tipo"; ?>      
                                                            </option>
                                                        <?php } ?>
                                                    </select>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Período</td>
                                            <td><div class="td-show">
                                                    <?php echo $QuantidadeDiasEvento; ?> dia<?php echo ($QuantidadeDiasEvento > 1) ? 's' : ''; ?> de evento <br>
                                                    Data início: <?php echo $this->my_data->ConverterData($evento[0]->eve_data_inicio, 'ISO', 'PT-BR'); ?> - <?php echo $this->my_data->diasemana($evento[0]->eve_data_inicio); ?> <br>
                                                    Data fim: <?php echo $this->my_data->ConverterData($evento[0]->eve_data_fim, 'ISO', 'PT-BR'); ?> - <?php echo $this->my_data->diasemana($evento[0]->eve_data_fim); ?>
                                                </div>
                                                <div class="td-edit">
                                                    <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="dd/mm/yyyy">
                                                        <input type="text" value="<?php echo $this->my_data->ConverterData($evento[0]->eve_data_inicio, 'ISO', 'PT-BR'); ?>" class="form-control" name="eve-data-inicio">
                                                        <span class="input-group-addon"> até </span>
                                                        <input type="text" value="<?php echo $this->my_data->ConverterData($evento[0]->eve_data_fim, 'ISO', 'PT-BR'); ?>" class="form-control" name="eve-data-fim">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Qtde. provas</td>
                                            <td><p class="td-show">
                                                    <?php echo $evento[0]->eve_quantidade_prova; ?>
                                                </p>
                                                <p class="td-edit">
                                                    <input type="number" value="<?php echo $evento[0]->eve_quantidade_prova; ?>" class="form-control" name="eve-quantidade-prova">
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Data limite <br> sem acréscimo</td>
                                            <td><p class="td-show">
                                                    <?php echo $this->my_data->ConverterData($evento[0]->eve_data_limite_sem_acrescimo, 'ISO', 'PT-BR'); ?>
                                                </p>
                                                <p class="td-edit">
                                                    <input type="text" value="<?php echo $this->my_data->ConverterData($evento[0]->eve_data_limite_sem_acrescimo, 'ISO', 'PT-BR'); ?>" class="form-control" name="eve-data-limite-sem-acrescimo">
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Local</td>
                                            <td><p class="td-show">
                                                    <?php echo $evento[0]->fk_pes_id; ?>
                                                </p>
                                                <p class="td-edit">
                                                    <select class="form-control" name="eve-local">
                                                        <?php
                                                        foreach ($arrLocais as $key => $local){ ?>
                                                            <option <?php echo ($local->pes_id == $evento[0]->fk_pes_id) ? "selected" : ""; ?>
                                                                value="<?php echo $local->pes_id; ?>">
                                                                <?php echo "$local->pes_nome_razao_social"; ?>
                                                            </option>
                                                        <?php } ?>
                                                    </select>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Ativo no Site</td>
                                            <td><div class="td-show">
                                                    <?php echo (!is_null($evento[0]->flag_site)) ? 'Sim  ( está sendo exibido no site/sistema )' : 'Não'; ?>
                                                </div>
                                                <div class="td-edit">
                                                    <select class="form-control" name="eve-ativo-no-site">
                                                        <!-- <option>- Selecione -</option> -->
                                                        <option <?php echo ($evento[0]->flag_site == "0") ? "selected" : "" ; ?> value="0">Não</option>
                                                        <option <?php echo ($evento[0]->flag_site == "1") ? "selected" : "" ; ?> value="1">Sim</option>
                                                    </select>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Abertura das Inscrições</td>
                                            <td><div class="td-show">
                                                    Inscrições Abertas: <?php echo $this->my_data->ConverterData($evento[0]->eve_site_inscricao_inicio, 'ISO', 'PT-BR'); ?> - <?php echo $this->my_data->diasemana($evento[0]->eve_site_inscricao_inicio); ?> <br>
                                                </div>
                                                <div class="td-edit">
                                                    <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="dd/mm/yyyy">
                                                        <input type="text" value="<?php echo $this->my_data->ConverterData($evento[0]->eve_site_inscricao_inicio, 'ISO', 'PT-BR'); ?>" class="form-control" name="eve-site-inscricao-inicio">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Encerramento das Inscrições</td>
                                            <td><div class="td-show">
                                                    Inscrições Encerram: <?php echo $this->my_data->ConverterData($evento[0]->eve_site_inscricao_fim, 'ISO', 'PT-BR'); ?> - <?php echo $this->my_data->diasemana($evento[0]->eve_site_inscricao_fim); ?> 
                                                    <br><?php echo DateDifferences($evento[0]->eve_site_inscricao_fim, $evento[0]->eve_site_inscricao_inicio, 'd') + 1; ?> dias de inscrições em aberto
                                                </div>
                                                <div class="td-edit">
                                                    <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="dd/mm/yyyy">
                                                        <input type="text" value="<?php echo $this->my_data->ConverterData($evento[0]->eve_site_inscricao_fim, 'ISO', 'PT-BR'); ?>" class="form-control" name="eve-site-inscricao-fim">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Venda de Inscrições por Tipo</td>
                                            <td><p class="td-show">
                                                    <?php echo $evento[0]->evv_venda; ?>
                                                </p>
                                                <p class="td-edit">
                                                    <select class="form-control" name="eve-tipo-venda-inscricoes">
                                                        <?php foreach ($arrTipoVenda as $key => $tipoVenda) { ?>
                                                            <option <?php echo ($tipoVenda->evv_id == $evento[0]->fk_evv_id) ? "selected" : "" ; ?>
                                                                value="<?php echo $tipoVenda->evv_id; ?>">
                                                                <?php echo $tipoVenda->evv_venda ?>
                                                            </option>
                                                        <?php }?>
                                                    </select>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Baia Ativo</td>
                                            <td><p class="td-show">
                                                    <?php echo (!is_null($evento[0]->flag_baia)) ? 'Sim' : 'Não'; ?>
                                                </p>
                                                <p class="td-edit">
                                                    <select class="form-control" name="eve-baia-ativo">
                                                        <!-- <option>- Selecione -</option> -->
                                                        <option <?php echo ($evento[0]->flag_baia == "0") ? "selected" : "" ; ?> value="0">Não</option>
                                                        <option <?php echo ($evento[0]->flag_baia == "1") ? "selected" : "" ; ?> value="1">Sim</option>
                                                    </select>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Quarto de Sela Ativo</td>
                                            <td><p class="td-show">
                                                    <?php echo (!is_null($evento[0]->flag_quarto)) ? 'Sim' : 'Não'; ?>
                                                </p>
                                                <p class="td-edit">
                                                    <select class="form-control" name="eve-quarto-sela">
                                                        <!-- <option>- Selecione -</option> -->
                                                        <option <?php echo ($evento[0]->flag_quarto == "0") ? "selected" : "" ; ?> value="0">Não</option>
                                                        <option <?php echo ($evento[0]->flag_quarto == "1") ? "selected" : "" ; ?> value="1">Sim</option>
                                                    </select>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Desenhador de Percurso</td>
                                            <td><p class="td-show">
                                                    <?php
                                                    foreach ($arrDesenhadorPercurso as $desenhador) {
                                                        echo ($desenhador->desenhadorDoEvento == 1) ? $desenhador->pes_nome_razao_social . "<br>" : "";
                                                    }
                                                    ?>
                                                </p>
                                                <p class="td-edit">
                                                    <select multiple="multiple" class="multi-select form-control" id="my_multi_select3" name="eve-desenhadores-percurso">
                                                        <?php
                                                        foreach ($arrDesenhadorPercurso as $key => $desenhador){ ?>
                                                            <option <?php echo ($desenhador->desenhadorDoEvento > 0) ? "selected" : ""; ?>
                                                                value="<?php echo $desenhador->pes_id; ?>">
                                                                <?php echo "$desenhador->pes_nome_razao_social"; ?>
                                                            </option>
                                                        <?php } ?>
                                                    </select>
                                                </p>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="text-right bold">Descrição</td>
                                            <td>
                                                <div class="td-show">
                                                    <?php echo $evento[0]->eve_descricao; ?>
                                                </div>
                                                <p class="td-edit">
                                                    <textarea name="eve-descricao" class="wysihtml5 form-control" rows="6" data-error-container="#editor1_error"><?php echo $evento[0]->eve_descricao; ?></textarea>
                                                <div id="editor1_error"> </div>
                                                </p>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="text-right bold">Logo</td>
                                            <td>
                                                <div class="td-show">
                                                    <img src="{base_url}assets/sigepe/Global/Images/Evento/<?php echo $evento[0]->eve_site_logotipo; ?>"
                                                        onerror="this.onerror=null;this.src='{base_url}assets/sigepe/Global/Images/Evento/no-photo.png';" 
                                                        style="width:130px;border-radius: 10% !important;border: 2px solid #fff; box-shadow: 1px 1px 1px #999;float: left;width:200px; margin-right: 17px;">
                                                </div>
                                                <div class="td-edit">
                                                    <label>Select Image</label>
                                                    <input type="file" class="form-control" name="file" id="file" />
                                                    <br />
                                                    <span id="uploaded_image"></span>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="text-right bold">Ações</td>
                                            <td>
                                                <div class="td-show">
                                                    <a href="#" class="btn btn-primary btn-editar">Editar <span class="glyphicon glyphicon-edit"></span></a>
                                                </div>
                                                <div class="td-edit">
                                                    <input type="hidden" name="id" value="<?php echo $evento[0]->eve_id; ?>" class="" />
                                                    <input type="hidden" name="eve-controle" value="<?php echo $evento[0]->eve_controle; ?>" class="" />
                                                    <button type="submit" class="btn btn-default btn-salvar">Salvar <span class="glyphicon glyphicon-edit"></span></button>
                                                    <a href="#" class="btn btn-danger btn-cancelar">Cancelar<span class="fa fa-close"></span></a>
                                                </div>
                                            </td>
                                        </tr>



                                        <!-- <tr>
                                            <td class="text-right bold">Descrição</td>
                                            <td><p class="td-show">
                                        <?php echo $evento[0]->eve_descricao; ?>
                                                </p>
                                                <p class="td-edit">
                                        <?php
                                        ?></p>
                                            </td>
                                        </tr> -->


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>



    <div class="row">
        <div class="col-sm-6">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green-sharp">
                        <i class="icon-speech font-green-sharp"></i>
                        <span class="caption-subject bold uppercase"> BAIAS</span>
                    </div>
                </div>
                <div class="portlet-body">

                    <div class="row">
                        <div class="col-sm-12">
                            <h5 class="bold">Permitir venda de Baias</h5>
                            <small>
                                <?php echo (!is_null($evento[0]->flag_baia)) ? 'Sim' : 'Não'; ?>
                            </small>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-sm-6">
                            <h5 class="bold">Estoque Total:</h5>
                            <small>
                                <?php echo (!is_null($evento[0]->eve_baia_quantidade)) ? $evento[0]->eve_baia_quantidade . ' Unidades' : 'N/I'; ?>
                            </small>
                        </div>
                        <div class="col-sm-6">
                            <h5 class="bold">Baias Disponíveis</h5>
                            -
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-sm-6">
                            <h5 class="bold">Valor <b>SEM</b> Acréscimo:</h5>
                            <small>
                                <?php
                                echo (!is_null($evento[0]->eve_baia_valor_sem_acrescimo)) ? 'R$ ' . $this->my_moeda->InserirPontuacao($evento[0]->eve_baia_valor_sem_acrescimo) : 'N/I';
                                ?>
                            </small>
                        </div>
                        <div class="col-sm-6">
                            <h5 class="bold">Valor <b>COM</b> Acréscimo</h5>
                            <?php
                            echo (!is_null($evento[0]->eve_baia_valor_com_acrescimo)) ? 'R$ ' . $this->my_moeda->InserirPontuacao($evento[0]->eve_baia_valor_com_acrescimo) : 'N/I';
                            ?>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-sm-12">
                            <h5 class="bold">Bloqueio:</h5>

                            <?php if (!is_null($evento[0]->eve_baia_bloqueio)): ?>
                                <small>
                                    <b>Sim</b> -
                                    Automaticamente a venda de baias é suspensa caso esgote o estoque de baias disponíveis.
                                </small>
                            <?php endif; ?>

                            <?php if (is_null($evento[0]->eve_baia_bloqueio)): ?>
                                <small>
                                    <b>Não</b> -
                                    Caso exceda a quantidade de estoque disponíveis para venda ainda sim será permitido usuários adquirerem baias.
                                </small>
                            <?php endif; ?>

                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-sm-12">
                            <a href="#" class="btn btn-primary" style="display: block;">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                Gerenciar Baias
                            </a>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <div class="col-sm-6">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green-sharp">
                        <i class="icon-speech font-green-sharp"></i>
                        <span class="caption-subject bold uppercase"> QUARTOS DE SELA</span>
                    </div>
                </div>
                <div class="portlet-body">

                    <div class="row">
                        <div class="col-sm-12">
                            <h5 class="bold">Permitir venda de Quartos de Sela</h5>
                            <small>
                                <?php echo (!is_null($evento[0]->flag_quarto)) ? 'Sim' : 'Não'; ?>
                            </small>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-sm-6">
                            <h5 class="bold">Estoque Total:</h5>
                            <small>
                                <?php echo (!is_null($evento[0]->eve_quarto_quantidade)) ? $evento[0]->eve_quarto_quantidade . ' Unidades' : 'N/I'; ?>
                            </small>
                        </div>
                        <div class="col-sm-6">
                            <h5 class="bold">Q.S. Disponíveis</h5>
                            -
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-sm-6">
                            <h5 class="bold">Valor <b>SEM</b> Acréscimo:</h5>
                            <small>
                                <?php
                                echo (!is_null($evento[0]->eve_quarto_valor_sem_acrescimo)) ? 'R$ ' . $this->my_moeda->InserirPontuacao($evento[0]->eve_quarto_valor_sem_acrescimo) : 'N/I';
                                ?>
                            </small>
                        </div>
                        <div class="col-sm-6">
                            <h5 class="bold">Valor <b>COM</b> Acréscimo</h5>
                            <?php
                            echo (!is_null($evento[0]->eve_quarto_valor_com_acrescimo)) ? 'R$ ' . $this->my_moeda->InserirPontuacao($evento[0]->eve_quarto_valor_com_acrescimo) : 'N/I';
                            ?>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-sm-12">
                            <h5 class="bold">Bloqueio:</h5>

                            <?php if (!is_null($evento[0]->eve_quarto_bloqueio)): ?>
                                <small>
                                    <b>Sim</b> -
                                    Automaticamente a venda de baias é suspensa caso esgote o estoque de baias disponíveis.
                                </small>
                            <?php endif; ?>

                            <?php if (is_null($evento[0]->eve_quarto_bloqueio)): ?>
                                <small>
                                    <b>Não</b> -
                                    Caso exceda a quantidade de estoque disponíveis para venda ainda sim será permitido usuários adquirerem quartos de sela.
                                </small>
                            <?php endif; ?>

                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-sm-12">
                            <a href="#" class="btn btn-primary" style="display: block;">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                Gerenciar Quartos de Sela
                            </a>
                        </div>
                    </div>

                </div>
            </div>

        </div>


    </div>


    <div class="row">
        <div class="col-sm-6">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green-sharp">
                        <i class="icon-speech font-green-sharp"></i>
                        <span class="caption-subject bold uppercase"> ÚLTIMAS INSCRIÇÕES</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="alert alert-warning" style="margin-bottom: 0px;">
                        <strong>Atenção!</strong> <br>
                        Nenhuma inscrição localizada.
                    </div>
                </div>
            </div>

        </div>

        <div class="col-sm-6">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green-sharp">
                        <i class="icon-speech font-green-sharp"></i>
                        <span class="caption-subject bold uppercase"> OFICIAIS FHBr</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="alert alert-warning" style="margin-bottom: 0px;">
                        <strong>Atenção!</strong> <br>
                        Nenhum Oficial FHBr localizado.
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>


