

    <div class="tab-pane" id="tab_<?php echo $value->srp_id; ?>_provasdependentes">

        <!-- BEGIN BORDERED TABLE PORTLET-->
        <div class="portlet light portlet-fit margin-top-0">

            <!-- FLAG -->
            <div class="form-group text-center">

                <label class="control-label text-center" style="display: block; background-color: #fff; padding: 15px; border-bottom: 2px solid #ddd; margin-bottom: 20px;">
                    Informe se a <b>Prova <?php echo ($value->srp_numero_prova < 10 ) ? '0' . $value->srp_numero_prova : $value->srp_numero_prova; ?> </b> possui provas dependentes.
                    <span class="required" aria-required="true"> * </span>
                </label>
                
                <input type="radio" name="prova-dependente-<?php echo $value->srp_numero_prova; ?>" value="1" id="prova-dependente-<?php echo $value->srp_numero_prova; ?>-sim" <?php echo (!is_null($value->flag_prova_dependente)) ? 'checked' : ''; ?> >
                <label for="prova-dependente-<?php echo $value->srp_numero_prova; ?>-sim" style="margin-right: 25px;">SIM</label>

                <input type="radio" name="prova-dependente-<?php echo $value->srp_numero_prova; ?>" value="2" id="prova-dependente-<?php echo $value->srp_numero_prova; ?>-nao" <?php echo (is_null($value->flag_prova_dependente)) ? 'checked' : ''; ?> >
                <label for="prova-dependente-<?php echo $value->srp_numero_prova; ?>-nao">NÃO</label>


            </div>
            <!-- fim prova-dependente -->

        </div>
        <!-- END BORDERED TABLE PORTLET-->


        <div class="btn-footer-actions" style="background-color: rgba(238, 238, 238, 0.41); padding: 15px; border-top: 2px solid #eee;margin-top: 25px;">
            <a href="#" class="btn btn-primary" style="display: block;">
                <i class="fa fa-floppy-o" aria-hidden="true"></i>
                Salvar Provas Dependentes da Prova <?php echo ($categoria->srp_numero_prova < 10 ) ? '0' . $categoria->srp_numero_prova : $categoria->srp_numero_prova; ?>
            </a>
        </div>

    </div>
    <!-- /tab ( provas dependentes ) -->
