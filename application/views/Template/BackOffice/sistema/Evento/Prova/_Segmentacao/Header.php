    <div class="col-sm-9 evento-conteudo" id="evento-prova">


        <div class="portlet light bordered" style="float: left;width: 100%;">
          
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <i class="icon-speech font-green-sharp"></i>
                    <span class="caption-subject bold uppercase"> PROVAS</span>
                    <span class="caption-helper">Relação das <b>provas</b> cadastradas para esse evento.</span>
                </div>
                <div class="actions">

                    <a href="{base_url}evento/CadastroProva/Formulario/{EventoId}" class="btn btn-circle display-block yellow-lemon ">
                        <i class="fa fa-plus"></i> CADASTRAR PROVA
                    </a>

                </div>
            </div>
            <!-- /porlet-title -->


            <div class="portlet-body">

                <div class="row">

