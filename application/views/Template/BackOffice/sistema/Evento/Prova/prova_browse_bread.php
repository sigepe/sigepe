<style type="text/css">

    .sigepe .tiles .tile{
        overflow: initial !important;
    }

    .tiles .tile{
        border: none !important;
    }

    .tiles .tile .tile-body{
        margin-bottom: 0 !important;
    }




/*
   .sigepe .tiles .tile:after{
        content: "1";
        background: red;
        width: 100%;
        height: 30px;   
    }
*/

    .tiles-empresas .tile{
        margin-bottom: 60px !important;
    }
    .sigepe .tiles .tile{
        width: 100% !important;
        margin-bottom: 0px;
    }
    .sigepe .tiles .tile .tile-object>.name{
        position: relative !important;
        margin-left: 0px !important;
        margin-right: 0px !important;
        text-align: center;
    }
    .sigepe .tiles{
        margin-right: 0px !important;
    }


    .btn.blue:not(.btn-outline) {
        color: #FFF;
        background-color: #3598dc;
        border-color: #3598dc;
        width: 100%;
        white-space: normal;
    }




    /* - */
    .tabbable-line>.nav-tabs>li.active{
        font-weight: bold;
    }

</style>


<div class="col-sm-9 evento-conteudo" id="evento-serie">

    




    <div class="portlet light bordered" style="float: left;width: 100%;">
      
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <i class="icon-speech font-green-sharp"></i>
                <span class="caption-subject bold uppercase"> PROVAS</span>
                <span class="caption-helper">Relação das <b>provas</b> cadastradas para esse evento.</span>
            </div>
            <div class="actions">

                <a href="{base_url}BackOffice/Evento/Prova/Cadastrar/{EventoId}" class="btn btn-circle display-block yellow-lemon ">
                    <i class="fa fa-plus"></i> CADASTRAR PROVA
                </a>

            </div>
        </div>

        <div class="portlet-body">

            <div class="row">
                <div class="col-md-2 col-sm-2 col-xs-2">
                    <ul class="nav nav-tabs tabs-left">

                        <?php
                            $Contador = 1;
                            foreach ($DatasetProva as $key => $value):
                        ?>
                        <li class="<?php echo ($Contador == '1') ? 'active' : ''; ?>">
                            <a href="#tab_<?php echo $value->srp_id; ?>" data-toggle="tab">
                                Pr. <?php echo ($value->srp_numero_prova < 10 ) ? '0' . $value->srp_numero_prova : $value->srp_numero_prova; ?>
                            </a>
                        </li>
                        <?php
                            $Contador++;
                            endforeach;
                        ?>

                    </ul>
                </div>

                <div class="col-md-10 col-sm-10 col-xs-10">

                    <div class="tab-content">

                        <?php
                            $Contador = 1;
                            foreach ($DatasetProva as $key => $value):
                        ?>
                        <div class="tab-pane <?php echo ($Contador == '1') ? 'active' : ''; ?>" id="tab_<?php echo $value->srp_id; ?>">
                                
                                <?php //echo $value->srp_nome; ?>


                            <div class="portlet-body">
                                <h3 style="margin-top: 0px;" class="bold"><?php echo nl2br($value->srp_nome); ?></h3>
                                <?php if(!is_null($value->srp_nome_trofeu)): ?><h4><?php echo nl2br($value->srp_nome_trofeu); ?></h4><?php endif; ?>

                                <hr style="margin-bottom: 10px;">

                                <div class="tabbable-line">
                                    <ul class="nav nav-tabs ">
                                        <li class="active">
                                            <a href="#tab_<?php echo $value->srp_id; ?>_informacoes" data-toggle="tab">
                                                Informações
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab_<?php echo $value->srp_id; ?>_categorias" data-toggle="tab">
                                                Categorias
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab_<?php echo $value->srp_id; ?>_desenhador" data-toggle="tab"> 
                                                Desenhador de Percurso 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab_<?php echo $value->srp_id; ?>_provasdependentes" data-toggle="tab"> 
                                                Provas Dependentes
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab_<?php echo $value->srp_id; ?>_inscricoes" data-toggle="tab"> 
                                                Inscrições
                                            </a>
                                        </li>
                                    </ul>


                                    <div class="tab-content" style="padding-top: 10px;">
                                        <div class="tab-pane active" id="tab_<?php echo $value->srp_id; ?>_informacoes">
                                            
                                            <form class="form form-horizontal form-prova-exibir" role="form">
                                                <span class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm active">
                                                    Geral
                                                </span>


                                                
                                                <!-- TIPO DA PISTA -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Nome:</label>
                                                            <div class="col-md-8">
                                                                <p class="form-control-static">
                                                                    <?php echo $value->srp_nome; ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <!-- TIPO DA PISTA -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Nome Troféu:</label>
                                                            <div class="col-md-8">
                                                                <p class="form-control-static">
                                                                    <?php echo $value->srp_nome_trofeu; ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <!-- DATA/HORA -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Data/Hora da Prova:</label>
                                                            <div class="col-md-8">
                                                                <p class="form-control-static">
                                                                    <?php echo $this->my_data->ConverterData($value->srp_dia, 'ISO', 'PT-BR'); ?>
                                                                    <br>
                                                                    <small class="font-grey-salsa "><?php echo $this->my_data->diasemana($value->srp_dia) . ' às ' . $value->srp_hora; ?> </small>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <!-- TIPO DA PISTA -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Tipo da Pista:</label>
                                                            <div class="col-md-8">
                                                                <p class="form-control-static">
                                                                    <?php echo $this->model_crud->get_rowSpecific('tb_evento_serie_prova_pista', 'spp_id', $value->fk_spp_id, 1, 'spp_pista'); ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- TIPO DE SORTEIO -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Tipo de Sorteio:</label>
                                                            <div class="col-md-8">
                                                                <p class="form-control-static">
                                                                    <?php echo $this->model_crud->get_rowSpecific('tb_evento_serie_prova_sorteio', 'sps_id', $value->fk_sps_id, 1, 'sps_sorteio'); ?> 
                                                                    <br>
                                                                    <small class="font-grey-salsa "><?php echo $this->model_crud->get_rowSpecific('tb_evento_serie_prova_sorteio', 'sps_id', $value->fk_sps_id, 1, 'sps_descricao'); ?> </small>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 

                                                <!-- STATUS DA PROVA -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Status:</label>
                                                            <div class="col-md-8">
                                                                <p class="form-control-static">
                                                                    <?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $value->fk_sta_id, 1, 'sta_status'); ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>     

                                                <!-- CARACTERISTICA -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Característica da Prova:</label>
                                                            <div class="col-md-8">
                                                                <p class="form-control-static">
                                                                    <?php echo $this->model_crud->get_rowSpecific('tb_evento_serie_prova_caracteristica', 'spc_id', $value->fk_spc_id, 1, 'spc_caracteristica'); ?> 
                                                                    <br>
                                                                    <small class="font-grey-salsa "><?php echo $this->model_crud->get_rowSpecific('tb_evento_serie_prova_caracteristica', 'spc_id', $value->fk_spc_id, 1, 'spc_regulamento'); ?> </small>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- ID DA PROVA -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 text-right form-control-static font-grey-mint">ID da Prova:</label>
                                                            <div class="col-md-8">
                                                                <p class="form-control-static">
                                                                    #<?php echo $value->srp_id; ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <hr>
                                                <span class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm active">
                                                    Valores e Limites
                                                </span>

                                                <!-- VALOR -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Valor:</label>
                                                            <div class="col-md-8">
                                                                <p class="form-control-static">
                                                                    R$ <?php echo $this->my_moeda->InserirPontuacao( $value->srp_valor ); ?> 
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- VALOR PROMOCIONAL -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Valor Promocional:</label>
                                                            <div class="col-md-8">
                                                                <p class="form-control-static">
                                                                    <?php if(!empty($value->srp_valor_promocional) || !is_null($value->srp_valor_promocional)): ?>
                                                                        R$ <?php echo $this->my_moeda->InserirPontuacao( $value->srp_valor_promocional ); ?> 
                                                                    <?php else: ?>
                                                                        -
                                                                    <?php endif; ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                               
                                                <!-- LIMITE DE INSCRICAO NA PROVA -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Limite de Inscrição na Prova:</label>
                                                            <div class="col-md-8">
                                                                <p class="form-control-static">
                                                                    <?php echo $value->srp_limite_inscricao_prova; ?> 
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- LIMITE DE INSCRICAO POR ATLETA -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Limite de Inscrição por Atleta:</label>
                                                            <div class="col-md-8">
                                                                <p class="form-control-static">
                                                                    <?php echo $value->srp_limite_inscricao_atleta; ?> 
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <hr>
                                                <span class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm active">
                                                    Salto
                                                </span>

                                                <!-- VELOCIDADE -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Velocidade:</label>
                                                            <div class="col-md-8">
                                                                <p class="form-control-static">
                                                                    <?php echo $value->srp_velocidade; ?>  m/min
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- OBSTACULO ( ALTURA ) -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Obstáculo ( altura ):</label>
                                                            <div class="col-md-8">
                                                                <p class="form-control-static">
                                                                    <?php echo $value->srp_obstaculo_altura; ?>  M
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- OBSTACULO ( LARGURA ) -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Obstáculo ( largura ):</label>
                                                            <div class="col-md-8">
                                                                <p class="form-control-static">
                                                                    <?php echo $value->srp_obstaculo_largura; ?>  M
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <hr>
                                                <span class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm active">
                                                    Auditoria
                                                </span>

                                                <!-- AUTOR -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Autor:</label>
                                                            <div class="col-md-8">
                                                                <p class="form-control-static">
                                                                    <?php echo $this->model_crud->get_rowSpecific( 'tb_pessoa', 'pes_id', $value->fk_aut_id, 1, 'pes_nome_razao_social' ); ?> <br>
                                                                    <small> ID: <?php echo $value->fk_aut_id; ?> </small>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- CRIADO EM -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Prova criada em:</label>
                                                            <div class="col-md-8">
                                                                <p class="form-control-static">
                                                                    <?php echo $this->my_data->ConverterData( $value->criado, 'ISO', 'PT-BR' ); ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- ULTIMA ALTERACAO EM -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Última alteração em:</label>
                                                            <div class="col-md-8">
                                                                <p class="form-control-static">
                                                                    <?php echo $this->my_data->ConverterData( $value->modificado, 'ISO', 'PT-BR' ); ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="btn-footer-actions" style="background-color: rgba(238, 238, 238, 0.41); padding: 30px; border-top: 2px solid #eee;margin-top: 25px;">
                                                    <a href="#" class="btn btn-primary btn-editar-prova">Editar Informações da Prova <span class="glyphicon glyphicon-edit"></span></a>
                                                </div>
                                            </form>

                                            <form class="form form-horizontal form-prova-editar" style="background: #eee" role="form" id="form-<?php echo $value->srp_id; ?>-prova-editar" name="form-<?php echo $value->srp_id; ?>-prova-editar">
                                                <input type="hidden" name="id" value="<?php echo $value->srp_id; ?>" class="" />
                                                <span class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm active">
                                                    Geral
                                                </span>

                                                <div class="row" style="">

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <!-- NOME PROVA -->
                                                            <div class="form-group">
                                                                <label class="control-label col-md-4">Nome da Prova <span class="required" aria-required="true">
                                                                * </span>
                                                                </label>
                                                                <div class="col-md-8">
                                                                    <textarea name="nome-prova" class="form-control" cols="40" rows="2" style="width:100%;"><?php echo $value->srp_nome; ?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <!-- NOME TROFEU -->
                                                            <div class="form-group">
                                                                <label class="control-label col-md-4">Nome do Troféu
                                                                </label>
                                                                <div class="col-md-8">
                                                                    <textarea name="nome-trofeu" class="form-control" cols="40" rows="2" style="width:100%;"><?php echo $value->srp_nome_trofeu; ?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <!-- DIA DA PROVA -->
                                                            <div class="form-group">
                                                                <label class="control-label col-md-4">Dia da Prova <span class="required" aria-required="true">
                                                                * </span>
                                                                </label>
                                                                <div class="col-md-8">
                                                                    <select name="dia" class="form-control">
                                                                        <option value="">- Selecione -</option>
                                                                        <?php
                                                                            for ($i=0; $i < $QuantidadeDiasEvento; $i++):
                                                                                $QuantidadeDias     =   '+' . $i . ' day';
                                                                                $dateValue = strtotime($QuantidadeDias, strtotime($EventoInicio));
                                                                                $dataProvaAtual = $this->my_data->ConverterData($value->srp_dia, 'ISO', 'PT-BR') . " - ". $this->my_data->diasemana($value->srp_dia);
                                                                                $dataProvaDisponivel = $this->my_data->ConverterData(date("Y-m-d", $dateValue), 'ISO', 'PT-BR') . ' - ' . $this->my_data->diasemana(date("Y-m-d", $dateValue));
                                                                        ?>
                                                                                <option
                                                                                    <?php echo ($dataProvaAtual == $dataProvaDisponivel) ? "selected" : "" ?>
                                                                                    value="<?php echo date("Y-m-d", $dateValue); ?>"
                                                                                    >
                                                                                    <?php
                                                                                        echo $dataProvaDisponivel;
                                                                                     ?>
                                                                                </option>
                                                                        <?php endfor; ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <!-- HORÁRIO -->
                                                            <div class="form-group">
                                                                <label class="control-label col-md-4">Hora da Prova
                                                                </label>
                                                                <div class="col-md-8">
                                                                    <div class="input-group">
                                                                        <input type="text" value="<?php echo $value->srp_hora; ?>" class="form-control timepicker timepicker-24 time" name="hora" placeholder="Hora da Prova HH:MM:SS">
                                                                        <span class="input-group-btn">
                                                                            <button class="btn default" type="button">
                                                                                <i class="fa fa-clock-o"></i>
                                                                            </button>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <!-- TIPO DA PISTA -->
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4">
                                                                Tipo da Pista
                                                                 <span class="required" aria-required="true">
                                                            * </span>
                                                            </label>
                                                            <div class="col-md-8">
                                                                <select class="form-control" name="tipo-pista" id="tipo-pista">
                                                                    <option value="">- Selecione um Tipo de Pista -</option>
                                                                    <?php
                                                                     foreach ($arrTipoDePista as $key => $pista): ?>
                                                                    <option 
                                                                    <?php echo ($pista->spp_id == $value->fk_spp_id) ? "selected" : "" ?>
                                                                    value="<?php echo $pista->spp_id; ?>">
                                                                        <?php echo $pista->spp_pista; ?>
                                                                    </option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <!-- CARACTERISTICA -->
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4">
                                                                Característica
                                                                 <span class="required" aria-required="true">
                                                            * </span>
                                                            </label>
                                                            <div class="col-md-8">
                                                                <select class="form-control" name="caracteristica" id="caracteristica">
                                                                    <option value="">- Selecione a Característica de Prova -</option>
                                                                    <?php foreach ($arrCaracteristica as $key => $caracteristica): ?>
                                                                    <option
                                                                    <?php echo ($caracteristica->spc_id == $value->fk_spc_id) ? "selected" : "" ?>
                                                                    value="<?php echo $caracteristica->spc_id; ?>">
                                                                        <?php echo $caracteristica->spc_caracteristica . ' - ' . $caracteristica->spc_regulamento; ?>
                                                                    </option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <!-- TIPO DE SORTEIO -->
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4">
                                                                Tipo de Sorteio
                                                                 <span class="required" aria-required="true">
                                                            * </span>
                                                            </label>
                                                            <div class="col-md-8">
                                                                <select class="form-control" name="tipo-sorteio" id="tipo-sorteio">
                                                                    <option value="">- Selecione o Tipo de Sorteio -</option>
                                                                    <?php foreach ($arrTipoDeSorteio as $key => $sorteio): ?>
                                                                    <option
                                                                    <?php echo ($sorteio->sps_id == $value->fk_sps_id) ? "selected" : "" ?>
                                                                    value="<?php echo $sorteio->sps_id; ?>">
                                                                        <?php echo $sorteio->sps_sorteio; ?>
                                                                    </option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <!-- STATUS -->
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4">
                                                                Status
                                                                 <span class="required" aria-required="true">
                                                            * </span>
                                                            </label>
                                                            <div class="col-md-8">
                                                                <select class="form-control" name="fk-status" id="fk-status">
                                                                    <option value="">- Selecione o Status -</option>
                                                                    <?php foreach ($arrStatus as $key => $sta): ?>
                                                                    <option
                                                                    <?php echo ($sta->sta_id == $value->fk_sta_id) ? "selected" : "" ?>
                                                                    value="<?php echo $sta->sta_id; ?>">
                                                                        <?php echo $sta->sta_status; ?>
                                                                    </option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <hr>
                                                <span class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm active">
                                                    Valores e Limites
                                                </span>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <!-- VALOR DA PROVA -->
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4">
                                                                Valor da Prova
                                                                 <span class="required" aria-required="true">
                                                            * </span>
                                                            </label>
                                                            <div class="col-md-3">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"> R$ </span>
                                                                    <input type="text" value="<?php echo $value->srp_valor; ?>" class="form-control text-center money" name="valor-prova" placeholder="Digite um valor">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- VALOR DA PROVA PROMOCIONAL -->
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4">
                                                                Valor da Prova ( Promocional )
                                                                 <span class="required" aria-required="true">
                                                            * </span>
                                                            </label>
                                                            <div class="col-md-3">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"> R$ </span>
                                                                    <input type="text" value="<?php echo $value->srp_valor_promocional; ?>" class="form-control text-center money" name="valor-prova-promocional" placeholder="Digite um valor">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <!-- LIMITE INSCRICAO PROVA -->
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4">
                                                                Limite de Inscrições da Prova
                                                                 <span class="required" aria-required="true">
                                                            * </span>
                                                            </label>
                                                            <div class="col-md-3">
                                                                <div class="input-group">
                                                                    <input type="number" value="<?php echo $value->srp_limite_inscricao_prova; ?>" name="limite-inscricao-prova" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <!-- LIMITE INSCRICAO PROVA -->
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4">
                                                                Limite de Inscrições por Atleta
                                                                 <span class="required" aria-required="true">
                                                            * </span>
                                                            </label>
                                                            <div class="col-md-3">
                                                                <div class="input-group">
                                                                    <input type="number" value="<?php echo $value->srp_limite_inscricao_atleta; ?>" name="limite-inscricao-atleta" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <?php
                                                if($evento[0]->fk_evm_id == '1'): ?>
                                                <hr>
                                                <span class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm active">
                                                    Salto
                                                </span>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <!-- VELOCIDADE -->
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4">
                                                                Velocidade
                                                            </label>
                                                            <div class="col-md-3">
                                                                <div class="input-group">
                                                                    <input type="text" value="<?php echo $value->srp_velocidade; ?>" class="form-control text-center" name="velocidade" placeholder="">
                                                                    <span class="input-group-addon"> m/min </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <!-- OBSTACULO -->
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4">Obstáculo (Altura)</label>
                                                            <div class="col-md-8">
                                                                <div class="input-group col-sm-4" style="float: left;margin-right: 10px;">
                                                                    <input type="text" value="<?php echo $value->srp_obstaculo_altura; ?>" class="form-control text-center" name="obstaculo-altura" placeholder="Altura">
                                                                    <span class="input-group-addon"> metros </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <!-- OBSTACULO -->
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4">Obstáculo (Largura)</label>
                                                            <div class="col-md-8">
                                                                <div class="input-group col-sm-4" style="float: left;margin-right: 10px;">
                                                                    <input type="text" value="<?php echo $value->srp_obstaculo_largura; ?>" class="form-control text-center" name="obstaculo-largura" placeholder="Largura">
                                                                    <span class="input-group-addon"> metros </span>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php endif; ?>


                                                <div class="btn-footer-actions" style="background-color: rgba(238, 238, 238, 0.41); padding: 30px; border-top: 2px solid #eee;margin-top: 25px;">
                                                    <button type="submit" class="btn btn-default btn-salvar-prova">Salvar <span class="glyphicon glyphicon-edit"></span></button>
                                                    <a href="#" class="btn btn-danger btn-cancelar-prova">Cancelar <span class="fa fa-close"></span></a>
                                                </div>
                                            </form>

                                        </div>
                                        <!-- /tab ( informacoes ) -->

                                        <div class="tab-pane" id="tab_<?php echo $value->srp_id; ?>_categorias">
                                            <!-- BEGIN BORDERED TABLE PORTLET-->
                                            <div class="portlet light portlet-fit margin-top-0">
                                                    <div class="table-scrollable table-scrollable-borderless">
                                                        <table class="table table-hover table-light">
                                                            <thead>
                                                                <tr class="uppercase">
                                                                    <th> Categoria </th>
                                                                    <th> Desconto </th>
                                                                    <th class="text-center"> Ações </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                                <?php
                                                                    $this->load->module('evento/prova');
                                                                    $CategoriasDaProva      =   $this->prova->GetCategoriasDaProva($value->srp_id);
                                                                    foreach ($CategoriasDaProva as $key => $categoria):
                                                                ?>
                                                                <tr>
                                                                    <td>
                                                                        <?php echo $categoria->evc_sigla; ?>
                                                                        <br>
                                                                        <small><?php echo $categoria->evc_categoria; ?></small>
                                                                    </td>
                                                                    <td> <?php echo (!is_null($categoria->prc_desconto)) ? "R$ " . $this->my_moeda->InserirPontuacao($categoria->prc_desconto) : '-' ; ?> </td>
                                                                    <td class="text-center">
                                                                        <button
                                                                            class="btn btn-sm btn-primary btn btn-circle popovers"
                                                                            data-container="body"
                                                                            onclick=" "
                                                                            data-html="true" 
                                                                            data-trigger="hover"
                                                                            data-placement="left"
                                                                            data-content="

                                                                                <small> 
                                        
                                                                                    <b>Desconto:</b><br>
                                                                                    <?php echo (!is_null($categoria->prc_desconto)) ? "R$ " . $this->my_moeda->InserirPontuacao($categoria->prc_desconto) : '-' ; ?>
                                                                                    <hr style='margin:8px 0;'>

                                                                                    <b>Limitar Inscrições:</b><br>
                                                                                    <?php echo (!is_null($categoria->flag_limite_inscricao)) ? 'Sim' : 'Não'; ?>
                                                                                    <?php echo (!is_null($categoria->flag_limite_inscricao)) ? '( ' . $categoria->prc_limite_inscricao_quantidade . ' ) ' : ''; ?>
                                                                                    
                                                                                    <hr style='margin:8px 0;'>

                                                                                    <b>Utilizar Limite do Atleta:</b><br>
                                                                                    <?php echo (!is_null($categoria->flag_limite_atleta)) ? 'Sim' : 'Não'; ?>
                                                                                    <?php echo (!is_null($categoria->flag_limite_atleta)) ? '( ' . $categoria->prc_limite_atleta_quantidade . ' ) ' : ''; ?>

                                                                                    <hr style='margin:8px 0;'>

                                                                                    <b>ID do Relacionamento:</b><br>
                                                                                    <?php echo $categoria->prc_id; ?>
                                                                                </small>

                                                                            "
                                                                            data-original-title="<b>PMR - Pré Mirim</b>">
                                                                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                                            Detalhes
                                                                        </button>


                                                                        <a href="#" class="btn btn-sm btn-info" style="border-radius: 28px !important;"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a>
                                                                        <a href="#" class="btn btn-sm btn-danger" style="border-radius: 28px !important;"><i class="fa fa-trash-o" aria-hidden="true"></i> Deletar</a>
                                                                    </td>
                                                                </tr>
                                                                <?php endforeach; ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                            </div>
                                            <!-- END BORDERED TABLE PORTLET-->


                                            <div class="btn-footer-actions" style="background-color: rgba(238, 238, 238, 0.41); padding: 30px; border-top: 2px solid #eee;margin-top: 25px;">
                                                <a href="#" class="btn btn-primary">Cadastrar Categoria na Prova <?php echo ($categoria->srp_numero_prova < 10 ) ? '0' . $categoria->srp_numero_prova : $categoria->srp_numero_prova; ?> <i class="fa fa-plus-circle" aria-hidden="true"></i> </a>
                                            </div>
                                        </div>
                                        <!-- /tab ( categorias ) -->


                                        <div class="tab-pane" id="tab_<?php echo $value->srp_id; ?>_desenhador">
                                            <!-- BEGIN BORDERED TABLE PORTLET-->
                                            <div class="portlet light portlet-fit margin-top-0">

                                                    <?php
                                                        $this->load->module('evento/prova');
                                                        $DatasetDesenhadorDePercursoDaProva      =   $this->prova->GetDesenhadorDePercursoDaProva($value->srp_id);
                                                        if(empty($DatasetDesenhadorDePercursoDaProva)):
                                                    ?>
                                                        <div class="alert alert-warning margin-top-15">
                                                            <strong>Atenção!</strong> <br>
                                                            Nenhum desenhador de percurso cadastrado para <b>Prova <?php echo ($categoria->srp_numero_prova < 10 ) ? '0' . $categoria->srp_numero_prova : $categoria->srp_numero_prova; ?></b>.
                                                        </div>                                                        
                                                    <?php endif; ?>


                                                    <?php
                                                    if(!empty($DatasetDesenhadorDePercursoDaProva)): ?>
                                                    <div class="table-scrollable table-scrollable-borderless">
                                                        <table class="table table-hover table-light">
                                                            <thead>
                                                                <tr class="uppercase">
                                                                    <th> Desenhador </th>
                                                                    <th class="text-center"> Ações </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                                <?php
                                                                    
                                                                    foreach ($DatasetDesenhadorDePercursoDaProva as $key => $desenhador):
                                                                ?>
                                                                <tr>
                                                                    <td>
                                                                        <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $desenhador->fk_pes_id, 1, 'pes_nome_razao_social'); ?>
                                                                        <br>
                                                                        <span class="badge badge-<?php echo (is_null($desenhador->flag_assistente)) ? 'primary' : 'default'; ?>">
                                                                            <?php echo (is_null($desenhador->flag_assistente)) ? 'Desenhador Principal' : 'Assistente'; ?>
                                                                        </span>
                                                                    </td>
                                                                    <td class="text-center">
                                                                        
                                                                        <span
                                                                            class="btn btn-sm btn-primary btn-circle popovers"
                                                                            data-container="body"
                                                                            onclick=" "
                                                                            data-html="true" 
                                                                            data-trigger="hover"
                                                                            data-placement="left"
                                                                            data-content="
                                                                                <small> 
                                                                                    <b>ID Pessoa do Desenhador:</b><br>
                                                                                    <?php echo $desenhador->pes_id; ?>
                                                                                    <hr style='margin:8px 0;'>

                                                                                    <b>ID do Vínculo ( Desenhador > Prova ):</b><br>
                                                                                    <?php echo $desenhador->prd_id; ?>
                                                                                    <hr style='margin:8px 0;'>

                                                                                    <b>Autor do Registro:</b><br>
                                                                                    <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $desenhador->fk_aut_id, 1, 'pes_nome_razao_social'); ?> <br>
                                                                                    ID: <?php echo $desenhador->fk_aut_id; ?>
                                                                                    <hr style='margin:8px 0;'>

                                                                                </small>
                                                                            "
                                                                            data-original-title="<b>PMR - Pré Mirim</b>">
                                                                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                                            Detalhes
                                                                        </span>



                                                                        <a href="#" class="btn btn-sm btn-info btn-circle"><i class="fa fa-pencil" aria-hidden="true"></i> <?php echo (is_null($desenhador->flag_assistente)) ? 'Definir Assistente' : 'Definir Desenhador'; ?></a>
                                                                        <a href="#" class="btn btn-sm btn-danger btn-circle"><i class="fa fa-trash-o" aria-hidden="true"></i> Deletar</a>
                                                                    </td>
                                                                </tr>
                                                                <?php endforeach;  ?>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <?php endif; ?>


                                            </div>
                                            <!-- END BORDERED TABLE PORTLET-->

                                            <hr>


                                            <?php
                                                $DatasetDesenhadorPercursoExcetoProva       =   $this->prova->GetDesenhadorDePercursoExcetoProva($value->srp_id, $EventoId);
                                                if(empty($DatasetDesenhadorPercursoExcetoProva)):
                                            ?>
                                            <div class="alert alert-info">
                                                <strong>Informativo!</strong> <br>
                                                Todos os desenhadores de percurso do evento estão cadastrados para essa prova.
                                            </div>
                                            <?php endif; ?>


                                            <?php if(!empty($DatasetDesenhadorPercursoExcetoProva)): ?>
                                            <div class="panel-group accordion" id="accordion3">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#desenhador_collapse_<?php echo $value->srp_id; ?>"> Desenhadores de Percurso do Evento </a>
                                                        </h4>
                                                    </div>
                                                    <div id="desenhador_collapse_<?php echo $value->srp_id; ?>" class="panel-collapse collapse">
                                                        <div class="panel-body" style="height:200px; overflow-y:auto;">
                                                            <select class="form-control" name="desenhador-percurso[]" id="desenhador-percurso" multiple>
                                                                <?php
                                                                   foreach ($DatasetDesenhadorPercursoExcetoProva as $key => $desenhador):
                                                                ?>
                                                                <option value="<?php echo $desenhador; ?>"><?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $desenhador, 1, 'pes_nome_razao_social'); ?></option>
                                                                <?php endforeach; ?>
                                                            </select>    
                                                            <a href="#" class="btn btn-primary block margin-top-15">
                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                Cadastrar
                                                            </a>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php endif; ?>
                                        </div>
                                        <!-- /tab ( desenhador de percurso ) -->


                                        <div class="tab-pane" id="tab_<?php echo $value->srp_id; ?>_provasdependentes">
                                            <!-- BEGIN BORDERED TABLE PORTLET-->
                                            <div class="portlet light portlet-fit margin-top-0">

                                                <!-- FLAG -->
                                                <div class="form-group text-center">

                                                    <label class="control-label text-center" style="display: block; background-color: #fff; padding: 15px; border-bottom: 2px solid #ddd; margin-bottom: 20px;">
                                                        Informe se a <b>Prova <?php echo ($value->srp_numero_prova < 10 ) ? '0' . $value->srp_numero_prova : $value->srp_numero_prova; ?> </b> possui provas dependentes.
                                                        <span class="required" aria-required="true"> * </span>
                                                    </label>
                                                    
                                                    <input type="radio" name="prova-dependente-<?php echo $value->srp_numero_prova; ?>" value="1" id="prova-dependente-<?php echo $value->srp_numero_prova; ?>-sim" <?php echo (!is_null($value->flag_prova_dependente)) ? 'checked' : ''; ?> >
                                                    <label for="prova-dependente-<?php echo $value->srp_numero_prova; ?>-sim" style="margin-right: 25px;">SIM</label>

                                                    <input type="radio" name="prova-dependente-<?php echo $value->srp_numero_prova; ?>" value="2" id="prova-dependente-<?php echo $value->srp_numero_prova; ?>-nao" <?php echo (is_null($value->flag_prova_dependente)) ? 'checked' : ''; ?> >
                                                    <label for="prova-dependente-<?php echo $value->srp_numero_prova; ?>-nao">NÃO</label>


                                                </div>
                                                <!-- fim prova-dependente -->

                                            </div>
                                            <!-- END BORDERED TABLE PORTLET-->


                                            <div class="btn-footer-actions" style="background-color: rgba(238, 238, 238, 0.41); padding: 15px; border-top: 2px solid #eee;margin-top: 25px;">
                                                <a href="#" class="btn btn-primary" style="display: block;">
                                                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                                    Salvar Provas Dependentes da Prova <?php echo ($categoria->srp_numero_prova < 10 ) ? '0' . $categoria->srp_numero_prova : $categoria->srp_numero_prova; ?>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- /tab ( provas dependentes ) -->


                                        <div class="tab-pane" id="tab_<?php echo $value->srp_id; ?>_inscricoes">
                                            <div class="alert alert-warning margin-top-15">
                                                <strong>Atenção!</strong> <br>
                                                Nenhum conjunto inscrito para a <b>Prova <?php echo ($categoria->srp_numero_prova < 10 ) ? '0' . $categoria->srp_numero_prova : $categoria->srp_numero_prova; ?></b>.
                                            </div>
                                        </div>
                                        <!-- /tab ( inscricoes ) -->
                                    </div>
                                    <!-- /tab-content -->

                                </div>
                                <!-- /tabbable-line -->

                            </div>
                            <!-- /portlet-body -->

                        </div>
                        <!-- /tab-pane -->

                        <?php
                            $Contador++;
                            endforeach;
                        ?>

                    </div>
                    <!-- /tab-content -->

                </div>
                <!-- /col-md-9 -->

            </div>
            <!-- /row -->

        </div>
        <!-- /portlet-body -->

    </div>
    <!-- /porlet -->

</div>
<!-- /evento-serie -->





