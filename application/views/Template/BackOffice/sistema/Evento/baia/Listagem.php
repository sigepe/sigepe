
    <div class="col-sm-9 evento-conteudo" id="evento-quarto-de-sela">

        <div class="portlet light bordered" style="float: left;width: 100%;">
          
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <i class="icon-speech font-green-sharp"></i>
                    <span class="caption-subject bold uppercase"> BAIA</span>
                    <span class="caption-helper">Detalhes de baia desse evento.</span>
                </div>
            </div>

            <div class="portlet-body" style="padding-top: 0px;">

                <div class="row">

                    <div class="col-sm-12">

                        <div class="tabbable-line">
                            <ul class="nav nav-tabs ">
                                <li class="active">
                                    <a href="#tab_quartodesela_informacoes" data-toggle="tab"> Informações Gerais </a>
                                </li>
                                <li>
                                    <a href="#tab_quartodesela_atletasresponsaveis" data-toggle="tab">
                                        Animais
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content" style="padding-top: 0px;">
                                
                                <div class="tab-pane active" id="tab_quartodesela_informacoes">

                                    <!-- FLAG -->
                                    <div class="form-group text-center">


                                        <label class="control-label text-center" style="display: block; margin-bottom: 5px; margin-top: 20px;">
                                            Informe se no evento será permitido a venda de <b>baia.</b>
                                            <span class="required" aria-required="true"> * </span>
                                        </label>
                                        
                                        <input type="radio" name="baia" value="1" id="baia-sim" <?php echo (!is_null($DatasetEvento[0]->flag_baia)) ? 'checked' : ''; ?> >
                                        <label for="baia-sim" style="margin-right: 25px;">SIM</label>

                                        <input type="radio" name="baia" value="2" id="baia-nao" <?php echo (is_null($DatasetEvento[0]->flag_baia)) ? 'checked' : ''; ?> >
                                        <label for="baia-nao">NÃO</label>

                                        <hr>

                                        <!-- QUANTIDADE -->
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="control-label col-md-4 text-right">Quantidade <span class="required" aria-required="true">
                                                * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <div class="input-group">
                                                        <input type="number" name="limite-inscricao-prova" class="form-control" value="<?php echo $DatasetEvento[0]->eve_baia_quantidade; ?>" >
                                                    </div>                                                
                                                </div>
                                            </div>
                                        </div>


                                        <!-- VALOR SEM ACRESCIMO -->
                                        <div class="row margin-top-15">
                                            <div class="form-group">
                                                <label class="control-label col-md-4 text-right">Valor sem acréscimo <span class="required" aria-required="true">
                                                * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"> R$ </span>
                                                        <input type="text" class="form-control text-center money" value="<?php echo $DatasetEvento[0]->eve_baia_valor_sem_acrescimo; ?>" name="valor-sem-acrescimo" placeholder="Digite um valor" maxlength="22">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <!-- VALOR COM ACRESCIMO -->
                                        <div class="row margin-top-15">
                                            <div class="form-group">
                                                <label class="control-label col-md-4 text-right">Valor com acréscimo <span class="required" aria-required="true">
                                                * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"> R$ </span>
                                                        <input type="text" class="form-control text-center money" name="valor-com-acrescimo" value="<?php echo $DatasetEvento[0]->eve_baia_valor_com_acrescimo; ?>" placeholder="Digite um valor" maxlength="22">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <!-- BLOQUEIO -->
                                        <div class="row margin-top-15">
                                            <div class="form-group">
                                                <label class="control-label col-md-4 text-right">Bloqueio
                                                </label>
                                                <div class="col-md-8 text-left">
                                                    
                                                    <input type="checkbox" name="bloqueio" id="bloqueio" style="margin-right: 10px;" <?php echo (!is_null($DatasetEvento[0]->eve_baia_bloqueio)) ? 'checked' : ''; ?> >
                                                    <label for="bloqueio">Bloquear venda de baia se exceder quantidade.</label>

                                                </div>
                                            </div>
                                        </div>

                                        <hr>


                                        <!-- OBRIGATORIEDADE -->
                                        <div class="row margin-top-15">
                                            <div class="form-group">
                                                <label class="control-label col-md-4 text-right">Obrigatoriedade
                                                </label>
                                                <div class="col-md-8 text-left">
                                                    
                                                    <input type="checkbox" value="" name="obrigatoriedade" id="obrigatoriedade" style="margin-right: 10px;">
                                                    <label for="obrigatoriedade">Tornar obrigatório a aquisição de baia para concluir a inscrição.</label>

                                                </div>
                                            </div>
                                        </div>

                                        <!-- OBRIGATORIEDADE -->
                                        <div class="row margin-top-15">
                                            <div class="form-group">
                                                <label class="control-label col-md-4 text-right">Exceção
                                                </label>
                                                <div class="col-md-7 text-left">
                                                    
                                                    <select class="form-control">
                                                        <option>- Selecione uma Entidade -</option>
                                                        <?php foreach ($DatasetEntidadeEscola as $key => $value): ?>
                                                        <option value="<?php echo $value->pes_id; ?>"><?php echo $value->pes_nome_razao_social; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>

                                                    <span class="help-block">
                                                        Indique qual entidade é isenta da obrigatoriedade de baia. 
                                                        <br>Opção válida apenas se a opção acima ativa.
                                                    </span>

                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <!-- fim quarto-de-sela-informacoes -->


                                    <div class="btn-footer-actions" style="background-color: rgba(238, 238, 238, 0.41); padding: 15px; border-top: 2px solid #eee;margin-top: 25px;">
                                        <a href="#" class="btn btn-primary" style="display: block;">
                                            <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                            Salvar Baia 
                                        </a>
                                    </div>

                                </div>
                                <!-- /tab_quartodesela_informacoes -->


                                <div class="tab-pane" id="tab_quartodesela_atletasresponsaveis">
                                    <div class="alert alert-warning margin-top-15">
                                        <strong>Atenção!</strong> <br>
                                        Nenhum animal vinculado a alguma baia. <br>
                                        <small>Não foi localizado nenhuma venda.</small>
                                    </div>
                                </div>
                                <!-- /tab_quartodesela_informacoes -->

                            </div>
                        </div>

                    </div>

                </div>
                <!-- /row -->

            </div>
            <!-- /portlet-body -->

        </div>
        <!-- /porlet -->

</div>
<!-- /evento-serie -->





 