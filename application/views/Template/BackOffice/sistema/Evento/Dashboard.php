<style type="text/css">
    .h4, .h5, .h6, h4, h5, h6 {
        margin-top: 0px;
        margin-bottom: 5px;
    }
    hr, p {
        margin: 10px 0;
    }
    .portlet.light.bordered {
        border-bottom: 3px solid rgba(204, 204, 204, 0.45) !important;
    }
    .td-show {
        margin: 0px;
    }
    .td-edit {
        margin: 0px;
    }
</style>

<div class="col-sm-9" id="evento-conteudo">



    <div class="row">

        <!-- ATLETA -->
        <div class="col-sm-4">
            <div class="portlet light bordered">
                <div class="portlet-body">
                    <div class="dashboard-stat2 ">
                        <div class="display">
                            <div class="number">
                                <h3 class="font-green-sharp">
                                    <span data-counter="counterup" data-value="0">0</span>
                                </h3>
                                <small>ATLETA</small>
                            </div>
                            <div class="icon">
                                <i class="icon-pie-chart"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- ANIMAL -->
        <div class="col-sm-4">
            <div class="portlet light bordered">
                <div class="portlet-body">
                    <div class="dashboard-stat2 ">
                        <div class="display">
                            <div class="number">
                                <h3 class="font-green-sharp">
                                    <span data-counter="counterup" data-value="0">0</span>
                                </h3>
                                <small>ANIMAL</small>
                            </div>
                            <div class="icon">
                                <i class="icon-pie-chart"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- INSCRICAO -->
        <div class="col-sm-4">
            <div class="portlet light bordered">
                <div class="portlet-body">
                    <div class="dashboard-stat2 ">
                        <div class="display">
                            <div class="number">
                                <h3 class="font-green-sharp">
                                    <span data-counter="counterup" data-value="0">0</span>
                                </h3>
                                <small>INSCRIÇÕES</small>
                            </div>
                            <div class="icon">
                                <i class="icon-pie-chart"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>




    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light bordered">

                <div class="portlet-title">
                    <div class="caption font-green-sharp">
                        <i class="icon-speech font-green-sharp"></i>
                        <span class="caption-subject bold uppercase"> INFORMAÇÕES GERAIS</span>
                        <span class="caption-helper">Resumo do Evento</span>
                    </div>
                    <div class="actions">
                        <a href="#" class="btn btn-circle btn-editar btn-default">
                            <i class="fa fa-pencil"></i> Editar </a>
                    </div>
                </div>

                <div class="portlet-body">
                    <div class="row">
                        <div class="col-sm-12">


                            <div class="table-scrollable table-scrollable-borderless">
                                <table class="table table-hover table-light">
                                    <tbody>
                                        <tr>
                                            <td class="text-right bold">Modalidade</td>
                                            <td><p class="td-show">Salto</p>
                                                <p class="td-edit"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Tipo</td>
                                            <td><p class="td-show">
                                                    <?php if (empty($DatasetTipoEvento)): ?>
                                                    <div class="alert alert-danger margin-top-15">
                                                        <strong>Erro!</strong> <br>
                                                        Nenhum tipo de evento encontrado. <br>
                                                        Referência: tb_evento_tipo | tb_evento_rel_tipo.
                                                    </div>
                                                <?php endif; ?>

                                                <?php foreach ($DatasetTipoEvento as $key => $value): ?>
                                                    <b><?php echo $this->model_crud->get_rowSpecific('tb_evento_tipo', 'evt_id', $value->fk_evt_id, 1, 'evt_sigla'); ?></b> -
                                                    <?php echo $this->model_crud->get_rowSpecific('tb_evento_tipo', 'evt_id', $value->fk_evt_id, 1, 'evt_tipo'); ?>
                                                    <br>
                                                <?php endforeach; ?>
                                                </p>
                                                <p class="td-edit"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Período</td>
                                            <td><p class="td-show">
                                                    <?php echo $QuantidadeDiasEvento; ?> dia<?php echo ($QuantidadeDiasEvento > 1) ? 's' : ''; ?> de evento <br>
                                                    Data início: <?php echo $this->my_data->ConverterData($DatasetEvento[0]->eve_data_inicio, 'ISO', 'PT-BR'); ?> - <?php echo $this->my_data->diasemana($DatasetEvento[0]->eve_data_inicio); ?> <br>
                                                    Data fim: <?php echo $this->my_data->ConverterData($DatasetEvento[0]->eve_data_fim, 'ISO', 'PT-BR'); ?> - <?php echo $this->my_data->diasemana($DatasetEvento[0]->eve_data_fim); ?>
                                                </p>
                                                <p class="td-edit"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Qtde. provas</td>
                                            <td><p class="td-show">
                                                    <?php echo $DatasetEvento[0]->eve_quantidade_prova; ?>
                                                </p>
                                                <p class="td-edit"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Data limite <br> sem acréscimo</td>
                                            <td><p class="td-show">
                                                    <?php echo $this->my_data->ConverterData($DatasetEvento[0]->eve_data_limite_sem_acrescimo, 'ISO', 'PT-BR'); ?>
                                                </p>
                                                <p class="td-edit"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Local</td>
                                            <td><p class="td-show">
                                                    <?php echo $DatasetEvento[0]->fk_pes_id; ?>
                                                </p>
                                                <p class="td-edit"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Ativo no Site</td>
                                            <td><p class="td-show">
                                                    <?php echo (!is_null($DatasetEvento[0]->flag_site)) ? 'Sim  ( está sendo exibido no site/sistema )' : 'Não'; ?>
                                                </p>
                                                <p class="td-edit"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Abertura das Inscrições</td>
                                            <td><p class="td-show">
                                                    Inscrições Abertas: <?php echo $this->my_data->ConverterData($DatasetEvento[0]->eve_site_inscricao_inicio, 'ISO', 'PT-BR'); ?> - <?php echo $this->my_data->diasemana($DatasetEvento[0]->eve_site_inscricao_inicio); ?> <br>
                                                </p>
                                                <p class="td-edit"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Encerramento das Inscrições</td>
                                            <td><p class="td-show">
                                                    Inscrições Encerram: <?php echo $this->my_data->ConverterData($DatasetEvento[0]->eve_site_inscricao_fim, 'ISO', 'PT-BR'); ?> - <?php echo $this->my_data->diasemana($DatasetEvento[0]->eve_site_inscricao_fim); ?> 
                                                    <br><?php echo DateDifferences($DatasetEvento[0]->eve_site_inscricao_fim, $DatasetEvento[0]->eve_site_inscricao_inicio, 'd') + 1; ?> dias de inscrições em aberto
                                                </p>
                                                <p class="td-edit"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Venda de Inscrições por Tipo</td>
                                            <td><p class="td-show">
                                                    <?php echo $this->model_crud->get_rowSpecific('tb_evento_venda', 'evv_id', $DatasetEvento[0]->fk_evv_id, 1, 'evv_venda'); ?>
                                                </p>
                                                <p class="td-edit"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Baia Ativo</td>
                                            <td><p class="td-show">
                                                    <?php echo (!is_null($DatasetEvento[0]->flag_baia)) ? 'Sim' : 'Não'; ?>
                                                </p>
                                                <p class="td-edit"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Quarto de Sela Ativo</td>
                                            <td><p class="td-show">
                                                    <?php echo (!is_null($DatasetEvento[0]->flag_quarto)) ? 'Sim' : 'Não'; ?>
                                                </p>
                                                <p class="td-edit"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right bold">Desenhador de Percurso</td>
                                            <td><p class="td-show">
                                                    <?php
                                                    foreach ($DesenhadorPercurso as $desenhador) {
                                                        echo $desenhador->pes_nome_razao_social . "<br>";
                                                    }
                                                    ?>
                                                </p>
                                                <p class="td-edit"></p>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td class="text-right bold">Descrição</td>
                                            <td><div class="td-show">
                                                    <?php echo $DatasetEvento[0]->eve_descricao; ?>
                                                </div>
                                            </td>
                                        </tr>



                                        <!-- <tr>
                                            <td class="text-right bold">Descrição</td>
                                            <td><p class="td-show">
                                                    <?php echo $DatasetEvento[0]->eve_descricao; ?>
                                                </p>
                                                <p class="td-edit"></p>
                                            </td>
                                        </tr> -->


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-sm-6">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green-sharp">
                        <i class="icon-speech font-green-sharp"></i>
                        <span class="caption-subject bold uppercase"> BAIAS</span>
                    </div>
                </div>
                <div class="portlet-body">

                    <div class="row">
                        <div class="col-sm-12">
                            <h5 class="bold">Permitir venda de Baias</h5>
                            <small>
                                <?php echo (!is_null($DatasetEvento[0]->flag_baia)) ? 'Sim' : 'Não'; ?>
                            </small>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-sm-6">
                            <h5 class="bold">Estoque Total:</h5>
                            <small>
                                <?php echo (!is_null($DatasetEvento[0]->eve_baia_quantidade)) ? $DatasetEvento[0]->eve_baia_quantidade . ' Unidades' : 'N/I'; ?>
                            </small>
                        </div>
                        <div class="col-sm-6">
                            <h5 class="bold">Baias Disponíveis</h5>
                            -
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-sm-6">
                            <h5 class="bold">Valor <b>SEM</b> Acréscimo:</h5>
                            <small>
                                <?php
                                echo (!is_null($DatasetEvento[0]->eve_baia_valor_sem_acrescimo)) ? 'R$ ' . $this->my_moeda->InserirPontuacao($DatasetEvento[0]->eve_baia_valor_sem_acrescimo) : 'N/I';
                                ?>
                            </small>
                        </div>
                        <div class="col-sm-6">
                            <h5 class="bold">Valor <b>COM</b> Acréscimo</h5>
                            <?php
                            echo (!is_null($DatasetEvento[0]->eve_baia_valor_com_acrescimo)) ? 'R$ ' . $this->my_moeda->InserirPontuacao($DatasetEvento[0]->eve_baia_valor_com_acrescimo) : 'N/I';
                            ?>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-sm-12">
                            <h5 class="bold">Bloqueio:</h5>

                            <?php if (!is_null($DatasetEvento[0]->eve_baia_bloqueio)): ?>
                                <small>
                                    <b>Sim</b> -
                                    Automaticamente a venda de baias é suspensa caso esgote o estoque de baias disponíveis.
                                </small>
                            <?php endif; ?>

                            <?php if (is_null($DatasetEvento[0]->eve_baia_bloqueio)): ?>
                                <small>
                                    <b>Não</b> -
                                    Caso exceda a quantidade de estoque disponíveis para venda ainda sim será permitido usuários adquirerem baias.
                                </small>
                            <?php endif; ?>

                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-sm-12">
                            <a href="#" class="btn btn-primary" style="display: block;">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                Gerenciar Baias
                            </a>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <div class="col-sm-6">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green-sharp">
                        <i class="icon-speech font-green-sharp"></i>
                        <span class="caption-subject bold uppercase"> QUARTOS DE SELA</span>
                    </div>
                </div>
                <div class="portlet-body">

                    <div class="row">
                        <div class="col-sm-12">
                            <h5 class="bold">Permitir venda de Quartos de Sela</h5>
                            <small>
                                <?php echo (!is_null($DatasetEvento[0]->flag_quarto)) ? 'Sim' : 'Não'; ?>
                            </small>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-sm-6">
                            <h5 class="bold">Estoque Total:</h5>
                            <small>
                                <?php echo (!is_null($DatasetEvento[0]->eve_quarto_quantidade)) ? $DatasetEvento[0]->eve_quarto_quantidade . ' Unidades' : 'N/I'; ?>
                            </small>
                        </div>
                        <div class="col-sm-6">
                            <h5 class="bold">Q.S. Disponíveis</h5>
                            -
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-sm-6">
                            <h5 class="bold">Valor <b>SEM</b> Acréscimo:</h5>
                            <small>
                                <?php
                                echo (!is_null($DatasetEvento[0]->eve_quarto_valor_sem_acrescimo)) ? 'R$ ' . $this->my_moeda->InserirPontuacao($DatasetEvento[0]->eve_quarto_valor_sem_acrescimo) : 'N/I';
                                ?>
                            </small>
                        </div>
                        <div class="col-sm-6">
                            <h5 class="bold">Valor <b>COM</b> Acréscimo</h5>
                            <?php
                            echo (!is_null($DatasetEvento[0]->eve_quarto_valor_com_acrescimo)) ? 'R$ ' . $this->my_moeda->InserirPontuacao($DatasetEvento[0]->eve_quarto_valor_com_acrescimo) : 'N/I';
                            ?>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-sm-12">
                            <h5 class="bold">Bloqueio:</h5>

                            <?php if (!is_null($DatasetEvento[0]->eve_quarto_bloqueio)): ?>
                                <small>
                                    <b>Sim</b> -
                                    Automaticamente a venda de baias é suspensa caso esgote o estoque de baias disponíveis.
                                </small>
                            <?php endif; ?>

                            <?php if (is_null($DatasetEvento[0]->eve_quarto_bloqueio)): ?>
                                <small>
                                    <b>Não</b> -
                                    Caso exceda a quantidade de estoque disponíveis para venda ainda sim será permitido usuários adquirerem quartos de sela.
                                </small>
                            <?php endif; ?>

                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-sm-12">
                            <a href="#" class="btn btn-primary" style="display: block;">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                Gerenciar Quartos de Sela
                            </a>
                        </div>
                    </div>

                </div>
            </div>

        </div>


    </div>


    <div class="row">
        <div class="col-sm-6">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green-sharp">
                        <i class="icon-speech font-green-sharp"></i>
                        <span class="caption-subject bold uppercase"> ÚLTIMAS INSCRIÇÕES</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="alert alert-warning" style="margin-bottom: 0px;">
                        <strong>Atenção!</strong> <br>
                        Nenhuma inscrição localizada.
                    </div>
                </div>
            </div>

        </div>

        <div class="col-sm-6">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green-sharp">
                        <i class="icon-speech font-green-sharp"></i>
                        <span class="caption-subject bold uppercase"> OFICIAIS FHBr</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="alert alert-warning" style="margin-bottom: 0px;">
                        <strong>Atenção!</strong> <br>
                        Nenhum Oficial FHBr localizado.
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
