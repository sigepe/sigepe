<style type="text/css">
.active a{
    font-weight: bold !important;
}
</style>



    <div class="col-sm-3" id="evento-sidebar">

        <div class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet " style="padding-top: 15px !important;">
                <!-- SIDEBAR USERPIC -->

                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu" style="margin-top: 0px !important;">
                    <ul class="nav">
                        <li class="<?php echo ($Class == 'Evento' && $Method == 'Dashboard') ? 'active' : ''; ?>">
                            <a href="{base_url}BackOffice/Evento/Dashboard/Detalhar/{EventoId}">
                                <i class="icon-home"></i> Visão Geral </a>
                        </li>
                        <li>
                            <a href="{base_url}BackOffice/Evento/Inscricao/Listar/{EventoId}">
                                <i class="icon-info"></i> Inscrições </a>
                        </li>
                        <li class="<?php echo ($Class == 'Serie' || $Class == 'CadastroSerie') ? 'active' : ''; ?>">
                            <a href="{base_url}BackOffice/Evento/Serie/Listar/{EventoId}">
                                <i class="icon-info"></i> Séries </a>
                        </li>
                        <li class="<?php echo ($Class == 'Prova' || $Class == 'CadastroProva') ? 'active' : ''; ?>">
                            <a href="{base_url}BackOffice/Evento/Prova/Listar/{EventoId}">
                                <i class="icon-info"></i> Provas </a>
                        </li>
                        <li class="<?php echo ($Class == 'Baia') ? 'active' : ''; ?>">
                            <a href="{base_url}BackOffice/Evento/Baia/Listar/{EventoId}">
                                <i class="icon-info"></i> Baias </a>
                        </li>
                        <li class="<?php echo ($Class == 'QuartoDeSela') ? 'active' : ''; ?>">
                            <a href="{base_url}BackOffice/Evento/QuartoDeSela/Listar/{EventoId}">
                                <i class="icon-info"></i> Quartos de Selas </a>
                        </li>
                        <li>
                            <a href="{base_url}evento/Oficial">
                                <i class="icon-info"></i> Oficiais </a>
                        </li>
                        <li>
                            <a href="{base_url}evento/Patrocinador">
                                <i class="icon-info"></i> Patrocinadores </a>
                        </li>
                        <li>
                            <a href="{base_url}evento/Apoiador">
                                <i class="icon-info"></i> Apoiadores </a>
                        </li>
                        <li>
                            <a href="{base_url}evento/Documento">
                                <i class="icon-info"></i> Documentos </a>
                        </li>
                        <li>
                            <a href="{base_url}evento/OrdemEntrada">
                                <i class="icon-info"></i> Ordem de Entrada </a>
                        </li>
                        <li>
                            <a href="{base_url}evento/Resultado">
                                <i class="icon-info"></i> Resultado </a>
                        </li>
                        <li>
                            <a href="{base_url}evento/Ranking">
                                <i class="icon-info"></i> Ranking </a>
                        </li>
                        <li>
                            <a href="{base_url}evento/Financeiro">
                                <i class="icon-info"></i> Financeiro </a>
                        </li>
                        <li>
                            <a href="{base_url}evento/Fotos">
                                <i class="icon-info"></i> Fotos </a>
                        </li>
                        <li>
                            <a href="{base_url}evento/Configuracao">
                                <i class="icon-info"></i> Configurações </a>
                        </li>
                    </ul>
                </div>
                <!-- END MENU -->
            </div>
            <!-- END PORTLET MAIN -->
        </div>
        <!-- /profile-sidebar -->

    </div>
    <!-- /evento-sidebar -->
