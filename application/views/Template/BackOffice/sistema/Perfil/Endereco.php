

    <!-- BEGIN PROFILE CONTENT -->
    <div class="profile-content" id="perfil-endereco">

        <div class="row">

            <div class="col-md-12">
            
                <div class="portlet light porlet-main">

                    <div class="portlet-title">
                        
                        <div class="caption font-blue-madison">
                            <span class="caption-subject bold uppercase"> ENDEREÇO</span>
                            <span class="caption-helper">Relação de endereços associados a sua conta.</span>
                        </div>
                        
                        <div class="actions">
                            <a class="btn btn-circle btn-edit btn-sm btn-cadastrar-endereco" href="javascript:;">
                                <i class="fa fa-plus"></i>
                                Cadastrar Endereço
                            </a>
                        </div>

                    </div>

                    <div class="portlet-body porlet-body-main">


                        <?php if ( empty($DatasetEndereco) || is_null($DatasetEndereco) ): ?>
                        <div class="alert alert-danger" id="aviso-nenhum-endereco">
                            <strong>Atenção,</strong> 
                            não existe nenhum endereço para você. Mantenha seus dados atualizados. <br>
                            <a href="#" title="" class="">Clique aqui para cadastrar um endereço.</a>
                        </div>
                        <?php endif; ?>


                        <div id="table-scrollable-endereco" class="table-scrollable table-scrollable-borderless <?php echo ( empty($DatasetEndereco) || is_null($DatasetEndereco) ) ? 'display-none' : ''; ?>">

                            <table class="table table-hover table-light" id="table-endereco">
                            
                                <thead>
                                    <tr class="uppercase">
                                        <th class="text-center">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            Endereço
                                        </th>
                                        <th class="text-center"> Ação </th>
                                    </tr>
                                </thead>

                                <tbody>

                                    <?php foreach ($DatasetEndereco as $key => $value): ?>
                                    <tr id="table-row-<?php echo $value->end_id; ?>" data-id="<?php echo $value->end_id; ?>">
                                        <td class="text-left">
                                             
                                            <?php if(!is_null($value->end_principal)): ?>
                                             <a href="javascript:;"
                                                class="tooltips badge badge-warning bold flag-endereco-principal"
                                                title=""
                                                data-original-title="Endereço Principal. Preferencialmente vamos utilizar esse número para entrar em contato com você.">
                                                P
                                            </a>
                                            <?php endif; ?>

                                             <span class="bold">Endereço <?php echo $this->model_crud->get_rowSpecific('tb_contato', 'con_id', $value->fk_con_id, 1, 'con_contato'); ?>:</span>

                                             <br>
                                            <small>
                                                <b>CEP:</b> <?php echo $value->end_cep; ?> <br>
                                                <?php echo $this->model_crud->get_rowSpecific('tb_estado', 'est_id', $value->fk_est_id, 1, 'est_nome'); ?> / <?php echo $this->model_crud->get_rowSpecific('tb_cidade', 'cid_id', $value->fk_cid_id, 1, 'cid_nome'); ?> <br>
                                                <?php echo $value->end_bairro; ?> - <?php echo $value->end_logradouro; ?> - <?php echo $value->end_numero; ?> <br>
                                                <?php echo $value->end_complemento; ?>
                                            </small>

                                        </td>
                                        <td class="text-center">
                                            <span class="btn btn-circle btn-sm btn-success btn-gerenciar-conjunto popovers" data-container="body" onclick=" " data-html="true" data-trigger="hover" data-placement="left" data-content="
                                                    <small> 

                                                        <b>Cadastrado em:</b><br>
                                                        <?php echo $this->my_data->datetime($value->criado, 'datetime_untilMinuts'); ?>

                                                        <hr style='margin: 7px 0;'>

                                                        <b>Por:</b><br>
                                                        <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_aut_id, 1, 'pes_nome_razao_social'); ?> <br>

                                                    </small>
                                                " data-original-title="Detalhes">
                                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                Detalhes
                                            </span>

                                            <?php if(is_null($value->end_principal)): ?>
                                            <a href="javascript:;"
                                                class="btn btn-circle btn-sm btn-warning btn-endereco-principal tooltips"
                                                data-original-title="Transformar esse endereço como principal."
                                            >
                                                
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                                Endereço Principal
                                            </a>
                                            <?php endif; ?>

                                            <?php if(is_null($value->end_principal)): ?>
                                            <a href="javascript:;" class="btn btn-circle btn-sm btn-danger btn-deletar-endereco">
                                                <i class="fa fa-close" aria-hidden="true"></i>
                                                Deletar Endereço
                                            </a>
                                            <?php endif; ?>

                                        </td>
                                    </tr>
                                    <?php endforeach; ?>

                                </tbody>

                            </table>

                        </div>

<!--                         <hr style="margin-top: 12px;"> -->

                        <!-- Formulario -->
                        <form action="#" role="form" id="form-endereco" class="form-horizontal form-view" style="display: none;">

                            <div class="form-body">

                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-yellow-crusta">
                                            <i class="icon-share font-yellow-crusta"></i>
                                            <span class="caption-subject bold uppercase"> Formulário</span>
                                            <span class="caption-helper">Preencha os campos abaixo para cadastrar um endereço a sua conta.</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">

                                        <div class="alert alert-danger display-hide">
                                            <button class="close" data-close="alert"></button>
                                            Preencha todos os campos do formulário.
                                        </div>
                                        
                                        <div class="alert alert-success display-hide">
                                            <button class="close" data-close="alert"></button>
                                            Formulário validado!
                                        </div>


                                        <!-- CEP -->
                                        <div class="form-group">
                                            <label class="control-label col-md-3">CEP
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-map-marker"></i>
                                                    </span>
                                                    <input type="text" class="form-control" name="cep" id="cep" placeholder="CEP">
                                                </div>
                                            </div>
                                        </div>


                                        <!-- ESTADO -->
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Estado
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <select class="form-control" name="estado" id="estado">
                                                    <option value="">- Selecione um Estado -</option>
                                                    <?php foreach ($DatasetEstado as $key => $value): ?>
                                                        <option value="<?php echo $value->est_id; ?>" data-sigla="<?php echo $value->est_sigla; ?>" data-id="<?php echo $value->est_id; ?>">
                                                            <?php echo $value->est_nome; ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>


                                        <!-- CIDADE -->
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Cidade
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <select class="form-control" name="cidade" id="cidade">
                                                    <option value="">- Selecione uma Cidade -</option>
                                                </select>
                                            </div>
                                        </div>


                                        <!-- BAIRRO -->
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Bairro
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="bairro" placeholder="Bairro">
                                                </div>
                                            </div>
                                        </div>


                                        <!-- LOGRADOURO -->
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Logradouro
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-8">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="logradouro" placeholder="Logradouro">
                                                    <span class="help-block" style="margin-top: 7px; float: left; display: block;">
                                                        <i class="fa fa-info-circle"></i>
                                                        Exemplo: Rua, Avenida, Viela, Praça, etc.
                                                    </span>                                                    
                                                </div>
                                            </div>
                                        </div>


                                        <!-- NUMERO -->
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Número
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="numero" placeholder="Número">
                                                </div>
                                            </div>
                                        </div>


                                        <!-- COMPLEMENTO -->
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Complemento
                                            </label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="complemento" placeholder="Complemento">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-md-3">Tipo do Endereço
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <select class="form-control" name="tipo">
                                                    <option value="">- Selecione uma Opção -</option>
                                                    <?php foreach ($DatasetTipoEndereco as $key => $value): ?>
                                                    <option value="<?php echo $value->con_id; ?>"><?php echo $value->con_contato; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- /.portlet-body -->

                                </div>
                                <!-- /.portlet -->
 
                            </div>
                            <!-- /form-body -->

                            <div class="form-actions text-center">
                                <a class="btn blue btn-submit">
                                    <i class="fa fa-save" aria-hidden="true"></i>
                                    Cadastrar Endereço
                                </a>
                            </div>

                        </form>
                        <!-- /form-->

                    </div>
                    <!-- /portlet-body -->

                </div>
                <!-- /.portlet -->

            </div>
            <!-- /.col-md-12 -->

        </div>
        <!-- /.row -->

    </div>
    <!-- END PROFILE CONTENT -->



    </div>  
    <!-- div aberta em sidebar.php ( /.col-sm-12 ) -->

</div>
<!-- div aberta em sidebar.php ( /.row ) -->