
    <!-- BEGIN PROFILE CONTENT -->
    <div class="profile-content" id="perfil-competidor">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light " style="width: 100%; float: left;padding-bottom: 0px;">



                        <div class="tabbable-line-disabled"> <!-- tabbable-line -->

                            <ul class="nav nav-tabs nav-tabs-principal">
                                <li class="active">
                                    <a href="#tab-informacoes-gerais" data-toggle="tab" aria-expanded="true"> Informações Gerais </a>
                                </li>
                                <li class="">
                                    <a href="#tab-vinculo-registro" data-toggle="tab" aria-expanded="false"> Detalhes Vínculo/Registro </a>
                                </li>
                                <li class="">
                                    <a href="#tab-historico-participacoes" data-toggle="tab" aria-expanded="false"> Histórico de Participações </a>
                                </li>
                            </ul>


                            <div class="tab-content" style="padding-top: 0px;">
                                

                                <!--
                                    ****************************************
                                        TAB-INFORMACOES-GERAIS
                                    ****************************************
                                -->
                                <div class="tab-pane active" id="tab-informacoes-gerais">


                                    <!--    
                                        REGISTRO ATIVOS
                                    ===================================================== -->
                                    <?php if($AtletaFhbr): ?>
                                    <div class="portlet light" id="portlet-registro" style="padding: 0;">

                                        <div class="portlet-title">
                                            <div class="caption font-green-sharp">
                                                <span class="caption-subject font-green bold">Registros Ativos - <?php echo date('Y'); ?></span>
                                                <span class="caption-helper">Veja todos os registros que possui ativo junto a federação</span>
                                            </div>
                                            <div class="actions">
                                                <?php if($QtdeTelefone > 0 && $QtdeEmail && $QtdeEndereco > 0 && $AtletaFhbr): ?>
                                                <a href="javascript:;" class="btn btn-circle btn-action-section btn-novo-registro">
                                                    <i class="fa fa-plus"></i> Novo registro
                                                </a>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="portlet-body" style="padding: 0px; float: left; width: 100%;">


                                                <?php if($PagamentoPendente): ?>
                                                <div class="alert alert-danger alert-dismissable" id="tipo-pagamento-boleto-aviso" style="font-weight: 400;font-size: 13px; margin: 0;">
                                                     <b>Atenção.</b>
                                                     Enquanto houver pagamento pendente não é possível criar novos registros.
                                                </div>
                                                <?php endif; ?>

                                                
                                                <?php if($QtdeTelefone == 0): ?>
                                                <div class="alert alert-danger alert-dismissable" id="tipo-pagamento-boleto-aviso" style="font-weight: 400;font-size: 13px; margin: 0;">
                                                     <b>Atenção.</b>
                                                     Notamos que não temos seu número de celular cadastrado em nosso sistema.<br>
                                                     Antes de registrar informe seu número. <br>
                                                     <a href="{base_url}FrontOffice/Perfil/Telefone" title="">Clique aqui para cadastrar um número de celular</a>
                                                </div>
                                                <?php endif; ?>

                                               
                                                <?php if($QtdeEmail == 0): ?>
                                                <div class="alert alert-danger alert-dismissable" id="tipo-pagamento-boleto-aviso" style="font-weight: 400;font-size: 13px; margin: 0;">
                                                     <b>Atenção.</b>
                                                     Notamos que não temos seu e-mail cadastrado em nosso sistema.<br>
                                                     Antes de registrar informe seu e-mail. <br>
                                                     <a href="{base_url}FrontOffice/Perfil/Email" title="">Clique aqui para cadastrar seu e-mail</a>
                                                </div>
                                                <?php endif; ?>

                                               
                                                <?php if($QtdeEndereco == 0): ?>
                                                <div class="alert alert-danger alert-dismissable" id="tipo-pagamento-boleto-aviso" style="font-weight: 400;font-size: 13px; margin: 0;">
                                                     <b>Atenção.</b>
                                                     Notamos que não temos seu endereço cadastrado em nosso sistema.<br>
                                                     Antes de registrar informe seu endereço. <br>
                                                     É necessário essas informações por questões de políticas de segurança da intermediadora do cartão de crédito. <br>
                                                     <a href="{base_url}FrontOffice/Perfil/Endereco" title="">Clique aqui para cadastrar seu endereço</a>
                                                </div>
                                                <?php endif; ?>




                                                <?php if(empty($RegistrosAtivos) && ( $QtdeTelefone > 0 || $QtdeEmail > 0 || $QtdeEndereco > 0 ) ): ?>
                                                <div class="alert alert-danger alert-dismissable" id="tipo-pagamento-boleto-aviso" style="font-weight: 400;font-size: 13px; margin: 0;">
                                                     <b>Atenção.</b>
                                                     Seu registro junto a FHBr está vencido ou é inexistente. <br>
                                                     Faça o seu registre e esteja habilitado a participar dos eventos. <br><a href="#" class="btn-novo-registro" title="">Clique aqui para registrar</a>
                                                </div>
                                                <?php endif; ?>


                                            <?php if($QtdeTelefone > 0 && $QtdeEmail > 0 && $QtdeEndereco > 0): ?>
                                                <?php if(!empty($RegistrosAtivos)): ?>
                                                <div class="table-scrollable table-scrollable-borderless" style="float: left;width: 100%;margin: 0px !important;">
                                                    <table class="table table-hover table-light">
                                                        <thead>
                                                            <tr class="uppercase">
                                                                <th> Registro </th>
                                                                <th class="text-center"> Financeiro </th>
                                                                <th class="text-center"> Status </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            
                                                            <?php foreach ($RegistrosAtivos as $key => $value): ?>
                                                            <tr>
                                                                <td>
                                                                    <b class="font-green-sharp">
                                                                        <i class="fa fa-check text-green"></i>
                                                                        <?php echo $this->model_crud->get_rowSpecific('tb_evento_modalidade', 'evm_id', $value->fk_mod_id, 1, 'evm_modalidade'); ?>
                                                                    </b>
                                                                    <br>
                                                                    <small>
                                                                        <?php echo $this->model_crud->get_rowSpecific('tb_registro_tipo', 'ret_id', $value->fk_tip_id, 1, 'ret_tipo'); ?>                                                            
                                                                    </small>
                                                                </td>
                                                                <td class="text-center">
                                                                    
                                                                    <?php if($value->fk_sta2_id == '200'): ?>
                                                                    <a href="<?php echo $value->fin_boleto; ?>" class="btn btn-circle btn-sm btn-warning bold btn-gerenciar-conjunto tooltips text-right" data-original-title="Clique aqui para acessar a segunda via do boleto." target="_blank">

                                                                        <i class="fa fa-clock-o"></i>
                                                                        Aguardando pagamento <br>
                                                                        <small>clique aqui para acessar 2ª via boleto</small>
                                                                    </a>
                                                                    <?php endif; ?>
                                                                    
                                                                    <?php if($value->fk_sta2_id == '201'): ?>
                                                                    <a href="{base_url}financeiro/FrontOffice/Fatura/Fatura/<?php echo $value->fin_transacao; ?>" class="btn btn-circle btn-sm btn-success bold btn-gerenciar-conjunto tooltips text-right" data-original-title="Clique aqui para emitir o comprovante de pagamento.">
                                                                        <i class="fa fa-check"></i>
                                                                        Pago
                                                                    </a>
                                                                    <?php endif; ?>

                                                                </td>
                                                                <td> 
                                                                    <?php if($value->fk_sta_id == '100'): ?>
                                                                    <span class="label label-sm tooltips label-default bold">
                                                                        <i class="fa fa-close" aria-hidden="true"></i>
                                                                        Inativo
                                                                    </span>
                                                                    <?php endif; ?>
                                                                    <?php if($value->fk_sta_id == '101'): ?>
                                                                    <span class="label label-sm tooltips label-success bold">
                                                                        <i class="fa fa-check" aria-hidden="true"></i>
                                                                        Ativo
                                                                    </span>
                                                                    <?php endif; ?>
                                                                </td>
                                                            </tr>
                                                            <?php endforeach; ?>
                                                       

                                                        </tbody>
                                                    </table>
                                                </div>  
                                                <?php endif; ?>
                                            <?php endif; ?>

                                        </div>
                                    </div>
                                    <?php endif; ?>
                                    <!-- /registro-ativos -->


                                    <!--    
                                        FORMULARIO - REGISTRO
                                    ===================================================== -->
                                    <?php if($QtdeTelefone > 0 && $QtdeEmail > 0 && $QtdeEndereco > 0 && $AtletaFhbr && !$PagamentoPendente): ?>
                                    <div class="portlet light bordered" id="portlet-formulario-registro" style="display: none;">
                                        <div class="portlet-title">
                                            <div class="caption font-green-sharp">
                                                <i class="icon-speech font-green-sharp"></i>
                                                <span class="caption-subject bold uppercase"> Formulário</span>
                                                <span class="caption-helper">Novo Registro - Preencha o formulário completo.</span>
                                            </div>
                                        </div>
                                        <div class="portlet-body">

                                            <div id="formulario-novo-registro">

                                                    <div class="portlet-body form">
                                                        <form class="form-horizontal" action="#" id="form-registro" method="POST">

                                                                <div class="alert alert-danger margin-top-10" style="display: none;">
                                                                    <strong>Atenção!</strong>
                                                                    Preencha todos os campos corretamente para avançar no registro de atleta.
                                                                </div>


                                                                <!-- MODALIDADE -->
                                                                <div class="row" id="formulario-registro-modalidade">
                                                                    <h4 class="bold titulo-secao" style="margin-top: 0px;"><span class="badge badge-primary">1</span> Modalidade</h4>
                                                                    <div class="form-group">
                                                                        <?php foreach ($ModalidadesRegistro as $key => $value): ?>
                                                                        <div class="modalidade-item">
                                                                            <input type="radio" value="<?php echo $value->evm_id; ?>" name="modalidade" id="modalidade-<?php echo $value->evm_id; ?>" data-title="<?php echo $value->evm_modalidade; ?>"> 
                                                                            <label class="" style="font-weight: 400;" for="modalidade-<?php echo $value->evm_id; ?>"> <?php echo $value->evm_modalidade; ?> </label>
                                                                        </div>
                                                                        <?php endforeach; ?>
                                                                        <div id="form-modalidade-erro"></div>
                                                                    </div>
                                                                </div>


                                                                <!-- TIPO DE REGISTRO -->
                                                                <div class="row" id="formulario-registro-tipo-registro">

                                                                    <h4 class="bold titulo-secao" style="margin-top: 0px;"> <span class="badge badge-primary">2</span> Tipo de Registro</h4>
                                                                   
                                                                    <div class="alert alert-warning alert-dismissable aviso-dependencia" style="font-weight: 400; font-size: 13px;"> Selecione uma modalidade. </div>

                                                                    <div class="form-group" style="display: none;">

                                                                        <div class="radio-tipo-registro">

                                                                            <div class="tipo-registro-item" id="tipo-registro-item-1" style="display: none;">
                                                                                <input type="radio" value="1" name="tipo-registro" id="tipo-registro-1" data-title="Anual"> 
                                                                                <label class="" for="tipo-registro-1">
                                                                                    Anual <br>
                                                                                    <small>Registro para participação em todas as provas estaduais e nacionais no ano em curso.</small>
                                                                                </label> 
                                                                            </div>

                                                                            <div class="tipo-registro-item" id="tipo-registro-item-2" style="display: none;">
                                                                                <input type="radio" value="2" name="tipo-registro" id="tipo-registro-2" data-title="Copa"> 
                                                                                <label class="" for="tipo-registro-2">
                                                                                    Copa <br>
                                                                                    <small>Registro para participação em provas tipo Copa no ano em curso.</small>
                                                                                </label> 
                                                                            </div>

                                                                            <div class="tipo-registro-item" id="tipo-registro-item-3" style="display: none;">
                                                                                <input type="radio" value="3" name="tipo-registro" id="tipo-registro-3" data-title="Participação Única"> 
                                                                                <label class="" for="tipo-registro-3">
                                                                                    Participação Única <br>
                                                                                    <small>Registro para participação em uma única prova no ano em curso.</small>
                                                                                </label> 
                                                                            </div>

                                                                            <div class="loading-tipo-registro" style="display: none;">
                                                                                <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Aguarde... carregando.
                                                                            </div>

                                                                        </div>
                                                                        <div id="form-tipo-registro-erro"></div>

                                                                    </div>
                                                                </div>


                                                                <!-- VALORES -->
                                                                <div class="row" id="formulario-registro-valores">

                                                                    <h4 class="bold titulo-secao"> <span class="badge badge-primary">3</span> Valores</h4>

                                                                    <div class="alert alert-warning alert-dismissable aviso-dependencia" style="font-weight: 400;font-size: 13px;"> Selecione uma modalidade e um tipo de registro para continuar. </div>

                                                                    <div class="portlet-body" id="portlet-valores" style="display: none;margin-top: 0px;padding-top: 0px;">
                                                                        <div class="table-scrollable">
                                                                            <table class="table table-hover table-light">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td colspan="2" class="bold" style="background: #f9fafb!important"> Taxas </td>
                                                                                    </tr>
                                                                                    <tr id="tablerow-taxa-item">
                                                                                        <td class="first">
                                                                                            <div class="titulo font-blue-sharp"></div>
                                                                                            <small class="descricao"><i class="fa fa-info-circle" aria-hidden="true" style="color: #337ab7;"></i> <span></span></small>
                                                                                        </td>
                                                                                        <td class="second"> </td>
                                                                                    </tr>
                                                                                    <tr id="tablerow-taxa-extra">
                                                                                        <td class="first">
                                                                                            <div class="titulo font-blue-sharp"></div>
                                                                                            <small class="descricao"><i class="fa fa-info-circle" aria-hidden="true" style="color: #337ab7;"></i> <span></span></small>
                                                                                        </td>
                                                                                        <td class="second">  </td>
                                                                                    </tr>

                                                                                    <tr id="tablerow-desconto">
                                                                                        <td colspan="2" class="bold" style="background: #f9fafb!important"> Descontos </td>
                                                                                    </tr>
                                                                                    <tr id="tablerow-desconto-item">
                                                                                        <td class="first">
                                                                                            <div class="titulo font-blue-sharp"></div>
                                                                                            <small class="descricao"><i class="fa fa-info-circle" aria-hidden="true" style="color: #337ab7;"></i> <span></span></small>
                                                                                        </td>
                                                                                        <td class="second">  </td>
                                                                                    </tr>

                                                                                    <tr>
                                                                                        <td colspan="2" class="bold" style="background: #f9fafb!important"> Total </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2" style="color: #53d232; font-size: 20px;">
                                                                                            <input type="hidden" id="pagarme-preco" value="" />
                                                                                            <small style="text-decoration: line-through;" id="preco-total-promocional"></small>
                                                                                            <span class="bold" id="preco-total"></span>
                                                                                        </td>
                                                                                    </tr>


                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>  

                                                                <!-- PAGAMENTO -->
                                                                <div class="row" id="formulario-registro-pagamento">

                                                                    <h4 class="bold titulo-secao"> <span class="badge badge-primary">4</span> Pagamento</h4>
                                                                    <div class="alert alert-warning alert-dismissable aviso-dependencia" style="font-weight: 400;font-size: 13px;"> Selecione uma modalidade e um tipo de registro para continaur. </div>

                                                                    <div class="form-group" id="form-groupo-tipo-pagamento" style="display: none;">
                                                                        <div class="mt-radio-list">

                                                                            <label class="mt-radio tipo-pagamento-item">
                                                                                <input type="radio" name="tipo-pagamento" id="tipo-pagamento-boleto" value="1">
                                                                                    <i class="fa fa-barcode" aria-hidden="true"></i>
                                                                                    Boleto 
                                                                                    <small style="display: block;font-weight: 500">Pague via boleto bancário</small>
                                                                                <span></span>
                                                                                
                                                                                <div id="tipo-pagamento-boleto-content" style="display: none">
                                                                                    <hr>
                                                                                    <div class="alert alert-warning alert-dismissable" id="tipo-pagamento-boleto-aviso" style="font-weight: 400;font-size: 13px; margin: 0;">
                                                                                         Após a finalização do registro será exibido um botão para impressão do boleto bancário. <br> * <small>Seu registro só terá validade após compensação do boleto bancário.</small>
                                                                                    </div>
                                                                                </div>

                                                                            </label>
                                                                            <label class="mt-radio tipo-pagamento-item">
                                                                                <input type="radio" name="tipo-pagamento" id="tipo-pagamento-cartao-credito" value="2">
                                                                                    <i class="fa fa-credit-card" aria-hidden="true"></i>
                                                                                    Cartão de Crédito
                                                                                    <small style="display: block;font-weight: 500">Parcele em até 6x sem juros</small>
                                                                                <span></span>

                                                                            </label>
                                                                        </div>

                                                                        <div id="form-tipo-pagamento-erro"></div>

                                                                    </div>

                                                                </div>






                                                                <div class="form-actions fluid">
                                                                    <div class="row">
                                                                        <div class="col-md-offset-3 col-md-9">
                                                                            <div class="btn green" id="form-registro-submit">
                                                                                    Finalizar Registro <i class="fa fa-caret-right"></i>
                                                                                    <small></small>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            <input type="hidden" name="tipo-competidor" value="1" />
                                                            <input type="hidden" name="sigepe-transacao" value="" />
                                                            <input type="hidden" name="sigepe-registro" value="" />
                                                            <input type="hidden" name="sigepe-taxa" value="" />


                                                        </form>
                                                    </div>

                                            </div>


                                        </div>
                                        <!-- /content-body -->
                                    </div>
                                    <!-- /portlet-formulario-registro -->
                                    <?php endif; ?>





                                    <!--    
                                        VINCULOS
                                    ===================================================== -->
                                    <div class="portlet light" style="padding: 0;" id="portlet-vinculos">

                                        <div class="portlet-title">
                                            <div class="caption font-green-sharp">
                                                <span class="caption-subject font-green bold">Vínculos do Atleta</span>
                                                <span class="caption-helper">Confira os vínculos que você está associado</span>
                                            </div>
                                            <div class="actions">
                                                <a href="javascript:;" class="btn btn-circle btn-action-section" id="btn-detalhes-vinculo">
                                                    <i class="fa fa-plus"></i> Mais Detalhes
                                                </a>
                                            </div>
                                        </div>
                                        <div class="portlet-body" style="padding: 0px;">

                                            <div class="table-scrollable table-scrollable-borderless" style="margin: 0px !important;">
                                                <table class="table table-hover table-light">
                                                    <thead>
                                                        <tr class="uppercase">
                                                            <th> Empresa </th>
                                                            <th> Vínculo </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <!-- CONFEDERACAO -->
                                                        <?php if(empty($ConfederacaoAtual)): ?>
                                                        <tr>
                                                            <td>
                                                                <b>Confederação</b> <a href="#" title="" class="trocar-vinculo"> <i class="fa fa-plus"></i>Informar Confederação </a><br><br>
                                                                <small>
                                                                    [ Vínculo Inexistente ]
                                                                </small>
                                                            </td>
                                                            <td> 
                                                                <span class="label label-sm tooltips label-danger bold label-vinculo-atual">
                                                                    <i class="fa fa-close" aria-hidden="true"></i>
                                                                    Atleta não possui vínculo com nenhuma Confederação.
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <?php endif; ?>   
                                                        <?php if(!empty($ConfederacaoAtual)): ?>
                                                        <tr>
                                                            <td>
                                                                <b>Confederação</b>  <br>
                                                                <small>
                                                                    <?php
                                                                        echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $ConfederacaoAtual[0]->fk_pes2_id, 1, 'pes_nome_razao_social');
                                                                    ?>
                                                                </small>
                                                            </td>
                                                            <td> 
                                                                <span class="btn btn-circle btn-xs blue btn btn-outline sbold <?php echo GetBootstrapColor($ConfederacaoAtual[0]->fk_sta_id); ?> bold label-vinculo-atual" data-original-title="<?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $ConfederacaoAtual[0]->fk_sta_id, 1, 'sta_status'); ?>" >
                                                                    <i class="fa <?php echo GetIconVinculo($ConfederacaoAtual[0]->fk_sta_id); ?>" aria-hidden="true"></i>
                                                                    <?php
                                                                        echo GetStatusVinculo($ConfederacaoAtual[0]->fk_sta_id);
                                                                    ?>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <?php endif; ?>   


                                                        <!-- FEDERACAO -->
                                                        <?php if(empty($FederacaoAtual)): ?>
                                                        <tr>
                                                            <td>
                                                                <b>Federação</b> <a href="#" title="" class="trocar-vinculo"> <i class="fa fa-plus"></i> Informar Federação </a> <br>
                                                                <small>
                                                                    [ Vínculo Inexistente ]
                                                                </small>
                                                            </td>
                                                            <td> 
                                                                <span class="label label-sm tooltips label-danger bold label-vinculo-atual">
                                                                    <i class="fa fa-close" aria-hidden="true"></i>
                                                                    Atleta não possui vínculo com nenhuma Federação
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <?php endif; ?>            
                                                        
                                                        <?php if(!empty($FederacaoAtual)): ?>
                                                        <tr>
                                                            <td>
                                                                <b>Federação</b> <a href="#" title="" class="trocar-vinculo" id="btn-trocar-federacao"> <i class="fa fa-undo"></i> Trocar Federação </a><br>
                                                                <small>
                                                                    <?php
                                                                        echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $FederacaoAtual[0]->fk_pes2_id, 1, 'pes_nome_razao_social');
                                                                    ?>
                                                                </small>
                                                            </td>
                                                            <td> 
                                                                <span class="btn btn-circle btn-xs  btn  btn-outline sbold <?php echo GetBootstrapColor($FederacaoAtual[0]->fk_sta_id); ?> label-vinculo-atual" data-original-title="<?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $FederacaoAtual[0]->fk_sta_id, 1, 'sta_status'); ?>" >
                                                                    <i class="fa <?php echo GetIconVinculo($FederacaoAtual[0]->fk_sta_id); ?>" aria-hidden="true"></i>
                                                                    <?php
                                                                        echo GetStatusVinculo($FederacaoAtual[0]->fk_sta_id);
                                                                    ?>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <?php endif; ?>
                                                        
                                                        <tr id="tablerow-trocar-federacao" class="tablerow-trocar-empresa" style="display:none;background: #f6f6f6; border-right: 0; border-left: 0;">
                                                            <td colspan="2" style="padding-top: 25px; padding-bottom: 25px;">
                                                                    
                                                                <form id="form-trocar-federacao" method="post">
                                                                    
                                                                    <h4>Formulario de Troca de Federaçao</h4>
                                                                        
                                                                    <?php if($AtletaFhbr): ?>
                                                                    <div class="alert alert-warning" style="margin-bottom: 0;">
                                                                        <strong>Atenção:</strong>
                                                                        Atualmente você está vinculado a <b>FHBr - Federação Hípica de Brasília</b>. A transferência de vínculo para outra federação irá cancelar qualquer registro que tenha em exercício para esse ano. Tambem anula qualquer vinculo que tenha com alguma entidade / escola equestre. Não haverá devoluções de valores. Certifique-se que esteja fazendo o procedimento correto.
                                                                    </div>
                                                                    <?php endif; ?>
                                                                        
                                                                    <?php if(!$AtletaFhbr): ?>
                                                                    <div class="alert alert-warning" style="margin-bottom: 0;">
                                                                        <strong>Atenção:</strong>
                                                                        Esse procedimento remove seu vínculo com a federação atual. Processo irreversível. 
                                                                    </div>
                                                                    <?php endif; ?>
                                                                    
                                                                    <!-- Federacao Atual -->
                                                                    <div class="form-group" style="margin-top: 30px; margin-bottom: 0px; float: left; width: 100%;">
                                                                        <label class="control-label col-md-3"> Federaçao Atual
                                                                        </label>
                                                                        <div class="col-md-8">
                                                                            <p class="form-control-static" style="padding-top: 3px;min-height: auto;">
                                                                                <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $FederacaoAtual[0]->fk_pes2_id, 1, 'pes_nome_razao_social'); ?>
                                                                            </p>
                                                                        </div>
                                                                    </div>

                                                                    
                                                                    <!-- Federacao -->
                                                                    <div class="form-group" style="margin-top: 10px; float: left; width: 100%;"  id="form-group-federacao">
                                                                        <label class="control-label col-md-3"> Nova Federação
                                                                        <span class="required" aria-required="true"> * </span>
                                                                        </label>
                                                                        <div class="col-md-8">
                                                                            <select class="form-control" name="federacao" id="federacao">
                                                                                <option value="">- Seleciona sua nova Federação -</option>
                                                                                <?php foreach ($DatasetFederacao as $key => $value): ?>
                                                                                <option value="<?php echo $value->pes_id; ?>"><?php echo $value->pes_nome_razao_social; ?></option>
                                                                                <?php endforeach; ?>
                                                                            </select>
                                                                            <div id="form-federacao-erro"></div>
                                                                        </div>
                                                                    </div>

                                                                                                                                        
                                                                    <!-- Entidade Equestre -->
                                                                    <div class="form-group" id="form-group-federacao-entidade-equestre" style="display:none;margin-bottom:10px;float:left;width:100%;">
                                                                        <label class="control-label col-md-3"> Entidade Equestre
                                                                        <span class="required" aria-required="true"> * </span>
                                                                        </label>
                                                                        <div class="col-md-8">
                                                                            <select class="form-control" name="federacao-entidade-equestre" id="federacao-entidade-equestre">
                                                                                <option value="">- Seleciona sua entidade equestre -</option>
                                                                                <?php foreach ($DatasetFederacaoEntidadeFiliada as $key => $value): ?>
                                                                                <option value="<?php echo $value->pes_id; ?>"><?php echo $value->pes_nome_razao_social; ?></option>
                                                                                <?php endforeach; ?>
                                                                            </select>                                    
                                                                            <div id="form-federacao-entidade-equestre-erro"></div>
                                                                        </div>
                                                                    </div>

                                                                                                                        
                                                                    <!-- Acoes -->
                                                                    <div class="form-group" style="float: left;width: 100%;margin-top: 0px;">
                                                                        <label class="control-label col-md-3"></label>
                                                                        <div class="col-md-8">
                                                                            <a href="#" title="" class="btn btn-submit btn-action-section" style="float: left; padding: 6px 20px;"><i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar Transferência</a>
                                                                            <a href="#" title="" class="btn btn-cancelar btn-action-section" style="float: left; padding: 6px 20px; background: #ccc; border: 1px solid #bbb; margin-left: 12px; font-weight: 500; color: #666;"><i class="fa fa-close" aria-hidden="true"></i> Cancelar Transferencia</a>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    
                                                                </form>
                                                                
                                                            </td>
                                                        </tr>


                                                        <!-- ENTIDADE EQUESTRE -->
                                                        <?php if($AtletaFhbr): ?>

                                                            <?php if(empty($EntidadeAtual)): ?>
                                                            <tr>
                                                                <td>
                                                                    <b>Entidade Equestre</b> <a href="#" title="" class="trocar-vinculo"> <i class="fa fa-plus"></i> Informar Entidade </a><br>
                                                                    <small>
                                                                        [ Vínculo Inexistente ]
                                                                    </small>
                                                                </td>
                                                                <td> 
                                                                    <span class="label label-sm tooltips label-danger bold label-vinculo-atual">
                                                                        <i class="fa fa-close" aria-hidden="true"></i>
                                                                        Atleta não possui vínculo com nenhuma Entidade Equestre
                                                                    </span>
                                                                    <a href="#" title="" style="display: block;">
                                                                        <i class="fa fa-plus"></i>
                                                                        Inserir Entidade
                                                                    </a>    
                                                                </td>
                                                            </tr>
                                                            <?php endif; ?>

                                                            <?php if(!empty($EntidadeAtual)): ?>
                                                            <tr>
                                                                <td>
                                                                    <b>Entidade Equestre</b> <a href="#" title="" class="trocar-vinculo" id="btn-trocar-entidade"> <i class="fa fa-undo"></i> Trocar Entidade </a><br>
                                                                    <small>
                                                                        <?php
                                                                            echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $EntidadeAtual[0]->fk_pes2_id, 1, 'pes_nome_razao_social');
                                                                        ?>
                                                                    </small>
                                                                </td>
                                                                <td> 
                                                                    <span class="btn btn-circle btn-xs btn btn-outline sbold <?php echo GetBootstrapColor($EntidadeAtual[0]->fk_sta_id); ?> bold label-vinculo-atual" data-original-title="<?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $EntidadeAtual[0]->fk_sta_id, 1, 'sta_status'); ?>" >
                                                                        <i class="fa <?php echo GetIconVinculo($EntidadeAtual[0]->fk_sta_id); ?>" aria-hidden="true"></i>
                                                                        <?php
                                                                            echo GetStatusVinculo($EntidadeAtual[0]->fk_sta_id);
                                                                        ?>
                                                                    </span>
                                                                </td>
                                                            </tr>



                                                            <tr id="tablerow-trocar-entidade" class="tablerow-trocar-empresa" style="display:none;background: #f6f6f6; border-right: 0; border-left: 0;">
                                                                <td colspan="2" style="padding-top: 25px; padding-bottom: 25px;">

                                                                    <form id="form-trocar-entidade" method="post">

                                                                        <h4>Formulario de Troca de Entidade</h4>

                                                                        <div class="alert alert-warning" style="margin-bottom: 0;">
                                                                            <strong>Atenção:</strong>
                                                                            Esse procedimento remove seu vínculo com a entidade equestre atual. Processo irreversível. Tenha certeza do que está fazendo.
                                                                        </div>

                                                                        <!-- Entidade Equestre Atual -->
                                                                        <div class="form-group" style="margin-top: 30px; margin-bottom: 0px; float: left; width: 100%;">
                                                                            <label class="control-label col-md-4"> Entidade Atual
                                                                            </label>
                                                                            <div class="col-md-7">
                                                                                <p class="form-control-static" style="padding-top: 3px;min-height: auto;">
                                                                                    <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $EntidadeAtual[0]->fk_pes2_id, 1, 'pes_nome_razao_social'); ?>
                                                                                </p>
                                                                            </div>
                                                                        </div>


                                                                        <!-- Entidade Equestre -->
                                                                        <div class="form-group" id="form-group-entidade-equestre" style="margin-bottom:10px;float:left;width:100%;">
                                                                            <label class="control-label col-md-4"> Nova Entidade 
                                                                            <span class="required" aria-required="true"> * </span>
                                                                            </label>
                                                                            <div class="col-md-7">
                                                                                <select class="form-control" name="entidade-equestre" id="entidade-equestre">
                                                                                    <option value="">- Seleciona sua entidade equestre -</option>
                                                                                    <?php foreach ($DatasetEntidadeFiliada as $key => $value): ?>
                                                                                    <option value="<?php echo $value->pes_id; ?>"><?php echo $value->pes_nome_razao_social; ?></option>
                                                                                    <?php endforeach; ?>
                                                                                </select>                                    
                                                                                <div id="form-entidade-equestre-erro"></div>
                                                                            </div>
                                                                        </div>


                                                                        <!-- Acoes -->
                                                                        <div class="form-group" style="float: left;width: 100%;margin-top: 0px;">
                                                                            <label class="control-label col-md-4"></label>
                                                                            <div class="col-md-7">
                                                                                <a href="#" title="" class="btn btn-submit btn-action-section" style="float: left; padding: 6px 10px;"><i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar Transferência</a>
                                                                                <a href="#" title="" class="btn btn-cancelar btn-action-section" style="float: left; padding: 6px 10px; background: #ccc; border: 1px solid #bbb; margin-left: 12px; font-weight: 500; color: #666;"><i class="fa fa-close" aria-hidden="true"></i> Cancelar Transferencia</a>
                                                                            </div>
                                                                        </div>


                                                                    </form>

                                                                </td>
                                                            </tr>

                                                        
                                                        
                                                        
                                                        
                                                            <?php endif; ?>
                                                        <?php endif; // fim $AtletaFhbr ?> 


                                                    </tbody>
                                                </table>
                                            </div>  

                                        </div>

                                    </div>
                                    <!-- /vinculos -->



                                    <!--    
                                        DADOS DO ATLETA
                                    ===================================================== -->
                                    <form method="post" action="#" class="form-view" id="form-dados-atleta">

                                        <div class="portlet light" style="padding: 0;">
                                            <div class="portlet-title">
                                                <div class="caption font-green-sharp">
                                                    <span class="caption-subject">Dados do Atleta</span>
                                                    <span class="caption-helper">Visualize suas informações de atleta junto a federação</span>
                                                </div>
                                                <div class="actions">
                                                    <a href="javascript:;" class="btn btn-circle btn-edit btn-action-section btn-editar-informacoes">
                                                        <i class="fa fa-pencil"></i> Editar Informações
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body" style="padding-top: 0px;margin-top: 0px;">

                                                <div class="table-scrollable table-scrollable-borderless" style="margin: 0 !important;">

                                                        <table class="table table-hover table-light">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="bold text-right col-sm-3"> Registro Atleta - FHBr </td>
                                                                    <td> <?php echo $PessoaFisicaAtletaId ?> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="bold text-right"> Registro - CBH </td>
                                                                    <td> <?php echo (!is_null($RegistroCbh)) ? $RegistroCbh : 'N/I' ?> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="bold text-right"> Registro - FEI </td>
                                                                    <td><?php echo (!is_null($RegistroFei)) ? $RegistroFei : 'N/I' ?> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="bold text-right"> Nome de Competição </td>
                                                                    <td>
                                                                        <div class="form-control-static" id="fcs-nome-competicao"> <?php echo (!is_null($NomeDeCompeticao)) ? $NomeDeCompeticao : 'N/I'; ?> </div>
                                                                        <input type="text" placeholder="Como quer ser chamado?" value="<?php echo (!is_null($NomeDeCompeticao)) ? $NomeDeCompeticao : ''; ?>" name="nome-competicao" id="nome-competicao" class="form-control" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="bold text-right">
                                                                        Categorias
                                                                        <a href="javascript:;" class="popovers" data-container="body" data-trigger="hover" data-content="Visualize as categorias que você está apto a participar nos eventos." data-original-title="Categorias" data-placement="top"> <i class="fa fa-info-circle" aria-hidden="true"></i> </a>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>

                                                        <div class="form-actions">
                                                            <div class="col-sm-3"></div>
                                                            <div class="col-sm-9 text-left">
                                                                <a href="#" title="" class="btn blue btn-submit" style="padding: 10px 40px;"><i class="fa fa-floppy-o" aria-hidden="true"></i> SALVAR</a>
                                                            </div>
                                                        </div>

                                                </div>

                                            </div>

                                        </div>

                                    </form>
                                    <!-- /dados-do-atleta -->



                                </div>
                                <!-- /tab-informacoes-gerais -->



                                <!--
                                    ****************************************
                                        TAB-VINCULO-REGISTRO
                                    ****************************************
                                -->
                                <div class="tab-pane" id="tab-vinculo-registro">


                                    <h3> <i class="fa fa-calendar" aria-hidden="true"></i> Histórico Vínculos </h3>

                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs ">
                                            <li class="active">
                                                <a href="#tab_atleta_vinculos_confederao" data-toggle="tab" aria-expanded="true"> Confederação </a>
                                            </li>
                                            <li class="">
                                                <a href="#tab_atleta_vinculos_federacao" data-toggle="tab" aria-expanded="false"> Federação </a>
                                            </li>
                                            <li>
                                                <a href="#tab_atleta_vinculos_entidade" data-toggle="tab"> Entidade Equestre </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content" style="padding-top: 0px;">    

                                            <!-- Confederacao -->
                                            <div class="tab-pane active" id="tab_atleta_vinculos_confederao">

                                                <?php if(empty($Confederacao)): ?>
                                                <div class="alert alert-danger margin-top-10">
                                                    <strong>Atenção!</strong>
                                                    Esse atleta não possui vínculo com nenhuma Confederação.
                                                </div>
                                                <?php endif; ?>
                                                

                                                <?php if(!empty($Confederacao)): ?>
                                                <div class="table-scrollable">
                                                    <table class="table table-striped table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th> # </th>
                                                                <th> Confederação </th>
                                                                <th> Início Vínculo </th>
                                                                <th> Fim Vínculo </th>
                                                                <th> Status </th>
                                                                <th> * </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                                foreach ($Confederacao as $key => $value):
                                                                    $NomeRazaoSocial    =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes2_id, 1, 'pes_nome_razao_social');
                                                                        
                                                                    // 5 Atleta vínculo cancelado pela Confederação.  |  49 Vínculo cancelado pelo próprio usuário. 
                                                                    ($value->fk_sta_id == '5' || $value->fk_sta_id == '49') ? $DataFimVinculo = true : $DataFimVinculo = false;
                                                                
                                                            ?>
                                                            <tr>
                                                                <td> <?php echo $value->vin_id; ?> </td>
                                                                <td> <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes2_id, 1, 'pes_nome_razao_social'); ?> </td>
                                                                <td> <?php echo $this->my_data->ConverterData($value->criado, 'ISO', 'PT-BR'); ?> </td>
                                                                <td> <?php echo ($DataFimVinculo) ? $this->my_data->ConverterData($value->modificado, 'ISO', 'PT-BR') : '-' ; ?> </td>
                                                                <td>                                   
                                                                    <span class="label label-sm tooltips label-<?php echo GetBootstrapColor($value->fk_sta_id); ?>" data-original-title="<?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $value->fk_sta_id, 1, 'sta_status'); ?>" >
                                                                        <?php
                                                                            echo GetStatusVinculo($value->fk_sta_id);
                                                                        ?>
                                                                    </span>
                                                                </td>
                                                                <td>
                                                                    <a href="#" title="" class="vinculo-detalhe-link" data-id="<?php echo $value->vin_id; ?>" data-nome-empresa="<?php echo $NomeRazaoSocial; ?>" data-id-empresa="<?php echo $value->fk_pes2_id; ?>">
                                                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                        Histórico
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <?php endif; ?>


                                            </div>
                                            <!-- /Confederacao -->


                                            <!-- Federacao -->
                                            <div class="tab-pane" id="tab_atleta_vinculos_federacao">


                                                <?php if(empty($Federacao)): ?>
                                                <div class="alert alert-danger margin-top-10">
                                                    <strong>Atenção!</strong>
                                                    Esse atleta não possui vínculo com nenhuma Federação.
                                                </div>
                                                <?php endif; ?>


                                                <?php if(!empty($Federacao)): ?>
                                                <div class="table-scrollable">
                                                    <table class="table table-striped table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th> # </th>
                                                                <th> Federação </th>
                                                                <th> Início Vínculo </th>
                                                                <th> Fim Vínculo </th>
                                                                <th> Status </th>
                                                                <th>  </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                                foreach ($Federacao as $key => $value):
                                                                    $NomeRazaoSocial    =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes2_id, 1, 'pes_nome_razao_social');
                                                                    
                                                                    // 14  Atleta vínculo cancelado pela Federação.  |  49 Vínculo cancelado pelo próprio usuário. 
                                                                    ($value->fk_sta_id == '14' || $value->fk_sta_id == '49') ? $DataFimVinculo = true : $DataFimVinculo = false;
                                                            ?>
                                                            <tr>
                                                                <td> <?php echo $value->vin_id; ?> </td>
                                                                <td> <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes2_id, 1, 'pes_nome_razao_social'); ?> </td>
                                                                <td> <?php echo $this->my_data->ConverterData($value->criado, 'ISO', 'PT-BR'); ?> </td>
                                                                <td> <?php echo ($DataFimVinculo) ? $this->my_data->ConverterData($value->modificado, 'ISO', 'PT-BR') : '-' ; ?> </td>
                                                                <td> 

                                                                    <span class="btn btn-circle btn-xs  btn  btn-outline sbold <?php echo GetBootstrapColor($value->fk_sta_id); ?> label-vinculo-atual" data-original-title="<?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $value->fk_sta_id, 1, 'sta_status'); ?>" >
                                                                        <i class="fa <?php echo GetIconVinculo($value->fk_sta_id); ?>" aria-hidden="true"></i>
                                                                        <?php
                                                                            echo GetStatusVinculo($value->fk_sta_id);
                                                                        ?>
                                                                    </span>

                                                                </td>
                                                                <td>
                                                                    <a href="#" title="" class="vinculo-detalhe-link" data-id="<?php echo $value->vin_id; ?>" data-nome-empresa="<?php echo $NomeRazaoSocial; ?>" data-id-empresa="<?php echo $value->fk_pes2_id; ?>">
                                                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <?php endif; ?>

                                            </div>
                                            <!-- /Federacao -->

                                            <!-- Entidade Equestre -->
                                            <div class="tab-pane" id="tab_atleta_vinculos_entidade">

                                                <?php if(empty($Entidade)): ?>
                                                <div class="alert alert-warning margin-top-10">
                                                    <strong>Atenção!</strong>
                                                    Esse atleta não possui vínculo com nenhuma Entidade Equestre.
                                                </div>
                                                <?php endif; ?>


                                                <?php if(!empty($Entidade)): ?>
                                                <div class="table-scrollable">
                                                    <table class="table table-striped table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th> # </th>
                                                                <th> Entidade </th>
                                                                <th> Início Vínculo </th>
                                                                <th> Fim Vínculo </th>
                                                                <th> Status </th>
                                                                <th> * </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            <?php
                                                                foreach ($Entidade as $key => $value):
                                                                    $NomeRazaoSocial    =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes2_id, 1, 'pes_nome_razao_social');
                                                            ?>
                                                            <tr>
                                                                <td> <?php echo $value->vin_id; ?> </td>
                                                                <td> <?php echo $NomeRazaoSocial; ?> </td>
                                                                <td> <?php echo $this->my_data->ConverterData($value->criado, 'ISO', 'PT-BR'); ?> </td>
                                                                <td> - </td>
                                                                <td>                                   
                                                                    <span class="label label-sm tooltips label-<?php echo GetBootstrapColor($value->fk_sta_id); ?>" data-original-title="<?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $value->fk_sta_id, 1, 'sta_status'); ?>" >
                                                                    <?php echo GetStatusVinculo($value->fk_sta_id); ?>
                                                                    </span>
                                                                </td>
                                                                <td>
                                                                    <a href="#" title="" class="vinculo-detalhe-link" data-id="<?php echo $value->vin_id; ?>" data-nome-empresa="<?php echo $NomeRazaoSocial; ?>" data-id-empresa="<?php echo $value->fk_pes2_id; ?>">
                                                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                        Histórico
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <?php endforeach; ?>


                                                        </tbody>
                                                    </table>
                                                </div>
                                                <?php endif; ?>

                                            </div>
                                            <!-- /Entidade Equestre -->

                                        </div>
                                    </div>
    

                                </div>
                                <!-- /tab-detalhes-vinculo-registro -->



                                <!--
                                    ****************************************
                                        TAB-HISTORICO-PARTICIPACOES
                                    ****************************************
                                -->
                                <div class="tab-pane" id="tab-historico-participacoes">


                                    <h3>
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                        Histórico de Participações em Eventos
                                    </h3>
                                    

                                    <div class="tabbable-custom" style="margin-top: 20px;">
                                        <ul class="nav nav-tabs ">
                                            <li class="active">
                                                <a href="#tab-registro-2018" data-toggle="tab"> 2018 </a>
                                            </li>
                                            <li>
                                                <a href="#tab-registro-2019" data-toggle="tab"> 2019 </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab-registro-2018">
                                                Histórico 2018
                                            </div>
                                            <div class="tab-pane" id="tab-registro-2019">
                                                Histórico 2019
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                        </div>



                </div>
            </div>
        </div>
    </div>
    <!-- END PROFILE CONTENT -->




    </div>  
    <!-- div aberta em sidebar.php ( /.col-sm-12 ) -->

</div>
<!-- div aberta em sidebar.php ( /.row ) -->









    <!-- 
    MODAL ( DETALHES DO VINCULO - CONF / FED / CLUBE )
    ==================================== -->
    <div class="modal fade bs-modal-lg" id="vinculo-detalhe" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title bold">CITTAC - Centro Integrado de Treinamento dos Trotadores de Águas Claras    </h4>
                    <div>
                        <small>
                            <b>ID Vínculo:</b> #<span id="vinculo-detalhe-id"></span> | 
                            <b>ID Empresa:</b> #<span id="vinculo-detalhe-id-empresa"></span>
                        </small>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="modal-body">

                        <div id="vinculo-detalhe-carregando" style="text-align: center; color: #008076; ">
                            <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
                            <h3 style="display: inline; ">Aguarde, carregando...</h3>
                        </div>

                        <div class="table-scrollable" id="vinculo-detalhe-tabela" style="display: none;">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th> # </th>
                                        <th> Autor </th>
                                        <th> Atualizado em </th>
                                        <th> Status </th>
                                        <th> Detalhes </th>
                                    </tr>
                                </thead>
                                <tbody>

                                

                                </tbody>
                            </table>
                        </div>
                        <!-- /table-scrollable -->

                    </div>                    


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn green" data-dismiss="modal">Fechar</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>





    <div id="layer-loading-registro" style="display: none;">
        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
        <span>Aguarde alguns instantes enquanto finalizamos o registro.</span>
    </div>
