<div class="container page-inscricao">

    <div class="row inscricao-titulo-secao">
      <div class="col-sm-12">
        <ul>
          <li class="numero-bloco">02</li>
          <!--<li class="titulo">SELECIONE <b>SÉRIE/PROVA</b></li>-->
          <li class="titulo">SELECIONE UMA <b>SÉRIE</b></li>
        </ul>
      </div>

    </div>

    <div class="row">
      <div class="col-sm-12">
        <hr class="separador-inscricao">
        <hr class="separador-inscricao">
        <hr class="separador-inscricao">
        <hr class="separador-inscricao">
        <hr class="separador-inscricao">
      </div>
    </div>


    <div class="row">
      <div class="col-sm-12 aviso-selecione-conjunto">
        <div class="alert alert-warning">
          <strong>Atenção!</strong> Selecione um conjunto para continuar a inscrição.
        </div>
      </div>
    </div>


    <!-- BOX-SERIE-PROVA -->
    <div id="box-serie-prova" style="display:none;">


      <div class="portlet light bordered">
          <div class="portlet-title">
              <div class="caption font-yellow-crusta">
                  <i class="icon-share font-yellow-crusta"></i>
                  <span class="caption-subject bold uppercase"> Série</span>
                  <span class="caption-helper">Inscrição por série</span>
              </div>
          </div>
          <div class="portlet-body">

            <div class="row" id="row-combobox-serie">
              <div class="col-sm-6" style="padding-right: 0px;">
                <div class="form-group">
                  <select class="form-control" id="combobox-serie">

                    <option value="" data-serie-id="undefined">- Selecione uma opção -</option>
                    <?php foreach ($SeriesDoEvento as $key => $value): ?>
                    <option value="<?php echo $value->ers_id; ?>" data-serie-id="<?php echo $value->ers_id; ?>">
                        <?php echo $value->ers_nome; ?> - R$ <?php echo $value->ers_valor_ate_inscricao_definitiva; ?>
                    </option>
                    <?php endforeach; ?>

                  </select>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <a href="javascript:;" class="btn yellow-crusta" id="btn-adicionar-serie">
                      <i class="fa fa-plus"></i>
                      Adicionar Série
                  </a>
                </div>
              </div>
            </div>
            <!-- /end-combobox-serie -->


            <div class="alert alert-success" id="aviso-limite-serie-inscricao" style="display: none;">
              <strong>Nota:</strong> Para esse evento é permitido apenas uma série por inscrição.
            </div>



            <div id="cabecalho-provas-da-serie" style="display:none;">
              <hr>
              <h4><b>Provas da série:</b> <span id="nome-serie-selecionada">{NomeSerie}</span></h4>
              <h5>Valor: <span style="text-decoration: line-through;">R$</span> <span id="serie-preco-cheio">0,00</span>  <b>R$ <span id="serie-preco-promocional">0,00</span></b> </h5>
              <a href="javascript:;" id="btn-remover-serie" class="btn btn-sm red" style="border-radius: 30px !important;"> <i class="fa fa-close"></i> Remover Série  </a>
            </div>

            <!-- Provas Vinculadas a serie -->
            <div class="table-scrollable" id="table-provas-da-serie" style="display:none;">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th> Dia </th>
                            <th> Nº Prova </th>
                            <th> Característica </th>
                            <th> Categorias da Prova </th>
                            <th> Pista </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                          foreach ($ProvasDoEvento as $key => $value):
                        ?>
                        <tr data-srp-id="<?php echo $value->srp_id; ?>" style="display: none;">
                            <td>
                                <?php echo $this->my_data->ConverterData($value->srp_dia, 'ISO', 'PT-BR'); ?>
                                <br>
                                <small>
                                    <?php echo $this->my_data->diasemana($value->srp_dia); ?>
                                    <br>
                                    às <?php echo $value->srp_hora; ?>
                                </small>
                            </td>
                            <td> Pr. <?php echo $value->srp_numero_prova; ?> </td>
                            <td>
                                <?php echo $this->model_crud->get_rowSpecific('tb_evento_serie_prova_caracteristica', 'spc_id', $value->fk_spc_id, 1, 'spc_caracteristica'); ?>
                                <br>
                                <small class="font-grey-cascade"><?php echo $this->model_crud->get_rowSpecific('tb_evento_serie_prova_caracteristica', 'spc_id', $value->fk_spc_id, 1, 'spc_regulamento'); ?></small>
                            </td>
                            <td>
                                <small>

                                    <?php
                                        $this->load->module('evento/BackOffice/Prova');
                                        $CategoriasDaProva  =   $this->prova->GetCategoriasDaProva($value->srp_id);
                                        foreach ($CategoriasDaProva as $key => $value):
                                    ?>
                                    <a href="javascript:;" class="btn btn-xs btn-default tooltips" data-original-title="<?php echo $value->evc_categoria; ?>" style="margin-right: 5px;"> <?php echo $value->evc_sigla; ?> </a>
                                    <?php endforeach; ?>

                                </small>
                            </td>
                            <td>
                                <?php echo $this->model_crud->get_rowSpecific('tb_evento_serie_prova_pista', 'spp_id', $value->fk_spp_id, 1, 'spp_pista'); ?>
                            </td>
                        </tr>
                        <?php endforeach;  ?>

                    </tbody>
                </table>
            </div>




          </div>
          <!-- /portlet-body -->
      </div>
      <!-- /portlet -->

    </div>
    <!-- end box-serie-prova -->




</div>
