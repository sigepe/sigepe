<div class="container page-inscricao">

    <div class="row inscricao-titulo-secao">
      <div class="col-sm-12">
        <ul>
          <li class="numero-bloco">02</li>
          <li class="titulo">SELECIONE <b>SÉRIE/PROVA</b></li>
        </ul>
      </div>

    </div>

    <div class="row">
      <div class="col-sm-12">
        <hr class="separador-inscricao">
        <hr class="separador-inscricao">
        <hr class="separador-inscricao">
        <hr class="separador-inscricao">
        <hr class="separador-inscricao">
      </div>
    </div>


    <div class="row">
      <div class="col-sm-12 aviso-selecione-conjunto">
        <div class="alert alert-warning">
          <strong>Atenção!</strong> Selecione um conjunto para continuar a inscrição.
        </div>
      </div>
    </div>


    <!-- BOX-SERIE-PROVA -->
    <div id="box-serie-prova" style="display:none;">


      <div class="portlet light bordered">
          <div class="portlet-title">
              <div class="caption font-yellow-crusta">
                  <i class="icon-share font-yellow-crusta"></i>
                  <span class="caption-subject bold uppercase"> Série</span>
                  <span class="caption-helper">Inscrição por série</span>
              </div>
          </div>
          <div class="portlet-body">
            Content
          </div>
      </div>



        <div class="form-group" id="form-give-name" style="">
          <div class="mt-radio-list">


              <?php foreach ($Series as $key => $value): ?>
              <label class="mt-radio serie-item">
                  <input type="radio" name="tipo-pagamento" id="tipo-pagamento-boleto" value="1">
                      <h4 style="font-weight:bold;"><?php echo $value->ers_nome; ?>   |   <small style="text-decoration: line-through;">R$ <?php echo $value->ers_valor_apos_inscricao_definitiva; ?></small> R$ <?php echo $value->ers_valor_ate_inscricao_definitiva; ?> </h4>

                      <small style="display: block;font-weight: 500">



                        <!-- Provas Vinculadas a serie -->
                        <?php

                        $this->load->module('evento/BackOffice/Serie');
                        $SerieId            =   $value->ers_id;
                        $DatasetProva       =   $this->serie->GetProvasDaSerie($SerieId);



                         if(!empty($DatasetProva)): ?>

                        <div class="table-scrollable">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th> Dia </th>
                                        <th> Nº Prova </th>
                                        <th> Característica </th>
                                        <th> Categorias da Prova </th>
                                        <th> Pista </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($DatasetProva as $key => $value): ?>
                                    <tr>
                                        <td>
                                            <?php echo $this->my_data->ConverterData($value->srp_dia, 'ISO', 'PT-BR'); ?>
                                            <br>
                                            <small>
                                                <?php echo $this->my_data->diasemana($value->srp_dia); ?>
                                                <br>
                                                às <?php echo $value->srp_hora; ?>
                                            </small>
                                        </td>
                                        <td> Pr. <?php echo $value->srp_numero_prova; ?> </td>
                                        <td>
                                            <?php echo $this->model_crud->get_rowSpecific('tb_evento_serie_prova_caracteristica', 'spc_id', $value->fk_spc_id, 1, 'spc_caracteristica'); ?>
                                            <br>
                                            <small class="font-grey-cascade"><?php echo $this->model_crud->get_rowSpecific('tb_evento_serie_prova_caracteristica', 'spc_id', $value->fk_spc_id, 1, 'spc_regulamento'); ?></small>
                                        </td>
                                        <td>
                                            <small>

                                                <?php
                                                    $this->load->module('evento/BackOffice/Prova');
                                                    $CategoriasDaProva  =   $this->prova->GetCategoriasDaProva($value->srp_id);
                                                    foreach ($CategoriasDaProva as $key => $value):
                                                ?>
                                                <a href="javascript:;" class="btn btn-xs btn-default tooltips" data-original-title="<?php echo $value->evc_categoria; ?>" style="margin-right: 5px;"> <?php echo $value->evc_sigla; ?> </a>
                                                <?php endforeach; ?>

                                            </small>
                                        </td>
                                        <td>
                                            <?php echo $this->model_crud->get_rowSpecific('tb_evento_serie_prova_pista', 'spp_id', $value->fk_spp_id, 1, 'spp_pista'); ?>
                                        </td>
                                    </tr>
                                    <?php endforeach;  ?>

                                </tbody>
                            </table>
                        </div>
                        <?php endif; ?>




                      </small>
                  <span></span>
              </label>
              <hr>
              <?php endforeach; ?>


          </div>

          <div id="form-tipo-pagamento-erro"></div>

      </div>

    </div>
    <!-- end box-serie-prova -->




</div>
