
        <div class="portlet light " id="form_wizard_1">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-red"></i>
                    <span class="caption-subject font-red bold uppercase"> Formulário -
                        <span class="step-title"> Etapa 1 de 2 </span>
                    </span>
                </div>
            </div>
            <div class="portlet-body form">
                <form class="form-horizontal" action="#" id="form-guest" method="POST">
                    <div class="form-wizard">
                        <div class="form-body">
                            <ul class="nav nav-pills nav-justified steps">
                                <li>
                                    <a href="#tab1" data-toggle="tab" class="step">
                                        <span class="number"> 1 </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> Informações Gerais </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab2" data-toggle="tab" class="step">
                                        <span class="number"> 2 </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> Conclusão </span>
                                    </a>
                                </li>
                            </ul>


                            <div id="bar" class="progress progress-striped" role="progressbar">
                                <div class="progress-bar progress-bar-success"> </div>
                            </div>



                            <!-- 
                            TAB-CONTENT
                            ================================================ -->
                            <div class="tab-content">
                            
                                <!-- ALERTS -->
                                <div class="alert alert-danger display-none">
                                    <button class="close" data-dismiss="alert"></button> Existe alguns campos no formulário que são obrigatórios ou que não foram preenchidos corretamente. Confira antes de avançar. </div>

                                <div class="alert alert-success display-none">
                                    <button class="close" data-dismiss="alert"></button> Formulário validado com sucesso! </div>

                                <!-- TAB1 -->
                                <div class="tab-pane active" id="tab1">

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Data de Nascimento
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control mask-data" name="data-nascimento-fundacao" id="data-nascimento-fundacao" />
                                        </div>
                                    </div>

                                    <div class="form-group" id="layer-cpf">
                                        <label class="control-label col-md-3">CPF
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control mask-cpf" name="cpf-cnpj" id="cpf-cnpj" />
                                        </div>
                                    </div>

                                    <div class="form-group" id="layer-cpf-isento" style="display: none;">
                                        <label class="control-label col-md-3">
                                        </label>
                                        <div class="col-md-4">


                                            <div class="note note-warning" id="usuario-aviso" style="display: none;">
                                                <h4 class="block" style="
                                                font-weight: bold;
                                                ">Seu usuário: <?php echo $CodigoUsuarioMenorIdade; ?></h4>
                                                <p>Anote o número acima. É o código para acessar sua conta. Seu usuário é único e intransferível.</p>
                                            </div>


                                            <input type="checkbox" value="1" name="cpf-isento" id="cpf-isento" />
                                            <label class="" for="cpf-isento"> Não tenho CPF </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Nome Completo
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="nome-razao-social" id="nome-razao-social" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Gênero
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <select class="form-control" name="genero" id="genero">
                                                <option value="">- Selecione uma opção -</option>
                                                <option value="2">Feminino</option>
                                                <option value="1">Masculino</option>
                                                <option value="3">Outro</option>
                                                <option value="4">Prefiro não informar</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Celular
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control mask-telefone" name="numero" id="numero" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">E-mail
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="email" id="email" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Senha
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="password" class="form-control" name="senha" id="senha" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Confirmar Senha
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="password" class="form-control" name="confirmar-senha" />
                                        </div>
                                    </div>  

                                    <input type="hidden" name="flag-usuario" value="1">
                                    <input type="hidden" name="usuario-menor-idade" value="<?php echo $CodigoUsuarioMenorIdade; ?>">
                                    <input type="hidden" name="numero-tipo" value="4">
                                    <input type="hidden" name="numero-principal" value="1">
                                    <input type="hidden" name="email-tipo" value="1">
                                    <input type="hidden" name="email-principal" value="1">
                                    <input type="hidden" name="pessoa-natureza" value="PF">
                                    <input type="hidden" name="pessoa-perfil" value="ordinario">



                                </div>
                                <!-- tab-pane / #tab1 -->

                                <div class="tab-pane" id="tab2">

                                    <div class="alert alert-warning">
                                        <strong>Atenção! </strong>Seu cadastro ainda não foi concluído. Confirme as informações abaixo e clique no botão <b>CADASTRAR</b>.
                                    </div>

                                    <h3 class="block">Confirme seus dados</h3>

                                    <h4 class="form-section">Informações Gerais</h4>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Data de Nascimento:</label>
                                        <div class="col-md-4">
                                            <p class="form-control-static" data-display="data-nascimento-fundacao"> </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">CPF:</label>
                                        <div class="col-md-4">
                                            <p class="form-control-static" data-display="cpf-cnpj"> </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Nome Completo:</label>
                                        <div class="col-md-4">
                                            <p class="form-control-static" data-display="nome-razao-social"> </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Gênero:</label>
                                        <div class="col-md-4">
                                            <p class="form-control-static" data-display="genero"> </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Celular:</label>
                                        <div class="col-md-4">
                                            <p class="form-control-static" data-display="numero"> </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">E-mail:</label>
                                        <div class="col-md-4">
                                            <p class="form-control-static" data-display="email"> </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- /tab-pane -->

                            </div>
                            <!-- /tab-content -->

                        </div>
                        <!-- /form-body -->


                        <!-- 
                        FORM-ACTIONS
                        ================================================ -->
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <a href="javascript:;" class="btn default button-previous">
                                        <i class="fa fa-angle-left"></i> Voltar </a>
                                    <a href="javascript:;" class="btn btn-outline green button-next"> Continuar
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                    <a href="javascript:;" class="btn green button-submit"> Cadastrar
                                        <i class="fa fa-check"></i>
                                    </a>
                                </div>
                            </div>
                        </div>


                    </div>
                </form>
            </div>
        </div>

