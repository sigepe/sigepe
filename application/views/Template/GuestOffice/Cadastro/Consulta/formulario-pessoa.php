<style type="text/css">
    .portlet-title{
        padding-bottom: 0px;
    }
    .portlet.light .portlet-body{
        padding: 0;
        margin: 0;
    }
    #atleta-tabs-left{
        min-height: 530px;
    }
    #atleta-tabs-left li a{
        text-align: right;                                
    }
    #atleta-tabs-left li.active a{
        background: #36c6d3;
        color: white;
        border: none;
    }
    .label-vinculo-atual{
        display: block;
        padding: 10px !important;        
    }

    #dashboard-pessoa .form-control-static {
        padding-top: 0px;
        padding-bottom: 0px;
        margin-bottom: 0;
        min-height: 22px;
    }
    #dashboard-pessoa .form-group{
        border-bottom: 1px solid #ccc;
        float: left;
        width: 100%;
        min-height: 28px;
        margin-bottom: 0;
    }

</style>


        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-red"></i>
                    <span class="caption-subject font-red bold uppercase"> CONSULTAR PESSOA - FÍSICA/JURÍDICA
                    </span>
                </div>
            </div>


            <form action="{base_url}cadastro/Consulta/Processar/" class="form-horizontal" method="post">

                <div class="form-body">

                    <div class="form-group last">
                        <label class="col-md-3 control-label">ID Pessoa</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <span class="input-group-addon input-circle-left">
                                    <i class="fa fa-user"></i>
                                </span>
                                <input type="text" class="form-control input-circle-right" name="id-pessoa" placeholder="ID da Pessoa">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-circle green">Consultar</button>
                    </div>

                </div>

            </form>


            <hr>

            <?php  if(isset($Pessoa)): ?>
            <?php $Pessoa = $Pessoa[0]; ?>

            <div class="portlet-body form" style="display: <?php echo ($IdPessoa) ? 'block' : 'none'; ?>" id="dashboard-pessoa">

                <div class="row">

                    <div class="col-sm-6">

                        <h4 class="form-section bold">
                            <i class="fa fa-globe" aria-hidden="true"></i>
                            Pessoa
                        </h4>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">ID Pessoa:</label>
                            <div class="col-md-4">
                                <p class="form-control-static">#<?php echo $Pessoa->pes_id ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Flag Usuário:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (is_null($Pessoa->flag_usuario)) ? 'Não' : 'Sim' ; ?></p>
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Autor do Registro:</label>
                            <div class="col-md-5">
                                <p class="form-control-static"><?php echo (!is_null($Pessoa->fk_aut_id)) ? $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $Pessoa->fk_aut_id, 1, 'pes_nome_razao_social') . '<br>' . '<small>#'. $Pessoa->fk_aut_id .'</small>' : '-' ; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Status:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $Pessoa->fk_sta_id, 1, 'sta_status');  ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Natureza:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo ($Pessoa->pes_natureza == 'PF') ? 'Pessoa Física' : 'Pessoa Jurídica' ; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">CPF/CNPJ:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $this->my_pessoa->InserirPontuacaoCpfCnpj($Pessoa->pes_cpf_cnpj); ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Nome Completo / Razão Social:</label>
                            <div class="col-md-5">
                                <p class="form-control-static"><?php echo strtoupper($Pessoa->pes_nome_razao_social); ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Data de Nascimento / Fundação</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $this->my_data->ConverterData($Pessoa->pes_data_nascimento_fundacao, 'ISO', 'PT-BR'); ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Foto</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $Pessoa->pes_foto; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Criado</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $this->my_data->ConverterData($Pessoa->criado, 'ISO', 'PT-BR'); ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Modificado</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $this->my_data->ConverterData($Pessoa->modificado, 'ISO', 'PT-BR'); ?></p>
                            </div>
                        </div>
                    </div>


                    <?php if(isset($PessoaFisica)): ?>
                    <?php $PessoaFisica     =   $PessoaFisica[0]; ?>
                    <div class="col-sm-6">

                        <h4 class="form-section bold">
                            <i class="fa fa-user" aria-hidden="true"></i>
                            Pessoa Física
                        </h4>


                        <div class="form-group">
                            <label class="control-label col-md-6 bold">ID Pessoa Física:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $PessoaFisica->pef_id; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">ID Pessoa:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $PessoaFisica->fk_pes_id; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Nacionalidade:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (!is_null($PessoaFisica->fk_esc_id)) ? $this->model_crud->get_rowSpecific('tb_pessoa_fisica_escolaridade', 'pfe_id', $PessoaFisica->fk_esc_id, 1, 'pfe_escolaridade') . '<br>' . '<small>#'. $PessoaFisica->fk_esc_id .'</small>' : '-' ; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Escolaridade:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (!is_null($PessoaFisica->fk_esc_id)) ? $this->model_crud->get_rowSpecific('tb_pessoa_fisica_escolaridade', 'pfe_id', $PessoaFisica->fk_esc_id, 1, 'pfe_escolaridade') . '<br>' . '<small>#'. $PessoaFisica->fk_esc_id .'</small>' : '-' ; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Tipo Sanguineo:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (!is_null($PessoaFisica->fk_tip_id)) ? $this->model_crud->get_rowSpecific('tb_pessoa_fisica_tiposanguineo', 'pft_id', $PessoaFisica->fk_tip_id, 1, 'pft_tipo_sanguineo') . '<br>' . '<small>#'. $PessoaFisica->fk_tip_id .'</small>' : '-' ; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Naturalidade:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (!is_null($PessoaFisica->fk_tip_id)) ? $this->model_crud->get_rowSpecific('tb_pessoa_fisica_tiposanguineo', 'pft_id', $PessoaFisica->fk_tip_id, 1, 'pft_tipo_sanguineo') . '<br>' . '<small>#'. $PessoaFisica->fk_tip_id .'</small>' : '-' ; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Estado Civil:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (!is_null($PessoaFisica->fk_tip_id)) ? $this->model_crud->get_rowSpecific('tb_pessoa_fisica_estadocivil', 'pfe_id', $PessoaFisica->fk_est_id, 1, 'pfe_estado_civil') . '<br>' . '<small>#'. $PessoaFisica->fk_est_id .'</small>' : '-' ; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Gênero:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (!is_null($PessoaFisica->fk_gen_id)) ? $this->model_crud->get_rowSpecific('tb_pessoa_fisica_genero', 'gen_id', $PessoaFisica->fk_gen_id, 1, 'pfg_genero') . '<br>' . '<small>#'. $PessoaFisica->fk_gen_id .'</small>' : '-' ; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Primeiro Nome:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $PessoaFisica->pef_primeiro_nome; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Sobrenome:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $PessoaFisica->pef_sobrenome; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Nome do Pai:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (!is_null($PessoaFisica->pef_nome_pai)) ? $PessoaFisica->pef_nome_pai : '-'; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Nome da Mãe:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (!is_null($PessoaFisica->pef_nome_mae)) ? $PessoaFisica->pef_nome_mae : '-'; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Apelido:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (!is_null($PessoaFisica->pef_apelido)) ? $PessoaFisica->pef_apelido : '-'; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Criado em:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $this->my_data->ConverterData($PessoaFisica->criado, 'ISO', 'PT-BR'); ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Modificado em:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $this->my_data->ConverterData($PessoaFisica->modificado, 'ISO', 'PT-BR'); ?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>

                </div>
                

            </div>
            <!-- /portlet-body -->


            <hr>



            <div class="tabbable-line"  style="display: <?php echo ($IdPessoa) ? 'block' : 'none'; ?>" >


                <ul class="nav nav-tabs ">
                    <li class="active">
                        <a href="#tab_telefone" data-toggle="tab">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            Telefone
                        </a>
                    </li>
                    <li>
                        <a href="#tab_email" data-toggle="tab">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                            E-mail
                        </a>
                    </li>
                    <li>
                        <a href="#tab_endereco" data-toggle="tab">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            Endereço
                        </a>
                    </li>
                    <li>
                        <a href="#tab_vinculos" data-toggle="tab">
                            <i class="fa fa-link" aria-hidden="true"></i>
                            Vínculos
                        </a>
                    </li>
                    <li>
                        <a href="#tab_perfis" data-toggle="tab">
                            <i class="fa fa-users" aria-hidden="true"></i>
                            Perfis
                        </a>
                    </li>
                    <li>
                        <a href="#tab_animais" data-toggle="tab">
                            <i class="fa fa-paw" aria-hidden="true"></i>
                            Animais
                        </a>
                    </li>
                    <li>
                        <a href="#tab_atleta" data-toggle="tab">
                            <i class="fa fa-universal-access" aria-hidden="true"></i>
                            Atleta
                        </a>
                    </li>
                    <li>
                        <a href="#tab_empresas" data-toggle="tab">
                            <i class="fa fa-briefcase" aria-hidden="true"></i>
                            Empresas
                        </a>
                    </li>
                </ul>



                <div class="tab-content" style="padding-top: 10px;">



                    <!-- 
                    TELEFONE
                    =========================================== -->
                    <div class="tab-pane active" id="tab_telefone">
                        <div class="portlet light" style="padding-top: 0;">
                            <div class="portlet-body">

                                <?php if(empty($Telefones)): ?>
                                <div class="alert alert-warning row">
                                    <strong>Atenção!</strong>
                                    Nenhum telefone cadastrado para essa pessoa.
                                </div>
                                <?php endif; ?>

                                <?php if(!empty($Telefones)): ?>
                                <div class="row">
                                    <div class="table-scrollable">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th> ID Telefone </th>
                                                    <th> ID Autor </th>
                                                    <th> ID Pessoa </th>
                                                    <th> Tipo Contato </th>
                                                    <th> Status </th>
                                                    <th> DDD </th>
                                                    <th> Numero </th>
                                                    <th> Telefone </th>
                                                    <th> Principal </th>
                                                    <th> Criado </th>
                                                    <th> Modificado </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($Telefones as $key => $value): ?>
                                                <tr>
                                                    <td> #<?php echo $value->tel_id; ?> </td>
                                                    <td> #<?php echo $value->fk_aut_id; ?> </td>
                                                    <td> #<?php echo $value->fk_peo_id; ?> </td>
                                                    <td> <?php echo $this->model_crud->get_rowSpecific('tb_contato', 'con_id', $value->fk_con_id, 1, 'con_contato'); ?> <small>( ID: <?php echo $value->fk_con_id; ?> )</small> </td>
                                                    <td>
                                                        <?php
                                                            if($value->fk_sta_id=='1')
                                                                echo '<span class="label label-sm label-info"> Ativo </span>';

                                                            if($value->fk_sta_id=='2')
                                                                echo '<span class="label label-sm label-default"> Inativo </span>';
                                                        ?>
                                                        
                                                    </td>
                                                    <td> <?php echo $value->tel_ddd; ?> </td>
                                                    <td> <?php echo $value->tel_numero; ?> </td>
                                                    <td> <?php echo $value->tel_telefone; ?> </td>
                                                    <td>
                                                        <?php if(!is_null($value->tel_principal)): ?>
                                                        <span class="label label-sm label-info"> PRINCIPAL </span>
                                                        <?php endif; ?>
                                                        <?php echo (is_null($value->tel_principal)) ? '-' : ''; ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                            $DataIso    =    $this->my_data->GetDateTimeIso($value->criado, 'Date');
                                                            echo $this->my_data->ConverterData($DataIso, 'ISO', 'PT-BR');
                                                        ?>
                                                        <br>
                                                        <small>
                                                            <?php echo $this->my_data->GetDateTimeIso($value->criado, 'Time'); ?>
                                                        </small>
                                                    </td>
                                                    <td> <?php echo $this->my_data->GetDateTimeIso($value->modificado, 'Date'); ?> <br> <small><?php echo $this->my_data->GetDateTimeIso($value->modificado, 'Time'); ?></small> </td>
                                                </tr>
                                                <?php endforeach; ?>
                                              
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>



                    <!-- 
                    EMAIL
                    =========================================== -->
                    <div class="tab-pane" id="tab_email">
                     
                        <div class="portlet light" style="padding-top: 0;">
                            <div class="portlet-body">


                                <?php if(empty($Emails)): ?>
                                <div class="alert alert-warning row">
                                    <strong>Atenção!</strong>
                                    Nenhum email cadastrado para essa pessoa.
                                </div>
                                <?php endif; ?>


                                <?php if(!empty($Emails)): ?>
                                <div class="row">
                                    <div class="table-scrollable">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th> ID E-mail </th>
                                                    <th> ID Autor </th>
                                                    <th> ID Pessoa </th>
                                                    <th> Tipo Contato </th>
                                                    <th> Status </th>
                                                    <th> E-mail </th>
                                                    <th> Principal </th>
                                                    <th> Criado </th>
                                                    <th> Modificado </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($Emails as $key => $value): ?>
                                            <tr>
                                                <td> #<?php echo $value->ema_id; ?> </td>
                                                <td> #<?php echo $value->fk_aut_id; ?> </td>
                                                <td> #<?php echo $value->fk_peo_id; ?> </td>
                                                <td> <?php echo $this->model_crud->get_rowSpecific('tb_contato', 'con_id', $value->fk_con_id, 1, 'con_contato'); ?> <small>( ID: <?php echo $value->fk_con_id; ?> )</small> </td>
                                                <td>
                                                    <?php
                                                        if($value->fk_sta_id=='1')
                                                            echo '<span class="label label-sm label-info"> Ativo </span>';

                                                        if($value->fk_sta_id=='2')
                                                            echo '<span class="label label-sm label-default"> Inativo </span>';
                                                    ?>
                                                </td>
                                                <td> <?php echo $value->ema_email; ?> </td>
                                                <td>
                                                    <?php if(!is_null($value->ema_principal)): ?>
                                                    <span class="label label-sm label-info"> PRINCIPAL </span>
                                                    <?php endif; ?>
                                                    <?php echo (is_null($value->ema_principal)) ? '-' : ''; ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        $DataIso    =    $this->my_data->GetDateTimeIso($value->criado, 'Date');
                                                        echo $this->my_data->ConverterData($DataIso, 'ISO', 'PT-BR');
                                                    ?>
                                                    <br>
                                                    <small>
                                                        <?php echo $this->my_data->GetDateTimeIso($value->criado, 'Time'); ?>
                                                    </small>
                                                </td>
                                                <td> <?php echo $this->my_data->GetDateTimeIso($value->modificado, 'Date'); ?> <br> <small><?php echo $this->my_data->GetDateTimeIso($value->modificado, 'Time'); ?></small> </td>
                                            </tr>
                                            <?php endforeach; ?>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>


                    <!-- 
                    ENDERECO
                    =========================================== -->
                    <div class="tab-pane" id="tab_endereco">
                        <div class="alert alert-warning"> <strong>Atenção!</strong> Nenhum endereço cadastrada para essa pessoa. </div>
                    </div>


                    <!-- 
                    VINCULOS
                    =========================================== -->
                    <div class="tab-pane" id="tab_vinculos">
<!--                         <div class="alert alert-warning"> <strong>Atenção!</strong> Nenhum vínculo encontrado. </div> -->
                        <div class="alert alert-danger"> <strong>Atenção!</strong> Aba não implantada. MBOTQA </div>


                        <div class="portlet light" style="padding-top: 0;">
                            <div class="portlet-body">
                                <div class="table-scrollable">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th> ID E-mail </th>
                                                <th> ID Autor </th>
                                                <th> ID Pessoa </th>
                                                <th> Tipo Contato </th>
                                                <th> Status </th>
                                                <th> E-mail </th>
                                                <th> Principal </th>
                                                <th> Criado </th>
                                                <th> Modificado </th>
                                            </tr>
                                        </thead>
<!-- 
                                        <tbody>
                                            <tr>
                                                <td> #94 </td>
                                                <td> #346 </td>
                                                <td> #138 </td>
                                                <td> Comercial <small>( ID: 04 )</small> </td>
                                                <td> <span class="label label-sm label-info"> Ativo </span> </td>
                                                <td> gustavobotega@gmail.com </td>
                                                <td> <span class="label label-sm label-info"> PRINCIPAL </span> </td>
                                                <td> 02/08/2014 <br> <small>09h50</small> </td>
                                                <td> 02/08/2014 <br> <small>09h50</small> </td>
                                            </tr>
                                            <tr>
                                                <td> #135 </td>
                                                <td> #346 </td>
                                                <td> #138 </td>
                                                <td> Comercial <small>( ID: 04 )</small> </td>
                                                <td> <span class="label label-sm label-info"> Ativo </span> </td>
                                                <td> gustavobotega@gmail.com </td>
                                                <td> - </td>
                                                <td> 02/08/2014 <br> <small>09h50</small> </td>
                                                <td> 02/08/2014 <br> <small>09h50</small> </td>
                                            </tr>
                                        </tbody>
                                         -->
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>


                    <!-- 
                    PERFIS
                    =========================================== -->
                    <div class="tab-pane" id="tab_perfis">



                        <?php if(empty($Perfis)): ?>
                        <div class="alert alert-warning">
                            <strong>Atenção!</strong>
                            Nenhum perfil vinculado para essa pessoa.
                        </div>
                        <?php endif; ?>


                        <?php if(!empty($Perfis)): ?>
                        <div class="portlet light" style="padding-top: 0;">
                            <div class="portlet-body">
                                <div class="table-scrollable">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th> ID Vínculo </th>
                                                <th> ID Pessoa </th>
                                                <th> Nome </th>
                                                <th> Perfil </th>
                                                <th> Status </th>
                                                <th> Criado </th>
                                                <th> Modificado </th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php

                                                $QueryPerfil    =   "
                                                                        SELECT * FROM tb_vinculo
                                                                            WHERE fk_pes1_id = ".$IdPessoa." AND
                                                                                  fk_pes2_id IS NULL AND
                                                                                  fk_ani_id IS NULL AND
                                                                                  fk_per_id IS NOT NULL AND
                                                                                  fk_tip_id IS NULL AND
                                                                                  fk_sta_id IS NOT NULL
                                                                    ";
                                                $Perfis  =   $this->db->query($QueryPerfil)->result();
                                                foreach ($Perfis as $key => $value):
                                            ?>
                                            <tr>
                                                <td> #<?php echo $value->vin_id; ?> </td>
                                                <td> #<?php echo $value->fk_pes1_id; ?> </td>
                                                <td>
                                                    <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes1_id, 1, 'pes_nome_razao_social'); ?>
                                                    <br>                                                        
                                                    <small>#<?php echo $value->fk_pes1_id; ?></small>
                                                </td>
                                                <td>
                                                    <?php echo $this->model_crud->get_rowSpecific('tb_vinculo_perfil', 'vip_id', $value->fk_per_id, 1, 'vip_papel'); ?>
                                                    <small>( ID: <?php echo $value->fk_per_id; ?> )</small>
                                                </td>
                                                <td>
                                                    <?php
                                                        if($value->fk_sta_id=='1')
                                                            echo '<span class="label label-sm label-info"> Ativo </span>';

                                                        if($value->fk_sta_id=='2')
                                                            echo '<span class="label label-sm label-default"> Inativo </span>';
                                                    ?>
                                                </td>

                                                <td>
                                                    <?php
                                                        $DataIso    =    $this->my_data->GetDateTimeIso($value->criado, 'Date');
                                                        echo $this->my_data->ConverterData($DataIso, 'ISO', 'PT-BR');
                                                    ?>
                                                    <br>
                                                    <small>
                                                        <?php echo $this->my_data->GetDateTimeIso($value->criado, 'Time'); ?>
                                                    </small>
                                                </td>
                                                <td>
                                                    <?php
                                                        $DataIso    =    $this->my_data->GetDateTimeIso($value->modificado, 'Date');
                                                        echo $this->my_data->ConverterData($DataIso, 'ISO', 'PT-BR');
                                                    ?>
                                                    <br>
                                                    <small>
                                                        <?php echo $this->my_data->GetDateTimeIso($value->modificado, 'Time'); ?>
                                                    </small>
                                                </td>


                                            </tr>
                                            <?php endforeach; ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>


                    </div>




                    <!-- 
                    ANIMAIS
                    =========================================== -->
                    <div class="tab-pane" id="tab_animais">
                        <div class="alert alert-warning"> <strong>Atenção!</strong> Nenhum animal cadastrada para essa pessoa. </div>




                        <div class="row display-none">
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <ul class="nav nav-tabs tabs-left">
                                    <li class="active">
                                        <a href="#tab_animais_vinculos" data-toggle="tab"> Vínculos </a>
                                    </li>
                                    <li>
                                        <a href="#tab_animais_proprietarios" data-toggle="tab"> Proprietário(s) </a>
                                    </li>
                                    <li>
                                        <a href="#tab_animais_responsavel" data-toggle="tab"> Responsável Financeiro </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_animais_vinculos">

                                    <h3>Vínculos</h3>

                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs ">
                                            <li class="active">
                                                <a href="#tab_animais_vinculos_confederacao" data-toggle="tab" aria-expanded="true"> Confederação </a>
                                            </li>
                                            <li class="">
                                                <a href="#tab_animais_vinculos_federacao" data-toggle="tab" aria-expanded="false"> Federação </a>
                                            </li>
                                            <li>
                                                <a href="#tab_animais_vinculos_entidade_equestre" data-toggle="tab"> Entidade Equestre </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_animais_vinculos_confederacao">
                                                <p> I'm in Section 1. </p>
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut
                                                    aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate
                                                    velit esse molestie consequat. </p>
                                            </div>
                                            <div class="tab-pane" id="tab_animais_vinculos_federacao">
                                                <p> Howdy, I'm in Section 2. </p>
                                                <p> Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate
                                                    velit esse molestie consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation. </p>
                                            </div>
                                            <div class="tab-pane" id="tab_animais_vinculos_entidade_equestre">
                                                <p> Howdy, I'm in Section 3. </p>
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                                                    Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat </p>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab_animais_responsavel">
                                        <p> Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan
                                            four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda
                                            labore aesthetic magna delectus mollit. Keytar helvetica VHS salvia yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit, sustainable
                                            jean shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester stumptown, tumblr butcher vero sint qui sapiente accusamus tattooed echo park. </p>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <!-- //tab_animais -->




                    <!-- 
                    ATLETA
                    =========================================== -->

                    <div class="tab-pane" id="tab_atleta">

                        <?php if(!$PerfilAtleta): ?>
                        <div class="alert alert-warning"> <strong>Atenção!</strong> Não há perfil atleta para essa pessoa. </div>
                        <?php endif; ?>

                        

                        <?php if($PerfilAtleta): ?>
                        <div class="row">

                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <ul class="nav nav-tabs tabs-left" id="atleta-tabs-left">
                                   <li class="active">
                                        <a href="#tab_atleta_informacoes_gerais" data-toggle="tab"> Informações Gerais </a>
                                   </li>
                                   <li>
                                        <a href="#tab_atleta_vinculos" data-toggle="tab"> Vínculos </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <div class="tab-content">


                                    <!-- Informacoes Gerais -->
                                    <div class="tab-pane active" id="tab_atleta_informacoes_gerais">

                                            
                                        <!-- 
                                        Informações Gerais
                                        **************************************** -->
                                        <h3 class="form-section">
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <div class="title">
                                                        <i class="fa fa-user-o" aria-hidden="true"></i>
                                                        Informações Gerais
                                                    </div>
                                                </div>
                                            </div>
                                        </h3>


                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="table-scrollable table-scrollable-borderless">
                                                    <table class="table table-hover table-light">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <b>ID Atleta:</b> <br>
                                                                    <small>#<?php echo $IdPessoaFisicaAtleta; ?></small>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b>Perfil Atleta Desde:</b> <br>
                                                                    <small><?php echo $this->my_data->ConverterData($PerfilAtletaDesde, 'ISO', 'PT-BR'); ?></small>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b>Status do Perfil</b> <br>
                                                                    <small>
                                                                        <span class="label label-sm tooltips label-<?php echo GetBootstrapColor($StatusPerfilAtleta); ?>" style="display: inline-block; padding: 6px 15px; margin-top: 7px; margin-bottom: 7px;">
                                                                            <?php echo strtoupper(GetStatusTitle($StatusPerfilAtleta)); ?>                                                                        
                                                                        </span>
                                                                    </small>
                                                                </td>
                                                            </tr>

                                                            <?php
                                                                if(is_null($FlagRegistroAtleta)){
                                                                    $StatusRegistroAtleta                   =   'VENCIDO';
                                                                    $BoostrapColorRegistro                  =   'danger';
                                                                }else{
                                                                    $StatusRegistroAtleta                   =   'VÁLIDO';
                                                                    $BoostrapColorRegistro                  =   'primary';
                                                                }
                                                            ?>                                                            
                                                            <tr>
                                                                <td>
                                                                    <b>Status do Registro ( Habilitação FHBr )</b> <br>
                                                                    <span class="label label-sm tooltips label-<?php echo $BoostrapColorRegistro; ?>" style="display: inline-block; padding: 6px 15px; margin-top: 7px; margin-bottom: 7px;">
                                                                        <?php echo $StatusRegistroAtleta; ?>                                                                        
                                                                    </span>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b>Responsável Financeiro pelo Atleta</b> <br>
                                                                    <small>-</small> <!-- QA20172 -->
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>  
                                            </div>  

                                            <div class="col-sm-6">
                                                <div class="table-scrollable table-scrollable-borderless">
                                                    <table class="table table-hover table-light">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <b>Nome Competição:</b> <br>
                                                                    <small>
                                                                        <?php echo $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'pfa_id', $IdPessoaFisicaAtleta, 1, 'pfa_nome_competicao'); ?>
                                                                    </small>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b>Registro CBH</b> <br>
                                                                    <small>
                                                                        <?php echo $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'pfa_id', $IdPessoaFisicaAtleta, 1, 'pfa_registro_cbh'); ?>
                                                                    </small>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b>Registro FEI</b> <br>
                                                                    <small>
                                                                        <?php
                                                                            $MatriculaFei   =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'pfa_id', $IdPessoaFisicaAtleta, 1, 'pfa_registro_fei');
                                                                            if(is_null($MatriculaFei)){
                                                                                echo 'N/I';
                                                                            }else{
                                                                                echo $MatriculaFei;
                                                                            }
                                                                        ?>
                                                                    </small>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b>Atleta pertence a Escola de Equitação</b> <br>
                                                                    <small><?php echo (!is_null($AtletaEscola)) ? 'SIM' : 'NÃO' ?></small><!-- QA20172 -->
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b>Modalidades Habilitadas</b> <br>
                                                                    <small>-</small><!-- QA20172 -->
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>  
                                            </div>  
                                        </div>





                                    </div>
                              

                                    <!-- Vinculos -->
                                    <div class="tab-pane fade" id="tab_atleta_vinculos">

                                        <h3>
                                            <i class="fa fa-link" aria-hidden="true"></i>
                                            Vínculos Atuais
                                        </h3>

                                        <div class="table-scrollable table-scrollable-borderless">
                                            <table class="table table-hover table-light">
                                                <thead>
                                                    <tr class="uppercase">
                                                        <th> Empresa </th>
                                                        <th> Vínculo </th>
                                                    </tr>
                                                </thead>
                                                <tbody>


                                                    <!-- 
                                                    CONFEDERACAO
                                                    =========================================
                                                    -->
                                                    <?php if(empty($ConfederacaoAtual)): ?>
                                                    <tr>
                                                        <td>
                                                            <b>Confederação</b> <br>
                                                            <small>
                                                                [ Vínculo Inexistente ]
                                                            </small>
                                                        </td>
                                                        <td> 
                                                            <span class="label label-sm tooltips label-danger bold label-vinculo-atual">
                                                                <i class="fa fa-close" aria-hidden="true"></i>
                                                                Atleta não possui vínculo com nenhuma Confederação.
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <?php endif; ?>   
                                                    <?php if(!empty($ConfederacaoAtual)): ?>
                                                    <tr>
                                                        <td>
                                                            <b>Confederação</b> <br>
                                                            <small>
                                                                <?php
                                                                    echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $ConfederacaoAtual[0]->fk_pes2_id, 1, 'pes_nome_razao_social');
                                                                ?>
                                                            </small>
                                                        </td>
                                                        <td> 
                                                            <span class="label label-sm tooltips label-<?php echo GetBootstrapColor($ConfederacaoAtual[0]->fk_sta_id); ?> bold label-vinculo-atual" data-original-title="<?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $ConfederacaoAtual[0]->fk_sta_id, 1, 'sta_status'); ?>" >
                                                                <i class="fa <?php echo GetIconVinculo($ConfederacaoAtual[0]->fk_sta_id); ?>" aria-hidden="true"></i>
                                                                <?php
                                                                    echo GetStatusVinculo($ConfederacaoAtual[0]->fk_sta_id);
                                                                ?>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <?php endif; ?>   



                                                    <!-- 
                                                    FEDERACAO
                                                    =========================================
                                                    -->
                                                    <?php if(empty($FederacaoAtual)): ?>
                                                    <tr>
                                                        <td>
                                                            <b>Federação</b> <br>
                                                            <small>
                                                                [ Vínculo Inexistente ]
                                                            </small>
                                                        </td>
                                                        <td> 
                                                            <span class="label label-sm tooltips label-danger bold label-vinculo-atual">
                                                                <i class="fa fa-close" aria-hidden="true"></i>
                                                                Atleta não possui vínculo com nenhuma Federação
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <?php endif; ?>                                                    
                                                    <?php if(!empty($FederacaoAtual)): ?>
                                                    <tr>
                                                        <td>
                                                            <b>Federação</b> <br>
                                                            <small>
                                                                <?php
                                                                    echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $FederacaoAtual[0]->fk_pes2_id, 1, 'pes_nome_razao_social');
                                                                ?>
                                                            </small>
                                                        </td>
                                                        <td> 
                                                            <span class="label label-sm tooltips label-<?php echo GetBootstrapColor($FederacaoAtual[0]->fk_sta_id); ?> bold label-vinculo-atual" data-original-title="<?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $FederacaoAtual[0]->fk_sta_id, 1, 'sta_status'); ?>" >
                                                                <i class="fa <?php echo GetIconVinculo($FederacaoAtual[0]->fk_sta_id); ?>" aria-hidden="true"></i>
                                                                <?php
                                                                    echo GetStatusVinculo($FederacaoAtual[0]->fk_sta_id);
                                                                ?>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <?php endif; ?>                                                    




                                                    <!-- 
                                                    ENTIDADE EQUESTRE
                                                    =========================================
                                                    -->
                                                    <?php if(empty($EntidadeAtual)): ?>
                                                    <tr>
                                                        <td>
                                                            <b>Entidade Equestre</b> <br>
                                                            <small>
                                                                [ Vínculo Inexistente ]
                                                            </small>
                                                        </td>
                                                        <td> 
                                                            <span class="label label-sm tooltips label-danger bold label-vinculo-atual">
                                                                <i class="fa fa-close" aria-hidden="true"></i>
                                                                Atleta não possui vínculo com nenhuma Entidade Equestre
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <?php endif; ?>

                                                    <?php if(!empty($EntidadeAtual)): ?>
                                                    <tr>
                                                        <td>
                                                            <b>Entidade Equestre</b> <br>
                                                            <small>
                                                                <?php
                                                                    echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $EntidadeAtual[0]->fk_pes2_id, 1, 'pes_nome_razao_social');
                                                                ?>
                                                            </small>
                                                        </td>
                                                        <td> 
                                                            <span class="label label-sm tooltips label-<?php echo GetBootstrapColor($EntidadeAtual[0]->fk_sta_id); ?> bold label-vinculo-atual" data-original-title="<?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $EntidadeAtual[0]->fk_sta_id, 1, 'sta_status'); ?>" >
                                                                <i class="fa <?php echo GetIconVinculo($EntidadeAtual[0]->fk_sta_id); ?>" aria-hidden="true"></i>
                                                                <?php
                                                                    echo GetStatusVinculo($EntidadeAtual[0]->fk_sta_id);
                                                                ?>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <?php endif; ?>



                                                </tbody>
                                            </table>
                                        </div>  

                                        <hr style="margin:40px 0;">

                                        <h3>
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                            Histórico Vínculos
                                        </h3>

                                        <div class="tabbable-line">
                                            <ul class="nav nav-tabs ">
                                                <li class="active">
                                                    <a href="#tab_atleta_vinculos_confederao" data-toggle="tab" aria-expanded="true"> Confederação </a>
                                                </li>
                                                <li class="">
                                                    <a href="#tab_atleta_vinculos_federacao" data-toggle="tab" aria-expanded="false"> Federação </a>
                                                </li>
                                                <li>
                                                    <a href="#tab_atleta_vinculos_entidade" data-toggle="tab"> Entidade Equestre </a>
                                                </li>
                                            </ul>
                                            <div class="tab-content" style="padding-top: 0px;">    

                                                <!-- Confederacao -->
                                                <div class="tab-pane active" id="tab_atleta_vinculos_confederao">



                                                    <?php if(empty($Confederacao)): ?>
                                                    <div class="alert alert-danger margin-top-10">
                                                        <strong>Atenção!</strong>
                                                        Esse atleta não possui vínculo com nenhuma Confederação.
                                                    </div>
                                                    <?php endif; ?>
                                                    

                                                    <?php if(!empty($Confederacao)): ?>
                                                    <div class="table-scrollable">
                                                        <table class="table table-striped table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th> # </th>
                                                                    <th> Confederação </th>
                                                                    <th> Início Vínculo </th>
                                                                    <th> Fim Vínculo </th>
                                                                    <th> Status </th>
                                                                    <th> * </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                    foreach ($Confederacao as $key => $value):
                                                                        $NomeRazaoSocial    =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes2_id, 1, 'pes_nome_razao_social');
                                                                ?>
                                                                <tr>
                                                                    <td> <?php echo $value->vin_id; ?> </td>
                                                                    <td> <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes2_id, 1, 'pes_nome_razao_social'); ?> </td>
                                                                    <td> <?php echo $this->my_data->ConverterData($value->criado, 'ISO', 'PT-BR'); ?> </td>
                                                                    <td> - </td>
                                                                    <td>                                   
                                                                        <span class="label label-sm tooltips label-<?php echo GetBootstrapColor($value->fk_sta_id); ?>" data-original-title="<?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $value->fk_sta_id, 1, 'sta_status'); ?>" >
                                                                            <?php
                                                                                echo GetStatusVinculo($value->fk_sta_id);
                                                                            ?>
                                                                        </span>
                                                                    </td>
                                                                    <td>
                                                                        <a href="#" title="" class="vinculo-detalhe-link" data-id="<?php echo $value->vin_id; ?>" data-nome-empresa="<?php echo $NomeRazaoSocial; ?>" data-id-empresa="<?php echo $value->fk_pes2_id; ?>">
                                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                            Histórico
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                                <?php endforeach; ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <?php endif; ?>


                                                </div>
                                                <!-- /Confederacao -->


                                                <!-- Federacao -->
                                                <div class="tab-pane" id="tab_atleta_vinculos_federacao">


                                                    <?php if(empty($Federacao)): ?>
                                                    <div class="alert alert-danger margin-top-10">
                                                        <strong>Atenção!</strong>
                                                        Esse atleta não possui vínculo com nenhuma Federação.
                                                    </div>
                                                    <?php endif; ?>


                                                    <?php if(!empty($Federacao)): ?>
                                                    <div class="table-scrollable">
                                                        <table class="table table-striped table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th> # </th>
                                                                    <th> Federação </th>
                                                                    <th> Início Vínculo </th>
                                                                    <th> Fim Vínculo </th>
                                                                    <th> Status </th>
                                                                    <th> * </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                    foreach ($Federacao as $key => $value):
                                                                        $NomeRazaoSocial    =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes2_id, 1, 'pes_nome_razao_social');
                                                                ?>
                                                                <tr>
                                                                    <td> <?php echo $value->vin_id; ?> </td>
                                                                    <td> <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes2_id, 1, 'pes_nome_razao_social'); ?> </td>
                                                                    <td> <?php echo $this->my_data->ConverterData($value->criado, 'ISO', 'PT-BR'); ?> </td>
                                                                    <td> - </td>
                                                                    <td>                                   
                                                                        <span class="label label-sm tooltips label-<?php echo GetBootstrapColor($value->fk_sta_id); ?>" data-original-title="<?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $value->fk_sta_id, 1, 'sta_status'); ?>" >
                                                                            <?php
                                                                                echo GetStatusVinculo($value->fk_sta_id);
                                                                                ?>
                                                                        </span>
                                                                    </td>
                                                                    <td>
                                                                        <a href="#" title="" class="vinculo-detalhe-link" data-id="<?php echo $value->vin_id; ?>" data-nome-empresa="<?php echo $NomeRazaoSocial; ?>" data-id-empresa="<?php echo $value->fk_pes2_id; ?>">
                                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                            Histórico
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                                <?php endforeach; ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <?php endif; ?>

                                                </div>
                                                <!-- /Federacao -->

                                                <!-- Entidade Equestre -->
                                                <div class="tab-pane" id="tab_atleta_vinculos_entidade">

                                                    <?php if(empty($Entidade)): ?>
                                                    <div class="alert alert-warning margin-top-10">
                                                        <strong>Atenção!</strong>
                                                        Esse atleta não possui vínculo com nenhuma Entidade Equestre.
                                                    </div>
                                                    <?php endif; ?>


                                                    <?php if(!empty($Entidade)): ?>
                                                    <div class="table-scrollable">
                                                        <table class="table table-striped table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th> # </th>
                                                                    <th> Entidade </th>
                                                                    <th> Início Vínculo </th>
                                                                    <th> Fim Vínculo </th>
                                                                    <th> Status </th>
                                                                    <th> * </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                                <?php
                                                                    foreach ($Entidade as $key => $value):
                                                                        $NomeRazaoSocial    =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes2_id, 1, 'pes_nome_razao_social');
                                                                ?>
                                                                <tr>
                                                                    <td> <?php echo $value->vin_id; ?> </td>
                                                                    <td> <?php echo $NomeRazaoSocial; ?> </td>
                                                                    <td> <?php echo $this->my_data->ConverterData($value->criado, 'ISO', 'PT-BR'); ?> </td>
                                                                    <td> - </td>
                                                                    <td>                                   
                                                                        <span class="label label-sm tooltips label-<?php echo GetBootstrapColor($value->fk_sta_id); ?>" data-original-title="<?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $value->fk_sta_id, 1, 'sta_status'); ?>" >
                                                                        <?php echo GetStatusVinculo($value->fk_sta_id); ?>
                                                                        </span>
                                                                    </td>
                                                                    <td>
                                                                        <a href="#" title="" class="vinculo-detalhe-link" data-id="<?php echo $value->vin_id; ?>" data-nome-empresa="<?php echo $NomeRazaoSocial; ?>" data-id-empresa="<?php echo $value->fk_pes2_id; ?>">
                                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                            Histórico
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                                <?php endforeach; ?>


                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <?php endif; ?>

                                                </div>
                                                <!-- /Entidade Equestre -->



                                            </div>
                                        </div>

                                    </div>
                                    <!-- // vinculos -->

                                </div>
                            </div>


                        </div>
                        <?php endif; ?>




                    </div>


                    <!-- 
                    EMPRESAS
                    =========================================== -->
                    <div class="tab-pane" id="tab_empresas">

                        <?php if(empty($Empresas)): ?>
                        <div class="alert alert-warning"> <strong>Atenção!</strong> Nenhum vínculo encontrado. </div>
                        <?php endif; ?>

                        <?php if(!empty($Empresas)): ?>
                        <div class="portlet light" style="padding-top: 0;">
                            <div class="portlet-body">
                                <div class="table-scrollable">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th> ID Vínculo </th>
                                                <th> ID Autor </th>
                                                <th> Empresa </th>
                                                <th> Pessoa </th>
                                                <th> Tipo Vínculo </th>
                                                <th> Status </th>
                                                <th> Criado </th>
                                                <th> Modificado </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($Empresas as $value): ?>
                                            <tr>
                                                <td> #<?php echo $value->vin_id; ?> </td>
                                                <td> #<?php echo $value->fk_aut_id; ?> </td>
                                                <td>
                                                    <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes2_id, 1, 'pes_nome_razao_social'); ?> <br> 
                                                    <small><b>ID:</b> #<?php echo $value->fk_pes2_id; ?></small><br>
                                                    <small>
                                                        <b>CNPJ:</b>
                                                        <?php
                                                            $Cnpj   =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes2_id, 1, 'pes_cpf_cnpj');
                                                            echo $this->my_pessoa->InserirPontuacaoCpfCnpj( $Cnpj );
                                                        ?>
                                                    </small>
                                                </td>
                                                <td>
                                                    Gustavo Mendes Botega <br> 
                                                    <small><b>ID:</b> #<?php echo $value->fk_pes1_id; ?></small><br>
                                                    <small>
                                                        <b>CPF:</b>
                                                        <?php
                                                            $Cpf   =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes1_id, 1, 'pes_cpf_cnpj');
                                                            echo $this->my_pessoa->InserirPontuacaoCpfCnpj( $Cpf );
                                                        ?>
                                                    </small>
                                                </td>
                                                <td> <?php echo $this->model_crud->get_rowSpecific('tb_vinculo_tipo', 'vit_id', $value->fk_tip_id, 1, 'vit_tipo_vinculo'); ?> </td>

                                                <td>
                                                    <?php
                                                        if($value->fk_sta_id=='1')
                                                            echo '<span class="label label-sm label-info"> Ativo </span>';

                                                        if($value->fk_sta_id=='2')
                                                            echo '<span class="label label-sm label-default"> Inativo </span>';
                                                    ?>
                                                    
                                                </td>


                                                <td> <?php echo $this->my_data->GetDateTimeIso($value->criado, 'Date'); ?> <br> <small><?php echo $this->my_data->GetDateTimeIso($value->criado, 'Time'); ?></small> </td>
                                                <td> <?php echo $this->my_data->GetDateTimeIso($value->modificado, 'Date'); ?> <br> <small><?php echo $this->my_data->GetDateTimeIso($value->modificado, 'Time'); ?></small> </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>

                    </div>




                </div>
            </div>





            <?php endif; ?>




        </div>
        <!-- /portlet -->















    <!-- 
    MODAL ( DETALHES DO VINCULO )
    ==================================== -->
    <div class="modal fade bs-modal-lg" id="vinculo-detalhe" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title bold">CITTAC - Centro Integrado de Treinamento dos Trotadores de Águas Claras    </h4>
                    <div>
                        <small>
                            <b>ID Vínculo:</b> #<span id="vinculo-detalhe-id"></span> | 
                            <b>ID Empresa:</b> #<span id="vinculo-detalhe-id-empresa"></span>
                        </small>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="modal-body">

                        <div id="vinculo-detalhe-carregando" style="text-align: center; color: #008076; ">
                            <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
                            <h3 style="display: inline; ">Aguarde, carregando...</h3>
                        </div>

                        <div class="table-scrollable" id="vinculo-detalhe-tabela" style="display: none;">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th> # </th>
                                        <th> Autor </th>
                                        <th> Atualizado em </th>
                                        <th> Status </th>
                                        <th> Observação </th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <tr>
                                        <td> 137 </td>
                                        <td> Cida Aparecida </td>
                                        <td> 14/01/2017 </td>
                                        <td>
                                            <span class="label label-sm label-danger"> Vínculo Cancelado </span>
                                        </td>
                                        <td> <a href="#" title=""><i class="fa fa-plus-circle" aria-hidden="true"></i> Observação</a> </td>
                                    </tr>

                                    <tr>
                                        <td> 67 </td>
                                        <td> Carla Rosana de Paula </td>
                                        <td> 03/03/2016 </td>
                                        <td>
                                            <span class="label label-sm label-success"> Vínculo Aprovado </span>
                                        </td>
                                        <td> <a href="#" title=""><i class="fa fa-plus-circle" aria-hidden="true"></i> Observação</a> </td>
                                    </tr>

                                    <tr>
                                        <td> 53 </td>
                                        <td> Carla Rosana de Paula </td>
                                        <td> 01/03/2016 </td>
                                        <td>
                                            <span class="label label-sm label-warning"> Vínculo Rejeitado </span>
                                        </td>
                                        <td> <a href="#" title=""><i class="fa fa-plus-circle" aria-hidden="true"></i> Observação</a> </td>
                                    </tr>

                                    <tr>
                                        <td> 37 </td>
                                        <td> Gustavo Mendes Botega </td>
                                        <td> 28/02/2016 </td>
                                        <td>
                                            <span class="label label-sm label-default"> Aguardando Validação </span>
                                        </td>
                                        <td> - </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /table-scrollable -->

                    </div>                    


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn green" data-dismiss="modal">Fechar</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
