


        <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        
                        <p class="text-center" style="width:80%;margin:0 auto;">No SIGEPE existem diversos tipos de cadastro. <br> Vamos direcionar você para o cadastro certo.</p>
                        <h1 class="text-center">Você é um(a) atleta?</h1>

                        <hr>

                        <div class="text-center">
                            <button type="button" class="btn default btn-lg" onclick="location.href = 'FrontOffice/Pessoa/CadastroAtleta';">Sim</button>
                            <button type="button" class="btn default btn-lg"onclick="location.href = 'FrontOffice/Pessoa/CadastroPessoaFisica';">Não</button>
                        </div>

                        <br>
                        
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->



        <div class="copyright"> <?php echo date("Y"); ?> © Desenvolvido por <a href="#" title="">M/Botega | Tecnologia</a> </div>


        <!--[if lt IE 9]>
            <script src="{base_url}assets/global/plugins/respond.min.js"></script>
            <script src="{base_url}assets/global/plugins/excanvas.min.js"></script> 
            <script src="{base_url}assets/global/plugins/ie8.fix.min.js"></script> 
        <![endif]-->


        <!-- Variable Environment Javascripts
        ============================================= -->
        <script type="text/javascript">

        /* Template */
        var BaseUrl                         =   '{base_url}';

        </script>


        <!-- BEGIN CORE PLUGINS -->
        <script src="{base_url}assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="{base_url}assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{base_url}assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="{base_url}assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="{base_url}assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="{base_url}assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>

        <!-- Form Validation -->
        <script src="{base_url}assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="{base_url}assets/global/plugins/jquery-validation/js/additional-methods.js" type="text/javascript"></script>
        <script src="{base_url}assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>


        <script src="{base_url}assets/global/stretch/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js" type="text/javascript"></script>
        <script src="{base_url}assets/global/stretch/jQuery-Mask-Plugin-master/dist/trigger.js" type="text/javascript"></script>

        <script src="{base_url}assets/sigepe/Packages/Scripts/Autenticacao/GuestOffice/FormValidation.js" type="text/javascript"></script>
        <script src="{base_url}assets/sigepe/Packages/Scripts/Autenticacao/GuestOffice/ResetarSenha.js" type="text/javascript"></script>
        <script src="{base_url}assets/sigepe/Packages/Scripts/Pessoa/GuestOffice/Pessoa.js" type="text/javascript"></script>


        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{base_url}assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="{base_url}assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="{base_url}assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->

        <script src="{base_url}assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script>




        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{base_url}assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->




        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{base_url}assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->


        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{base_url}assets/pages/scripts/ui-modals.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->


        <script src="{base_url}assets/pages/scripts/ui-sweetalert.min.js" type="text/javascript"></script>


        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{base_url}assets/pages/scripts/login.min.js" type="text/javascript"></script>

        <!--  FORM-WIZARD -->
        <script src="{base_url}assets/sigepe/Packages/Scripts/Autenticacao/GuestOffice/Autenticacao.js" type="text/javascript"></script>




        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
        <script>
            $(document).ready(function()
            {
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
            })
        </script>
    </body>

</html>