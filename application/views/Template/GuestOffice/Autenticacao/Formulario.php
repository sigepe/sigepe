
        
        <?php if($this->uri->segment(2) == 'inscricao'): ?>
            <div class="note note-warning" style="margin: 20px;">
                <h4 class="block" style="font-weight: bold; margin-bottom: 0px;">
                Atenção. Para continuar a inscrição...</h4> <p>é necessário que você seja um usuário ativo do SIGEPE. <br>Caso não tenha conta clique no botão <b>Criar uma Conta.</b><br>Se já for usuário identifique-se para continuar com sua inscrição.</p>
            </div>
        <?php endif; ?>



        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="#">
                <img src="{base_url}assets/pages/img/logo-sigepe.png" style="opacity: 0.9;" alt="" /> </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" method="post" id="form-authentication">
                <div class="form-title">
                    <span class="form-title">Bem-vindo.</span>
                    <span class="form-subtitle">Por favor, identifique-se.</span>
                </div>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Preencha o formulário corretamente. </span>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9" id="label-cpf">CPF</label>
                    <input class="form-control form-control-solid placeholder-no-fix mask-cpf" type="text" autocomplete="off" placeholder="CPF" id="cpf" name="cpf" /> </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Senha</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Senha" name="senha" /> </div>
                <div class="form-actions">
                    <button type="submit" class="btn red btn-block uppercase">Entrar</button>
                </div>
                <div class="form-actions">
                    <div class="pull-left forget-password-block">
                        <a href="javascript:;" id="cpf-isento">Não tenho CPF</a>
                    </div>
                    <div class="pull-right forget-password-block">
                        <a href="javascript:;" id="forget-password" class="forget-password">Esqueceu sua senha?</a>
                    </div>
                </div>

                <p>
                <a class="btn-primary btn" data-toggle="modal" href="#basic" style="background: white; color: darkcyan !important; border: 2px solid #316180;display: block;">Criar uma Conta</a>
                </p>

            </form>
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
            <form class="forget-form" action="#" method="post" id="form-resetar-senha">

                <div class="form-title">
                    <span class="form-title">Esqueceu sua senha?</span>
                    <span class="form-subtitle">Digite seu CPF.</span>
                </div>

                <div class="form-group">
                    <input class="form-control placeholder-no-fix mask-cpf" type="text" autocomplete="off" placeholder="CPF" id="cpf-resetar-senha" name="cpf-resetar-senha" />
                </div>

                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn btn-default">VOLTAR</button>
                    <button type="submit" class="btn btn-primary uppercase pull-right" style="color:white;">ENVIAR</button>
                </div>

            </form>
            <!-- END FORGOT PASSWORD FORM -->

        </div>

        

