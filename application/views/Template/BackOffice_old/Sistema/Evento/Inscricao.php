    <style type="text/css">

      #lista-serie-inscricao {
        float: left;
      }


        .sigepe .tiles .tile{
            overflow: initial !important;
        }

        .tiles .tile{
            border: none !important;
        }

        .tiles .tile .tile-body{
            margin-bottom: 0 !important;
        }




/*
       .sigepe .tiles .tile:after{
            content: "1";
            background: red;
            width: 100%;
            height: 30px;
        }
*/

        .tiles-empresas .tile{
            margin-bottom: 60px !important;
        }
        .sigepe .tiles .tile{
            width: 100% !important;
            margin-bottom: 0px;
        }
        .sigepe .tiles .tile .tile-object>.name{
            position: relative !important;
            margin-left: 0px !important;
            margin-right: 0px !important;
            text-align: center;
        }
        .sigepe .tiles{
            margin-right: 0px !important;
        }


        .btn.blue:not(.btn-outline) {
            color: #FFF;
            background-color: #3598dc;
            border-color: #3598dc;
            width: 100%;
            white-space: normal;
        }


    </style>


    <div class="col-sm-9 evento-conteudo" id="evento-inscricoes">


      <div class="portlet light bordered">
          <div class="portlet-title tabbable-line">
              <div class="caption">
                  <i class="icon-pin font-yellow-lemon"></i>
                  <span class="caption-subject bold font-yellow-lemon uppercase"> Inscrições </span>
                  <span class="caption-helper">Relação de todos os inscritos</span>
              </div>
              <ul class="nav nav-tabs">
                  <li class="active">
                      <a href="#portlet_serie" data-toggle="tab" aria-expanded="true"> Série </a>
                  </li>
                  <li>
                      <a href="#portlet_prova" data-toggle="tab"> Prova </a>
                  </li>
              </ul>
          </div>
          <div class="portlet-body">
              <div class="tab-content">

                  <!-- PORTLET DE INSCRICOES POR SERIE -->
                  <div class="tab-pane active" id="portlet_serie">


                      <div class="portlet-body">

                          <div class="row">
                              <div class="col-md-2 col-sm-2 col-xs-2">
                                  <ul class="nav nav-tabs tabs-left">


                                      <?php
                                          $Contador   =   1;
                                          foreach ($DatasetSerie as $key => $value):
                                      ?>

                                      <li class="<?php echo ($Contador===1) ? 'active' : ''; ?>">
                                          <a href="#tab-<?php echo $value->ers_id; ?>" data-toggle="tab" class="tooltips" data-container="body" data-placement="left" data-original-title="<?php echo $value->ers_nome; ?>">
                                              <i class="fa fa-caret-right" aria-hidden="true"></i>
                                              <?php echo $value->evs_altura . 'M'; ?> <br>
                                          </a>
                                      </li>
                                      <?php
                                          $Contador++;
                                          endforeach;
                                      ?>


                                  </ul>
                              </div>

                              <div class="col-md-10 col-sm-10 col-xs-10">

                                  <div class="tab-content">



                                      <?php
                                          $Contador = 1;
                                          foreach ($DatasetSerie as $key => $ValueSerie):
                                      ?>

                                      <div class="tab-pane" id="tab-<?php echo $ValueSerie->ers_id; ?>">
                                          <h4 style="font-weight: 600; padding-top:0px;margin-top:0px;">
                                            <?php echo $ValueSerie->evs_altura . 'M'; ?> - <?php echo $ValueSerie->ers_nome; ?>
                                          </h4>
                                          <hr>


                                          <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                                              <thead>
                                                  <tr class="">
                                                      <th> Inscrição </th>
                                                      <th> Conjunto </th>
                                                      <th> Status <br> <small>Financeiro</small> </th>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                  <?php
                                                    foreach ($DatasetInscricoesPorSerie as $key => $Inscricao):
                                                        if($Inscricao->fk_ers_id != $ValueSerie->ers_id)
                                                          continue;
                                                  ?>
                                                  <tr>
                                                      <td> <?php echo $Inscricao->frf_controle; ?> </td>
                                                      <td>
                                                        <b> <?php echo $Inscricao->pes_nome_razao_social; ?> </b>
                                                        <small>
                                                            (
                                                              <?php
                                                                  $NomeEmpresa  =  $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $Inscricao->fk_pje_id, 1, 'pes_nome_razao_social');
                                                                  $Arr = explode(" ", $NomeEmpresa);
                                                                  echo $Arr[0];
                                                                  if(empty($Arr[0]))
                                                                    echo "Sem Entidade";
                                                              ?>
                                                            /
                                                            <?php echo $Inscricao->evc_sigla; ?> )
                                                        </small>
                                                        <br>
                                                        <b> <?php echo $Inscricao->ani_nome_completo; ?></b>
                                                        <small>
                                                          (
                                                          <?php
                                                              $NomeEmpresa  =  $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $Inscricao->AnimalEntidadeId, 1, 'pes_nome_razao_social');
                                                              $Arr = explode(" ", $NomeEmpresa);
                                                              echo $Arr[0];
                                                          ?>
                                                          )
                                                        </small>
                                                      </td>
                                                      <td>
                                                          -
                                                      </td>
                                                  </tr>
                                                  <?php endforeach; ?>
                                              </tbody>
                                          </table>





                                      </div>

                                      <?php
                                          $Contador++;
                                          endforeach;
                                      ?>

                                  </div>
                                  <!-- /tab-content -->

                              </div>
                              <!-- /col-md-9 -->

                          </div>
                          <!-- /row -->

                      </div>
                      <!-- /portlet-body -->











                              <!--
                              // Serie entra como parametro de where.

//                                WHERE
                                    - ID DO Evento
                                    - ID Da Serie / da Prova
                              -->



                  </div>
                  <!-- FIM DO PORTLET POR SERIE -->


                  <div class="tab-pane " id="portlet_prova">

                    PROVA PORLET

                    <div class="portlet-body">

                        <div class="row">
                            <div class="col-md-2 col-sm-2 col-xs-2">
                                <ul class="nav nav-tabs tabs-left">

                                    <?php
                                        $Contador = 1;
                                        foreach ($DatasetProva as $key => $value):
                                    ?>
                                    <li class="<?php echo ($Contador == '1') ? 'active' : ''; ?>">
                                        <a href="#tab_<?php echo $value->srp_id; ?>" data-toggle="tab">
                                            Pr. <?php echo ($value->srp_numero_prova < 10 ) ? '0' . $value->srp_numero_prova : $value->srp_numero_prova; ?>
                                        </a>
                                    </li>
                                    <?php
                                        $Contador++;
                                        endforeach;
                                    ?>

                                </ul>
                            </div>

                            <div class="col-md-10 col-sm-10 col-xs-10">

                                <div class="tab-content">

                                    <?php
                                        $Contador = 1;
                                        foreach ($DatasetProva as $key => $value):
                                    ?>
                                    <?php
                                        $Contador++;
                                        endforeach;
                                    ?>

                                </div>
                                <!-- /tab-content -->

                            </div>
                            <!-- /col-md-9 -->

                        </div>
                        <!-- /row -->

                    </div>
                    <!-- /portlet-body -->

                  </div>
              </div>
          </div>
      </div>




</div>
<!-- /evento-inscricoes -->
