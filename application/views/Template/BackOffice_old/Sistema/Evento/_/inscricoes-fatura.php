
<link href="{base_url}assets/apps/css/todo-2.min.css" rel="stylesheet" type="text/css" />

<style type="text/css">
.profile-usertitle{
    margin-top: 5px;
}
.profile-usertitle-name{
    font-size: 17px !important;
}
#vanguarda-content-inscricoes .portlet.light.bordered{
    border: 0px !important;
    margin-bottom: 0px !important;
    padding-bottom: 0px !important;
}
#vanguarda-content-inscricoes .portlet.light.bordered>.portlet-title{
    margin-bottom: 0;   
}
#vanguarda-content-inscricoes .active.nav-tabs-pendentes a{
    font-weight: bold;
    color: red;
}
#vanguarda-content-inscricoes .active.nav-tabs-pago{
    border-top: 3px solid #00d200 !important;
}
#vanguarda-content-inscricoes .active.nav-tabs-pago a{
    color: #00d200 !important;
    font-weight: bold;
}
#vanguarda-content-inscricoes .active.nav-tabs-pago a{
    color: #00d200 !important;
    font-weight: bold;
}
#vanguarda-content-inscricoes .active.nav-tabs-cancelado a{
    color: #ccc !important;
    font-weight: bold;
}
#vanguarda-content-inscricoes .active.nav-tabs-cancelado{
    border-top: 3px solid #ccc !important;
}
.table>tbody>tr>td, .table>tbody>tr>th{
    padding: 2px;
}
</style>



<script type="text/javascript">
$(document).ready(function(){
    
    $(".btn-gerenciar-conjunto").click(function(){
        $(this).parent().parent().find('.tab-gerenciar-conjunto').slideDown();
    });
    
});
</script>


        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="{base_url}assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->

                                        <div class="row">



                                            <div class="col-md-12">
                                                <!-- BEGIN PROFILE SIDEBAR -->
                                                <div class="profile-sidebar">
                                                    <!-- PORTLET MAIN -->
                                                    <div class="portlet light profile-sidebar-portlet ">
                                                        <!-- SIDEBAR USERPIC -->
                                                        <div class="profile-userpic">
                                                            <img src="http://fhbr.com.br/uploads/evento/Destaque_CBrS_Amazonas.png" class="img-responsive" alt=""> </div>
                                                        <!-- END SIDEBAR USERPIC -->
                                                        <!-- SIDEBAR USER TITLE -->
                                                        <div class="profile-usertitle">
                                                            <div class="profile-usertitle-name"> Campeonato Brasiliense de Salto para Amazonas </div>
                                                            <div class="profile-usertitle-job"> Modalidade: Salto </div>
                                                        </div>
                                                        <!-- END SIDEBAR USER TITLE -->
                                                        <!-- SIDEBAR MENU -->
                                                        <div class="profile-usermenu">
                                                            <ul class="nav">
                                                                <li>
                                                                    <a href="#">
                                                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                                        Home
                                                                    </a>
                                                                </li>
                                                                <li class="active">
                                                                    <a href="#">
                                                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                                        Inscrições
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                                        Baias
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                                        Quartos de Sela
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="{base_url}engine/engine/financeiroEvento">
                                                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                                        Financeiro
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                                        Ajuda
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <!-- END MENU -->
                                                    </div>
                                                    <!-- END PORTLET MAIN -->
                                                    <!-- PORTLET MAIN -->
                                                    <div class="portlet light ">
                                                        <!-- STAT -->
                                                        <div class="row list-separated profile-stat">
                                                            <div class="col-md-4 col-sm-4 col-xs-6">
                                                                <div class="uppercase profile-stat-title"> 37 </div>
                                                                <div class="uppercase profile-stat-text"> Projects </div>
                                                            </div>
                                                            <div class="col-md-4 col-sm-4 col-xs-6">
                                                                <div class="uppercase profile-stat-title"> 51 </div>
                                                                <div class="uppercase profile-stat-text"> Tasks </div>
                                                            </div>
                                                            <div class="col-md-4 col-sm-4 col-xs-6">
                                                                <div class="uppercase profile-stat-title"> 61 </div>
                                                                <div class="uppercase profile-stat-text"> Uploads </div>
                                                            </div>
                                                        </div>
                                                        <!-- END STAT -->
                                                        <div>
                                                            <h4 class="profile-desc-title">About Marcus Doe</h4>
                                                            <span class="profile-desc-text"> Lorem ipsum dolor sit amet diam nonummy nibh dolore. </span>
                                                            <div class="margin-top-20 profile-desc-link">
                                                                <i class="fa fa-globe"></i>
                                                                <a href="http://www.keenthemes.com">www.keenthemes.com</a>
                                                            </div>
                                                            <div class="margin-top-20 profile-desc-link">
                                                                <i class="fa fa-twitter"></i>
                                                                <a href="http://www.twitter.com/keenthemes/">@keenthemes</a>
                                                            </div>
                                                            <div class="margin-top-20 profile-desc-link">
                                                                <i class="fa fa-facebook"></i>
                                                                <a href="http://www.facebook.com/keenthemes/">keenthemes</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- END PORTLET MAIN -->
                                                </div>
                                                <!-- END BEGIN PROFILE SIDEBAR -->
                                                <!-- BEGIN PROFILE CONTENT -->
                                                <div class="profile-content" id="vanguarda-content-inscricoes">
                                                    <div class="row">
                                                        <div class="col-md-12">



                                                            <div class="tabbable-custom ">
                                                                <ul class="nav nav-tabs ">
                                                                    <li class="nav-tabs-cancelado active">
                                                                        <a href="#tab_5_2" data-toggle="tab" aria-expanded="false" style="color: #337ab7 !important;"> Minhas Inscrições </a>
                                                                    </li>
                                                                    <li class="nav-tabs-cancelado">
                                                                        <a href="#tab_5_1" data-toggle="tab" aria-expanded="false"> Inscrições que eu realizei </a>
                                                                    </li>
                                                                </ul>
                                                                <div class="tab-content">
                                                                    <div class="tab-pane active" id="tab_5_1">

                                                                        <div class="todo-tasklist-item todo-tasklist-item-border-green">
                                                                            <div class="todo-tasklist-item-title" style="padding-bottom: 0px;"> FATURA: #1506183132 </div>


                                                                            <!-- Data e Autor -->
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <div class="todo-tasklist-controls pull-left">
                                                                                        <span class="todo-tasklist-date">
                                                                                            <i class="fa fa-calendar"></i> Gerada em 14 Set 2017
                                                                                            por Gustavo Mendes Botega
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>


                                                                            <!-- Preco -->
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <div class="todo-tasklist-controls pull-left">
                                                                                        <span class="todo-tasklist-date">
                                                                                            <i class="fa fa-usd"></i> Preço Total: R$ 1.980,00 </span>
                                                                                        <span class="todo-tasklist-badge badge badge-roundless">Aguardando Pagamento</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>  

                                                                            <hr>

                                                                            <div class="clearfix">
                                                                                <a href="javascript:;" class="btn btn-circle btn-sm blue">
                                                                                    <i class="fa fa-user"></i>
                                                                                    Boleto todas inscrições
                                                                                </a>
                                                                                <a href="javascript:;" class="btn btn-circle btn-sm blue">
                                                                                    <i class="fa fa-user"></i>
                                                                                    Adicionar Conjunto a essa Fatura
                                                                                </a>
                                                                            </div>


                                                                            <div class="tab-gerenciar-conjunto todo-tasklist-item-text" style="display: block;">

                                                                                <div class="table-scrollable">
                                                                                    <table class="table table-striped table-hover">
                                                                                        <thead>
                                                                                            <tr>
                                                                                                <th> Conjunto </th>
                                                                                                <th> Preço </th>
                                                                                                <th> Financeiro </th>
                                                                                                <th> Gerenciar </th>
                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="vertical-align: middle;">
                                                                                                    <img src="http://fhbr.dev/assets/pages/media/users/avatar4.jpg" class="rounded" style="height: 30px;" />
                                                                                                    GUSTAVO 
                                                                                                    <img src="http://4.bp.blogspot.com/_KJM7MbMirrM/TQ6n4nWN68I/AAAAAAAAG7M/Kk33IzENaq4/s400/2011%2BJockey%2BBrian%2BNyawo%2B3.jpg" class="rounded" style="height: 30px;margin-left: 20px;" />
                                                                                                    CZAR
                                                                                                </td>
                                                                                                <td> R$ 870,00 </td>
                                                                                                <td>
                                                                                                    <span class="label label-sm label-default"> Aguardando Pagamento </span>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <a href="javascript:;" class="btn btn-circle btn-sm blue btn-gerenciar-conjunto">  Gerenciar Conjunto </a>
                                                                                                    <a href="javascript:;" class="btn btn-circle btn-sm blue btn-gerenciar-conjunto">  Boleto Inscrição </a>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="vertical-align: middle;">
                                                                                                    <img src="http://fhbr.dev/assets/pages/media/users/avatar4.jpg" class="rounded" style="height: 30px;" />
                                                                                                    GUSTAVO 
                                                                                                    <img src="http://4.bp.blogspot.com/_KJM7MbMirrM/TQ6n4nWN68I/AAAAAAAAG7M/Kk33IzENaq4/s400/2011%2BJockey%2BBrian%2BNyawo%2B3.jpg" class="rounded" style="height: 30px;margin-left: 20px;" />
                                                                                                    ADONINI
                                                                                                </td>
                                                                                                <td> R$ 530,00 </td>
                                                                                                <td>
                                                                                                    <span class="label label-sm label-default"> Aguardando Pagamento </span>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <a href="javascript:;" class="btn btn-circle btn-sm blue btn-gerenciar-conjunto">  Gerenciar Conjunto </a>
                                                                                                    <a href="javascript:;" class="btn btn-circle btn-sm blue btn-gerenciar-conjunto">  Boleto Inscrição </a>
                                                                                                </td>
                                                                                            </tr>                                                                                            

                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>

                                                                            </div>

                                                                        </div>


                                                                    </div>
                                                                    <div class="tab-pane" id="tab_5_2">
                                                                        <p> Howdy, I'm in Section 2. </p>
                                                                        <p> Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate
                                                                            velit esse molestie consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation. </p>
                                                                        <p>
                                                                            <a class="btn green" href="ui_tabs_accordions_navs.html#tab_5_2" target="_blank"> Activate this tab via URL </a>
                                                                        </p>
                                                                    </div>
                                                                    <div class="tab-pane" id="tab_5_3">
                                                                        <p> Howdy, I'm in Section 3. </p>
                                                                        <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                                                                            Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat </p>
                                                                        <p>
                                                                            <a class="btn yellow" href="ui_tabs_accordions_navs.html#tab_5_3" target="_blank"> Activate this tab via URL </a>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>



                                                </div>
                                                <!-- END PROFILE CONTENT -->
                                            </div>
                                        </div>