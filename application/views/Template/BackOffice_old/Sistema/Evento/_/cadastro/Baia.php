<style type="text/css">

.easy-autocomplete-container{
    top: 25px;
}
.easy-autocomplete{
    width: 100% !important;
}
#nome-proprietario{
    text-transform: uppercase;
}
.page-header .page-header-menu.fixed{
    position: relative;
}
.form-actions{
}
.form .form-actions, .portlet-form .form-actions{
    padding: 20px !important;
    margin: 0 !important;
    background-color: #f5f5f5 !important;
    border-top: 1px solid #e7ecf1 !important;     
}

</style>


    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> Evento</span>
                <span class="caption-helper">Formulário de Cadastro</span>
            </div>
        </div>
        <div class="portlet-body">


            <form class="form form-horizontal" role="form">
                <div class="form-body">


                    <!-- MODALIDADE -->
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            Modalidade
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select class="form-control" name="raca" id="raca">
                                <option value="">- Selecione uma Modalidade -</option>
                                <?php
                                    foreach ($DatasetRaca as $key => $value):
                                ?>
                                <option value="<?php echo $value->anr_id; ?>"><?php echo $value->anr_raca; ?></option>
                                <?php
                                    endforeach;
                                ?>=
                            </select>      
                        </div>
                    </div>


                    <!-- NOME EVENTO -->
                    <div class="form-group">
                        <label class="control-label col-md-3">Nome do Evento <span class="required" aria-required="true">
                        * </span>
                        </label>
                        <div class="col-md-6">
                            <textarea name="nome" cols="40" rows="4" style="width:100%;"></textarea>
                        </div>
                    </div>
            

                    <!-- TIPO DE EVENTO -->
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            Tipo de Evento
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select class="form-control" name="raca" id="raca" multiple>
                                <?php
                                    foreach ($DatasetTipoEvento as $key => $value):
                                ?>
                                <option value="<?php echo $value->anr_id; ?>"><?php echo $value->anr_raca; ?></option>
                                <?php
                                    endforeach;
                                ?>
                            </select>      
                        </div>
                    </div>
            

                    <!-- PERIODO -->
                    <div class="form-group">
                        <label class="control-label col-md-3">Periodo</label>
                        <div class="col-md-9">
                            <div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                <input type="text" class="form-control" name="data-inicio">
                                <span class="input-group-addon"> ate </span>
                                <input type="text" class="form-control" name="data-fim"> </div>
                            <!-- /input-group -->
                        </div>
                    </div>
                    


                    <!-- DATA LIMITE SEM ACRESCIMO  -->
                    <div class="form-group">
                        <label class="control-label col-md-3">Data Limite s/ Acrescimo</label>
                        <div class="col-md-9">

                            <div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
                                <input type="text" class="form-control" name="data-limite-sem-acrescimo" readonly>
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>

                        </div>
                    </div>
            

                    <!-- VENDA DE INSCRICOES POR -->
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            Venda de Inscrições por
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select class="form-control" name="tipo-inscricao" id="tipo-inscricao">
                                <option value="">- Selecione um Tipo -</option>
                                <?php foreach ($DatasetTipoInscricao as $key => $value): ?>
                                <option value="<?php echo $value->anr_id; ?>"><?php echo $value->anr_raca; ?></option>
                                <?php endforeach; ?>
                            </select>      
                        </div>
                    </div>
                    

                    <!-- DESENHADOR DE PERCURSO -->
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            Desenhador de Percurso
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select class="form-control" name="desenhador-percurso" id="desenhador-percurso" multiple>
                                <option value="">- Selecione os Desenhadores de Percurso -</option>
                                <?php foreach ($DatasetDesenhadorPercurso as $key => $value): ?>
                                <option value="<?php echo $value->anr_id; ?>"><?php echo $value->anr_raca; ?></option>
                                <?php endforeach; ?>
                            </select>      
                        </div>
                    </div>

            

            <!-- BAIAS -->
            <div class="form-group">
                <label class="control-label col-md-3">Baias</label>
                <div class="col-md-9">
                    <div class="form-control" style="border:none;">
                        
                        <input type="radio" name="baia" value="1" id="baia-sim">
                        <label for="baia-sim" style="margin-right: 15px;">SIM</label>

                        <input type="radio" name="baia" value="2" id="baia-nao">
                        <label for="baia-nao">NÃO</label>

                    </div>
                    <!-- /input-group -->

                    <div class="panel panel-default" style="display: none;">
                        <div class="panel-heading">
                            <h3 class="panel-title">Gestão da Baia</h3>
                        </div>
                        <div class="panel-body">
                            
                            <span class="help-block">
                                Marque SIM, para todos os competidores nos eventos do tipo Nacional e Brasileiro, será obrigatório comprar baia. 
                                Desmarque as entidades locais que não devem ser obrigatório.
                             </span>

                            <h5>Entidades Filiadas</h5>

                            <input type="checkbox" name="baia-entidade-filiada" value="1" id="baia-entidade-filiada-1">
                            <label for="baia-entidade-filiada-1" style="margin-right: 15px;">RCG</label>

                            <hr style="margin: 10px 0;">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <h5>Quantidade</h5>
                                            <div class="input-group">
                                                <input type="number" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3" style="margin:0 20px;">
                                        <div class="form-group">
                                            <h5>Valor sem acréscimo</h5>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    R$
                                                </span>
                                                <input type="text" class="form-control" placeholder=""> </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <h5>Valor com acréscimo</h5>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    R$
                                                </span>
                                                <input type="text" class="form-control" placeholder=""> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr style="margin: 10px 0;">

                            <div class="row">
                                <div class="col-sm-12">
                                    <h5>Bloqueio</h5>
                                    <input type="checkbox" name="baia-entidade-filiada" value="1" id="baia-entidade-filiada-1">
                                    <label for="baia-entidade-filiada-1">Bloquar venda de baia se exceder quantidade.</label>
                                </div>
                            </div>

                        </div>
                        <!-- /panel-body -->

                    </div>
                    <!-- /panel -->


                </div>
            </div>
            <!-- fim baia -->









            <!-- QUARTO DE SELA -->
            <div class="form-group">
                <label class="control-label col-md-3">Quarto de Sela</label>
                <div class="col-md-9">
                    <div class="form-control" style="border:none;">
                        
                        <input type="radio" name="baia" value="1" id="qs-sim">
                        <label for="baia-sim" style="margin-right: 15px;">SIM</label>

                        <input type="radio" name="baia" value="2" id="qs-nao">
                        <label for="baia-nao">NÃO</label>


                    </div>
                    <!-- /input-group -->

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Gestão do Quarto de Sela</h3>
                        </div>
                        <div class="panel-body">
                            

                            <div class="row">

                                <div class="col-sm-12">

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <h5>Quantidade</h5>
                                            <div class="input-group">
                                                <input type="number" class="form-control">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-sm-3" style="margin:0 20px;">
                                        <div class="form-group">
                                            <h5>Valor sem acréscimo</h5>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    R$
                                                </span>
                                                <input type="text" class="form-control" placeholder=""> </div>
                                        </div>
                                    </div>


                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <h5>Valor com acréscimo</h5>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    R$
                                                </span>
                                                <input type="text" class="form-control" placeholder=""> </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <hr>

                            <div class="row">

                                <div class="col-sm-12">
                                    <h5>Bloqueio</h5>
                                    <input type="checkbox" name="baia-entidade-filiada" value="1" id="baia-entidade-filiada-1">
                                    <label for="baia-entidade-filiada-1">Bloquar venda de baia se exceder quantidade.</label>

                                </div>

                            </div>


                        </div>
                    </div>


                    


                </div>
            </div>
            <!-- fim qs -->


            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-yellow-crusta">
                        <i class="icon-share font-yellow-crusta"></i>
                        <span class="caption-subject bold uppercase"> SITE</span>
                    </div>
                </div>
                <div class="portlet-body">


                    <!-- Ativar no Site -->
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            Ativar no Site
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <div class="form-control" style="border:none;">
                                <input type="radio" name="baia" value="1" id="baia-sim">
                                <label for="baia-sim" style="margin-right: 15px;">SIM</label>

                                <input type="radio" name="baia" value="2" id="baia-nao">
                                <label for="baia-nao">NÃO</label>
                            </div>
                        </div>
                    </div>


                    <!-- Local -->
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            Local
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            
                            <select class="form-control" name="raca" id="raca">
                                <option value="">- Selecione um Local -</option>
                                <?php
                                    foreach ($DatasetRaca as $key => $value):
                                ?>
                                <option value="<?php echo $value->anr_id; ?>"><?php echo $value->anr_raca; ?></option>
                                <?php
                                    endforeach;
                                ?>=
                            </select>      

                        </div>
                    </div>

                    <!-- Programa -->
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            Programa
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="input-group input-large">
                                    <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                        <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                        <span class="fileinput-filename"> </span>
                                    </div>
                                    <span class="input-group-addon btn default btn-file">
                                        <span class="fileinput-new"> Select file </span>
                                        <span class="fileinput-exists"> Change </span>
                                        <input type="file" name="..."> </span>
                                    <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!-- Logotipo -->
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            Logotipo
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="input-group input-large">
                                    <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                        <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                        <span class="fileinput-filename"> </span>
                                    </div>
                                    <span class="input-group-addon btn default btn-file">
                                        <span class="fileinput-new"> Select file </span>
                                        <span class="fileinput-exists"> Change </span>
                                        <input type="file" name="..."> </span>
                                    <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                </div>
                            </div>

                            <span class="help-block"> Envie a imagem na resolução 400x400 (pixels). </span>


                        </div>
                    </div>

                    <!-- Abertura das Inscricoes -->
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            Abertura das Inscrições
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <div class="input-group date form_datetime form_datetime bs-datetime">
                                <input type="text" size="16" class="form-control">
                                <span class="input-group-addon">
                                    <button class="btn default date-set" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>

                        </div>
                    </div>


                    <!-- Encerramento das Inscricoes -->
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            Encerramento das Inscrições
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <div class="input-group date form_datetime form_datetime bs-datetime">
                                <input type="text" size="16" class="form-control">
                                <span class="input-group-addon">
                                    <button class="btn default date-set" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>

                        </div>
                    </div>


                    <!-- Descricao do Evento -->
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            Descrição
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <textarea class="wysihtml5 form-control" rows="6"></textarea>
                        </div>
                    </div>









                </div>
                <!-- /form-body -->
                <div class="form-actions">
                    <button type="submit" class="btn green">Cadastrar Evento</button>
                </div>
                <!-- /form-actions -->
            </form>
            <!-- /form -->


        </div>
        <!-- /portlet-body -->

    </div>
    <!-- /portlet -->












