

            <div class="tab-pane" id="tab_<?php echo $value->srp_id; ?>_desenhador">

                <!-- BEGIN BORDERED TABLE PORTLET-->
                <div class="portlet light portlet-fit margin-top-0">

                        <?php
                            $this->load->module('evento/prova');
                            $DatasetDesenhadorDePercursoDaProva      =   $this->prova->GetDesenhadorDePercursoDaProva($value->srp_id);
                            if(empty($DatasetDesenhadorDePercursoDaProva)):
                        ?>
                            <div class="alert alert-warning margin-top-15">
                                <strong>Atenção!</strong> <br>
                                Nenhum desenhador de percurso cadastrado para <b>Prova <?php echo ($categoria->srp_numero_prova < 10 ) ? '0' . $categoria->srp_numero_prova : $categoria->srp_numero_prova; ?></b>.
                            </div>                                                        
                        <?php endif; ?>


                        <?php
                        if(!empty($DatasetDesenhadorDePercursoDaProva)): ?>
                        <div class="table-scrollable table-scrollable-borderless">
                            <table class="table table-hover table-light">
                                <thead>
                                    <tr class="uppercase">
                                        <th> Desenhador </th>
                                        <th class="text-center"> Ações </th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                        
                                        foreach ($DatasetDesenhadorDePercursoDaProva as $key => $desenhador):
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $desenhador->fk_pes_id, 1, 'pes_nome_razao_social'); ?>
                                            <br>
                                            <span class="badge badge-<?php echo (is_null($desenhador->flag_assistente)) ? 'primary' : 'default'; ?>">
                                                <?php echo (is_null($desenhador->flag_assistente)) ? 'Desenhador Principal' : 'Assistente'; ?>
                                            </span>
                                        </td>
                                        <td class="text-center">
                                            
                                            <span
                                                class="btn btn-sm btn-primary btn-circle popovers"
                                                data-container="body"
                                                onclick=" "
                                                data-html="true" 
                                                data-trigger="hover"
                                                data-placement="left"
                                                data-content="
                                                    <small> 
                                                        <b>ID Pessoa do Desenhador:</b><br>
                                                        <?php echo $desenhador->pes_id; ?>
                                                        <hr style='margin:8px 0;'>

                                                        <b>ID do Vínculo ( Desenhador > Prova ):</b><br>
                                                        <?php echo $desenhador->prd_id; ?>
                                                        <hr style='margin:8px 0;'>

                                                        <b>Autor do Registro:</b><br>
                                                        <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $desenhador->fk_aut_id, 1, 'pes_nome_razao_social'); ?> <br>
                                                        ID: <?php echo $desenhador->fk_aut_id; ?>
                                                        <hr style='margin:8px 0;'>

                                                    </small>
                                                "
                                                data-original-title="<b>PMR - Pré Mirim</b>">
                                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                Detalhes
                                            </span>



                                            <a href="#" class="btn btn-sm btn-info btn-circle"><i class="fa fa-pencil" aria-hidden="true"></i> <?php echo (is_null($desenhador->flag_assistente)) ? 'Definir Assistente' : 'Definir Desenhador'; ?></a>
                                            <a href="#" class="btn btn-sm btn-danger btn-circle"><i class="fa fa-trash-o" aria-hidden="true"></i> Deletar</a>
                                        </td>
                                    </tr>
                                    <?php endforeach;  ?>

                                </tbody>
                            </table>
                        </div>
                        <?php endif; ?>


                </div>
                <!-- END BORDERED TABLE PORTLET-->

                <hr>


                <?php
                    $DatasetDesenhadorPercursoExcetoProva       =   $this->prova->GetDesenhadorDePercursoExcetoProva($value->srp_id, $EventoId);
                    if(empty($DatasetDesenhadorPercursoExcetoProva)):
                ?>
                <div class="alert alert-info">
                    <strong>Informativo!</strong> <br>
                    Todos os desenhadores de percurso do evento estão cadastrados para essa prova.
                </div>
                <?php endif; ?>


                <?php if(!empty($DatasetDesenhadorPercursoExcetoProva)): ?>
                <div class="panel-group accordion" id="accordion3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#desenhador_collapse_<?php echo $value->srp_id; ?>"> Desenhadores de Percurso do Evento </a>
                            </h4>
                        </div>
                        <div id="desenhador_collapse_<?php echo $value->srp_id; ?>" class="panel-collapse collapse">
                            <div class="panel-body" style="height:200px; overflow-y:auto;">
                                <select class="form-control" name="desenhador-percurso[]" id="desenhador-percurso" multiple>
                                    <?php
                                       foreach ($DatasetDesenhadorPercursoExcetoProva as $key => $desenhador):
                                    ?>
                                    <option value="<?php echo $desenhador; ?>"><?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $desenhador, 1, 'pes_nome_razao_social'); ?></option>
                                    <?php endforeach; ?>
                                </select>    
                                <a href="#" class="btn btn-primary block margin-top-15">
                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                    Cadastrar
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>



            </div>
            <!-- /tab ( desenhador de percurso ) -->
