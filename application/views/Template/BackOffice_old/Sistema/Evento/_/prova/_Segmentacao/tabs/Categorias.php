
    <div class="tab-pane" id="tab_<?php echo $value->srp_id; ?>_categorias">


        <!-- BEGIN BORDERED TABLE PORTLET-->
        <div class="portlet light portlet-fit margin-top-0">

                <div class="table-scrollable table-scrollable-borderless">
                    <table class="table table-hover table-light">
                        <thead>
                            <tr class="uppercase">
                                <th> Categoria </th>
                                <th> Desconto </th>
                                <th class="text-center"> Ações </th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                                $this->load->module('evento/prova');
                                $CategoriasDaProva      =   $this->prova->GetCategoriasDaProva($value->srp_id);
                                foreach ($CategoriasDaProva as $key => $categoria):
                            ?>
                            <tr>
                                <td>
                                    <?php echo $categoria->evc_sigla; ?>
                                    <br>
                                    <small><?php echo $categoria->evc_categoria; ?></small>
                                </td>
                                <td> <?php echo (!is_null($categoria->prc_desconto)) ? "R$ " . $this->my_moeda->InserirPontuacao($categoria->prc_desconto) : '-' ; ?> </td>
                                <td class="text-center">
                                    <button
                                        class="btn btn-sm btn-primary btn btn-circle popovers"
                                        data-container="body"
                                        onclick=" "
                                        data-html="true" 
                                        data-trigger="hover"
                                        data-placement="left"
                                        data-content="

                                            <small> 
    
                                                <b>Desconto:</b><br>
                                                <?php echo (!is_null($categoria->prc_desconto)) ? "R$ " . $this->my_moeda->InserirPontuacao($categoria->prc_desconto) : '-' ; ?>
                                                <hr style='margin:8px 0;'>

                                                <b>Limitar Inscrições:</b><br>
                                                <?php echo (!is_null($categoria->flag_limite_inscricao)) ? 'Sim' : 'Não'; ?>
                                                <?php echo (!is_null($categoria->flag_limite_inscricao)) ? '( ' . $categoria->prc_limite_inscricao_quantidade . ' ) ' : ''; ?>
                                                
                                                <hr style='margin:8px 0;'>

                                                <b>Utilizar Limite do Atleta:</b><br>
                                                <?php echo (!is_null($categoria->flag_limite_atleta)) ? 'Sim' : 'Não'; ?>
                                                <?php echo (!is_null($categoria->flag_limite_atleta)) ? '( ' . $categoria->prc_limite_atleta_quantidade . ' ) ' : ''; ?>

                                                <hr style='margin:8px 0;'>

                                                <b>ID do Relacionamento:</b><br>
                                                <?php echo $categoria->prc_id; ?>
                                            </small>

                                        "
                                        data-original-title="<b>PMR - Pré Mirim</b>">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        Detalhes
                                    </button>


                                    <a href="#" class="btn btn-sm btn-info" style="border-radius: 28px !important;"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a>
                                    <a href="#" class="btn btn-sm btn-danger" style="border-radius: 28px !important;"><i class="fa fa-trash-o" aria-hidden="true"></i> Deletar</a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
        </div>
        <!-- END BORDERED TABLE PORTLET-->


        <div class="btn-footer-actions" style="background-color: rgba(238, 238, 238, 0.41); padding: 30px; border-top: 2px solid #eee;margin-top: 25px;">
            <a href="#" class="btn btn-primary">Cadastrar Categoria na Prova <?php echo ($categoria->srp_numero_prova < 10 ) ? '0' . $categoria->srp_numero_prova : $categoria->srp_numero_prova; ?> <i class="fa fa-plus-circle" aria-hidden="true"></i> </a>
        </div>


    </div>
    <!-- /tab ( categorias ) -->
