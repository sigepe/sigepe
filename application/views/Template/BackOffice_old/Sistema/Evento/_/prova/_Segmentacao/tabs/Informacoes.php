
<div class="tab-pane active" id="tab_<?php echo $value->srp_id; ?>_informacoes">

    <span class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm active">
        Geral
    </span>

    <!-- DATA/HORA -->
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Data/Hora da Prova:</label>
                <div class="col-md-8">
                    <p class="form-control-static">
                        <?php echo $this->my_data->ConverterData($value->srp_dia, 'ISO', 'PT-BR'); ?>
                        <br>
                        <small class="font-grey-salsa "><?php echo $this->my_data->diasemana($value->srp_dia) . ' às ' . $value->srp_hora; ?> </small>
                    </p>
                </div>
            </div>
        </div>
    </div>


    <!-- TIPO DA PISTA -->
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Tipo da Pista:</label>
                <div class="col-md-8">
                    <p class="form-control-static">
                        <?php echo $this->model_crud->get_rowSpecific('tb_evento_serie_prova_pista', 'spp_id', $value->fk_spp_id, 1, 'spp_pista'); ?>
                    </p>
                </div>
            </div>
        </div>
    </div>


    <!-- TIPO DE SORTEIO -->
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Tipo de Sorteio:</label>
                <div class="col-md-8">
                    <p class="form-control-static">
                        <?php echo $this->model_crud->get_rowSpecific('tb_evento_serie_prova_sorteio', 'sps_id', $value->fk_sps_id, 1, 'sps_sorteio'); ?> 
                        <br>
                        <small class="font-grey-salsa "><?php echo $this->model_crud->get_rowSpecific('tb_evento_serie_prova_sorteio', 'sps_id', $value->fk_sps_id, 1, 'sps_descricao'); ?> </small>
                    </p>
                </div>
            </div>
        </div>
    </div>


    <!-- CARACTERISTICA -->
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Característica da Prova:</label>
                <div class="col-md-8">
                    <p class="form-control-static">
                        <?php echo $this->model_crud->get_rowSpecific('tb_evento_serie_prova_caracteristica', 'spc_id', $value->fk_spc_id, 1, 'spc_caracteristica'); ?> 
                        <br>
                        <small class="font-grey-salsa "><?php echo $this->model_crud->get_rowSpecific('tb_evento_serie_prova_caracteristica', 'spc_id', $value->fk_spc_id, 1, 'spc_regulamento'); ?> </small>
                    </p>
                </div>
            </div>
        </div>
    </div>


    <!-- ID DA PROVA -->
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label col-md-4 text-right form-control-static font-grey-mint">ID da Prova:</label>
                <div class="col-md-8">
                    <p class="form-control-static">
                        #<?php echo $value->srp_id; ?>
                    </p>
                </div>
            </div>
        </div>
    </div>


    <hr>

    <span class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm active">
        Valores e Limites
    </span>


    <!-- VALOR -->
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Valor:</label>
                <div class="col-md-8">
                    <p class="form-control-static">
                        R$ <?php echo $this->my_moeda->InserirPontuacao( $value->srp_valor ); ?> 
                    </p>
                </div>
            </div>
        </div>
    </div>


    <!-- VALOR PROMOCIONAL -->
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Valor Promocional:</label>
                <div class="col-md-8">
                    <p class="form-control-static">
                        <?php if(!empty($value->srp_valor_promocional) || !is_null($value->srp_valor_promocional)): ?>
                            R$ <?php echo $this->my_moeda->InserirPontuacao( $value->srp_valor_promocional ); ?> 
                        <?php else: ?>
                            -
                        <?php endif; ?>
                    </p>
                </div>
            </div>
        </div>
    </div>


    <!-- LIMITE DE INSCRICAO NA PROVA -->
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Limite de Inscrição na Prova:</label>
                <div class="col-md-8">
                    <p class="form-control-static">
                        <?php echo $value->srp_limite_inscricao_prova; ?> 
                    </p>
                </div>
            </div>
        </div>
    </div>



    <!-- LIMITE DE INSCRICAO POR ATLETA -->
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Limite de Inscrição por Atleta:</label>
                <div class="col-md-8">
                    <p class="form-control-static">
                        <?php echo $value->srp_limite_inscricao_atleta; ?> 
                    </p>
                </div>
            </div>
        </div>
    </div>


    <hr>

    <span class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm active">
        Salto
    </span>

    <!-- VELOCIDADE -->
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Velocidade:</label>
                <div class="col-md-8">
                    <p class="form-control-static">
                        <?php echo $value->srp_velocidade; ?>  km/h
                    </p>
                </div>
            </div>
        </div>
    </div>


    <!-- OBSTACULO ( ALTURA ) -->
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Obstáculo ( altura ):</label>
                <div class="col-md-8">
                    <p class="form-control-static">
                        <?php echo $value->srp_obstaculo_altura; ?>  M
                    </p>
                </div>
            </div>
        </div>
    </div>


    <!-- OBSTACULO ( LARGURA ) -->
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Obstáculo ( largura ):</label>
                <div class="col-md-8">
                    <p class="form-control-static">
                        <?php echo $value->srp_obstaculo_largura; ?>  M
                    </p>
                </div>
            </div>
        </div>
    </div>


    <hr>

    <span class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm active">
        Auditoria
    </span>


    <!-- AUTOR -->
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Autor:</label>
                <div class="col-md-8">
                    <p class="form-control-static">
                        <?php echo $this->model_crud->get_rowSpecific( 'tb_pessoa', 'pes_id', $value->fk_aut_id, 1, 'pes_nome_razao_social' ); ?> <br>
                        <small> ID: <?php echo $value->fk_aut_id; ?> </small>
                    </p>
                </div>
            </div>
        </div>
    </div>


    <!-- CRIADO EM -->
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Prova criada em:</label>
                <div class="col-md-8">
                    <p class="form-control-static">
                        <?php echo $this->my_data->ConverterData( $value->criado, 'ISO', 'PT-BR' ); ?>
                    </p>
                </div>
            </div>
        </div>
    </div>


    <!-- ULTIMA ALTERACAO EM -->
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label col-md-4 text-right form-control-static font-grey-mint">Última alteração em:</label>
                <div class="col-md-8">
                    <p class="form-control-static">
                        <?php echo $this->my_data->ConverterData( $value->modificado, 'ISO', 'PT-BR' ); ?>
                    </p>
                </div>
            </div>
        </div>
    </div>


    <div class="btn-footer-actions" style="background-color: rgba(238, 238, 238, 0.41); padding: 30px; border-top: 2px solid #eee;margin-top: 25px;">
        <a href="#" class="btn btn-primary">Editar Informações da Prova <span class="glyphicon glyphicon-edit"></span></a>
    </div>


</div>
<!-- /tab ( informacoes ) -->