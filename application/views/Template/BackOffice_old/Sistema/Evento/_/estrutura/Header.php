<style type="text/css">

.easy-autocomplete-container{
    top: 25px;
}
.easy-autocomplete{
    width: 100% !important;
}
#nome-proprietario{
    text-transform: uppercase;
}
.page-header .page-header-menu.fixed{
    position: relative;
}
.form-actions{
}
.form .form-actions, .portlet-form .form-actions{
    padding: 20px !important;
    margin: 0 !important;
    background-color: #f5f5f5 !important;
    border-top: 1px solid #e7ecf1 !important;     
}
#baia-error,
#quarto-de-sela-error,
#ativar-site-error{
    display: block;
    width: 100%;
    float: left;    
}

.page-head{
    display: none;
}

.profile-sidebar{
    width: 100%;
}

.dashboard-stat2{
    padding-bottom: 0px !important;
}


/* --------------- */


#nome-evento::first-line {
    font-size: 35px;
    font-weight: bold;
    letter-spacing: -1px;
}


</style>
    
    
<div class="row">


    <div class="col-sm-12 margin-top-10 margin-bottom-25" id="evento-top">
           <img src="{base_url}assets/sigepe/global/images/evento/CSN-Brasilia-Indoor-logo.png" style="width:130px;border-radius: 10% !important;border: 2px solid #fff; box-shadow: 1px 1px 1px #999;margin-right: 35px;float: left;">
           <div class="" style="float: left;">
                <h3 id="nome-evento" style="margin-top: 0;"><?php echo nl2br($NomeEvento); ?></h3>
                <h4>
                    <div type="button" class="btn btn-default btn-sm" style="border-radius: 5px !important;">{Modalidade}</div>
                </h4>
           </div>
    </div>
    <!-- /evento-top -->

