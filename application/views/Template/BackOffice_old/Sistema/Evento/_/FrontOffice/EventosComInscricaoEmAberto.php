<style type="text/css">
.label-primary {
/*white-space: initial;*/
}
</style>


     <!-- Begin: life time stats -->
    <div class="portlet light portlet-fit portlet-datatable ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-caret-right" aria-hidden="true"></i>
                <span class="caption-subject sbold uppercase">Eventos</span>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover" id="sample_3">
                    <thead>
                        <tr>
                            <th> Evento </th>
                            <th> Local </th>
                            <th> Data Início Evento </th>
                            <th> Data Fim Inscrição </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($DatasetEvento as $key => $value): ?>
                        <tr>
                            <td style="padding: 30px 10px;">
                                <a href="{base_url}FrontOffice/Evento/<?php echo $value->eve_id; ?>/#tab-inscricoes" title="Clique para acessar informações de inscrição desse evento">
                                    <img src="{base_url}files/evento/<?php echo $value->eve_controle; ?>/logotipo/<?php echo $value->eve_site_logotipo; ?>" style="width: 100px;display: inline-block;float: left;margin-right: 20px;border: 1px solid #ccc;padding: 1px;">
                                    <?php echo nl2br($value->eve_nome); ?>
                                </a>
                            </td>
                            <td>
                                <?php
                                    $NomeEntidade = $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes_id, 1, 'pes_nome_razao_social');
                                    $NomeEntidade = explode('-', $NomeEntidade);
                                    echo $NomeEntidade[0] . '<br>' . $NomeEntidade[1];
                                ?>
                            </td>
                            <td>
                                <?php echo $this->my_data->ConverterData($value->eve_data_inicio, 'ISO', 'PT-BR'); ?> <br>
                                <span class="badge badge-danger badge-roundless"> <?php echo DateDifferences($value->eve_data_inicio, date("Y-m-d H:i:s"), 'd'); ?> dia(s) restante(s) </span>
                            </td>
                            <td>
                                <?php echo $this->my_data->ConverterData($value->eve_site_inscricao_fim, 'ISO', 'PT-BR'); ?> <br>
                                <span class="badge badge-danger badge-roundless"> <?php echo DateDifferences($value->eve_site_inscricao_fim, date("Y-m-d H:i:s"), 'd'); ?> dia(s) restante(s) </span>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- End: life time stats -->
