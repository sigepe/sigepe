

    <tr>
        <td class="text-right">
            <b>Baia</b> <br>
            <small class="font-grey-cascade"> <?php echo ($DatasetLinhaBaiaQuantidade > 1) ? $DatasetLinhaBaiaQuantidade . ' Unidades' : $DatasetLinhaBaiaQuantidade . ' Unidade'; ?></small>
        </td>
        <td class="text-left">
   

            <?php if($PoliticaPreco == '1' && !is_null($DatasetLinhaBaiaValorPromocional)): ?>
            <small style="text-decoration: line-through;" class="font-grey-cascade">
                R$ <?php echo $this->my_moeda->InserirPontuacao($DatasetLinhaBaiaValor); ?>
            </small>  
            R$ <?php echo $this->my_moeda->InserirPontuacao($DatasetLinhaBaiaValorPromocional); ?>
            <?php endif; ?>


            <?php if($PoliticaPreco == '2' || ( $PoliticaPreco=='1' && is_null($DatasetLinhaBaiaValorPromocional) )  ): ?>
            R$ <?php echo $this->my_moeda->InserirPontuacao($DatasetLinhaBaiaValor); ?>
            <?php endif; ?>
                



        </td>
    </tr>