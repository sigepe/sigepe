    

    <?php foreach ($DatasetLinhaProvaAvulsa as $key => $value): ?>
    <tr>
        <td class="text-right">
        	<b>
                <?php echo $value->srp_nome . ' - ' . $value->srp_obstaculo_altura ?>M
            </b> <br>
        	<small class="font-grey-cascade">
                <?php echo $value->srp_nome_trofeu; ?>
            </small>
        </td>
        <td class="text-left">


            <?php if($PoliticaPreco == '1' && !is_null($value->fri_valor_promocional)): ?>
            <small style="text-decoration: line-through;" class="font-grey-cascade">
                R$ <?php echo $this->my_moeda->InserirPontuacao($value->fri_valor); ?>    
            </small>  
                R$ <?php echo $this->my_moeda->InserirPontuacao($value->fri_valor_promocional); ?> 
            <?php endif; ?>


            <?php if($PoliticaPreco == '2' || ( $PoliticaPreco=='1' && is_null($value->fri_valor_promocional) )  ): ?>
            R$ <?php echo $this->my_moeda->InserirPontuacao($value->fri_valor); ?>    
            <?php endif; ?>


        </td>




    </tr>
    <?php endforeach; ?>
