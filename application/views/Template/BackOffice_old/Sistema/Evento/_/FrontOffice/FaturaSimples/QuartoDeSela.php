
<?php
    $this->load->module('evento/FrontOffice/QuartoDeSela');

    $DatasetQuartosDeSela	=	$this->quartodesela->GetQuartosDeSela($FaturaSimples[0]->frf_id);
	$FlagBaia 				=	$this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $EventoId, 1, 'flag_baia');
	$FlagQuartoDeSela 		=	$this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $EventoId, 1, 'flag_quarto');

	( is_null($FlagBaia) || is_null($FlagQuartoDeSela)) ? $ColunaBootstrap = '12' : $ColunaBootstrap = '6';
?>


	<?php if( !is_null($FlagQuartoDeSela) || !empty($DatasetQuartosDeSela) ): ?>

	<div class="col-sm-<?php echo $ColunaBootstrap; ?>">

	    <h3 style="font-weight: 800; margin-bottom: 23px; font-size: 27px; letter-spacing: -1px;">
	        Q.S.
	        <small>Restam 14 Quartos de Sela</small>

	        <?php if($FlagQuartoDeSela): ?>
			<a href="#" class="btn btn-circle btn-warning btn-sm" style="border-radius: 28px !important;font-weight: 800 !important;letter-spacing:0px;float: right;padding: 5px 40px;">
				<i class="fa fa-plus-circle" aria-hidden="true" style="margin-right: 10px;"></i>
				Comprar Quarto de Sela
			</a>
	        <?php endif; ?>

	     </h3>

	    <div class="portlet light bordered">
	        <div class="portlet-body">

	        	<?php if(empty($DatasetQuartosDeSela)): ?>
	        	<div class="row">
	        		<div class="col-sm-12">
						<div class="alert alert-warning" style="margin-bottom: 0px;">
						    <strong>Atenção!</strong> Não existe nenhuma quarto de sela para essa inscrição. <br>
						    <a href="#" title="">Clique aqui para comprar quarto de sela.</a>
						</div>
					</div>
				</div>
				<?php endif; ?>


	        	<?php if(!empty($DatasetQuartosDeSela)): ?>
	        	<div class="row">
	        		<div class="col-sm-12">

						<table class="table table-striped table-hover" style="margin-bottom: 0px;">
						    <thead>
						        <tr>
						            <th> Identificação </th>
						            <th> Responsável </th>
						            <th> Preço </th>
						            <th class="text-center"> Ação </th>
						        </tr>
						    </thead>
						    <tbody>
						    	<?php foreach ($DatasetQuartosDeSela as $key => $value): ?>
						        <tr>
						            <td>
                                        <span
                                            class="popovers"
                                            data-container="body"
                                            onclick=" "
                                            data-html="true" 
                                            data-trigger="hover"
                                            data-placement="right"
                                            data-content="

                                                <small> 

                                                    <b>Status:</b><br>
                                                    Quarto de sela não está reservada. <br>Aguardando pagamento. 

                                                    <hr style='margin:8px 0;'>

                                                    <b>Quarto de sela adicionada por:</b><br>
                                                    <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_aut_id, 1, 'pes_nome_razao_social') ?>
                                                   
                                                    <hr style='margin:8px 0;'>
      
                                                    <b>Registro criado em:</b><br>
                                                    <?php echo $this->my_data->ConverterData($value->criado, 'ISO', 'PT-BR'); ?>

                                                </small>

                                            "
                                            data-original-title="<b>Identificação</b>">
                                            <i class="fa fa fa-info-circle font-blue" aria-hidden="true"></i>
    						            	<small class="font-grey-gallery"><?php echo $value->frq_controle; ?></small>
                                        </span>
						            </td>
						            <td class="font-grey-gallery">
										<a href="javascript:;" class="tooltips" data-original-title="<?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes_id, 1, 'pes_nome_razao_social') ?>">
											<?php echo FirstWord($this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes_id, 1, 'pes_nome_razao_social')); ?>
										</a>				            	
						            </td>



						            <td class="font-grey-gallery">

						            	<?php if( $PoliticaPreco == '1' && !is_null($value->frq_valor_promocional) ): ?>
						            	<small style="text-decoration: line-through;" class="font-grey-cascade">
								            R$ <?php echo  $this->my_moeda->InserirPontuacao( $value->frq_valor ); ?>
						            	</small>
						            	<br>
							            <?php endif; ?>


							            R$ <?php

						            		if($PoliticaPreco == '1'){
						            			echo (!is_null($value->frq_valor_promocional)) ? $this->my_moeda->InserirPontuacao($value->frq_valor_promocional) : $this->my_moeda->InserirPontuacao($value->frq_valor);
						            		}else{
						            			echo $this->my_moeda->InserirPontuacao($value->frq_valor);
						            		}

						            	?>

						            </td>




						            <td class="text-center">
										<a href="#" class="btn btn-circle btn-danger btn-sm" style="border-radius: 28px !important;font-weight: 400 !important;">
											<i class="fa fa-trash" aria-hidden="true"></i>
											Excluir
										</a>

						            </td>
						        </tr>
							    <?php endforeach; ?>

						    </tbody>
						</table>

					</div>
				</div>
				<?php endif; ?>


	        </div>
	    </div>

	</div>
	<?php endif; ?>





</div>
<!-- /opened baia -->

