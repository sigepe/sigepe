
    <div class="row">    
        <div class="col-sm-12 margin-top-10 margin-bottom-15" id="evento-top">
               <img src="{base_url}files/evento/<?php echo $DatasetEvento[0]->eve_controle; ?>/logotipo/<?php echo $DatasetEvento[0]->eve_site_logotipo; ?>" style="width:130px;border-radius: 10% !important;border: 2px solid #fff; box-shadow: 1px 1px 1px #999;margin-right: 35px;float: left;">
               <div class="" style="float: left;">
                    <h3 id="nome-evento" style="margin-top: 0;"><?php echo nl2br($DatasetEvento[0]->eve_nome); ?></h3>
                    <h4>
                        <div type="button" class="btn btn-default btn-sm" style="border-radius: 5px !important;">
                            <b>Status:</b> <?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $DatasetEvento[0]->fk_sta_id, 1, 'sta_status'); ?>
                        </div>
                    </h4>
               </div>
        </div>
        <!-- /evento-top -->
    </div>

    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:30px;border: 1px dotted #ddd;">

    <div class="row margin-bottom-20">
        <div class="col-sm-4 text-left">
            <h3 style="margin-top: 0px;font-weight: 600;">
                <small class="badge badge-warning bold">Inscrição n°</small><br>
                <span class="font-yellow-crusta" style="display: block; font-size: 38px; margin-top: 3px;">
                    <?php echo $FaturaSimples[0]->frf_controle; ?> 
                </span>
                <div style="display: none">[ AGUARDANDO PAGAMENTO ] ( HOVER INSCRIÇÕES SÓ SERÃO VÁLIDADAS APÓS COMPENSAÇÃO DE PAGAMENTO. ATENÇÃO: FIM DAS INSCRIÇÕES EM 21 DIAS. FIM DO DESCONTO EM X DIAS.)</div>
            </h3>
        </div>
        <div class="col-sm-8">
            <div class="row">
                <div class="col-sm-6">
                    <h3 style="margin-top: 0px;font-weight: bold;">
                        <small class="badge badge-primary bold">Cavaleiro</small><br>
                        <span style="letter-spacing: -1px;font-weight: 600;">
                            <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $FaturaSimples[0]->fk_pes_id, 1, 'pes_nome_razao_social'); ?>
                        </span>
                        <small style="letter-spacing: -1px;display: block;margin-top: 5px;color: #888;">
                            <b>Categoria:</b>
                                <?php echo $this->model_crud->get_rowSpecific('tb_evento_categoria', 'evc_id', $FaturaSimples[0]->fk_evc_id, 1, 'evc_sigla'); ?> - 
                                <?php echo $this->model_crud->get_rowSpecific('tb_evento_categoria', 'evc_id', $FaturaSimples[0]->fk_evc_id, 1, 'evc_categoria'); ?> 
                        </small>
                    </h3>
                </div>
                <div class="col-sm-6">
                    <h3 style="margin-top: 0px;">
                        <small class="badge badge-primary bold">Animal</small><br>
                        <span style="font-weight: 600;letter-spacing: -1px;">
                            <?php echo $this->model_crud->get_rowSpecific('tb_animal', 'ani_id', $FaturaSimples[0]->fk_ani_id, 1, 'ani_nome_completo'); ?>
                        </span>
                        <small style="letter-spacing: -1px;display: block;margin-top: 5px;color: #888;"><b>Categoria:</b> {Sigla} - {Categoria}</small>
                    </h3>
                </div>
            </div>

        </div>
    </div>


    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:30px;border: 1px dotted #ddd;">

