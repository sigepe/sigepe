<?php
    $this->load->module('evento/FrontOffice/Baia');

    $DatasetBaias 			=	$this->baia->GetBaias($FaturaSimples[0]->frf_id);
	$FlagBaia 				=	$this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $EventoId, 1, 'flag_baia');
	$FlagQuartoDeSela 		=	$this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $EventoId, 1, 'flag_quarto');

	( is_null($FlagBaia) || is_null($FlagQuartoDeSela)) ? $ColunaBootstrap = '12' : $ColunaBootstrap = '6';
?>


<div class="row">

	<?php if( !is_null($FlagBaia) || !empty($DatasetBaias) ): ?>

	<div class="col-sm-<?php echo $ColunaBootstrap; ?>">

	    <h3 style="font-weight: 800; margin-bottom: 23px; font-size: 27px; letter-spacing: -1px;">
	        BAIA
	        <small>Restam 27 Baias</small>


	        <?php if($FlagBaia): ?>
			<a href="#" class="btn btn-circle btn-warning btn-sm" style="border-radius: 28px !important;font-weight: 800 !important;letter-spacing:0px;float: right;padding: 5px 40px;">
				<i class="fa fa-plus-circle" aria-hidden="true" style="margin-right: 10px;"></i>
				Comprar Baia
			</a>
			<?php endif; ?>

	     </h3>

	    <div class="portlet light bordered">
	        <div class="portlet-body">

	        	<?php if(empty($DatasetBaias)): ?>
	        	<div class="row">
	        		<div class="col-sm-12">
						<div class="alert alert-warning" style="margin-bottom: 0px;">
						    <strong>Atenção!</strong> Não existe nenhuma baia para essa inscrição. <br>
						    <a href="#" title="">Clique aqui para comprar baia.</a>
						</div>
					</div>
				</div>
				<?php endif; ?>


	        	<?php if(!empty($DatasetBaias)): ?>
	        	<div class="row">
	        		<div class="col-sm-12">

						<table class="table table-striped table-hover" style="margin-bottom: 0px;">
						    <thead>
						        <tr>
						            <th class="text-center"> Identificação <i class="fa fa fa-info-circle font-blue" aria-hidden="true"></i></th>
						            <th> Animal </th>
						            <th> Preço </th>
						            <th class="text-center"> Ação </th>
						        </tr>
						    </thead>
						    <tbody>
						    	<?php foreach ($DatasetBaias as $key => $value): ?>
						        <tr>
						            <td class="text-center">
                                        <span
                                            class="popovers"
                                            data-container="body"
                                            onclick=" "
                                            data-html="true" 
                                            data-trigger="hover"
                                            data-placement="right"
                                            data-content="

                                                <small> 

                                                    <b>Status:</b><br>
                                                    Baia não está reservada. <br>Aguardando pagamento. 

                                                    <hr style='margin:8px 0;'>

                                                    <b>Baia adicionada por:</b><br>
                                                    <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_aut_id, 1, 'pes_nome_razao_social'); ?>
                                                   
                                                    <hr style='margin:8px 0;'>
      
                                                    <b>Registro criado em:</b><br>
                                                    <?php echo $this->my_data->ConverterData($value->criado, 'ISO', 'PT-BR'); ?>

                                                </small>

                                            "
                                            data-original-title="<b>Controle</b>">
    						            	<small class="font-grey-gallery"><?php echo $value->frb_controle; ?></small>
                                        </span>
						            </td>
						            <td class="font-grey-gallery"> <?php echo $this->model_crud->get_rowSpecific('tb_animal', 'ani_id', $value->fk_ani_id, 1, 'ani_nome_completo') ?> </td>



						            <td class="font-grey-gallery">


						            	<?php if( $PoliticaPreco == '1' && !is_null($value->frb_valor_promocional) ): ?>
						            	<small style="text-decoration: line-through;" class="font-grey-cascade">
								            R$ <?php echo  $this->my_moeda->InserirPontuacao( $value->frb_valor ); ?>
						            	</small>
						            	<br>
							            <?php endif; ?>


							            R$ <?php echo  $this->my_moeda->InserirPontuacao( ($PoliticaPreco == '1') ? $value->frb_valor_promocional : $value->frb_valor ); ?>


						            </td>




						            <td class="text-center">
										<a href="#" class="btn btn-circle btn-danger btn-sm" style="border-radius: 28px !important;font-weight: 400 !important;">
											<i class="fa fa-trash" aria-hidden="true"></i>
											Excluir
										</a>

						            </td>
						        </tr>
							    <?php endforeach; ?>

						    </tbody>
						</table>

					</div>
				</div>
				<?php endif; ?>


	        </div>
	    </div>
	    <?php endif; ?>

	</div>