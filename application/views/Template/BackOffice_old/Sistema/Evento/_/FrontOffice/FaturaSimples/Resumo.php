<style type="text/css">
	.titulo-secao-resumo{
		text-align: center;
	}
	#preco-total{
	    letter-spacing: -3px;
	    font-weight: 400;
	    font-size: 50px;
	    color: #000;		
	}
	#btn-boleto{
		background-color: #3de43d;
		border-color: #1fc11f;
		border-radius: 7px !important;
		margin-top: 30px;
		margin: 30px auto;
		font-weight: bold;
		color: #0d3c0d;
		padding: 23px;
		transition: .5s;
	}
	#btn-boleto:hover{
	    background-color: #187e18;
	    border-color: #0d5f0d;
	    color: white;
	}
</style>


<?php $this->load->module('evento/FrontOffice/FaturaSimples'); ?>


    <h3 class="titulo-secao titulo-secao-resumo">
        RESUMO FINANCEIRO 
    </h3>


    <!--
    	AGRUPAR AVISOS EM DOIS GRANDES BLOCOS. POLITICA DE PRECO 1 E 2 . 
    	SEPARAR ARQUIVOS DE RESUMO EM CAMADAS. ARQUIVO: AVISOS | ETC ... 
     -->



    <!--
    	Aviso fim do preco promocional.
    	So e exibido esse bloco se a politica do preco for 1 e NAO for o ultimo dia para encerrar a data limite.
    -->
    <?php if( isset($PoliticaPreco) && $PoliticaPreco == '1' && !isset($AvisoEncerramentoDataLimiteSemAcrescimo)): ?>
	<div class="alert alert-warning" style="text-align: center; ">
		<strong>Atenção</strong> <br>
		Pague a inscrição até <?php echo strftime('%A, %d de %B', strtotime($DataLimiteSemAcrescimo)); ?> e garanta o desconto.<br>
		Após esse prazo os valores serão atualizados.<br>
		<small>
			<b>
				<?php
					$DiasParaFimPrecoPromocional 	=	DateDifferences($DataLimiteSemAcrescimo, date("Y-m-d"), 'd');
					echo ($DiasParaFimPrecoPromocional == 1) ? 'Encerra amanhã o preço promocional. Aproveite!' : 'Faltam ' . $DiasParaFimPrecoPromocional . ' dias para encerrar o preço promocional';
				?>
			</b>
		</small>
	</div>
    <?php endif; ?>



    <!--
    	// Encerra Hoje preço promocional
    	Aviso fim do preco promocional. ( hoje )
    	So e exibido esse bloco se a politica do preco for 1 e se for o ultimo dia para encerrar a data limite.
    -->
    <?php if( isset($PoliticaPreco) && $PoliticaPreco == '1' && isset($AvisoEncerramentoDataLimiteSemAcrescimo)): ?>
	<div class="alert alert-danger" style="text-align: center; ">
		<strong>Atenção</strong> <br>
		Pague a inscrição <b>hoje <?php echo strftime('%A, %d de %B', strtotime($DataLimiteSemAcrescimo)); ?></b> e garanta o desconto.<br>
		Amanhã os valores já serão atualizados<br>
	</div>
    <?php endif; ?>



    <!--
    	AVISO FIM DAS INSCRICOES
    	- Aviso amarelo sinalizando o dia do fim das inscricoes. Alertar que apos esse prazo nao sera permitido inscricoes no sistema somente via federacao com preço diferente.
    	- Aviso vermelho sinalizando que hoje e o ultimo dia e amanha já nao será possivel fazer inscrição no sistema
    -->








    <div class="portlet light bordered col-sm-12" style="margin-bottom: 10px;">
        <div class="portlet-body">
			<table class="table table-striped table-hover">
			    <thead>
			        <tr>
			            <th style="width: 50%;text-align: right;"> Item </th>
			            <th  style="width: 50%;text-align: left;"> Preço </th>
			        </tr>
			    </thead>
			    <tbody>

			    	<?php echo $this->faturasimples->ResumoLinhaSerie($FaturaSimplesControle); ?>
			    	<?php echo $this->faturasimples->ResumoLinhaProvaAvulsa($FaturaSimplesControle); ?>
			    	<?php echo $this->faturasimples->ResumoLinhaBaia($FaturaSimplesControle); ?>
			    	<?php echo $this->faturasimples->ResumoLinhaQuartoDeSela($FaturaSimplesControle); ?>
			    	<?php echo $this->faturasimples->ResumoValores($FaturaSimplesControle); ?>



			    </tbody>
			</table>
		</div>
	</div>

    <div class="col-sm-12 text-center">

		<h1 style="margin-top: 0px;font-weight: bold;">
		    <small class="badge badge-success bg-green-meadow bg-font-green-meadow bold">
		    	VALOR TOTAL A SER PAGO ( À VISTA )
		    </small><br>

		    <?php if($PoliticaPreco == '1'): ?>
		    <span id="preco-total">
		    	<span style="text-decoration: line-through; color: #666; font-size: 33px; margin-right: 20px; font-weight: 100;">R$ <b style="font-weight: 100;"><?php echo number_format($ResumoValorTotal, 2, ',' , '.'); ?></b> </span>
		    	R$ <b><?php echo number_format($ResumoValorTotalPromocional, 2, ',' , '.'); ?></b>
		    </span>
			<?php endif; ?>


		    <?php if($PoliticaPreco == '2'): ?>
		    <span id="preco-total">
		    	R$ <b><?php echo number_format($ResumoValorTotal, 2); ?></b>
		    </span>
			<?php endif; ?>


		    <br>
		    <a href="#" class="btn btn-block btn-lg btn-success" id="btn-boleto"><span class="glyphicon glyphicon-barcode"></span> GERAR BOLETO</a>
		</h1>

    </div>



    <hr style="margin-top: 10px;margin-bottom:0px;border: 1px dotted #ddd;float:left;width: 100%">
    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:40px;border: 1px dotted #ddd;float:left;width: 100%;">