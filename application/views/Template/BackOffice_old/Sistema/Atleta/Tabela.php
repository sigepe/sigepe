
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet light">
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                <thead>
                    <tr>
                        <th> Atleta </th>
                        <th> Federação </th>
                        <th> Entidade </th>
                        <th> Registro </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th> Atleta </th>
                        <th> Federação </th>
                        <th> Entidade </th>
                        <th> Registro </th>
                    </tr>
                </tfoot>
                <tbody>
                    <tr>
                        <td>
                            <b>GUSTAVO MENDES BOTEGA</b> <br>
                            <small>ID PESSOA: 55    |  ID ATLETA: 7</small>
                        </td>
                        <td> 
                            <b>FHBr</b><br>
                            Federação Hípica de Brasília
                        </td>
                        <td>
                            <b>SHBr</b><br>
                            Sociedade Hípica de Brasília
                        </td>
                        <td>
                            <b>Tipo:</b> Anual <br>
                            <b>Status Registro:</b> Inativo <br>
                            <b>Status Financeiro:</b> Aguardando <br>
                            <b>Tipo Pagamento:</b> Cartão de Crédito <br>
                            <b>Valor:</b> R$ 570,00 <br>
                            <b>ID Pagar.me:</b> 5465798 <br>
                            <b>SIGEPE Fatura:</b> 5465798

                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
