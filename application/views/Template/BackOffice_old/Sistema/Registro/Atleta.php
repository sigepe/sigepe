 


<style type="text/css">
.label{
    border-radius: 30px !important;
    width: 100%;
    display: block;
}    
</style>


    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet light">
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                <thead>
                    <tr>
                        <th> Atleta </th>
                        <th> Entidade </th>
                        <th> Modalidade </th>
                        <th> Tipo </th>
                        <th> Registro </th>
                        <th> Financeiro </th>
                        <th> Pagamento </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th> Atleta </th>
                        <th> Entidade </th>
                        <th> Modalidade </th>
                        <th> Tipo </th>
                        <th> Registro </th>
                        <th> Financeiro </th>
                        <th> Pagamento </th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php
                        foreach ($DatasetAtleta as $key => $value):

                            /*
                            $EntidadeId  =   $value->fk_pje_id;
                            $PessoaJuridicaId           =   $this->model_crud->get_rowSpecific('tb_pessoa_juridica', 'fk_pes_id', $EntidadeId, 1, 'pej_id');
                            $PessoaJuridicaEntidade     =   $this->model_crud->get_rowSpecific('tb_pessoa_juridica_entidade', 'fk_pej_id', $PessoaJuridicaId, 1, 'pje_acronimo');



                            $Sql                        = '
                                                                SELECT * FROM tb_registro as reg
                                                                WHERE reg.fk_pes_id = '.$value->pes_id.'

                                                                ORDER BY reg_id DESC
                                                                LIMIT 1
                                                                ;
                                                                    ';
                            $Query   = $this->db->query($Sql)->result();
                            */

                    ?>
                    <tr>
                        <td>
                            <b><?php echo $value->pes_nome_razao_social; ?></b> <br>
                            <small>ID PESSOA: <?php echo $value->pes_id; ?>    |  ID ATLETA: <?php echo $value->pfa_id; ?></small>
                        </td>
                        <td>

                            <?php //echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pje_id, 1, 'pes_nome_razao_social');  ?>

                        </td>
                        <td>
                            <?php echo $value->evm_modalidade; ?>
                        </td>
                        <td>
                            <?php echo $value->ret_tipo; ?>
                        </td>
                        <td>
                            
                            <?php if($value->sta_id == '300'): ?>
                            <span class="label label-default"> <?php echo $value->sta_status; ?>  </span>
                            <?php endif; ?>

                            <?php if($value->sta_id == '301'): ?>
                            <span class="label label-primary"> <?php echo $value->sta_status; ?>  </span>
                            <?php endif; ?>

                            <?php if($value->sta_id == '303'): ?>
                            <span class="label label-danger"> <?php echo $value->sta_status; ?>  </span>
                            <?php endif; ?>

                            <small>ID Registro: <?php echo $value->reg_id; ?></small>

                        </td>
                        <td>
                            <?php if($value->status_financeiro == 100): ?>
                            <span class="label label-default"> <?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $value->status_financeiro, 1, 'sta_status'); ?> </span>
                            <?php endif; ?>

                            <?php if($value->status_financeiro == 101): ?>
                            <span class="label label-primary"> <?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $value->status_financeiro, 1, 'sta_status'); ?> </span>
                            <?php endif; ?>

                            <?php if($value->status_financeiro == 102): ?>
                            <span class="label label-primary"> <?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $value->status_financeiro, 1, 'sta_status'); ?> </span>
                            <?php endif; ?>

                            <?php if($value->status_financeiro == 104): ?>
                            <span class="label label-danger"> <?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $value->status_financeiro, 1, 'sta_status'); ?> </span>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php echo $value->fip_pagamento; ?> <br>
                            <small>R$ <?php echo $value->fin_valor_liquido; ?></small>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
