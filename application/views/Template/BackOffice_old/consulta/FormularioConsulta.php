<style type="text/css">
    .portlet-title{
        padding-bottom: 0px;
    }
    .portlet.light .portlet-body{
        padding: 0;
        margin: 0;
    }
</style>



        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-red"></i>
                    <span class="caption-subject font-red bold uppercase"> CONSULTAR PESSOA - FÍSICA/JURÍDICA
                    </span>
                </div>
            </div>


            <div class="portlet-body form">



                <div class="form-group">
                    <label class="control-label col-md-3"> ID
                        <span class="required"> * </span>
                    </label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="nome-completo" id="nome-completo" />
                    </div>
                </div>

                <hr>

                

                    <div class="alert alert-warning">
                        <strong>Atenção! </strong>Seu cadastro ainda não foi concluído. Confirme as informações abaixo e clique no botão <b>CADASTRAR</b>.
                    </div>

                    <h3 class="block">Confirme seus dados</h3>

                    <div class="col-sm-6">

                        <h4 class="form-section">Pessoa</h4>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">ID Pessoa:</label>
                            <div class="col-md-4">
                                <p class="form-control-static">#198</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Flag Usuário:</label>
                            <div class="col-md-4">
                                <p class="form-control-static">-</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Autor do Registro:</label>
                            <div class="col-md-4">
                                <p class="form-control-static" data-display="cpf">Gustavo Mendes Botega <br> #364</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Status:</label>
                            <div class="col-md-4">
                                <p class="form-control-static" data-display="nome-completo">Ativo</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Natureza:</label>
                            <div class="col-md-4">
                                <p class="form-control-static" data-display="sexo">PF</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">CPF/CNPJ:</label>
                            <div class="col-md-4">
                                <p class="form-control-static" data-display="celular">094.511.270-03</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Nome Completo / Razão Social:</label>
                            <div class="col-md-4">
                                <p class="form-control-static" data-display="celular">Gustavo Mendes Botega</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Data de Nascimento / Fundação</label>
                            <div class="col-md-4">
                                <p class="form-control-static" data-display="email">02/06/2008</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Foto</label>
                            <div class="col-md-4">
                                <p class="form-control-static" data-display="email">thumb.jpg</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Criado</label>
                            <div class="col-md-4">
                                <p class="form-control-static" data-display="email">2017-08-20</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Modificado</label>
                            <div class="col-md-4">
                                <p class="form-control-static" data-display="email">2017-08-20</p>
                            </div>
                        </div>

                    </div>


                    <div class="col-sm-6">

                        <h4 class="form-section">Pessoa Física</h4>

                        <div class="form-group">
                            <label class="control-label col-md-6 bold">ID Pessoa Física:</label>
                            <div class="col-md-4">
                                <p class="form-control-static">-</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">ID Pessoa:</label>
                            <div class="col-md-4">
                                <p class="form-control-static">-</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Nacionalidade:</label>
                            <div class="col-md-4">
                                <p class="form-control-static">-</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Escolaridade:</label>
                            <div class="col-md-4">
                                <p class="form-control-static">-</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Tipo Sanguineo:</label>
                            <div class="col-md-4">
                                <p class="form-control-static">-</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Naturalidade:</label>
                            <div class="col-md-4">
                                <p class="form-control-static">-</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Estado Civil:</label>
                            <div class="col-md-4">
                                <p class="form-control-static">-</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Gênero:</label>
                            <div class="col-md-4">
                                <p class="form-control-static">-</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Primeiro Nome:</label>
                            <div class="col-md-4">
                                <p class="form-control-static">-</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Sobrenome:</label>
                            <div class="col-md-4">
                                <p class="form-control-static">-</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Data de Nascimento:</label>
                            <div class="col-md-4">
                                <p class="form-control-static">-</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Nome do Pai:</label>
                            <div class="col-md-4">
                                <p class="form-control-static">-</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Nome da Mãe:</label>
                            <div class="col-md-4">
                                <p class="form-control-static">-</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Apelido:</label>
                            <div class="col-md-4">
                                <p class="form-control-static">-</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Criado em:</label>
                            <div class="col-md-4">
                                <p class="form-control-static">2018-02-12 13:14:17</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">MOdificado em:</label>
                            <div class="col-md-4">
                                <p class="form-control-static">2018-02-12 13:16:17</p>
                            </div>
                        </div>

                    </div>

                

            </div>
            <!-- /portlet-body -->


        </div>
        <!-- /portlet -->

