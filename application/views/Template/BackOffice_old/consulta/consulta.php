<style type="text/css">
    .portlet-title{
        padding-bottom: 0px;
    }
    .portlet.light .portlet-body{
        padding: 0;
        margin: 0;
    }
</style>



    



        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-red"></i>
                    <span class="caption-subject font-red bold uppercase"> CONSULTAR PESSOA - FÍSICA/JURÍDICA
                    </span>
                </div>
            </div>


            <form action="{base_url}cadastro/Pessoa/ConsultarPessoa/" class="form-horizontal" method="post">

                <div class="form-body">

                    <div class="form-group last">
                        <label class="col-md-3 control-label">ID Pessoa</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <span class="input-group-addon input-circle-left">
                                    <i class="fa fa-user"></i>
                                </span>
                                <input type="text" class="form-control input-circle-right" name="id-pessoa" placeholder="ID da Pessoa">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-circle green">Consultar</button>
                    </div>

                </div>

            </form>


            <hr>

            <?php  if(isset($Pessoa)): ?>
            <?php $Pessoa = $Pessoa[0]; ?>

            <div class="portlet-body form" style="display: <?php echo ($IdPessoa) ? 'block' : 'none'; ?>">

                <div class="row">

                    <div class="col-sm-6">

                        <h4 class="form-section bold">
                            <i class="fa fa-globe" aria-hidden="true"></i>
                            Pessoa
                        </h4>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">ID Pessoa:</label>
                            <div class="col-md-4">
                                <p class="form-control-static">#<?php echo $Pessoa->pes_id ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Flag Usuário:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (is_null($Pessoa->flag_usuario)) ? 'Não' : 'Sim' ; ?></p>
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Autor do Registro:</label>
                            <div class="col-md-5">
                                <p class="form-control-static"><?php echo (!is_null($Pessoa->fk_aut_id)) ? $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $Pessoa->fk_aut_id, 1, 'pes_nome_razao_social') . '<br>' . '<small>#'. $Pessoa->fk_aut_id .'</small>' : '-' ; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Status:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $Pessoa->fk_sta_id, 1, 'sta_status');  ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Natureza:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo ($Pessoa->pes_natureza == 'PF') ? 'Pessoa Física' : 'Pessoa Jurídica' ; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">CPF/CNPJ:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $this->my_pessoa->InserirPontuacaoCpfCnpj($Pessoa->pes_cpf_cnpj); ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Nome Completo / Razão Social:</label>
                            <div class="col-md-5">
                                <p class="form-control-static"><?php echo strtoupper($Pessoa->pes_nome_razao_social); ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Data de Nascimento / Fundação</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $this->my_data->ConverterData($Pessoa->pes_data_nascimento_fundacao, 'ISO', 'PT-BR'); ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Foto</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $Pessoa->pes_foto; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Criado</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $this->my_data->ConverterData($Pessoa->criado, 'ISO', 'PT-BR'); ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Modificado</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $this->my_data->ConverterData($Pessoa->modificado, 'ISO', 'PT-BR'); ?></p>
                            </div>
                        </div>
                    </div>


                    <?php if(isset($PessoaFisica)): ?>
                    <?php $PessoaFisica     =   $PessoaFisica[0]; ?>
                    <div class="col-sm-6">

                        <h4 class="form-section bold">
                            <i class="fa fa-user" aria-hidden="true"></i>
                            Pessoa Física
                        </h4>


                        <div class="form-group">
                            <label class="control-label col-md-6 bold">ID Pessoa Física:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $PessoaFisica->pef_id; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">ID Pessoa:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $PessoaFisica->fk_pes_id; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Nacionalidade:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (!is_null($PessoaFisica->fk_esc_id)) ? $this->model_crud->get_rowSpecific('tb_pessoa_fisica_escolaridade', 'pfe_id', $PessoaFisica->fk_esc_id, 1, 'pfe_escolaridade') . '<br>' . '<small>#'. $PessoaFisica->fk_esc_id .'</small>' : '-' ; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Escolaridade:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (!is_null($PessoaFisica->fk_esc_id)) ? $this->model_crud->get_rowSpecific('tb_pessoa_fisica_escolaridade', 'pfe_id', $PessoaFisica->fk_esc_id, 1, 'pfe_escolaridade') . '<br>' . '<small>#'. $PessoaFisica->fk_esc_id .'</small>' : '-' ; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Tipo Sanguineo:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (!is_null($PessoaFisica->fk_tip_id)) ? $this->model_crud->get_rowSpecific('tb_pessoa_fisica_tiposanguineo', 'tip_id', $PessoaFisica->fk_tip_id, 1, 'pft_tipo_sanguineo') . '<br>' . '<small>#'. $PessoaFisica->fk_tip_id .'</small>' : '-' ; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Naturalidade:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (!is_null($PessoaFisica->fk_tip_id)) ? $this->model_crud->get_rowSpecific('tb_pessoa_fisica_tiposanguineo', 'tip_id', $PessoaFisica->fk_tip_id, 1, 'pft_tipo_sanguineo') . '<br>' . '<small>#'. $PessoaFisica->fk_tip_id .'</small>' : '-' ; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Estado Civil:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (!is_null($PessoaFisica->fk_tip_id)) ? $this->model_crud->get_rowSpecific('tb_pessoa_fisica_estadocivil', 'pfe_id', $PessoaFisica->fk_est_id, 1, 'pfe_estado_civil') . '<br>' . '<small>#'. $PessoaFisica->fk_est_id .'</small>' : '-' ; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Gênero:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (!is_null($PessoaFisica->fk_gen_id)) ? $this->model_crud->get_rowSpecific('tb_pessoa_fisica_genero', 'gen_id', $PessoaFisica->fk_gen_id, 1, 'pfg_genero') . '<br>' . '<small>#'. $PessoaFisica->fk_gen_id .'</small>' : '-' ; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Primeiro Nome:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $PessoaFisica->pef_primeiro_nome; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Sobrenome:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $PessoaFisica->pef_sobrenome; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Data de Nascimento:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $this->my_data->ConverterData($PessoaFisica->pef_data_nascimento, 'ISO', 'PT-BR'); ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Nome do Pai:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $PessoaFisica->pef_nome_pai; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Nome da Mãe:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $PessoaFisica->pef_nome_mae; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Apelido:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $PessoaFisica->pef_apelido; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Criado em:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $this->my_data->ConverterData($PessoaFisica->criado, 'ISO', 'PT-BR'); ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Modificado em:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $this->my_data->ConverterData($PessoaFisica->modificado, 'ISO', 'PT-BR'); ?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>

                </div>
                

            </div>
            <!-- /portlet-body -->


            <hr>



            <div class="tabbable-line"  style="display: <?php echo ($IdPessoa) ? 'block' : 'none'; ?>" >


                <ul class="nav nav-tabs ">
                    <li class="active">
                        <a href="#tab_telefone" data-toggle="tab">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            Telefone
                        </a>
                    </li>
                    <li>
                        <a href="#tab_email" data-toggle="tab">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                            E-mail
                        </a>
                    </li>
                    <li>
                        <a href="#tab_endereco" data-toggle="tab">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            Endereço
                        </a>
                    </li>
                    <li>
                        <a href="#tab_vinculos" data-toggle="tab">
                            <i class="fa fa-link" aria-hidden="true"></i>
                            Vínculos
                        </a>
                    </li>
                    <li>
                        <a href="#tab_perfis" data-toggle="tab">
                            <i class="fa fa-users" aria-hidden="true"></i>
                            Perfis
                        </a>
                    </li>
                    <li>
                        <a href="#tab_animais" data-toggle="tab">
                            <i class="fa fa-paw" aria-hidden="true"></i>
                            Animais
                        </a>
                    </li>
                    <li>
                        <a href="#tab_atleta" data-toggle="tab">
                            <i class="fa fa-universal-access" aria-hidden="true"></i>
                            Atleta
                        </a>
                    </li>
                    <li>
                        <a href="#tab_empresas" data-toggle="tab">
                            <i class="fa fa-briefcase" aria-hidden="true"></i>
                            Empresas
                        </a>
                    </li>
                </ul>



                <div class="tab-content" style="padding-top: 10px;">



                    <!-- 
                    TELEFONE
                    =========================================== -->
                    <div class="tab-pane active" id="tab_telefone">
                        <div class="portlet light" style="padding-top: 0;">
                            <div class="portlet-body">

                                <?php if(empty($Telefones)): ?>
                                <div class="alert alert-warning row">
                                    <strong>Atenção!</strong>
                                    Nenhum telefone cadastrado para essa pessoa.
                                </div>
                                <?php endif; ?>

                                <?php if(!empty($Telefones)): ?>
                                <div class="row">
                                    <div class="table-scrollable">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th> ID Telefone </th>
                                                    <th> ID Autor </th>
                                                    <th> ID Pessoa </th>
                                                    <th> Tipo Contato </th>
                                                    <th> Status </th>
                                                    <th> DDD </th>
                                                    <th> Numero </th>
                                                    <th> Telefone </th>
                                                    <th> Principal </th>
                                                    <th> Criado </th>
                                                    <th> Modificado </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($Telefones as $key => $value): ?>
                                                <tr>
                                                    <td> #<?php echo $value->tel_id; ?> </td>
                                                    <td> #<?php echo $value->fk_aut_id; ?> </td>
                                                    <td> #<?php echo $value->fk_peo_id; ?> </td>
                                                    <td> <?php echo $this->model_crud->get_rowSpecific('tb_contato', 'con_id', $value->fk_con_id, 1, 'con_contato'); ?> <small>( ID: <?php echo $value->fk_con_id; ?> )</small> </td>
                                                    <td>
                                                        <?php
                                                            if($value->fk_sta_id=='1')
                                                                echo '<span class="label label-sm label-info"> Ativo </span>';

                                                            if($value->fk_sta_id=='2')
                                                                echo '<span class="label label-sm label-default"> Inativo </span>';
                                                        ?>
                                                        
                                                    </td>
                                                    <td> <?php echo $value->tel_ddd; ?> </td>
                                                    <td> <?php echo $value->tel_numero; ?> </td>
                                                    <td> <?php echo $value->tel_telefone; ?> </td>
                                                    <td>
                                                        <?php if(!is_null($value->tel_principal)): ?>
                                                        <span class="label label-sm label-info"> PRINCIPAL </span>
                                                        <?php endif; ?>
                                                        <?php echo (is_null($value->tel_principal)) ? '-' : ''; ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                            $DataIso    =    $this->my_data->GetDateTimeIso($value->criado, 'Date');
                                                            echo $this->my_data->ConverterData($DataIso, 'ISO', 'PT-BR');
                                                        ?>
                                                        <br>
                                                        <small>
                                                            <?php echo $this->my_data->GetDateTimeIso($value->criado, 'Time'); ?>
                                                        </small>
                                                    </td>
                                                    <td> <?php echo $this->my_data->GetDateTimeIso($value->modificado, 'Date'); ?> <br> <small><?php echo $this->my_data->GetDateTimeIso($value->modificado, 'Time'); ?></small> </td>
                                                </tr>
                                                <?php endforeach; ?>
                                              
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>



                    <!-- 
                    EMAIL
                    =========================================== -->
                    <div class="tab-pane" id="tab_email">
                     
                        <div class="portlet light" style="padding-top: 0;">
                            <div class="portlet-body">


                                <?php if(empty($Emails)): ?>
                                <div class="alert alert-warning row">
                                    <strong>Atenção!</strong>
                                    Nenhum email cadastrado para essa pessoa.
                                </div>
                                <?php endif; ?>


                                <?php if(!empty($Emails)): ?>
                                <div class="row">
                                    <div class="table-scrollable">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th> ID E-mail </th>
                                                    <th> ID Autor </th>
                                                    <th> ID Pessoa </th>
                                                    <th> Tipo Contato </th>
                                                    <th> Status </th>
                                                    <th> E-mail </th>
                                                    <th> Principal </th>
                                                    <th> Criado </th>
                                                    <th> Modificado </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($Emails as $key => $value): ?>
                                            <tr>
                                                <td> #<?php echo $value->ema_id; ?> </td>
                                                <td> #<?php echo $value->fk_aut_id; ?> </td>
                                                <td> #<?php echo $value->fk_peo_id; ?> </td>
                                                <td> <?php echo $this->model_crud->get_rowSpecific('tb_contato', 'con_id', $value->fk_con_id, 1, 'con_contato'); ?> <small>( ID: <?php echo $value->fk_con_id; ?> )</small> </td>
                                                <td>
                                                    <?php
                                                        if($value->fk_sta_id=='1')
                                                            echo '<span class="label label-sm label-info"> Ativo </span>';

                                                        if($value->fk_sta_id=='2')
                                                            echo '<span class="label label-sm label-default"> Inativo </span>';
                                                    ?>
                                                </td>
                                                <td> <?php echo $value->ema_email; ?> </td>
                                                <td>
                                                    <?php if(!is_null($value->ema_principal)): ?>
                                                    <span class="label label-sm label-info"> PRINCIPAL </span>
                                                    <?php endif; ?>
                                                    <?php echo (is_null($value->ema_principal)) ? '-' : ''; ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        $DataIso    =    $this->my_data->GetDateTimeIso($value->criado, 'Date');
                                                        echo $this->my_data->ConverterData($DataIso, 'ISO', 'PT-BR');
                                                    ?>
                                                    <br>
                                                    <small>
                                                        <?php echo $this->my_data->GetDateTimeIso($value->criado, 'Time'); ?>
                                                    </small>
                                                </td>
                                                <td> <?php echo $this->my_data->GetDateTimeIso($value->modificado, 'Date'); ?> <br> <small><?php echo $this->my_data->GetDateTimeIso($value->modificado, 'Time'); ?></small> </td>
                                            </tr>
                                            <?php endforeach; ?>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>


                    <!-- 
                    ENDERECO
                    =========================================== -->
                    <div class="tab-pane" id="tab_endereco">
                        <div class="alert alert-warning"> <strong>Atenção!</strong> Nenhum endereço cadastrada para essa pessoa. </div>
                    </div>


                    <!-- 
                    VINCULOS
                    =========================================== -->
                    <div class="tab-pane" id="tab_vinculos">
                        <div class="alert alert-warning"> <strong>Atenção!</strong> Nenhum vínculo encontrado. </div>


                        <div class="portlet light" style="padding-top: 0;">
                            <div class="portlet-body">
                                <div class="table-scrollable">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th> ID E-mail </th>
                                                <th> ID Autor </th>
                                                <th> ID Pessoa </th>
                                                <th> Tipo Contato </th>
                                                <th> Status </th>
                                                <th> E-mail </th>
                                                <th> Principal </th>
                                                <th> Criado </th>
                                                <th> Modificado </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td> #94 </td>
                                                <td> #346 </td>
                                                <td> #138 </td>
                                                <td> Comercial <small>( ID: 04 )</small> </td>
                                                <td> <span class="label label-sm label-info"> Ativo </span> </td>
                                                <td> gustavobotega@gmail.com </td>
                                                <td> <span class="label label-sm label-info"> PRINCIPAL </span> </td>
                                                <td> 02/08/2014 <br> <small>09h50</small> </td>
                                                <td> 02/08/2014 <br> <small>09h50</small> </td>
                                            </tr>
                                            <tr>
                                                <td> #135 </td>
                                                <td> #346 </td>
                                                <td> #138 </td>
                                                <td> Comercial <small>( ID: 04 )</small> </td>
                                                <td> <span class="label label-sm label-info"> Ativo </span> </td>
                                                <td> gustavobotega@gmail.com </td>
                                                <td> - </td>
                                                <td> 02/08/2014 <br> <small>09h50</small> </td>
                                                <td> 02/08/2014 <br> <small>09h50</small> </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>


                    <!-- 
                    PERFIS
                    =========================================== -->
                    <div class="tab-pane" id="tab_perfis">



                        <?php if(empty($Perfis)): ?>
                        <div class="alert alert-warning">
                            <strong>Atenção!</strong>
                            Nenhum perfil vinculado para essa pessoa.
                        </div>
                        <?php endif; ?>


                        <?php if(!empty($Perfis)): ?>
                        <div class="portlet light" style="padding-top: 0;">
                            <div class="portlet-body">
                                <div class="table-scrollable">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th> ID Vínculo </th>
                                                <th> ID Pessoa </th>
                                                <th> Nome </th>
                                                <th> Perfil </th>
                                                <th> Status </th>
                                                <th> Criado </th>
                                                <th> Modificado </th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php foreach ($Perfis as $key => $value): ?>
                                            <tr>
                                                <td> #<?php echo $value->vin_id; ?> </td>
                                                <td> #<?php echo $value->fk_pes1_id; ?> </td>
                                                <td>
                                                    <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes1_id, 1, 'pes_nome_razao_social'); ?>
                                                    <br>                                                        
                                                    <small>#<?php echo $value->fk_pes1_id; ?></small>
                                                </td>
                                                <td>
                                                    <?php echo $this->model_crud->get_rowSpecific('tb_vinculo_perfil', 'vip_id', $value->fk_per_id, 1, 'vip_papel'); ?>
                                                    <small>( ID: <?php echo $value->fk_per_id; ?> )</small>
                                                </td>
                                                <td>
                                                    <?php
                                                        if($value->fk_sta_id=='1')
                                                            echo '<span class="label label-sm label-info"> Ativo </span>';

                                                        if($value->fk_sta_id=='2')
                                                            echo '<span class="label label-sm label-default"> Inativo </span>';
                                                    ?>
                                                </td>

                                                <td>
                                                    <?php
                                                        $DataIso    =    $this->my_data->GetDateTimeIso($value->criado, 'Date');
                                                        echo $this->my_data->ConverterData($DataIso, 'ISO', 'PT-BR');
                                                    ?>
                                                    <br>
                                                    <small>
                                                        <?php echo $this->my_data->GetDateTimeIso($value->criado, 'Time'); ?>
                                                    </small>
                                                </td>
                                                <td>
                                                    <?php
                                                        $DataIso    =    $this->my_data->GetDateTimeIso($value->modificado, 'Date');
                                                        echo $this->my_data->ConverterData($DataIso, 'ISO', 'PT-BR');
                                                    ?>
                                                    <br>
                                                    <small>
                                                        <?php echo $this->my_data->GetDateTimeIso($value->modificado, 'Time'); ?>
                                                    </small>
                                                </td>


                                            </tr>
                                            <?php endforeach; ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>


                    </div>




                    <!-- 
                    ANIMAIS
                    =========================================== -->
                    <div class="tab-pane" id="tab_animais">
                        <div class="alert alert-warning"> <strong>Atenção!</strong> Nenhum animal cadastrada para essa pessoa. </div>




                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <ul class="nav nav-tabs tabs-left">
                                    <li class="active">
                                        <a href="#tab_animais_vinculos" data-toggle="tab"> Vínculos </a>
                                    </li>
                                    <li>
                                        <a href="#tab_animais_proprietarios" data-toggle="tab"> Proprietário(s) </a>
                                    </li>
                                    <li>
                                        <a href="#tab_animais_responsavel" data-toggle="tab"> Responsável Financeiro </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_animais_vinculos">

                                    <h3>Vínculos</h3>

                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs ">
                                            <li class="active">
                                                <a href="#tab_animais_vinculos_confederacao" data-toggle="tab" aria-expanded="true"> Confederação </a>
                                            </li>
                                            <li class="">
                                                <a href="#tab_animais_vinculos_federacao" data-toggle="tab" aria-expanded="false"> Federação </a>
                                            </li>
                                            <li>
                                                <a href="#tab_animais_vinculos_entidade_equestre" data-toggle="tab"> Entidade Equestre </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_animais_vinculos_confederacao">
                                                <p> I'm in Section 1. </p>
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut
                                                    aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate
                                                    velit esse molestie consequat. </p>
                                            </div>
                                            <div class="tab-pane" id="tab_animais_vinculos_federacao">
                                                <p> Howdy, I'm in Section 2. </p>
                                                <p> Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate
                                                    velit esse molestie consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation. </p>
                                            </div>
                                            <div class="tab-pane" id="tab_animais_vinculos_entidade_equestre">
                                                <p> Howdy, I'm in Section 3. </p>
                                                <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                                                    Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat </p>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab_animais_responsavel">
                                        <p> Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan
                                            four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda
                                            labore aesthetic magna delectus mollit. Keytar helvetica VHS salvia yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit, sustainable
                                            jean shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester stumptown, tumblr butcher vero sint qui sapiente accusamus tattooed echo park. </p>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <!-- //tab_animais -->




                    <!-- 
                    ATLETA
                    =========================================== -->
                    <?php 
                        $PerfilAtleta   =   FALSE;
                        foreach ($Perfis as $key => $value) {
                            if( $value->fk_per_id == '5' && $value->fk_sta_id == '1')
                                $PerfilAtleta = TRUE;
                        }
                    ?>
                    <div class="tab-pane" id="tab_atleta">

                        <?php if(!$PerfilAtleta): ?>
                        <div class="alert alert-warning"> <strong>Atenção!</strong> Não há perfil atleta para essa pessoa. </div>
                        <?php endif; ?>


                        <?php if($PerfilAtleta): ?>
                        <div class="row">

                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <ul class="nav nav-tabs tabs-left">
                                   <li class="active">
                                        <a href="#tab_atleta_informacoes_gerais" data-toggle="tab"> Informações Gerais </a>
                                   </li>
                                   <li>
                                        <a href="#tab_atleta_vinculos" data-toggle="tab"> Vínculos </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <div class="tab-content">


                                    <!-- Informacoes Gerais -->
                                    <div class="tab-pane active" id="tab_atleta_informacoes_gerais">

                                            
                                        <!-- 
                                        Informações Gerais
                                        **************************************** -->
                                        <h3 class="form-section">
                                            <div class="row">
                                                <div class="col-sm-8"><div class="title"> <span aria-hidden="true" class="icon-pencil"></span> Informações Gerais</div></div>
                                                                </div>
                                        </h3>


                                        <!-- Código ID / CPF -->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Identificação:</label>
                                                    <div class="col-md-7 noPaddingLeft">
                                                        <p class="form-control-static">
                                                             #AT-2                            </p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <!-- NOME COMPLETO / DATA DE NASCIMENTO -->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Atleta desde:</label>
                                                    <div class="col-md-7 noPaddingLeft">
                                                        <p class="form-control-static">
                                                             -                            </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-5">N. Competição:</label>
                                                    <div class="col-md-7 noPaddingLeft">
                                                        <p class="form-control-static">
                                                                                         </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->


                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Registro CBH:</label>
                                                    <div class="col-md-7 noPaddingLeft">
                                                        <p class="form-control-static">
                                                                                         </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-5">Registro FEI:</label>
                                                    <div class="col-md-7 noPaddingLeft">
                                                        <p class="form-control-static">
                                                                                         </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->


                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Tipo de Registro:</label>
                                                    <div class="col-md-7 noPaddingLeft">
                                                        <p class="form-control-static">
                                                             <!-- {typeEntry} -->
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-5">Entidade Filiada:</label>
                                                    <div class="col-md-7 noPaddingLeft">
                                                        <p class="form-control-static">
                                                             <!-- {nameEntity} ( {acronymEntity}  -->
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->



                                        <!-- EMAIL 3 -->
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Modalidade:  </label>
                                                    <div class="col-md-10 noPaddingRight noPaddingLeft" style="margin-top:10px;">
                                                            <span class="label label-success"> <!-- {modality} --> </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                              

                                    <!-- Vinculos -->
                                    <div class="tab-pane fade" id="tab_atleta_vinculos">

                                        <h3>Vínculos Atuais</h3>

                                        <div class="portlet light bordered">
                                            
                                            <div class="portlet-body">

                                                <!-- Confederacao -->
                                                <div class="row">
                                                    <div class="col-sm-3 bold text-right">Confederação:</div>
                                                    <div class="col-sm-9"> <!-- <small>( vinculado desde 25/07/2014 até hoje )</small> --></div>
                                                </div>

                                                <!-- Federacao -->
                                                <div class="row">
                                                    <div class="col-sm-3 bold text-right">Federação:</div>
                                                    <div class="col-sm-9"> <!-- <small>( vinculado desde 25/07/2014 até hoje )</small> --></div>
                                                </div>

                                                <!-- Entidade Filiada -->
                                                <div class="row">
                                                    <div class="col-sm-3 bold text-right">Entidade Filiada:</div>
                                                    <div class="col-sm-9"> <!-- <small>( vinculado desde 25/07/2014 até hoje )</small> --></div>
                                                </div>

                                                <!-- Escola de Equitacao -->
                                                <div class="row">
                                                    <div class="col-sm-3 bold text-right">Escola de Equitação:</div>
                                                    <div class="col-sm-9"></div>
                                                </div>

                                            </div>
                                        </div>


                                        <h3>Histórico Vínculos</h3>

                                        <div class="tabbable-line">
                                            <ul class="nav nav-tabs ">
                                                <li class="active">
                                                    <a href="#tab_atleta_vinculos_confederao" data-toggle="tab" aria-expanded="true"> Confederação </a>
                                                </li>
                                                <li class="">
                                                    <a href="#tab_atleta_vinculos_federacao" data-toggle="tab" aria-expanded="false"> Federação </a>
                                                </li>
                                                <li>
                                                    <a href="#tab_atleta_vinculos_entidade" data-toggle="tab"> Entidade Equestre </a>
                                                </li>
                                            </ul>
                                            <div class="tab-content" style="padding-top: 0px;">    

                                                <!-- Confederacao -->
                                                <div class="tab-pane active" id="tab_atleta_vinculos_confederao">

                                                    <div class="table-scrollable">
                                                        <table class="table table-striped table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th> # </th>
                                                                    <th> Confederação </th>
                                                                    <th> Início Vínculo </th>
                                                                    <th> Fim Vínculo </th>
                                                                    <th> Status </th>
                                                                    <th> * </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td> 1 </td>
                                                                    <td> CBH - Confederação Brasileira de Hipismo </td>
                                                                    <td> 28/02/2016 </td>
                                                                    <td> - </td>
                                                                    <td>
                                                                        <span class="label label-sm label-success"> Approved </span>
                                                                    </td>
                                                                    <td> <a href="#" title=""><i class="fa fa-plus-circle" aria-hidden="true"></i></a> </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>

                                                </div>
                                                <!-- /Confederacao -->


                                                <!-- Federacao -->
                                                <div class="tab-pane" id="tab_atleta_vinculos_federacao">


                                                    <?php if(empty($Federacao)): ?>
                                                    <div class="alert alert-danger row">
                                                        <strong>Atenção!</strong>
                                                        Nenhuma federação vinculada a esse atleta.
                                                    </div>
                                                    <?php endif; ?>

                                                    <?php if(!empty($Federacao)): ?>
                                                    <div class="table-scrollable">
                                                        <table class="table table-striped table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th> # </th>
                                                                    <th> Federação </th>
                                                                    <th> Início Vínculo </th>
                                                                    <th> Fim Vínculo </th>
                                                                    <th> Status </th>
                                                                    <th> * </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                    foreach ($Federacao as $key => $value):

                                                                ?>
                                                                <tr>
                                                                    <td> <?php echo $value->vin_id; ?> </td>
                                                                    <td> <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes2_id, 1, 'pes_nome_razao_social'); ?> </td>
                                                                    <td> <?php echo $this->my_data->ConverterData($value->criado, 'ISO', 'PT-BR'); ?> </td>
                                                                    <td> - </td>
                                                                    <td>                                   
                                                                        <span class="label label-sm tooltips label-<?php echo GetBootstrapColor($value->fk_sta_id); ?>" data-original-title="<?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $value->fk_sta_id, 1, 'sta_status'); ?>" >
                                                                            <?php
                                                                                echo GetStatusVinculo($value->fk_sta_id);
                                                                                ?>
                                                                        </span>
                                                                    </td>
                                                                    <td> <a href="#" title=""><i class="fa fa-plus-circle" aria-hidden="true"></i></a> </td>
                                                                </tr>
                                                                <?php endforeach; ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <?php endif; ?>

                                                </div>
                                                <!-- /Federacao -->

                                                <!-- Entidade Equestre -->
                                                <div class="tab-pane" id="tab_atleta_vinculos_entidade">

                                                    <div class="table-scrollable">
                                                        <table class="table table-striped table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th> # </th>
                                                                    <th> Entidade </th>
                                                                    <th> Início Vínculo </th>
                                                                    <th> Fim Vínculo </th>
                                                                    <th> Status </th>
                                                                    <th> * </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td> 1 </td>
                                                                    <td> BCC - Brasília Country Club </td>
                                                                    <td> 28/03/2017 </td>
                                                                    <td> - </td>
                                                                    <td>
                                                                        <span class="label label-sm label-primary"> Aguardando validação </span>
                                                                    </td>
                                                                    <td> <a href="#" title=""><i class="fa fa-plus-circle" aria-hidden="true"></i></a> </td>
                                                                </tr>                                                            
                                                                <tr>
                                                                    <td> 1 </td>
                                                                    <td> CITTAC - Centro Integrado de Treinamento dos Trotadores de Águas Claras </td>
                                                                    <td> 28/02/2016 </td>
                                                                    <td> 28/03/2017 </td>
                                                                    <td>
                                                                        <span class="label label-sm label-danger"> Cancelado </span>
                                                                    </td>
                                                                    <td>
                                                                        <a href="#" title="" class="vinculo-detalhe-link" data-id="7" data-nome-empresa="CITTAC - Centro Integrado de Treinamento dos Trotadores de Águas Claras" data-id-empresa="67">
                                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>

                                                </div>
                                                <!-- /Entidade Equestre -->



                                            </div>
                                        </div>

                                    </div>
                                    <!-- // vinculos -->

                                </div>
                            </div>


                        </div>
                        <?php endif; ?>




                    </div>


                    <!-- 
                    EMPRESAS
                    =========================================== -->
                    <div class="tab-pane" id="tab_empresas">

                        <?php if(empty($Empresas)): ?>
                        <div class="alert alert-warning"> <strong>Atenção!</strong> Nenhum vínculo encontrado. </div>
                        <?php endif; ?>

                        <?php if(!empty($Empresas)): ?>
                        <div class="portlet light" style="padding-top: 0;">
                            <div class="portlet-body">
                                <div class="table-scrollable">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th> ID Vínculo </th>
                                                <th> ID Autor </th>
                                                <th> Empresa </th>
                                                <th> Pessoa </th>
                                                <th> Tipo Vínculo </th>
                                                <th> Status </th>
                                                <th> Criado </th>
                                                <th> Modificado </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($Empresas as $value): ?>
                                            <tr>
                                                <td> #<?php echo $value->vin_id; ?> </td>
                                                <td> #<?php echo $value->fk_aut_id; ?> </td>
                                                <td>
                                                    <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes2_id, 1, 'pes_nome_razao_social'); ?> <br> 
                                                    <small><b>ID:</b> #<?php echo $value->fk_pes2_id; ?></small><br>
                                                    <small>
                                                        <b>CNPJ:</b>
                                                        <?php
                                                            $Cnpj   =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes2_id, 1, 'pes_cpf_cnpj');
                                                            echo $this->my_pessoa->InserirPontuacaoCpfCnpj( $Cnpj );
                                                        ?>
                                                    </small>
                                                </td>
                                                <td>
                                                    Gustavo Mendes Botega <br> 
                                                    <small><b>ID:</b> #<?php echo $value->fk_pes1_id; ?></small><br>
                                                    <small>
                                                        <b>CPF:</b>
                                                        <?php
                                                            $Cpf   =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes1_id, 1, 'pes_cpf_cnpj');
                                                            echo $this->my_pessoa->InserirPontuacaoCpfCnpj( $Cpf );
                                                        ?>
                                                    </small>
                                                </td>
                                                <td> <?php echo $this->model_crud->get_rowSpecific('tb_vinculo_tipo', 'vit_id', $value->fk_tip_id, 1, 'vit_tipo_vinculo'); ?> </td>

                                                <td>
                                                    <?php
                                                        if($value->fk_sta_id=='1')
                                                            echo '<span class="label label-sm label-info"> Ativo </span>';

                                                        if($value->fk_sta_id=='2')
                                                            echo '<span class="label label-sm label-default"> Inativo </span>';
                                                    ?>
                                                    
                                                </td>


                                                <td> <?php echo $this->my_data->GetDateTimeIso($value->criado, 'Date'); ?> <br> <small><?php echo $this->my_data->GetDateTimeIso($value->criado, 'Time'); ?></small> </td>
                                                <td> <?php echo $this->my_data->GetDateTimeIso($value->modificado, 'Date'); ?> <br> <small><?php echo $this->my_data->GetDateTimeIso($value->modificado, 'Time'); ?></small> </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>

                    </div>




                </div>
            </div>





            <?php endif; ?>




        </div>
        <!-- /portlet -->















    <!-- 
    MODAL ( DETALHES DO VINCULO )
    ==================================== -->
    <div class="modal fade bs-modal-lg" id="vinculo-detalhe" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title bold">CITTAC - Centro Integrado de Treinamento dos Trotadores de Águas Claras    </h4>
                    <div>
                        <small>
                            <b>ID Vínculo:</b> #<span id="vinculo-detalhe-id"></span> | 
                            <b>ID Empresa:</b> #<span id="vinculo-detalhe-id-empresa"></span>
                        </small>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="modal-body">

                        <div id="vinculo-detalhe-carregando" style="text-align: center; color: #008076; ">
                            <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
                            <h3 style="display: inline; ">Aguarde, carregando...</h3>
                        </div>

                        <div class="table-scrollable" id="vinculo-detalhe-tabela" style="display: none;">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th> # </th>
                                        <th> Autor </th>
                                        <th> Atualizado em </th>
                                        <th> Status </th>
                                        <th> Detalhes </th>
                                    </tr>
                                </thead>
                                <tbody>

                                

                                </tbody>
                            </table>
                        </div>
                        <!-- /table-scrollable -->

                    </div>                    


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn green" data-dismiss="modal">Fechar</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
