<style type="text/css">
    .portlet-title{
        padding-bottom: 0px;
    }
    .portlet.light .portlet-body{
        padding: 0;
        margin: 0;
    }
    #atleta-tabs-left{
        min-height: 530px;
    }
    #atleta-tabs-left li a{
        text-align: right;                                
    }
    #atleta-tabs-left li.active a{
        background: #36c6d3;
        color: white;
        border: none;
    }
    .label-vinculo-atual{
        display: block;
        padding: 10px !important;        
    }

    #dashboard-animal .form-control-static {
        padding-top: 0px;
        padding-bottom: 0px;
        margin-bottom: 0;
        min-height: 22px;
    }
    #dashboard-animal .form-group{
        border-bottom: 1px solid #ccc;
        float: left;
        width: 100%;
        min-height: 28px;
        margin-bottom: 0;
    }

</style>


        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-red"></i>
                    <span class="caption-subject font-red bold uppercase"> CONSULTAR ANIMAL
                    </span>
                </div>
            </div>


            <form action="{base_url}cadastro/Consulta/ProcessarAnimal/" class="form-horizontal" method="post">

                <div class="form-body">

                    <div class="form-group last">
                        <label class="col-md-3 control-label">ID Animal</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <span class="input-group-addon input-circle-left">
                                    <i class="fa fa-paw"></i>
                                </span>
                                <input type="text" class="form-control input-circle-right" name="animal-id" placeholder="ID do Animal">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-circle green">Consultar</button>
                    </div>

                </div>

            </form>


            <hr>


            <?php if(empty($Animal)): ?>
            <div class="alert alert-warning row">
                <strong>Atenção!</strong>
                Nenhum animal encontrado com esse ID.
            </div>
            <?php endif; ?>



            <?php
                if(!empty($Animal)):
                $Animal = $Animal[0];
            ?>
            <div class="portlet-body form" style="display: <?php echo ($AnimalId) ? 'block' : 'none'; ?>" id="dashboard-animal">

                <div class="row">

                    <div class="col-sm-6">

                        <h4 class="form-section bold">
                            <i class="fa fa-globe" aria-hidden="true"></i>
                            Informações Gerais
                        </h4>


                        <!-- ID Animal -->
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">ID Animal:</label>
                            <div class="col-md-4">
                                <p class="form-control-static">#<?php echo $Animal->ani_id ?></p>
                            </div>
                        </div>


                        <!-- Autor do Registro -->
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Autor do Registro:</label>
                            <div class="col-md-5">
                                <p class="form-control-static"><?php echo (!is_null($Animal->fk_aut_id)) ? $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $Animal->fk_aut_id, 1, 'pes_nome_razao_social') . '<br>' . '<small>#'. $Animal->fk_aut_id .'</small>' : '-' ; ?></p>
                            </div>
                        </div>


                        <!-- Status -->
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Status:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $Animal->fk_sta_id, 1, 'sta_status');  ?></p>
                            </div>
                        </div>


                        <!-- N Chip -->
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Nº Chip:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $Animal->ani_chip; ?></p>
                            </div>
                        </div>


                        <!-- N Passaporte -->
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Nº Passaporte:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $Animal->ani_passaporte; ?></p>
                            </div>
                        </div>


                        <!-- Nome Completo -->
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Nome Completo</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $Animal->ani_nome_completo; ?></p>
                            </div>
                        </div>


                        <!-- Nome Patrocinado -->
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Nome Patrocinado</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (!is_null($Animal->ani_nome_patrocinado)) ? $Animal->ani_nome_patrocinado : '-'; ?></p>
                            </div>
                        </div>


                        <!-- Raca -->
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Raça:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $this->model_crud->get_rowSpecific('tb_animal_raca', 'anr_id', $Animal->fk_anr_id, 1, 'anr_raca');  ?></p>
                            </div>
                        </div>


                        <!-- Pelagem -->
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Pelagem:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $this->model_crud->get_rowSpecific('tb_animal_pelagem', 'anp_id', $Animal->fk_anp_id, 1, 'anp_pelagem');  ?></p>
                            </div>
                        </div>


                        <!-- Genero -->
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Gênero:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo $this->model_crud->get_rowSpecific('tb_animal_genero', 'ang_id', $Animal->fk_ang_id, 1, 'ang_genero');  ?></p>
                            </div>
                        </div>


                        <!-- Genero Tipo -->
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Gênero Tipo:</label>
                            <div class="col-md-4">
                                <p class="form-control-static">
                                <?php
                                    if(!is_null($Animal->fk_agt_id)){
                                        echo $this->model_crud->get_rowSpecific('tb_animal_genero_tipo', 'agt_id', $Animal->fk_agt_id, 1, 'agt_tipo'); 
                                    }else{
                                        echo "-";
                                    }
                                ?>
                                </p>
                            </div>
                        </div>


                        <!-- Peso -->
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Peso</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (!is_null($Animal->ani_peso)) ? $Animal->ani_peso : '-'; ?></p>
                            </div>
                        </div>


                        <!-- Altura da Cruz -->
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Altura da Cruz</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (!is_null($Animal->ani_altura_cruz)) ? $Animal->ani_altura_cruz : '-'; ?></p>
                            </div>
                        </div>


                        <!-- Animal Escola -->
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Animal pertence a Escola?</label>
                            <div class="col-md-4">
                                <p class="form-control-static">
                                    <small><?php echo (!is_null($Animal->ani_escola)) ? 'SIM' : 'NÃO' ?></small><!-- QA20172 -->
                                </p>
                            </div>
                        </div>


                        <!-- FLAG Permissao Autor -->
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Flag Permissão Autor</label>
                            <div class="col-md-4">
                                <p class="form-control-static">
                                    <small><?php echo (!is_null($Animal->flag_permissao_autor)) ? 'SIM' : 'NÃO' ?></small><!-- QA20172 -->
                                </p>
                            </div>
                        </div>

                    </div>


                    <div class="col-sm-6">

                        <h4 class="form-section bold">
                            <i class="fa fa-user" aria-hidden="true"></i>
                            Identificação
                        </h4>

                        <!-- N FEI -->
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Nº FEI:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (!is_null($Animal->ani_registro_fei)) ? $Animal->ani_registro_fei : '-'; ?></p>
                            </div>
                        </div>


                        <!-- N CAPA FEI -->
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Nº FEI:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (!is_null($Animal->ani_registro_capa_fei)) ? $Animal->ani_registro_capa_fei : '-'; ?></p>
                            </div>
                        </div>


                        <!-- N REGISTRO CBH -->
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Nº Registro CBH:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (!is_null($Animal->ani_registro_cbh)) ? $Animal->ani_registro_cbh : '-'; ?></p>
                            </div>
                        </div>


                        <div style="height: 100px;"> </div>


                        <h4 class="bold">
                            <i class="fa fa-user" aria-hidden="true"></i>
                            Genealogia
                        </h4>

                        <!-- N REGISTRO GENEALOGICO -->
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">Nº REGISTRO GENEALÓGICO:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (!is_null($Animal->ani_registro_genealogico)) ? $Animal->ani_registro_genealogico : '-'; ?></p>
                            </div>
                        </div>


                        <!-- NOME ASSOCIACAO -->
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">NOME ASSOCIAÇÃO DE REGISTRO:</label>
                            <div class="col-md-4">
                                <p class="form-control-static">
                                    <?php
                                        if(!is_null($Animal->fk_ass_id)){
                                            echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $Animal->fk_ass_id, 1, 'pes_nome_razao_social');
                                        }else{
                                            echo "-";
                                        }
                                    ?>
                                </p>

                            </div>
                        </div>


                        <!-- PAIS ORIGEM -->
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">PAÍS DE ORIGEM:</label>
                            <div class="col-md-4">
                                <p class="form-control-static">
                                    <?php
                                        if(!is_null($Animal->fk_pai_id)){
                                            echo $this->model_crud->get_rowSpecific('tb_pais', 'pai_id', $Animal->fk_pai_id, 1, 'pai_pais');
                                        }else{
                                            echo "-";
                                        }
                                    ?>
                                </p>

                            </div>
                        </div>


                        <!-- NOME PAI -->
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">NOME DO PAI:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (!is_null($Animal->ani_nome_pai)) ? $Animal->ani_nome_pai : '-'; ?></p>
                            </div>
                        </div>


                        <!-- NOME MAE -->
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">NOME DA MÃE:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (!is_null($Animal->ani_nome_mae)) ? $Animal->ani_nome_mae : '-'; ?></p>
                            </div>
                        </div>


                        <!-- NOME AVO MATERNO -->
                        <div class="form-group">
                            <label class="control-label col-md-6 bold">NOME AVÔ MATERNO:</label>
                            <div class="col-md-4">
                                <p class="form-control-static"><?php echo (!is_null($Animal->ani_nome_avo_materno)) ? $Animal->ani_nome_avo_materno : '-'; ?></p>
                            </div>
                        </div>


                    </div>

                </div>
                

            </div>
            <!-- /portlet-body -->


            <hr>



            <div class="tabbable-line"  style="display: <?php echo ($AnimalId) ? 'block' : 'none'; ?>" >


                <ul class="nav nav-tabs ">
                    <li class="active">
                        <a href="#tab-vinculos" data-toggle="tab">
                            <i class="fa fa-caret-right" aria-hidden="true"></i>
                            Vínculos
                        </a>
                    </li>
                    <li>
                        <a href="#tab-proprietario" data-toggle="tab">
                            <i class="fa fa-caret-right" aria-hidden="true"></i>
                            Proprietário
                        </a>
                    </li>
                    <li>
                        <a href="#tab-responsavel-financeiro" data-toggle="tab">
                            <i class="fa fa-caret-right" aria-hidden="true"></i>
                            Responsável Financeiro
                        </a>
                    </li>
                </ul>



                <div class="tab-content" style="padding-top: 10px;">



                    <!-- 
                    VINCULOS
                    =========================================== -->
                    <div class="tab-pane active" id="tab-vinculos">



                        <h3>
                            <i class="fa fa-link" aria-hidden="true"></i>
                            Vínculos Atuais
                        </h3>

                        <div class="table-scrollable table-scrollable-borderless">
                            <table class="table table-hover table-light">
                                <thead>
                                    <tr class="uppercase">
                                        <th> Empresa </th>
                                        <th> Vínculo </th>
                                    </tr>
                                </thead>
                                <tbody>


                                    <!-- 
                                    CONFEDERACAO
                                    =========================================
                                    -->
                                    <?php if(empty($ConfederacaoAtual)): ?>
                                    <tr>
                                        <td>
                                            <b>Confederação</b> <br>
                                            <small>
                                                [ Vínculo Inexistente ]
                                            </small>
                                        </td>
                                        <td> 
                                            <span class="label label-sm tooltips label-danger bold label-vinculo-atual">
                                                <i class="fa fa-close" aria-hidden="true"></i>
                                                Atleta não possui vínculo com nenhuma Confederação.
                                            </span>
                                        </td>
                                    </tr>
                                    <?php endif; ?>   
                                    <?php if(!empty($ConfederacaoAtual)): ?>
                                    <tr>
                                        <td>
                                            <b>Confederação</b> <br>
                                            <small>
                                                <?php
                                                    echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $ConfederacaoAtual[0]->fk_pes1_id, 1, 'pes_nome_razao_social');
                                                ?>
                                            </small>
                                        </td>
                                        <td> 
                                            <span class="label label-sm tooltips label-<?php echo GetBootstrapColor($ConfederacaoAtual[0]->fk_sta_id); ?> bold label-vinculo-atual" data-original-title="<?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $ConfederacaoAtual[0]->fk_sta_id, 1, 'sta_status'); ?>" >
                                                <i class="fa <?php echo GetIconVinculo($ConfederacaoAtual[0]->fk_sta_id); ?>" aria-hidden="true"></i>
                                                <?php
                                                    echo GetStatusVinculo($ConfederacaoAtual[0]->fk_sta_id);
                                                ?>
                                            </span>
                                        </td>
                                    </tr>
                                    <?php endif; ?>   



                                    <!-- 
                                    FEDERACAO
                                    =========================================
                                    -->
                                    <?php if(empty($FederacaoAtual)): ?>
                                    <tr>
                                        <td>
                                            <b>Federação</b> <br>
                                            <small>
                                                [ Vínculo Inexistente ]
                                            </small>
                                        </td>
                                        <td> 
                                            <span class="label label-sm tooltips label-danger bold label-vinculo-atual">
                                                <i class="fa fa-close" aria-hidden="true"></i>
                                                Animal não possui vínculo
                                            </span>
                                        </td>
                                    </tr>
                                    <?php endif; ?>                                                    
                                    <?php if(!empty($FederacaoAtual)): ?>
                                    <tr>
                                        <td>
                                            <b>Federação</b> <br>
                                            <small>
                                                <?php
                                                    echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $FederacaoAtual[0]->fk_pes1_id, 1, 'pes_nome_razao_social');
                                                ?>
                                            </small>
                                        </td>
                                        <td> 
                                            <span class="label label-sm tooltips label-<?php echo GetBootstrapColor($FederacaoAtual[0]->fk_sta_id); ?> bold label-vinculo-atual" data-original-title="<?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $FederacaoAtual[0]->fk_sta_id, 1, 'sta_status'); ?>" >
                                                <i class="fa <?php echo GetIconVinculo($FederacaoAtual[0]->fk_sta_id); ?>" aria-hidden="true"></i>
                                                <?php
                                                    echo GetStatusVinculo($FederacaoAtual[0]->fk_sta_id);
                                                ?>
                                            </span>
                                        </td>
                                    </tr>
                                    <?php endif; ?>                                                    




                                    <!-- 
                                    ENTIDADE EQUESTRE
                                    =========================================
                                    -->
                                    <?php if(empty($EntidadeAtual)): ?>
                                    <tr>
                                        <td>
                                            <b>Entidade Equestre</b> <br>
                                            <small>
                                                [ Vínculo Inexistente ]
                                            </small>
                                        </td>
                                        <td> 
                                            <span class="label label-sm tooltips label-danger bold label-vinculo-atual">
                                                <i class="fa fa-close" aria-hidden="true"></i>
                                                Animal não possui vínculo
                                            </span>
                                        </td>
                                    </tr>
                                    <?php endif; ?>

                                    <?php if(!empty($EntidadeAtual)): ?>
                                    <tr>
                                        <td>
                                            <b>Entidade Equestre</b> <br>
                                            <small>
                                                <?php
                                                    echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $EntidadeAtual[0]->fk_pes1_id, 1, 'pes_nome_razao_social');
                                                ?>
                                            </small>
                                        </td>
                                        <td> 
                                            <span class="label label-sm tooltips label-<?php echo GetBootstrapColor($EntidadeAtual[0]->fk_sta_id); ?> bold label-vinculo-atual" data-original-title="<?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $EntidadeAtual[0]->fk_sta_id, 1, 'sta_status'); ?>" >
                                                <i class="fa <?php echo GetIconVinculo($EntidadeAtual[0]->fk_sta_id); ?>" aria-hidden="true"></i>
                                                <?php
                                                    echo GetStatusVinculo($EntidadeAtual[0]->fk_sta_id);
                                                ?>
                                            </span>
                                        </td>
                                    </tr>
                                    <?php endif; ?>



                                </tbody>
                            </table>
                        </div>  

                        <hr style="margin:40px 0;">

                        <h3>
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                            Histórico Vínculos
                        </h3>

                        <div class="tabbable-line">
                            <ul class="nav nav-tabs ">
                                <li class="active">
                                    <a href="#tab_atleta_vinculos_confederao" data-toggle="tab" aria-expanded="true"> Confederação </a>
                                </li>
                                <li class="">
                                    <a href="#tab_atleta_vinculos_federacao" data-toggle="tab" aria-expanded="false"> Federação </a>
                                </li>
                                <li>
                                    <a href="#tab_atleta_vinculos_entidade" data-toggle="tab"> Entidade Equestre </a>
                                </li>
                            </ul>
                            <div class="tab-content" style="padding-top: 0px;">    

                                <!-- Confederacao -->
                                <div class="tab-pane active" id="tab_atleta_vinculos_confederao">



                                    <?php if(empty($Confederacao)): ?>
                                    <div class="alert alert-danger margin-top-10">
                                        <strong>Atenção!</strong>
                                        Esse atleta não possui vínculo com nenhuma Confederação.
                                    </div>
                                    <?php endif; ?>
                                    

                                    <?php if(!empty($Confederacao)): ?>
                                    <div class="table-scrollable">
                                        <table class="table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th> # </th>
                                                    <th> Confederação </th>
                                                    <th> Início Vínculo </th>
                                                    <th> Fim Vínculo </th>
                                                    <th> Status </th>
                                                    <th> * </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    foreach ($Confederacao as $key => $value):
                                                        $NomeRazaoSocial    =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes1_id, 1, 'pes_nome_razao_social');
                                                ?>
                                                <tr>
                                                    <td> <?php echo $value->vin_id; ?> </td>
                                                    <td> <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes1_id, 1, 'pes_nome_razao_social'); ?> </td>
                                                    <td> <?php echo $this->my_data->ConverterData($value->criado, 'ISO', 'PT-BR'); ?> </td>
                                                    <td> - </td>
                                                    <td>                                   
                                                        <span class="label label-sm tooltips label-<?php echo GetBootstrapColor($value->fk_sta_id); ?>" data-original-title="<?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $value->fk_sta_id, 1, 'sta_status'); ?>" >
                                                            <?php
                                                                echo GetStatusVinculo($value->fk_sta_id);
                                                            ?>
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <a href="#" title="" class="vinculo-detalhe-link" data-id="<?php echo $value->vin_id; ?>" data-nome-empresa="<?php echo $NomeRazaoSocial; ?>" data-id-empresa="<?php echo $value->fk_pes1_id; ?>">
                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            Histórico
                                                        </a>
                                                    </td>
                                                </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <?php endif; ?>


                                </div>
                                <!-- /Confederacao -->


                                <!-- Federacao -->
                                <div class="tab-pane" id="tab_atleta_vinculos_federacao">


                                    <?php if(empty($Federacao)): ?>
                                    <div class="alert alert-danger margin-top-10">
                                        <strong>Atenção!</strong>
                                        Esse atleta não possui vínculo com nenhuma Federação.
                                    </div>
                                    <?php endif; ?>


                                    <?php if(!empty($Federacao)): ?>
                                    <div class="table-scrollable">
                                        <table class="table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th> # </th>
                                                    <th> Federação </th>
                                                    <th> Início Vínculo </th>
                                                    <th> Fim Vínculo </th>
                                                    <th> Status </th>
                                                    <th> * </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    foreach ($Federacao as $key => $value):
                                                        $NomeRazaoSocial    =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes1_id, 1, 'pes_nome_razao_social');
                                                ?>
                                                <tr>
                                                    <td> <?php echo $value->vin_id; ?> </td>
                                                    <td> <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes1_id, 1, 'pes_nome_razao_social'); ?> </td>
                                                    <td> <?php echo $this->my_data->ConverterData($value->criado, 'ISO', 'PT-BR'); ?> </td>
                                                    <td> - </td>
                                                    <td>                                   
                                                        <span class="label label-sm tooltips label-<?php echo GetBootstrapColor($value->fk_sta_id); ?>" data-original-title="<?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $value->fk_sta_id, 1, 'sta_status'); ?>" >
                                                            <?php
                                                                echo GetStatusVinculo($value->fk_sta_id);
                                                                ?>
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <a href="#" title="" class="vinculo-detalhe-link" data-id="<?php echo $value->vin_id; ?>" data-nome-empresa="<?php echo $NomeRazaoSocial; ?>" data-id-empresa="<?php echo $value->fk_pes1_id; ?>">
                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            Histórico
                                                        </a>
                                                    </td>
                                                </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <?php endif; ?>

                                </div>
                                <!-- /Federacao -->

                                <!-- Entidade Equestre -->
                                <div class="tab-pane" id="tab_atleta_vinculos_entidade">

                                    <?php if(empty($Entidade)): ?>
                                    <div class="alert alert-warning margin-top-10">
                                        <strong>Atenção!</strong>
                                        Esse atleta não possui vínculo com nenhuma Entidade Equestre.
                                    </div>
                                    <?php endif; ?>


                                    <?php if(!empty($Entidade)): ?>
                                    <div class="table-scrollable">
                                        <table class="table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th> # </th>
                                                    <th> Entidade </th>
                                                    <th> Início Vínculo </th>
                                                    <th> Fim Vínculo </th>
                                                    <th> Status </th>
                                                    <th> * </th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <?php
                                                    foreach ($Entidade as $key => $value):
                                                        $NomeRazaoSocial    =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes1_id, 1, 'pes_nome_razao_social');
                                                ?>
                                                <tr>
                                                    <td> <?php echo $value->vin_id; ?> </td>
                                                    <td> <?php echo $NomeRazaoSocial; ?> </td>
                                                    <td> <?php echo $this->my_data->ConverterData($value->criado, 'ISO', 'PT-BR'); ?> </td>
                                                    <td> - </td>
                                                    <td>                                   
                                                        <span class="label label-sm tooltips label-<?php echo GetBootstrapColor($value->fk_sta_id); ?>" data-original-title="<?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $value->fk_sta_id, 1, 'sta_status'); ?>" >
                                                        <?php echo GetStatusVinculo($value->fk_sta_id); ?>
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <a href="#" title="" class="vinculo-detalhe-link" data-id="<?php echo $value->vin_id; ?>" data-nome-empresa="<?php echo $NomeRazaoSocial; ?>" data-id-empresa="<?php echo $value->fk_pes1_id; ?>">
                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            Histórico
                                                        </a>
                                                    </td>
                                                </tr>
                                                <?php endforeach; ?>


                                            </tbody>
                                        </table>
                                    </div>
                                    <?php endif; ?>

                                </div>
                                <!-- /Entidade Equestre -->



                            </div>
                        </div>

                    </div>
                    <!-- // vinculos -->




                    <!-- 
                    PROPRIETARIO
                    =========================================== -->
                    <div class="tab-pane" id="tab-proprietario">

                        <div class="portlet light" style="padding-top: 0;">
                            <div class="portlet-body">

                    

                                <?php if(empty($ProprietarioAnimal)): ?>
                                <div class="alert alert-warning row">
                                    <strong>Atenção!</strong>
                                    Não existe nenhum proprietário para esse animal.
                                </div>
                                <?php endif; ?>
                                

                                <?php if(!empty($ProprietarioAnimal)): ?>
                                <div class="table-scrollable">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th> # </th>
                                                <th> Proprietário </th>
                                                <th> Início Vínculo </th>
                                                <th> Fim Vínculo </th>
                                                <th> Status </th>
                                                <th> * </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                foreach ($ProprietarioAnimal as $key => $value):
                                                    $NomeRazaoSocial    =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes1_id, 1, 'pes_nome_razao_social');
                                            ?>
                                            <tr>
                                                <td> <?php echo $value->vin_id; ?> </td>
                                                <td> <?php echo $NomeRazaoSocial; ?> </td>
                                                <td> <?php echo $this->my_data->ConverterData($value->criado, 'ISO', 'PT-BR'); ?> </td>
                                                <td> - </td>
                                                <td>                                            
                                                    <span class="label label-sm tooltips label-<?php echo GetBootstrapColor($value->fk_sta_id); ?>" data-original-title="<?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $value->fk_sta_id, 1, 'sta_status'); ?>" >
                                                        <?php
                                                            echo GetStatusVinculo($value->fk_sta_id);
                                                            ?>
                                                    </span>
                                                </td>
                                                <td>
                                                    <a href="#" title="" class="vinculo-detalhe-link" data-id="<?php echo $value->vin_id; ?>" data-nome-empresa="<?php echo $NomeRazaoSocial; ?>" data-id-empresa="<?php echo $value->fk_pes1_id; ?>">
                                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                        Histórico
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php endif; ?>

                            </div>
                        </div>

                    </div>



                    <!-- 
                    RESPONSAVEL FINANCEIRO
                    =========================================== -->
                    <div class="tab-pane" id="tab-responsavel-financeiro">
                        <div class="alert alert-danger"> <strong>Atenção!</strong> Aba não implantada. MBOTQA </div>

                        <div class="portlet light" style="padding-top: 0;">
                            <div class="portlet-body">
                                <div class="table-scrollable">
                                    <table class="table table-hover">
                                       
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>




                </div>
            </div>





            <?php endif; ?>




        </div>
        <!-- /portlet -->















    <!-- 
    MODAL ( DETALHES DO VINCULO )
    ==================================== -->
    <div class="modal fade bs-modal-lg" id="vinculo-detalhe" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title bold">CITTAC - Centro Integrado de Treinamento dos Trotadores de Águas Claras    </h4>
                    <div>
                        <small>
                            <b>ID Vínculo:</b> #<span id="vinculo-detalhe-id"></span> | 
                            <b>ID Empresa:</b> #<span id="vinculo-detalhe-id-empresa"></span>
                        </small>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="modal-body">

                        <div id="vinculo-detalhe-carregando" style="text-align: center; color: #008076; ">
                            <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
                            <h3 style="display: inline; ">Aguarde, carregando...</h3>
                        </div>

                        <div class="table-scrollable" id="vinculo-detalhe-tabela" style="display: none;">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th> # </th>
                                        <th> Autor </th>
                                        <th> Atualizado em </th>
                                        <th> Status </th>
                                        <th> Observação </th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <tr>
                                        <td> 137 </td>
                                        <td> Cida Aparecida </td>
                                        <td> 14/01/2017 </td>
                                        <td>
                                            <span class="label label-sm label-danger"> Vínculo Cancelado </span>
                                        </td>
                                        <td> <a href="#" title=""><i class="fa fa-plus-circle" aria-hidden="true"></i> Observação</a> </td>
                                    </tr>

                                    <tr>
                                        <td> 67 </td>
                                        <td> Carla Rosana de Paula </td>
                                        <td> 03/03/2016 </td>
                                        <td>
                                            <span class="label label-sm label-success"> Vínculo Aprovado </span>
                                        </td>
                                        <td> <a href="#" title=""><i class="fa fa-plus-circle" aria-hidden="true"></i> Observação</a> </td>
                                    </tr>

                                    <tr>
                                        <td> 53 </td>
                                        <td> Carla Rosana de Paula </td>
                                        <td> 01/03/2016 </td>
                                        <td>
                                            <span class="label label-sm label-warning"> Vínculo Rejeitado </span>
                                        </td>
                                        <td> <a href="#" title=""><i class="fa fa-plus-circle" aria-hidden="true"></i> Observação</a> </td>
                                    </tr>

                                    <tr>
                                        <td> 37 </td>
                                        <td> Gustavo Mendes Botega </td>
                                        <td> 28/02/2016 </td>
                                        <td>
                                            <span class="label label-sm label-default"> Aguardando Validação </span>
                                        </td>
                                        <td> - </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /table-scrollable -->

                    </div>                    


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn green" data-dismiss="modal">Fechar</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
