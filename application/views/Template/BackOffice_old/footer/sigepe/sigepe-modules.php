
        <!-- BEGIN SIGEPE MODULES -->
		<?php if(isset($PackageScripts) && !is_null($PackageScripts)): ?>
			<?php foreach ($PackageScripts as $script): ?>   
				<?php if(!is_null($script)):?>
					<script src="<?php echo base_url() . 'assets/sigepe/' . $script; ?>.js" type="text/javascript"></script>
				<?php endif;?>
			<?php endforeach; ?>
        <?php endif; ?>
        <!-- END SIGEPE MODULES -->

