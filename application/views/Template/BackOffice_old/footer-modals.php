
<!--
  COMMON ( all operations Crud )
=============================================-->
<div id="modalLoading" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-body" style="text-align:center;">
        <i class="fa fa-refresh fa-spin" style="margin: 80px auto; text-align: center; font-size: 10em; color: #fff;"></i>
         <h2 style="font-weight:900;color:white;">Aguarde,</h2>
         <div style="text-align: center; width: 80%; color: #fff; font-size: 15px; margin: 10px auto 30px auto;">estamos processando as informações. Pode demorar alguns instantes.</div>
    </div>
</div>


<!-- Modal Insert Again -->
<div id="modalAlert" class="modal fade bs3patch " tabindex="-1" data-backdrop="static" data-keyboard="false">
	<div class="modal-body">
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn blue">Fechar Aviso</button>
	</div>
</div>



<!-- Modal Change Password -->
<div id="modalChangePassword" class="modal fade bs3patch modal-scroll" tabindex="-1" data-backdrop="static" data-keyboard="false">
    
	<form class="form-horizontal" id="formChangePassword" method="POST">

		<div class="modal-header">
			<h4 class="modal-title">Alteração de Senha</h4>
		</div>

	    <div class="modal-body">

			<div class="row" style="margin:0;padding:0;">

				<div class="col-sm-12">

					<div class="alert alert-danger display-hide">
						<button class="close" data-close="alert"></button>
						Há alguns erros no formulário. Verifique e tente novamente.
					</div>
					<div class="alert alert-success display-hide">
						<button class="close" data-close="alert"></button>
						Formulário validado com sucesso!
					</div>

					<div class="form-group">
						<label class="control-label">Senha Atual</label>
						<input type="password" id="passwordCurrent" name="passwordCurrent" class="form-control" placeholder="Entre com sua senha atual" />
					</div>

					<hr />

					<div class="form-group">
						<label class="control-label">Nova Senha</label>
						<input type="password" id="password_strength" name="passwordNew" class="form-control" placeholder="" />
					</div>

					<div class="form-group">
						<label class="control-label">Confirmação</label>
						<input type="password" id="passwordConfirm" name="passwordConfirm" class="form-control" placeholder="" />
						<span class="help-block"></span>
					</div>


				</div>

			</div>

	    </div>

		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn gray">Fechar Janela</button>
			<button type="submit" class="btn blue">Trocar Senha</button>
		</div>

	</form>

</div>



<!-- * * MODAL - CONFIRMACAO DE DELECAO * static * -->
<div id="staticDelete" class="modal fade bs3patch" tabindex="-1" data-backdrop="static" data-keyboard="false">
	<div class="modal-body">
		<p>
			{peopleFirstName}, tem certeza que deseja excluir esse item?
			<hr />
			<small>
				<b>Módulo:</b> <span id="modal-module">{module}</span> |
				<b>ID:</b> <span id="modal-identification"></span> |
				<b>Autor:</b> {peopleFirstName}
			</small>
		</p>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">Cancelar</button>
		<button type="button" data-dismiss="modal" class="btn red-sunglo" data-href="" id="modal-delete">Excluir Definitivamente</button>
	</div>
</div>


<!-- * * MODAL - DETAIL OF ITEM * static * -->
<div id="modalDatatableDetails" class="modal container modal-scroll fade bs3patch" tabindex="-1">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">Detalhes</h4>
	</div>
	<div class="modal-body">
		<p class="loading">
			 <i class="fa fa-spinner fa-spin"></i>
			 <span>Carregando <i class="fa fa-ellipsis-h fa-5x"></i></span>
		</p>
		<div id="modal-result">
			Conteúdo
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">Fechar Janela</button>
	</div>
</div>




<!-- * * MODAL - FILEMANAGER -->
<div id="modalFileManager" class="modal container modal-scroll fade bs3patch" tabindex="-1">
	<div class="modal-body">
		<div id="modal-result">
			<!-- <iframe src="http://www.fhbr.com.br/manager/filemanager/dialog.php?type=2&field_id=fieldID3&crossdomain=1" style="width:100%;min-height:700px;border:0;" frameborder="0"></iframe> -->
			<iframe src="http://www.fhbr.com.br/manager/filemanager/dialog.php?type=0" style="width:100%;min-height:700px;border:0;" frameborder="0"></iframe>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">Fechar Janela</button>
	</div>
</div>


<!-- Modal Insert Again -->
<div id="modalInsertAgain" class="modal fade bs-modal-sm bs3patch bs3patchNotCss" tabindex="-1" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Aviso</h4>
			</div>
			<div class="modal-body">
				<p>
			 		Deseja realizar mais um cadastro?
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal" id="smallDenial">Não</button>
				<button type="button" class="btn blue" data-dismiss="modal" id="smallStatement">Sim</button>
			</div>
		</div>
	</div>
</div>



<!--
  CREATE
=============================================-->
<?php if($crudOperation=='create'): ?>
<?php endif; ?>



<!--
  RETRIEVE
=============================================-->
<?php if($crudOperation == 'retrieve'): ?>
<?php endif; ?>