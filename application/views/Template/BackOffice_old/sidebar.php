

	<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper">
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
			<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
			<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
			<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
			<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
			<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
			<ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">


				<!-- 
				Evento
				*************************************************-->
				<!-- <li class="">
					<a href="{base_url}engine/gear/evento/retrieve">
					<i class="fa fa-calendar"></i>	
					<span class="title">Evento</span>
					</a>
				</li> -->


				<!-- 
				Páginas
				*************************************************-->
				<li class="">
					<a href="javascript:;">
					<i class="fa fa-file-text-o"></i>					
					<span class="title">Páginas</span>
					<span class="arrow"></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{base_url}engine/gear/pagina/">
							<i></i>
							Gerenciar Páginas </a>
						</li>
						<li>
							<a href="{base_url}engine/gear/categoriapagina/">
							<i></i>
							Gerenciar Categorias </a>
						</li>
					</ul>
				</li>



				<!-- 
				Modalidades
				*************************************************-->
				<li class="">
					<a href="{base_url}engine/gear/modalidade/">
					<i class="fa fa-certificate"></i>
					<span class="title">Modalidades</span>
					</a>
				</li>
						

				<!-- 
				Entidades
				*************************************************-->
				<li class="">
					<a href="{base_url}engine/gear/entidade/">
					<i class="fa fa-users"></i>
					<span class="title">Entidades</span>
					</a>
				</li>


				<!-- 
				Blog
				*************************************************-->
				<li class="">
					<a href="javascript:;">
					<i class="fa fa-newspaper-o"></i>
					<span class="title">Blog</span>
					<span class="arrow"></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{base_url}engine/gear/blog/">
							<i></i>
							Gerenciar Posts </a>
						</li>
						<li>
							<a href="{base_url}engine/gear/categoria/">
							<i></i>
							Gerenciar Categorias </a>
						</li>
					</ul>
				</li>


				<!-- 
				Parceiros
				*************************************************-->
				<li class="">
					<a href="{base_url}engine/gear/parceiro/">
					<i class="fa fa-building"></i>
					<span class="title">Parceiros</span>
					</a>
				</li>


				<!-- 
				Contato
				*************************************************-->
				<li class="">
					<a href="javascript:;">
					<i class="fa fa-phone"></i>
					<span class="title">Contato</span>
					<span class="arrow"></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="{base_url}engine/gear/departamento/">
							<i></i>
							Departamento </a>
						</li>
						<li>
							<a href="{base_url}engine/gear/comoencontrou/">
							<i></i>
							Como nos encontrou </a>
						</li>
					</ul>
				</li>

				<!-- 
				Anúncios
				*************************************************-->
				<li class="">
					<a href="{base_url}engine/gear/advertising/">
					<i class="fa fa-bullhorn"></i>
					<span class="title">Anúncio</span>
					</a>
				</li>


				<!-- 
				Configuracao
				*************************************************-->
				<li class="">
					<a href="filemanager/dialog.php?type=0&editor=mce_0" id="linkFileManager">
					<i class="fa fa-folder-open"></i>
					<span class="title">Arquivos</span>
					</a>
				</li>


				<!-- 
				Configuracao
				*************************************************-->
				<li class="">
					<a href="{base_url}engine/gear/configuracao/">
					<i class="fa fa-cogs"></i>
					<span class="title">Configuração</span>
					</a>
				</li>



			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>
	<!-- END SIDEBAR -->
