<!-- Variable Environment Javascripts
============================================= -->
<script type="text/javascript">


function VariablesEnvironment(){

	return setVariablesEnvironment();
}

function setVariablesEnvironment(){
			
			var slug 		         			=	''; // ???

			/* Template */
			window.baseUrl							=	'{base_url}';
			window.userId        					=	'{userId}';
			window.peopleId      					=	'{peopleId}';
			window.peopleFirstName 				= 	'{peopleFirstName}';
			window.crudOperation 					= 	'{crudOperation}';
			window.extensionModule 				= 	false;
							
			/* General Settings Module */
			window.moduleSlug						=	'<?php echo (isset($moduleSlug)) ? $moduleSlug : ""; ?>';
			window.module							=	'<?php echo (isset($module)) ? $module : ""; ?>';
			window.formIdentification     			= 	'<?php echo (isset($formIdentification)) ? $formIdentification : ""; ?>';

			/* Database */
			window.databaseTable					=	'<?php echo (isset($databaseTable)) ? $databaseTable : ""; ?>';
			window.databaseColumnPk				=	'<?php echo (isset($databaseColumnPk)) ? $databaseColumnPk : ""; ?>';
			window.databaseColumnFk				=	'<?php echo (isset($databaseColumnFk)) ? $databaseColumnFk : ""; ?>';
			window.databaseItemCurrent				=	'<?php echo (isset($databaseItemCurrent)) ? $databaseItemCurrent : ""; ?>'; /* Change to databaseItemCurrent */

			/* Thumb */
			window.tabThumb						=	'<?php echo (isset($tabThumb)) ? $tabThumb : ""; ?>';
			window.pathThumb						=	'<?php echo (isset($pathThumb)) ? $pathThumb : ""; ?>';
			window.databaseColumnThumb				=	'<?php echo (isset($databaseColumnThumb)) ? $databaseColumnThumb : ""; ?>';
			window.filenameCurrentThumb			=	'<?php echo (isset($filenameCurrentThumb)) ? $filenameCurrentThumb : ""; ?>';

			/* Map */
			window.tabMapsCenterLat				=	'<?php echo (isset($tabMapsCenterLat)) ? $tabMapsCenterLat : ""; ?>';
			window.tabMapsCenterLng				=	'<?php echo (isset($tabMapsCenterLng)) ? $tabMapsCenterLng : ""; ?>';
			window.tabMapsInitialZoom				=	'<?php echo (isset($tabMapsInitialZoom)) ? $tabMapsInitialZoom : ""; ?>';
			window.tabMapsCurrentLat				=	'<?php echo (isset($tabMapsCurrentLat)) ? $tabMapsCurrentLat : ""; ?>';
			window.tabMapsCurrentLng				=	'<?php echo (isset($tabMapsCurrentLng)) ? $tabMapsCurrentLng : ""; ?>';
			window.tabMapsDatabaseColumnLat		=	'<?php echo (isset($tabMapsDatabaseColumnLat)) ? $tabMapsDatabaseColumnLat : ""; ?>';
			window.tabMapsDatabaseColumnLng		=	'<?php echo (isset($tabMapsDatabaseColumnLng)) ? $tabMapsDatabaseColumnLng : ""; ?>';

			/* Gallery */
			window.tabGallery 						=	'<?php echo (isset($tabGallery) && $tabGallery==TRUE) ? "true" : "false"; ?>';
			window.databaseTableGallery 			=	'<?php echo (isset($databaseTableGallery)) ? $databaseTableGallery: ""; ?>';
			window.databaseTableGalleryColumnFk 	=	'<?php echo (isset($databaseTableGalleryColumnFk)) ? $databaseTableGalleryColumnFk: ""; ?>';
			window.databaseTableGalleryColumnPk 	=	'<?php echo (isset($databaseTableGalleryColumnPk)) ? $databaseTableGalleryColumnPk: ""; ?>';
			window.databaseTableGalleryColumnThumb =	'<?php echo (isset($databaseTableGalleryColumnThumb)) ? $databaseTableGalleryColumnThumb: ""; ?>';
			window.pathGallery 					=	'<?php echo (isset($pathGallery)) ? $pathGallery: ""; ?>';
			window.databaseModuleColumnFolder 		=	'<?php echo (isset($databaseModuleColumnFolder)) ? $databaseModuleColumnFolder: ""; ?>';

			/* Rules Validation Form */
			window.rulesDetails					=	'{rulesDetails}';


}

VariablesEnvironment();


</script>
