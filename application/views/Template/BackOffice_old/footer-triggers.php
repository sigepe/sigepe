

<!-- Triggers
============================================= -->
<script>
jQuery(document).ready(function() {  
	Metronic.init(); 
	Layout.init(); 
	Demo.init(); 
	FormChangePassword.init(); 
	<?php if($use_datatable): ?>TableAdvanced.init();<?php endif; ?>		
	<?php if($use_modal): ?>UIExtendedModals.init();<?php endif; ?>
	<?php if($use_toastr): ?>UIToastr.init();<?php endif; ?>
	<?php if($use_dropdown): ?>ComponentsDropdowns.init();<?php endif; ?>
	<?php if($use_datepicker): ?>ComponentsPickers.init();<?php endif; ?>
	<?php if($use_cft): ?>ComponentsFormTools.init();<?php endif; ?>
	<?php if($use_cft): ?>FormSamples.init();<?php endif; ?>
  	<?php if($use_cft): ?>ComponentsEditors.init();<?php endif; ?>
	<?php if($use_cft): ?>FormValidation.init();<?php endif; ?>
	<?php if($use_jfum): ?>FormFileUpload.init();<?php endif; ?>
	<?php if($use_portfolio): ?>Portfolio.init();<?php endif; ?>
	<?php if(isset($tabMaps) && $tabMaps): ?>initializeMap();<?php endif; ?>
});
</script>
<!-- END JAVASCRIPTS -->

<!-- Module Scripts
============================================= -->
<?php if(!empty($moduleJs)): ?>
  <?php foreach ($moduleJs as $scripts): ?>
  <script src="<?php echo base_url() . 'assets/modules/' . $moduleSlug . '/js/' . $scripts; ?>" type="text/javascript"></script>
  <?php endforeach; ?>
<?php endif; ?>


</body>
<!-- END BODY -->
</html>