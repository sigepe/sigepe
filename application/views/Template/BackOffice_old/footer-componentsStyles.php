
<!-- Setup General Styles
============================================= -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="{base_url}assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="{base_url}assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="{base_url}assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="{base_url}assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<link href="{base_url}assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>


<!-- Datepicker
============================================= -->
<?php if($use_datepicker): ?>
<link rel="stylesheet" type="text/css" href="{base_url}assets/global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="{base_url}assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
<link rel="stylesheet" type="text/css" href="{base_url}assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="{base_url}assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="{base_url}assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="{base_url}assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<?php endif; ?>


<!-- Select - Custom Dropdowns
============================================= -->
<?php if($use_dropdown): ?>
<link rel="stylesheet" type="text/css" href="{base_url}assets/global/plugins/bootstrap-select/bootstrap-select.min.css"/>
<link rel="stylesheet" type="text/css" href="{base_url}assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="{base_url}assets/global/plugins/jquery-multi-select/css/multi-select.css"/>
<?php endif; ?> 


<!-- CFT - Components Form Tools
============================================= -->
<?php if($use_cft): ?>
<link rel="stylesheet" type="text/css" href="{base_url}assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="{base_url}assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>
<link rel="stylesheet" type="text/css" href="{base_url}assets/global/plugins/jquery-tags-input/jquery.tagsinput.css"/>
<link rel="stylesheet" type="text/css" href="{base_url}assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet" type="text/css" href="{base_url}assets/global/plugins/typeahead/typeahead.css">
<?php endif; ?> 


<!-- Markdown & WYSIWYG Editors
============================================= -->
<?php if($use_cft): ?>
<link rel="stylesheet" type="text/css" href="{base_url}assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
<link rel="stylesheet" type="text/css" href="{base_url}assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet" type="text/css" href="{base_url}assets/global/plugins/bootstrap-summernote/summernote.css">
<?php endif; ?> 


<!-- Toastr
============================================= -->
<?php if($use_toastr): ?>
<link rel="stylesheet" type="text/css" href="{base_url}assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
<?php endif; ?>


<!-- Datatable
============================================= -->
<?php if($use_datatable): ?>
<link rel="stylesheet" type="text/css" href="{base_url}assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="{base_url}assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="{base_url}assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="{base_url}assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<?php endif; ?>


<!-- Fancybox
============================================= -->
<link href="{base_url}assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>


<!-- Modal
============================================= -->
<?php if($use_modal): ?>
<link href="{base_url}assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="{base_url}assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<?php endif; ?>

<!-- jFum
============================================= -->
<?php if($use_jfum): ?>
<link rel="stylesheet" href="{base_url}assets/override/plugins/jfum/css/style.css">
<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<link rel="stylesheet" href="{base_url}assets/override/plugins/jfum/css/jquery.fileupload.css">
<link rel="stylesheet" href="{base_url}assets/override/plugins/jfum/css/jquery.fileupload-ui.css">
<noscript><link rel="stylesheet" href="{base_url}assets/override/plugins/jfum/css/jquery.fileupload-noscript.css"></noscript>
<noscript><link rel="stylesheet" href="{base_url}assets/override/plugins/jfum/css/jquery.fileupload-ui-noscript.css"></noscript>
<?php endif; ?>

<!-- jCrop
============================================= -->
<?php if($use_jcrop): ?>
  <link href="{base_url}assets/override/plugins/cropper-master/dist/cropper.min.css" rel="stylesheet">
  <link href="{base_url}assets/override/plugins/cropper-master/examples/crop-avatar/css/main.css" rel="stylesheet">
<?php endif; ?>


<!-- Portfolio
============================================= -->
<?php if($use_portfolio): ?>
<link href="{base_url}assets/admin/pages/css/portfolio.css" rel="stylesheet" type="text/css"/>
<?php endif; ?>


<!-- Theme Styles Global
============================================= -->
<link href="{base_url}assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="{base_url}assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="{base_url}assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
<link id="style_color" href="{base_url}assets/admin/layout4/css/themes/light.css" rel="stylesheet" type="text/css"/>
<link href="{base_url}assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>
<link href="{base_url}assets/admin/layout4/css/vanguarda.css" rel="stylesheet" type="text/css"/>


<!-- Module Styles
============================================= -->
<?php if(!empty($moduleCss)): ?>
	<?php foreach ($moduleCss as $style): ?>
	<link href="<?php echo base_url() . 'assets/modules/' . $moduleSlug . '/css/' . $style; ?>" rel="stylesheet" type="text/css"/>
	<?php endforeach; ?>
<?php endif; ?>
