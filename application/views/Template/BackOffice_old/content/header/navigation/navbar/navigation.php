

                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown active">
                                            <a href="javascript:;">
                                                <i class="fa fa-home"></i> Início
                                                <span class="arrow"></span>
                                            </a>
                                        </li>

                                        <!-- 
                                        MEU PERFIL
                                        ========================= -->
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown ">
                                            <a href="javascript:;">
                                                <i class="fa fa-user"></i> Meu Perfil
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-left">
                                                <li aria-haspopup="true" class=" ">
                                                    <a href="#" class="nav-link  "> Minhas Informações </a>
                                                </li>
                                                <li aria-haspopup="true" class="dropdown-submenu ">
                                                    <a href="javascript:;" class="nav-link nav-toggle ">
                                                        Perfis
                                                        <span class="arrow"></span>
                                                    </a>
                                                    <ul class="dropdown-menu">
                                                        <li aria-haspopup="true" class=" "> <a href="#" class="nav-link "> Atleta </a> </li>
                                                        <li aria-haspopup="true" class=" "> <a href="#" class="nav-link "> Desenhador de Percurso (Armador) </a> </li>
                                                        <li aria-haspopup="true" class=" "> <a href="#" class="nav-link "> Juiz </a> </li>
                                                        <li aria-haspopup="true" class=" "> <a href="#" class="nav-link "> Juiz Externo </a> </li>
                                                        <li aria-haspopup="true" class=" "> <a href="#" class="nav-link "> Delegado Técnico </a> </li>
                                                        <li aria-haspopup="true" class=" "> <a href="#" class="nav-link "> Comissário </a> </li>
                                                        <li aria-haspopup="true" class=" "> <a href="#" class="nav-link "> Veterinário </a> </li>
                                                        <li aria-haspopup="true" class=" "> <a href="#" class="nav-link "> Desenhador </a> </li>
                                                        <li aria-haspopup="true" class=" "> <a href="#" class="nav-link "> Instrutor </a> </li>
                                                        <li aria-haspopup="true" class=" "> <a href="#" class="nav-link "> Locutor </a> </li>
                                                    </ul>
                                                </li>
                                                <li aria-haspopup="true" class=" ">
                                                    <a href="#" class="nav-link  "> Dependentes </a>
                                                </li>
                                                <li aria-haspopup="true" class=" ">
                                                    <a href="#" class="nav-link  "> Avisos </a>
                                                </li>
                                                <li aria-haspopup="true" class=" ">
                                                    <a href="#" class="nav-link  "> Dúvidas? </a>
                                                </li>
                                            </ul>
                                        </li>


                                        <!-- 
                                        INSCRICOES
                                        ========================= -->
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown">
                                            <a href="javascript:;">
                                                <i class="fa fa-id-card"></i> Inscrições
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-left">
                                                <li aria-haspopup="true" class=" ">
                                                    <a href="#" class="nav-link  "> Realizar Inscrição </a>
                                                </li>
                                                <li aria-haspopup="true" class=" ">
                                                    <a href="#" class="nav-link  "> Consultar Inscrição </a>
                                                </li> 
                                                <li aria-haspopup="true" class=" ">
                                                    <a href="#" class="nav-link  "> Dúvidas? </a>
                                                </li>
                                            </ul>
                                        </li>



                                        <!-- 
                                        MEUS ANIMAIS
                                        ========================= -->
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown">
                                            <a href="javascript:;">
                                                <i class="fa fa-paw"></i> Meus Animais
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-left">
                                                <li aria-haspopup="true" class=" ">
                                                    <a href="#" class="nav-link  "> Realizar Inscrição </a>
                                                </li>
                                                <li aria-haspopup="true" class=" ">
                                                    <a href="#" class="nav-link  "> Consultar Inscrição </a>
                                                </li> 
                                                <li aria-haspopup="true" class=" ">
                                                    <a href="#" class="nav-link  "> Dúvidas? </a>
                                                </li>
                                            </ul>
                                        </li>



                                        <li aria-haspopup="true" class="menu-dropdown mega-menu-dropdown  mega-menu-full">
                                            <a href="javascript:;">
                                                <i class="fa fa-briefcase"></i> Empresas
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="dropdown-menu" style="min-width: 200px">
                                                <li>
                                                    <div class="mega-menu-content">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <ul class="mega-menu-submenu">
                                                                    <li>
                                                                        <h3>Components 1</h3>
                                                                    </li>
                                                                    <li>
                                                                        <a href="components_date_time_pickers.html"> Date & Time Pickers </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="components_color_pickers.html"> Color Pickers </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="components_select2.html"> Select2 Dropdowns </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="components_bootstrap_multiselect_dropdown.html"> Bootstrap Multiselect Dropdowns </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="components_bootstrap_select.html"> Bootstrap Select </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="components_multi_select.html"> Bootstrap Multiple Select </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <ul class="mega-menu-submenu">
                                                                    <li>
                                                                        <h3>Components 2</h3>
                                                                    </li>
                                                                    <li>
                                                                        <a href="components_bootstrap_select_splitter.html"> Select Splitter </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="components_clipboard.html"> Clipboard </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="components_typeahead.html"> Typeahead Autocomplete </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="components_bootstrap_tagsinput.html"> Bootstrap Tagsinput </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="components_bootstrap_switch.html"> Bootstrap Switch </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="components_bootstrap_maxlength.html"> Bootstrap Maxlength </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <ul class="mega-menu-submenu">
                                                                    <li>
                                                                        <h3>Components 3</h3>
                                                                    </li>
                                                                    <li>
                                                                        <a href="components_bootstrap_fileinput.html"> Bootstrap File Input </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="components_bootstrap_touchspin.html"> Bootstrap Touchspin </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="components_form_tools.html"> Form Widgets & Tools </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="components_context_menu.html"> Context Menu </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="components_editors.html"> Markdown & WYSIWYG Editors </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <ul class="mega-menu-submenu">
                                                                    <li>
                                                                        <h3>Components 4</h3>
                                                                    </li>
                                                                    <li>
                                                                        <a href="components_code_editors.html"> Code Editors </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="components_ion_sliders.html"> Ion Range Sliders </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="components_noui_sliders.html"> NoUI Range Sliders </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="components_knob_dials.html"> Knob Circle Dials </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>


                                        <!-- 
                                        FINANCEIRO
                                        ========================= -->
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown">
                                            <a href="javascript:;">
                                                <i class="fa fa-usd"></i> Financeiro
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-left">
                                                <li aria-haspopup="true" class=" ">
                                                    <a href="#" class="nav-link  "> Extrato Online </a>
                                                </li>
                                                <li aria-haspopup="true" class=" ">
                                                    <a href="#" class="nav-link  ">  2ª via de boleto </a>
                                                </li> 
                                                <li aria-haspopup="true" class=" ">
                                                    <a href="#" class="nav-link  "> Dúvidas? </a>
                                                </li>
                                            </ul>
                                        </li>


                                        <!-- 
                                        DUVIDAS
                                        ========================= -->
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown">
                                            <a href="javascript:;">
                                                <i class="fa fa-question"></i> Dúvidas
                                                <span class="arrow"></span>
                                            </a>
                                        </li>


                                        <!-- 
                                        SUPORTE TECNICO
                                        ========================= -->
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown">
                                            <a href="javascript:;">
                                                <i class="fa fa-cogs"></i> Suporte Técnico
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-left">
                                                <li aria-haspopup="true" class=" ">
                                                    <a href="#" class="nav-link  "> Abrir ticket de suporte </a>
                                                </li>
                                                <li aria-haspopup="true" class=" ">
                                                    <a href="#" class="nav-link  ">  Consultar tickets abertos</a>
                                                </li> 
                                            </ul>
                                        </li>


                                        <!-- 
                                        Chat
                                        ========================= -->
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown">
                                            <a href="javascript:;">
                                                <i class="fa fa-comments"></i> Chat
                                                <span class="arrow"></span>
                                            </a>
                                        </li>

