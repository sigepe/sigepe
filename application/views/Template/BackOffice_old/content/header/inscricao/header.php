<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700" rel="stylesheet">


    <div class="container inscricao-cabecalho">

        <!-- BEGIN LOGO -->
        <div class="page-logo col-sm-4">
            <a href="{base_url}dashboard">
                <img src="http://fhbr.com.br/assets/frontend/layout/img/logos/logo-header.png" alt="logo" class="logo-default" style="margin-top: 6px; width: 250px;">
            </a>
        </div>
        <!-- END LOGO -->

        <div class="col-sm-8 nome-evento">
          <small class="badge badge-primary bold">Inscrição para o evento</small>
          <h3><?php echo $DatasetEvento[0]->eve_nome; ?></h3>
        </div>

    </div>

    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <hr class="separador-inscricao">
          <hr class="separador-inscricao">
          <hr class="separador-inscricao">
          <hr class="separador-inscricao">
          <hr class="separador-inscricao">
        </div>
      </div>
    </div>
