<?php if(isset($inputs)): ?>

<!-- Slug
============================================= -->
<?php
	$contadorSlug = 0;
	if(isset($inputs)):
		foreach ($inputs as $input) {
			if($input['input-type']=='slug'){
				$contadorSlug ++;
			}
		}
	endif;
?>

<script type="text/javascript">
var slugJson = [ 

<?php
		
	$contadorForeachSlug = 1;
	foreach ($inputs as $input):
		if( $input['input-type']=='slug' ):

			echo "{";
				echo '"from":';
					echo '"' . $input['input-from'] . '"';
					echo ",";

				echo '"to":';
					echo '"' . $input['input-id'] . '"';

			echo "}";

			echo ($contadorForeachSlug==$contadorSlug) ? '' : ',';

			$contadorForeachSlug++;

		endif;
	endforeach;

?>
];
</script>


<!-- Validation
============================================= -->
<!--  Validation Form 1) Refatorar - levar script para dentro de documento .js so chamar na pagina a variavel em json. ============================================= -->
<?php
	$contadorValidation = 0;
	foreach ($inputs as $input) {
		if(isset($input['validation-frontend']) && !is_null($input['validation-frontend'])){
			$contadorValidation++;
		}
	}
?>


<script type="text/javascript">

	var rulesRun = {

		<?php
			$contadorForeachValidation = 1;
			foreach ($inputs as $input):
				if( isset($input['validation-frontend']) && !is_null($input['validation-frontend'])):
					$required = $input['validation-frontend'][0];
		?>	
        <?php echo $input['input-id'] ?>: {
            required: <?php echo (!is_null($required) && !empty($required)) ? 'true' : 'false'; ?>
        }

        <?php echo ($contadorValidation==$contadorForeachValidation) ? '' : ','; ?>

	    <?php
	    		$contadorForeachValidation++;
	    		endif;
	    	endforeach;
	    ?>

	};

</script>

<?php endif; ?>


