

    <!-- BEGIN PROFILE CONTENT -->
    <div class="profile-content">
        <div class="row">
            <div class="col-md-12">

                <div class="portlet light ">
                    <div class="portlet-body">


                        <div class="tabbable-line">
                            <ul class="nav nav-tabs ">
                                <li class="active">
                                    <a href="#tab_15_1" data-toggle="tab" aria-expanded="true"> Informações Gerais </a>
                                </li>
                                <li class="">
                                    <a href="#tab_15_2" data-toggle="tab" aria-expanded="false"> Vínculos </a>
                                </li>
                                <li class="">
                                    <a href="#tab_15_3" data-toggle="tab" aria-expanded="false"> Registro </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_15_1">

                                    <div class="row">
                                        <div class="col-sm-6">

                                            <form role="form" action="#" class="form-view" id="form-meu-perfil">

                                                <div class="form-group fg-50 fg-left fg-view">    
                                                    <label class="control-label">ID Pessoa</label>
                                                    <div class="form-control-static"> #<?php echo $PessoaId ?> </div>
                                                </div>

                                                <div class="form-group fg-50 fg-right fg-view">
                                                    <label class="control-label">ID Pessoa Física</label>
                                                    <div class="form-control-static"> #<?php echo $PessoaFisicaId; ?> </div>
                                                </div>

                                                <div class="form-group fg-50 fg-left fg-view">    
                                                    <label class="control-label">Nome Completo</label>
                                                    <div class="form-control-static"> <?php echo $NomeCompleto ?> </div>
                                                    <input type="text" placeholder="<?php echo $NomeCompleto ?>" class="form-control" />
                                                </div>

                                                <div class="form-group fg-50 fg-right fg-view">
                                                    <label class="control-label">CPF</label>
                                                    <div class="form-control-static"> <?php echo $this->my_pessoa->InserirPontuacaoCpfCnpj($CPF); ?> </div>
                                                </div>

                                                <div class="form-group fg-50 fg-left fg-view">
                                                    <label class="control-label">Data de Nascimento</label>
                                                    <div class="form-control-static">
                                                        <?php echo $this->my_data->ConverterData($DataDeNascimento, 'ISO', 'PT-BR'); ?>
                                                        ( <?php echo DateDifferences( date("Y-m-d"), $DataDeNascimento, 'y' ) ?> anos )
                                                    </div>
                                                </div>


                                                <div class="form-group fg-50 fg-right fg-view">
                                                    <label class="control-label">Gênero</label>
                                                    <div class="form-control-static"><?php echo $this->model_crud->get_rowSpecific('tb_pessoa_fisica_genero', 'pfg_id', $Genero, 1, 'pfg_genero' ); ?> </div>
                                                </div>


                                                <hr id="meu-perfil-divisao">


                                                <div class="form-group fg-50 fg-left fg-edit">
                                                    <label class="control-label">Apelido</label>
                                                    <div class="form-control-static" id="fcs-apelido"> <?php echo (!is_null($Apelido)) ? $Apelido : 'N/I'; ?> </div>
                                                    <input type="text" placeholder="Como quer ser chamado?" value="<?php echo $Apelido ?>" name="apelido" class="form-control" />
                                                </div>

                                                <div class="form-group fg-50 fg-right fg-edit">
                                                    <label class="control-label">
                                                        Estado Civil 
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="form-control-static" id="fcs-estado-civil"> <?php echo (!is_null($EstadoCivil)) ? $EstadoCivil : 'N/I'; ?> </div>
                                                    <select class="form-control" name="estado-civil">
                                                        <option value="">- Selecione uma Opção</option>
                                                        <?php foreach ($DatasetEstadoCivil as $key => $value): ?>
                                                        <option value="<?php echo $value->pfe_id; ?>"  <?php echo ($value->pfe_id == $EstadoCivilId) ? 'selected' : ''; ?> ><?php echo $value->pfe_estado_civil; ?></option>
                                                       <?php endforeach; ?>
                                                    </select>
                                                </div>




                                                <div class="form-group fg-50 fg-left fg-edit">
                                                    <label class="control-label">
                                                        Nacionalidade 
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="form-control-static" id="fcs-nacionalidade"> <?php echo (!is_null($Nacionalidade)) ? $Nacionalidade : 'N/I'; ?> </div>
                                                    <select class="form-control" name="nacionalidade">
                                                        <option value="">- Selecione uma Opção</option>
                                                        <?php foreach ($DatasetNacionalidade as $key => $value): ?>
                                                        <option value="<?php echo $value->pai_id; ?>" <?php echo ($value->pai_id == $NacionalidadeId) ? 'selected' : ''; ?> ><?php echo $value->pai_pais; ?></option>
                                                       <?php endforeach; ?>
                                                    </select>
                                                </div>

                                                <div class="form-group fg-50 fg-right fg-edit">
                                                    <label class="control-label">
                                                        Naturalidade
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="form-control-static" id="fcs-naturalidade"> <?php echo (!is_null($Naturalidade)) ? $Naturalidade : 'N/I'; ?> </div>
                                                    <input type="text" placeholder="Onde você nasceu?" value="<?php echo $Naturalidade; ?>" name="naturalidade" class="form-control" />

                                                </div>




                                                <div class="form-group fg-50 fg-left fg-edit">
                                                    <label class="control-label">
                                                        Tipo Sanguíneo 
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="form-control-static" id="fcs-tipo-sanguineo"> <?php echo (!is_null($TipoSanguineo)) ? $TipoSanguineo : 'N/I'; ?> </div>
                                                    <select class="form-control" name="tipo-sanguineo">
                                                        <option value="">- Selecione uma Opção</option>
                                                        <?php foreach ($DatasetTipoSanguineo as $key => $value): ?>
                                                        <option value="<?php echo $value->pft_id; ?>" <?php echo ($value->pft_id == $TipoSanguineoId) ? 'selected' : ''; ?>><?php echo $value->pft_tipo_sanguineo ?></option>
                                                       <?php endforeach; ?>
                                                    </select>
                                                </div>

                                                <div class="form-group fg-50 fg-right fg-edit">
                                                    <label class="control-label">
                                                        Escolaridade
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="form-control-static" id="fcs-escolaridade"> <?php echo (!is_null($Escolaridade)) ? $Escolaridade : 'N/I'; ?> </div>
                                                    <select class="form-control" name="escolaridade">
                                                        <option value="">- Selecione uma Opção</option>
                                                        <?php foreach ($DatasetEscolaridade as $key => $value): ?>
                                                        <option value="<?php echo $value->pfe_id; ?>" <?php echo ($value->pfe_id == $EscolaridadeId) ? 'selected' : ''; ?> ><?php echo $value->pfe_escolaridade; ?></option>
                                                       <?php endforeach; ?>
                                                    </select>
                                                </div>



                                                <div class="form-group fg-50 fg-left fg-edit">
                                                    <label class="control-label">Nome do Pai</label>
                                                    <div class="form-control-static" id="fcs-nome-pai"> <?php echo (!is_null($NomeDoPai)) ? $NomeDoPai : 'N/I'; ?> </div>
                                                    <input type="text" placeholder="Informe o nome do pai" value="<?php echo $NomeDoPai ?>" name="nome-pai" class="form-control" />
                                                </div>

                                                <div class="form-group fg-50 fg-right fg-edit">
                                                    <label class="control-label">Nome da Mãe</label>
                                                    <div class="form-control-static" id="fcs-nome-mae"> <?php echo (!is_null($NomeDaMae)) ? $NomeDaMae : 'N/I'; ?> </div>
                                                    <input type="text" placeholder="Informe o nome da mãe" value="<?php echo $NomeDaMae ?>" name="nome-mae" class="form-control" />
                                                </div>




                                                <div class="form-actions" style="text-align: center;">
                                                    <input type="submit" class="btn blue" style="padding: 10px 40px;display: block;" value="Salvar">
                                                </div>


                                            </form>

                                        </div>
                                        <div class="col-sm-6">2</div>
                                    </div>


                                </div>
                            </div>
                        </div>



                    </div>
                </div>






            </div>
        </div>
    </div>
    <!-- END PROFILE CONTENT -->



    </div>  
    <!-- div aberta em sidebar.php ( /.col-sm-12 ) -->
</div>
<!-- div aberta em sidebar.php ( /.row ) -->