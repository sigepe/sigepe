    <style type="text/css">

        .sigepe .tiles .tile{
            overflow: initial !important;
        }
        .tiles .tile{
            border: none !important;
        }
        .tiles .tile .tile-body{
            margin-bottom: 0 !important;
        }
/*
       .sigepe .tiles .tile:after{
            content: "1";
            background: red;
            width: 100%;
            height: 30px;   
        }
*/

        .tiles-empresas .tile{
            margin-bottom: 60px !important;
        }
        .sigepe .tiles .tile{
            width: 100% !important;
            margin-bottom: 0px;
        }
        .sigepe .tiles .tile .tile-object>.name{
            position: relative !important;
            margin-left: 0px !important;
            margin-right: 0px !important;
            text-align: center;
        }
        .sigepe .tiles{
            margin-right: 0px !important;
        }
        .btn.blue:not(.btn-outline) {
            color: #FFF;
            background-color: #3598dc;
            border-color: #3598dc;
            width: 100%;
            white-space: normal;
        }
    </style>


    <!-- BEGIN PROFILE CONTENT -->
    <div class="profile-content">
        <div class="row">
            <div class="col-md-12">

                <div class="portlet light ">
                    <div class="portlet-title tabbable-line">
                        <div class="caption caption-md">
                            <i class="icon-globe theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase">PERFIL ATLETA</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <!-- 
                        <div class="alert alert-success" style="margin-bottom: 0px;">
                        <strong>Olá,</strong> por enquanto não temos nenhum recado para você. </div>
                        -->                        
                        <h2 style="margin: 0px;font-weight: bold;"><?php echo ucwords($this->session->userdata('PessoaPrimeiroNome')); ?>,</h2>
                        <div class="note note-info" style="margin-top: 17px;">
                            <p style="font-size: 18px;">
                                sua conta não tem o perfil atleta ativado. <br> <small> Tornando um atleta você poderá ser inscrito em eventos, participar do ranking e outros benefícios.</small><br>
                            </p>
                        </div>

                        <div class="alert alert-warning"> <strong>Atenção!</strong> Ativar o perfil atleta para sua conta pode gerar custos de registro. </div>


                        <hr>

                        <button type="button" class="btn btn-circle btn-primary">Ativar perfil atleta</button>


                    </div>
                </div>



            </div>
        </div>
    </div>
    <!-- END PROFILE CONTENT -->



    </div>  
    <!-- div aberta em sidebar.php ( /.col-sm-12 ) -->
</div>
<!-- div aberta em sidebar.php ( /.row ) -->