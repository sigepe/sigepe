

    <!-- BEGIN PROFILE CONTENT -->
    <div class="profile-content" id="perfil-telefone">

        <div class="row">

            <div class="col-md-12">
            
                <div class="portlet light porlet-main">

                    <div class="portlet-title">
                        
                        <div class="caption font-blue-madison">
                            <span class="caption-subject bold uppercase"> TELEFONE</span>
                            <span class="caption-helper">Relação de telefones associados a sua conta.</span>
                        </div>
                        
                        <div class="actions">
                            <a class="btn btn-circle btn-edit btn-sm btn-cadastrar-telefone" href="javascript:;">
                                <i class="fa fa-plus"></i>
                                Cadastrar Telefone
                            </a>
                        </div>

                    </div>

                    <div class="portlet-body porlet-body-main">


                        <?php if ( empty($DatasetTelefone) || is_null($DatasetTelefone) ): ?>
                        <div class="alert alert-danger" id="aviso-nenhum-telefone">
                            <strong>Atenção,</strong> 
                            não existe nenhum telefone para você. Mantenha seus dados atualizados. <br>
                            <a href="#" title="" class="">Clique aqui para cadastrar um telefone.</a>
                        </div>
                        <?php endif; ?>


                        <div id="table-scrollable-telefone" class="table-scrollable table-scrollable-borderless <?php echo ( empty($DatasetTelefone) || is_null($DatasetTelefone) ) ? 'display-none' : ''; ?>">

                            <table class="table table-hover table-light" id="table-telefone">
                            
                                <thead>
                                    <tr class="uppercase">
                                        <th class="text-center">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                            Número
                                        </th>
                                        <th class="text-center"> Ação </th>
                                    </tr>
                                </thead>

                                <tbody>

                                    <?php foreach ($DatasetTelefone as $key => $value): ?>
                                    <tr id="table-row-<?php echo $value->tel_id; ?>" data-id="<?php echo $value->tel_id; ?>">
                                        <td class="text-center">
                                             
                                            <?php if(!is_null($value->tel_principal)): ?>
                                             <a href="javascript:;"
                                                class="tooltips badge badge-warning bold flag-telefone-principal"
                                                title=""
                                                data-original-title="Telefone Principal. Preferencialmente vamos utilizar esse número para entrar em contato com você.">
                                                P
                                            </a>
                                            <?php endif; ?>

                                             <span class="bold"><?php echo $this->model_crud->get_rowSpecific('tb_contato', 'con_id', $value->fk_con_id, 1, 'con_contato'); ?>:</span>
                                            <?php echo $value->tel_numero; ?>

                                        </td>
                                        <td class="text-center">
                                            <span class="btn btn-circle btn-sm btn-success btn-gerenciar-conjunto popovers" data-container="body" onclick=" " data-html="true" data-trigger="hover" data-placement="left" data-content="
                                                    <small> 

                                                        <b>Cadastrado em:</b><br>
                                                        <?php echo $this->my_data->datetime($value->criado, 'datetime_untilMinuts'); ?>

                                                        <hr style='margin: 7px 0;'>

                                                        <b>Por:</b><br>
                                                        <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_aut_id, 1, 'pes_nome_razao_social'); ?> <br>

                                                    </small>
                                                " data-original-title="Detalhes">
                                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                Detalhes
                                            </span>

                                            <?php if(is_null($value->tel_principal)): ?>
                                            <a href="javascript:;"
                                                class="btn btn-circle btn-sm btn-warning btn-telefone-principal tooltips"
                                                data-original-title="Transformar esse telefone como principal."
                                            >
                                                
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                                Telefone Principal
                                            </a>
                                            <?php endif; ?>

                                            <?php if(is_null($value->tel_principal)): ?>
                                            <a href="javascript:;" class="btn btn-circle btn-sm btn-danger btn-deletar-telefone">
                                                <i class="fa fa-close" aria-hidden="true"></i>
                                                Deletar Telefone
                                            </a>
                                            <?php endif; ?>

                                        </td>
                                    </tr>
                                    <?php endforeach; ?>

                                </tbody>

                            </table>

                        </div>

<!--                         <hr style="margin-top: 12px;"> -->

                        <!-- Formulario -->
                        <form action="#" role="form" id="form-telefone" class="form-horizontal form-view" style="display: none;">

                            <div class="form-body">

                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-yellow-crusta">
                                            <i class="icon-share font-yellow-crusta"></i>
                                            <span class="caption-subject bold uppercase"> Formulário</span>
                                            <span class="caption-helper">Preencha os campos abaixo para cadastrar um telefone a sua conta.</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">

                                        <div class="alert alert-danger display-hide">
                                            <button class="close" data-close="alert"></button>
                                            Preencha todos os campos do formulário.
                                        </div>
                                        
                                        <div class="alert alert-success display-hide">
                                            <button class="close" data-close="alert"></button>
                                            Formulário validado!
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Telefone
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-phone"></i>
                                                    </span>
                                                    <input type="text" class="form-control mask-telefone" name="telefone" placeholder="Telefone"> </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Tipo do Telefone
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <select class="form-control" name="tipo">
                                                    <option value="">- Selecione uma Opção -</option>
                                                    <?php foreach ($DatasetTipoTelefone as $key => $value): ?>
                                                    <option value="<?php echo $value->con_id; ?>"><?php echo $value->con_contato; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- /.portlet-body -->

                                </div>
                                <!-- /.portlet -->
 
                            </div>
                            <!-- /form-body -->

                            <div class="form-actions text-center">
                                <a class="btn blue btn-submit">
                                    <i class="fa fa-save" aria-hidden="true"></i>
                                    Cadastrar Telefone
                                </a>
                            </div>

                        </form>
                        <!-- /form-->

                    </div>
                    <!-- /portlet-body -->

                </div>
                <!-- /.portlet -->

            </div>
            <!-- /.col-md-12 -->

        </div>
        <!-- /.row -->

    </div>
    <!-- END PROFILE CONTENT -->



    </div>  
    <!-- div aberta em sidebar.php ( /.col-sm-12 ) -->

</div>
<!-- div aberta em sidebar.php ( /.row ) -->