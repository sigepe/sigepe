    <style type="text/css">

        .sigepe .tiles .tile{
            overflow: initial !important;
        }
        .tiles .tile{
            border: none !important;
        }
        .tiles .tile .tile-body{
            margin-bottom: 0 !important;
        }
/*
       .sigepe .tiles .tile:after{
            content: "1";
            background: red;
            width: 100%;
            height: 30px;   
        }
*/

        .tiles-empresas .tile{
            margin-bottom: 60px !important;
        }
        .sigepe .tiles .tile{
            width: 100% !important;
            margin-bottom: 0px;
        }
        .sigepe .tiles .tile .tile-object>.name{
            position: relative !important;
            margin-left: 0px !important;
            margin-right: 0px !important;
            text-align: center;
        }
        .sigepe .tiles{
            margin-right: 0px !important;
        }
        .btn.blue:not(.btn-outline) {
            color: #FFF;
            background-color: #3598dc;
            border-color: #3598dc;
            width: 100%;
            white-space: normal;
        }
    </style>


    <!-- BEGIN PROFILE CONTENT -->
    <div class="profile-content">
        <div class="row">
            <div class="col-md-12">

                <div class="portlet light ">
                    <div class="portlet-title tabbable-line">
                        <div class="caption caption-md">
                            <i class="icon-globe theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase">RECADOS</span>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <div class="alert alert-success" style="margin-bottom: 0px;">
                        <strong>Olá,</strong> por enquanto não temos nenhum recado para você. </div>

                    </div>
                </div>


                <div class="portlet light ">

                    <div class="portlet-body">

                        <div class="tiles">

                            <div class="row">

                                <div class="col-sm-3">
                                    <div class="tile bg-blue link-redirect" data-redirect="FrontOffice/Perfil/MeuPerfil">
                                        <div class="tile-body">
                                            <i class="fa fa-address-card"></i>
                                        </div>
                                        <div class="tile-object">
                                            <div class="name"> Editar Perfil </div>
                                        </div>
                                    </div>

                                </div>
                                

                                <div class="col-sm-3">
                                    <div class="tile bg-blue link-redirect" data-redirect="FrontOffice/Perfil/Telefone">
                                        <div class="tile-body">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <div class="tile-object">
                                            <div class="name"> Gerenciar Telefone </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-sm-3">
                                    <div class="tile bg-blue link-redirect" data-redirect="FrontOffice/Perfil/Email">
                                        <div class="tile-body">
                                            <i class="fa fa-envelope"></i>
                                        </div>
                                        <div class="tile-object">
                                            <div class="name"> Gerenciar Email </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="tile bg-blue link-redirect" data-redirect="FrontOffice/Perfil/Endereco">
                                        <div class="tile-body">
                                            <i class="fa fa-map-marker"></i>
                                        </div>
                                        <div class="tile-object">
                                            <div class="name"> Gerenciar Endereço </div>
                                        </div>
                                    </div>
                                </div>

                                <hr style="float: left;width: 100%;">
                                
                                <div class="col-sm-3">
                                    <div class="tile bg-blue link-redirect" data-redirect="FrontOffice/Perfil/TrocarSenha">
                                        <div class="tile-body">
                                            <i class="fa fa-key"></i>
                                        </div>
                                        <div class="tile-object">
                                            <div class="name"> Trocar Senha </div>
                                        </div>
                                    </div>
                                </div>
                                

                                <div class="col-sm-3">
                                    <div class="tile bg-blue link-redirect" data-redirect="FrontOffice/Perfil/TrocarFotoPerfil">
                                        <div class="tile-body">
                                            <i class="fa fa-user-circle"></i>
                                        </div>
                                        <div class="tile-object">
                                            <div class="name"> Trocar Foto do Perfil </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-sm-3">
                                    <div class="tile bg-blue">
                                        <div class="tile-body">
                                            <i class="fa fa-comments-o"></i>
                                        </div>
                                        <div class="tile-object">
                                            <div class="name"> Chat </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="tile bg-blue link-redirect" data-redirect="FrontOffice/Perfil/Duvidas">
                                        <div class="tile-body">
                                            <i class="fa fa-info"></i>
                                        </div>
                                        <div class="tile-object">
                                            <div class="name"> Perguntas Frequentes </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <!-- /MENUS -->

                        </div>

                    </div>

                </div>
                <!-- /portlet-light -->




                <div class="portlet light ">

                    <div class="portlet-body">

                        <div class="tiles">


                            <!-- 
                            PERFIS
                            ======================================= -->
                            <?php
                            $Perfis  =   $this->session->userdata('Perfis');
                            if(!empty($Perfis)):
                            ?>
                            <div class="row tiles-perfis">


                                <div class="portlet light ">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <span class="caption-subject font-dark bold uppercase">Perfil</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body row">

                                        <?php foreach ($Perfis as $key => $value): ?>
                                        <div class="col-sm-3">
                                            <a class="tile bg-grey-cascade" href='{base_url}<?php echo ($value == '5') ? 'FrontOffice/Perfil/Atleta' : '';  ?>'>
                                                <div class="tile-body">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <div class="tile-object">
                                                    <div class="name">
                                                    <?php echo $this->model_crud->get_rowSpecific('tb_vinculo_perfil', 'vip_id', $value, 1, 'vip_papel'); ?>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <?php endforeach; ?>

                                    </div>
                                </div>
                                <!-- end portlet -->



                            </div>
                            <?php endif;  ?>
                            <!-- /PERFIS -->
                                




                            <!-- 
                            EMPRESAS
                            ======================================= -->
                            <?php
                            $Empresas  =   $this->session->userdata('Empresas');
                            if(!empty($Empresas)):
                            ?>
                            <div class="row tiles-empresas">

                                <hr />


                                <div class="portlet light ">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <span class="caption-subject font-dark bold uppercase">Empresas</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body row">

                                        <?php foreach ($Empresas as $key => $value): ?>
                                        <div class="col-sm-3">

                                            <div class="tile bg-grey-cascade popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="<?php echo $this->model_crud->get_rowSpecific('tb_vinculo_tipo', 'vit_id', $value['TipoVinculo'], 1, 'vit_tipo_vinculo'); ?>" data-original-title="Vínculo">
                                                <a href="javascript:;">
                                                    <div class="tile-body">
                                                        <i class="fa fa-briefcase"></i>
                                                    </div>
                                                    <div class="tile-object">
                                                        <div class="name"> 

                                                        <?php

                                                            if($value['TipoVinculo'] == '12' || $value['TipoVinculo'] == '16')
                                                                echo 'Confederação';

                                                            if($value['TipoVinculo'] == '13' || $value['TipoVinculo'] == '17')
                                                                echo 'Federação';

                                                            if($value['TipoVinculo'] == '14' || $value['TipoVinculo'] == '18')
                                                                echo 'Entidade Equestre';

                                                            if($value['TipoVinculo'] == '15' || $value['TipoVinculo'] == '19')
                                                                echo 'Empresa Ordinária';

                                                        ?>

                                                        </div>
                                                    </div>
                                                </a>
                                                <a href="javascript:;" class="btn blue"> <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value['IdEmpresa'], 1, 'pes_nome_razao_social') ?> </a>
                                            </div>
                                        </div>
                                        <?php endforeach; ?>

                                    </div>
                                </div>
                                <!-- end portlet -->

                            </div>
                            <?php endif; ?>
                            <!-- /EMPRESAS -->
                                




                        </div>
                        <!-- /tiles -->


                    </div>
                </div>



            </div>
        </div>
    </div>
    <!-- END PROFILE CONTENT -->



    </div>  
    <!-- div aberta em sidebar.php ( /.col-sm-12 ) -->
</div>
<!-- div aberta em sidebar.php ( /.row ) -->