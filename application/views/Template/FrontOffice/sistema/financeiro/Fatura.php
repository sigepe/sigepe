<?php

    // Dados da Fatura
    $FinanceiroId                      =  $DadosFatura['fin_id']; 
   
    $FaturaTransacao                   =  $DadosFatura['fin_transacao']; 

    $FaturaTransacaoPagarme            =  $DadosFatura['fin_transacao_pagarme']; 
    $FaturaStatus                      =  $DadosFatura['fk_sta_id']; 
    $FaturaResponsavelFinanceiroId     =  $DadosFatura['fk_pes_id']; 
    $FaturaTipoPagamento               =  $DadosFatura['fk_fip_id']; 
    $FaturaNatureza                    =  $DadosFatura['fk_fin_id']; 
    $FaturaValorBruto                  =  $DadosFatura['fin_valor_bruto']; 
    $FaturaValorDesconto               =  $DadosFatura['fin_valor_desconto']; 
    $FaturaValorLiquido                =  $DadosFatura['fin_valor_liquido']; 
    $FaturaBoletoUrl                   =  $DadosFatura['fin_boleto']; 

?>


<div class="page-content-inner">
    <div class="invoice">
        
        
        <!--
        CABEÇALHO SUPERIOR
        **************************************-->
        <div class="row invoice-logo cabecalho-superior">
            <div class="col-xs-6">
                <span class="badge badge-warning">Fatura nº</span>
                <span class="fatura-numero font-yellow-crusta">#<?php echo $FaturaTransacao ?></span>
                <small class="fatura-data-gerada">
                    Fatura gerada em 
                    <?php echo date("d", strtotime($DadosFatura['criado'])); ?>
                    <?php echo date("M", strtotime($DadosFatura['criado'])); ?>
                    <?php echo date("Y", strtotime($DadosFatura['criado'])); ?>
                </small>
            </div>
            <div class="col-xs-6 invoice-logo-space">
                <img src="http://fhbr.com.br/assets/frontend/layout/img/logos/logo-header.png" class="img-responsive" alt="">
            </div>
        </div>

        <hr>

        <!--
        CABEÇALHO INFERIOR
        **************************************-->
        <div class="row cabecalho-inferior">

            <div class="col-xs-4">
                <h3>Natureza da Fatura:</h3>
                <ul class="list-unstyled">

                    <?php
                        if($FaturaNatureza == '1'):
                            $PessoaId                   =   $this->model_crud->get_rowSpecific('tb_registro', 'fk_fin_id', $FinanceiroId, 1, 'fk_pes_id'); // ID DO ATLETA ( PESSOA )
                            $ModalidadeId               =   $this->model_crud->get_rowSpecific('tb_registro', 'fk_fin_id', $FinanceiroId, 1, 'fk_mod_id'); // 
                            $TipoRegistroId             =   $this->model_crud->get_rowSpecific('tb_registro', 'fk_fin_id', $FinanceiroId, 1, 'fk_tip_id'); // 
                            $PessoaFisicaId             =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'fk_pes_id', $PessoaId, 1, 'pef_id');
                            $PessoaFisicaAtletaId       =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'fk_pef_id', $PessoaFisicaId, 1, 'pfa_id');

                    ?>
                    <li><b><i class="fa fa-caret-right"></i>Registro de Atleta</b> </li>
                    <li>ID do Atleta: <?php echo $PessoaFisicaAtletaId; ?></li>
                    <li>Atleta: <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $PessoaId, 1, 'pes_nome_razao_social'); ?></li>
                    <li> Modalidade: <?php echo $this->model_crud->get_rowSpecific('tb_evento_modalidade', 'evm_id', $ModalidadeId, 1, 'evm_modalidade'); ?></li>
                    <li> Tipo do Registro: <?php echo $this->model_crud->get_rowSpecific('tb_registro_tipo', 'ret_id', $TipoRegistroId, 1, 'ret_tipo'); ?> </li>
                    <?php endif; ?>


                    <?php
                        if($FaturaNatureza == '2'):
                            $AnimalId                   =   $this->model_crud->get_rowSpecific('tb_registro', 'fk_fin_id', $FinanceiroId, 1, 'fk_ani_id'); // ID DO ATLETA ( PESSOA )
                            $ModalidadeId               =   $this->model_crud->get_rowSpecific('tb_registro', 'fk_fin_id', $FinanceiroId, 1, 'fk_mod_id'); // 
                            $TipoRegistroId             =   $this->model_crud->get_rowSpecific('tb_registro', 'fk_fin_id', $FinanceiroId, 1, 'fk_tip_id'); // 

                    ?>
                    <li><b><i class="fa fa-caret-right"></i>Registro de Atleta</b> </li>
                    <li>ID do Animal: <?php echo $AnimalId; ?></li>
                    <li>Animal: <?php echo $this->model_crud->get_rowSpecific('tb_animal', 'ani_id', $AnimalId, 1, 'ani_nome_completo'); ?></li>
                    <li> Modalidade: <?php echo $this->model_crud->get_rowSpecific('tb_evento_modalidade', 'evm_id', $ModalidadeId, 1, 'evm_modalidade'); ?></li>
                    <li> Tipo do Registro: <?php echo $this->model_crud->get_rowSpecific('tb_registro_tipo', 'ret_id', $TipoRegistroId, 1, 'ret_tipo'); ?> </li>
                    <?php endif; ?>


                </ul>                
            </div> 
            
            <div class="col-xs-4">
                <h3>Responsavel Financeiro:</h3>
                <ul class="list-unstyled">
                    <li class="bold"><i class="fa fa-user"></i> <?php echo $DadosResponsavelFinanceiro['pes_nome_razao_social'];?> </li>
                    <li> <?php echo $DadosResponsavelFinanceiro['ema_email'];?> </li>
                    <li> CEP: <?php echo $DadosResponsavelFinanceiro['end_cep'];?> - <?php echo $DadosResponsavelFinanceiro['estado-sigla'];?> / <?php echo $DadosResponsavelFinanceiro['cidade'];?> </li>
                    <li> <?php echo $DadosResponsavelFinanceiro['end_logradouro'];?> - nº <?php echo $DadosResponsavelFinanceiro['end_numero'];?>  </li>
                    <?php if(!is_null($DadosResponsavelFinanceiro['end_complemento'])): ?><li> <?php echo $DadosResponsavelFinanceiro['end_complemento'];?> </li><?php endif; ?>
                </ul>
            </div>
            
            <div class="col-xs-4 invoice-payment">
                <h3>Detalhes do Pagamento:</h3>
                <ul class="list-unstyled">
                    <li>
                        <?php if($FaturaTipoPagamento == '1'): ?><b><i class="fa fa-barcode"></i> BOLETO BANCÁRIO</b><?php endif; ?>
                        <?php if($FaturaTipoPagamento == '2'): ?><b><i class="fa fa-credit-card" aria-hidden="true"></i> CARTÃO DE CRÉDITO</b><?php endif; ?>
                    </li>
                    <li>
                        
                        <?php if($FaturaStatus == '100'): ?>
                        <span class="btn btn-circle btn-xs  btn btn-default sbold default label-vinculo-atual" data-original-title="" style="width: 100%; padding: 6px; margin-bottom: 7px; margin-top: 6px;">
                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                            Aguardando Pagamento
                        </span>
                        <?php endif; ?>
                        
                        <?php if($FaturaStatus == '101'): ?>
                        <span class="btn btn-circle btn-xs  btn  sbold btn-success label-vinculo-atual" data-original-title="" style="width: 100%; padding: 6px; margin-bottom: 7px; margin-top: 6px;">
                            <i class="fa fa-check" aria-hidden="true"></i>
                            Pago
                        </span>
                        <?php endif; ?>

                    </li>
                    <li>
                        Transação Gateway: <?php echo (!is_null($FaturaTransacaoPagarme)) ? "#" . $FaturaTransacaoPagarme : 'N/I'; ?>
                    </li>
                </ul>
            </div>

        </div>
        <!-- /cabecalho -->
        
    
        <!--
        TABELA ITENS DA FATURA
        **********************************************-->
        <div id="tabela-item">
            <h3>Itens da Fatura</h3>
            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th> ID </th>
                                <th style="width: 20%"> Item </th>
                                <th style="width: 40%"> Descrição </th>
                                <th class="text-center"> Quantidade </th>
                                <th class="text-center"> Preço Unitário </th>
                                <th class="text-center"> Total </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($DadosItem as $key => $value): ?>
                            <tr>
                                <td> <?php echo $value->fii_id;?> </td>
                                <td> <?php echo $value->fii_item;?> </td>
                                <td> <?php echo $value->fii_descricao;?> </td>
                                <td class="text-center"> <?php echo $value->fii_quantidade;?> </td>
                                <td class="text-center"> R$ <?php echo $this->my_moeda->InserirPontuacao($value->fii_valor_unitario);?> </td>
                                <td class="text-center"> R$ <?php echo $this->my_moeda->InserirPontuacao($value->fii_valor_total);?> </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /#tabela-item -->


        <!--
        TABELA DESCONTO
        **********************************************-->
        <?php if(!empty($DadosDesconto)): ?>
        <div id="tabela-desconto">
            <h3>Descontos</h3>
            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th style="width: 20%"> Desconto </th>
                                <th style="width: 40%"> Descrição </th>
                                <th class="text-center"> Valor </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($DadosDesconto as $key => $value): ?>
                            <tr>
                                <td> <?php echo $value->fid_id; ?> </td>
                                <td style="width:35%;"> <?php echo $value->fid_desconto; ?> </td>
                                <td style="width:40%;"> <?php echo $value->fid_descricao; ?> </td>
                                <td class="text-center"> R$ <?php echo $this->my_moeda->InserirPontuacao($value->fid_valor); ?> </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>    
        </div>
        <?php endif; ?>
        <!-- /#tabela-desconto -->
        

        <!--
        RODAPE
        **********************************************-->
        <div class="row" id="rodape">
            <div class="col-xs-6">
                <div class="well">


                    <div class="company-address">
                        <span class="bold uppercase">FHBr - Federação Hípica de Brasília</span>
                        <br/> SHIP Sul Lote nº 08
                        <br/> Brasília - DF - CEP 70610-900
                        <br/>

                        <span class="bold">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                        </span>
                        +55 (61) 3245-5870
                        <br/>

                        <span class="bold">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </span>
                        financeiro@fhbr.com.br
                        <br/>

                        <span class="bold">
                            <i class="fa fa-globe" aria-hidden="true"></i>
                        </span>
                         www.fhbr.com.br/sigepe
                    </div>

                </div>
            </div>
            <div class="col-xs-6 invoice-block">
                <ul class="list-unstyled amounts">
                    <li>
                        <strong>Subtotal:</strong>
                        R$ <?php echo $this->my_moeda->InserirPontuacao($FaturaValorBruto); ?>
                    </li>
                    <?php if(!empty($DadosDesconto)): ?>
                    <li>
                        <strong>Desconto(s):</strong>
                        R$ <?php echo $this->my_moeda->InserirPontuacao($FaturaValorDesconto); ?>
                    </li>
                    <?php endif; ?>
                    <li>
                        <strong>Total:</strong>
                        R$ <?php echo $this->my_moeda->InserirPontuacao($FaturaValorLiquido); ?>
                    </li>
                </ul>
                <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();"> Imprimir Fatura
                    <i class="fa fa-print"></i>
                </a>

                <?php if($FaturaTipoPagamento == '1' && $FaturaStatus == '100'): ?>
                <a class="btn btn-lg green btn-segunda-via-boleto hidden-print margin-bottom-5" href="<?php echo $FaturaBoletoUrl; ?>" >
                    <i class="fa fa-barcode" aria-hidden="true"></i>
                    Imprimir 2ª Via Boleto
                </a>
                <?php endif; ?>


            </div>
        </div>


    </div>
    <!--/.invoice-->

</div>
<!--/.page-content-inner-->

