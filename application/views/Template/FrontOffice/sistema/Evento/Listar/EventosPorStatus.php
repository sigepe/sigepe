<style type="text/css">
.label-primary {
/*white-space: initial;*/
}

.nav-tabs {
    border-bottom: 2px solid #066;
}

.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
    background-color: #066;
    border: 1px solid #066;
    color: white;
    border-radius: 13px 13px 0 0 !important;    
    font-weight: bold;
}
.nav-pills, .nav-tabs{
    margin-bottom: 0px;
}
.tab-content>.active{
    background: white;
}
.nav>li>a:focus, .nav>li>a,
.nav>li>a:focus, .nav>li>a:hover{
    border: none;
}
.nav>li>a:focus, .nav>li>a:hover{
    background-color: transparent;
}
.nav>li>a:focus, .nav>li>a, .nav>li>a:focus, .nav>li>a:hover{
    color: #666;
}
.table-bordered, .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
    border: none;
}
.nav-tabs>li{
    margin-right: 20px;
}
</style>







    <div class="tabbable tabbable-tabdrop">
        <ul class="nav nav-tabs">
            <li class="dropdown pull-right tabdrop">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                   <i class="fa fa-ellipsis-v"></i>&nbsp;
                   <i class="fa fa-angle-down"></i> <b class="caret"></b>
                   Outros
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="#evento-encerrado" data-toggle="tab">Evento Encerrado</a>
                    </li>
                    <li>
                        <a href="#evento-cancelado" data-toggle="tab">Evento Cancelado</a>
                    </li>
                </ul>
            </li>
            <li class="active">
                <a href="#acontecendo" data-toggle="tab" aria-expanded="true">Evento Acontecendo</a>
            </li>
            <li class="">
                <a href="#inscricoes-em-breve" data-toggle="tab" aria-expanded="false">Inscrições em breve</a>
            </li>
            <li class="">
                <a href="#inscricoes-abertas" data-toggle="tab" aria-expanded="false">Inscrições abertas</a>
            </li>
            <li class="">
                <a href="#inscricoes-encerradas" data-toggle="tab" aria-expanded="false">Inscrições encerradas</a>
            </li>
            
            
            
            
        </ul>
        <div class="tab-content">


            <!-- START ACONTECENDO -->
            <div class="tab-pane active" id="acontecendo">


                <div class="table-container">
                    <table class="table table-striped table-bordered table-hover" id="sample_3">
                        <thead style="background: #0badad; color: white;font-weight: bold;">
                            <tr>
                                <th class="bold"> Evento </th>
                                <th class="bold"> Local </th>
                                <th class="bold"> Fim do Evento </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach ($DatasetEvento as $key => $value):
                                    if($value->fk_sta_id == 403): // 403 = Evento Acontecendo. Tabela tb_status.
                            ?>
                            <tr style="border-bottom: 1px solid #ccc;">
                                <td>
                                    <div class="col-sm-2">
                                        <a href="{base_url}evento/FrontOffice/Evento/Dashboard/<?php echo $value->eve_id; ?>" title="Clique para acessar informações de inscrição desse evento" style="display:block;float:left;">
                                            <?php
                                                $filename = "{base_url}files/evento/{EventoControle}/logotipo/$value->eve_site_logotipo";
                                                if (is_null($value->eve_site_logotipo) == true || !file_exists($filename)){
                                                    $filename = "{base_url}assets/sigepe/Global/Images/Evento/no-photo.png";
                                                }
                                            ?>
                                            <img src="<?php echo $filename; ?>" style="width: 70px;display: inline-block;float: left;margin-right: 20px;border: 1px solid #ccc;padding: 1px;">

                                        </a>
                                    </div>
                                    <div class="col-sm-10">
                                        <a href="{base_url}evento/FrontOffice/Evento/Dashboard/<?php echo $value->eve_id; ?>" title="Clique para acessar informações de inscrição desse evento" style="display:block;float:left;color: #333;text-decoration: none;font-weight: 600;">
                                        <?php echo nl2br($value->eve_nome); ?>
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <?php
                                        $NomeEntidade = $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes_id, 1, 'pes_nome_razao_social');
                                        $NomeEntidade = explode('-', $NomeEntidade);
                                        echo '<b>' . $NomeEntidade[0] . '</b>' . '<br>' . $NomeEntidade[1];
                                    ?>
                                </td>
                                <td class="bold">
                                    <?php echo $this->my_data->ConverterData($value->eve_data_fim, 'ISO', 'PT-BR'); ?> <br>
                                    <span class="badge badge-danger badge-roundless" style="border-radius: 14px !important; font-weight: bold; background-color: red;">

                                        <?php 
                                            $QtdeDias   =   DateDifferences($value->eve_data_fim, date("Y-m-d"), 'd');
                                            switch ($QtdeDias) {
                                                case '0':
                                                    echo 'Hoje';
                                                    break;

                                                case '1':
                                                    echo 'Amanhã';
                                                    break;

                                                case '2':
                                                    echo '2 dias';
                                                    break;

                                                case '3':
                                                    echo '3 dias';
                                                    break;

                                                case '4':
                                                    echo '4 dias';
                                                    break;

                                                case '5':
                                                    echo '5 dias';
                                                    break;

                                            }
                                        ?>

                                    </span>
                                </td>
                            </tr>
                            <?php
                                    endif;
                                endforeach;
                            ?>
                        </tbody>
                    </table>
                </div>


            </div>
            <!-- END ACONTECENDO -->




            <div class="tab-pane" id="inscricoes-em-breve">

                <div class="table-container">
                    <table class="table table-striped table-bordered table-hover" id="sample_3">
                        <thead style="background: #0badad; color: white;font-weight: bold;">
                            <tr>
                                <th class="bold"> Evento </th>
                                <th class="bold"> Local </th>
                                <th class="bold"> Fim do Evento </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach ($DatasetEvento as $key => $value):
                                    if($value->fk_sta_id == 400): // 400 = Inscricoes Em Breve. Tabela tb_status.
                            ?>
                            <tr style="border-bottom: 1px solid #ccc;">
                                <td>
                                    <div class="col-sm-2">
                                        <a href="{base_url}evento/FrontOffice/Evento/Dashboard/<?php echo $value->eve_id; ?>" title="Clique para acessar informações de inscrição desse evento" style="display:block;float:left;">
                                            <?php
                                                $filename = "{base_url}files/evento/{EventoControle}/logotipo/$value->eve_site_logotipo";
                                                if (is_null($value->eve_site_logotipo) == true || !file_exists($filename)){
                                                    $filename = "{base_url}assets/sigepe/Global/Images/Evento/no-photo.png";
                                                }
                                            ?>
                                            <img src="<?php echo $filename; ?>" style="width: 70px;display: inline-block;float: left;margin-right: 20px;border: 1px solid #ccc;padding: 1px;">
                                        </a>
                                    </div>
                                    <div class="col-sm-10">
                                        <a href="{base_url}evento/FrontOffice/Evento/Dashboard/<?php echo $value->eve_id; ?>" title="Clique para acessar informações de inscrição desse evento" style="display:block;float:left;color: #333;text-decoration: none;font-weight: 600;">
                                        <?php echo nl2br($value->eve_nome); ?>
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <?php
                                        $NomeEntidade = $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes_id, 1, 'pes_nome_razao_social');
                                        $NomeEntidade = explode('-', $NomeEntidade);
                                        echo '<b>' . $NomeEntidade[0] . '</b>' . '<br>' . $NomeEntidade[1];
                                    ?>
                                </td>
                                <td class="bold">
                                    <?php echo $this->my_data->ConverterData($value->eve_data_fim, 'ISO', 'PT-BR'); ?> <br>
                                    <span class="badge badge-danger badge-roundless" style="border-radius: 14px !important; font-weight: bold; background-color: red;">

                                        <?php 
                                            $QtdeDias   =   DateDifferences($value->eve_data_fim, date("Y-m-d"), 'd');
                                            switch ($QtdeDias) {
                                                case '0':
                                                    echo 'Hoje';
                                                    break;

                                                case '1':
                                                    echo 'Amanhã';
                                                    break;

                                                case '2':
                                                    echo '2 dias';
                                                    break;

                                                case '3':
                                                    echo '3 dias';
                                                    break;

                                                case '4':
                                                    echo '4 dias';
                                                    break;

                                                case '5':
                                                    echo '5 dias';
                                                    break;

                                            }
                                        ?>

                                    </span>
                                </td>
                            </tr>
                            <?php
                                    endif;
                                endforeach;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END INSCRICOES EM BREVE -->




            <div class="tab-pane" id="inscricoes-abertas">

                <div class="table-container">
                    <table class="table table-striped table-bordered table-hover" id="sample_3">
                        <thead style="background: #0badad; color: white;font-weight: bold;">
                            <tr>
                                <th class="bold"> Evento </th>
                                <th class="bold"> Início das Inscrições </th>
                                <th class="bold"> Início do Evento </th>
                                <th class="bold"> Local </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach ($DatasetEvento as $key => $value):
                                    if($value->fk_sta_id == 401): // 401 = Inscricoes Abertas. Tabela tb_status.
                            ?>
                            <tr style="border-bottom: 1px solid #ccc;">
                                <td>
                                    <div class="col-sm-2">
                                        <a href="{base_url}FrontOffice/Evento/<?php echo $value->eve_id; ?>/#tab-inscricoes" title="Clique para acessar informações de inscrição desse evento" style="display:block;float:left;">
                                            <?php
                                                $filename = "{base_url}files/evento/{EventoControle}/logotipo/$value->eve_site_logotipo";
                                                if (is_null($value->eve_site_logotipo) == true || !file_exists($filename)){
                                                    $filename = "{base_url}assets/sigepe/Global/Images/Evento/no-photo.png";
                                                }
                                            ?>
                                            <img src="<?php echo $filename; ?>" style="width: 70px;display: inline-block;float: left;margin-right: 20px;border: 1px solid #ccc;padding: 1px;">
                                        </a>
                                    </div>
                                    <div class="col-sm-10">
                                        <a href="{base_url}FrontOffice/Evento/<?php echo $value->eve_id; ?>/#tab-inscricoes" title="Clique para acessar informações de inscrição desse evento" style="display:block;float:left;color: #333;text-decoration: none;font-weight: 600;">
                                        <?php echo nl2br($value->eve_nome); ?>
                                        </a>
                                    </div>
                                </td>
                                <td class="bold">
                                    <?php echo $this->my_data->ConverterData($value->eve_site_inscricao_inicio, 'ISO', 'PT-BR'); ?> <br>
                                    <span class="badge badge-danger badge-roundless" style="border-radius: 14px !important; font-weight: bold; background-color: red;">

                                        <?php 
                                            $QtdeDias   =   DateDifferences($value->eve_site_inscricao_inicio, date("Y-m-d"), 'd');
                                            switch ($QtdeDias) {
                                                case '0':
                                                    echo 'Hoje';
                                                    break;

                                                case '1':
                                                    echo 'Amanhã';
                                                    break;

                                                case '2':
                                                    echo '2 dias';
                                                    break;

                                                case '3':
                                                    echo '3 dias';
                                                    break;

                                                case '4':
                                                    echo '4 dias';
                                                    break;

                                                case '5':
                                                    echo '5 dias';
                                                    break;

                                            }
                                        ?>

                                    </span>
                                </td>

                                <td class="bold">
                                    <?php echo $this->my_data->ConverterData($value->eve_data_inicio, 'ISO', 'PT-BR'); ?> <br>
                                </td>

                                <td>
                                    <?php
                                        $NomeEntidade = $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_pes_id, 1, 'pes_nome_razao_social');
                                        $NomeEntidade = explode('-', $NomeEntidade);
                                        echo '<b>' . $NomeEntidade[0] . '</b>' . '<br>' . $NomeEntidade[1];
                                    ?>
                                </td>

                            </tr>
                            <?php
                                    endif;
                                endforeach;
                            ?>
                        </tbody>
                    </table>
                </div>


            </div>
            <div class="tab-pane " id="inscricoes-encerradas">
                Inscricoes Encerradas <br>
                Thumb/Nome  |  Encerrada em | Inicio do Evento (countdown em dias) |  Local
            </div>

            <div class="tab-pane " id="evento-encerrado">
                Evento Encerradas <br>
                Thumb/Nome  |  Início e Fim  |   Local
            </div>

            <div class="tab-pane " id="evento-cancelado">
                Evento Cancelado <br>
                Thumb/Nome  |  Local
            </div>




        </div>
    </div>







