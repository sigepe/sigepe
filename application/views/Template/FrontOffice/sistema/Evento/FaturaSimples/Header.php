<?php $this->load->module('evento/FrontOffice/FaturaSimples'); ?>


    <div class="row">
        <div class="col-sm-12 margin-top-10 margin-bottom-15" id="evento-top">
               <img src="{base_url}files/evento/<?php echo $DatasetEvento[0]->eve_controle; ?>/logotipo/<?php echo $DatasetEvento[0]->eve_site_logotipo; ?>" style="width:130px;border-radius: 10% !important;border: 2px solid #fff; box-shadow: 1px 1px 1px #999;margin-right: 35px;float: left;">
               <div class="" style="float: left;">
                    <h3 id="nome-evento" style="margin-top: 0;"><?php echo nl2br($DatasetEvento[0]->eve_nome); ?></h3>
                    <h4>
                        <div type="button" class="btn btn-default btn-sm" style="border-radius: 5px !important;">
                            <b>Status:</b> <?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $DatasetEvento[0]->fk_sta_id, 1, 'sta_status'); ?>
                        </div>
                    </h4>
               </div>
        </div>
        <!-- /evento-top -->
    </div>

    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:30px;border: 1px dotted #ddd;">

    <div class="row margin-bottom-20">
        <div class="col-sm-3 text-left">
            <h3 class="titulo-inscricao">
                <small class="badge badge-warning bold">Inscrição n°</small><br>
                <span class="font-yellow-crusta">
                    <?php echo $FaturaSimples[0]->frf_controle; ?>
                </span>
                <div class="display-none">[ AGUARDANDO PAGAMENTO ] ( HOVER INSCRIÇÕES SÓ SERÃO VÁLIDADAS APÓS COMPENSAÇÃO DE PAGAMENTO. ATENÇÃO: FIM DAS INSCRIÇÕES EM 21 DIAS. FIM DO DESCONTO EM X DIAS.)</div>
            </h3>
            <h3 class="titulo-cavaleiro">
                <small class="badge badge-primary bold">Cavaleiro</small><br>
                <span>
                    <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $FaturaSimples[0]->fk_pes_id, 1, 'pes_nome_razao_social'); ?>
                </span>
                <small class="titulo-categoria">
                    <b>Categoria:</b>
                        <?php echo $this->model_crud->get_rowSpecific('tb_evento_categoria', 'evc_id', $FaturaSimples[0]->fk_evc_id, 1, 'evc_sigla'); ?> -
                        <?php echo $this->model_crud->get_rowSpecific('tb_evento_categoria', 'evc_id', $FaturaSimples[0]->fk_evc_id, 1, 'evc_categoria'); ?>
                </small>
            </h3>
            <h3 class="titulo-animal">
                <small class="badge badge-primary bold">Animal</small><br>
                <span>
                    <?php echo $this->model_crud->get_rowSpecific('tb_animal', 'ani_id', $FaturaSimples[0]->fk_ani_id, 1, 'ani_nome_completo'); ?>
                </span>
                <small class="titulo-categoria display-none" style="display:none;"><b>Categoria:</b> {Sigla} - {Categoria}</small>
            </h3>
        </div>

        <div class="col-sm-6">
            <div class="portlet light bordered col-sm-12" style="margin-bottom: 10px;">
                  <div class="portlet-body">
          			<table class="table table-striped table-hover">
          			    <thead>
          			        <tr>
          			            <th style="width: 50%;text-align: left;"> Item </th>
                            <th  style="width: 25%;text-align: left;"> Preço </th>
          			            <th  style="width: 25%;text-align: left;"> Ação </th>
          			        </tr>
          			    </thead>
          			    <tbody>

                      
          			    	<?php echo $this->faturasimples->ResumoLinhaSerie($FaturaSimplesControle); ?>
          			    	<?php echo $this->faturasimples->ResumoLinhaProvaAvulsa($FaturaSimplesControle); ?>
          			    	<?php echo $this->faturasimples->ResumoLinhaBaia($FaturaSimplesControle); ?>
          			    	<?php echo $this->faturasimples->ResumoLinhaQuartoDeSela($FaturaSimplesControle); ?>
          			    	<?php echo $this->faturasimples->ResumoValores($FaturaSimplesControle); ?>
          			    </tbody>
          			</table>
          		</div>
          	</div>
        </div>
        <div class="col-sm-3 text-left">

      			<h1 style="margin-top: 0px;font-weight: bold;">
      			    <small class="badge badge-success bg-green-meadow bg-font-green-meadow bold">
      			    	VALOR TOTAL A SER PAGO ( À VISTA )
      			    </small><br>

      			    <?php
                  $PoliticaPreco = 1; // Remover essa linha

                 if($PoliticaPreco == '1'): ?>
      			    <span id="preco-total">
      			    	<span style="text-decoration: line-through; color: #666; font-size: 33px; margin-right: 20px; font-weight: 100;">R$ <b style="font-weight: 100;"><?php echo number_format($ResumoValorTotal, 2, ',' , '.'); ?></b> </span><br>
      			    	R$ <b><?php echo number_format($ResumoValorTotalPromocional, 2, ',' , '.'); ?></b>
      			    </span>
      				<?php endif; ?>


      			 <?php if($PoliticaPreco == '2'): ?>
      			    <span id="preco-total">
      			    	R$ <b><?php echo number_format($ResumoValorTotal, 2, ',', '.'); ?></b>
      			    </span>
      				<?php endif; ?>
      			    <br>
      			    <a href="<?php echo $FaturaSimples[0]->frf_boleto; ?>" class="btn btn-block btn-lg btn-success" id="btn-boleto"><span class="glyphicon glyphicon-barcode"></span> GERAR BOLETO</a>
      			</h1>

        </div>


    </div>
