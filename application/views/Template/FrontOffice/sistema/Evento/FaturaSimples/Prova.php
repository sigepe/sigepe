<style type="text/css">
#fatura-simples-prova{
    
}

#fatura-simples-prova .table>tbody>tr>td,
#fatura-simples-prova .table>tbody>tr>th,
#fatura-simples-prova .table>tfoot>tr>td,
#fatura-simples-prova .table>tfoot>tr>th,
#fatura-simples-prova.table>thead>tr>td,
#fatura-simples-prova .table>thead>tr>th{
/*    padding: 0px;*/
}

#fatura-simples-prova .titulo-provas{
    margin-top: 10px;
    margin-bottom: 0;
    border-bottom: 1px solid #ddd;
    padding-bottom: 20px;
    color: #666;
    position: relative;
    font-size: 23px;
    font-weight: 900;

}
#fatura-simples-prova .titulo-provas b{
    font-weight: 500;
    color: #3598dc;
}
#fatura-simples-prova .categorias-da-serie{
    color: rgba(102, 102, 102, 0.6);
    display: block;
    margin-top: 8px;
}
#fatura-simples-prova #tabela-scrollable{
    margin-top: 0px !important;
}
#fatura-simples-prova #btn-deletar-serie,
#fatura-simples-prova #btn-cadastrar-prova,
#fatura-simples-prova #btn-cadastrar-serie{
    border-radius: 28px !important;
    font-weight: 400 !important;
    float: right;
    padding: 9px 60px;
    position: absolute;
    top: 3px;
    right: 0;
    font-size: 14px;
}
.titulo-secao{
    font-weight: 800;
    margin-bottom: 23px;
    font-size: 27px;
    letter-spacing: -1px;    
}
</style>



<?php
    $this->load->module('evento/FrontOffice/FaturaSimples');
?>


    <h3 class="titulo-secao">
        PROVAS QUE O CONJUNTO ESTÁ INSCRITO
        <i class="fa fa-caret-down" aria-hidden="true"></i>
     </h3>

    <div class="portlet light bordered" id="fatura-simples-prova">
        <div class="portlet-body">

            <div class="table-scrollable table-scrollable-borderless" id="tabela-scrollable">


                <?php if(is_null($FaturaSimples[0]->fk_ers_id)): ?>

                    <?php if($TipoDeVendaDaInscricao == '1' || $TipoDeVendaDaInscricao == '3'): ?>

                    <h3 class="titulo-provas" style="">
                        <b>Série</b>
                        <small class="categorias-da-serie">
                            Provas agrupadas em Série
                        </small>
                        <a href="#" class="btn btn-circle btn-warning btn-sm" id="btn-cadastrar-serie">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            Comprar Série
                        </a>    
                    </h3>

                    <div class="alert alert-success">
                        <strong>Informativo!</strong>
                        Para este evento é permitido a venda de inscrições em série. Compre várias provas de uma mesma série e garanta um super desconto. <br>
                        <a href="#" title="">Clique aqui para comprar uma série.</a>
                    </div>
                    <?php endif; ?>

                <?php endif; ?>


                <?php if(!is_null($FaturaSimples[0]->fk_ers_id)): ?>
                <h3 class="titulo-provas">
                    <b><?php echo $this->model_crud->get_rowSpecific('tb_evento_rel_serie', 'ers_id', $FaturaSimples[0]->fk_ers_id, 1, 'ers_nome'); ?></b>
                    <small class="categorias-da-serie">

                        <?php
                            $this->load->module('evento/serie');
                            $CategoriasDaSerie      =   $this->serie->GetCategoriasDaSerie($FaturaSimples[0]->fk_ers_id);
                            $Contador   =   1;
                            foreach ($CategoriasDaSerie as $key => $CategoriaDaSerie):

                                echo  '<span class="tooltips" title="" data-original-title="'.$CategoriaDaSerie->evc_categoria.'" > ' . $CategoriaDaSerie->evc_sigla . '</span>';
                                if($Contador != count($CategoriasDaSerie))
                                    echo ' / ';

                                $Contador++;
                            endforeach;
                        ?>

                    </small>
                    <a href="#" class="btn btn-circle btn-danger btn-sm" id="btn-deletar-serie">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                        Deletar Série
                    </a>
                </h3>

                <?php
                    $InscricoesDaSerie  =   $this->faturasimples->GetInscricoesDaSerie($FaturaSimples[0]->frf_id);
                    if(!empty($InscricoesDaSerie)):
                ?>

                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">
                                Identificação
                                <i class="fa fa fa-info-circle font-blue" aria-hidden="true"></i>
                            </th>
                            <th class="text-center"> Prova </th>
                            <th class="text-center"> Status </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($InscricoesDaSerie as $key => $value): ?>
                        <tr>
                            <td class="text-center"> <?php echo $value->fri_controle; ?> </td>
                            <td class="text-center">
                                <b class="btn btn-circle btn-sm blue bold" style="display: block;">
                                    <?php echo $this->model_crud->get_rowSpecific('tb_evento_rel_serie_rel_prova', 'srp_id', $value->fk_srp_id, 1, 'srp_nome'); ?> - 
                                    <?php echo $this->model_crud->get_rowSpecific('tb_evento_rel_serie_rel_prova', 'srp_id', $value->fk_srp_id, 1, 'srp_obstaculo_altura'); ?>M

                                    <?php if(!is_null($value->srp_nome_trofeu)): ?>
                                        <br>
                                        <small> <?php echo $value->srp_nome_trofeu; ?> </small>
                                    <?php endif; ?>

                                </b>
                            </td>
                            <td class="text-center">
                                <span class="btn btn-circle btn-sm  bg-grey-steel font-grey-mint" style="display: block;">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <b>Inscrição não confirmada.</b> Aguardando pagamento.
                                </span>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                   </tbody>
                </table>
                <?php endif; ?>

                <hr>
                <?php endif; ?>




















                <?php
                    $InscricoesDaProva  =   $this->faturasimples->GetInscricoesDaProva($FaturaSimples[0]->frf_id);
                    if( ($TipoDeVendaDaInscricao == '2' || $TipoDeVendaDaInscricao == '3') || !empty($InscricoesDaProva)  ):
                ?>
                <h3 class="titulo-provas" style="float: left;width: 100%;">
                    <b>Provas Avulsas</b>
                    <small class="categorias-da-serie">
                        Relação de Provas
                    </small>

                    <?php if($TipoDeVendaDaInscricao == '2' || $TipoDeVendaDaInscricao == '3'): ?>
                    <a href="#" class="btn btn-circle btn-warning btn-sm" id="btn-cadastrar-prova">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        Comprar Prova Avulsa
                    </a>    
                    <?php endif; ?>

                </h3>
                <?php endif; ?>



                <?php
                    if( !empty($InscricoesDaProva) ):
                ?>
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 20%">
                                Identificação
                                <i class="fa fa fa-info-circle font-blue" aria-hidden="true"></i>
                            </th>
                            <th class="text-center" style="width: 30%"> Prova </th>
                            <th class="text-center" style="width: 40%"> Status </th>
                            <th class="text-center" style="width: 10%"> Ação </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($InscricoesDaProva as $key => $value):
                        ?>
                        <tr>
                            <td class="text-center"> <?php echo $value->fri_controle; ?> </td>

                            <td class="text-center">
                                <b class="btn btn-circle btn-sm blue bold" style="display: block;">
                                    <?php echo $this->model_crud->get_rowSpecific('tb_evento_rel_serie_rel_prova', 'srp_id', $value->fk_srp_id, 1, 'srp_nome'); ?> - 
                                    <?php echo $this->model_crud->get_rowSpecific('tb_evento_rel_serie_rel_prova', 'srp_id', $value->fk_srp_id, 1, 'srp_obstaculo_altura'); ?>M
    
                                    <?php if(!is_null($value->srp_nome_trofeu)): ?>
                                        <br>
                                        <small> <?php echo $value->srp_nome_trofeu; ?> </small>
                                    <?php endif; ?>

                                </b>
                                

                            </td>
                            <td class="text-center">
                                <span class="btn btn-circle btn-sm  bg-grey-steel font-grey-mint" style="display: block;">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <b>Inscrição não confirmada.</b> Aguardando pagamento.
                                </span>
                            </td>

                            <td class="text-center"> <a href="#" class="btn btn-circle btn-danger btn-sm" style="border-radius: 28px !important;font-weight: 400 !important;float: right;padding: 5px 40px;"><i class="fa fa-trash" aria-hidden="true"></i> Deletar Prova</a> </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?php endif; ?>

















            </div>



        </div>
    </div>




    <hr style="margin-top: 20px;margin-bottom:0px;border: 1px dotted #ddd;float:left;width: 100%">
    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:30px;border: 1px dotted #ddd;">