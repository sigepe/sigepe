    <div class="row">
        <div class="col-sm-12 margin-top-10 margin-bottom-15" id="evento-top">
               <img src="{base_url}files/evento/<?php echo $DatasetEvento[0]->eve_controle; ?>/logotipo/<?php echo $DatasetEvento[0]->eve_site_logotipo; ?>" style="width:130px;border-radius: 10% !important;border: 2px solid #fff; box-shadow: 1px 1px 1px #999;margin-right: 35px;float: left;">
               <div class="" style="float: left;">
                    <h3 id="nome-evento" style="margin-top: 0;"><?php echo nl2br($DatasetEvento[0]->eve_nome); ?></h3>
                    <h4>
                        <div type="button" class="btn btn-default btn-sm" style="border-radius: 5px !important;">
                            <b>Status:</b> <?php echo $DatasetEvento[0]->fk_sta_id; ?>
                        </div>
                    </h4>
               </div>
        </div>
        <!-- /evento-top -->
    </div>

    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:30px;border: 1px dotted #ddd;">

    <div class="row margin-bottom-20">
        <div class="col-sm-4 text-left">
            <h3 style="margin-top: 0px;font-weight: 600;">
                <small class="badge badge-warning bold">Inscrição n°</small><br>
                <span class="font-yellow-crusta" style="display: block; font-size: 38px; margin-top: 3px;">
                    <?php echo $FaturaSimples[0]->ifs_controle; ?>
                </span>
            </h3>
        </div>
        <div class="col-sm-8">
            <div class="row">
                <div class="col-sm-6">
                    <h3 style="margin-top: 0px;font-weight: bold;">
                        <small class="badge badge-primary bold">Cavaleiro</small><br>
                        <span style="letter-spacing: -1px;font-weight: 600;">GUSTAVO MENDES BOTEGA</span>
                        <small style="letter-spacing: -1px;display: block;margin-top: 5px;color: #888;"><b>Categoria:</b> JCA - Jovem Cavaleiro A</small>
                    </h3>
                </div>
                <div class="col-sm-6">
                    <h3 style="margin-top: 0px;">
                        <small class="badge badge-primary bold">Animal</small><br>
                        <span style="font-weight: 600;letter-spacing: -1px;">CAVALLUS BANGKOK</span>
                        <small style="letter-spacing: -1px;display: block;margin-top: 5px;color: #888;"><b>Categoria:</b> CN4 - Cavalos Novos 5 anos</small>
                    </h3>
                </div>
            </div>

        </div>
    </div>


    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:30px;border: 1px dotted #ddd;">


    <h3 style="font-weight: 800; margin-bottom: 23px; font-size: 27px; letter-spacing: -1px;">
        PROVAS QUE O CONJUNTO ESTÁ INSCRITO
        <i class="fa fa-caret-down" aria-hidden="true"></i>
     </h3>

    <div class="portlet light bordered">
        <div class="portlet-body">


            <div class="table-scrollable table-scrollable-borderless" style="margin-top: 0px !important;">
                <table class="table table-hover table-light">
                    <h3 style="margin-top: 10px; border-bottom: 1px solid #ddd; padding-bottom: 20px;color: #666;">
                        <b>Série 1.10M</b>
                        <small style="color: #666;">
                            PMR / JCA / AMA / MA / ABE / CN5
                        </small>
                        <a href="#" class="btn btn-circle btn-danger btn-sm" style="border-radius: 28px !important;font-weight: 400 !important;float: right;padding: 5px 40px;"><i class="fa fa-trash" aria-hidden="true"></i> Deletar Série</a>
                    </h3>
                    <tbody>
                        <tr>
                            <td>
                                <a href="javascript:;" class="tooltips" data-original-title="1,00 M | LIVRE, PROFISSIONAL E AMADOR">
                                    <span class="label label-primary" style="display: block;padding: 4px;">
                                        <b>Prova 01 - 1.10M</b>
                                    </span>
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="javascript:;" class="btn btn-circle btn-sm blue btn-gerenciar-conjunto">  Informações </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="javascript:;" class="tooltips" data-original-title="1,00 M | LIVRE, PROFISSIONAL E AMADOR">
                                    <span class="label label-primary" style="display: block;padding: 4px;">
                                        <b>Prova 04 - 1.10M</b>
                                    </span>
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="javascript:;" class="btn btn-circle btn-sm blue btn-gerenciar-conjunto">  Informações </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="javascript:;" class="tooltips" data-original-title="1,00 M | LIVRE, PROFISSIONAL E AMADOR">
                                    <span class="label label-primary" style="display: block;padding: 4px;">
                                        <b>Prova 11 - 1.10M</b>
                                    </span>
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="javascript:;" class="btn btn-circle btn-sm blue btn-gerenciar-conjunto">  Informações </a>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <hr>

                <table class="table table-hover table-light">
                    <h3 style="margin-top: 10px; border-bottom: 1px solid #ddd; padding-bottom: 20px;color: #666;">
                        <b>Provas Avulsas</b>
                        <a href="#" class="btn btn-circle btn-warning btn-sm" style="border-radius: 28px !important;font-weight: 400 !important;float: right;padding: 5px 40px;"><i class="fa fa-plus" aria-hidden="true"></i> Adicionar Prova</a>
                    </h3>
                    <tbody>
                        <tr>
                            <td>
                                <a href="javascript:;" class="tooltips" data-original-title="1,00 M | LIVRE, PROFISSIONAL E AMADOR">
                                    <span class="label label-primary" style="display: block;padding: 4px;">
                                        <b>Prova 16 - 1.10M </b> <br>
                                        <small>Copa Ouro Brasília Indoor 2017</small>
                                    </span>
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="javascript:;" class="btn btn-circle btn-sm blue btn-gerenciar-conjunto">  Informações </a>
                                <a href="javascript:;" class="btn btn-circle btn-sm blue btn-gerenciar-conjunto">  Deletar </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="javascript:;" class="tooltips" data-original-title="1,00 M | LIVRE, PROFISSIONAL E AMADOR">
                                    <span class="label label-primary" style="display: block;padding: 4px;">
                                        <b>Prova 21 - 1.40M </b> <br>
                                        <small>Clássico Troféu Perpétuo Luis Ferreira Correa</small>
                                    </span>
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="javascript:;" class="btn btn-circle btn-sm blue btn-gerenciar-conjunto">  Informações </a>
                                <a href="javascript:;" class="btn btn-circle btn-sm blue btn-gerenciar-conjunto">  Deletar </a>
                            </td>
                        </tr>

                    </tbody>
                </table>



            </div>



        </div>
    </div>







    <h3 style="font-weight: 800; margin-bottom: 23px; font-size: 27px; letter-spacing: -1px;">
        QUARTOS DE SELA
        <i class="fa fa-caret-down" aria-hidden="true"></i>
     </h3>

    <div class="portlet light bordered">
        <div class="portlet-body">

            content

        </div>
    </div>



    <h3 style="font-weight: 800; margin-bottom: 23px; font-size: 27px; letter-spacing: -1px;">
        RESUMO
     </h3>
