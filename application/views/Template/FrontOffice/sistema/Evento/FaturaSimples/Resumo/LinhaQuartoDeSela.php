

    <tr>
        <td class="text-right">
            <b>Quarto de Sela</b> <br>
            <small class="font-grey-cascade"> <?php echo ($DatasetLinhaQuartoQuantidade > 1) ? $DatasetLinhaQuartoQuantidade . ' Unidades' : $DatasetLinhaQuartoQuantidade . ' Unidade'; ?></small>
        </td>
        <td class="text-left">
   

            <?php if($PoliticaPreco == '1' && !is_null($DatasetLinhaQuartoValorPromocional)): ?>
            <small style="text-decoration: line-through;" class="font-grey-cascade">
                R$ <?php echo $this->my_moeda->InserirPontuacao($DatasetLinhaQuartoValor); ?>
            </small>  
            R$ <?php echo $this->my_moeda->InserirPontuacao($DatasetLinhaQuartoValorPromocional); ?>
            <?php endif; ?>


            <?php if($PoliticaPreco == '2' || ( $PoliticaPreco=='1' && is_null($DatasetLinhaQuartoValorPromocional) )  ): ?>
            R$ <?php echo $this->my_moeda->InserirPontuacao($DatasetLinhaQuartoValor); ?>
            <?php endif; ?>
                



        </td>
    </tr>