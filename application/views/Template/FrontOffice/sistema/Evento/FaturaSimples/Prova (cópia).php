<style type="text/css">
#fatura-simples-prova{
    
}

#fatura-simples-prova .table>tbody>tr>td,
#fatura-simples-prova .table>tbody>tr>th,
#fatura-simples-prova .table>tfoot>tr>td,
#fatura-simples-prova .table>tfoot>tr>th,
#fatura-simples-prova.table>thead>tr>td,
#fatura-simples-prova .table>thead>tr>th{
    padding: 0px;
}
</style>



    <h3 style="font-weight: 800; margin-bottom: 23px; font-size: 27px; letter-spacing: -1px;">
        PROVAS QUE O CONJUNTO ESTÁ INSCRITO
        <i class="fa fa-caret-down" aria-hidden="true"></i>
     </h3>

    <div class="portlet light bordered" id="fatura-simples-prova">
        <div class="portlet-body">


            <div class="table-scrollable table-scrollable-borderless" style="margin-top: 0px !important;">
                <table class="table table-hover table-light">
                    <h3 style="margin-top: 10px; border-bottom: 1px solid #ddd; padding-bottom: 20px;color: #666;">
                        <b>Série 1.10M</b>
                        <small style="color: #666;">
                            PMR / JCA / AMA / MA / ABE / CN5
                        </small>
                        <a href="#" class="btn btn-circle btn-danger btn-sm" style="border-radius: 28px !important;font-weight: 400 !important;float: right;padding: 5px 40px;"><i class="fa fa-trash" aria-hidden="true"></i> Deletar Série</a>
                    </h3>
                    <tbody>
                        <tr>
                            <td>
                                <a href="javascript:;" class="tooltips" data-original-title="1,00 M | LIVRE, PROFISSIONAL E AMADOR">
                                    <span class="label label-primary" style="display: block;padding: 4px;">
                                        <b>Prova 01 - 1.10M</b>
                                    </span> 
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="javascript:;" class="btn btn-circle btn-sm blue btn-gerenciar-conjunto">  Informações </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="javascript:;" class="tooltips" data-original-title="1,00 M | LIVRE, PROFISSIONAL E AMADOR">
                                    <span class="label label-primary" style="display: block;padding: 4px;">
                                        <b>Prova 04 - 1.10M</b>
                                    </span> 
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="javascript:;" class="btn btn-circle btn-sm blue btn-gerenciar-conjunto">  Informações </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="javascript:;" class="tooltips" data-original-title="1,00 M | LIVRE, PROFISSIONAL E AMADOR">
                                    <span class="label label-primary" style="display: block;padding: 4px;">
                                        <b>Prova 11 - 1.10M</b>
                                    </span> 
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="javascript:;" class="btn btn-circle btn-sm blue btn-gerenciar-conjunto">  Informações </a>
                            </td>
                        </tr>
                    </tbody>
                </table>    

                <hr>

                <table class="table table-hover table-light">
                    <h3 style="margin-top: 10px; border-bottom: 1px solid #ddd; padding-bottom: 20px;color: #666;">
                        <b>Provas Avulsas</b>
                        <a href="#" class="btn btn-circle btn-warning btn-sm" style="border-radius: 28px !important;font-weight: 400 !important;float: right;padding: 5px 40px;"><i class="fa fa-plus" aria-hidden="true"></i> Adicionar Prova</a>
                    </h3>
                    <tbody>
                        <tr>
                            <td>
                                <a href="javascript:;" class="tooltips" data-original-title="1,00 M | LIVRE, PROFISSIONAL E AMADOR">
                                    <span class="label label-primary" style="display: block;padding: 4px;">
                                        <b>Prova 16 - 1.10M </b> <br>
                                        <small>Copa Ouro Brasília Indoor 2017</small>
                                    </span> 
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="javascript:;" class="btn btn-circle btn-sm blue btn-gerenciar-conjunto">  Informações </a>
                                <a href="javascript:;" class="btn btn-circle btn-sm blue btn-gerenciar-conjunto">  Deletar </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="javascript:;" class="tooltips" data-original-title="1,00 M | LIVRE, PROFISSIONAL E AMADOR">
                                    <span class="label label-primary" style="display: block;padding: 4px;">
                                        <b>Prova 21 - 1.40M </b> <br>
                                        <small>Clássico Troféu Perpétuo Luis Ferreira Correa</small>
                                    </span> 
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="javascript:;" class="btn btn-circle btn-sm blue btn-gerenciar-conjunto">  Informações </a>
                                <a href="javascript:;" class="btn btn-circle btn-sm blue btn-gerenciar-conjunto">  Deletar </a>
                            </td>
                        </tr>

                    </tbody>
                </table>



            </div>



        </div>
    </div>


