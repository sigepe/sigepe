<style type="text/css">
.active a{
    font-weight: bold !important;
}
</style>



    <div class="col-sm-3" id="evento-sidebar">

        <div class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet " style="padding-top: 15px !important;">
                <!-- SIDEBAR USERPIC -->

                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu" style="margin-top: 0px !important;">
                    <ul class="nav">
                        <li class="<?php echo ($Class == 'Evento' && $Method == 'Dashboard') ? 'active' : ''; ?>">
                            <a href="{base_url}BackOffice/Evento/Dashboard/Detalhar/{EventoId}">
                                <i class="icon-home"></i> Visão Geral </a>
                        </li>
                        <li>
                            <a href="{base_url}BackOffice/Evento/Inscricao/Listar/{EventoId}">
                                <i class="icon-info"></i> Inscrições </a>
                        </li>
                        <li class="<?php echo ($Class == 'Serie' || $Class == 'CadastroSerie') ? 'active' : ''; ?>">
                            <a href="{base_url}BackOffice/Evento/Serie/Listar/{EventoId}">
                                <i class="icon-info"></i> Séries </a>
                        </li>
                        <li class="<?php echo ($Class == 'Prova' || $Class == 'CadastroProva') ? 'active' : ''; ?>">
                            <a href="{base_url}BackOffice/Evento/Prova/Listar/{EventoId}">
                                <i class="icon-info"></i> Provas</a>
                        </li>
                        <li class="<?php echo ($Class == 'Baia') ? 'active' : ''; ?>">
                            <a href="{base_url}BackOffice/Evento/Prova/Detalhar/{EventoId}">
                                <i class="icon-info"></i> Baias </a>
                        </li>
                        <li class="<?php echo ($Class == 'QuartoDeSela') ? 'active' : ''; ?>">
                            <a href="{base_url}BackOffice/Evento/QuartoSela/Detalhar/{EventoId}">
                                <i class="icon-info"></i> Quartos de Selas </a>
                        </li>
                        <li>
                            <a href="{base_url}BackOffice/Evento/Oficial/Listar/{EventoId}">
                                <i class="icon-info"></i> Oficiais </a>
                        </li>
                        <li>
                            <a href="{base_url}BackOffice/Evento/PatApoi/Listar/{EventoId}">
                                <i class="icon-info"></i> Patrocinadores / Apoiador </a>
                        </li>
                        <li>
                            <a href="{base_url}BackOffice/Evento/Documento/Listar/{EventoId}">
                                <i class="icon-info"></i> Documentos </a>
                        </li>
                        <li>
                            <a href="{base_url}BackOffice/Evento/OrdemEntrada/Listar/{EventoId}">
                                <i class="icon-info"></i> Ordem de Entrada </a>
                        </li>
                        <li>
                            <a href="{base_url}BackOffice/Evento/Resultado/Listar/{EventoId}">
                                <i class="icon-info"></i> Resultado </a>
                        </li>
                        <li>
                            <a href="{base_url}{base_url}BackOffice/Evento/Ranking/Listar/{EventoId}">
                                <i class="icon-info"></i> Ranking </a>
                        </li>
                        <li>
                            <a href="{base_url}BackOffice/Evento/Financeiro/Listar/{EventoId}">
                                <i class="fa fa-usd"></i> Financeiro </a>
                        </li>
                        <li>
                            <a href="{base_url}BackOffice/Evento/Galeria/Listar/{EventoId}">
                                <i class="icon-camera"></i> Fotos </a>
                        </li>
                        <li>
                            <a href="{base_url}BackOffice/Evento/Configuracao/Editar/{EventoId}">
                                <i class="fa fa-cog"></i> Configurações </a>
                        </li>
                    </ul>
                </div>
                <!-- END MENU -->
            </div>
            <!-- END PORTLET MAIN -->
        </div>
        <!-- /profile-sidebar -->

    </div>
    <!-- /evento-sidebar -->
