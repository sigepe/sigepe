<style type="text/css">

.easy-autocomplete-container{
    top: 25px;
}
.easy-autocomplete{
    width: 100% !important;
}
#nome-proprietario{
    text-transform: uppercase;
}
.page-header .page-header-menu.fixed{
    position: relative;
}
.form-actions{
}
.form .form-actions, .portlet-form .form-actions{
    padding: 20px !important;
    margin: 0 !important;
    background-color: #f5f5f5 !important;
    border-top: 1px solid #e7ecf1 !important;     
}
#baia-error,
#quarto-de-sela-error,
#ativar-site-error{
    display: block;
    width: 100%;
    float: left;    
}


.ms-container{
    width: 600px;
}
</style>




    <style type="text/css">

        .sigepe .tiles .tile{
            overflow: initial !important;
        }

        .tiles .tile{
            border: none !important;
        }

        .tiles .tile .tile-body{
            margin-bottom: 0 !important;
        }




/*
       .sigepe .tiles .tile:after{
            content: "1";
            background: red;
            width: 100%;
            height: 30px;   
        }
*/

        .tiles-empresas .tile{
            margin-bottom: 60px !important;
        }
        .sigepe .tiles .tile{
            width: 100% !important;
            margin-bottom: 0px;
        }
        .sigepe .tiles .tile .tile-object>.name{
            position: relative !important;
            margin-left: 0px !important;
            margin-right: 0px !important;
            text-align: center;
        }
        .sigepe .tiles{
            margin-right: 0px !important;
        }


        .btn.blue:not(.btn-outline) {
            color: #FFF;
            background-color: #3598dc;
            border-color: #3598dc;
            width: 100%;
            white-space: normal;
        }

        #my_multi_select1-error{
            color: red;
        }


    </style>



<div class="col-sm-9 evento-conteudo" id="evento-cadastro-serie">

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> Série</span>
                <span class="caption-helper">Formulário de cadastro de série</span>
            </div>
        </div>
        <div class="portlet-body">


            <form class="form form-horizontal" role="form" id="form-serie" name="form-serie" action="{base_url}evento/CadastroSerie/Processar" method="post">
               
                <div class="form-body">

                    <!-- MODALIDADE -->
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            Altura do Obstáculo
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select class="form-control" name="altura" id="altura">
                                <option value="">- Selecione uma Altura -</option>
                                <?php foreach ($DatasetSerie as $key => $value): ?>
                                <option value="<?php echo $value->evs_id; ?>"><?php echo $value->evs_altura; ?>M</option>
                                <?php endforeach; ?>
                            </select>      
                        </div>
                    </div>


                    <!-- NOME EVENTO -->
                    <div class="form-group">
                        <label class="control-label col-md-3">Nome da Série <span class="required" aria-required="true">
                        * </span>
                        </label>
                        <div class="col-md-9">
                            <input name="nome-serie" class="form-control" type="text" style="width:100%;">
                        </div>
                    </div>
            

                    <hr>


                    <!-- VALOR ATE INSCRICAO DEFINITIVA -->
                    <div class="form-group">
                        <label class="control-label col-md-3">Valor ATÉ Inscrição Definitiva <span class="required" aria-required="true">
                        * </span>
                        </label>
                        <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon"> R$ </span>
                                    <input type="text" class="form-control text-center money" name="valor-ate-inscricao-definitiva" placeholder="Digite um valor">
                                </div>
                        </div>
                    </div>


                    <!-- VALOR APÓS INSCRICAO DEFINITIVA -->
                    <div class="form-group">
                        <label class="control-label col-md-3">Valor APÓS Inscrição Definitiva <span class="required" aria-required="true">
                        * </span>
                        </label>
                        <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon"> R$ </span>
                                    <input type="text" class="form-control text-center money" name="valor-apos-inscricao-definitiva" placeholder="Digite um valor">
                                </div>
                        </div>
                    </div>
            



                    <!-- CATEGORIAS -->
                    <div class="portlet light bordered">

                        <div class="portlet-title">
                            <div class="caption font-yellow-crusta">
                                <i class="icon-share font-yellow-crusta"></i>
                                <span class="caption-subject bold uppercase"> Categorias</span>
                                <span class="caption-helper">Indique quais são as categorias que fazem parte da série</span>
                            </div>
                        </div>

                        <div class="portlet-body">
                            <select multiple="multiple" class="multi-select" id="my_multi_select1" name="serie-categoria[]">
                                <?php foreach ($DatasetSerieCategoria as $key => $value): ?>
                                <option value="<?php echo $value->evc_id; ?>"><?php echo $value->evc_sigla . ' - ' . $value->evc_categoria; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <!-- /porlet-body (site ) -->

                        <div id="serie-categoria-error"></div>

                    </div>
                    <!-- portlet(site) -->


                </div>
                <!-- /form-body -->
                  
                <div class="form-actions">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-3">
                        <button type="submit" class="btn green">Cadastrar Série</button>
                    </div>
                </div>
                <!-- /form-actions -->


                <!-- HIDDEN -->
                <input type="hidden" name="evento-id" value="{EventoId}">

            </form>

        </div>
        <!-- /portlet-body -->

    </div>
    <!-- /portlet -->

</div>
<!-- /evento-cadastro-serie -->



