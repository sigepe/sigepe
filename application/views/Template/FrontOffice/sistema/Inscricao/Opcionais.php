<style type="text/css">
  .item-opcional h3 {
      border-bottom: 1px dotted #eee;
      padding-bottom: 10px;
  }
  .box-opcional {
      background: #fcfcfc;
      border-radius: 5px !important;
      padding: 13px 13px;
      box-shadow: 1px 1px 1px #eee;
  }
  .stroke{
    text-decoration: line-through;
  }

</style>


<div class="container page-inscricao">

    <div class="row inscricao-titulo-secao">
      <div class="col-sm-12">
        <ul>
          <li class="numero-bloco">03</li>
          <li class="titulo">ESCOLHA <b>OPCIONAIS</b></li>
        </ul>
      </div>

    </div>

    <div class="row">
      <div class="col-sm-12">
        <hr class="separador-inscricao">
        <hr class="separador-inscricao">
        <hr class="separador-inscricao">
        <hr class="separador-inscricao">
        <hr class="separador-inscricao">
      </div>
    </div>


    <div class="row">


      <?php if( !is_null($DatasetEvento[0]->flag_baia) ||  !is_null($DatasetEvento[0]->flag_quarto) ): ?>
      <div class="col-sm-6 item-opcional" id="opcional-baia">

        <h3>Baia</h3>

        <?php if( !is_null($DatasetEvento[0]->flag_baia) ): ?>
        <h4>
            <i class="fa fa-usd" aria-hidden="true"></i>
            
            <?php 
                $date     = new DateTime( $DatasetEvento[0]->eve_data_limite_sem_acrescimo );
                $now      = new DateTime();

                if($date < $now): // preco promocional
                    echo '
                      <span class="stroke">
                          R$ '.$DatasetEvento[0]->eve_baia_valor_com_acrescimo.'
                      </span>
                          R$ '.$DatasetEvento[0]->eve_baia_valor_sem_acrescimo.'
                    ';

                    $ValorBaia  =  $DatasetEvento[0]->eve_baia_valor_sem_acrescimo;
                endif;

                if($date > $now): // preco cheio
                    echo ' R$ '.$DatasetEvento[0]->eve_baia_valor_com_acrescimo.' ';
                    $ValorBaia  =  $DatasetEvento[0]->eve_baia_valor_com_acrescimo;
                endif;
            ?>
                / por q.s.
        </h4>

        <div class="box-opcional">
          
          <div class="row">

              <div class="col-sm-6 text-right">
                <label class="bold">Selecione a quantidade de baias:</label>
              </div>
              <div class="col-sm-6 text-left">
                <input type="number" name="quantity" min="0" max="9" id="quantidade-baia" style="padding: 6px 10px 6px 20px; text-align: center;">
                <input type="hidden" name="valor-baia" id="valor-baia" value="<?php echo $ValorBaia; ?>" >
              </div>

          </div>

        </div>
        <?php endif; ?>  
        <?php if( is_null($DatasetEvento[0]->flag_baia) ): ?>
            <div class="col-sm-12 aviso-baia-desabilitado">
              <div class="alert alert-warning">
                <strong>Atenção!</strong> Para este evento baia não está disponível.
              </div>
            </div>
        <?php endif; ?>  


      </div>

      <div class="col-sm-6 item-opcional" id="opcional-qs">

        <h3>Quarto de Sela</h3>


        <?php if( !is_null($DatasetEvento[0]->flag_quarto) ): ?>
        <h4>
            <i class="fa fa-usd" aria-hidden="true"></i>
            
            <?php 
                $date     = new DateTime( $DatasetEvento[0]->eve_data_limite_sem_acrescimo );
                $now      = new DateTime();

                if($date < $now): // preco promocional
                    echo '
                      <span class="stroke">
                          R$ '.$DatasetEvento[0]->eve_quarto_valor_sem_acrescimo.'
                      </span>
                          R$ '.$DatasetEvento[0]->eve_quarto_valor_com_acrescimo.'
                    ';
                    $ValorQs  =  $DatasetEvento[0]->eve_quarto_valor_sem_acrescimo;
                endif;

                if($date > $now): // preco cheio
                    echo ' R$ '.$DatasetEvento[0]->eve_quarto_valor_com_acrescimo.' ';
                    $ValorQs  =  $DatasetEvento[0]->eve_quarto_valor_com_acrescimo;
                endif;
            ?>
                / por q.s.
        </h4>

        <div class="box-opcional">
          <div class="row">
            <div class="col-sm-6 text-right">
              <label class="bold">Selecione a quantidade de q.s.:</label>
            </div>
            
            <div class="col-sm-6 text-left">
              <input type="number" name="quantity" min="0" max="9" id="quantidade-qs" style="padding: 6px 10px 6px 20px; text-align: center;">
              <input type="hidden" name="valor-qs" id="valor-qs" value="<?php echo $ValorQs; ?>" >
            </div>
          </div>
        </div>  
        <?php endif; ?>

        <?php if( is_null($DatasetEvento[0]->flag_quarto) ): ?>
            <div class="col-sm-12 aviso-qs-desabilitado">
              <div class="alert alert-warning">
                <strong>Atenção!</strong> Para este evento quarto de sela não está disponível.
              </div>
            </div>
        <?php endif; ?>  


      </div>
      <?php endif; ?>  



      <div class="col-sm-12 aviso-selecione-conjunto" style="display: none;">
        <div class="alert alert-warning">
          <strong>Atenção!</strong> Selecione um conjunto para continuar a inscrição.
        </div>
      </div>



      <?php if( is_null($DatasetEvento[0]->flag_baia) &&  is_null($DatasetEvento[0]->flag_quarto) ): ?>
      <div class="col-sm-12 aviso-baia-qs-desabilitado" style="display: none;">
        <div class="alert alert-warning">
          <strong>Atenção!</strong> Para este evento baia e quarto de sela estão inativos.
        </div>
      </div>
      <?php endif; ?>



    </div>

</div>
