
<style type="text/css">




.linha-prova .td-checkbox {
    background: transparent !important;
}

.linha-prova .td-checkbox input{
    margin: 0;
    position: relative;
}
.linha-prova .nome .linha-1{
    font-weight: bold;
    font-size: 15px;
    color: #666;
}


tr.linha-prova.linha-prova-selecionado,
tr.linha-prova-selecionado-serie
{
    background: green !important;
}

.linha-prova-selecionado,
.linha-prova-selecionado td,
.linha-prova-selecionado:hover,
.linha-prova-selecionado:hover td.
.linha-prova-selecionado-serie,
.linha-prova-selecionado-serie td,
.linha-prova-selecionado-serie:hover,
.linha-prova-selecionado-serie:hover td{
    background: green !important;
}

.table.table-light.table-hover>tbody>tr.linha-prova-selecionado:hover>td, .table.table-light.table-hover>tbody>tr.linha-prova-selecionado>td:hover, .table.table-light.table-hover>tbody>tr.linha-prova-selecionado>th:hover{background: green !important; }
.table-hover>tbody>tr.linha-prova-selecionado:hover, .table-hover>tbody>tr.linha-prova-selecionado:hover>td{background: green !important; }

.table.table-light.table-hover>tbody>tr.linha-prova-selecionado-serie:hover>td, .table.table-light.table-hover>tbody>tr.linha-prova-selecionado-serie>td:hover, .table.table-light.table-hover>tbody>tr.linha-prova-selecionado-serie>th:hover{background: green !important; }
.table-hover>tbody>tr.linha-prova-selecionado-serie:hover, .table-hover>tbody>tr.linha-prova-selecionado-serie:hover>td{background: green !important; }






/*.table.table-light.table-hover>tbody>tr:hover>td, .table.table-light.table-hover>tbody>tr>td:hover, .table.table-light.table-hover>tbody>tr>th:hover{}*/


.linha-prova-selecionado *,
.linha-prova-selecionado-serie *{
    color: white !important;
}

.linha-prova-habilitado:hover{
    opacity: 1;
    cursor: pointer !important;
}

.linha-prova-inabilitado{
    opacity: 0.2;
    cursor: default;
}

.linha-prova{
    cursor: pointer !important;
}
.linha-prova-inabilitado{
    cursor: default !important;
}





</style>



<?php 
/*

                             <label for="prova-<?php echo $Prova->srp_id; ?>">
                                Prova <?php echo $Prova->srp_numero_prova; ?><br>
                                <small> <?php echo $Prova->srp_nome; ?> </small> <br>
                                <small> Preço: --- </small> <br>
                                <small> Característica / Pista </small> <br>
                            </label>



                        <div class="col-sm-3 prova-item">
                            <div class="box <?php echo ($i == 13) ? 'selecionado' : ''; ?>   <?php echo ($i == 3 || $i == 5 || $i == 9 || $i == 12 ) ? 'desabilitado' : ''; ?>">
                                
                                <label for="prova-<?php echo $i; ?>">
                                    <div class="numero">Prova <?php echo $i; ?></div>
                                    <div class="nome"> <?php echo $i; ?> </div>
                                    <div class="preco">
                                        <span class="preco-promocional">R$ 280,00</span> <span class="preco-cheio">R$ 150,00</span>
                                    </div>
                                    <div> Característica </div>
                                    <div> Pista </div>
                                </label>
                            </div>
                        </div>
*/
?>






<div class="container page-inscricao" id="bloco-provas">


    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-yellow-crusta">
                <i class="icon-share font-yellow-crusta"></i>
                <span class="caption-subject bold uppercase"> Prova</span>
                <span class="caption-helper">Faça a inscrição por seleção de provas avulsas</span>
            </div>
        </div>
        <div class="portlet-body">

            <div class="row">
                <div class="col-sm-12">
                    <div class="table-scrollable table-scrollable-borderless">
                        <table class="table table-hover table-light">
                            <thead>
                                <tr>
                                    <th> # </th>
                                    <th width="30%"> Prova </th>
                                    <th> Preço </th>
                                    <th> Característica </th>
                                    <th> Pista </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($ProvasCategorias as $Prova): ?>
                                    
                                    <tr class="linha-prova" data-prova-id="<?php echo $Prova->srp_id; ?>">
                                        <td class="td-checkbox">
                                            <input type="checkbox" value="<?php echo $Prova->srp_id; ?>" name="provas[]" id="prova-<?php echo $Prova->srp_id; ?>" />
                                        </td>
                                        <td class="nome"> 
                                            <span class="linha-1">Prova <?php echo $Prova->srp_numero_prova . ' - ' . $Prova->srp_nome;?></span><br>
                                            <small class="linha-3"> <i class="fa fa-clock-o" aria-hidden="true"></i> Horário da Prova <!--14/04 (sábado) às 08h--></small>
                                        </td>
                                        <td class="preco">
                                            <?php if($PoliticaPreco == 'promocional'): ?>
                                            <small style="text-decoration: line-through;">R$ <?php echo $Prova->srp_valor; ?></small>
                                            <span style="font-weight: bold;">R$ <?php echo $Prova->srp_valor_promocional; ?></span>
                                            <?php endif; ?>
                                            <?php if($PoliticaPreco == 'cheio'): ?>
                                            <span style="font-weight: bold;">R$ <?php echo $Prova->srp_valor; ?></span>
                                            <?php endif; ?>
                                        </td>
                                        <td class="caracteristica"> <?php echo $Prova->spc_caracteristica; ?> </td>
                                        <td class="pista"> <?php echo $Prova->spp_pista; ?> </td>
                                    </tr>
                                
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>

        </div>
    </div>

</div>

