

    <!-- BEGIN PROFILE CONTENT -->
    <div class="profile-content" id="meu-perfil">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light " style="width: 100%; float: left;">


                    <div class="portlet-title">
                        <div class="caption font-blue-madison">
                            <span class="caption-subject bold uppercase"> PERFIL DO ANIMAL</span>
                            <span class="caption-helper">Informações do perfil do animal no SIGEPE</span>
                        </div>
                        <div class="actions">
                          <!--   <a class="btn btn-circle btn-edit red-sunglo btn-sm" href="javascript:;">
                                <i class="fa fa-pencil"></i>
                                Editar Informações do Animal
                            </a> -->
                        </div>
                    </div>

                    <div class="portlet-body">
                        <div class="tab-content">
                            <!-- PERSONAL INFO TAB -->
                            <div class="tab-pane active" id="tab_1_1">


                                <div class="table-scrollable table-scrollable-borderless">
                                    <table class="table table-hover table-light">
                                        <thead>
                                            <tr>
                                                <th style="width:30%;" class="text-right bold"> ITEM </th>
                                                <th style="width:70%;" class="text-left bold"> DESCRIÇÃO </th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <!--  Codigo FHBr -->
                                            <tr>
                                                <td class="text-right bold"> Código Animal </td>
                                                <td> #{AnimalId} </td>
                                            </tr>

                                            <!--  Autor -->
                                            <tr>
                                                <td class="text-right bold"> Autor </td>
                                                <td> <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $fk_aut_id, 1, 'pes_nome_razao_social'); ?> <br> <small>ID: {fk_aut_id}-</small> </td>
                                            </tr>

                                            <!--  Codigo FHBr -->
                                            <tr>
                                                <td class="text-right bold"> Proprietário </td>
                                                <td> <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $ProprietarioId, 1, 'pes_nome_razao_social'); ?> <br> <small>ID: {ProprietarioId}</small> </td>
                                            </tr>

                                            <!--  N Passaporte Atual -->
                                            <tr>
                                                <td class="text-right bold"> Passaporte Atual </td>
                                                <td>
                                                    {Npassaporte}
                                                    <br>
                                                    <small>ID: -</small>
                                                </td>
                                            </tr>

                                            <!--  Confederacao -->
                                            <tr>
                                                <td class="text-right bold"> Confederação </td>
                                                <td> {Sigla} - {Nome} </td>
                                            </tr>

                                            <!--  Federacao -->
                                            <tr>
                                                <td class="text-right bold"> Federação </td>
                                                <td> {Sigla} - {Nome} </td>
                                            </tr>

                                            <!--  Entidade -->
                                            <tr>
                                                <td class="text-right bold"> Entidade </td>
                                                <td> {Sigla} - {Nome} </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>

                                <hr>

                                <style type="text/css">
                                    .table-scrollable input,
                                    .table-scrollable select{
                                        display: none;
                                    }
                                    .table-scrollable .form-control-static{
                                        min-height: auto;
                                        padding: 0;
                                    }
                                </style>

                                <div class="table-scrollable">
                                    <table class="table table-hover table-light">
                                        <thead>
                                            <tr style="background: #eee; height: 50px;">
                                                <th style="width:30%;" class="text-right bold"> ITEM </th>
                                                <th style="width:70%;" class="text-left bold"> DESCRIÇÃO </th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <!--  N Chip -->
                                            <tr>
                                                <td class="text-right bold"> Nº Chip </td>
                                                <td>
                                                    <div class="form-control-static" id="fcs-numero-chip"><?php echo (empty($ani_numero_chip)) ? '-' : $ani_numero_chip; ?></div>
                                                    <input type="text" name="numero-chip" value="<?php echo (empty($ani_numero_chip)) ? '' : $ani_numero_chip ; ?>" class="form-control" />
                                                </td>
                                            </tr>


                                            <!--  Data de Nascimento -->
                                            <tr>
                                                <td class="text-right bold"> Data de Nascimento </td>
                                                <td>                                                    
                                                    <div class="form-control-static" id="fcs-data-nascimento"><?php echo (empty($ani_data_nascimento)) ? '-' : $this->my_data->ConverterData($ani_data_nascimento, 'ISO', 'PT-BR') . '<br><small>'. $this->my_data->CalcularIdade($ani_data_nascimento, 'pt-br') .' anos</small>' ; ?></div>
                                                    <input type="text" name="data-nascimento" value="<?php echo (empty($ani_data_nascimento)) ? '' : $this->my_data->ConverterData($ani_data_nascimento, 'ISO', 'PT-BR') ; ?>" class="form-control mask-date" />
                                                </td>
                                            </tr>

                                            <!--  Nome Completo -->
                                            <tr>
                                                <td class="text-right bold"> Nome Completo </td>
                                                <td>
                                                    <div class="form-control-static" id="fcs-nome-completo"><?php echo (!is_null($ani_nome_completo)) ? $ani_nome_completo : '-' ?></div>
                                                    <input type="text" name="nome-completo" value="{ani_nome_completo}" class="form-control" />
                                                </td>
                                            </tr>

                                            <!--  Nome Patrocinado -->
                                            <tr>
                                                <td class="text-right bold"> Nome Patrocinado </td>
                                                <td>
                                                    <div class="form-control-static" id="fcs-nome-patrocinado"><?php echo (!is_null($ani_nome_patrocinado)) ? $ani_nome_patrocinado : '-' ?></div>
                                                    <input type="text" name="nome-patrocinado" value="{ani_nome_patrocinado}" class="form-control" />
                                                </td>
                                            </tr>

                                            <!--  Raça -->
                                            <tr>
                                                <td class="text-right bold">  Raça </td>
                                                <td>
                                                    <div class="form-control-static" id="fcs-raca"> <?php echo (empty($fk_anr_id)) ? '-' : $this->model_crud->get_rowSpecific('tb_animal_raca', 'anr_id', $fk_anr_id, 1, 'anr_raca'); ?> </div>
                                                    <select class="form-control" name="raca">
                                                        <option value="">- Selecione uma Opção</option>
                                                        <?php foreach ($DatasetRaca as $key => $value): ?>
                                                        <option value="<?php echo $value->anr_id; ?>"  <?php echo ($value->anr_id == $fk_anr_id) ? 'selected' : ''; ?> ><?php echo $value->anr_raca; ?></option>
                                                       <?php endforeach; ?>
                                                    </select>
                                                </td>
                                            </tr>

                                            <!--  Pelagem -->
                                            <tr>
                                                <td class="text-right bold"> Pelagem </td>
                                                <td>
                                                    <div class="form-control-static" id="fcs-pelagem"> <?php echo (empty($fk_anp_id)) ? '-' : $this->model_crud->get_rowSpecific('tb_animal_pelagem', 'anp_id', $fk_anp_id, 1, 'anp_pelagem'); ?> </div>
                                                    <select class="form-control" name="pelagem">
                                                        <option value="">- Selecione uma Opção</option>
                                                        <?php foreach ($DatasetPelagem as $key => $value): ?>
                                                        <option value="<?php echo $value->anp_id; ?>"  <?php echo ($value->anp_id == $fk_anp_id) ? 'selected' : ''; ?> ><?php echo $value->anp_pelagem; ?></option>
                                                       <?php endforeach; ?>
                                                    </select>
                                                </td>
                                            </tr>

                                            <!--  Gênero -->
                                            <tr>
                                                <td class="text-right bold"> Gênero </td>
                                                <td>
                                                    <div class="form-control-static" id="fcs-genero"><?php echo (empty($fk_ang_id)) ? '-' : $this->model_crud->get_rowSpecific('tb_animal_genero', 'ang_id', $fk_ang_id, 1, 'ang_genero'); ?> </div>
                                                    <select class="form-control" name="genero">
                                                        <option value="">- Selecione uma Opção</option>
                                                        <?php foreach ($DatasetGenero as $key => $value): ?>
                                                        <option value="<?php echo $value->ang_id; ?>"  <?php echo ($value->ang_id == $fk_ang_id) ? 'selected' : ''; ?> ><?php echo $value->ang_genero; ?></option>
                                                       <?php endforeach; ?>
                                                    </select>
                                                </td>
                                            </tr>

                                            <!--  Tipo do Gênero -->
                                            <?php if(!empty($fk_ang_id) && $fk_ang_id == '1'): ?>
                                            <tr>
                                                <td class="text-right bold"> Tipo do Gênero </td>
                                                <td>
                                                    <div class="form-control-static" id="fcs-tipo-genero"><?php echo (empty($fk_agt_id)) ? '-' : $this->model_crud->get_rowSpecific('tb_animal_genero_tipo', 'agt_id', $fk_agt_id, 1, 'agt_tipo'); ?> </div>
                                                    <select class="form-control" name="tipo-genero-tipo">
                                                        <option value="">- Selecione uma Opção</option>
                                                        <?php foreach ($DatasetGeneroTipo as $key => $value): ?>
                                                        <option value="<?php echo $value->agt_id; ?>"  <?php echo ($value->agt_id == $fk_agt_id) ? 'selected' : ''; ?> ><?php echo $value->agt_tipo; ?></option>
                                                       <?php endforeach; ?>
                                                    </select>
                                                </td>
                                            </tr>
                                            <?php endif; ?>

                                            <!--  Peso -->
                                            <tr>
                                                <td class="text-right bold"> Peso </td>
                                                <td>
                                                    <div class="form-control-static" id="fcs-peso"><?php echo (!is_null($ani_peso)) ? $ani_peso . ' kg' : '-' ?></div>
                                                    <input type="text" name="peso" value="<?php echo $ani_peso; ?>" class="form-control" />
                                                </td>
                                            </tr>

                                            <!--  Altura da Cruz -->
                                            <tr>
                                                <td class="text-right bold"> Altura da Cruz </td>
                                                <td>
                                                    <div class="form-control-static" id="fcs-altura-cruz"><?php echo (!is_null($ani_altura_cruz)) ? $ani_altura_cruz : '-' ?></div>
                                                    <input type="text" name="altura-cruz" value="<?php echo $ani_altura_cruz; ?>" class="form-control" />
                                                </td>
                                            </tr>

                                            <!--  N FEI -->
                                            <tr>
                                                <td class="text-right bold"> Nº FEI </td>
                                                <td>
                                                    <div class="form-control-static" id="fcs-registro-fei"><?php echo (!is_null($ani_registro_fei)) ? $ani_registro_fei : '-' ?></div>
                                                    <input type="text" name="registro-fei" value="<?php echo $ani_registro_fei; ?>" class="form-control" />
                                                </td>
                                            </tr>

                                            <!--  N CBH -->
                                            <tr>
                                                <td class="text-right bold"> Nº CBH </td>
                                                <td>
                                                    <div class="form-control-static" id="fcs-registro-cbh"><?php echo (!is_null($ani_registro_cbh)) ? $ani_registro_cbh : '-' ?></div>
                                                    <input type="text" name="registro-cbh" value="<?php echo $ani_registro_cbh; ?>" class="form-control" />
                                                </td>
                                            </tr>

                                            <!--  N Registro Genealogico -->
                                            <tr>
                                                <td class="text-right bold"> Nº Registro Genealógico </td>
                                                <td>
                                                    <div class="form-control-static" id="fcs-registro-genealogico"><?php echo (!is_null($ani_registro_genealogico)) ? $ani_registro_genealogico : '-' ?></div>
                                                    <input type="text" name="registro-genealogico" value="<?php echo $ani_registro_genealogico; ?>" class="form-control" />
                                                </td>
                                            </tr>

                                            <!--  Associação de Registro -->
                                            <tr>
                                                <td class="text-right bold"> Associação de Registro </td>
                                                <td>
                                                    <div class="form-control-static" id="fcs-associacao"><?php echo (empty($fk_ass_id)) ? '-' : $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $fk_ass_id, 1, 'pes_nome_razao_social'); ?></div>
                                                    <select class="form-control" name="pais">
                                                        <option value="">- Selecione uma Opção</option>
                                                        <?php foreach ($DatasetAssociacao as $key => $value): ?>
                                                        <option value="<?php echo $value->pes_id; ?>"  <?php echo ($value->pes_id == $fk_ass_id) ? 'selected' : ''; ?> ><?php echo $value->pes_nome_razao_social; ?></option>
                                                       <?php endforeach; ?>
                                                    </select>

                                                </td>
                                            </tr>

                                            <!--  Pais de Origem -->
                                            <tr>
                                                <td class="text-right bold"> País de Origem </td>
                                                <td>
                                                    <div class="form-control-static" id="fcs-pais"><?php echo (empty($fk_pai_id)) ? '-' : $this->model_crud->get_rowSpecific('tb_pais', 'pai_id', $fk_pai_id, 1, 'pai_pais'); ?></div>
                                                    <select class="form-control" name="pais">
                                                        <option value="">- Selecione uma Opção</option>
                                                        <?php foreach ($DatasetPais as $key => $value): ?>
                                                        <option value="<?php echo $value->pai_id; ?>"  <?php echo ($value->pai_id == $fk_pai_id) ? 'selected' : ''; ?> ><?php echo $value->pai_pais; ?></option>
                                                       <?php endforeach; ?>
                                                    </select>
                                                </td>
                                            </tr>

                                            <!--  Nome do Pai -->
                                            <tr>
                                                <td class="text-right bold"> Nome do Pai </td>
                                                <td>
                                                    <div class="form-control-static" id="fcs-nome-pai"><?php echo (!is_null($ani_nome_pai)) ? $ani_nome_pai : '-' ?></div>
                                                    <input type="text" name="nome-pai" value="<?php echo $ani_nome_pai; ?>" class="form-control" />
                                                </td>
                                            </tr>

                                            <!--  Nome da Mae -->
                                            <tr>
                                                <td class="text-right bold"> Nome da Mãe </td>
                                                <td>
                                                    <div class="form-control-static" id="fcs-nome-mae"><?php echo (!is_null($ani_nome_mae)) ? $ani_nome_mae : '-' ?></div>
                                                    <input type="text" name="nome-mae" value="<?php echo $ani_nome_mae; ?>" class="form-control" />
                                                </td>
                                            </tr>

                                            <!--  Nome do Avo Materno -->
                                            <tr class="tr-edit">
                                                <td class="text-right bold"> Nome do Avô Materno </td>
                                                <td>
                                                    <div class="form-control-static" id="fcs-nome-avo-materno"><?php echo (!is_null($ani_nome_avo_materno)) ? $ani_nome_avo_materno : '-' ?></div>
                                                    <input type="text" name="nome-avo-materno" value="<?php echo $ani_nome_avo_materno; ?>" class="form-control" />
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>

                                
                            </div>
                            <!-- END PERSONAL INFO TAB -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PROFILE CONTENT -->




    </div>  
    <!-- div aberta em sidebar.php ( /.col-sm-12 ) -->

</div>
<!-- div aberta em sidebar.php ( /.row ) -->