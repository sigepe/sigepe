    <style type="text/css">

        .sigepe .tiles .tile{
            overflow: initial !important;
        }

        .tiles .tile{
            border: none !important;
        }

        .tiles .tile .tile-body{
            margin-bottom: 0 !important;
        }




/*
       .sigepe .tiles .tile:after{
            content: "1";
            background: red;
            width: 100%;
            height: 30px;   
        }
*/

        .tiles-empresas .tile{
            margin-bottom: 60px !important;
        }
        .sigepe .tiles .tile{
            width: 100% !important;
            margin-bottom: 0px;
        }
        .sigepe .tiles .tile .tile-object>.name{
            position: relative !important;
            margin-left: 0px !important;
            margin-right: 0px !important;
            text-align: center;
        }
        .sigepe .tiles{
            margin-right: 0px !important;
        }


        .btn.blue:not(.btn-outline) {
            color: #FFF;
            background-color: #3598dc;
            border-color: #3598dc;
            width: 100%;
            white-space: normal;
        }


    </style>

                
    <div class="portlet light ">
        <div class="portlet-body">
            
                <a href="{base_url}FrontOffice/Animal/Cadastro" class="btn btn-circle display-block red-sunglo ">
                    <i class="fa fa-plus"></i> CADASTRAR ANIMAL </a>

            <hr>


            <div class="tabbable-line" style="display: block">


                <ul class="nav nav-tabs ">
                    <li class="active">
                        <a href="#tab-todos" data-toggle="tab">
                            <i class="fa fa-caret-right" aria-hidden="true"></i>
                            Todos os <b>animais</b>
                        </a>
                    </li>
                    <li>
                        <a href="#tab-autor" data-toggle="tab">
                            <i class="fa fa-caret-right" aria-hidden="true"></i>
                            Animais <b>registrados por mim</b>
                        </a>
                    </li>
                    <li>
                        <a href="#tab-proprietario" data-toggle="tab">
                            <i class="fa fa-caret-right" aria-hidden="true"></i>
                            Animais que sou <b>proprietário</b>
                        </a>
                    </li>
                    <li>
                        <a href="#tab-responsavel-financeiro" data-toggle="tab">
                            <i class="fa fa-caret-right" aria-hidden="true"></i>
                            Animais que sou <b>responsável financeiro</b>
                        </a>
                    </li>
                </ul>



                <div class="tab-content" style="padding-top: 10px;">



                    <!-- 
                    TODOS OS ANIMAIS
                    =========================================== -->
                    <div class="tab-pane active" id="tab-todos">


                    </div>
                    <!-- // tab-todos -->


                    <!-- 
                    AUTOR
                    =========================================== -->
                    <div class="tab-pane" id="tab-autor">


                        <?php if(empty($AnimaisAutor)): ?>
                        <div class="alert alert-warning">
                            <strong>Informativo!</strong> Você não tem nenhum animal registrado por você.
                        </div>
                        <?php endif; ?>


                        <?php if(!empty($AnimaisAutor)): ?>
                        <div class="table-scrollable">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center"> ID do Animal </th>
                                        <th> Animal </th>
                                        <th> Autor </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($AnimaisAutor as $key => $value): ?>
                                    <tr>
                                        <td class="text-center"> <?php echo $value->ani_id; ?> </td>
                                        <td>
                                            <a href="{base_url}FrontOffice/Animal/Dashboard/<?php echo $value->ani_id; ?>" title="" class="tooltips" data-original-title="Acessar perfil do animal">
                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                <?php echo $value->ani_nome_completo; ?>
                                            </a>        
                                            <br>
                                            <small><b>Nº Chip:</b> <?php echo $value->ani_numero_chip; ?></small>                                     
                                        </td>
                                        <td>
                                            <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_aut_id, 1, 'pes_nome_razao_social'); ?>
                                            <br>
                                            <b>ID:</b> <?php echo $value->fk_aut_id; ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <?php endif; ?>


                    </div>
                    <!-- // tab-autor -->


                    <!-- 
                    PROPRIETARIO
                    =========================================== -->
                    <div class="tab-pane" id="tab-proprietario">


                        <?php if(empty($AnimaisProprietario)): ?>
                        <div class="alert alert-warning">
                            <strong>Informativo!</strong> Você não é proprietário de nenhum animal.
                        </div>
                        <?php endif; ?>


                        <?php if(!empty($AnimaisProprietario)): ?>
                        <div class="table-scrollable">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center"> ID do Animal </th>
                                        <th> Animal </th>
                                        <th> Proprietário </th>
                                        <th> Status </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($AnimaisProprietario as $key => $value): ?>
                                    <tr>
                                        <td class="text-center"> <?php echo $value->ani_id; ?> </td>
                                        <td>
                                            <a href="{base_url}FrontOffice/Animal/Dashboard/<?php echo $value->ani_id; ?>" title="" class="tooltips" data-original-title="Acessar perfil do animal">
                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                <?php echo $value->ani_nome_completo; ?>
                                            </a>       
                                            <br>
                                            <small><b>Nº Chip:</b> <?php echo $value->ani_numero_chip; ?></small>                                     
                                        </td>
                                        <td>
                                            <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_aut_id, 1, 'pes_nome_razao_social'); ?>
                                            <br>
                                            <b>ID:</b> #<?php echo $value->fk_pes1_id; ?>
                                        </td>
                                        <td>

                                            <span class="label label-sm tooltips label-<?php echo GetBootstrapColor($value->fk_sta_id); ?> bold" data-original-title="<?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $value->fk_sta_id, 1, 'sta_status'); ?>" >
                                                <i class="fa <?php echo GetIconVinculo($value->fk_sta_id); ?>" aria-hidden="true"></i>
                                                <?php
                                                    echo GetStatusVinculo($value->fk_sta_id);
                                                ?>
                                            </span>

                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <?php endif; ?>


                    </div>
                    <!-- // tab-proprietario -->


                    <!-- 
                    RESPONSAVEL FINANCEIRO
                    =========================================== -->
                    <div class="tab-pane" id="tab-responsavel-financeiro">

                        <div class="alert alert-warning">
                            <strong>Atenção!</strong> Você não é responsável financeiro por nenhum animal.
                        </div>

                    </div>
                    <!-- // tab-proprietario -->

                </div>

            </div>            

        </div>
        <!-- /portlet-body -->

    </div>
    <!-- /porlet -->

