    <style type="text/css">

        .sigepe .tiles .tile{
            overflow: initial !important;
        }
        .tiles .tile{
            border: none !important;
        }
        .tiles .tile .tile-body{
            margin-bottom: 0 !important;
        }
/*
       .sigepe .tiles .tile:after{
            content: "1";
            background: red;
            width: 100%;
            height: 30px;
        }
*/

        .tiles-empresas .tile{
            margin-bottom: 60px !important;
        }
        .sigepe .tiles .tile{
            width: 100% !important;
            margin-bottom: 0px;
        }
        .sigepe .tiles .tile .tile-object>.name{
            position: relative !important;
            margin-left: 0px !important;
            margin-right: 0px !important;
            text-align: center;
        }
        .sigepe .tiles{
            margin-right: 0px !important;
        }
        .btn.blue:not(.btn-outline) {
            color: #FFF;
            background-color: #3598dc;
            border-color: #3598dc;
            width: 100%;
            white-space: normal;
        }
    </style>


    <div class="portlet light ">
        <div class="portlet-body">


            <div class="tiles">



                <!--
                MENUS
                ======================================= -->
                <div class="row">

                    <div class="col-sm-3">
                        <a href="{base_url}evento/FrontOffice/Listar/EventosPorStatus#inscricoes-abertas" title="">
                            <div class="tile bg-green-meadow">
                                <div class="tile-body">
                                    <i class="fa fa-id-card"></i>
                                </div>
                                <div class="tile-object">
                                    <div class="name"> Inscrições </div>
                                </div>
                            </div>
                        </a>
                    </div>


                    <div class="col-sm-3">
                        <div class="tile bg-yellow link-redirect" data-redirect="FrontOffice/Animal/MeusAnimais">
                            <div class="tile-body">
                                <i class="fa fa-paw"></i>
                            </div>
                            <div class="tile-object">
                                <div class="name"> Meus Animais </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-3">
                        <div class="tile bg-green">
                            <div class="tile-body">
                                <i class="fa fa-comments-o"></i>
                            </div>
                            <div class="tile-object">
                                <div class="name"> Chat Zendesk </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /MENUS -->


                <!--
                PERFIS
                ======================================= -->
                <?php
                $Perfis  =   $this->session->userdata('Perfis');
                if(!empty($Perfis)):
                ?>
                <div class="row tiles-perfis">

                    <hr />


                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-dark bold uppercase">Perfil</span>
                            </div>
                        </div>
                        <div class="portlet-body row">



                            <?php
                                foreach ($Perfis as $key => $value):
                                    if($value == '5')
                                        $LinkTile   =   'FrontOffice/Perfil/Atleta'; 

                                    if($value == '14')
                                        $LinkTile   =   'BackOffice/Dashboard'; 
                            ?>
                            <div class="col-sm-3">
                                <a class="tile bg-grey-cascade" href='{base_url}<?php echo $LinkTile; ?>'>
                                    <div class="tile-body">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                        <?php echo $this->model_crud->get_rowSpecific('tb_vinculo_perfil', 'vip_id', $value, 1, 'vip_papel'); ?>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <?php endforeach; ?>

                        </div>
                    </div>
                    <!-- end portlet -->



                </div>
                <?php endif;  ?>
                <!-- /PERFIS -->





                <!--
                EMPRESAS
                ======================================= -->
                <?php
                $Empresas  =   $this->session->userdata('Empresas');
                if(!empty($Empresas)):
                ?>
                <div class="row tiles-empresas">

                    <hr />


                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-dark bold uppercase">Empresas</span>
                            </div>
                        </div>
                        <div class="portlet-body row">

                            <?php foreach ($Empresas as $key => $value): ?>
                            <div class="col-sm-3">

                                <div class="tile bg-grey-cascade popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="<?php echo $this->model_crud->get_rowSpecific('tb_vinculo_tipo', 'vit_id', $value['TipoVinculo'], 1, 'vit_tipo_vinculo'); ?>" data-original-title="Vínculo">
                                    <a href="javascript:;">
                                        <div class="tile-body">
                                            <i class="fa fa-briefcase"></i>
                                        </div>
                                        <div class="tile-object">
                                            <div class="name">

                                            <?php

                                                if($value['TipoVinculo'] == '12' || $value['TipoVinculo'] == '16')
                                                    echo 'Confederação';

                                                if($value['TipoVinculo'] == '13' || $value['TipoVinculo'] == '17')
                                                    echo 'Federação';

                                                if($value['TipoVinculo'] == '14' || $value['TipoVinculo'] == '18')
                                                    echo 'Entidade Equestre';

                                                if($value['TipoVinculo'] == '15' || $value['TipoVinculo'] == '19')
                                                    echo 'Empresa Ordinária';

                                            ?>

                                            </div>
                                        </div>
                                    </a>
                                    <a href="javascript:;" class="btn blue"> <?php echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value['IdEmpresa'], 1, 'pes_nome_razao_social') ?> </a>
                                </div>
                            </div>
                            <?php endforeach; ?>

                        </div>
                    </div>
                    <!-- end portlet -->

                </div>
                <?php endif; ?>
                <!-- /EMPRESAS -->





            </div>
            <!-- /tiles -->

        </div>
        <!-- /portlet-body -->

    </div>
    <!-- /porlet -->

   <!--  </div> -->
