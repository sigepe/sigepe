<style type="text/css">
    .profile-usermenu{
        margin-top: 0px !important;
        padding-bottom: 0px !important;
    }
</style>

<script type="text/javascript">
    document.querySelector('.disable-btn').click(function(e){
        e.preventDefault();
        return false;
    });
</script>


<div class="row">
    
    <div class="col-sm-12">

        <!-- BEGIN PROFILE SIDEBAR -->
        <div class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet" style="padding-top: 1px !important;">

                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">
                    <ul class="nav">
                        
                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'Dashboard' ) ? 'active' : ''; ?>">
                            <a href="{base_url}evento/FrontOffice/Evento/Dashboard/{EventoId}"> <i class="fa fa-info-circle"></i> Informações </a>
                        </li>

                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'Inscricao' ) ? 'active' : ''; ?>">
                            <a href="{base_url}evento/FrontOffice/FaturaGlobal/Dashboard/{EventoId}"> <i class="fa fa-file-text"></i> Inscrições </a>
                        </li>   

                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'Serie' ) ? 'active' : ''; ?>">
                            <a href="{base_url}evento/FrontOffice/Evento/Serie/{EventoId}" onclick="return false;"> <i class="fa fa-bars"></i> Séries </a>
                        </li>

                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'Prova' ) ? 'active' : ''; ?>">
                            <a href="{base_url}inscricao/FrontOffice/Evento/Prova/{EventoId}" onclick="return false;"> <i class="fa fa-pencil-square-o"></i> Provas </a>
                        </li>

                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'Patrocinador' ) ? 'active' : ''; ?>">
                            <a href="{base_url}inscricao/FrontOffice/Evento/Patrocinador/{EventoId}" onclick="return false;"> <i class="fa fa-star"></i> Patrocinadores </a>
                        </li>

                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'Apoiador' ) ? 'active' : ''; ?>">
                            <a href="{base_url}inscricao/FrontOffice/Evento/Apoiador/{EventoId}" onclick="return false;"> <i class="fa fa-users"></i> Apoiadores </a>
                        </li>

                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'Documento' ) ? 'active' : ''; ?>">
                            <a href="{base_url}evento/FrontOffice/Evento/Documento/{EventoId}" onclick="return false;"> <i class="fa fa-book"></i> Documentos </a>
                        </li>

                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'OrdemEntrada' ) ? 'active' : ''; ?>">
                            <a href="{base_url}evento/FrontOffice/Evento/OrdemEntrada/{EventoId}" onclick="return false;"> <i class="fa fa-sort"></i> Ordem de Entrada </a>
                        </li>

                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'Resultado' ) ? 'active' : ''; ?>">
                            <a href="{base_url}evento/FrontOffice/Evento/Resultado/{EventoId}" onclick="return false;"> <i class="fa fa-trophy"></i> Resultado </a>
                        </li>

                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'Ranking' ) ? 'active' : ''; ?>">
                            <a href="{base_url}evento/FrontOffice/Evento/Ranking/{EventoId}" onclick="return false;"> <i class="fa fa-bar-chart"></i> Ranking </a>
                        </li>

                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'Fotos' ) ? 'active' : ''; ?>">
                            <a href="{base_url}evento/FrontOffice/Evento/Fotos/{EventoId}" onclick="return false;"> <i class="fa fa-picture-o"></i> Fotos </a>
                        </li>

                    </ul>
                </div>
                <!-- END MENU -->
            </div>
            <!-- END PORTLET MAIN -->
           
        </div>
        <!-- END BEGIN PROFILE SIDEBAR -->


