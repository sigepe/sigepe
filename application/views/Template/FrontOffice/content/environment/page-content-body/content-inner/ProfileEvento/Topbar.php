<style type="text/css">
    .separador-inscricao {
    margin-top: 0px !important;
    margin-bottom: 0px !important;
    border: 1px dotted #eee !important;
    }
    .page-logo{
        width: 150px;
        float: left;
        margin-right: 20px;
    }
    .nome-evento{
        float: left;
        color: #666;
    }
    .nome-evento h3{
        margin-top: 5px;
    }
    .nome-evento h3::first-line{
        color: #333;
        font-weight: 600;
        font-size: 30px;
    }

</style>


    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700" rel="stylesheet">

    <!-- BEGIN LOGO -->
    <div class="page-logo">
        
        <?php
            $filename = "{base_url}files/evento/{EventoControle}/logotipo/$Logotipo";
            if (is_null($Logotipo) == true || !file_exists($filename)){
                $filename = "{base_url}assets/sigepe/Global/Images/Evento/no-photo.png";
            }
        ?>

        <img src="<?php echo $filename; ?>" style="width: 150px;display: inline-block;float: left;margin-right: 20px;border: none; ">

        
    </div>
    <!-- END LOGO -->

    <div class="col-sm-10 nome-evento">
      <small class="badge badge-primary bold">{StatusdoEvento}</small>
      <h3><?php echo nl2br(htmlentities($NomeEvento, ENT_QUOTES, 'UTF-8'));; ?></h3>
    </div>


    <div class="container" style="margin-bottom: 20px;">
      <div class="row">
        <div class="col-sm-12">
          <hr class="separador-inscricao" style="margin-top: 0px !important; margin-bottom: 0px !important; border: 1px dotted #eee !important;">
          <hr class="separador-inscricao" style="margin-top: 0px !important; margin-bottom: 0px !important; border: 1px dotted #eee !important;">
          <hr class="separador-inscricao" style="margin-top: 0px !important; margin-bottom: 0px !important; border: 1px dotted #eee !important;">
          <hr class="separador-inscricao" style="margin-top: 0px !important; margin-bottom: 0px !important; border: 1px dotted #eee !important;">
          <hr class="separador-inscricao" style="margin-top: 0px !important; margin-bottom: 0px !important; border: 1px dotted #eee !important;">
        </div>
      </div>
    </div>

