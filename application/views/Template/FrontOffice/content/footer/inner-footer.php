                    <!-- BEGIN INNER FOOTER -->
                    <div class="page-footer">
                        <div class="container-fluid"> <?php echo date("Y"); ?> &copy; Sigepe - Sistema Integrado para Gestão de Provas Equestres. Licenciado para FHBr.

                        <span style="float: right;">Página carregada em <strong>{elapsed_time}</strong> segundos.</span>
                            <!-- <a target="_blank" href="http://keenthemes.com">Keenthemes</a> &nbsp;|&nbsp; -->
                            <!-- <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a> -->
                        </div>
                    </div>
                    <div class="scroll-to-top">
                        <i class="icon-arrow-up"></i>
                    </div>
                    <!-- END INNER FOOTER -->
