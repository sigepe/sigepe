


    <!-- 
    MEU PERFIL
    ========================= -->
    <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown <?php echo ($this->uri->segment(2) == 'Perfil') ? 'active': ''; ?>">
        <a href="{base_url}FrontOffice/Perfil/Dashboard">
            <i class="fa fa-user"></i> Meu Perfil
            <span class="arrow"></span>
        </a>
        <?php /*
        <a href="javascript:;">
            <i class="fa fa-user"></i> Meu Perfil
            <span class="arrow"></span>
        </a>
        <ul class="dropdown-menu pull-left">
            <li aria-haspopup="true" class=" ">
                <a href="#" class="nav-link  "> Gerenciar Perfil </a>
            </li>
            <li aria-haspopup="true" class="dropdown-submenu ">
                <a href="javascript:;" class="nav-link nav-toggle ">
                    Perfis
                    <span class="arrow"></span>
                </a>
                <ul class="dropdown-menu">
                    <li aria-haspopup="true" class=" "> <a href="#" class="nav-link "> Atleta </a> </li>
                    <li aria-haspopup="true" class=" "> <a href="#" class="nav-link "> Desenhador de Percurso (Armador) </a> </li>
                    <li aria-haspopup="true" class=" "> <a href="#" class="nav-link "> Juiz </a> </li>
                    <li aria-haspopup="true" class=" "> <a href="#" class="nav-link "> Juiz Externo </a> </li>
                    <li aria-haspopup="true" class=" "> <a href="#" class="nav-link "> Delegado Técnico </a> </li>
                    <li aria-haspopup="true" class=" "> <a href="#" class="nav-link "> Comissário </a> </li>
                    <li aria-haspopup="true" class=" "> <a href="#" class="nav-link "> Veterinário </a> </li>
                    <li aria-haspopup="true" class=" "> <a href="#" class="nav-link "> Desenhador </a> </li>
                    <li aria-haspopup="true" class=" "> <a href="#" class="nav-link "> Instrutor </a> </li>
                    <li aria-haspopup="true" class=" "> <a href="#" class="nav-link "> Locutor </a> </li>
                </ul>
            </li>

<!-- 
            <li aria-haspopup="true" class=" ">
                <a href="#" class="nav-link  "> Dependentes </a>
            </li>
            <li aria-haspopup="true" class=" ">
                <a href="#" class="nav-link  "> Avisos </a>
            </li>
-->                                                
            <li aria-haspopup="true" class=" ">
                <a href="#" class="nav-link  "> Dúvidas? </a>
            </li>
        </ul>
        */
        ?>
    </li>
