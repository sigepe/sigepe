<!-- Variable Environment Javascripts
============================================= -->
<script type="text/javascript">
var slug 		         			=	''; // ???

/* Template */
var BaseUrl	 						=	'{base_url}';
var userId        					=	'{userId22}';
var peopleId      					=	'{peopleId}';
var peopleFirstName 				= 	'{peopleFirstName}';
var templateClass					=	'<?php echo (isset($EnvironmentClass)) ? $EnvironmentClass : ""; ?>';

/* General Settings Module */
var moduleSlug						=	'<?php echo (isset($moduleSlug)) ? $moduleSlug : ""; ?>';
var module							=	'<?php echo (isset($module)) ? $module : ""; ?>';
var FormIdentification     			= 	'<?php echo (isset($FormIdentification)) ? $FormIdentification : ""; ?>';

/* Environment Default Template Class */
var EnvironmentModule				=	'<?php echo (isset($EnvironmentModule)) ? $EnvironmentModule : ""; ?>';
var EnvironmentModuleSlug			=	'<?php echo (isset($EnvironmentModuleSlug)) ? $EnvironmentModuleSlug : ""; ?>';
var EnvironmentClass				=	'<?php echo (isset($EnvironmentClass)) ? $EnvironmentClass : ""; ?>';
var EnvironmentMethod				=	'<?php echo (isset($EnvironmentMethod)) ? $EnvironmentMethod : ""; ?>';
var EnvironmentOffice				=	'<?php echo (isset($EnvironmentOffice)) ? $EnvironmentOffice : ""; ?>';
var EnvironmentOfficeSlug			=	'<?php echo (isset($EnvironmentOfficeSlug)) ? $EnvironmentOfficeSlug : ""; ?>';

/* Profile */
var PeopleId						=	'<?php echo (isset($PeopleId)) ? $PeopleId : ""; ?>';
var UserId							=	'<?php echo (isset($UserId)) ? $UserId : ""; ?>';
var AthleteId						=	'<?php echo (isset($AthleteId)) ? $AthleteId : ""; ?>';
var OwnerId							=	'<?php echo (isset($OwnerId)) ? $OwnerId : ""; ?>';





<?php if(isset($DatasetPagarmeCheckout)): ?>
// Carregando dados a serem usados na transacao com gateway
var PagarmeExternalId               =   '<?php echo $PessoaId; ?>'; // Autor
var PagarmeNome                     =   '<?php echo $PagarmeNome; ?>'; //
var PagarmeCpf                      =   '<?php echo $PagarmeCpf; ?>'; // 
var PagarmeEmail                    =   '<?php echo $PagarmeEmail; ?>'; //
var PagarmeTelefonePrincipal        =   '<?php echo $PagarmeTelefonePrincipal; ?>'; // // just numbers
var PagarmeDataNascimento           =   '<?php echo $PagarmeDataNascimento; ?>'; // iso
var PagarmeEstadoSigla              =   '<?php echo (!isset($PagarmeEstadoSigla)) ? '-' : $PagarmeEstadoSigla; ?>'; //
var PagarmeCidade                   =   '<?php echo (!isset($PagarmeCidade)) ? '-' : $PagarmeCidade; ?>'; //
var PagarmeBairro                   =   '<?php echo (!isset($PagarmeBairro)) ? '-' : $PagarmeBairro; ?>'; //
var PagarmeLogradouro               =   '<?php echo (!isset($PagarmeLogradouro)) ? '-' : $PagarmeLogradouro; ?>'; //
var PagarmeNumero                   =   '<?php echo (!isset($PagarmeNumero)) ? '-' : $PagarmeNumero; ?>'; //
var PagarmeCep                      =   '<?php echo (!isset($PagarmeCep)) ? '-' : $PagarmeCep; ?>'; //
<?php endif; ?>





</script>




<?php if(isset($DatasetPagarmeCheckout)): ?>
<script src="https://assets.pagar.me/pagarme-js/3.0/pagarme.min.js"></script>
<script src="https://assets.pagar.me/checkout/1.1.0/checkout.js"></script>
<?php endif; ?>
