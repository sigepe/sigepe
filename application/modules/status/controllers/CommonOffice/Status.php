<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Status extends MY_Controller {

    public $tabelaNome = "tb_status";
    public $columnId = "sta_id";
    public $columnDesc = "sta_status";

    function __construct() {
        parent::__construct();
        $this->load->model('model_crud');
    }

    public function index(){
        //var_dump($this->listar());
        var_dump($this->obter(1));
        var_dump($this->obterEntre(410, 413));
    }

    public function listar(){
        return $this->db->query("SELECT * from $this->tabelaNome order by $this->columnDesc")->result();
    }

    public function obter($id){
        return $this->model_crud->get_rowSpecificObject($this->tabelaNome, $this->columnId, $id);
    }

    public function obterEntre($idInicial, $idFinal){
        return $this->db->query("SELECT * from $this->tabelaNome where $this->columnId between $idInicial and $idFinal order by $this->columnDesc")->result();   
    }

    public function obterContido($tag){
        return $this->db->query("SELECT * FROM $this->tabelaNome where $this->$columnDesc like '%$tag%' order by $this->columnDesc asc");
    }
}