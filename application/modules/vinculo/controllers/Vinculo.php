<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_GuestOffice.php';
class Vinculo extends MY_GuestOffice {

    public $data;

    function __construct() {
        
        parent::__construct();

    }



    


    /**
    * Atualizar
    *
    * @author Gustavo Botega 
    */
    public function Atualizar( $Dados = NULL  ) {

      if(is_null($Dados) || !is_array($Dados))
        return false;


        /* Dados */
        $Dataset       =  array(
          'fk_sta_id'                         =>    $Dados['fk_sta_id'],
          'modificado'                        =>    date("Y-m-d H:i:s")
        );

        $Query    =   $this->db->update('tb_vinculo', $Dataset, array('vin_id' => $Dados['vin_id']));
    
        if($Query):
        
            $GravarHistorico    =   $this->GravarHistorico(NULL, $Dados);
            return ($GravarHistorico) ? true : false;
        
        endif;

        return false;
        
    }






    /**
    * Gravar
    *
    * @author Gustavo Botega 
    */
    public function Gravar( $Dados = NULL  ) {

      if(is_null($Dados) || !is_array($Dados))
        return false;

        
      /* Dados */
      $Dataset       =  array(
          'fk_aut_id'                         =>    $Dados['fk_aut_id'],
          'fk_pes1_id'                        =>    $Dados['fk_pes1_id'],
          'fk_pes2_id'                        =>    $Dados['fk_pes2_id'],
          'fk_ani_id'                         =>    $Dados['fk_ani_id'],
          'fk_per_id'                         =>    $Dados['fk_per_id'],
          'fk_tip_id'                         =>    $Dados['fk_tip_id'],
          'fk_sta_id'                         =>    $Dados['fk_sta_id'],
          'criado'                            =>    date("Y-m-d H:i:s")
      );


      /* Query */
      $Query = $this->db->insert('tb_vinculo', $Dataset);

      if($Query):
        
        $GravarHistorico    =   $this->GravarHistorico($this->db->insert_id(), $Dados);
        return ($GravarHistorico) ? true : false;

      endif;

      return false;

    }


    /**
    * GravarHistorico
    *
    * @author Gustavo Botega 
    */
    public function GravarHistorico($VinculoId = NULL, $Dados) {

        if(!isset($Dados['vih_historico']))
            $Dados['vih_historico']   = NULL;
        
        if(!isset($Dados['fk_aut_id']))
            $Dados['fk_aut_id']   = $this->session->userdata('PessoaId');
        
        if(is_null($VinculoId))
            $VinculoId   = $Dados['vin_id'];
        
        

        /* Dados */
        $Dataset       =  array(
            'fk_aut_id'                         =>    $Dados['fk_aut_id'],
            'fk_vin_id'                         =>    $VinculoId,
            'fk_sta_id'                         =>    $Dados['fk_sta_id'],
            'vih_historico'                     =>    $Dados['vih_historico'],
            'criado'                            =>    date("Y-m-d H:i:s")
        );


        /* Query */
        $Query = $this->db->insert('tb_vinculo_historico', $Dataset);

        return ($Query) ? true : false;

    }




    /**
    * GravarPerfilAtleta
    *
    * @author Gustavo Botega 
    */
    public function GravarPerfilAtleta($PessoaId){  

        $AutorId     =   $this->session->userdata('PessoaId');
        if(is_null($this->session->userdata('PessoaId')))
            $AutorId    =   $PessoaId;
        


        $Dataset =    array(
          'fk_aut_id'                         =>    $AutorId, 
          'fk_pes1_id'                        =>    $PessoaId,
          'fk_pes2_id'                        =>    NULL,
          'fk_ani_id'                         =>    NULL,
          'fk_per_id'                         =>    5,
          'fk_tip_id'                         =>    NULL,
          'fk_sta_id'                         =>    1
        );

        $Gravar = $this->Gravar($Dataset);

        return ($Gravar) ? true : false;

    } 


    /**
    * GravarVinculoAtletaConfederacao
    *
    * @author Gustavo Botega 
    */
    public function GravarVinculoAtletaConfederacao($Dados, $PessoaId){
        
        return $this->GravarVinculoCompetidorEmpresa($Dados, $PessoaId, NULL, 2);

    } 


    /**
    * GravarVinculoAtletaFederacao
    *
    * @author Gustavo Botega 
    */
    public function GravarVinculoAtletaFederacao($Dados, $PessoaId){
        
        return $this->GravarVinculoCompetidorEmpresa($Dados, $PessoaId, NULL, 3);

    } 


    /**
    * GravarVinculoAtletaEntidade
    *
    * @author Gustavo Botega 
    */
    public function GravarVinculoAtletaEntidade($Dados, $PessoaId){
        
        return $this->GravarVinculoCompetidorEmpresa($Dados, $PessoaId, NULL, 4);

    } 


    /**
    * GravarVinculoAnimalConfederacao
    *
    * @author Gustavo Botega 
    */
    public function GravarVinculoAnimalConfederacao($Dados, $AnimalId){
        
        return $this->GravarVinculoCompetidorEmpresa($Dados, NULL, $AnimalId, 2);

    } 


    /**
    * GravarVinculoAnimalFederacao
    *
    * @author Gustavo Botega 
    */
    public function GravarVinculoAnimalFederacao($Dados, $AnimalId){
        
        return $this->GravarVinculoCompetidorEmpresa($Dados, NULL, $AnimalId, 3);

    } 


    /**
    * GravarVinculoAtletaEntidade
    *
    * @author Gustavo Botega 
    */
    public function GravarVinculoAnimalEntidade($Dados, $AnimalId){
        
        return $this->GravarVinculoCompetidorEmpresa($Dados, NULL, $AnimalId, 4);

    } 


    /**
     * GravarVinculoAtletaEmpresa
     *
     * Competidor pode ser um atleta ou um animal
     *
     * @author Gustavo Botega 
     * @return boolean
     */
    public function GravarVinculoCompetidorEmpresa($Dados, $AtletaId = NULL, $AnimalId = NULL, $PerfilEmpresa){


        /* Atleta */
        if(!is_null($AtletaId)):

            $TipoVinculo        =   9; // Atleta vinculado a empresa

            ($PerfilEmpresa == '2') ? $StatusId = 3 : ''; // [ Vínculos ] Atleta aguardando aprovação do vínculo pela Confederação.
            ($PerfilEmpresa == '3') ? $StatusId = 12 : ''; // [ Vinculos ] Atleta aguardando aprovação de vínculo pela Federação.
            ($PerfilEmpresa == '4') ? $StatusId = 20 : ''; // [ Vinculos ] Atleta aguardando aprovação de vínculo pela Entidade Equestre.

        endif;


        /* Animal */
        if(!is_null($AnimalId)):

            $TipoVinculo        =   10; // Animal vinculado a empresa

            ($PerfilEmpresa == '2') ? $StatusId = 7 : ''; // [ Vínculos ] Animal aguardando aprovação de vínculo pela Confederação
            ($PerfilEmpresa == '3') ? $StatusId = 16 : ''; // [ Vinculos ] Atleta aguardando aprovação de vínculo pela Federação.
            ($PerfilEmpresa == '4') ? $StatusId = 24 : ''; // [ Vinculos ] Atleta aguardando aprovação de vínculo pela Entidade Equestre.

        endif;



        /*
            Hack para gravar o vinculo. 
            Na tabela tb_vinculo e obrigatorio o campo fk_aut_id.
            Para casos em que o usuario esta entrando no sistema, nao existe ainda sessao ativa.
            Logo nao existe o objeto userdata('PessoaId'). Nessas situacoes o autor assume o valor de PessoaId.
                
        */
        $AutorId        =   $this->session->userdata('PessoaId');
        if(is_null($AutorId) || empty($AutorId))
            $AutorId    =   $Dados['PessoaId']; 
        
        
        
        
        
        /* Perfil Empresa */
        switch ($PerfilEmpresa):

            case '2':
                $EmpresaId      =   '140'; // ID CBH
                break;
            case '3':
                $EmpresaId      =   $Dados['federacao']; // ID Federacao
                break;
            case '4':
                $EmpresaId      =   $Dados['entidade-equestre']; // ID Entidade Equestre
                break;
            
        endswitch;


        /* Dataset */
        if(!is_null($AtletaId)):
        $Dataset =    array(
          'fk_aut_id'                         =>    $AutorId,
          'fk_pes1_id'                        =>    $AtletaId, // ID Atleta /ID Animal
          'fk_pes2_id'                        =>    $EmpresaId, //
          'fk_ani_id'                         =>    NULL, // 
          'fk_per_id'                         =>    $PerfilEmpresa, // 
          'fk_tip_id'                         =>    $TipoVinculo, // Atleta vinculado a empresa
          'fk_sta_id'                         =>    $StatusId
         );
        endif;

        /* Dataset */
        if(!is_null($AnimalId)):
        $Dataset =    array(
          'fk_aut_id'                         =>    $AutorId,
          'fk_pes1_id'                        =>    $EmpresaId, // ID Atleta /ID Animal
          'fk_pes2_id'                        =>    NULL, //
          'fk_ani_id'                         =>    $AnimalId, // 
          'fk_per_id'                         =>    $PerfilEmpresa, // 
          'fk_tip_id'                         =>    $TipoVinculo, // Atleta vinculado a empresa
          'fk_sta_id'                         =>    $StatusId
         );
        endif;


        $GravarVinculo = $this->Gravar($Dataset);

        
        
        
        return ($GravarVinculo) ? true : false;

    }


    /**
    * GravarResponsavelFinanceiroCompetidor
    *
    * @author Gustavo Botega 
    */
    public function GravarResponsavelFinanceiroCompetidor($PessoaId = NULL, $AnimalId = NULL, $ResponsavelFinanceiroId){


        /* ATLETA - P1 é responsável financeiro por P2. P2 Deve ser o id da pessoa (atleta). */
        if(!is_null($PessoaId)){
            $TipoVinculo    =   5; // Responsável Financeiro por Atleta             
            $Pes1           =   $ResponsavelFinanceiroId;
            $Pes2           =   $PessoaId;
        }


        /* ANIMAL - P1 é responsável financeiro pelo Animal. P2 deve ser NULL. fk_ani_id deve ser obrigatoriamente preenchido. */
        if(!is_null($AnimalId)){
            $TipoVinculo    =   7; // Responsável Financeiro por Animal            
            $Pes1           =   $ResponsavelFinanceiroId;
            $Pes2           =   NULL;
        }


        /* Dataset */
        $Dataset =    array(
          'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
          'fk_pes1_id'                        =>    $Pes1, // ID Atleta
          'fk_pes2_id'                        =>    $Pes2, // ID Empresa Vinculada
          'fk_ani_id'                         =>    $AnimalId, // 
          'fk_per_id'                         =>    NULL, // 
          'fk_tip_id'                         =>    $TipoVinculo, // Atleta vinculado a empresa
          'fk_sta_id'                         =>    1
         );


        $Gravar = $this->Gravar($Dataset);

        return $Gravar;


    } 


    /**
    * GravarVinculoProprietarioAnimal
    *
    * @author Gustavo Botega 
    */
    public function GravarVinculoProprietarioAnimal($ProprietarioId = NULL, $AnimalId = NULL){

        if(is_null($ProprietarioId) || is_null($AnimalId))
            return false;


        /* Dataset */
        $Dataset =    array(
          'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
          'fk_pes1_id'                        =>    $ProprietarioId, // ID da pessoa proprietaria do animal. Referencia: tb_pessoa
          'fk_pes2_id'                        =>    NULL, // ID Empresa Vinculada
          'fk_ani_id'                         =>    $AnimalId, // 
          'fk_per_id'                         =>    NULL, // 
          'fk_tip_id'                         =>    8, // Proprietário de Animal 
          'fk_sta_id'                         =>    1
         );


        $Gravar = $this->Gravar($Dataset);

        return $Gravar;

    } 






}
