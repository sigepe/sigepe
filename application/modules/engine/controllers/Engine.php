<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';
class Engine extends MY_FrontOFfice {

    function __construct() {
        parent::__construct();

        /*
        1) Carregar modulo
        */

    }

    public function index() {
		$this->gear();    	
    }

    public function teste(){


        $this->loadTemplateUser('template/user/main');
    }

    public function dashboard(){

        /*PEGAR A HORA LOCAL DA PESSOA. QUEM ACESSAR POR NY VAI TER UM HORARIO DIFERENTE DE QUEM ACESSAR POR BR */                    
        $Settings['PageHeadTitle']      = 'Bom dia Gustavo, tudo bem? '; /* */
        $Settings['PageHeadSubtitle']   = 'Seu último acesso foi às 18h40 em 16/04/2017.';


        $this->loadTemplateUser('template/user/tiles', $Settings);
    }

    public function invoice(){


        $this->loadTemplateUser('template/user/invoice');
    }

    public function empresas(){


        $this->loadTemplateUser('template/user/empresas');
    }


    public function EventosPorInscricao(){


        $this->loadTemplateUser('template/user/sistema/evento/eventos-inscritos');
    }

    public function InscricoesPorFatura(){


        $this->loadTemplateUser('template/user/sistema/evento/inscricoes-fatura');
    }
    public function InscricoesPorEvento(){


        $this->loadTemplateUser('template/user/sistema/evento/inscricoes');
    }

    public function InscricoesPorEvento2(){


        $this->loadTemplateUser('template/user/sistema/evento/inscricoe2');
    }

    public function dashboardEvento(){


        $this->loadTemplateUser('template/user/sistema/evento/dashboard');
    }

    public function financeiroEvento(){


        $this->loadTemplateUser('template/user/sistema/evento/financeiro');
    }


    public function PedidoDeCompra(){


        $this->loadTemplateUser('template/user/PedidoDeCompra');
    }

    public function MeuPerfil(){


        $this->LoadTemplateProfile('template/user/sistema/pessoa/fisica/modulos/meu-perfil/dashboard');
    }

    public function MeuPerfilEmpresa(){


        $this->LoadTemplateProfile('template/user/sistema/pessoa/juridica/modulos/meu-perfil/dashboard');
    }

    public function teste2(){


        $this->loadTemplateAdmin('template/admin/main');
    }


    public function blankPage(){


        $this->loadBlankPage('');
    }

    /***********************************************    
      GEAR
    ***********************************************/
    public function gear($module=NULL, $action=NULL, $value=NULL) {
        // Action se for nula = exibir e cadastrar. Recebe ainda edit e delete. Se houver action value deve ser obrigatório
        // Tratar rotas para ficar assim
        // http://localhost/engine/Cengine/gear/module -> http://localhost/engine/{module}/
    	if(!is_null($module)){

            // Carregando modulo
			$this->load->module($module);
            
            switch ($action) {

                case 'retrieve':
                    $settings   = $this->$module->settings();
                    $crud       = $this->$module->retrieve();
                    $loadPackagesFrontend  = $this->loadPackagesFrontend('retrieve');
                    $crudOperation         = array('crudOperation'=>'retrieve');
                    $external   = array_merge($loadPackagesFrontend, $crudOperation);
                    $settings['retrieve']   =   'TRUE';
                    $this->loadtemplate('template/crud-retrieve', $settings, $crud, $external);
                    break;
    
                case 'create':
                    $settings   = $this->$module->settings();
                    $crud       = $this->$module->create();
                    $loadPackagesFrontend  = $this->loadPackagesFrontend('create');
                    $crudOperation         = array('crudOperation'=>'create');
                    $external   = array_merge($loadPackagesFrontend, $crudOperation);
                    $settings['create']   =   'TRUE';
                    $this->loadtemplate('template/crud/create/crud-create', $settings, $crud, $external);
                    break;
                                
                case 'update':
                    $settings   = $this->$module->settings();
                    $crud       = $this->$module->update();
                    $loadPackagesFrontend  = $this->loadPackagesFrontend('update');
                    $crudOperation         = array('crudOperation'=>'update');
                    $external   = array_merge($loadPackagesFrontend, $crudOperation);
                    $settings['update']   =   'TRUE';
                    $this->loadtemplate(
                        array(
                            'template/crud/update/portlet-begin',
                            'template/crud/update/portlet-tabsTitle',
                            'template/crud/update/portlet-beginBody',
                            'template/crud/update/portlet-tabManager',
                            'template/crud/update/portlet-tabThumb',
                            'template/crud/update/portlet-tabGallery',
                            'template/crud/update/portlet-tabMaps',
                            'template/crud/update/portlet-tabExtension',
                            'template/crud/update/portlet-endBody',
                            'template/crud/update/portlet-end'
                        ), 
                        $settings, $crud, $external
                    );
                    break;

                case 'delete':
                    if(!is_null($value)){
                        echo "certinho - parametro de deletar eh" . $value;
                        // executar bloco
                        // checar regras de delecao de item
                        // checar caso quando deletar uma galeria deletar todas as fotos
                        // $this->destroy($value);
                    }else{
                        echo "[ --- ERRO --- ] <br /> Nao recebido $value. Parametro obrigatorio para o \$action solicitado.";
                    }
                    break;
                
	    		default:
                    $settings   = $this->$module->settings();
                    $crud       = $this->$module->retrieve();
                    $loadPackagesFrontend  = $this->loadPackagesFrontend('retrieve');
                    $crudOperation         = array('crudOperation'=>'retrieve');
                    $external   = array_merge($loadPackagesFrontend, $crudOperation);
                    $settings['retrieve']   =   'TRUE';
                    $this->loadtemplate('template/crud-retrieve', $settings, $crud, $external);
	    			break;
	    	}
    	}else{
    		echo "[ INFORME UMA URL VALIDA PARA ACESSAR O PROTOTIPO ITERATIVO ]";
    	}

    }



    public function loadPackagesFrontend($slugCrud){

        switch ($slugCrud) {

            case 'create':
                $data = array(
                    'use_datatable'    =>  FALSE,
                    'use_fancybox'     =>  TRUE,
                    'use_modal'        =>  TRUE,
                    'use_toastr'       =>  TRUE,
                    'use_datepicker'   =>  TRUE,
                    'use_dropdown'     =>  TRUE,
                    'use_cft'          =>  TRUE,
                    'use_jmask'        =>  TRUE,
                    'use_slug'         =>  TRUE,
                );
                return $data;   
                break;

            case 'retrieve':
                $data = array(
                    'use_datatable'    =>  TRUE,
                    'use_fancybox'     =>  TRUE,
                    'use_modal'        =>  TRUE,
                    'use_toastr'       =>  TRUE,
                    'use_cft'          =>  TRUE,
                );
                return $data;   
                break;

            case 'update':
                $data = array(
                    #'use_datatable'     =>  TRUE,
                    'use_fancybox'      =>  TRUE,
                    'use_modal'         =>  TRUE,
                    'use_toastr'        =>  TRUE,
                    'use_datepicker'    =>  TRUE,
                    'use_dropdown'      =>  TRUE,
                    'use_cft'           =>  TRUE,
                    'use_slug'          =>  TRUE,
                    'use_jfum'          =>  TRUE,
                    'use_jmask'         =>  TRUE,
                    'use_jcrop'         =>  TRUE,
                    'use_portfolio'     =>  TRUE,
                );
                return $data;   
                break;

            default:
                $data = array(
                    'use_datatable'    =>  TRUE,
                    'use_fancybox'     =>  TRUE,
                    'use_modal'        =>  TRUE,
                    'use_toastr'       =>  TRUE,
                    'use_datepicker'   =>  TRUE,
                    'use_dropdown'     =>  TRUE,
                    'use_cft'          =>  TRUE,
                );
                return $data;   
                break;

        }
    }



    /**
     * Recebe uma interacao via Ajax. Metodo POST. 
     *
     * @author Gustavo Botega 
     * @param int $valueReference,... unlimited OPTIONAL number of additional variables to display with var_dump()
     * @return int|string could be an int, could be a string
     */
    public function delete() {

        /* Setting variables this escope */
        $moduleSlug                 =   $_POST['data']['moduleSlug'];
        $databaseTable              =   $_POST['data']['databaseTable'];
        $databaseColumnPk           =   $_POST['data']['databaseColumnPk'];
        $valueReference             =   $_POST['data']['valueReference'];
        $tabThumb                   =   $_POST['data']['tabThumb'];
        $databaseColumnThumb        =   $_POST['data']['databaseColumnThumb'];
        $pathThumb                  =   $_POST['data']['pathThumb'];
        $userId                     =   $_POST['data']['userId'];
        $databaseModuleColumnFolder =   $_POST['data']['databaseModuleColumnFolder'];
/*
        var_dump($moduleSlug);
        var_dump($databaseTable);
        var_dump($databaseColumnPk);
        var_dump($valueReference);
        var_dump($tabThumb);
        var_dump($databaseColumnThumb);
        var_dump($pathThumb);
        var_dump($userId);
        var_dump($databaseModuleColumnFolder);*/

        // Checa se é um modulo que tem Galeria. Se tiver mover a pasta para trash. 
        $this->load->module($moduleSlug);
        $settings               = $this->$moduleSlug->settings();
        $tabGallery             = $settings['tabGallery'];

        /* Variables of Gallery */
        $databaseTableGallery           =   $settings['databaseTableGallery']; 
        $databaseTableGalleryColumnFk   =   $settings['databaseTableGalleryColumnFk'];

        /* Paths */ 
        if($tabGallery){
            $pathGallery        = $settings['pathGallery'];
            $databaseFolder     = $this->model_crud->get_rowSpecific($databaseTable, $databaseColumnPk, $valueReference, 1, $databaseModuleColumnFolder);  
        }



        /* vem primeiro por conta da FK */
        if ($tabGallery) {

            if(!is_null($databaseFolder)){

                // Deletar Fotos do banco
                $this->model_crud->delete($databaseTableGallery, $databaseTableGalleryColumnFk , $valueReference);

                // Movendo a pasta para o lixo (trash)
                $filenameCurrentFolderGallery = FCPATH . $pathGallery . $databaseFolder  . '/';            
                $filenameNewFolderGallery = FCPATH . 'trash/' . $databaseFolder . '/';            
@                   $rename = rename($filenameCurrentFolderGallery, $filenameNewFolderGallery);
                if(!$rename):
                    // @@@ se retornar falso gravar em arquivo de log de erro
                endif;
            }else{
                // nada acontece porque nao existe diretorio. fluxo continua
            }

        } 





        // Checking if $tabThumb is true, if YES delete record database and file, elseif just data
        if($tabThumb)
        {
            $query = $this->model_crud->deleteDataAndFile($databaseTable, $databaseColumnPk, $valueReference, $databaseColumnThumb, $pathThumb);
        }
        else{
            $query = $this->model_crud->delete($databaseTable, $databaseColumnPk, $valueReference);
        }



        // checking if query is successfully
        if($query)
        {


            $array = array(
                'status'    =>  'processed',
                'datetime'  =>  date("Y-m-d H:i:s"),
                'ip'        =>  $this->input->ip_address()
            );
        }
        else
        {
            $array = array(
                'status'    =>  'unprocessed'
            );
        }
        echo json_encode($array);

    }
 



    /**
     * Funcao Trocar Senha   
     *
     *
     * @author Gustavo Botega 
     * @param string $database -   
     * @param int $columnId -    
     * @return string Json Encode 
     */      
    public function changePassword(){


        /**
         * Instanciando modulo. O modulo instanciado eh recebido por POST.
         * $settings herda os valores do metodo settings() do modulo instanciado
         */
        $this->load->module('authentication');
        $settings               = $this->authentication->settings();


        /**
         * Resgatando valores
         */
        $passwordCurrent          =   $_POST['passwordCurrent'];
        $passwordNew              =   $_POST['passwordNew'];


        /**
         * Setando variaveis de usuario e pessoa
         */
        $userId                   =   $this->session->userdata('userId');
        $peopleId                 =   $this->session->userdata('peopleId');


        /**
         * Criptografando senhas
         */
        $passwordCurrent          =   md5($settings['token'] . $passwordCurrent);
        $passwordNew              =   md5($settings['token'] . $passwordNew);


        /**
         * Query - People ID e User ID
         */
        $checkCredentials = $this->model_crud->select(
            'tb_usuario',
            array('fk_pes_id', 'usu_id', 'usu_senha'),
            array('fk_pes_id =' => $peopleId, 'usu_id =' => $userId),
            NULL,
            NULL
        ); 


        if($checkCredentials)
        {


            if($checkCredentials[0]->usu_senha != $passwordCurrent):
                $array = array(
                    'status'            =>  'processed',
                    'passwordStatus'    =>  'passwordWrong',
                    'datetime'          =>  date("Y-m-d H:i:s"),
                    'ip'                =>  $this->input->ip_address()
                );
            endif;

            if($checkCredentials[0]->usu_senha == $passwordCurrent):

                $arrDataSet = array(
                    'usu_senha'=>$passwordNew,
                    'modificado'=>date("Y-m-d H:i:s")
                );

                /* Setando no Banco */
                $this->db->where('usu_id', $userId);
                $query = $this->db->update('tb_usuario', $arrDataSet); 
                $str = $this->db->last_query();

                if(!$query):
                    $array = array(
                        'status'            =>  'processed',
                        'passwordStatus'    =>  'passwordSetFail',
                        'datetime'          =>  date("Y-m-d H:i:s"),
                        'ip'                =>  $this->input->ip_address()
                    );
                endif;

                if($query):
                    $array = array(
                        'status'            =>  'processed',
                        'passwordStatus'    =>  'passwordSure',
                        'datetime'          =>  date("Y-m-d H:i:s"),
                        'ip'                =>  $this->input->ip_address()
                    );
                endif;

            endif;

        }
        else
        {
            $array = array(
                'status'    =>  'unprocessed'
            );
        }        
        echo json_encode($array);

    }






    /**
     * Funcao recebe uma requisicao Ajax.  
     * Atencao: a funcao nao recebe nenhum valor, porem sao resgatados por $_POST
     *
     * Falta implementar recursos de validação backend e auditoria 
     *
     * @author Gustavo Botega 
     * @param $_POST['moduleSlug'] -   
     * @param $_POST['arrayData'] -   
     * @param $_POST['tabGallery'] -  NO NEED! REMOVE this js  
     * @param $_POST['formIdentification'] -    
     * @return string JSON
     */      
    public function record(){


        /**
         * Instanciando modulo. O modulo instanciado eh recebido por POST.
         * $settings herda os valores do metodo settings() do modulo instanciado
         */
        $moduleSlug             =   $_POST['moduleSlug'];
        $this->load->module($moduleSlug);
        $settings               = $this->$moduleSlug->settings();


        /**
         * Documentar
         */
        $tabGallery     = array($_POST['tabGallery']);
        if ($tabGallery[0]=='TRUE') {
            $tabGallery = TRUE;
            $databaseModuleColumnFolder   =   $settings['databaseModuleColumnFolder'];
            $columnFolder2          =   array($settings['databaseModuleColumnFolder']=>date('YmdHis'));
        } else {
            $tabGallery = FALSE;
            $columnFolder2   =   array();
        }


        /**
         * Documentar - Unserialize Data ( Convert string to Arr Object PHP in $searchArray )
         */
        $arrayData      = $_POST['arrayData'];
        parse_str($arrayData, $searchArray);


        /**
         * Documentar - Increment variables
         */
        $columnFolder   = $columnFolder2;
        $criado         = array('criado'    =>  date("Y-m-d H:i:s"));
        

        /**
         * Documentar - Merge
         */
        $merge          = array_merge($searchArray, $criado, $columnFolder);
            

        /**
         * Documentar - [Reescrevendo o array - caso boostrap switch ]
         */
        foreach ($merge as $key => $value) {
           
            /* bootstrap-switch */
            if($value=='on'){
                $merge[$key]    = TRUE;
            }
            /* EMPTY */
            if($value=='' || empty($value)){
                $merge[$key]    = NULL;
            }
            if($value=='bootstrapSwitchEmpty'){
                $merge[$key]    = NULL;
            }
        }

        $merge['fk_aut_id']     =   $this->session->userdata('peopleId');
        $merge['criado']        =   date("Y-m-d H:i:s");


        /**
         * Documentar - $query
         */
        $query = $this->db->insert($settings['databaseTable'], $merge);

        /**
         * Documentar - $query
         */
        if($query)
        {
            if($tabGallery)
                $newFolder              =   mkdir( $settings['pathGallery'] . $columnFolder2[$settings['databaseModuleColumnFolder']] );

            $array = array(
                'status'    =>  'processed',
                'datetime'  =>  date("Y-m-d H:i:s"),
                'ip'        =>  $this->input->ip_address()
            );
        }
        else
        {
            $array = array(
                'status'    =>  'unprocessed'
            );
        }        


        /**
         * Documentar - saida
         */
        echo json_encode($array);

    }




 



    /**
     * Funcao Update   
     *
     *
     * @author Gustavo Botega 
     * @param string $database -   
     * @param int $columnId -    
     * @return string Json Encode 
     */      
    public function update(){


        /**
         * Instanciando modulo. O modulo instanciado eh recebido por POST.
         * $settings herda os valores do metodo settings() do modulo instanciado
         */
        $moduleSlug             =   $_POST['moduleSlug'];
        $this->load->module($moduleSlug);
        $settings               = $this->$moduleSlug->settings();


        /**
         * Setando as variaveis que serao usadas no escopo da funcao
         */
        $databaseTable          =   $settings['databaseTable'];
        $databaseColumnPk       =   $settings['databaseColumnPk'];
        $databaseItemCurrent    =   $_POST['databaseItemCurrent'];


        /**
         * Unserialize Data - Convert string to Arr Object PHP in $searchArray
         */
        $arrayData      = $_POST['arrayData'];
        parse_str($arrayData, $searchArray);


        /**
         * New variables to increment on array data
         */
        $modificado     = array('modificado'    =>  date("Y-m-d H:i:s"));
        

        /**
         * Merge 
         */
        $merge          = array_merge($searchArray, $modificado);
        

        /**
         * Reescrevendo o array - caso boostrap switch
         */
       foreach ($merge as $key => $value) {
           
            /* bootstrap-switch */
            if($value=='on'){
                $merge[$key]    = TRUE;
            }
            if($value=='bootstrapSwitchEmpty'){
                $merge[$key]    = NULL;
            }

        }

        $merge['modificado']    =   date("Y-m-d H:i:s");


        /**
         * Query
         */
        $query = $this->db->update($databaseTable, $merge, array($databaseColumnPk => $databaseItemCurrent));

        if($query)
        {
            $array = array(
                'status'    =>  'processed',
                'datetime'  =>  date("Y-m-d H:i:s"),
                'ip'        =>  $this->input->ip_address()
            );
        }
        else
        {
            $array = array(
                'status'    =>  'unprocessed'
            );
        }        
        echo json_encode($array);

    }





    /**
     * Recebe uma interacao via Ajax. Metodo POST.
     * data, moduleSlug, table, columnId, valueReference }, 
     * @return array
     */
    public function details() {

        $beginContent       =   '';
        $rulesDetails       =   json_decode($_POST['data']); // rules columns


        $moduleSlug         =   $_POST['moduleSlug'];
        $databaseTable      =   $_POST['databaseTable'];
        $databaseColumnPk   =   $_POST['databaseColumnPk'];
        $valueReference     =   $_POST['valueReference'];

        $detailsDataBulkSpecific   = $this->detailsDataBulkSpecific($moduleSlug, $databaseColumnPk, $valueReference);

        foreach ($detailsDataBulkSpecific as $value): 

            foreach ($rulesDetails as $columnDatabase => $rules):
                $cell = $value->$columnDatabase;
                $cellFilter = $this->processDetails($cell, $rules);
                    
                $beginContent .= '<div class="row">';
                    $beginContent .= '<div class="col-sm-2 text-right bold">';
                        $beginContent .= $rules->title . ':';
                    $beginContent .= '</div>';


                    $beginContent .= '<div class="col-sm-10 cellFilter">';
                        $beginContent .= $cellFilter;
                    $beginContent .= '</div>';

                $beginContent .= '</div>';

            endforeach;
        endforeach;

        $array = array(
            'status'    =>  'processed',
            'contentHtml'   => $beginContent,
        );
        echo json_encode($array);
    }






    /**
     * Recebe uma interacao via Ajax. Metodo POST.
     * @return array
     */
    /* Change name setThumb for setTabThumb*/
    public function setThumb() {

        $filenameNewThumbOriginal   =   $_POST['nameFileOriginal'];
        $filenameNewThumb           =   $_POST['filenameNewThumb'];
        $filenameOldThumb           =   $_POST['filenameOldThumb'];
        $databaseTable              =   $_POST['databaseTable'];
        $databaseItemCurrent        =   $_POST['databaseItemCurrent'];
        $databaseColumnThumb        =   $_POST['databaseColumnThumb'];
        $pathThumb                  =   $_POST['pathThumb'];
        $databaseColumnPk           =   $_POST['databaseColumnPk'];


        $explode = explode('.', $filenameOldThumb);
        
        // DELETAR TAMBEM A THUMB ORIGINAL
   @    $unlink = unlink(FCPATH . $pathThumb . $filenameOldThumb);
        $unlink = unlink(FCPATH . $pathThumb . $filenameNewThumbOriginal);


        $data = array($databaseColumnThumb=>$filenameNewThumb);
        $data['modificado'] = date("Y-m-d H:i:s");
        $query = $this->model_crud->update($databaseTable, $databaseColumnPk, $databaseItemCurrent, $data);

        // checking if query is successfully
        if($query)
        {
            $array = array(
                'status'    =>  'processed',
            );
        }
        else
        {
            $array = array(
                'status'    =>  'unprocessed'
            );
        }
        echo json_encode($array);

    }





    /**
     * Recebe uma interacao via Ajax. Metodo POST.
     * @return array
     */
    public function setTabMaps() {

        /*
          Instance of class
        =============================================*/ 
        $moduleSlug             =   $_POST['moduleSlug'];
        $this->load->module($moduleSlug);
        $settings               = $this->$moduleSlug->settings();


        /*
          Setting Variables
        =============================================*/ 
        $databaseTable              =   $settings['databaseTable'];
        $databaseColumnPk           =   $settings['databaseColumnPk'];
        $databaseItemCurrent        =   $_POST['databaseItemCurrent'];;
        $modificado                 =   array('modificado'    =>  date("Y-m-d H:i:s"));


        /*
          Unserializing Variables 
        =============================================*/ 
        $arrayData      = $_POST['arrayData'];
        parse_str($arrayData, $searchArray);
        $merge          = array_merge($searchArray, $modificado);


        /*
          Query 
        =============================================*/ 
        $query = $this->model_crud->update($databaseTable, $databaseColumnPk, $databaseItemCurrent, $merge);


        /*
          Verify Query Successfully and exit Json_encode
        =============================================*/ 
        if($query)
        {
            $array = array(
                'status'    =>  'processed',
                'datetime'  =>  date("Y-m-d H:i:s"),
                'ip'        =>  $this->input->ip_address()
            );
        }
        else
        {
            $array = array(
                'status'    =>  'unprocessed'
            );
        }
        echo json_encode($array);

    }





    /**
     * Funcao   
     *
     * Nao e necessario algumas variaveis, somente a slugModule, idFoto e talvez filenamePhoto ( checar ) 
     *
     * @author Gustavo Botega 
     * @param string $database -   
     * @param int $columnId -    
     * @return string Json Encode 
     */      
    public function deletePhoto() {

        $databaseTableGallery           =   $_POST['databaseTableGallery'];
        $databaseTableGalleryColumnPk   =   $_POST['databaseTableGalleryColumnPk'];
        $databaseTableGalleryColumnThumb   =   $_POST['databaseTableGalleryColumnThumb'];
        $idFoto                         =   $_POST['idFoto'];
        $pathGallery                    =   $_POST['pathGallery'];

        /*
        DELETAR IMAGEM DO DIRETORIO
        1)   Se o arquivo nao existir mais no diretorio, mesmo assim o PHP vai tentar excluir o arquivo,
            e vai retornar um warning, por isso foi inserido o @ antes da linha a fim de ignorar.
            Nesse momento o sistema deve registrar esse acontecimento num arquivo de log relatando o erro.
        2) Criar recurso - sempre que o browser nao conseguir carregar uma imagem porque nao esta no arquivo, gravar em um log.
        */
        $filenamePhoto  = $this->model_crud->get_rowSpecific($databaseTableGallery, $databaseTableGalleryColumnPk, $idFoto, 1, $databaseTableGalleryColumnThumb);


        /* DELETANDO REGISTRO DO BANCO */

        $query      =   $this->model_crud->delete($databaseTableGallery, $databaseTableGalleryColumnPk, $idFoto);
        if($query)
        {


@           rename( $pathGallery . $filenamePhoto, 'trash/'.$filenamePhoto);


            $array = array(
                'status'    =>  'processed',
            );
        }
        else
        {
            $array = array(
                'status'    =>  'unprocessed'
            );
        }
        echo json_encode($array);

    }






    /**
     * Funcao   
     *
     * @author Gustavo Botega 
     * @param string $moduleSlug -   
     * @param int $columnId -    
     * @return ??? $valueReference -
     */      
    public function detailsDataBulkSpecific($moduleSlug=NULL, $columnId, $valueReference)
    {
        $this->load->module($moduleSlug);
        $retrieve   = $this->$moduleSlug->retrieve();
        $query      = $this->model_crud->select($retrieve['table'], $retrieve['fieldsDetailsDatatable'], array($columnId . ' ='=>$valueReference), $retrieve['orderby'], $retrieve['limit']);
        return $query;
    }



    /**
     * Funcao   
     *
     * @author Gustavo Botega 
     * @param string $moduleSlug -   
     * @param int $columnId -    
     * @return ??? $valueReference -
     */      
    public function selectReassembling(){

        /**
         * Instanciando modulo. O modulo instanciado eh recebido por POST.
         * $settings herda os valores do metodo settings() do modulo instanciado
         */
        $moduleSlug             =   $_POST['moduleSlug'];
        $this->load->module($moduleSlug);
        $settings               = $this->$moduleSlug->settings();


        /**
         * Definindo as variaveis que serao usadas no escopo da funcao
         */
        $formIdentification     =   $_POST['formIdentification'];
        $dataInstruction        =   $_POST['dataInstruction'];
        $valueCurrent           =   $_POST['valueCurrent'];
        $dataInstructionDecode  =   base64_decode($_POST['dataInstruction']);
        $dataInstructionJson    =   json_decode($dataInstructionDecode);


        /**
         * Variaveis que serao utilizadas para montar o novo Select
         */
        $selectDependenceIdentification     =   $dataInstructionJson[0];
        $selectDependenceOptionInstruction  =   $dataInstructionJson[1];
        $selectDependenceTable              =   $dataInstructionJson[2];
        $selectDependenceColumns            =   $dataInstructionJson[3];
        $selectDependenceWhere              =   $dataInstructionJson[4];
        $selectDatabaseOrderby              =   $dataInstructionJson[5];
        $selectDependenceIndex              =   $dataInstructionJson[6];
        $selectDependenceValue              =   $dataInstructionJson[7];
        

        /**
         * Reescrevendo $selectDependenceWhere
         * - Onde o value for igual a {$1}, e substituido pelo id do item que foi selecionado no select pai
         */
        $arrayClausuleWhere = array();
        foreach ($selectDependenceWhere as $key => $value) {

            if($value=='{$1}'){
                $value = $valueCurrent;
            }

            $arrayClausuleWhere[$key] = $value;
        }
        $selectDependenceWhere = $arrayClausuleWhere;


        /**
         * Query
         */
        /*
        var_dump($selectDependenceTable);
        var_dump($selectDependenceColumns);
        var_dump($selectDependenceWhere);
        var_dump($selectDatabaseOrderby);
        var_dump($selectDependenceIndex);
        var_dump($selectDependenceValue);
*/
        $query                  =   $this->model_crud->select($selectDependenceTable, $selectDependenceColumns, $selectDependenceWhere, NULL, NULL);


        /**
         * Remontando o array no formato que possa ser lido pela funcao form_dropdown() - nativa do framework
         */
        $reassemblingArray = array(''   =>  $selectDependenceOptionInstruction);
        foreach ($query as $key => $value) {
          $reassemblingArray[$value->$selectDependenceIndex]  = $value->$selectDependenceValue;
        }


        /**
         * Dropdown
         */
        $resultFormDropdown     =   form_dropdown( $selectDependenceIdentification, $reassemblingArray, '', ' id="'.$selectDependenceIdentification.'" data-instruction="empty" data-dependencies="" class="form-control selectDependence" ' ); /*' data-teste="hue" '*/
        echo header('Content-Type: application/json; charset=utf-8');


        /**
         * Documentar - $query
         */
        if($query)
        {
            $array = array(
                'status'                =>  'processed',
                'selectIdentification'  =>  $selectDependenceIdentification,
                'resultFormDropdown'    =>  $resultFormDropdown,
            );
        }
        else
        {
            $array = array(
                'status'    =>  'unprocessed'
            );
        }        

        /**
         * Documentar - saida
         */
        echo json_encode($array);


    }





    /**
     * Funcao   
     *
     * @author Gustavo Botega 
     * @param array $cell -   
     * @param array $rules -    
     * @return array $cell -
     */      
    public function processDetails($cell, $rules)
    {       

        /* FK - Foreign Key */
        if(isset($rules->fk) && !empty($rules->fk))
        {
            $fk = $this->model_crud->get_rowSpecific($rules->fk[0], $rules->fk[1], $cell, 1, $rules->fk[2]);
            $cell = $fk;
        }

        // Label - Bootstrap 
        if(isset($rules->label) && !empty($rules->label)){
            $startLabel     = '<h'.$rules->label[1].'> <span class="label '.$rules->label[0].'">';
            $endLabel       = ' </span> </h'.$rules->label[1].'>';
            $composition    = $startLabel . $cell . $endLabel;
            $cell           = $composition;
        }

        // Date - Transform
        if(isset($rules->date) && !empty($rules->date)){
            $date = $this->my_date->datetime($cell, 'justDate');
            $cell = $date;
        }

        // Image
        if(isset($rules->img) && !empty($rules->img)){
            $src    =   base_url() . 'uploads/' . $rules->img . '/' . $cell;
            $img    =   '<a href="'.$src.'" class="fancybox-button">';
            $img    .=  '<img class="img-responsive img-thumbnail" src="'.$src.'" alt="" style="width:100px;margin:0 auto;display:block;float:left;text-align:center;">';
            $img    .=  '</a>';
            $cell   =   $img;
        }


        // Status
        if(isset($rules->status) && !empty($rules->status)){
            switch ($cell) {
                case 1:
                    $cell = '<span class="label label-success">Ativo </span>';
                    break;
                
                case 0:
                    $cell = '<span class="label label-default">Inativo </span>';
                    break;
            }
        }



        /* Mountaing HTML */
        return $cell;
    }







    /*----------------------------------------------    
      EXTENSION
    -----------------------------------------------*/
    public function extension()
    {   


        /**
         * Instanciando modulo. O modulo instanciado eh recebido por POST.
         * $settings herda os valores do metodo settings() do modulo instanciado
         */
        $moduleSlug             =   $_POST;
        $moduleSlug             =   $_POST['moduleSlug'];
        $this->load->module($moduleSlug);

        $create                 =   $this->$moduleSlug->create();
        $retrieve               =   $this->$moduleSlug->retrieve();
        /*$update                 =   $this->$moduleSlug->update();*/
        $settings               =   $this->$moduleSlug->settings();


        $array      =   array(
            'status'                          =>  'processed',
            'moduleSlug'                      =>  $settings['moduleSlug'],                
            'databaseTable'                   =>  $settings['databaseTable'],             
            'databaseColumnPk'                =>  $settings['databaseColumnPk'],          
            'databaseModuleColumnFolder'      =>  $settings['databaseModuleColumnFolder'],
            'rulesDetails'                    =>  $retrieve['rulesDetails'],
            'formIdentification'              =>  $settings['formIdentification'],
            'databaseColumnFk'                =>  $settings['databaseColumnFk'],
            /*'databaseItemCurrent'             =>  $update['databaseItemCurrent']*/
        );     


        echo json_encode($array);

    }






     /*
             // @@@ Implementar permissões por usuario
        
        // AUDITORIA:
            [0]=> Slug Module
            [1]=> Table Database
            [2]=> Column Database
            [3]=> Value Deleted
            [4]=> Folder Thumb
            [5]=> Delete Gallery
            [6]=> Table Gallery
            [7]=> Column Table Gallery
            [8]=> Data/hora
            [9]=> IP
            [10]=> ID Usuário  
            [11]=> Type of Operation  
            */

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

