<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_GuestOffice.php';



/**
    ???
 */
class Pessoa extends MY_GuestOffice {

    public $data;

    function __construct() {

        parent::__construct();


        /* Package */
#        $this->ThemeComponent();
#        $this->SigepeAssets(); 

    }

    public function index() {
        $this->Formulario();        
    }





    /**
     * FlagUsuario
     *
     * Verifica se a pessoa fisica esta com o flag de usuario ativo. Se sim retorna true se nao false.
     *
     * @author Gustavo Botega 
     * @return boolean
     */
    public function FlagUsuario($IdPessoa = NULL, $IdPessoaFisica = NULL, $Cpf = NULL)
    {

    }

    



    /**
     * DeletarPessoa
     *
     * Funcao Deleta pessoa e suas dependencias no sistema.
     *
     * @author Gustavo Botega 
     * @return boolean
     */
    public function DeletarPessoa($PessoaId)
    {
        
        echo "<h3>Sequencia de Passos:</h3><br>";
        
        echo '
        <pre>
            ID PESSOA: '.$PessoaId.'
        
            --- Informacoes de Contato ---
            1) tb_email
            2) tb_endereco
            3) tb_telefone

            --- Vinculo ---
            4) tb_vinculo_historico
            5) tb_vinculo

            --- Pessoa ---
            6) tb_pessoa_fisica_atleta
            7) tb_pessoa_fisica
            8) tb_pessoa        
            
            <hr>
            
        </pre>

        ';
        
        /***********************************************************
            1) Deletar Email
        ************************************************************/
        $SqlEmail                   =   'SELECT * FROM tb_email WHERE fk_peo_id = "'.$PessoaId.'" ';
        $QueryEmail                 =   $this->db->query($SqlEmail)->result();
        foreach ($QueryEmail as $key => $value):

            $SqlEmail               =   'DELETE FROM tb_email WHERE ema_id = "'.$value->ema_id.'" AND fk_peo_id = "'.$PessoaId.'" ';
            $QueryEmail             =   $this->db->query($SqlEmail);
            echo (!$QueryEmail) ? 'ERRO. 1) tb_email. Parando o processando...' : '[OK]  1) tb_email - Deletado todas as ocorrencias para o ID: ' . $PessoaId . '<hr>';
            if(!$QueryEmail) return false;

        endforeach;


        /***********************************************************
            2) Deletar Endereco
        ************************************************************/
        $SqlEndereco                =   'SELECT * FROM tb_endereco WHERE fk_peo_id = "'.$PessoaId.'" ';
        $QueryEndereco              =   $this->db->query($SqlEndereco)->result();
        foreach ($QueryEndereco as $key => $value):

            $SqlEndereco               =   'DELETE FROM tb_endereco WHERE end_id = "'.$value->end_id.'" AND fk_peo_id = "'.$PessoaId.'" ';
            $QueryEndereco             =   $this->db->query($SqlEndereco);
            echo (!$QueryEndereco) ? 'ERRO. 2) tb_endereco. Parando o processando...' : '[OK]  2) tb_endereco - Deletado todas as ocorrencias para o ID: ' . $PessoaId . '<hr>';
            if(!$QueryEndereco) return false;

        endforeach;



        /***********************************************************
            3) Deletar Telefone
        ************************************************************/
        $SqlTelefone                =   'SELECT * FROM tb_telefone WHERE fk_peo_id = "'.$PessoaId.'" ';
        $QueryTelefone              =   $this->db->query($SqlTelefone)->result();
        foreach ($QueryTelefone as $key => $value):

            $SqlEndereco               =   'DELETE FROM tb_telefone WHERE tel_id = "'.$value->tel_id.'" AND fk_peo_id = "'.$PessoaId.'" ';
            $QueryEndereco             =   $this->db->query($SqlEndereco);
            echo (!$QueryEndereco) ? 'ERRO. 3) tb_telefone. Parando o processando...' : '[OK]  3) tb_telefone - Deletado todas as ocorrencias para o ID: ' . $PessoaId . '<hr>';
            if(!$QueryEndereco) return false;

        endforeach;

        
        

        /***********************************************************
            4) tb_vinculo_historico
            5) tb_vinculo
        ************************************************************/
# O CERTO E ESSA LINHA. PROCURAR PELO FK_PES_ID        $SqlVinculo            =   'SELECT * FROM tb_vinculo WHERE fk_pes1_id = "'.$PessoaId.'" ';
        $SqlVinculo            =   'SELECT * FROM tb_vinculo WHERE fk_aut_id = "'.$PessoaId.'" ';
        $QueryVinculo          =   $this->db->query($SqlVinculo)->result();
        foreach ($QueryVinculo as $key => $value):

            // Obtendo os historicos do vinculo current
            $VinculoId                  =  $value->vin_id;        
            echo "<h3> ID Vinculo: " . $VinculoId . "</h3>"; 
        
            $SqlVinculoHistorico       =   'SELECT * FROM tb_vinculo_historico WHERE fk_vin_id = "'.$VinculoId.'" ';
            $QueryVinculoHistorico     =   $this->db->query($SqlVinculoHistorico)->result();

            foreach ($QueryVinculoHistorico as $key => $value):

                // Deletando todos os historicos do vinculo associado
                echo "<h4> ID Vinculo Historico: ". $value->vih_id ."</h4>";
                $SqlDeletarVinculoHistorico       =   'DELETE FROM tb_vinculo_historico WHERE fk_vin_id = "'.$VinculoId.'" ';
                $QueryDeletarVinculoHistorico     =   $this->db->query($SqlDeletarVinculoHistorico);
                echo (!$QueryDeletarVinculoHistorico) ? '<span style="background:red;color:white;padding:20px;">ERRO. 4) tb_vinculo_historico. Parando o processando...</span>' : '[OK]  11) tb_vinculo_historico - Deletado todas as ocorrencias para o ID: ' . $PessoaId . '<hr>';
                if(!$QueryDeletarVinculoHistorico) return false;

            endforeach;
            // Fim do foreach na tabela tb_vinculos_historico


            /*
            Deletando o vinculo. Primeiro e deletado o historico do vinculo e depois o vinculo.
            E feito nessa ordem para que possa se obter o ID do Vinculo ( vin_id ).
            */
            $SqlDeletarVinculo       =   'DELETE FROM tb_vinculo WHERE vin_id = "'.$VinculoId.'" ';
            $QueryDeletarVinculo     =   $this->db->query($SqlDeletarVinculo);
            echo (!$QueryDeletarVinculoHistorico) ? '<span style="background:red;color:white;padding:20px;">ERRO. 5) tb_vinculo. Parando o processando...</span>' : '5) tb_vinculo - Deletado todas as ocorrencias para o ID: ' . $PessoaId . '<hr>';
            if(!$QueryDeletarVinculoHistorico) return false;

        
        endforeach;
        // Fim do foreach da tabela tb_vinculos

        

        /***********************************************************
            6) Deletar Pessoa Fisica Atleta
            7) Deletar Pessoa Fisica
        ************************************************************/
        $PessoaFisica                         =   $this->db->query('SELECT * FROM tb_pessoa_fisica where fk_pes_id = '.$PessoaId.' ')->result();
        if (count($PessoaFisica) > 1)   echo "<h1 style='color:red;font-size:40px;'>ATENÇAO: MAIS DE UM REGISTRO DE PESSOA FISICA PRA MESMA PESSOA. DUPLICIDADE. DELETAR MANUALMENTE.</h1>";
        foreach($PessoaFisica as $key => $value):
        
            $PessoaFisicaId     =   $value->pef_id;
            
            // Deletando Pessoa Fisica Atleta
            $SqlPessoaFisicaAtleta                =   'SELECT * FROM tb_pessoa_fisica_atleta WHERE fk_pef_id = "'.$PessoaFisicaId.'" ';
            $QueryPessoaFisicaAtleta              =   $this->db->query($SqlPessoaFisicaAtleta)->result();
            if(!$QueryPessoaFisicaAtleta):
                foreach ($QueryPessoaFisicaAtleta as $key => $value):
        
                    $SqlDeletarPessoaFisicaAtleta     =   'DELETE FROM tb_pessoa_fisica_atleta WHERE fk_pef_id = "'.$PessoaFisicaId.'" ';
                    $QueryDeletarPessoaFisicaAtleta   =   $this->db->query($SqlDeletarPessoaFisicaAtleta);
                    echo (!$QueryDeletarPessoaFisicaAtleta) ? '<span style="background:red;color:white;padding:20px;">ERRO. 6) tb_pessoa_fisica_atleta. Parando o processando...</span>' : '[OK]  6) tb_pessoa_fisica_atleta - Deletado todas as ocorrencias para o ID: ' . $PessoaId . '<hr>';
                    if(!$QueryDeletarPessoaFisicaAtleta) return false;
        
                endforeach;
            endif;


            // Deletando Pessoa Fisica
            $SqlDeletarPessoaFisica     =   'DELETE FROM tb_pessoa_fisica WHERE pef_id = "'.$PessoaFisicaId.'" ';
            $QueryDeletarPessoaFisica   =   $this->db->query($SqlDeletarPessoaFisica);
            echo (!$QueryDeletarPessoaFisica) ? '<span style="background:red;color:white;padding:20px;">ERRO. 7) tb_pessoa_fisica. Parando o processando...</span>' : '[OK]  7) tb_pessoa_fisica - Deletado todas as ocorrencias para o ID: ' . $PessoaId . '<hr>';
            if(!$QueryDeletarPessoaFisica) return false;

        
        endforeach;
        
        
        /***********************************************************
            8) Deletar Pessoa 
        ************************************************************/        
        $SqlDeletarPessoa       =   'DELETE FROM tb_pessoa WHERE pes_id = "'.$PessoaId.'" ';
        $QueryDeletarPessoa     =   $this->db->query($SqlDeletarPessoa);
        echo (!$QueryDeletarPessoa) ? '<span style="background:red;color:white;padding:20px;">ERRO. 8) tb_pessoa. Parando o processando...</span>' : '[OK]  8) tb_pessoa - Deletado todas as ocorrencias para o ID: ' . $PessoaId . '<hr>';
        if(!$QueryDeletarPessoa) return false;

        
        
        
        
        
        
    }

}

/* End of file PessoaFisica.php */
/* Location: ./application/controllers/welcome.php */

