<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_GuestOffice.php';
class Pessoa extends MY_GuestOffice {

    public $data;

    function __construct() {
        parent::__construct();


        /* Package */
        $this->PackagesClass(); // TROCAR PARA ThemeComponent();
        $this->PackagesAssets();  // TROCAR PARA SigepeAssets();
        $this->data['TesteClasse']                =   'GuestOffice';



    }

    public function index() {
       return NULL;
    }







    /*
     * GetPerfil
     * -
     * @author Gustavo Botega 
     * @param     
     * @return boolean  retorna TRUE se o CPF existir
    */
    public function GetPerfil($IdPessoa, $PerfilAtivo = TRUE, $Json = FALSE) {

        ($PerfilAtivo) ? $Status = '1' : $Status = 'IS NOT NULL';

        $QueryPerfil    =   "
                                SELECT * FROM tb_vinculo
                                    WHERE fk_pes1_id = ".$IdPessoa." AND
                                          fk_pes2_id IS NULL AND
                                          fk_ani_id IS NULL AND
                                          fk_per_id IS NOT NULL AND
                                          fk_tip_id IS NULL AND
                                          fk_sta_id = ".$Status."
                            ";

        $Resultado  =   $this->db->query($QueryPerfil)->result();


        $Perfis     =   array();
        foreach ($Resultado as $value) {
            $Perfis[]   =   $value->fk_per_id;
        }


        if($Json)
            echo json_encode($Perfis); 

        if(!$Json)
            return $Perfis; 

    }



    /*
     * GetEmpresas
     * -
     * @author Gustavo Botega 
     * @param     
     * @return boolean  retorna TRUE se o CPF existir
    */
    public function GetEmpresas($IdPessoa, $VinculoAtivo = TRUE, $Json = FALSE) {


        ($VinculoAtivo) ? $Status = '1' : $Status = 'IS NOT NULL';

        $QueryEmpresas    =   "
                                SELECT * FROM tb_vinculo
                                    WHERE fk_pes1_id = ".$IdPessoa." AND
                                          fk_pes2_id IS NOT NULL AND
                                          fk_ani_id IS NULL AND
                                          fk_per_id IS NULL AND
                                          fk_tip_id IN('12','13','14','15','16','17','18','19') AND
                                          fk_sta_id = ".$Status."
                            ";

        $Resultado  =   $this->db->query($QueryEmpresas)->result();

        $Empresas     =   array();
        foreach ($Resultado as $value) {
            $Empresas[]   =  array(
                                'IdVinculo'    =>    $value->vin_id,
                                'TipoVinculo'  =>    $value->fk_tip_id,
                                'IdEmpresa'    =>    $value->fk_pes2_id,
                                'Status'       =>    $value->fk_sta_id,
                            );
        }

        if($Json)
            echo json_encode($Empresas); 

        if(!$Json)
            return $Empresas; 

    }





    /*
     * GetVinculo
     * -
     * @author Gustavo Botega 
     * @param     
     * @return boolean  retorna TRUE se o CPF existir
    */
    public function GetVinculo() {

        $RetornoDeDados     =   '';
        echo json_encode($RetornoDeDados); 

    }







    /*
     * CpfCnpjExiste
     * - Verifica na coluna peo_cpf da tabela tb_people se existe alguma ocorrencia com o valor submetido. 
     * - A funcao e chamada por uma requisicao ajax tipo post. Nao recebe parametros na funcao.
     * - Se ja existir algum CPF a funcao retorna TRUE se nao FALSE
     * @author Gustavo Botega 
     * @param     
     * @return boolean  retorna TRUE se o CPF existir
    */
    public function CpfCnpjExiste() {

        $CpfCnpj    =   $_POST['CpfCnpj'];

        $Query = $this->model_crud->select(
                    'tb_pessoa',
                    array( 'pes_id'),
                    array('pes_cpf_cnpj =' => $CpfCnpj),
                    array(),
                    1
                );

        if( is_null($Query) || empty($Query) ) {
            $RetornoDeDados     =   false;            
        }else{
            $RetornoDeDados     =   true;            
        }

        echo json_encode($RetornoDeDados); 

    }


    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function PackagesClass() // Trocar nome da function para ThemeComponent()
    { 

     // Trocar nome da function para ThemeComponent()


        /*
            
            NO SABADO 22/07 fazer com o que a view processe e imprima os valores abaixo.

        */

        /* 
            StylesFile 
        */
            /*  Plugins*/
            $this->data['StylesFile']['Plugins']['select2']                = TRUE;
        
            /*  Syles*/
    //            $this->data['StylesFile']['Styles']['profile']                 = TRUE;



        /* 
            ScriptsFile 
        */
            /*  Plugins*/
            $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;

            /*  Scripts*/
            $this->data['ScriptsFile']['Scripts']['form-wizard']                              = TRUE;

    }


    /**
    * PackagesAssets
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function PackagesAssets()
    { 
        /* Carregando Estilos */
        $this->data['PackageStyles'][]    =   'packages/guestoffice/style/cadastro-ordinario';


        /* Carregando Scripts */
        $this->data['PackageScripts'][]   =   'packages/guestoffice/script/cadastro-ordinario';
        $this->data['PackageScripts'][]   =   'packages/guestoffice/script/form-wizard';
        $this->data['PackageScripts'][]   =   'packages/guestoffice/script/pessoa';


    }


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

