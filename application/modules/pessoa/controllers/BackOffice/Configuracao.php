<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Configuracao extends MY_BackOffice {

    public $data;
    public $ambiente = "BackOffice";
    public $modulo = "Pessoa";
    public $tabelaNome = "tb_vinculo";
    public $viewNome = "vw_vinculo";
    public $columnId = "fk_pessoa_chave";
    public $columnDesc = "vin_pessoa";

    /* url de acesso*/
    // http://localhost/sigepe/pessoa/BackOffice/Configuracao/

    function __construct() {
        parent::__construct();

        $this->metronicAsset();
        $this->sigepeAsset();

        /* Setando variaveis de ambiente */
        $this->data['PageHeadTitle'] = "Configuracao";
        $this->data['PageHeadSubtitle'] = "Pessoa";


        $modulo = "Pessoa";
        $this->data['ShowSidebar'] = false;
        $this->data['ShowColumnLeft'] = "Profile" . $modulo;
        $this->data['base_dir'] = "Template/BackOffice/sistema/$modulo/";
        $this->data['Class'] = $this->router->fetch_class();

        /* Controladores Externos */
        #$this->load->module('evento/XOffice/Evento');

        /* copiado do endereço, melhorar integração no core do backoffice */
        $this->load->module('pessoa/BackOffice/Pessoa');
        $this->data['StylesFile']['Styles']['profile']  =  TRUE;
        $this->data['ShowProfilePessoa'] = TRUE;
        $this->data['Class'] = $this->router->fetch_class();
        $this->data['pessoaId'] = $this->uri->segment(5);
        $this->data['Method']   = '';
        $this->data['menuAtivo']  = 'configuracao';
}

    public function index() {
        $this->configuracao_browse_bread();
    }

    /**
     * configuracao_browse_bread.
     * @author Nillander
     * @return
     */
    public function configuracao_browse_bread($pessoaId) {
        /* Copiado de outros módulos, melhorar */
        $pessoa = $this->pessoa->obterPessoa($pessoaId);
        $this->data['PageHeadTitle'] = $pessoa->pes_nome_razao_social;
        /* Copiado de outros módulos, melhorar */

        $sql = "SELECT * FROM $this->viewNome where $this->columnId = $pessoaId and fk_tip_id is not null order by vin_criado asc";
        $dataset = $this->db->query($sql)->result();
        $this->data['dataset'] = $dataset;
        /* View */
        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);
    }

// fim do metodo @configuracao_browse_bread

    /**
     * configuracao_read
     * @author Nillander
     * @return
     */
    public function configuracao_read($contextoId) {
        $sql = "SELECT * FROM $this->viewNome where $this->columnId = $contextoId";
        $dataset = $this->db->query($sql)->result();
        $this->data['dataset'] = $dataset;
        /* View */
        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);
    }

// fim do metodo @configuracao_read

    /**
     * configuracao_add
     * @author Nillander
     * @return
     */
    public function configuracao_add() {
        /* View */
        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);
    }

// fim do méotodo @configuracao_add

    /**
     * configuracao_edit
     * @author Nillander
     * @return
     */
    public function configuracao_edit() {
        /* View */
        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);
    }

// fim do méotodo @configuracao_edit



    /* # Fim Bread
      # Inicio Middleware
      ==================================================================================================================== */

    /**
     * processar
     * @author Nillander
     * @param $dataJson
     * @return
     */
    public function processar($dataJson = false) {
        if (isset($_POST['dataJson']) && is_array($_POST['dataJson'])) {
            $dados = $_POST['dataJson'];
        }

        if ($dataJson) {
            $dados = $dataJson;
        }

        if (!isset($_POST['dataJson']) && !$dataJson) {
            echo json_encode(false);
        }

        // Validacao Formulario
        $validar = $this->validar($dados);
        if (!$validar) {
            echo json_encode(false);
            return false;
        }

        // Se existir algum ID por convencao entende-se que o metodo a ser direcionado e de atualizacao.
        if (!isset($dados['id'])) {
            $resultadoAcao = $this->gravar($dados);
            echo ($resultadoAcao) ? json_encode(true) : json_encode(false);
        } else {
            $resultadoAlterar = $this->alterar($dados);
            echo ($resultadoAlterar) ? json_encode(true) : json_encode(false);
        }
    }

// fim do metodo @processar

    /**
     * gravar
     * @author Nillander
     * @param $dados
     * @return
     */
    public function gravar($dados) {
        foreach ($dados as $key => $value) {
            if (empty($value))
                $dados[$key] = NULL;
        }

        // dados
        $dataset = array(
            'fk_aut_id' => $this->session->userdata('PessoaId'),
            'tab_coluna' => gerarTimeStamp("tb_tabela", "tab_coluna"),
            'eve_slug' => gerarSlug($dados['eve-nome-evento']),
            'criado' => date("Y-m-d H:i:s")
        );

        /* query */
        $query = $this->db->insert($this->tabelaNome, $dataset);

        if ($query) {
            $classeId = $this->db->insert_id();
            return true;
        } else {
            return false;
        }
    }

// fim do metodo @gravar

    /**
     * alterar.
     *
     * -
     *
     * @author Nillander
     * @param $dados
     * @return
     */
    public function alterar($dados) {

        (!isset($dados['campo_null'])) ? $dados['campo_null'] = null : '';

        // dados
        $dataset = array(
            $this->columnId => $dados['id'],
            'fk_aut_id' => $this->session->userdata('PessoaId'),
            'tab_coluna' => gerarTimeStamp("tb_tabela", "tab_coluna"),
            'eve_slug' => gerarSlug($dados['name-objeto']),
            'criado' => date("Y-m-d H:i:s")
        );

        /* Query */
        $query = $this->db->update($this->tabelaNome, $dataset, array($this->columnId => $dados['id']));
        return $query;
    }

// fim do metodo @alterar

    /**
     * validar
     * @author Nillander
     * @param $dados
     * @return
     */
    public function validar($dados) {
        return true; // Validar inputs e regras de negocio do formulario. Tratar erro.
    }

// fim do metodo @validar



    /* # Fim Middleware
      # Inicio Acessório
      ==================================================================================================================== */

    /**
     * fazerAlgumaCoisa.
     * @author Nillander
     * @param $contextoId
     * @return
     */
    public function fazerAlgumaCoisa($contextoId) {
        
    }

    /* # Fim Metodo Acessório
      # Inicio Asset
      ========================================================================= */

    /**
     * metronicAsset.
     * @author Nillander
     * @param 
     * @return
     */
    public function metronicAsset() {
        /* STYLES */
        $this->data['StylesFile']['Plugins']['datatables'] = TRUE;
        $this->data['StylesFile']['Plugins']['datatables-bootstrap'] = TRUE;

        /* SCRIPTS */
        $this->data['ScriptsFile']['Plugins']['datatable'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['datatables'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['datatables-bootstrap'] = TRUE;

        $this->data['ScriptsFile']['Scripts']['table-datatables-fixedheader'] = TRUE;
    }

// fim do metodo MetronicAsset

    /**
     * sigepeAsset.
     * @author Nillander
     * @param 
     * @return
     */
    public function sigepeAsset() {
        /* Carregando Estilos */
        $this->data['PackageStyles'][] = "Packages/Styles/$this->modulo/$this->ambiente/". __CLASS__ ."";

        /* Carregando Scripts */
        $this->data['PackageScripts'][] = "Packages/Scripts/$this->modulo/$this->ambiente/". __CLASS__ ."";
    }

// fim do metodo sigepeAsset
}

/* End of file classe.php */
