<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pessoa extends MY_BackOffice {

    public $data;

    function __construct() {

        parent::__construct();

        $this->data['Class'] = $this->router->fetch_class();
        $this->MetronicAsset();
        $this->SigepeAsset();
        $this->data['base_dir'] = 'Template/BackOffice/sistema/Pessoa/';
        $this->data['pessoaId'] = $this->uri->segment(5);
        $this->data['Method']   = '';

        $this->data['PageHeadTitle'] = "Pessoas";
        $this->data['PageHeadSubtitle'] = "Físicas";

        $this->data['StylesFile']['Styles']['profile']  =  TRUE;
        $this->data['ShowProfilePessoa'] = TRUE;

        /* Controladores */
#        $this->load->module('pessoa/BackOffice/Pessoa');
    }

    public function index() {
        $this->pessoa_browse();
    }

    // fim do método @index



    /**
     * pessoa_dashboard.
     *
     *
     * @author Gustavo
     *
     * @return view
     */
    public function pessoa_dashboard($pessoaId) {
        $pessoa = $this->obterPessoa($pessoaId);
        $this->data['menuAtivo']  = 'dashboard';
        $this->data['PageHeadTitle'] = $pessoa->pes_nome_razao_social;

        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);
    }


    /**
     * pessoa_browse.
     *
     *
     * @author Nillander
     *
     * @return view
     */
    public function pessoa_browse() {
        $this->data['ShowProfilePessoa'] = NULL;
      
        $sqlPessoas = "
                        SELECT pes.pes_id, pes.pes_cpf_cnpj, pes.pes_nome_razao_social, pes.pes_foto, pes.fk_sta_id, pes.pes_natureza,
                        stats.sta_status from tb_pessoa pes
                        join tb_status stats on pes.fk_sta_id = stats.sta_id
                        where pes.pes_natureza = 'PF'
                        order by pes_nome_razao_social asc
                    ";
        //$this->data['DataSetPessoas'] = $this->db->query($sqlPessoas)->result();
        $dataSetPessoas = $this->db->query($sqlPessoas)->result();

        $sqlPerfilDaPessoa = "
        SELECT pes.pes_id, pes.pes_cpf_cnpj
          ,vin.fk_per_id
          ,vip.vip_papel
          ,vin.fk_pes1_id, vin.fk_sta_id as vin_fk_sta_id,stats.sta_status as vin_status, vin.criado as vin_criado
        from tb_pessoa pes
        join tb_vinculo vin on vin.fk_pes1_id = pes.pes_id and vin.fk_pes2_id is null and vin.fk_tip_id is null
        left join tb_vinculo_perfil vip on vip.vip_id = vin.fk_per_id 
        left join tb_status stats on vin.fk_sta_id = stats.sta_id
        where pes.pes_natureza = 'PF'
        and vip.vip_natureza_pessoa = 'PF'";
        $dataSetPerfilDaPessoa = $this->db->query($sqlPerfilDaPessoa)->result();

        foreach ($dataSetPessoas as $pessoa) {
            $procurado = $pessoa->pes_id;
            $pessoa->perfil = array_filter(
                $dataSetPerfilDaPessoa,
                function ($e) use (&$procurado) {
                    return $e->fk_pes1_id == $procurado;
                }
            );
        }

        $this->data['DataSetPessoas'] = $dataSetPessoas;

        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);
    }

    // fim do método @pessoa_browse

    /**
     * pessoa_read.
     *
     *
     * @author Nillander
     *
     * @return view
     */
    public function pessoa_read_ed($pessoaId) {




        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Meu Perfil';
        $this->data['PageHeadSubtitle']   = 'Visualize e gerencie seu perfil. Mantenha seus dados sempre atualizados.';
        $this->data['NavActiveSidebar']   = 'MeuPerfil';

        $this->MeuPerfilDataset();  // Carrega os valores dos selects


        $PessoaId                           =   $pessoaId;
        $PessoaFisicaId                     =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'fk_pes_id', $PessoaId, 1, 'pef_id');
        $this->data['PessoaId']             =   $PessoaId;        
        $this->data['PessoaFisicaId']       =   $PessoaFisicaId;        


        $this->data['NomeCompleto']         =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $PessoaId, 1, 'pes_nome_razao_social');
        $this->data['Apelido']              =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'pef_id', $PessoaFisicaId, 1, 'pef_apelido');
        $this->data['DataDeNascimento']     =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $PessoaId, 1, 'pes_data_nascimento_fundacao');
        $this->data['Genero']               =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'pef_id', $PessoaFisicaId, 1, 'fk_gen_id');
        $this->data['GeneroId']             =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'pef_id', $PessoaFisicaId, 1, 'fk_gen_id');
        
        $this->data['EstadoCivilId']        =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'pef_id', $PessoaFisicaId, 1, 'fk_est_id');
        $this->data['EstadoCivil']          =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_estadocivil', 'pfe_id', $this->data['EstadoCivilId'], 1, 'pfe_estado_civil');

        $this->data['NacionalidadeId']      =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'pef_id', $PessoaFisicaId, 1, 'fk_nac_id');
        $this->data['Nacionalidade']        =   $this->model_crud->get_rowSpecific('tb_pais', 'pai_id', $this->data['NacionalidadeId'], 1, 'pai_pais');

        $this->data['Naturalidade']         =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'pef_id', $PessoaFisicaId, 1, 'pef_naturalidade');

        $this->data['TipoSanguineoId']      =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'pef_id', $PessoaFisicaId, 1, 'fk_tip_id');
        $this->data['TipoSanguineo']        =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_tiposanguineo', 'pft_id', $this->data['TipoSanguineoId'], 1, 'pft_tipo_sanguineo');
 
        $this->data['EscolaridadeId']       =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'pef_id', $PessoaFisicaId, 1, 'fk_esc_id');
        $this->data['Escolaridade']         =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_escolaridade', 'pfe_id', $this->data['EscolaridadeId'], 1, 'pfe_escolaridade');

        $this->data['NomeDoPai']            =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'pef_id', $PessoaFisicaId, 1, 'pef_nome_pai');
        $this->data['NomeDaMae']            =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'pef_id', $PessoaFisicaId, 1, 'pef_nome_mae');
        $this->data['CPF']                  =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $PessoaId, 1, 'pes_cpf_cnpj');
//        $this->data['CPFIsento']            =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'pef_id', $PessoaFisicaId, 1, 'flag_cpf_isento');



       $this->data['PackageStyles'][]    =   'Packages/Styles/Perfil/FrontOffice/MeuPerfil';
       $this->data['PackageScripts'][]   =   'Packages/Scripts/Perfil/FrontOffice/MeuPerfil';




        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);
    }

    // fim do método @pessoa_read
    /**
     * pessoa_edit.
     *
     *
     * @author Nillander
     *
     * @return view
     */
    public function pessoa_edit() {
        
    }

    // fim do método @pessoa_edit
    /**
     * pessoa_add.
     *
     *
     * @author Nillander
     *
     * @return view
     */
    public function pessoa_add() {
        
    }

    // fim do método @pessoa_add
    /**
     * pessoa_disable.
     *
     *
     * @author Nillander
     *
     * @return view
     */
    public function pessoa_disable() {
        
    }

    // fim do método @pessoa_disable

    /**
     * pessoa_cpf_duplicado.
     *
     *
     * @author Nillander
     *
     * @return view
     */
    public function pessoa_cpf_duplicado() {
        $this->data['ShowProfilePessoa'] = NULL;
        
        $sqlPessoas = "
          SELECT pes_id, pes_foto, stats.sta_status, pes_nome_razao_social, pes_cpf_cnpj from tb_pessoa 
          left join tb_status stats on fk_sta_id = stats.sta_id
          where pes_cpf_cnpj in (
                 select pes_cpf_cnpj from (SELECT pes_id, pes_cpf_cnpj, count(pes_cpf_cnpj) as contCpf
                  from tb_pessoa
                  join tb_status stats on fk_sta_id = stats.sta_id
                  group by pes_cpf_cnpj
                  having contCpf >= 2) reslutado)
          ";
        /*$sqlPessoas = "
        SELECT pes_id, pes_foto, stats.sta_status, pes_nome_razao_social, pes_cpf_cnpj, count(pes_id) as contCpf
        from tb_pessoa
        join tb_status stats on fk_sta_id = stats.sta_id
        group by pes_cpf_cnpj
        having contCpf >= 2";*/

        $this->data['DataSetPessoas'] = $this->db->query($sqlPessoas)->result();

        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);

    }

    // fim do método @pessoa_cpf_duplicado



    /* # Fim Bread
      # Inicio Middleware
    ====================================================================================================================*/



    /**
    * processar.
    *
    * -
    *
    * @author Gustavo Botega
    *
    * @param
    *
    * @return boolean
    */
    public function processar($dataJson = false) {
        if(isset($_POST['dataJson']) && is_array($_POST['dataJson'])) {
            $dados = $_POST['dataJson'];
        }

        if($dataJson) {
            $dados = $dataJson;
        }

        if(!isset($_POST['dataJson']) && !$dataJson) {
            echo json_encode(false);
        }

        // Validacao Formulario
        $validar = $this->validar($dados);
        if (!$validar) {
            echo json_encode(false);
            return false;
        }

        // Se existir algum ID por convencao entende-se que o metodo a ser direcionado e de atualizacao.
        if(!isset($dados['id'])) {
            /*$resultadoAcao = $this->gravar($dados);
            echo ($resultadoAcao) ? json_encode(true) : json_encode(false);*/
        } else {
            $resultadoAlterar = $this->alterar($dados);
            echo ($resultadoAlterar) ? json_encode(true) : json_encode(false);
        }

    } // fim do metodo @processar



    /**
    * Gravar
    *
    * @author Gustavo Botega
    */
    public function gravar($Dados) {

    } // fim do metodo @gravar



    /**
    * Alterar
    *
    * @author Gustavo Botega
    */
    public function alterar($dados) {

        foreach ($dados as $key => $value) {
            if(empty($value))
                $dados[$key]    =   NULL;
        }

        // Dados
        $dataset = array(
            'pef_apelido' => $dados['pes-apelido'], // Apelido
            'fk_nac_id' => $dados['pes-nacionalidade'], // Nacionalidade
            'fk_gen_id' => $dados['pes-genero'], // Nacionalidade
            'fk_esc_id' => $dados['pes-escolaridade'], // Escolaridade
            'fk_tip_id' => $dados['pes-tipo-sanguineo'], // Tipo Sanguineo
            'pef_naturalidade' => $dados['pes-naturalidade'], // Naturalidade
            'fk_est_id' => $dados['pes-estado-civil'], // Estado Civil
            'pef_nome_pai' => $dados['pes-nome-pai'], // Nome Pai
            'pef_nome_mae' => $dados['pes-nome-mae'], // Nome Mae
            'modificado' => date("Y-m-d H:i:s")
        );



        $pessoaFisicaId = $this->model_crud->get_rowSpecificObject('tb_pessoa_fisica', 'fk_pes_id', $dados['id']);

        // Query
        $query = $this->db->update('tb_pessoa_fisica', $dataset, "pef_id = " . $pessoaFisicaId->pef_id);
        
        
            
        // Dados
        $dataset = array(
            'pes_nome_razao_social' => $dados['pes-nome-completo'], // Nome Completo
            'pes_data_nascimento_fundacao' => $this->my_data->ConverterData($dados['pes-data-nascimento'], 'PT-BR', 'ISO'), // Nome Completo
            'modificado' => date("Y-m-d H:i:s")
        );

        $query = $this->db->update('tb_pessoa', $dataset, "pes_id = " . $dados['id']);


        return $query;

        //var_dump($dados); return false;

    } // fim do método @alterar



    /**
    * validar
    *
    * @author Gustavo Botega
    */
    public function validar($dados) {
        return true; // Validar inputs e regras de negocio do formulario. Tratar erro.
    } // fim do método @validar



    /* # Fim Middleware
       # Inicio Acessório
      ==================================================================================================================== */


    /**
    * MeuPerfilDataset
    *
    * @author Gustavo Botega 
    */
    public function MeuPerfilDataset(){


        /* Genero */
        $QueryGenero    =   "
                                SELECT * FROM tb_pessoa_fisica_genero
                                    ORDER BY pfg_id ASC 
                            ";
        $this->data['DatasetGenero']  =   $this->db->query($QueryGenero)->result();


        /* Estado Civil */
        $QueryEstadoCivil    =   "
                                SELECT * FROM tb_pessoa_fisica_estadocivil
                                    ORDER BY pfe_posicao ASC 
                            ";
        $this->data['DatasetEstadoCivil']  =   $this->db->query($QueryEstadoCivil)->result();


        /* Nacionalidade */
        $QueryNacionalidade    =   "
                                SELECT * FROM tb_pais
                                    ORDER BY pai_posicao ASC 
                            ";
        $this->data['DatasetNacionalidade']  =   $this->db->query($QueryNacionalidade)->result();


        /* Naturalidade */
        $QueryNaturalidade    =   "
                                SELECT * FROM tb_municipio
                                    ORDER BY mun_id ASC 
                            ";
        $this->data['DatasetNaturalidade']  =   $this->db->query($QueryNaturalidade)->result();


        /* Tipo Sanguineo */
        $QueryTipoSanguineo    =   "
                                SELECT * FROM tb_pessoa_fisica_tiposanguineo
                                    ORDER BY pft_id ASC 
                            ";
        $this->data['DatasetTipoSanguineo']  =   $this->db->query($QueryTipoSanguineo)->result();


        /* Escolaridade */
        $QueryEscolaridade    =   "
                                SELECT * FROM tb_pessoa_fisica_escolaridade
                                    ORDER BY pfe_id ASC 
                            ";
        $this->data['DatasetEscolaridade']  =   $this->db->query($QueryEscolaridade)->result();

    }





    /* # Fim Acessório
      # Inicio Controladores
      ==================================================================================================================== */



      /**
       * obterPessoa.
       *
       *
       * @author Nillander
       *
       * @return objeto Pessoa
       */
    public function obterPessoa($pessoaId) {
        return $this->model_crud->get_rowSpecificObject('tb_pessoa', 'pes_id', $pessoaId);
    } // fim do metodo @obterSerie



    /* # Fim Controladores
      # Inicio Asset
      ==================================================================================================================== */



    public function MetronicAsset() {
        $this->data['StylesFile']['Plugins']['datatables'] = TRUE;
        /* Styles */
        $this->data['StylesFile']['Plugins']['datatables-bootstrap'] = TRUE;
        $this->data['StylesFile']['Plugins']['bootstrap-sweetalert'] = TRUE;
        /* Scripts */
        $this->data['ScriptsFile']['Scripts']['ui-sweetalert'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['jquery-validation'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['datatable'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['datatables'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['datatables-bootstrap'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-datepicker'] = TRUE;
        $this->data['ScriptsFile']['Scripts']['table-datatables-buttons'] = TRUE;
        $this->data['ScriptsFile']['Scripts']['table-datatables-fixedheader'] = TRUE;
    }

// fim do método @MetronicAsset

    /**
     * SigepeAsset
     *
     * Carregar aqui dinamicamente JS e CSS
     *
     * @author Gustavo Botega
     * @return array
     */
    public function SigepeAsset() {
        /* Carregando Estilos */
        /* Carregando Scripts */
        $this->data['PackageScripts'][] = 'Packages/Scripts/' . __CLASS__ . '/BackOffice/' . __CLASS__ . '/' . __CLASS__;
    }

// fim do método @SigepeAsset



}

// end of Class
// end of file