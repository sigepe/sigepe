<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_BackOffice.php';
class CadastroPessoaJuridica extends MY_GuestOffice {

    public $data;

    function __construct() {

        parent::__construct();


        /* Package */
        $this->ThemeComponent();
        $this->SigepeAssets(); 

    }

    public function index() {
        $this->Formulario();        
    }


    /**
    * Formulario
    *
    * @author Gustavo Botega 
    */
    public function Formulario(){

        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']             =   get_class($this);
        $this->data['PageHeadTitle']                =   'Cadastro';
        $this->data['PageHeadSubtitle']             =   'Para cadastrar uma empresa no SIGEPE preencha o formulário abaixo.';
        $this->data['CodigoUsuarioMenorIdade']      =   $this->my_pessoa_fisica->GerarCpf('2');


        /* Carrega View */
        $this->LoadTemplateGuest('template/guest/cadastro/ordinario/formulario', $this->data);
    } 






















    /**
    * ThemeComponent
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function ThemeComponent()
    { 


        /* 
            StylesFile 
        */
            /*  Plugins*/
            $this->data['StylesFile']['Plugins']['select2']                = TRUE;
            $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']   = TRUE;
        
            /*  Syles*/
    //            $this->data['StylesFile']['Styles']['profile']                 = TRUE;



        /* 
            ScriptsFile 
        */
            /*  Plugins*/
            $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

            /*  Scripts*/
            $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                            = TRUE;

    }


    /**
    * SigepeAssets
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function SigepeAssets()
    { 
        /* Carregando Estilos */
        $this->data['PackageStyles'][]    =   'packages/guestoffice/style/cadastro-ordinario';


        /* Carregando Scripts */
        $this->data['PackageScripts'][]   =   'packages/guestoffice/script/cadastro-ordinario';
        $this->data['PackageScripts'][]   =   'packages/guestoffice/script/form-wizard';
        $this->data['PackageScripts'][]   =   'packages/guestoffice/script/pessoa';


    }



    /*





    public function Processar($JsonEncode = false, $DataExternal = false){

        if(!$DataExternal){
            $ArrayDataSerialized        =   $_POST['DataSerialized'];
            parse_str($ArrayDataSerialized, $DataUnserialized);            
        }else{
            $DataUnserialized   =   $DataExternal;
        }

        $PessoaTipo                 =   $DataUnserialized['pessoa-tipo'];
        $PessoaPerfil               =   $DataUnserialized['pessoa-perfil'];
        $Erro                       =   []; 

        
       
        $ValidaFormulario = $this->ValidaFormulario($DataUnserialized);

        if( !is_array($ValidaFormulario) ) : // formulario validado

           $Gravar = $this->Gravar( $DataUnserialized );
           if( isset($Gravar['Status']) && $Gravar['Status'] == TRUE ):

                // Gravar Telefone
                $GravarTelefone = $this->my_pessoa_telefone->Gravar($Gravar['IdPessoa'], $DataUnserialized);
                if( !isset($GravarTelefone['Status']) || !$GravarTelefone['Status'])
                    $Erro[]     =       '77561764';


                // Gravar Email
                $GravarEmail    =   $this->my_pessoa_email->Gravar($Gravar['IdPessoa'], $DataUnserialized);
                if( !isset($GravarEmail['Status']) || !$GravarEmail['Status'])
                    $Erro[]     =       '40719339';


                // Gravar Pessoa Fisica
                $GravarPessoaFisica     =   $this->my_pessoa_fisica->Gravar($Gravar['IdPessoa'], $DataUnserialized);
                if( !isset($GravarPessoaFisica['Status']) || !$GravarPessoaFisica['Status'] )
                    $Erro[]     =       '12900685';

           endif;


           if( !isset($Gravar['Status']) || $Gravar['Status'] != TRUE ):
                foreach ($Gravar as $item) {
                    $Erro[]     =   $item;
                }
           endif;


        endif;


        if( is_array($ValidaFormulario) ) : // formulario nao validado
            foreach ($ValidaFormulario as $item) {
                $Erro[]     =   $item;
            }
        endif;




        // Tipo Pessoa Inexistente 
        if($PessoaTipo != 'PF')
            $Erro[]     =   '38230242';



        if(!empty($Erro)):

            if(!$JsonEncode)
                return $Erro;

            if($JsonEncode)
                echo json_encode($Erro);
        endif;

        if (empty($Erro)):

            if(!$JsonEncode) 
                return $Gravar;

            if($JsonEncode)
                echo json_encode($Gravar);

        endif;

    }














    public function ValidarFormulario(){


        // $FlagUsuario        =   $DataUnserialized['flag-usuario']; O flag usuario vai ser apenas usado para PF. 

        // Setando erro 
        $Erro = []; 
    

        // Setando valores de $DataUnserialized    
        $DataNascimento     =   $DataUnserialized['data-nascimento'];
        $NomeCompleto       =   $DataUnserialized['nome-completo'];
        $Sexo               =   $DataUnserialized['sexo'];
        $Numero             =   $DataUnserialized['numero'];
        $Senha              =   $DataUnserialized['senha'];
        $ConfirmarSenha     =   $DataUnserialized['confirmar-senha'];



        // CPF Isento
        if(!isset($DataUnserialized['cpf-isento'])){
           $CpfIsento     =   FALSE;
        }else{
           $CpfIsento     =   TRUE;
        }


        // Se CPF nao for isento checar se CPF ja esta cadastrado. Se sim retornar error.


        // Validar Data de Nascimento Checar se a data esta em formato pt-br e nao possui idade superior a 18
        $Idade  =   $this->my_data->CalcularIdade($DataNascimento);
        if( $Idade >= 100)
            $Erro[] = '79879402';


        // Checar se CPF e valido
        if(!$CpfIsento):

            $Cpf    =   $DataUnserialized['cpf'];
            if( $this->my_pessoa->ValidarCpf($Cpf) == FALSE )
                $Erro[]     =  '61718581';


            if( $this->my_pessoa->CpfCnpjExiste($Cpf) == TRUE)
                $Erro[]     =  '12849031';

        endif;


        // Checar se nome completo possui mais de 6 caracteres e possui mais de 1 palavra.
        $ValidarNome         =   $this->my_pessoa->ValidarNome($NomeCompleto);
        if( $ValidarNome == FALSE )
            $Erro[]     =   '43906393';



        // Checar se o celular contem 15 caracteres. Usar $this->my_contato->ValidaCelular(); 
        $ValidarTelefone    =   $this->my_pessoa_telefone->ValidarTelefone($Numero);
        if( is_array($ValidarTelefone) ){
            foreach ($ValidarTelefone as $value):
                $Erro[]     =   $value;
            endforeach;
        }


        // ValidarEmail  
        $ValidarEmail    =   $this->my_pessoa_email->ValidarEmail($Email);
        if( is_array($ValidarEmail) ){
            foreach ($ValidarEmail as $value):
                $Erro[]     =   $value;
            endforeach;
        }



        // Checar se o ID do sexo recebido esta dentro de algum dos cadastros da tabela de sexo. Usar $this->my_pessoa_fisica->ValidaSexo()
        if( $this->my_pessoa_fisica->ValidaSexo($Sexo) == FALSE )
            $Erro[]     =   '81218076';




        // Checar se a senha informada e a conferida são as mesmas
        // Checar situacao senha isenta ( caso da pessoa que vai cadastrar outra )
        // Validar senha. Quantidade minima de caracteres e depois se os valores sao os mesmos
        if( $Senha != $ConfirmarSenha )
            $Erro[]     =   '81531303';



        if(!empty($Erro))
            return $Erro;


    }
    */




}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

