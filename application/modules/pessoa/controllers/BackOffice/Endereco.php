<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Endereco extends MY_BackOffice {

    public $data;

    function __construct() {

        parent::__construct();

        $this->data['Class'] = $this->router->fetch_class();
        $this->MetronicAsset();
        $this->SigepeAsset();
        $this->data['base_dir'] = 'Template/BackOffice/sistema/Pessoa/';
        $this->data['pessoaId'] = $this->uri->segment(5);
        $this->data['Method']   = '';
        $this->data['StylesFile']['Styles']['profile']  =  TRUE;
        $this->data['ShowProfilePessoa'] = TRUE;

        /* Controladores */
        $this->load->module('pessoa/BackOffice/Pessoa');
    }

    public function index() {
        $this->endereco_browse();
    }

    // fim do método @index



    /**
     * endereco_dashboard.
     *
     *
     * @author Gustavo
     *
     * @return view
     */
    public function endereco_dashboard($pessoaId) {
        $pessoa = $this->obterPessoa($pessoaId);
        $this->data['menuAtivo']  = 'dashboard';
        $this->data['PageHeadTitle'] = $pessoa->pes_nome_razao_social;

        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);
    }


    /**
     * endereco_browse.
     *
     *
     * @author Nillander
     *
     * @return view
     */
    public function endereco_browse_bread($pessoaId) {
        $pessoa = $this->pessoa->obterPessoa($pessoaId);
        $this->data['menuAtivo']  = 'endereco';
        $this->data['pessoaId']   = $pessoa->pes_id;
        $this->data['PageHeadTitle'] = $pessoa->pes_nome_razao_social;
        
        /************************************************/
        $pessoa = $this->pessoa->obterPessoa($pessoaId);
        
        $SqlEndereco = "
        SELECT * FROM tb_endereco as end
        WHERE fk_peo_id = $pessoaId AND flag_deletado IS NULL
        ORDER BY end_principal DESC, end_id DESC";
        $this->data['DatasetEndereco']      =   $this->db->query($SqlEndereco)->result();        

        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);

    }

    // fim do método @endereco_browse

    /**
     * endereco_read.
     *
     *
     * @author Nillander
     *
     * @return view
     */
    public function endereco_read($pessoaId) {
        

    }

    // fim do método @endereco_read
    /**
     * endereco_edit.
     *
     *
     * @author Nillander
     *
     * @return view
     */
    public function endereco_edit() {
        
    }

    // fim do método @endereco_edit
    /**
     * endereco_add.
     *
     *
     * @author Nillander
     *
     * @return view
     */
    public function endereco_add() {
        
    }

    // fim do método @endereco_add
    /**
     * endereco_disable.
     *
     *
     * @author Nillander
     *
     * @return view
     */
    public function endereco_disable() {
        
    }

    // fim do método @endereco_disable



    /* # Fim Bread
      # Inicio Middleware
      ==================================================================================================================== */



    /* # Fim Middleware
      # Inicio Acessório
      ==================================================================================================================== */



    /* # Fim Acessório
      # Inicio Controladores
      ==================================================================================================================== */



      /**
       * obterPessoa.
       *
       *
       * @author Nillander
       *
       * @return objeto Pessoa
       */
    public function obterEndereco($enderecoId) {
        return $this->model_crud->get_rowSpecificObject('tb_endereco', 'end_id', $enderecoId);
    } // fim do metodo @obterEndereco

    public function obterEnderecosDaPessoa($pessoaId) {
        return $this->db->query("SELECT * FROM tb_endereco where fk_peo_id = $pessoaId order by end_endereco asc")->result();
    } // fim do metodo @obterEnderecosDaPessoa



    /* # Fim Controladores
      # Inicio Asset
      ==================================================================================================================== */



    public function MetronicAsset() {
        /* Styles */


        /* Scripts */
    }

// fim do método @MetronicAsset

    /**
     * SigepeAsset
     *
     * Carregar aqui dinamicamente JS e CSS
     *
     * @author 
     * @return array
     */
    public function SigepeAsset() {
        /* Carregando Estilos */
        /* Carregando Scripts */
        $this->data['PackageStyles'][]    =   'Packages/Styles/Perfil/FrontOffice/endereco';
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Perfil/FrontOffice/endereco';
    }

// fim do método @SigepeAsset



}

// end of Class
// end of file