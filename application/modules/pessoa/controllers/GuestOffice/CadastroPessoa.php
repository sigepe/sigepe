<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_GuestOffice.php';
class CadastroPessoa extends MY_GuestOffice {

    public $data;

    function __construct() {
        parent::__construct();

    }


    /**
    * Processar
    *
    *
    * @author Gustavo Botega 
    */
    public function Processar($Dados = false){


        $Retorno        =   array();

        if(empty($Dados) || !$Dados || !is_array($Dados)){
            $Retorno['Status']   =   false;
            return $Retorno;
        }


        $Validar        =       $this->Validar($Dados);
        if(!$Validar){
            $Retorno['Status']   =   false;
            return $Retorno;
        }


        $Gravar         =   $this->Gravar($Dados);
        if(!$Gravar){
            $Retorno['Status']   =      false;
            return $Retorno;
        }


        $Retorno['Status']      =      true;
        $Retorno['PessoaId']    =      $Gravar['PessoaId'];
        return $Retorno;
    }




    /**
    * Validar
    *
    * {desc}
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function Validar($Dados){

        // Flag Usuario

        // Fk Autor Id

        // Fk Status Id

        // Pessoa Natureza

        // Pessoa CPF CNPJ

        // Pessoa Nome Razao Social

        // Pessoa Data Nascimento Fundacao

        // Pessoa Foto

        // Pessoa Senha

        return true;

    }







    /**
    * Gravar
    *
    *
    * @author Gustavo Botega 
    */
    public function Gravar( $Dados ){


        $Erro = array(); 


        // Carregando modulo autenticacao
        $this->load->module('autenticacao');


        // Setando Variaveis
        $Dados['pessoa-status']             =   '1'; 
        (isset($Dados['cpf-isento'])) ? $Dados['cpf-cnpj'] = $Dados['usuario-menor-idade'] : '';


        // Autor
        $AutorId    =   NULL;   
        if( $this->session->userdata('Autenticado') )
            $AutorId  =   $this->session->userdata('PessoaId');


        // Data Nascimento Fundacao
        $DataNascimentoFundacao             =   $this->my_data->ConverterData($Dados['data-nascimento-fundacao'], 'PT-BR', 'ISO');


        // Foto
        (!isset($Dados['Foto']) ? $Dados['Foto'] = NULL : '' );


        // Senha
        if($Dados['pessoa-natureza'] == 'PF'){
            $SenhaEncriptada        =   $this->autenticacao->EncriptarSenha($Dados['senha']);
        }else{
            $SenhaEncriptada  =   NULL;
        }


        // Flag Usuario
        (isset($Dados['flag-usuario'])) ? $FlagUsuario = 1 : $FlagUsuario = NULL;


        // Dados
        $Dados       =  array(
            'fk_aut_id'                         =>    $AutorId,
            'fk_sta_id'                         =>    $Dados['pessoa-status'], // Status 
            'flag_usuario'                      =>    $FlagUsuario, // Flag Usuario
            'pes_natureza'                      =>    $Dados['pessoa-natureza'],
            'pes_cpf_cnpj'                      =>    $this->my_pessoa->RemoverPontuacaoCpfCnpj( $Dados['cpf-cnpj'] ),
            'pes_nome_razao_social'             =>    $Dados['nome-razao-social'],
            'pes_data_nascimento_fundacao'      =>    $DataNascimentoFundacao,
            'pes_foto'                          =>    $Dados['Foto'],
            'pes_senha'                         =>    $SenhaEncriptada,
            'criado'                            =>    date("Y-m-d H:i:s")
        );


        /* Query */
        $Query = $this->db->insert('tb_pessoa', $Dados);
        if($Query){

            $Retorno = array(
                'Status'    =>  true,
                'DateTime'  =>  date("d/m/Y") . " às " . date("H:i:s"),
                'Ip'        =>  $this->input->ip_address(),
                'PessoaId'  =>  $this->db->insert_id()
            );
        }else{
            $Retorno['Status'] = false;
        }        


        return $Retorno;

    }





    /**
    * AjaxProcessar
    *
    * @author Gustavo Botega 
    */
    public function AjaxProcessar(){

        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);    

        $Processar  =   $this->Processar($Dados);
        echo json_encode($Processar);

    }


}



/* End of file CadastroPessoa.php */
/* Location: ./application/modules/pessoa/controllers/GuestOffice/CadastroPessoa.php */

