<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_GuestOffice.php';
class CadastroAtleta extends MY_GuestOffice {

    public $data;

    function __construct() {
        parent::__construct();


        /* Package */
        $this->ThemeComponent(); 
        $this->SigepeAsset(); 

    }

    public function index() {
		$this->Formulario();    	
    }

    public function Formulario(){

        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Cadastro';
        $this->data['PageHeadSubtitle']   = 'Para se cadastrar como atleta no SIGEPE preencha o formulário abaixo.';
        $this->data['Breadcrumbs']        = array();

        $SqlFederacao                     = '
                                                SELECT * FROM
                                                    tb_pessoa as pes
                                                INNER JOIN
                                                    tb_vinculo as vin
                                                ON
                                                    pes.pes_id = vin.fk_pes1_id
                                                WHERE
                                                    vin.fk_per_id = 3 AND
                                                    vin.fk_tip_id IS NULL AND
                                                    vin.fk_sta_id = 1 AND
                                                    pes.fk_sta_id = 1 
                                                ORDER BY
                                                    pes_id=141 desc,
                                                    pes_nome_razao_social asc
                                            ';
        $this->data['DatasetFederacao']   = $this->db->query($SqlFederacao)->result();



        /* Entidade Filiada */
        $SqlEntidadeFiliada                     = '
                                                SELECT * FROM tb_pessoa as pes

                                                INNER JOIN tb_vinculo as vin
                                                ON pes.pes_id = vin.fk_pes1_id

                                                INNER JOIN tb_pessoa_juridica as pej
                                                ON pej.fk_pes_id = pes.pes_id


                                                INNER JOIN tb_pessoa_juridica_entidade as pje
                                                ON pje.fk_pej_id = pej.pej_id


                                                WHERE vin.fk_per_id = 4 AND 
                                                vin.fk_sta_id = 1 AND
                                                vin.fk_tip_id IS NULL AND
                                                pes.fk_sta_id = 1 AND
                                                pje.flag_entidade_filiada = 1

                                                ORDER BY pes.pes_nome_razao_social ASC

                                            ';
        $this->data['DatasetEntidadeFiliada']   = $this->db->query($SqlEntidadeFiliada)->result();;


        /* Escola Equitacao */
        $SqlEscolaEquitacao                     = '
                                                SELECT * FROM tb_pessoa as pes

                                                INNER JOIN tb_vinculo as vin
                                                ON pes.pes_id = vin.fk_pes1_id

                                                INNER JOIN tb_pessoa_juridica as pej
                                                ON pej.fk_pes_id = pes.pes_id


                                                INNER JOIN tb_pessoa_juridica_entidade as pje
                                                ON pje.fk_pej_id = pej.pej_id


                                                WHERE vin.fk_per_id = 4 AND 
                                                vin.fk_sta_id = 1 AND
                                                vin.fk_tip_id IS NULL AND
                                                pes.fk_sta_id = 1 AND
                                                pje.flag_escola_equitacao = 1

                                                ORDER BY pes.pes_nome_razao_social ASC
                                            ';
        $this->data['DatasetEscolaEquitacao']   = $this->db->query($SqlEscolaEquitacao)->result();

        $this->data['CodigoUsuarioMenorIdade']      =   $this->my_pessoa_fisica->GerarCpf('2');



        /* Carrega View */
        $this->LoadTemplateGuest('Template/GuestOffice/Pessoa/Atleta/Formulario', $this->data);

    }


    /**
    * Processar
    *
    * @author Gustavo Botega 
    */
    public function Processar($Ajax = false, $AjaxDados = false){


        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);                       

        if($AjaxDados)
            $Dados  =   $AjaxDados;



        $this->load->module('pessoa/GuestOffice/CadastroPessoa');
        $this->load->module('pessoa/GuestOffice/CadastroPessoaFisica');
        $this->load->module('vinculo');
        $this->load->module('cadastro/Telefone');
        $this->load->module('cadastro/Email');



        // Setando Variaveis
        $Dados['pessoa-status']             =   '1'; 
        (isset($Dados['cpf-isento'])) ? $Dados['cpf-cnpj'] = $Dados['usuario-menor-idade'] : '';


        (!empty($Dados['escola-equitacao']) && $Dados['escola-equitacao'] > 0) ? $Dados['entidade-equestre'] = $Dados['escola-equitacao'] : $Dados['entidade-equestre'] = $Dados['entidade-filiada'];



        // Pessoa 
        $Pessoa                             =   $this->cadastropessoa->Processar($Dados);
        if(!isset($Pessoa['PessoaId']))
            return false;

        // Desc
        $Dados['PessoaId']               =  $Pessoa['PessoaId'];


        // Pessoa Fisica
        $PessoaFisica                       =   $this->cadastropessoafisica->Processar( $Dados, $Pessoa['PessoaId']  );
        if(!isset($PessoaFisica['PessoaFisicaId']))
            return false;


        // Pessoa Fisica Atleta
        $Validar                            =   $this->Validar($Dados);
        $Gravar                             =   $this->Gravar($Dados, $PessoaFisica['PessoaFisicaId']);
        if($Gravar):        

            // Gravar Telefone
            $GravarTelefone                 =   $this->telefone->Processar($Dados, $Pessoa['PessoaId']);

            // Gravar Email
            $GravarEmail                    =   $this->email->Processar($Dados, $Pessoa['PessoaId']);

            // Gravar Responsavel Financeiro
            /*
            if(isset($Dados['responsavel-financeiro']))
                $this->vinculo->GravarResponsavelFinanceiroCompetidor($Pessoa['PessoaId'], NULL, $Dados['responsavel-financeiro']);
            */

            // Gravar Vinculo - Perfil Atleta
            $GravarVinculoPerfilAtleta      =   $this->vinculo->GravarPerfilAtleta($Pessoa['PessoaId']);
                    
        
            // Gravar Vinculo - Confederacao
            $GravarVinculoAtletaConfederacao = $this->vinculo->GravarVinculoAtletaConfederacao($Dados, $Pessoa['PessoaId']);

            // Gravar Vinculo - Federacao
            $GravarVinculoAtletaFederacao   =   $this->vinculo->GravarVinculoAtletaFederacao($Dados, $Pessoa['PessoaId']);

            // Gravar Vinculo - Entidade Equestre
            if($Dados['federacao'] == '141'){ // O vinculo entre atleta e entidade so acontece se a federacao escolhida por a FHBr. ID = 141 
                $GravarVinculoAtletaEntidade    =   $this->vinculo->GravarVinculoAtletaEntidade($Dados, $Pessoa['PessoaId']);
                if(!$GravarVinculoAtletaEntidade)
                    return false;
            } 

            /*
                Atualizando vinculo na tabela do atleta. tb_pessoa_fisica_atleta.
                Se for FHBr atualiza separadamente o vinculo de entidade. Sendo que se nao for FHBr o atleta nao deve
                ser vinculado a nenhuma entidade equestre.
            
            */
            $Dataset    =   array(
                                'fk_pjc_id'         =>  140,  // Federacao ID
                                'fk_pjf_id'         =>  $Dados['federacao'],  // Federacao ID
                            );
            $Query    =   $this->db->update('tb_pessoa_fisica_atleta', $Dataset, array('fk_pef_id' => $PessoaFisica['PessoaFisicaId'] ));        
            if($Dados['federacao'] == '141'){ 
                $Dataset    =   array(
                                    'fk_pje_id'         =>  $Dados['entidade-equestre'],  // Entidade Equestre ID
                                );
                $Query    =   $this->db->update('tb_pessoa_fisica_atleta', $Dataset, array('fk_pef_id' => $PessoaFisica['PessoaFisicaId'] ));        
            }
        

            if(
                !$GravarTelefone ||
                !$GravarEmail ||
                !$GravarVinculoPerfilAtleta ||
                !$GravarVinculoAtletaConfederacao ||
                !$GravarVinculoAtletaFederacao
            )
                return false; 



        endif;


        if(!$Gravar)
            return false;
        


        // Cadastro atleta finalizado com sucesso
        return true;

    }




    /**
    * AjaxProcessar
    *
    * @author Gustavo Botega 
    */
    public function AjaxProcessar(){


        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);    


        $Processar  =   $this->Processar(true, $Dados);
        echo json_encode($Processar);

    }





    /**
    * Validar
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function Validar($Dados){

        // Nome Competicao

        // Matricula CBH

        // Matricula FEI

        // Federacao

        // Entidade

    }




    /**
    * Gravar
    *
    * @author Gustavo Botega 
    */
    public function Gravar($Dados, $PessoaFisicaId){


        // Foto
        (!isset($Dados['Foto']) ? $Dados['Foto'] = NULL : '' );


        // Matricula CBH
        (!isset($Dados['matricula-cbh']) ? $Dados['matricula-cbh'] = NULL : '' );


        // Matricula FEI
        (!isset($Dados['matricula-fei']) ? $Dados['matricula-fei'] = NULL : '' );


        // Atleta Escola
        (($Dados['escola-equitacao']) > 0) ? $AtletaEscola = 1 : $AtletaEscola = NULL ;


        // Senha
        if($Dados['pessoa-natureza'] == 'PF'){
            $SenhaEncriptada        =   $this->autenticacao->EncriptarSenha($Dados['senha']);
        }else{
            $Senha  =   NULL;
        }


        // Atleta Escola
        $Dados['atleta-escola'] = NULL;
        if(isset($Dados['escola-equitacao'])){
            (!empty($Dados['escola-equitacao']) && $Dados['escola-equitacao'] > 0) ? $AtletaEscola = 1 : $AtletaEscola = NULL;
        }


        // Flag Usuario
        (isset($Dados['flag-usuario'])) ? $FlagUsuario = 1 : $FlagUsuario = NULL;


        // Dados
        $Dados       =  array(
            'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
            'fk_pef_id'                         =>    $PessoaFisicaId,
            'pfa_nome_competicao'               =>    $Dados['nome-competicao'],
            'pfa_registro_cbh'                  =>    $Dados['registro-cbh'],
            'pfa_registro_fei'                  =>    $Dados['registro-fei'],
            'pfa_escola'                        =>    $AtletaEscola,
            'flag_registro'                     =>    NULL, // Todo atleta entra no sistema com o flag de registro vencido.
            'criado'                            =>    date("Y-m-d H:i:s")
        );


        /* Query */
        $Query = $this->db->insert('tb_pessoa_fisica_atleta', $Dados);
        if($Query)
            $DadosRetorno['PessoaFisicaAtletaId']   =    $this->db->insert_id();


        return ($Query) ? $DadosRetorno : false;

    }



    /**
    * JsonProcessar
    *
    * @author Gustavo Botega 
    */
    public function JsonProcessar(){

    }





    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    */
    public function ThemeComponent(){ 


        /* 
            StylesFile 
        */
            /*  Plugins*/
            $this->data['StylesFile']['Plugins']['select2']                = TRUE;
            $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']   = TRUE;
        
            /*  Syles*/
    //            $this->data['StylesFile']['Styles']['profile']                 = TRUE;



        /* 
            ScriptsFile 
        */
            /*  Plugins*/
            $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

            /*  Scripts*/
            $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                            = TRUE;

    }



    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function SigepeAsset()
    { 
        /* Carregando Estilos */
        $this->data['PackageStyles'][]    =   'Packages/Styles/Pessoa/GuestOffice/CadastroAtleta';


        /* Carregando Scripts */
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Pessoa/GuestOffice/CadastroPessoaFisica';
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Pessoa/GuestOffice/CadastroAtleta';
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Pessoa/GuestOffice/FormWizardCadastroAtleta';
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Pessoa/GuestOffice/Pessoa';


    }



     /*
             // @@@ Implementar permissões por usuario
        
        // AUDITORIA:
            [0]=> Slug Module
            [1]=> Table Database
            [2]=> Column Database
            [3]=> Value Deleted
            [4]=> Folder Thumb
            [5]=> Delete Gallery
            [6]=> Table Gallery
            [7]=> Column Table Gallery
            [8]=> Data/hora
            [9]=> IP
            [10]=> ID Usuário  
            [11]=> Type of Operation  
            */

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

