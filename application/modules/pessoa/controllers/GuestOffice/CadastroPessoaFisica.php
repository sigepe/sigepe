<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_GuestOffice.php';


/**
 * Classe responsavel por manipular o formulario de cadastro e processamento de pessoa fisica.
 */
class CadastroPessoaFisica extends MY_GuestOffice {

    public $data;



    /**
     * Metodo construtor da classe. Carrega na classe o ThemeComponent e o SigepeAssets
     *
     */
    function __construct() {

        parent::__construct();

        $this->ThemeComponent();
        $this->SigepeAssets(); 

    }



    /**
     * Funcao responsavel por exibir para o usuario o formulario de cadastro de pessoa fisica.
     *
     * @author Gustavo Botega 
     *
     */
    public function Formulario(){

        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']             =   get_class($this);
        $this->data['PageHeadTitle']                =   'Cadastro';
        $this->data['PageHeadSubtitle']             =   'Para se cadastrar no SIGEPE preencha o formulário abaixo.';
        $this->data['CodigoUsuarioMenorIdade']      =   $this->GerarCpf('2');


        /* Carrega View */
        $this->LoadTemplateGuest('Template/GuestOffice/Pessoa/PessoaFisica/Formulario', $this->data);
    } 



    /**
    * Processar
    *
    * @author Gustavo Botega 
    */
    public function Processar($Dados = false, $PessoaId){

        $Retorno        =   array();


        if(empty($Dados) || !$Dados || !is_array($Dados)){
            $Retorno['Status']   =   false;
            return $Retorno;
        }


        $Validar        =       $this->Validar($Dados, $PessoaId);
        if(!$Validar){
            $Retorno['Status']   =   false;
            return $Retorno;
        }


        $Gravar         =   $this->Gravar($Dados, $PessoaId);
        if(!$Gravar){
            $Retorno['Status']   =      false;
            return $Retorno;
        }


        $Retorno['Status']              =      true;
        $Retorno['PessoaFisicaId']      =      $Gravar['PessoaFisicaId'];
         return $Retorno;
    }




    /**
    * Validar
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function Validar($Dados){


        // FK Pessoa ID

        // FK Nacionalidade ID

        // Fk Escolaridade ID

        // FK Tipo Sanguineo ID

        // FK Naturalidade ID

        // FK Estado Civil ID

        // FK Genero ID

        // Flag CPF Isento

        // Primeiro Nome

        // Sobrenome

        // Data Nascimento

        // Nome Pai

        // Nome Mae

        // Nome Apelido


        return true;

    }








    /**
     * A summary informing the user what the associated element does.
     *
     * A *description*, that can span multiple lines, to go _in-depth_ into
     * the details of this element and to provide some background information
     * or textual references.
     *
     * @param array $Dados 
     * @param int $PessoaId 
     *
     * @return array
     */
    public function Gravar( $Dados, $PessoaId ){


        $Erro = array(); 


        // Carregando modulo autenticacao
        $this->load->module('autenticacao');


        // Autor
        $AutorId    =   NULL;   
        if( $this->session->userdata('Autenticado') )
            $AutorId  =   $this->session->userdata('PessoaId');


        // Data Nascimento Fundacao
        $DataNascimentoFundacao             =   $this->my_data->ConverterData($Dados['data-nascimento-fundacao'], 'PT-BR', 'ISO');


        // Foto
        (!isset($Dados['foto']) ? $Dados['foto'] = NULL : '' );

        // Nacionalidade
        (!isset($Dados['nacionalidade']) ? $Dados['nacionalidade'] = NULL : '' );

        // Escolaridade
        (!isset($Dados['escolaridade']) ? $Dados['escolaridade'] = NULL : '' );

        // Tipo Sanguineo
        (!isset($Dados['tipo-sanguineo']) ? $Dados['tipo-sanguineo'] = NULL : '' );

        // Naturalidade
        (!isset($Dados['naturalidade']) ? $Dados['naturalidade'] = NULL : '' );

        // Estado Civil
        (!isset($Dados['estado-civil']) ? $Dados['estado-civil'] = NULL : '' );

        // Genero
        (!isset($Dados['genero']) ? $Dados['genero'] = NULL : '' );

        // Flag CPF Isento
        (!isset($Dados['flag-cpf-isento'])) ? $Dados['flag-cpf-isento'] = NULL : '';

        // Nome Pai
        (!isset($Dados['nome-pai'])) ? $Dados['nome-pai'] = NULL : '';

        // Nome Mae
        (!isset($Dados['nome-mae'])) ? $Dados['nome-mae'] = NULL : '';

        // Apelido
        (!isset($Dados['apelido'])) ? $Dados['apelido'] = NULL : '';


        // Dados
        $Dados       =  array(
            'fk_aut_id'                         =>    $AutorId,
            'fk_pes_id'                         =>    $PessoaId,
            'fk_nac_id'                         =>    $Dados['nacionalidade'],
            'fk_esc_id'                         =>    $Dados['escolaridade'],
            'fk_tip_id'                         =>    $Dados['tipo-sanguineo'],
            'fk_nat_id'                         =>    $Dados['naturalidade'],
            'fk_est_id'                         =>    $Dados['estado-civil'],
            'fk_gen_id'                         =>    $Dados['genero'],
            'flag_cpf_isento'                   =>    $Dados['flag-cpf-isento'],
            'pef_primeiro_nome'                 =>    FirstWord($Dados['nome-razao-social']),
            'pef_sobrenome'                     =>    LastWord($Dados['nome-razao-social']),
            'pef_nome_pai'                      =>    $Dados['nome-pai'],
            'pef_nome_mae'                      =>    $Dados['nome-mae'],
            'pef_apelido'                       =>    $Dados['apelido'],
            'criado'                            =>    date("Y-m-d H:i:s")
        );


        /* Query */
        $Query = $this->db->insert('tb_pessoa_fisica', $Dados);
        if($Query){

            $Retorno = array(
                'Status'            =>  true,
                'DateTime'          =>  date("d/m/Y") . " às " . date("H:i:s"),
                'Ip'                =>  $this->input->ip_address(),
                'PessoaFisicaId'    =>  $this->db->insert_id()
            );
        }else{
            $Retorno['Status'] = false;
        }        


        return $Retorno;

    }

























    /**
     * FlagUsuario
     *
     * Verifica se a pessoa fisica esta com o flag de usuario ativo. Se sim retorna true se nao false.
     *
     * @author Gustavo Botega 
     * @return boolean
     */
    public function FlagUsuario($IdPessoa = NULL, $IdPessoaFisica = NULL, $Cpf = NULL)
    {

    }



    /**
     * ValidaSexo
     *
     * Verifica se a pessoa fisica esta com o flag de usuario ativo. Se sim retorna true se nao false.
     *
     * @author Gustavo Botega 
     * @return boolean
     */
    public function ValidaSexo($IdSexo)
    {

    }



    /**
     * Funcao responsavel por gerar um CPF aleatorio que seja relativo a um dos estados
     * Ṕará, Amazonas, Acre, Amapá, Rondônia e Roraima )
     *
     * @author Gustavo Botega 
     * @return boolean
     */
    public static function GerarCpf($mascara = "1") {

        $n1 = rand(0, 9);
        $n2 = rand(0, 9);
        $n3 = rand(0, 9);
        $n4 = rand(0, 9);
        $n5 = rand(0, 9);
        $n6 = rand(0, 9);
        $n7 = rand(0, 9);
        $n8 = rand(0, 9);
//        $n9 = rand(0, 9);
        $n9 = 2; // 2 = Ṕará, Amazonas, Acre, Amapá, Rondônia e Roraima
        $d1 = $n9 * 2 + $n8 * 3 + $n7 * 4 + $n6 * 5 + $n5 * 6 + $n4 * 7 + $n3 * 8 + $n2 * 9 + $n1 * 10;
        $d1 = 11 - (self::Mod($d1, 11) );
        if ($d1 >= 10) {
            $d1 = 0;
        }
        $d2 = $d1 * 2 + $n9 * 3 + $n8 * 4 + $n7 * 5 + $n6 * 6 + $n5 * 7 + $n4 * 8 + $n3 * 9 + $n2 * 10 + $n1 * 11;
        $d2 = 11 - (self::Mod($d2, 11) );
        if ($d2 >= 10) {
            $d2 = 0;
        }
        $retorno = '';
        if ($mascara == 1) {
            $retorno = '' . $n1 . $n2 . $n3 . "." . $n4 . $n5 . $n6 . "." . $n7 . $n8 . $n9 . "-" . $d1 . $d2;
        } else {
            $retorno = '' . $n1 . $n2 . $n3 . $n4 . $n5 . $n6 . $n7 . $n8 . $n9 . $d1 . $d2;
        }
        return $retorno;

    }



    /**
     * @param type $dividendo
     * @param type $divisor
     * @return type
     */
    private static function Mod($dividendo, $divisor) {
        return round($dividendo - (floor($dividendo / $divisor) * $divisor));
    }


    /**
    * ThemeComponent
    */
    public function ThemeComponent()
    { 
        $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
        $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;
    
        $this->data['ScriptsFile']['Plugins']['select2']                                    = TRUE;
        $this->data['ScriptsFile']['Plugins']['jquery-validation']                          = TRUE;
        $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']       = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                           = TRUE;
        $this->data['ScriptsFile']['Plugins']['input-mask']                                 = TRUE;
        $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                         = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                       = TRUE;
        $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                              = TRUE;
    }


    /**
    * SigepeAssets
    */
    public function SigepeAssets()
    { 
        $this->data['PackageStyles'][]    =   'Packages/Styles/Pessoa/GuestOffice/CadastroPessoaFisica';
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Pessoa/GuestOffice/CadastroPessoaFisica';
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Pessoa/GuestOffice/FormWizardCadastroPessoaFisica';
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Pessoa/GuestOffice/Pessoa';
    }



}

/* End of file CadastroPessoaFisica.php */
/* Location: ./application/modules/pessoa/controllers/GuestOffice/CadastroPessoaFisica.php */

