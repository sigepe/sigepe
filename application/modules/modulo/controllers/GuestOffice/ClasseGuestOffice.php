<?php

if (!defined('BASEPATH')) { exit('No direct script access allowed'); }
    
include_once APPPATH . '/core/MY_GuestOffice.php';
class ClasseGuestOffice extends MY_GuestOffice {

    public $modulo = "Faq";
    public $tabelaNome = "tb_tabela";
    public $viewNome = "vw_view";
    public $columnId = "tab_id";
    public $columnDesc = "tab_nome";    

    function __construct() {
        parent::__construct();
        $this->load->model('model_crud');
    }

    public function index(){
        $minId = $this->db->query("SELECT min($this->columnId) as min_id from $this->viewNome ")->result();
        $minId = $minId[0];
        echo json_decode($this->obter($minId));
    }

    public function listar(){
        echo json_encode($this->db->query("SELECT * from $this->viewNome order by $this->columnId")->result());
    }

    public function listarComCondicao(){
        $args = $this->getOrderBy(func_get_args());
        #echo json_encode($this->db->query("SELECT * from $this->viewNome order by $args")->result());   
    }

    public function listarComOrdem(){
        $args = $this->getOrderBy(func_get_args());
        echo json_encode($this->db->query("SELECT * from $this->viewNome order by $args")->result());   
    }

    public function obter($id){
        echo json_encode($this->model_crud->get_rowSpecificObject($this->viewNome, $this->columnId, $id));
    }

    /* Prepared Statement */
    public function getOrderBy($args){        
        if (sizeof($args) == 0){
            return $this->columnId . " asc";
        }        

        $order = "";

        foreach ($args as $arg) {
            $order .= $arg.", ";
        }
        $order = str_replace(", asc,", " asc,", $order);
        $order = str_replace(", desc,", " desc,", $order);
        return substr($order, 0, -2);
    }

} /* End of file classe.php */