<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Testes extends MY_BackOffice {

    public $data;
    public $ambiente = "BackOffice";
    public $modulo = "Nillander";
    public $tabelaNome = "tb_nill_testes";
    public $viewNome = "vw_nill_testes";
    public $columnId = "tes_id";
    public $columnDesc = "tes_descricao";

    /* url de acesso*/
    // http://localhost/sigepe/nillander/BackOffice/Testes/

    function __construct() {
        parent::__construct();

        $this->metronicAsset();
        $this->sigepeAsset();

        /* Setando variaveis de ambiente */
        $this->data['PageHeadTitle'] = "Testes";
        $this->data['PageHeadSubtitle'] = "Nillander";


        $modulo = "Nillander";
        $this->data['ShowSidebar'] = true;
        $this->data['ShowColumnLeft'] = "Profile" . $modulo;
        $this->data['base_dir'] = "Template/BackOffice/sistema/$modulo/";
        $this->data['Class'] = $this->router->fetch_class();

        /* Controladores Externos */
        #$this->load->module('evento/XOffice/Evento');
    }

    public function index() {
        $this->testes_browse_bread();
    }

    /**
     * testes_browse_bread.
     * @author Nillander
     * @return
     */
    public function testes_browse_bread() {
        $sql = "SELECT * from vanguarda_tools.tb_navitem;";
        $dataset = $this->db->query($sql)->result();
        $this->data['dataset'] = $dataset;
        /* View */
        $this->NillTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);
    }

// fim do metodo @testes_browse_bread

    /**
     * testes_read
     * @author Nillander
     * @return
     */
    public function testes_read($contextoId) {
        $sql = "SELECT * FROM $this->viewNome where $this->columnId = $contextoId";
        $dataset = $this->db->query($sql)->result();
        $this->data['dataset'] = $dataset;
        /* View */
        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);
    }

// fim do metodo @testes_read

    /**
     * testes_add
     * @author Nillander
     * @return
     */
    public function testes_add() {
        /* View */
        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);
    }

// fim do méotodo @testes_add

    /**
     * testes_edit
     * @author Nillander
     * @return
     */
    public function testes_edit() {
        /* View */
        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);
    }

// fim do méotodo @testes_edit



    /* # Fim Bread
      # Inicio Middleware
      ==================================================================================================================== */

    /**
     * processar
     * @author Nillander
     * @param $dataJson
     * @return
     */
    public function processar($dataJson = false) {
        if (isset($_POST['dataJson']) && is_array($_POST['dataJson'])) {
            $dados = $_POST['dataJson'];
        }

        if ($dataJson) {
            $dados = $dataJson;
        }

        if (!isset($_POST['dataJson']) && !$dataJson) {
            echo json_encode(false);
        }

        // Validacao Formulario
        $validar = $this->validar($dados);
        if (!$validar) {
            echo json_encode(false);
            return false;
        }

        // Se existir algum ID por convencao entende-se que o metodo a ser direcionado e de atualizacao.
        if (!isset($dados['id'])) {
            $resultadoAcao = $this->gravar($dados);
            echo ($resultadoAcao) ? json_encode(true) : json_encode(false);
        } else {
            $resultadoAlterar = $this->alterar($dados);
            echo ($resultadoAlterar) ? json_encode(true) : json_encode(false);
        }
    }

// fim do metodo @processar

    /**
     * gravar
     * @author Nillander
     * @param $dados
     * @return
     */
    public function gravar($dados) {
        foreach ($dados as $key => $value) {
            if (empty($value))
                $dados[$key] = NULL;
        }

        // dados
        $dataset = array(
            'fk_aut_id' => $this->session->userdata('PessoaId'),
            'tab_coluna' => gerarTimeStamp("tb_tabela", "tab_coluna"),
            'eve_slug' => gerarSlug($dados['eve-nome-evento']),
            'criado' => date("Y-m-d H:i:s")
        );

        /* query */
        $query = $this->db->insert($this->tabelaNome, $dataset);

        if ($query) {
            $classeId = $this->db->insert_id();
            return true;
        } else {
            return false;
        }
    }

// fim do metodo @gravar

    /**
     * alterar.
     *
     * -
     *
     * @author Nillander
     * @param $dados
     * @return
     */
    public function alterar($dados) {

        (!isset($dados['campo_null'])) ? $dados['campo_null'] = null : '';

        // dados
        $dataset = array(
            $this->columnId => $dados['id'],
            'fk_aut_id' => $this->session->userdata('PessoaId'),
            'tab_coluna' => gerarTimeStamp("tb_tabela", "tab_coluna"),
            'eve_slug' => gerarSlug($dados['name-objeto']),
            'criado' => date("Y-m-d H:i:s")
        );

        /* Query */
        $query = $this->db->update($this->tabelaNome, $dataset, array($this->columnId => $dados['id']));
        return $query;
    }

// fim do metodo @alterar

    /**
     * validar
     * @author Nillander
     * @param $dados
     * @return
     */
    public function validar($dados) {
        return true; // Validar inputs e regras de negocio do formulario. Tratar erro.
    }

// fim do metodo @validar



    /* # Fim Middleware
      # Inicio Acessório
      ==================================================================================================================== */

    /**
     * fazerAlgumaCoisa.
     * @author Nillander
     * @param $contextoId
     * @return
     */
    public function fazerAlgumaCoisa($contextoId) {
        
    }

    /* # Fim Metodo Acessório
      # Inicio Asset
      ========================================================================= */

    /**
     * metronicAsset.
     * @author Nillander
     * @param 
     * @return
     */
    public function metronicAsset() {
        /* STYLES */
        // PLUGINS //
        /* {NamePackage} */
        #$this->data['StylesFile']['Plugins']['nome-do-componente'] = TRUE;
        // STYLES //
        /* {NamePackage} */
        #$this->data['StylesFile']['Styles']['nome-do-componente'] = TRUE;


        /* SCRIPTS */
        // PLUGINS  //
        /* {NamePackage} */
        #$this->data['ScriptsFile']['Plugins']['nome-do-componente'] = TRUE;
        // SCRIPTS //
        #$this->data['ScriptsFile']['Scripts']['nome-do-componente'] = TRUE;
    }

// fim do metodo MetronicAsset

    /**
     * sigepeAsset.
     * @author Nillander
     * @param 
     * @return
     */
    public function sigepeAsset() {
        /* Carregando Estilos */
        $this->data['PackageStyles'][] = "Packages/Styles/$this->modulo/$this->ambiente/". __CLASS__ ."";

        /* Carregando Scripts */
        $this->data['PackageScripts'][] = "Packages/Scripts/$this->modulo/$this->ambiente/". __CLASS__ ."";
    }

// fim do metodo sigepeAsset
}

/* End of file classe.php */
