<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';

class Dashboard extends MY_FrontOffice {

    public $data;

    function __construct() {
        parent::__construct();

        $this->clear_cache();
        
    }


    public function index() {

        date_default_timezone_set('America/Sao_Paulo');

        $Saudacao                       =   GetSaudacao();
        $PrimeiroNome                   =   $this->session->userdata('PessoaPrimeiroNome');

        $Settings['PageHeadTitle']      =   ucfirst(strtolower($PrimeiroNome)) . ", " . $Saudacao ." Tudo bem?";
//        $Settings['PageHeadSubtitle']   = 'Seu último acesso foi às 18h40 em 16/04/2017.';
        $Settings['PageHeadSubtitle']   = '';

        $Settings['Class']    =   $this->router->fetch_class();

        $this->LoadTemplateUser('Template/FrontOffice/sistema/dashboard/dashboard', $Settings);

    }

    

    function clear_cache()
    {
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
    }    



    /***********************************************    
      User
    ***********************************************/
    public function Dashb() {

        $this->UsuarioAutenticado();
        $this->LoadAuthentication('user', $this->data);            

    }


    /***********************************************    
      UsuarioAutenticado
    ***********************************************/
    public function UsuarioAutenticado() {


        if($this->session->userdata('Autenticado'))
            redirect(base_url().'dashboard');

    }



    /**
     * Funcao recebe uma requisicao Ajax.
     * Atencao: a funcao nao recebe nenhum valor, porem sao resgatados por $_POST
     * A funcao recebe o valor do cpf e verifica se existe alguma ocorrencia na tabela tb_pessoa
     * se existir ele retorna com algumas informacoes.
     *
     * @author Gustavo Botega 
     * @param $_POST['cpfWithoutMask'] - recebe valor do CPF com o valor formatado. Valor contem 11 digitos.    
     * @return string JSON
     */      
    public function verifyUser() {


        /* CPF */
        $cpf    =   $_POST['cpfWithoutMask'];

        // Checar se existe na tabela tb_pessoa alguem com o cpf informado
        $query = $this->model_crud->select('tb_pessoa', array('pes_id', 'fk_sta_id', 'pes_nome', 'pes_cpf', 'pes_foto'), array('pes_cpf ='=>$cpf), NULL, 1 );

        /* Verificacao da consulta*/
        if( !empty($query) ){

            $array = array(
                'pes_id'        =>  $query[0]->pes_id,
                'fk_sta_id'     =>  $query[0]->fk_sta_id,
                'pes_nome'      =>  $query[0]->pes_nome,
                'pes_cpf'       =>  $query[0]->pes_cpf,
                'pes_foto'      =>  $query[0]->pes_foto,
                'status'        =>  'processed'
            );

        }else{

            $array = array(
                'status'    =>  'unprocessed'
            );            

        }


        /**
         * Saida
         */
        echo json_encode($array);

    }



    /**
     * Funcao recebe uma requisicao Ajax.
     * Atencao: a funcao nao recebe nenhum valor, porem sao resgatados por $_POST
     * A funcao recebe o valor do cpf e verifica se existe alguma ocorrencia na tabela tb_pessoa
     * se existir ele retorna com algumas informacoes.
     *
     * @author Gustavo Botega 
     * @param $_POST['cpfWithoutMask'] - recebe valor do CPF com o valor formatado. Valor contem 11 digitos.    
     * @return string JSON
     */      
    public function Processar() {

        $ArrayDataSerialized        = $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $DataUnserialized);


        /**
         * Resgatando valores por $_POST e armazenando em variaveis
         */      
        $Cpf         =   $this->my_pessoa->RemoverPontuacaoCpfCnpj($DataUnserialized['cpf']);
        $Senha       =   $DataUnserialized['senha'];



        /**
         * Setando valor do $token
         */      
        $Token              =   '7f6e1612-cf50-4394-8445-235f99392fca'; // Criar module seguranca e puxar valor do token de la > 7f6e1612-cf50-4394-8445-235f99392fca   
        $SenhaToken         =   $Token . $Senha;


        /**
         * Encriptando senha
         */      
        $SenhaEncriptada  =   md5($SenhaToken);


        /**
         * SQL
         */      
        $Sql    =   "
                        SELECT * FROM tb_pessoa  
                        WHERE pes_cpf_cnpj = '".$Cpf."'
                        AND pes_senha = '".$SenhaEncriptada."'
                        LIMIT 1
                    ";
        $Query  =   $this->db->query($Sql)->result();

        /**
         * Checa se a query nao retornou vazio
         */      
        if( !empty($Query) && $Query ){

            /**
             * Apenas e registrado a sessao se o status do USUARIO for ATIVO.
             */      
            if($Query[0]->fk_sta_id=='1' && $Query[0]->flag_usuario == '1'){
                $this->SetSession(
                    $Query[0]->pes_id,
                    $Query[0]->pes_cpf_cnpj,
                    $Query[0]->pes_foto,
                    $Query[0]->pes_nome_razao_social,
                    $Query[0]->flag_usuario
                );
            }

            $array = array(
                'fk_sta_id'     =>  $Query[0]->fk_sta_id, // status do usuario. 
                'Status'        =>  TRUE,
                'FlagUsuario'   =>  $Query[0]->flag_usuario
            );

        }else{

            $array = array(
                'Status'        =>  FALSE
            );            

        }


        /**
         * Saida
         */
        echo json_encode($array);

    }



    public function settings()
    {

        $data           = array();
        $data['token']  =   't7nr';

        return $data;
    }






    /**
     * Set Session   
     *
     *
     * @author Gustavo Botega 
     * @param int $userId -    
     * @return TRUE 
     */      
    public function SetSession($PessoaId, $PessoaCpf, $PessoaFoto, $PessoaNomeCompleto, $FlagUsuario)
    {

        /* RETORNAR ARRAY COM OS DADOS */
        $Array = array(
            'PessoaId'            =>      $PessoaId,
            'PessoaCpf'           =>      $PessoaCpf,
            'PessoaFoto'          =>      $PessoaFoto,
            'PessoaPrimeiroNome'  =>      $this->my_pessoa_fisica->GetPrimeiroNome($PessoaNomeCompleto),
            'PessoaFlagUsuario'   =>      $FlagUsuario,
            'Autenticado'         =>      TRUE
        );

        $this->session->set_userdata($Array);

        return TRUE;
    }





    public function logout()
    {

        // # destruindo sessao do code Igniter 
        $this->session->sess_destroy(); 

        // O usuario seve ser redirecionado pra a tela de login .
        redirect(base_url() . 'authentication/authentication/index', 'refresh');

    }






    /**
     * Mostra a estrutura do template 
     * @param string $pathView
     */
    protected function LoadAuthentication( $pathView = 'template/authentication/', $settings = NULL )
    {
        $data = array();

        $settings['base_url']   =   base_url();
        $data['settings']       =   $settings;

        /*Settings variables */
        $this->parser->parse('template/authentication/header', $data['settings']);
        $this->parser->parse( $pathView, $data['settings']);
        $this->parser->parse('template/authentication/footer', $data['settings']);
    }


}
