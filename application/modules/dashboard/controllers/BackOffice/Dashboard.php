<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_BackOffice.php';

class Dashboard extends MY_BackOffice {

    public $data;


    function __construct() {
        parent::__construct();
    }



    public function index() {

        date_default_timezone_set('America/Sao_Paulo');

        $Saudacao                       =   GetSaudacao();
        $PrimeiroNome                   =   $this->session->userdata('PessoaPrimeiroNome');

        $Settings['ShowSidebar']      	=   TRUE;
        $Settings['PageHeadTitle']      =   ucfirst(strtolower($PrimeiroNome)) . ", " . $Saudacao ." Tudo bem?";
  //        $Settings['PageHeadSubtitle']   = 'Seu último acesso foi às 18h40 em 16/04/2017.';
        $Settings['PageHeadSubtitle']   = '';

        $Settings['Class']    =   $this->router->fetch_class();

        $this->LoadTemplate('Template/BackOffice/sistema/dashboard/dashboard', $Settings);

    }




    /**
    * PackagesClass
    *
    * @author Gustavo Botega
    */
    public function ThemeComponent(){


        /*
            StylesFile
        */
            /*  Plugins*/
            $this->data['StylesFile']['Plugins']['datetime']                                     = TRUE;
            $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete']                           = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                    = TRUE;
            $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;

            /*  Syles*/
    //            $this->data['StylesFile']['Styles']['profile']                                = TRUE;



        /*
            ScriptsFile
        */
            /*  Plugins*/
            $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
            $this->data['ScriptsFile']['Plugins']['easy-autocomplete']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

            /*  Scripts*/
            $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                            = TRUE;

    }



    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega
    * @return array
    */
    public function SigepeAsset()
    {
        /* Carregando Estilos */
  //        $this->data['PackageStyles'][]    =   'packages/frontoffice/style/cadastro-animal';


        /* Carregando Scripts */
  //        $this->data['PackageScripts'][]   =   'packages/frontoffice/script/animal';


    }






}
