<?php

if (!defined('BASEPATH')) { exit('No direct script access allowed'); }
    
    include_once APPPATH . '/core/MY_FrontOffice.php';

class Animal extends MY_FrontOffice {

    public $data;
    public $tableNome = "tb_animal";
    public $viewNome = "vw_animal";
    public $columnId = "ani_id";
    public $columnDesc = "ani_nome_completo";   

    function __construct() {
        parent::__construct();
        $this->metronicAsset();
        $this->sigepeAsset();
        $this->data['ShowColumnLeft'] = "ProfileAnimal";
        $this->data['base_dir'] = 'Template/FrontOffice/sistema/Animal/';
    }



    public function index() {

    }



    /**
    * classe_browse_bread.
    *
    *
    * @author {Author}
    *
    * @return view
    */
    public function animal_read_re($animalId) {
        $this->data['ClassEnvironment'] = get_class($this);
        $this->data['PageHeadTitle'] = 'Perfil do Animal';
        $this->data['PageHeadSubtitle'] = 'Visualize e gerencie o perfil do animal. Mantenha seus dados sempre atualizados.';
        $this->data['NavActiveSidebar'] = 'PerfilCompleto';

        $PessoaId = $this->session->userdata('PessoaId');

        $animal = array(json_decode(file_get_contents(base_url()."animal/GuestOffice/AnimalController/obter/$animalId")));
        $this->data['AnimalId'] = $animalId;
        $this->data['animal'] = $animal;



        // Raca
        $SqlRaca = "SELECT * FROM tb_animal_raca ORDER BY anr_raca ASC";
        $this->data['arrRaca'] = $this->db->query($SqlRaca)->result();

        // Pelagem
        $SqlPelagem = "SELECT * FROM tb_animal_pelagem ORDER BY anp_pelagem ASC";
        $this->data['arrPelagem'] = $this->db->query($SqlPelagem)->result();


        // Genero
        $SqlGenero = "SELECT * FROM tb_animal_genero ORDER BY ang_genero ASC";
        $this->data['arrGenero'] = $this->db->query($SqlGenero)->result();


        // Genero Tipo
        $SqlGeneroTipo = "SELECT * FROM tb_animal_genero_tipo ORDER BY agt_tipo ASC";
        $this->data['arrGeneroTipo'] = $this->db->query($SqlGeneroTipo)->result();


        // Nome Associacao de Registro
        $SqlAssociacao = 'SELECT * FROM tb_pessoa as pes INNER JOIN tb_vinculo as vin ON pes.pes_id = vin.fk_pes1_id WHERE vin.fk_per_id = 15 AND vin.fk_tip_id IS NULL AND vin.fk_sta_id = 1 AND pes.fk_sta_id = 1 ORDER BY pes.pes_nome_razao_social asc ';
        $this->data['arrAssociacao'] = $this->db->query($SqlAssociacao)->result();


        // Pais
        $SqlPais = 'SELECT * FROM tb_pais as pai ORDER BY pai.pai_posicao ASC';
        $this->data['arrPais'] = $this->db->query($SqlPais)->result();


        /* Carrega View */
        $this->LoadTemplate('Template/FrontOffice/sistema/Animal/Animal/animal_read_re', $this->data);
    }


    
    /**
    * classe_read
    *
    * @author {Author}
    *
    * @return view
    */
    public function classe_read($contextoId) {

        $sql = "";
        $dataset = $this->db->query($sql)->result();

        $this->data['dataset'] = $dataset;

        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);

    } // fim do metodo @classe_read



    /**
    * classe_add
    *
    * @author {Author}
    *
    * @return 
    */
    public function classe_add($eventoId) {

        /* Setando variaveis de ambiente */
        $this->data['PageHeadTitle']      = 'Cadastro';
        $this->data['PageHeadSubtitle']   = 'Para cadastrar uma classe no SIGEPE preencha o formulário abaixo.';

        /* View */ $this->LoadTemplate($this->data['base_dir'].__CLASS__."/".__FUNCTION__, $this->data);

    } // fim do méotodo @classe_add



    /* # Fim Bread
       # Inicio Middleware
    ====================================================================================================================*/



    /**
    * processar.
    *
    * -
    *
    * @author {Author}
    *
    * @param $dataJson
    *
    * @return boolean
    */
    public function processar($dataJson = false) {

        if(isset($_POST['dataJson']) && is_array($_POST['dataJson'])) {
            $dados = $_POST['dataJson'];
        }

        if($dataJson) {
            $dados = $dataJson;
        }

        if(!isset($_POST['dataJson']) && !$dataJson) {
            echo json_encode(false);
        }

        // Validacao Formulario
        $validar = $this->validar($dados);
        if (!$validar) {
            echo json_encode(false);
            return false;
        }

        // Se existir algum ID por convencao entende-se que o metodo a ser direcionado e de atualizacao.
        if(!isset($dados['id'])) {
            $resultadoAcao = $this->gravar($dados);
            echo ($resultadoAcao) ? json_encode(true) : json_encode(false);
        } else {
            $resultadoAlterar = $this->alterar($dados);
            echo ($resultadoAlterar) ? json_encode(true) : json_encode(false);
        }

    } // fim do metodo @processar




    /**
    * gravar.
    *
    * -
    *
    * @author {Author}
    *
    * @param $dados
    *
    * @return boolean
    */
    public function gravar($dados) {

        foreach ($dados as $key => $value) {
            if(empty($value))
                $dados[$key]    =   NULL;
        }

        // dados
        $dataset       =  array(
            'fk_aut_id'             => $this->session->userdata('PessoaId'),
            'tab_coluna'            => gerarTimeStamp("tb_tabela", "tab_coluna"),
            'eve_slug'              => gerarSlug($dados['eve-nome-evento']),
            'criado'                => date("Y-m-d H:i:s")
        );


        /* query */
        $query = $this->db->insert('tb_tabela', $dataset);

        if ($query){
            #$classeId = $this->db->insert_id();
            return true;
        } else {
            return false;
        }

    } // fim do metodo @gravar



    /**
    * alterar.
    *
    * -
    *
    * @author {Author}
    *
    * @param $dados
    *
    * @return boolean
    */
    public function alterar($dados) {

        // dados
        $dataset       =  array(
            'ani_id'                 => $dados['id'],
            'ani_peso'               => $dados['peso'],
            'ani_nome_patrocinado'   => $dados['nome-patrocinado'],
            'modificado'             => date("Y-m-d H:i:s")
        );

        /* Query */
        $query = $this->db->update($this->tableNome, $dataset, array($this->columnId => $dados['id'] ));
        return $query;

    } // fim do metodo @alterar



    /**
    * validar.
    *
    * -
    *
    * @author {Author}
    *
    * @param $dados
    *
    * @return boolean
    */
    public function validar($dados) {
        return true; // Validar inputs e regras de negocio do formulario. Tratar erro.
    } // fim do metodo @validar



    /* # Fim Middleware
       # Inicio Asset 
    =========================================================================*/
    /**
    * metronicAsset.
    *
    * -
    *
    * @author {Author}
    *
    * @param 
    *
    * @return
    */
    public function metronicAsset() {
        /* StylesFile */
        /* Plugins */
        $this->data['StylesFile']['Plugins']['select2'] = TRUE;
        $this->data['StylesFile']['Plugins']['bootstrap-sweetalert'] = TRUE;
        $this->data['StylesFile']['Plugins']['easy-autocomplete'] = TRUE;
        $this->data['StylesFile']['Plugins']['easy-autocomplete-themes'] = TRUE;
        $this->data['StylesFile']['Styles']['todo-2'] = TRUE;

        /*  Syles */
        $this->data['StylesFile']['Styles']['profile'] = TRUE;



        /* ScriptsFile */
        /* Plugins */
        $this->data['ScriptsFile']['Plugins']['select2'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['jquery-validation'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-wizard'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['input-mask'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['input-mask-trigger'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['easy-autocomplete'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert'] = TRUE;

        /*  Scripts */
        $this->data['ScriptsFile']['Scripts']['ui-sweetalert'] = TRUE;
    } // fim do metodo MetronicAsset



    /**
    * sigepeAsset.
    *
    * -
    *
    * @author {Author}
    *
    * @param 
    *
    * @return
    */
    public function sigepeAsset() {
    
        /* Carregando Estilos */
        $this->data['PackageStyles'][]   =   'Packages/Styles/Animal/FrontOffice/Perfil';
        
        /* Carregando Scripts */
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Animal/FrontOffice/Perfil';
    
    } // fim do metodo sigepeAsset

} /* End of file classe.php */