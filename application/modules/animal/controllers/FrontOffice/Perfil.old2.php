<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

include_once APPPATH . '/core/MY_FrontOffice.php';

class Perfil extends MY_FrontOffice {

    public $data;

    function __construct() {

        parent::__construct();

        $this->ThemeComponent();
        $this->SigepeAsset();
        $this->data['ShowColumnLeft'] = "ProfileAnimal";
    }

    public function index() {
        
    }

    /**
     * PerfilCompleto
     *
     * @author Nillander Alarcão 
     */
    public function PerfilCompleto($animalId) {
        $this->data['ClassEnvironment'] = get_class($this);
        $this->data['PageHeadTitle'] = 'Perfil do Animal';
        $this->data['PageHeadSubtitle'] = 'Visualize e gerencie o perfil do animal. Mantenha seus dados sempre atualizados.';
        $this->data['NavActiveSidebar'] = 'PerfilCompleto';

        $PessoaId = $this->session->userdata('PessoaId');

        $animal = $this->db->query("SELECT * FROM vw_animal where ani_id = $animalId")->result();
        $this->data['AnimalId'] = $animalId;
        $this->data['animal'] = $animal;




        // Raca
        $SqlRaca = "SELECT * FROM tb_animal_raca ORDER BY anr_raca ASC";
        $this->data['arrRaca'] = $this->db->query($SqlRaca)->result();

        // Pelagem
        $SqlPelagem = "SELECT * FROM tb_animal_pelagem ORDER BY anp_pelagem ASC";
        $this->data['arrPelagem'] = $this->db->query($SqlPelagem)->result();


        // Genero
        $SqlGenero = "SELECT * FROM tb_animal_genero ORDER BY ang_genero ASC";
        $this->data['arrGenero'] = $this->db->query($SqlGenero)->result();


        // Genero Tipo
        $SqlGeneroTipo = "SELECT * FROM tb_animal_genero_tipo ORDER BY agt_tipo ASC";
        $this->data['arrGeneroTipo'] = $this->db->query($SqlGeneroTipo)->result();


        // Nome Associacao de Registro
        $SqlAssociacao = 'SELECT * FROM tb_pessoa as pes INNER JOIN tb_vinculo as vin ON pes.pes_id = vin.fk_pes1_id WHERE vin.fk_per_id = 15 AND vin.fk_tip_id IS NULL AND vin.fk_sta_id = 1 AND pes.fk_sta_id = 1 ORDER BY pes.pes_nome_razao_social asc ';
        $this->data['arrAssociacao'] = $this->db->query($SqlAssociacao)->result();


        // Pais
        $SqlPais = 'SELECT * FROM tb_pais as pai ORDER BY pai.pai_posicao ASC';
        $this->data['arrPais'] = $this->db->query($SqlPais)->result();


        /* Carrega View */
        $this->LoadTemplate('Template/FrontOffice/sistema/Animal/Perfil/animal_read', $this->data);
    }

    public function AjaxProcessar() {
        $ArrayDataSerialized = $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);

        $Processar = $this->Processar(true, $Dados);

        echo json_encode($Processar);
    }

    public function ThemeComponent() {
        /* StylesFile */
        /* Plugins */
        $this->data['StylesFile']['Plugins']['select2'] = TRUE;
        $this->data['StylesFile']['Plugins']['bootstrap-sweetalert'] = TRUE;
        $this->data['StylesFile']['Plugins']['easy-autocomplete'] = TRUE;
        $this->data['StylesFile']['Plugins']['easy-autocomplete-themes'] = TRUE;
        $this->data['StylesFile']['Styles']['todo-2'] = TRUE;

        /*  Syles */
        $this->data['StylesFile']['Styles']['profile'] = TRUE;



        /* ScriptsFile */
        /* Plugins */
        $this->data['ScriptsFile']['Plugins']['select2'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['jquery-validation'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-wizard'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['input-mask'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['input-mask-trigger'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['easy-autocomplete'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert'] = TRUE;

        /*  Scripts */
        $this->data['ScriptsFile']['Scripts']['ui-sweetalert'] = TRUE;
    }

    public function SigepeAsset() {
        $this->data['PackageScripts'][] = 'Packages/Scripts/Animal/FrontOffice/Perfil';
    }

}
