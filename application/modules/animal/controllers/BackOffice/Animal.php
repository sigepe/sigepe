<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_BackOffice.php';
class Animal extends MY_BackOffice {

    public $data;

    function __construct() {
    
        parent::__construct();

        $this->InformacoesTemplate();
        $this->data['Class']    =   $this->router->fetch_class();
        $this->ThemeComponent();
        $this->SigepeAsset();

        $this->data['ShowSidebar'] = TRUE;
        $this->data['PageHeadTitle'] = "Animais";
        $this->data['PageHeadSubtitle'] = "";

    }

    public function index() {
        $this->animal_browse();
    } // end of method


    public function InformacoesTemplate(){
        
    } // end of method


    public function animal_browse(){
        $sqlAnimal = "SELECT * from vw_animal;";

        $this->data['DataSetAnimal'] = $this->db->query($sqlAnimal)->result();
        $this->LoadTemplate('Template/BackOffice/sistema/Animal/Listagem/TodosAnimais', $this->data);
    } // end of method


    public function animal_fhbr_browse(){
        $sqlAnimal = "SELECT * from vw_animal where fk_pjf_id = 141";

        $this->data['DataSetAnimal'] = $this->db->query($sqlAnimal)->result();
        $this->LoadTemplate('Template/BackOffice/sistema/Animal/Listagem/AnimaisFhbr', $this->data);
    }

    public function animal_outra_federacao(){
        $sqlAnimal = "SELECT * from vw_animal where fk_pjf_id != 141";

        $this->data['DataSetAnimal'] = $this->db->query($sqlAnimal)->result();
        $this->LoadTemplate('Template/BackOffice/sistema/Animal/Listagem/OutrasFederacoes', $this->data);
    }


    public function ThemeComponent(){
        $this->data['StylesFile']['Plugins']['datatables'] = TRUE;
        /* Styles */
        $this->data['StylesFile']['Plugins']['datatables-bootstrap'] = TRUE;


        /* Scripts */
        $this->data['ScriptsFile']['Plugins']['datatable'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['datatables'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['datatables-bootstrap'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-datepicker'] = TRUE;
        $this->data['ScriptsFile']['Scripts']['table-datatables-buttons'] = TRUE;
        $this->data['ScriptsFile']['Scripts']['table-datatables-fixedheader'] = TRUE;
    } // end of method


    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega
    * @return array
    */
    public function SigepeAsset()
    {
        /* Carregando Estilos */


        /* Carregando Scripts */
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Evento/BackOffice/ConsultaEvento';
    } // end of method
} // end of Class
// end of file