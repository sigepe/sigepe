<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Guest extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();


        /* Package */
        $this->PackagesClass(); // TROCAR PARA ThemeComponent();
        $this->PackagesAssets();  // TROCAR PARA SigepeAssets();
        $this->data['TesteClasse']                =   'GuestOffice';



    }

    public function index() {
		$this->gear();    	
    }

    public function CadastrarPessoa(){

        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Cadastro';
        $this->data['PageHeadSubtitle']   = 'Para se cadastrar no SIGEPE preencha o formulário abaixo.';
        $this->data['Breadcrumbs']        = array();

        /* Carrega View */
        $this->LoadTemplateGuest('template/guest/register/generic/form', $this->data);

    }


    public function CadastrarAtleta(){

        $this->LoadTemplateGuest('template/user/main');
    }




    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function PackagesClass() // Trocar nome da function para ThemeComponent()
    { 

     // Trocar nome da function para ThemeComponent()


        /*
            
            NO SABADO 22/07 fazer com o que a view processe e imprima os valores abaixo.

        */

        /* 
            StylesFile 
        */
            /*  Plugins*/
            $this->data['StylesFile']['Plugins']['select2']                = TRUE;
        
            /*  Syles*/
    //            $this->data['StylesFile']['Styles']['profile']                 = TRUE;



        /* 
            ScriptsFile 
        */
            /*  Plugins*/
            $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;

            /*  Scripts*/
            $this->data['ScriptsFile']['Scripts']['form-wizard']                              = TRUE;

    }


    /**
    * PackagesAssets
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function PackagesAssets()
    { 
        /* Carregando Estilos */
        $this->data['PackageStyles'][]    =   'packages/guestoffice/style/cadastro-ordinario';


        /* Carregando Scripts */
        $this->data['PackageScripts'][]   =   'packages/guestoffice/script/cadastro-ordinario';
        $this->data['PackageScripts'][]   =   'packages/guestoffice/script/form-wizard';
        $this->data['PackageScripts'][]   =   'packages/guestoffice/script/pessoa';


    }



     /*
             // @@@ Implementar permissões por usuario
        
        // AUDITORIA:
            [0]=> Slug Module
            [1]=> Table Database
            [2]=> Column Database
            [3]=> Value Deleted
            [4]=> Folder Thumb
            [5]=> Delete Gallery
            [6]=> Table Gallery
            [7]=> Column Table Gallery
            [8]=> Data/hora
            [9]=> IP
            [10]=> ID Usuário  
            [11]=> Type of Operation  
            */

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

