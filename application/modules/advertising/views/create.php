
<script type="text/javascript">
var baseUrl			=	'<?php echo base_url(); ?>';
/*
var table			=	'<?php echo $settings["table"]; ?>';
var columnId		=	'<?php echo $settings["columnId"]; ?>';
var thumb			=	'<?php echo $settings["thumb"]; ?>';
var directoryThumb	=	'<?php echo $settings["directoryThumb"]; ?>';
var columnThumb		=	'<?php echo $settings["columnThumb"]; ?>';
var rulesDetails	=	'<?php echo $settings["rulesDetails"]; ?>';
var gallery			=	'';
var moduleSlug		=	'<?php echo $settings["moduleSlug"]; ?>';
var module			=	'module';
var idUser   		=	'idUserValue';
*/
</script>

<!-- BEGIN Header Btns -->
<?php if($createHeaderFlag): ?>
<hr class="gray" />  

<div class="header-buttons">
	<?php foreach ($settings['createHeaderBtn'] as $createHeaderBtn): ?>
		<?php $this->inputmounting->btn($createHeaderBtn); ?>
	<?php endforeach; ?>
</div>

<hr class="gray" />  
<?php endif; ?>
<!-- END Header Btns -->


<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-equalizer font-blue-hoki"></i>
			<span class="caption-subject font-blue-hoki bold uppercase">Formulário</span>
			<span class="caption-helper">Novo Post</span>
		</div>
		<div class="tools">
			<a href="" class="collapse">
			</a>
			<a href="#portlet-config" data-toggle="modal" class="config">
			</a>
			<a href="" class="reload">
			</a>
			<a href="" class="remove">
			</a>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<form action="#" class="horizontal-form">
			<div class="form-body">
				
				<div class="row">
				
					<!--
					/***********************************************    
					  Tipo de Post
					***********************************************/  
					-->
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">Tipo de Post</label>
							<select class="select2_category form-control" data-placeholder="Escolha um item" tabindex="1">
								<option value="Category 1">Notícia</option>
								<option value="Category 2">Galeria</option>
							</select>
						</div>
					</div>
					<!--/span-->

				</div>
				<!--/row-->
				
				<div class="row">

					<!--
					/***********************************************    
					  Categoria
					***********************************************/  
					-->
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Categoria</label>
							<select class="select2_category form-control" data-placeholder="Escolha um item" tabindex="1">
								<option value="">- Selecione uma Opção -</option>
								<option value="Category 1">Notícia</option>
								<option value="Category 2">Galeria</option>
							</select>
							<span class="help-block">Selecione apenas uma categoria OU uma modalidade </span>
						</div>
					</div>

					<!--
					/***********************************************    
					  Modalidade
					***********************************************/  
					-->
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Modalidade</label>
							<select class="select2_category form-control" data-placeholder="Escolha um item" tabindex="1">
								<option value="">- Selecione uma Opção -</option>
								<option value="">Adestramento</option>
								<option value="">Atrelagem</option>
								<option value="">CCE</option>
								<option value="">Enduro</option>
								<option value="">Paraequestre</option>
								<option value="">Rédeas</option>
								<option value="">Salto</option>
								<option value="">Volteio</option>
							</select>
							<span class="help-block">Selecione apenas uma categoria OU uma modalidade </span>
						</div>
					</div>


				</div>
				<!--/row-->


				<div class="row">
					
					<!--
					/***********************************************    
					  Titulo
					***********************************************/  
					-->
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">Título</label>
							<input type="text" class="form-control" placeholder="">
							<span class="help-block">Atenção: não é permitido dois títulos iguais. </span>
						</div>
					</div>

				</div>
				<!--/row-->


				<div class="row">
					
					<!--
					/***********************************************    
					  Slug
					***********************************************/  
					-->
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">Slug</label>
							<input type="text" class="form-control" placeholder="" disabled>
						</div>
					</div>

				</div>
				<!--/row-->


				<div class="row">
					
					<!--
					/***********************************************    
					  Data
					***********************************************/  
					-->
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Data</label>
							<input class="form-control form-control-inline date-picker" type="text" value=""/>
							<span class="help-block">Instrução: indique a data da publicação. </span>
						</div>
					</div>
					<!--/span-->


					<!--
					/***********************************************    
					  Fonte
					***********************************************/  
					-->
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Fonte</label>
							<input type="text" class="form-control" placeholder="">
							<span class="help-block">Instrução: indique a origem da publicação. </span>
						</div>
					</div>
					<!--/span-->

				</div>
				<!--/row-->


				<div class="row">
					
					<!--
					/***********************************************    
					  Destaque
					***********************************************/  
					-->
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Destaque</label>
							<div class="radio-list">
								<input type="checkbox" class="make-switch" data-on-text="Ativo" data-off-text="Inativo">
							</div>
							<span class="help-block">Instrução: ative para a publicação ser fixada na página principal do site. </span>
						</div>
					</div>
					<!--/span-->

					
					<!--
					/***********************************************    
					  Posição
					***********************************************/  
					-->
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Posição</label>

							<div id="spinner1">
								<div class="input-group input-small">
									<input type="text" class="spinner-input form-control" maxlength="3" readonly>
									<div class="spinner-buttons input-group-btn btn-group-vertical">
										<button type="button" class="btn spinner-up btn-xs blue">
										<i class="fa fa-angle-up"></i>
										</button>
										<button type="button" class="btn spinner-down btn-xs blue">
										<i class="fa fa-angle-down"></i>
										</button>
									</div>
								</div>
							</div>

							<span class="help-block">Instrução: indique a origem da publicação. </span>
						</div>
					</div>
					<!--/span-->

				</div>
				<!--/row-->


				<div class="row">

					<!--
					/***********************************************    
					  Link Externo
					***********************************************/  
					-->
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">Link Externo</label>
							<input type="text" class="form-control" placeholder="">
							<span class="help-block">Instrução: preencha o campo somente se deseja que o usuário seja direcionado para outra página. </span>
						</div>
					</div>
					<!--/span-->

				</div>
				<!--/row-->



				<div class="row">

					<!--
					/***********************************************    
					  Descrição Curta
					***********************************************/  
					-->
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">Descrição Curta</label>
							<textarea class="wysihtml5 form-control" rows="6"></textarea>
						</div>
					</div>
					<!--/span-->

				</div>
				<!--/row-->

				<div class="row">

					<!--
					/***********************************************    
					  Conteúdo
					***********************************************/  
					-->
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">Conteúdo</label>
							<textarea class="ckeditor form-control" name="editor1" rows="6"></textarea>
						</div>
					</div>
					<!--/span-->

				</div>
				<!--/row-->


				<div class="row">
					
					<!--
					/***********************************************    
					  Thumb
					***********************************************/  
					-->
					<div class="col-md-12">
						<div class="form-group last">
							<label class="control-label" style="float:left !important;width:100%;">Imagem de Capa</label>
							<div class="col-md-9">
								<div class="fileinput fileinput-new" data-provides="fileinput">
									<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
										<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
									</div>
									<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
									</div>
									<div>
										<span class="btn default btn-file">
										<span class="fileinput-new">
										Selecione uma Imagem </span>
										<span class="fileinput-exists">
										Trocar </span>
										<input type="file" name="...">
										</span>
										<a href="javascript:;" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">
										Remover </a>
									</div>
								</div>
								<div class="clearfix margin-top-10">
									<span class="label label-danger">
									NOTA! </span> 
									 É aceito apenas imagens nos seguintes formatos: .gif, .jpg, .jpg, .tiff, .png, .bmp - com tamanho máximo de 4 Mb.
								</div>
							</div>
						</div>
					</div>
					<!--/span-->

				</div>
				<!--/row-->


				<div class="row margin-top-20">
					
					<!--
					/***********************************************    
					  Comentários
					***********************************************/  
					-->
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Comentários</label>
							<div class="radio-list">
								<input type="checkbox" class="make-switch" data-on-text="Ativo" data-off-text="Inativo">
							</div>
							<span class="help-block">Instrução: ative para exibir a caixa de comentários para a publicação. </span>
						</div>
					</div>
					<!--/span-->

				</div>
				<!--/row-->

				<div class="row">
					
					<!--
					/***********************************************    
					  Status
					***********************************************/  
					-->
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Status</label>
							<div class="radio-list">
								<input type="checkbox" class="make-switch" data-on-text="Ativo" data-off-text="Inativo">
							</div>
							<span class="help-block">Instrução: ative para exibir a publicação no site. </span>
						</div>
					</div>
					<!--/span-->

				</div>
				<!--/row-->



			</div>
			<!-- /form-body -->
			<div class="form-actions right">
				<button type="button" class="btn default">Voltar</button>
				<button type="submit" class="btn blue"><i class="fa fa-check"></i> Salvar</button>
			</div>
		</form>
		<!-- END FORM-->
	</div>
</div>
