<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Advertising extends MY_Controller {

    function __construct() {
        parent::__construct();

    }

    public function index() {
        $this->settings();      
    }

    /*----------------------------------------------    
      Settings
    -----------------------------------------------*/
    public function settings( $crudOperation = 'retrieve' ) {

        /* NO CREATE CHAMA DUAS VEZES O SETTING, NO CREATE 1, e NO UPDATE 2*/
        #   var_dump("problema");

        /*
          Form   
        =============================================*/
        $data['formIdentification']      =       'form_sample_1';        


        /*-- Template
        =============================================*/
        $data['module']                             =   'Advertising';
        $data['moduleSlug']                         =   'advertising';

        switch ($crudOperation) {
            case 'create':
                $data['templateMounting']  =   array(
                    'porlet'            => TRUE, 'Formulário', 'Novo Anúncio',
                    'portletButton'     =>
                        array(
                            array(
                                'Voltar', 'btn-md', FALSE, 'btn grey-cascade ', 'fa fa-chevron-left', TRUE, '', '', array('linkInternal', 'engine/gear/advertising/retrieve', '_self')
                            )
                        ),
                    'breadcrumb'        => array( 'Módulo: Anúncio' => 'Link Interno', 'Formulário' => '')
                );
                break;
            
            case 'retrieve':
                $data['templateMounting']  =   array(
                    'porlet'            => TRUE, 'Listagem', 'Anúncio',
                    'portletButton'     =>
                        array(
                            array(
                                'Cadastrar Anúncio', 'btn-md', FALSE, 'btn yellow-crusta ', 'fa fa-plus', TRUE, '', '', array('linkInternal', 'engine/gear/advertising/create', '_self')
                            ),  
                        ),
                    'breadcrumb'        => array( 'Módulo: Anúncio' => 'Link Interno', 'Listagem' => '')
                );
                break;

            case 'update':
                $data['templateMounting']  =   array(
                    'porlet'            => TRUE, 'Listagem', 'Posts',
                    'portletButton'     =>
                        array(
                            array(
                                'Voltar', 'btn-md', FALSE, 'btn grey-cascade ', 'fa fa-chevron-left', TRUE, '', '', array('linkInternal', 'engine/gear/advertising/retrieve', '_self')
                            ),
                            array(
                                'Cadastrar Anúncio', 'btn-md', FALSE, 'btn yellow-crusta ', 'fa fa-plus', TRUE, '', '', array('linkInternal', 'engine/gear/advertising/create', '_self')
                            ),  
                        ),
                    'breadcrumb'        => array( 'Módulo: Anúncio' => 'Link Interno', 'Gerenciar' => '')
                );
                break;
        }


        /*-- Database
        =============================================*/
        $data['databaseTable']                      =   'tb_banner';
        $data['databaseColumnPk']                   =   'ban_id';
        

        /*-- Tabs
        =============================================*/
        $data['tabs']           =   TRUE;   
        $data['tabManager']     =   TRUE;   
        $data['tabThumb']       =   TRUE;   
        $data['tabGallery']     =   FALSE;   
        $data['tabMaps']        =   FALSE;   


        /*-- Thumb
        =============================================*/
        $data['pathThumb']                          =   'uploads/advertising/';
        $data['databaseColumnThumb']                =   'ban_thumb';
        $data['tabThumbDetails']                    =   array(
                                                            'title'         =>  'Thumb',
                                                            'description'   =>  'Miniatura utilizada na listagem',
                                                            'ratioX'        =>  728,
                                                            'ratioY'        =>  90,
                                                            'coefficient'   =>  1
                                                    );   


        /*-- Gallery
        =============================================*/
        $data['pathGallery']                        =   'uploads/entidade/galerias/';
        $data['databaseModuleColumnFolder']         =   'ent_folder'; 
        $data['databaseTableGallery']               =   'tb_entidadeescola_foto';
        $data['databaseTableGalleryColumnFk']       =   'fk_ent_id';
        $data['databaseTableGalleryColumnPk']       =   'fot_id';
        $data['databaseTableGalleryColumnThumb']    =   'fot_thumb';


        /*-- Maps
        =============================================*/
        $data['tabMapsInitialZoom']                 =   5;
        $data['tabMapsCenterLat']                   =   '-16.680541';
        $data['tabMapsCenterLng']                   =   '-49.256383';
        $data['tabMapsDatabaseColumnLat']           =   'ent_eixox';
        $data['tabMapsDatabaseColumnLng']           =   'ent_eixoy';


        /*-- Loading File JS/CSS from Module
        =============================================*/
        $data['moduleJs']       =   array('main.js');
        $data['moduleCss']      =   array('main.css');

        return $data;
    }


    /*----------------------------------------------    
      Create
    -----------------------------------------------*/
    public function create()
    {

        /*
          Instance of class
        =============================================*/ 
        $settings   =   Advertising::settings();

        /*
          Inputs   
        =============================================*/
        /* Titulo */ $h1 = $this->inputconstructor->text(array(array('col-sm-12', 'Título', '', '', 'ban_titulo', '', '', '', TRUE, FALSE, 'vF', 'vB'), array('', '', ''), array(TRUE, FALSE), array('ban_titulo', '') ) );
        /* Link */ $h2 = $this->inputconstructor->text(array(array('col-sm-12', 'Link', '', '', 'ban_link', '', '', '', TRUE, FALSE, 'vF', 'vB'), array('', '', ''), array(TRUE, FALSE), array('ban_link', '') ) );
        /* Status */ $h3 = $this->inputconstructor->boolean(array(array('col-sm-12', 'Status', '<b>Instrução:</b> ative para a publicação ser vinculada no site.', '', 'ban_flagStatus', '', '', '', TRUE, FALSE, 'vF', 'vB'), array('Ativo', 'Inativo'), array(TRUE, FALSE), array('ban_flagStatus', '') ) );

        /*
          Gear - Generate Variables for Enviroment Scope   
        =============================================*/
        $foo = array();
        for ($i=1; $i <= 100; $i++) { 
            $variableName = 'h' . $i;
            if(isset($$variableName)){
                $foo[] = $$variableName;
            }
            else{
                break;
            }
        }
        $html = $foo;


        /*
          Return data   
        =============================================*/
       $data['inputs'] = $html;
       return $data;
    }


    /*----------------------------------------------    
      Retrieve
    -----------------------------------------------*/
    public function retrieve() {


        /*
          Instance of class
        =============================================*/ 
        $settings   =   Advertising::settings();


        /*
          Variables of the function scope
        =============================================*/
        $databaseTable                 = $settings['databaseTable'];
        $databaseColumnPk              = $settings['databaseColumnPk'];

        /*
          Main Query - Database
        =============================================*/
        $data['table']          =   $databaseTable;
        $data['where']          =   array();
        $data['orderby']        =   array(
                                        'ban_id'=>'DESC',
                                    );
        $data['limit']          =   NULL;
        $data['columnId']       =   $databaseColumnPk; // indica qual eh a coluna ID (PK) da tabela


        /*
          Settings Data Table   
        =============================================*/
        $data['btnActions']         =   array(
                                            'status'    => TRUE,
                                            'manager'   =>  TRUE,
                                            'detail'    =>  TRUE,
                                            'delete'    =>  TRUE
                                        );
        $data['fieldsDetails']      =   array();


        /*
          Data Table   
        =============================================*/
        $data['datatableFlag']      =   TRUE;   // Key for active datatable
        $data['datatableTitle']     =   'Parceiros';  // Title of datatable
        $data['datatableSubTitle']  =   'Listagem';  // Title of datatable
        $data['datatableHeaders']   =   array( '#ID', 'Parceiro', 'Thumb', 'Status');
        $data['fieldsDatatable']    =   array( 'ban_id', 'ban_titulo', 'ban_link', 'ban_thumb', 'ban_flagStatus' );
 

        /* Levar o retrieveDataBulk pro MY_Controller */
        $data['retrieveDataBulk']           =   $this->model_crud->select(
                                                                            $data['table'], 
                                                                            $data['fieldsDatatable'],
                                                                            $data['where'],
                                                                            $data['orderby'],
                                                                            $data['limit']
                                                                        );




        /*
          Rules DataBulk   
        =============================================*/
        $data['rulesDataBulk']      =   array(
            'ban_id'        =>  array(),
            'ban_titulo'        =>  array(),
            'ban_thumb' =>  array(  'img'   =>  'advertising' ),
            'ban_flagStatus' =>  array(  'status'  =>  TRUE ),
        );


        /*
          # Details   
          Details - Data Table ( displayed only when requested )    
        =============================================*/
        $data['fieldsDetailsDatatable']     =   array( 'ban_id', 'fk_aut_id', 'ban_titulo', 'ban_thumb', 'ban_posicao', 'ban_link', 'ban_flagStatus', 'criado', 'modificado' );
        $retrieveDetails                    =   array(
            'ban_id'            =>  array( 'title' => 'Identificação' ),
            'fk_aut_id'         =>  array(
                'title'     =>  'Autor',
                'fk'        =>  array('tb_pessoa', 'pes_id', 'pes_nome'),
                'label'     =>  array('label-primary', '4'),
            ),
            'ban_titulo'        =>  array( 'title' =>  'Título' ),
            'ban_link'          =>  array( 'title' =>  'Link' ),
            'ban_posicao'       =>  array( 'title' =>  'Posição' ),
            'ban_thumb'         =>  array(  'title'   =>  'Thumb', 'img' =>  'advertising' ),
            'ban_flagStatus'    =>  array(  'title'   =>  'Status', 'status'  =>  TRUE ),   
            'criado'            =>  array(  'title'   =>  'Criado em', 'date'    =>  'justDate' ),
            'modificado'        =>  array(  'title'   =>  'Última Modificação', 'date'    =>  'justDate' ),
        );                                            
        $data['rulesDetails']      =   json_encode($retrieveDetails);

        return $data;
    }


    /*----------------------------------------------    
      Update
    -----------------------------------------------*/
    public function update( $moduleItem = NULL )
    {

        /*
          Instance of class
        =============================================*/ 
        $settings   =   Advertising::settings();
        $create     =   Advertising::create();


        /*
          Variables of the function scope
        =============================================*/
        $databaseTable                 = $settings['databaseTable'];
        $databaseColumnPk              = $settings['databaseColumnPk'];
        $databaseItemCurrent           = $this->uri->segment(5);
        $data['databaseItemCurrent']   = $databaseItemCurrent;
        $query                         = $this->model_crud->get_rowSpecific( $databaseTable, $databaseColumnPk, $databaseItemCurrent, 1, $databaseColumnPk );

        /* Se nao existir o databaseItemCurrent e redirecionado para a pagina de retrieve do modulo */
        if( empty($databaseItemCurrent) || is_null( $databaseItemCurrent ) || is_null($query) )
            redirect(base_url() . 'engine/gear/' . $settings['moduleSlug'] . '/retrieve', 'refresh');


        /*
          Set inputs with value ( html )
        =============================================*/
        $arrInputs = array(); 
        foreach ($create['inputs'] as $input) {

            $inputDatabaseColumn    =   $input['database']['database-column'];
            $query                  =   $this->model_crud->get_rowSpecific($settings['databaseTable'], $settings['databaseColumnPk'], $databaseItemCurrent, 1, $inputDatabaseColumn);
            $input['database']['database-value']   =   $query;

            $arrInputs[]   =   $input;

        }
       $data['inputs'] = $arrInputs;


        /*
          Manager Tab
        =============================================*/


        /*
          Thumbnail Tab
        =============================================*/
        if($settings['tabThumb']):
            $data['filenameCurrentThumb']   =   $this->model_crud->get_rowSpecific($settings['databaseTable'], $settings['databaseColumnPk'], $data['databaseItemCurrent'], 1, $settings['databaseColumnThumb']);
        endif;


        /*
          Gallery Tab
        =============================================*/
        if($settings['tabGallery']):
            $data['databaseFolder']             =   $this->model_crud->get_rowSpecific($settings['databaseTable'], $settings['databaseColumnPk'], $data['databaseItemCurrent'], 1, $settings['databaseModuleColumnFolder']);  
            $data['pathGallery']                =   $settings['pathGallery'] . $data['databaseFolder'] . '/';
            $data['galleryDataBulk']            =   $this->model_crud->select($settings['databaseTableGallery'], array('*'), array($settings['databaseTableGalleryColumnFk'] . ' =' => $data['databaseItemCurrent']), array(), NULL);
        endif;


        /*
          Maps
        =============================================*/
        if($settings['tabMaps']):
            $data['tabMapsCurrentLat']          =   $this->model_crud->get_rowSpecific($settings['databaseTable'], $settings['databaseColumnPk'], $data['databaseItemCurrent'], 1, $settings['tabMapsDatabaseColumnLat']);  
            $data['tabMapsCurrentLng']          =   $this->model_crud->get_rowSpecific($settings['databaseTable'], $settings['databaseColumnPk'], $data['databaseItemCurrent'], 1, $settings['tabMapsDatabaseColumnLng']);  
        endif;


        /*
          Tabs Extension
        =============================================*/
        $data['tabsExtension']  =   array(
            #array( 'type'=>'link', 'link'=>'',  'title' => 'Obras', 'slug' => 'obras', 'icon' => 'fa fa-database', )
        );


        /*
        Return data   
        =============================================*/
        return $data;
    }

}
// end class



