<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';
class Taxa extends MY_FrontOffice {

    public $data;

    function __construct() {

        parent::__construct();


    }


    /**
    * Index
    *
    * @author Gustavo Botega 
    */
    public function index() {
    	return true;
    }


    /*
       X [C] TB_FINANCEIRO
       X [C] TB_FINANCEIRO_HISTORICO
        [C] TB_FINANCEIRO_ITEM
        [C] TB_FINANCEIRO_DESCONTO

       X [C] TB_REGISTRO
       X [C] TB_REGISTRO_HISTORICO
        [C] TB_FINANCEIRO_REGISTRO
        [C] TB_FINANCEIRO_REL_REGISTRO

       ~ [U] TB_FINANCEIRO ( atualizar numero pagar-me transacao )
       ~ [U] TB_FINANCEIRO / TB_FINANCEIRO_HISTORICO ( se for cartão atualizar status da transacao )
    */




    /**
    * GetTipoRegistro
    *
    * @author Gustavo Botega 
    */
    public function GetTipoRegistro(){

        $ModalidadeId   =    $_POST['ModalidadeId'];


        $SqlConsultaTipoRegistro      =   "
                                    SELECT * FROM tb_registro_taxa 
                                    WHERE fk_evm_id = '".$ModalidadeId."'
                                ";
        $Query              =   $this->db->query($SqlConsultaTipoRegistro)->result();        

        $ArrTipoRegistro    =   array();
        foreach ($Query as $key => $value) {        

            if(!in_array($value->fk_ret_id, $ArrTipoRegistro))
                $ArrTipoRegistro[] = $value->fk_ret_id;

        }

        echo json_encode($ArrTipoRegistro);

    }




    /**
    * GetValores
    *
    * @author Gustavo Botega 
    */
    public function GetValores($ModalidadeId = NULL, $TipoRegistroId = NULL, $TipoCompetidorId = NULL){

        if(is_null($ModalidadeId))
            $ModalidadeId               =    $_POST['ModalidadeId'];
        
        if(is_null($TipoRegistroId))
            $TipoRegistroId             =    $_POST['TipoRegistroId'];
        
        if(is_null($TipoCompetidorId))
            $TipoCompetidorId             =    $_POST['TipoCompetidorId'];


        $AnoAtual                   =   date('Y');
        $AnoId                      =   $this->model_crud->get_rowSpecific('tb_registro_ano', 'rea_ano', $AnoAtual, 1, 'rea_id');

        $SqlConsultaValores         =   "
                                            SELECT * FROM tb_registro_taxa 
                                            WHERE fk_evm_id = '".$ModalidadeId."' AND 
                                                  fk_ret_id = '".$TipoRegistroId."' AND 
                                                  fk_rec_id = '".$TipoCompetidorId."' AND
                                                  fk_rea_id = '".$AnoId."'
                                        ";
        $Query                      =   $this->db->query($SqlConsultaValores)->result();        

        $ArrInfo                    =   $this->GetInfo($Query[0]);
        $ArrTaxa                    =   $this->GetTaxa($Query[0]);
        $ArrTaxaExtra               =   $this->GetTaxaExtra($Query[0]);
        $ArrDesconto                =   $this->GetDesconto($Query[0]);
        $Total                      =   $this->GetTotal( $ArrTaxa, $ArrTaxaExtra, $ArrDesconto, $ModalidadeId, $TipoRegistroId );


        $Dataset                    =   array(
                                                'Info'      => $ArrInfo,
                                                'Taxa'      => $ArrTaxa,
                                                'TaxaExtra' => $ArrTaxaExtra,
                                                'Desconto'  => $ArrDesconto,
                                                'Total'     => $Total
                                            );

            
        if(isset($_POST['ModalidadeId']) || isset($_POST['TipoRegistroId']) || isset($_POST['TipoCompetidorId']))
            echo json_encode($Dataset);
        
        return json_encode($Dataset);

    }





    /**
    * GetInfo
    *
    * Obtem informacoes gerais da taxa
    *
    * @author Gustavo Botega 
    */
    public function GetInfo($Query){

        $Array  =   array();

        $Array['Id']                =   $Query->ret_id; // ID da Taxa.

        $Array['TipoRegistro']      =   $this->model_crud->get_rowSpecific('tb_registro_tipo', 'ret_id', $Query->fk_ret_id, 1, 'ret_tipo');
        $Array['TipoRegistroId']    =   $Query->fk_ret_id;

        $Array['TipoCompetidor']    =   $this->model_crud->get_rowSpecific('tb_registro_competidor', 'rec_id', $Query->fk_rec_id, 1, 'rec_competidor');
        $Array['TipoCompetidorId']  =   $Query->fk_rec_id;

        $Array['Modalidade']        =   $this->model_crud->get_rowSpecific('tb_evento_modalidade', 'evm_id', $Query->fk_evm_id, 1, 'evm_modalidade');
        $Array['ModalidadeId']      =   $Query->fk_evm_id;

        $Array['Status']            =   $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $Query->fk_sta_id, 1, 'sta_status');
        $Array['StatusId']          =   $Query->fk_sta_id;

        $Array['Ano']               =   $this->model_crud->get_rowSpecific('tb_registro_ano', 'rea_id', $Query->fk_rea_id, 1, 'rea_ano');
        $Array['AnoId']             =   $Query->fk_rea_id;

        $Array['FlagCavalosNovos']  =   $Query->flag_cavalos_novos;
        $Array['Criado']            =   $Query->criado;
        $Array['Modificado']        =   $Query->criado;

        return $Array;

    }



    /**
    * GetTaxa
    *
    * @author Gustavo Botega 
    */
    public function GetTaxa($Query){

        $Array  =   array();

        // Get Valor Cheio
        $Array['Titulo']            =   $Query->ret_taxa;
        $Array['Descricao']         =   $Query->ret_descricao;
        $Array['Valor']             =   $Query->ret_preco;

        return $Array;

    }



    /**
    * GetTaxaExtra
    *
    * @author Gustavo Botega 
    */
    public function GetTaxaExtra($Query){

        $Array  =   array();
        $Array['Flag']                  =   false;

        $RegistroTaxaId                 =   $Query->ret_id;


        // Faz o laco dentro de cada desconto e checa se a data atual esta dentro da faixa de data. Se encontrar alguma ocorrencia automaticamente para e assume o desconto
        // Obter todos os descontos dessa taxa
        $SqlTaxaExtraParaTaxaSelecionada   =   "
                                            SELECT * FROM tb_registro_taxa_extra 
                                            WHERE fk_ret_id = '".$RegistroTaxaId."' AND 
                                                  fk_sta_id = 1 AND
                                                  flag_habilitado IS NOT NULL AND
                                                  flag_deletado IS NULL
                                        ";
        $Query                      =   $this->db->query($SqlTaxaExtraParaTaxaSelecionada)->result();     

        if(empty($Query))
            return $Array;

        if(!empty($Query)):

            foreach ($Query as $key => $value):

              $Array['Flag']                    =   true;
              $Array['Id']                      =   $value->rte_id;
              $Array['Titulo']                  =   $value->rte_taxa_extra;
              $Array['Descricao']               =   $value->rte_descricao;
              $Array['Obrigatorio']             =   $value->flag_obrigatorio;
              $Array['Valor']                	=   number_format($value->rte_valor, 2, '.', '');
              break;

            endforeach;

        endif;


        return $Array;




        return $Array;

    }


    /**
    * GetDesconto
    *
    * @author Gustavo Botega 
    */
    public function GetDesconto($Query){

        $RegistroTaxaId                 =   $Query->ret_id;

        $Array      =   array();
        $Array['Flag']                  =   false;
        $DataAtual                      =   date("Y-m-d");
        $DataAtualDateTime              =   new DateTime();



        // Faz o laco dentro de cada desconto e checa se a data atual esta dentro da faixa de data. Se encontrar alguma ocorrencia automaticamente para e assume o desconto
        // Obter todos os descontos dessa taxa
        $SqlDescontosParaTaxaSelecionada   =   "
                                            SELECT * FROM tb_registro_taxa_desconto 
                                            WHERE fk_ret_id = '".$RegistroTaxaId."' AND 
                                                  fk_sta_id = 1 
                                        ";
        $Query                      =   $this->db->query($SqlDescontosParaTaxaSelecionada)->result();     

        if(empty($Query))
            return $Array;

        if(!empty($Query)):

            foreach ($Query as $key => $value):

                $DataInicio     =   $value->rtd_data_inicio;  // Data inicio do desconto
                $DataFim        =   $value->rtd_data_fim; // Data fim do desconto

//                $DataAtualDateTime = new DateTime('2018-10-12'); // Simular outra data
                $DateBegin = new DateTime($DataInicio);
                $DateEnd  = new DateTime($DataFim);

                if (
                  $DataAtualDateTime->getTimestamp() > $DateBegin->getTimestamp() && 
                  $DataAtualDateTime->getTimestamp() < $DateEnd->getTimestamp()){
                    // Dentro do intervalo de datas do desconto
                      $Array['Flag']          =   true;
                      $Array['Id']            =   $value->rtd_id;
                      $Array['Titulo']        =   $value->rtd_taxa_desconto;
                      $Array['Descricao']     =   $value->rtd_descricao;
                      $Array['Tipo']          =   2; // Indica qual e o tipo do desconto. 2 = Desconto no Registro Atleta/Animal. Referencia: tb_financeiro_desconto_tipo 
                      $Array['Valor']         =   number_format($value->rtd_valor, 2, '.', '');
                      break;
                }else{
                    // Fora do intervalo de datas
                }


            endforeach;

        endif;


        return $Array;

    }


    /**
    * GetTotal
    *
    * @author Gustavo Botega 
    */
    public function GetTotal($ArrTaxa, $ArrTaxaExtra, $ArrDesconto, $ModalidadeId, $TipoRegistroId){

        $Array                              =   array();
        $Desconto                           =   0.00; // Setando desconto zerado
        $Taxa                               =   $ArrTaxa['Valor']; 


        // Associado o valor da taxa a variavel $TotalBruto
        $TotalBruto                         =   $Taxa;


        // Taxa Extra
        if($ArrTaxaExtra['Flag'])
            $TotalBruto                     =   $Taxa + $ArrTaxaExtra['Valor'];
        

        // Descontos Mes a Mes 
        if($ArrDesconto['Flag'])
            $Desconto                       =   $ArrDesconto['Valor'];


        // Retornando dados
        $Array['Bruto']                     =   number_format($TotalBruto, 2, '.', '');
        $Array['Liquido']                   =   number_format($TotalBruto - $Desconto, 2, '.', '');
        return $Array;

    }







}

/* End of file */

