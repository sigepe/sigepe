<?php

if (!defined('BASEPATH')) { exit('No direct script access allowed'); }
    
class Atleta extends MY_BackOffice {

    public $data;

    function __construct() {

        parent::__construct();

        $this->metronicAsset();
        $this->sigepeAsset();

        $this->data['PageHeadTitle'] = "Atletas";
                
        $this->data['base_dir'] = "Template/BackOffice/sistema/".__CLASS__."/";
        $this->data['Class']    =   $this->router->fetch_class();

        /* Controladores Externos */
#        $this->load->module('evento/BackOffice/Evento');

    }



    public function index() {
        $this->atleta_browse_bread();
    }



    /**
    * atleta_browse_bread.
    *
    *
    * @author {Author}
    *
    * @return view
    */
    public function atleta_browse_bread() {
        $this->data['PageHeadSubtitle'] = "Todos os atletas do sistema";

        $sql = "
        SELECT pfa.*
            ,pes.pes_nome_razao_social
            ,pef.pef_id
            ,pef.fk_pes_id
            /*
            ,CASE WHEN pjf.pjf_acronimo
                is not null then
                    pjf.pjf_acronimo
              else ''
            end as pjf_acronimo*/
            ,pjf.pes_nome_razao_social as pjf_acronimo
            ,vin.criado as vin_criado
            ,vin.fk_per_id
            ,sta.sta_status
            from tb_pessoa_fisica_atleta pfa

            join tb_pessoa_fisica pef on pef.pef_id = pfa.fk_pef_id
            join tb_pessoa pes on pes.pes_id = pef.fk_pes_id   
            left join tb_pessoa pjf on pjf.pes_id = pfa.fk_pjf_id
            left join tb_vinculo vin on vin.fk_pes1_id = pef.fk_pes_id and vin.fk_per_id = 5 and vin.fk_tip_id is null
            left join tb_status sta on sta.sta_id = vin.fk_sta_id
            order by pfa.pfa_nome_competicao";
        $dataset = $this->db->query($sql)->result();

        $this->data['dataset'] = $dataset;

        /* View */ $this->LoadTemplate($this->data['base_dir']."/".__FUNCTION__, $this->data);

    } // fim do metodo @atleta_browse_bread



    /**
    * atleta_federado_browse.
    *
    *
    * @author {Author}
    *
    * @return view
    */
    public function atleta_federado_browse() {
        $this->data['PageHeadSubtitle'] = "Todos os atletas federados na FHBr";

        $sql = "
        SELECT pfa.*
            ,pes.pes_nome_razao_social
            ,pef.pef_id
            ,pef.fk_pes_id
            /*
            ,CASE WHEN pjf.pjf_acronimo
                is not null then
                    pjf.pjf_acronimo
              else ''
            end as pjf_acronimo*/
            ,pjf.pes_nome_razao_social as pjf_acronimo
            ,vin.criado as vin_criado
            ,vin.fk_per_id
            ,sta.sta_status as vin_status
            ,pje.pes_nome_razao_social as pje_entidade
            from tb_pessoa_fisica_atleta pfa

            join tb_pessoa_fisica pef on pef.pef_id = pfa.fk_pef_id
            join tb_pessoa pes on pes.pes_id = pef.fk_pes_id   
            left join tb_pessoa pjf on pjf.pes_id = pfa.fk_pjf_id
            left join tb_vinculo vin on vin.fk_pes1_id = pef.fk_pes_id and vin.fk_per_id = 5 and vin.fk_tip_id is null
            left join tb_status sta on sta.sta_id = vin.fk_sta_id
            left join tb_pessoa pje on pje.pes_id = pfa.fk_pje_id
            where pfa.fk_pjf_id = 141
            order by pfa.pfa_nome_competicao";
        $dataset = $this->db->query($sql)->result();

        $sqlModalidade = "
        SELECT reg.reg_id, reg.fk_pes_id, reg.fk_mod_id, reg.fk_sta_id, reg.criado as reg_criado
        , evm.evm_modalidade
        , sta.sta_status as reg_status
        from tb_registro reg
        join tb_evento_modalidade evm on evm.evm_id = reg.fk_mod_id
        join tb_status sta on sta.sta_id = reg.fk_sta_id
        where fk_ani_id is null
        order by reg_criado desc
        ";
        $datasetModalidade = $this->db->query($sqlModalidade)->result();

        foreach ($dataset as $atleta) {
            $procurado = $atleta->fk_pes_id;
            $atleta->modalidades = array_filter(
                $datasetModalidade,
                function ($e) use (&$procurado) {
                    return $e->fk_pes_id == $procurado;
                }
            );
        }

        /*$array = array('apple', 'orange', 'pear', 'banana', 'apple', 'pear', 'kiwi', 'kiwi', 'kiwi');
        $searchObject = 'kiwi';
        $keys = array_keys($array, $searchObject);
        print_r($keys);*/

        $this->data['dataset'] = $dataset;

        $this->LoadTemplate($this->data['base_dir']."/".__FUNCTION__, $this->data);

    } // fim do metodo @atleta_federado_browse


    
    /**
    * atleta_outra_federacao_browse.
    *
    *
    * @author {Author}
    *
    * @return view
    */
    public function atleta_outra_federacao_browse() {
        $this->data['PageHeadSubtitle'] = "Atletas de oturas federações";

        $sql = "
        SELECT pfa.*
            ,pes.pes_nome_razao_social
            ,pes.criado as pes_criado
            ,pef.pef_id
            ,pef.fk_pes_id
            /*
            ,CASE WHEN pjf.pjf_acronimo
                is not null then
                    pjf.pjf_acronimo
              else ''
            end as pjf_acronimo*/
            ,pjf.pes_nome_razao_social as pjf_acronimo
            ,vin.criado as vin_criado
            ,vin.fk_per_id
            ,sta.sta_status as vin_status
            ,pje.pes_nome_razao_social as pje_entidade
            from tb_pessoa_fisica_atleta pfa

            join tb_pessoa_fisica pef on pef.pef_id = pfa.fk_pef_id
            join tb_pessoa pes on pes.pes_id = pef.fk_pes_id   
            left join tb_pessoa pjf on pjf.pes_id = pfa.fk_pjf_id
            left join tb_vinculo vin on vin.fk_pes1_id = pef.fk_pes_id and vin.fk_per_id = 5 and vin.fk_tip_id is null
            left join tb_status sta on sta.sta_id = vin.fk_sta_id
            left join tb_pessoa pje on pje.pes_id = pfa.fk_pje_id
            where pfa.fk_pjf_id != 141
            order by pfa.pfa_nome_competicao";
        $dataset = $this->db->query($sql)->result();

        $sqlModalidade = "
        SELECT reg.reg_id, reg.fk_pes_id, reg.fk_mod_id, reg.fk_sta_id, reg.criado as reg_criado
        , evm.evm_modalidade
        , sta.sta_status as reg_status
        from tb_registro reg
        join tb_evento_modalidade evm on evm.evm_id = reg.fk_mod_id
        join tb_status sta on sta.sta_id = reg.fk_sta_id
        where fk_ani_id is null
        order by reg_criado desc
        ";
        $datasetModalidade = $this->db->query($sqlModalidade)->result();

        foreach ($dataset as $atleta) {
            $procurado = $atleta->fk_pes_id;
            $atleta->modalidades = array_filter(
                $datasetModalidade,
                function ($e) use (&$procurado) {
                    return $e->fk_pes_id == $procurado;
                }
            );
        }

        /*$array = array('apple', 'orange', 'pear', 'banana', 'apple', 'pear', 'kiwi', 'kiwi', 'kiwi');
        $searchObject = 'kiwi';
        $keys = array_keys($array, $searchObject);
        print_r($keys);*/

        $this->data['dataset'] = $dataset;

        $this->LoadTemplate($this->data['base_dir']."/".__FUNCTION__, $this->data);

    } // fim do metodo @atleta_outra_federacao_browse


    
    /**
    * atleta_read
    *
    * @author {Author}
    *
    * @return view
    */
    public function alteta_read($contextoId) {

        $sql = "";
        $dataset = $this->db->query($sql)->result();

        $this->data['dataset'] = $dataset;

        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);

    } // fim do metodo @atleta_read



    /**
    * atleta_add
    *
    * @author {Author}
    *
    * @return 
    */
    public function atleta_add($eventoId) {

        /* Setando variaveis de ambiente */
        $this->data['PageHeadTitle']      = 'Cadastro';
        $this->data['PageHeadSubtitle']   = 'Para cadastrar uma atleta no SIGEPE preencha o formulário abaixo.';

        /* View */ $this->LoadTemplate($this->data['base_dir'].__CLASS__."/".__FUNCTION__, $this->data);

    } // fim do méotodo @atleta_add



    /* # Fim Bread
       # Inicio Middleware
    ====================================================================================================================*/



    /**
    * processar.
    *
    * -
    *
    * @author {Author}
    *
    * @param $dataJson
    *
    * @return boolean
    */
    public function processar($dataJson = false) {

        if(isset($_POST['dataJson']) && is_array($_POST['dataJson'])) {
            $dados = $_POST['dataJson'];
        }

        if($dataJson) {
            $dados = $dataJson;
        }

        if(!isset($_POST['dataJson']) && !$dataJson) {
            echo json_encode(false);
        }

        // Validacao Formulario
        $validar = $this->validar($dados);
        if (!$validar) {
            echo json_encode(false);
            return false;
        }

        // Se existir algum ID por convencao entende-se que o metodo a ser direcionado e de atualizacao.
        if(!isset($dados['id'])) {
            $resultadoAcao = $this->gravar($dados);
            echo ($resultadoAcao) ? json_encode(true) : json_encode(false);
        } else {
            $resultadoAlterar = $this->alterar($dados);
            echo ($resultadoAlterar) ? json_encode(true) : json_encode(false);
        }

    } // fim do metodo @processar




    /**
    * gravar.
    *
    * -
    *
    * @author {Author}
    *
    * @param $dados
    *
    * @return boolean
    */
    public function gravar($dados) {

        foreach ($dados as $key => $value) {
            if(empty($value))
                $dados[$key]    =   NULL;
        }

        // dados
        $dataset       =  array(
            'fk_aut_id'             => $this->session->userdata('PessoaId'),
            'tab_coluna'            => gerarTimeStamp("tb_tabela", "tab_coluna"),
            'eve_slug'              => gerarSlug($dados['eve-nome-evento']),
            'criado'                => date("Y-m-d H:i:s")
        );


        /* query */
        $query = $this->db->insert('tb_tabela', $dataset);

        if ($query){
            $atletaId = $this->db->insert_id();
            return true;
        } else {
            return false;
        }

    } // fim do metodo @gravar



    /**
    * alterar.
    *
    * -
    *
    * @author {Author}
    *
    * @param $dados
    *
    * @return boolean
    */
    public function alterar($dados) {

        (!isset($dados['velocidade'])) ?  $dados['velocidade'] = null : '';

        // dados
        $dataset       =  array(
            'tb_id'                 => $dados['id'],
            'fk_aut_id'             => $this->session->userdata('PessoaId'),
            'tab_coluna'            => gerarTimeStamp("tb_tabela", "tab_coluna"),
            'eve_slug'              => gerarSlug($dados['name-objeto']),
            'criado'                => date("Y-m-d H:i:s")
        );

        /* Query */
        $query = $this->db->update('tb_tabela', $dataset, array('tb_id' => $dados['id'] ));
        return $query;

    } // fim do metodo @alterar



    /**
    * validar.
    *
    * -
    *
    * @author {Author}
    *
    * @param $dados
    *
    * @return boolean
    */
    public function validar($dados) {
        return true; // Validar inputs e regras de negocio do formulario. Tratar erro.
    } // fim do metodo @validar



    /* # Fim Middleware
       # Inicio Acessório
    ====================================================================================================================*/



    /**
    * getAlgumaCoisa.
    *
    * -
    *
    * @author {Author}
    *
    * @param $contextoId
    *
    * @return boolean
    */
    public function getAlgumaCoisa($contextoId) {

        $SqlCategoriasDaSerie = "
            SELECT * FROM tb_evento_rel_serie_rel_categoria as src
            WHERE fk_ers_id  = $serieId
            ORDER BY fk_evc_id desc";

       return $this->db->query($SqlCategoriasDaSerie)->result();

    }



    /* # Fim Metodo Acessório
       # Inicio metodo Controladores
    =========================================================================*/

    public function obterAtleta($contextoId) {
        return $this->model_crud->get_rowSpecificObject('tb_tabela', 'tb_id', $contextoId);
    } // fim do metodo @obterSerie


    /* # Fim Metodo Controladores
       # Inicio Asset 
    =========================================================================*/
    /**
    * metronicAsset.
    *
    * -
    *
    * @author {Author}
    *
    * @param 
    *
    * @return
    */
    public function metronicAsset() {
        /* STYLES */
        $this->data['StylesFile']['Plugins']['datatables'] = TRUE;
        $this->data['StylesFile']['Plugins']['datatables-bootstrap'] = TRUE;

        /* SCRIPTS */
        $this->data['ScriptsFile']['Plugins']['datatable'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['datatables'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['datatables-bootstrap'] = TRUE;

        $this->data['ScriptsFile']['Scripts']['table-datatables-fixedheader'] = TRUE;
    } // fim do metodo MetronicAsset



    /**
    * sigepeAsset.
    *
    * -
    *
    * @author {Author}
    *
    * @param 
    *
    * @return
    */
    public function sigepeAsset() {
    
        /* Carregando Estilos */
        //$this->data['PackageStyles'][]   =   'Packages/Scripts/Evento/BackOffice/FormValidationAtleta';
        
        /* Carregando Scripts */
        //$this->data['PackageScripts'][]   =   'Packages/Scripts/Evento/BackOffice/FormValidationAtleta';
    
    } // fim do metodo sigepeAsset


} /* End of file Atleta.php */
