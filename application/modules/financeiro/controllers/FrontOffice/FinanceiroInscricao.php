<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';

//require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
class FinanceiroInscricao extends MY_FrontOffice {

    public $data;

    function __construct() {

        parent::__construct();

    }


    /**
    * Gravar
    *
        [C] TB_FINANCEIRO_FINANCEIRO
    *
    *
    * @author Gustavo Botega
    */
    public function Gravar($FinanceiroId, $RegistroId, $DatasetValores) {

        $TaxaId     =   $DatasetValores->Info->Id;
        ($DatasetValores->TaxaExtra->Flag) ? $TaxaExtraId = $DatasetValores->TaxaExtra->Id : $TaxaExtraId = NULL;
        ($DatasetValores->Desconto->Flag) ? $DescontoId = $DatasetValores->Desconto->Id : $DescontoId = NULL;

        $Dataset       =   array(
                            'fk_aut_id'             =>      $this->session->userdata('PessoaId'), // Autor ID
                            'fk_fin_id'             =>      $FinanceiroId, // tb_financeiro
                            'fk_reg_id'             =>      $RegistroId,  // tb_registro
                            'fk_ret_id'             =>      $TaxaId, // tb_registro_taxa
                            'fk_rte_id'             =>      $TaxaExtraId, // tb_registro_taxa_extra
                            'fk_rtd_id'             =>      $DescontoId, // tb_registro_taxa_desconto
                            'criado'                =>      date("Y-m-d H:i:s")
                        );

        $Query = $this->db->insert('tb_financeiro_registro', $Dataset);

    }



    /**
    * GravarRelacionamento
    *
        [C] TB_FINANCEIRO_REL_REGISTRO
    *
    * @author Gustavo Botega
    */
    public function GravarRelacionamento($FinanceiroId, $RegistroId) {

        $Dataset       =   array(
                            'fk_aut_id'             =>      $this->session->userdata('PessoaId'),
                            'fk_fin_id'             =>      $FinanceiroId,
                            'fk_reg_id'             =>      $RegistroId,
                            'criado'                =>      date("Y-m-d H:i:s")
                        );

        $Query = $this->db->insert('tb_financeiro_rel_registro', $Dataset);

    }




}

/* End of file  */
