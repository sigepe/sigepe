<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';

//require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
class Transacao extends MY_FrontOffice {

    public $data;

    function __construct() {

        parent::__construct();

        $this->load->library('uri');
        $this->data['TransacaoId']    =      $this->uri->segment('5');
            
    }


    /**
    * Index
    *
    * @author Gustavo Botega 
    */
    public function index() {
    }


    /**
    * Gravar
    *
    * @author Gustavo Botega 
    */
    public function Gravar($Dataset  = NULL) {

        if(is_null($Dataset))
            parse_str($_POST['FormSerializedGravarTransacao'], $Dataset);            


        // Inicializando array. Prevent errors.
        $DatasetResultado                                =  array();


        //  Obtendo o timestamp. 
        $CodigoTransacao                                 =   $this->CriarCodigoTransacao();


        // Dados
        $ArrGravar       =  array(
            'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
            'fk_sta_id'                         =>    $Dataset['Transacao']['status-sigepe'], // Status Sigepe
            'fk_sta2_id'                        =>    $Dataset['Transacao']['status-pagarme'], // Status Pagarme 
            'fk_pes_id'                         =>    $Dataset['Transacao']['responsavel-financeiro'], // Responsavel Financeiro
            'fk_fip_id'                         =>    $Dataset['Transacao']['tipo-pagamento'], // Tipo de Pagamento
            'fk_fin_id'                         =>    $Dataset['Transacao']['natureza-operacao'], // Natureza da Operacao
            'fin_transacao'                     =>    $CodigoTransacao,
            'fin_valor_bruto'                   =>    $Dataset['Transacao']['valor-bruto'],
            'fin_valor_desconto'                =>    (isset($Dataset['Desconto'][0]['valor'])) ? $Dataset['Desconto'][0]['valor'] : NULL,
            'fin_valor_liquido'                 =>    $Dataset['Transacao']['valor-liquido'],
            'fin_ip'                            =>    $this->input->ip_address(),
            'criado'                            =>    date("Y-m-d H:i:s")
        );
        $Query = $this->db->insert('tb_financeiro', $ArrGravar);


        if(!$Query)
            $DatasetResultado['Status'] = false;


        if($Query):
            $FinanceiroId                       =   $this->db->insert_id(); // ID da transacao financeira que acabou de ser criada. Coluna: fin_id. Tabela: tb_financeiro.
            $DatasetResultado                   =   array(
                                                        'Status'            =>  true,
                                                        'Transacao'         =>  $CodigoTransacao,
                                                        'FinanceiroId'      =>  $FinanceiroId
                                                    );

            // Gravando Historico da Transacao
            $this->GravarHistorico($FinanceiroId, $Dataset);

            // Gravando Item
            $this->GravarItem($FinanceiroId, $Dataset['Item']);

            // Gravando Desconto
            $this->GravarDesconto($FinanceiroId, $Dataset['Desconto']);

        endif;


        if(isset($_POST['FormSerializedGravarTransacao']))
            echo json_encode($DatasetResultado);

        return $DatasetResultado;
    }



    /**
    * GravarHistorico
    *
    * @author Gustavo Botega 
    */
    public function GravarHistorico($FinanceiroId, $Dataset = FALSE) {

        if(!$Dataset)
            $Dataset    =   $_POST['FormSerializedGravarTransacaoHistorico'];


        // Dados
        $Arr       =  array(
            'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
            'fk_sta_id'                         =>    $Dataset['Transacao']['status-sigepe'], // Status SIGEPE
            'fk_sta2_id'                        =>    (isset($Dataset['Transacao']['status-pagarme'])) ? $Dataset['Transacao']['status-pagarme'] : NULL, // Status Pagarme
            'fk_fin_id'                         =>    $FinanceiroId, // ID da transacao
            'fk_fht_id'                         =>    (isset($Dataset['Historico']['tipo'])) ? $Dataset['Historico']['tipo'] : NULL,
            'fih_historico'                     =>    (isset($Dataset['Historico']['historico'])) ? $Dataset['Historico']['historico'] : NULL, // 
            'criado'                            =>    date("Y-m-d H:i:s")
        );
        $Query = $this->db->insert('tb_financeiro_historico', $Arr);

        /* Verificando operacao */
        ($Query) ?  $DatasetResultado['Status']     =   true : $DatasetResultado['Status']  =   false;



        if(isset($_POST['FormSerializedGravarTransacaoHistorico']))
            echo json_encode($DatasetResultado);

        return $DatasetResultado;

    }



    /**
    * CriarCodigoFatura
    *
    * @author Gustavo Botega 
    */
    public function CriarCodigoTransacao() {

        $TransacaoId        =   strtotime(date("Y-m-d H:i:s"));
        $TransacaoUnica     =   $this->model_crud->get_rowSpecific('tb_financeiro', 'fin_transacao', $TransacaoId, 1, 'fin_transacao');
        if(!is_null($TransacaoUnica))
            $this->CriarCodigoTransacao();

        return $TransacaoId;

    }



    /**
    * GravarItem
    *
    * @author Gustavo Botega 
    */
    public function GravarItem($FinanceiroId, $DatasetItem) {

        if(empty($DatasetItem))
            return false;

        foreach ($DatasetItem as $key => $value):

            $Item       =   array(
                                'fk_aut_id'             =>      $this->session->userdata('PessoaId'),
                                'fk_fin_id'             =>      $FinanceiroId,
                                'fii_item'              =>      $value['item'],
                                'fii_descricao'         =>      $value['descricao'],
                                'fii_quantidade'        =>      $value['quantidade'],
                                'fii_valor_unitario'    =>      $value['valor-unitario'],
                                'fii_valor_total'       =>      $value['valor-unitario'] * (int)$value['quantidade'],
                                'criado'                =>      date("Y-m-d H:i:s")
                            );

            $Query = $this->db->insert('tb_financeiro_item', $Item);

        endforeach;

        return true;

    }



    /**
    * GravarDesconto
    *
    * @author Gustavo Botega 
    */
    public function GravarDesconto($FinanceiroId, $DatasetDesconto) {


        if(empty($DatasetDesconto))
            return false;

        foreach ($DatasetDesconto as $key => $value):

            $Desconto       =   array(
                                'fk_aut_id'             =>      $this->session->userdata('PessoaId'),
                                'fk_fin_id'             =>      $FinanceiroId,
                                'fk_sta_id'             =>      1,
                                'fk_fdt_id'             =>      $value['tipo'],
                                'fid_desconto'          =>      $value['item'],
                                'fid_descricao'         =>      $value['descricao'],
                                'fid_valor'             =>      $value['valor'],
                                'criado'                =>      date("Y-m-d H:i:s")
                            );

            $Query = $this->db->insert('tb_financeiro_desconto', $Desconto);

        endforeach;

        return true;

    }



    /**
    * SetAtualizarTransacao
    *
    * @author Gustavo Botega 
    */
    public function SetAtualizarTransacao($FinanceiroId = NULL, $Transacao = NULL, $Dataset) {

    }


    /**
    * SetTransacaoPago
    *
    * @author Gustavo Botega 
    */
    public function SetTransacaoPago($FinanceiroId = NULL, $Transacao = NULL, $Dataset) {

        if(!is_null($FinanceiroId)){
            $Coluna         =   'fin_id';
            $Parametro      =   $FinanceiroId;
        }

        if(!is_null($Transacao)){
            $Coluna             =   'fin_transacao';
            $FinanceiroId       =   $this->model_crud->get_rowSpecific('tb_financeiro', 'fin_transacao', $Transacao, 1, 'fin_id');
            $Parametro      =   $Transacao;
        }


        // Atualizar
        $DatasetTransacao        =   array(
                                        'fk_sta_id'                     =>  '101', // [ SIGEPE ] Transação - Pago
                                        'fk_sta2_id'                    =>  '201', // [ Pagarme ] Pago
                                        'modificado'                    =>  date("Y-m-d H:i:s") // 
                                     );

        $this->db->where( $Coluna, $Parametro);
        $query = $this->db->update('tb_financeiro', $DatasetTransacao); 


        // Gravar Historico
        $DatasetHistorico       =   array(
                                        'Transacao'     =>  array(
                                                        'status-sigepe'     =>  '101', // [ SIGEPE ] Transação - Pago
                                                        'status-pagarme'    =>  '201'  // [ Pagarme ] Pago
                                                    ),
                                        'Historico'     =>  array(
                                                                'tipo'      =>  $Dataset['Historico']['tipo'],
                                                                'historico' =>  $Dataset['Historico']['historico']
                                                            )
                                    );


        $this->GravarHistorico($FinanceiroId, $DatasetHistorico);

    }


    /**
    * SetTransacaoCancelada
    *
    * @author Gustavo Botega
     */
    public function SetTransacaoCancelada($FinanceiroId = NULL, $Transacao = NULL, $Dataset) {


        // Obtendo o ID do financeiro se nao tiver sido passado como parametro
        if(is_null($FinanceiroId))
            $FinanceiroId       =   $this->model_crud->get_rowSpecific('tb_financeiro', 'fin_transacao', $Transacao, 1, 'fin_id');


        // Atualizando no banco
        $DatasetTransacao        =   array(
                                        'fk_sta_id'                     =>  104, // [ SIGEPE ] Transação Cancelada
                                        'modificado'                    =>  date("Y-m-d H:i:s") // 
                                     );

        if(isset($Dataset['Transacao']['status-pagarme']))
            $DatasetTransacao['fk_sta2_id']         =       $Dataset['Transacao']['status-pagarme'];


        $this->db->where( 'fin_id', $FinanceiroId );
        $Query = $this->db->update('tb_financeiro', $DatasetTransacao); 

        if(!$Query)
            return false;


        // Se a Query for executada com sucesso e gravado o historico da transacao
        if($Query):
            
            // Gravar Historico
            $DatasetHistorico       =   array( 
                                            'Transacao'     =>  array(
                                                            'status-sigepe'     =>  '104', // [ SIGEPE ] Transação Cancelada
                                                        ),
                                            'Historico'     =>  array(
                                                                    'tipo'      =>  $Dataset['Historico']['tipo'],
                                                                    'historico' =>  $Dataset['Historico']['historico']
                                                                )
                                        );

            if(isset($Dataset['Transacao']['status-pagarme']))
                $DatasetHistorico['Transacao']['status-pagarme']     =       $Dataset['Transacao']['status-pagarme'];


            $GravarHistorico    =   $this->GravarHistorico($FinanceiroId, $DatasetHistorico);


        endif;

        return true;

    }






}

/* End of file  */

