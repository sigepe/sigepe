<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';
#include_once(APPPATH."third_party/pagarme/PagarMe.php");
require FCPATH.'/pagarme/pagarme-php/Pagarme.php';


/*
    ak_live_DOwxAmD3S45SpXF3GV7t5oI9Whu648
    ak_test_pW6VqWmqmOi3XASgJ2g4kKr9cu0DYE 

    ek_live_IIpzTxJcAkbyzgjbBDXSsYiG73GsWM 
    ek_test_2E7rIVgIqzo4dgpFEvCMuoDLy0GlE2 
*/


//require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
class PagarmeVanguarda extends MY_FrontOffice {

    public $data;

    function __construct() {

        parent::__construct();

        $PessoaId                               =   $this->session->userdata('PessoaId');
        $this->data['PessoaId']                 =   $PessoaId;        
     
        Pagarme::setApiKey('ak_test_pW6VqWmqmOi3XASgJ2g4kKr9cu0DYE');
     

    }


    /**
    * Index
    *
    * @author Gustavo Botega 
    */
    public function index() {
        $this->Dashboard();
    }




    /**
    * CreateTransactionBoleto
    *
    * @author Gustavo Botega 
    */
    public function CreateTransactionBoleto() { /*$Amout $ResponsavelFinanceiroId*/

        // Resgatando dados
        $Amount                         =  $_POST['PagarmeAmount'];
        $SigepeTransacao                =  $_POST['SigepeTransacao'];
        $SigepeNaturezaOperacao         =  $_POST['SigepeNaturezaOperacao'];



        $transaction = new PagarMe_Transaction(array(
            "amount" => 100,
            "payment_method" => "boleto",
            "postback_url" => "http://requestb.in/pkt7pgpk",
            "customer" => array(
                "name" => "Aardvark Silva", 
                'external_id' => '444',
                'type' => 'individual',
                'country' => 'br',
                'documents' => [
                    [
                        'type' => 'cpf',
                        'number' => '00000000000'
                    ]
                ],

/*      
                "phone" =>  array(
                    "ddi" => "55",
                    "ddd" => "11",
                    "number" => "99999999" 
                ),
*/

                "email" => "aardvark.silva@pagar.me"
               
            ),
            "metadata" => array(
                "idProduto" => 13933139
            )
        ));

 #       var_dump( $transaction->charge() );

        $UltimaTransacao    =   PagarMe_Transaction::all(1, 1);

        echo "<pre>";
        $CustomerId         =   $UltimaTransacao[0]['customer']['external_id']; // ID SIGEPE do cliente que fez a ultima transacao

        // Verificando se a ultima transacao e do ultimo cliente
            // make verification



        // Atualizar Financeiro SIGEPE com os dados da Transacao Pagarme
        $Dataset        =   array(
                                'fk_fib_id'                     =>  $this->GetBandeiraCartao( $transaction['card_brand'] ), // Obtendo o id da bandeira do cartao. Se for boleto resultado e NULL.
                                'fin_transacao_pagarme'         =>  $transaction['tid'],
                                'fin_boleto'                    =>  $transaction['boleto_url'],
                                'fin_boleto_codigo_barras'      =>  $transaction['boleto_barcode'],
                                'fin_vencimento'                =>  $transaction['boleto_expiration_date'],
                                'fin_ip'                        =>  $transaction['ip']
                            );

        $this->db->where('fin_transacao', $SigepeTransacao);
        $query = $this->db->update('tb_financeiro', $Dataset); 


    }





    /**
    * CaptureTransactionCartaoCredito
    *
    * @author Gustavo Botega 
    */
    public function CaptureTransactionCartaoCredito() {

        // Carregando modulos externos
        $this->load->module('financeiro/FrontOffice/Transacao');


        // Resgatando dados
        $Token                          =  $_POST['DatasetPagarme']['token'];
        $Amount                         =  $_POST['PagarmeAmount'];
        $SigepeTransacao                =  $_POST['SigepeTransacao'];
        $SigepeNaturezaOperacao         =  $_POST['SigepeNaturezaOperacao'];


        // Realizando a captura 
        $transaction = PagarMe_Transaction::findById( $Token );
        $transaction->capture( $Amount ); // tratar casos de erro


        // HandleNaturezaOperacao
        $this->HandleNaturezaOperacao( $_POST );


        // Atualizando fatura com o status de pago e gerando historico da atualizacao.
        $DatasetFaturaPago      =   array(
                                        'Historico'     =>  array(
                                                                'tipo'      =>  2,
                                                                'historico' =>  'Pagamento confirmado via cartão de credito.'
                                                            )
                                    );
        $this->transacao->SetTransacaoPago(NULL, $SigepeTransacao, $DatasetFaturaPago);

        
        // Atualizar Financeiro SIGEPE com os dados da Transacao Pagarme
        $Dataset        =   array(
                                'fk_fib_id'                     =>  $this->GetBandeiraCartao( $transaction['card_brand'] ), // Obtendo o id da bandeira do cartao. Se for boleto resultado e NULL.
                                'fin_pagarme_token'             =>  $Token,
                                'fin_transacao_pagarme'         =>  $transaction['tid'],
                                'fin_cartao_titular'            =>  $transaction['card_holder_name'],
                                'fin_cartao_primeiros_digitos'  =>  $transaction['card_first_digits'],
                                'fin_cartao_ultimos_digitos'    =>  $transaction['card_last_digits'],
                                'fin_cartao_data_expiracao'     =>  $transaction->card['expiration_date'],
                                'fin_quantidade_parcela'        =>  $transaction['installments'],
                                'fin_boleto'                    =>  $transaction['boleto_url'],
                                'fin_boleto_codigo_barras'      =>  $transaction['boleto_barcode'],
                                'fin_vencimento'                =>  $transaction['boleto_expiration_date'],
                                'fin_ip'                        =>  $transaction['ip']
                            );

        $this->db->where('fin_transacao', $SigepeTransacao);
        $query = $this->db->update('tb_financeiro', $Dataset); 


        echo json_encode($Transacao);
   }










    /**
    * HandleNaturezaOperacao
    *
    * @author Gustavo Botega 
    */
    public function HandleNaturezaOperacao($Dataset) {

        // Tratando casos de acordo com a natureza de operacao        
        switch ($Dataset['SigepeNaturezaOperacao']):
            case '1': // Registro Atleta
                $this->HandleRegistro($Dataset['SigepeRegistroId']);
                break;

            case '2': // Registro Animal
                $this->HandleRegistro($Dataset['SigepeRegistroId']);
                break;
            case '3': // Passaporte
                # code...
                break;

            case '4': // Selo
                # code...
                break;
           
            case '5': // Inscricao
                # code...
                break;
            
        endswitch;

    }


    /**
    * HandleRegistro
    *
    * @author Gustavo Botega 
    */
    public function HandleRegistro($RegistroId) {

        // Atualizando o Status como ativo.
        $this->load->module('registro/FrontOffice/Registro');
        $this->registro->SetStatusAtivo($RegistroId);

    }


    

    /**
    * GetBandeiraCartao
    *
    * @author Gustavo Botega 
    */
    public function GetBandeiraCartao($Bandeira) {

        switch ($Bandeira):
            case 'mastercard':
                $BandeiraId     =   1;
                break;
            
            case 'visa':
                $BandeiraId     =   2;
                break;
            
            case 'amex':
                $BandeiraId     =   3;
                break;
            
            case 'jcb':
                $BandeiraId     =   4;
                break;
            
            case 'elo':
                $BandeiraId     =   5;
                break;
            
            case 'hipercard':
                $BandeiraId     =   6;
                break;
            
            case 'diners':
                $BandeiraId     =   7;
                break;
            
            case 'aura':
                $BandeiraId     =   8;
                break;
            
            case 'discover':
                $BandeiraId     =   9;
                break;
            
            default:
                $BandeiraId     =   NULL; // bandeira nao identificada
                break;
        endswitch;

        return $BandeiraId;

    }


    





}

/* End of file */





