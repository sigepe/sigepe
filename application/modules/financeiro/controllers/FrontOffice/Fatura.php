<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';

//require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
class Fatura extends MY_FrontOffice {

    public $data;

    function __construct() {

        parent::__construct();

        $this->ThemeComponent(); 
        $this->SigepeAsset(); 
        $this->load->library('uri');

        $this->data['TransacaoId']    =      $this->uri->segment('5');

        // Implementar: so permitir ver a fatura quem for permitido


    }


    /**
    * Index
    *
    * @author Gustavo Botega 
    */
    public function index() {
        $this->Fatura();
    }




    /**
    * Fatura
    *
    *
    * @author Gustavo Botega 
    */
    public function Fatura($Transacao){

        header('Access-Control-Allow-Origin: *');

        $FinanceiroId   =   $this->model_crud->get_rowSpecific('tb_financeiro', 'fin_transacao', $Transacao, 1, 'fin_id');


        $this->GetDetalhes();
        $this->GetResponsavelFinanceiro();
        $this->GetItem($FinanceiroId);
        $this->GetDesconto($FinanceiroId);
        
        
        
        
        $this->data['PackageStyles'][]    =   'Packages/Styles/Financeiro/FrontOffice/Fatura';
        
                
        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Financeiro';
        $this->data['PageHeadSubtitle']   = 'Detalhes da sua transacao financeira.';
        $this->data['Breadcrumbs']        = array();
        $this->data['NavActiveSidebar']   = '';
        
        /* Carrega View */
        $this->LoadTemplate('Template/FrontOffice/sistema/Financeiro/Fatura', $this->data);
        
    }



    /**
    * GetDetalhes
    *
    * @author Gustavo Botega 
    */
    public function GetDetalhes($TransacaoId = NULL){
        
        if(is_null($TransacaoId))
            $TransacaoId    =   $this->data['TransacaoId'];
        
        
        // Obtendo o Dados da Fatura
        $SqlDadosFatura   =    '
                                                    SELECT * FROM
                                                            tb_financeiro as fin
                                                    WHERE
                                                        fin.fin_transacao = '.$TransacaoId.'
                                                        
                                                    LIMIT 1
                                                ';
        $Query                              =   $this->db->query($SqlDadosFatura)->result();               
            

        if(empty($Query)) return json_encode(false);
        
        if(!empty($Query)):
        
            foreach ($Query[0] as $key => $value):
            
                $this->data['DadosFatura'][$key]    =   $value;
            
            endforeach;

        endif;
        
        
        return json_encode($this->data['DadosFatura']);
    }


    
        
    /**
    * GetResponsavelFinanceiro
    *
    * Refatorar esse bloco. O Endereco e o email, as informacoes do comprador devem ser impressas na transacao.
    * Motivo: se ele trocar o endereco altera aqui tambem. Sendo que o endereco era um na compra e nao pode mudar depois. Questoes fiscais.
    *         o mesmo vale pra email e outros dados. O dado uma vez capturado nao pode ser alterado. O dado deve ser estatico.
    *
    * @author Gustavo Botega 
    */
    public function GetResponsavelFinanceiro($TransacaoId = NULL){

        if(is_null($TransacaoId))
            $TransacaoId    =   $this->data['TransacaoId'];
        
        
        // Obtendo o ID do responsavel financeiro associado a fatura. 
        $ResponsavelFinanceiroId   =    $this->model_crud->get_rowSpecific('tb_financeiro', 'fin_transacao', $TransacaoId, 1, 'fk_pes_id');

                
        // Obtendo o Dados do Responsavel Financeiro
        $SqlDadosResponsavelFinanceiro      =    '
                                                    SELECT pes_id, pes_cpf_cnpj, pes_nome_razao_social, pes_foto FROM
                                                            tb_pessoa as pes
                                                    WHERE
                                                        pes.pes_id = "'.$ResponsavelFinanceiroId.'"

                                                    LIMIT 1
                                                ';
        $QueryDadosResponsavelFinanceiro   =   $this->db->query($SqlDadosResponsavelFinanceiro)->result();               
                
        // Obtendo o Dados de Endereço do Responsavel Financeiro
        $SqlDadosResponsavelFinanceiroEndereco      =    '
                                                    SELECT * FROM
                                                            tb_endereco as end
                                                    WHERE
                                                        end.fk_peo_id = "'.$ResponsavelFinanceiroId.'" AND
                                                        end.end_principal IS NOT NULL AND
                                                        end.fk_sta_id = 1

                                                    LIMIT 1
                                                ';
        $QueryDadosResponsavelFinanceiroEndereco   =   $this->db->query($SqlDadosResponsavelFinanceiroEndereco)->result();               
        
        
        // Obtendo o Dados de Email do Responsavel Financeiro
        $SqlDadosResponsavelFinanceiroEmail      =    '
                                                    SELECT * FROM
                                                        tb_email as ema
                                                    WHERE
                                                        ema.fk_peo_id = "'.$ResponsavelFinanceiroId.'" AND
                                                        ema.ema_principal IS NOT NULL AND
                                                        ema.fk_sta_id = 1

                                                    LIMIT 1
                                                ';
        $QueryDadosResponsavelFinanceiroEmail   =   $this->db->query($SqlDadosResponsavelFinanceiroEmail)->result();               
        
        
        
        if(empty($QueryDadosResponsavelFinanceiro) || empty($QueryDadosResponsavelFinanceiroEndereco) || empty($QueryDadosResponsavelFinanceiroEmail)) return json_encode(false);

        if(!empty($QueryDadosResponsavelFinanceiro) && !empty($QueryDadosResponsavelFinanceiroEndereco) && !empty($QueryDadosResponsavelFinanceiroEmail) ):

            foreach ($QueryDadosResponsavelFinanceiro[0] as $key => $value):

                $this->data['DadosResponsavelFinanceiro'][$key]    =   $value;

            endforeach;

            foreach ($QueryDadosResponsavelFinanceiroEndereco[0] as $key => $value):

                $this->data['DadosResponsavelFinanceiro'][$key]    =   $value;

            endforeach;
            $EstadoId   =   $this->data['DadosResponsavelFinanceiro']['fk_est_id'];
            $CidadeId   =   $this->data['DadosResponsavelFinanceiro']['fk_cid_id'];
            $this->data['DadosResponsavelFinanceiro']['estado']             =   $this->model_crud->get_rowSpecific('tb_estado', 'est_id', $EstadoId, 1, 'est_nome');
            $this->data['DadosResponsavelFinanceiro']['estado-sigla']       =   $this->model_crud->get_rowSpecific('tb_estado', 'est_id', $EstadoId, 1, 'est_sigla');
            $this->data['DadosResponsavelFinanceiro']['cidade']             =   $this->model_crud->get_rowSpecific('tb_cidade', 'cid_id', $CidadeId, 1, 'cid_nome');
        

            foreach ($QueryDadosResponsavelFinanceiroEmail[0] as $key => $value):

                $this->data['DadosResponsavelFinanceiro'][$key]    =   $value;

            endforeach;
        
        endif;


        return json_encode($this->data['DadosResponsavelFinanceiro']);        

    }


    /**
    * GetItem
    *
    * @author Gustavo Botega 
    */
    public function GetItem($FinanceiroId){


        $QueryItem    =   "
                                        SELECT * FROM
                                            tb_financeiro_item as fii

                                        WHERE fii.fk_fin_id = ".$FinanceiroId." AND
                                              fii.flag_deletado IS NULL AND
                                              fii.fk_aut2_id IS NULL

                                        ORDER BY fii_id ASC
                            "; 
        $this->data['DadosItem']    =   $this->db->query($QueryItem)->result();
        return true;
    }


    /**
    * GetDesconto
    *
    * @author Gustavo Botega 
    */
    public function GetDesconto($FinanceiroId){
        

        $QueryDesconto    =   "
                                        SELECT * FROM
                                            tb_financeiro_desconto as fid

                                        WHERE fid.fk_fin_id = ".$FinanceiroId." AND
                                              fid.flag_deletado IS NULL AND
                                              fid.fk_aut2_id IS NULL

                                        ORDER BY fid_id ASC
                            "; 
        $this->data['DadosDesconto']    =   $this->db->query($QueryDesconto)->result();
        return true;
    }









    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    */
    public function ThemeComponent(){ 


        /* 
            StylesFile 
        */
            /*  Plugins*/
            $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
            $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete']                           = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                    = TRUE;
            $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;

            /*  Syles*/
               $this->data['StylesFile']['Styles']['invoice']                                   = TRUE;



        /* 
            ScriptsFile 
        */
            /*  Plugins*/
            $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
            $this->data['ScriptsFile']['Plugins']['easy-autocomplete']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

            /*  Scripts*/
            $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                            = TRUE;

    }



    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function SigepeAsset()
    { 
        /* Carregando Estilos */
//        $this->data['PackageStyles'][]    =   'Packages/Styles/Animal/FrontOffice/Cadastro';


        /* Carregando Scripts */
//        $this->data['PackageScripts'][]   =   'Packages/Scripts/Animal/FrontOffice/Animal';




    }



     /*
             // @@@ Implementar permissões por usuario
        
        // AUDITORIA:
            [0]=> Slug Module
            [1]=> Table Database
            [2]=> Column Database
            [3]=> Value Deleted
            [4]=> Folder Thumb
            [5]=> Delete Gallery
            [6]=> Table Gallery
            [7]=> Column Table Gallery
            [8]=> Data/hora
            [9]=> IP
            [10]=> ID Usuário  
            [11]=> Type of Operation  
            */

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

