<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';
#include_once(APPPATH."third_party/pagarme/PagarMe.php");
require FCPATH.'/pagarme/pagarme-php/Pagarme.php';


/*
    ak_live_DOwxAmD3S45SpXF3GV7t5oI9Whu648
    ak_test_pW6VqWmqmOi3XASgJ2g4kKr9cu0DYE

    ek_live_IIpzTxJcAkbyzgjbBDXSsYiG73GsWM
    ek_test_2E7rIVgIqzo4dgpFEvCMuoDLy0GlE2
*/


//require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
class PagarmeVanguarda extends MY_FrontOffice {

    public $data;

    function __construct() {

        parent::__construct();

        $PessoaId                               =   $this->session->userdata('PessoaId');
        $this->data['PessoaId']                 =   $PessoaId;

        Pagarme::setApiKey('ak_live_DOwxAmD3S45SpXF3GV7t5oI9Whu648');


    }


    /**
    * Index
    *
    * @author Gustavo Botega
    */
    public function index() {
        $this->Dashboard();
    }




    /**
    * CaptureTransactionBoleto
    *
    * @author Gustavo Botega
    */
    public function CaptureTransactionBoleto() { /*$Amout $ResponsavelFinanceiroId*/

    }





    /**
    * CaptureTransactionCartaoCredito
    *
    * @author Gustavo Botega
    */
    public function CaptureTransactionCartaoCredito() {


   }










    /**
    * HandleNaturezaOperacao
    *
    * @author Gustavo Botega
    */
    public function HandleNaturezaOperacao( ) {

        $POST   =   $_POST;

        // Tratando casos de acordo com a natureza de operacao
        switch ($POST['SigepeNaturezaOperacao']):
            case '1': // Registro Atleta
                $HandleRegistro     =   $this->HandleRegistro( $POST);
                echo json_encode( $HandleRegistro );
                break;

            case '2': // Registro Animal
                $HandleRegistro     =   $this->HandleRegistro( $POST);
                echo json_encode( $HandleRegistro );
                break;
            case '3': // Passaporte
                # code...
                break;

            case '4': // Selo
                # code...
                break;

            case '5': // Inscricao
                $HandleInscricao     =   $this->HandleInscricao( $POST);
                echo json_encode( $HandleInscricao );
                break;

        endswitch;

    }





    /**
    * HandleInscricao
    *
    * @author Gustavo Botega
    */
    public function HandleInscricao($POST) {

        // Carregando modulos externos
        $this->load->module('financeiro/FrontOffice/Transacao');


        // Resgatando dados
        $Token                          =  $_POST['DatasetPagarme']['token'];
        $Amount                         =  $_POST['PagarmeAmount'];
    #    $SigepeTransacao                =  $_POST['SigepeTransacao'];
        $SigepeNaturezaOperacao         =  $_POST['SigepeNaturezaOperacao'];
#        $FinanceiroId                   =  $this->model_crud->get_rowSpecific('tb_financeiro', 'fin_transacao', $_POST['SigepeTransacao'], 1, 'fin_id');#        $TipoPagamento                  =  $this->model_crud->get_rowSpecific('tb_financeiro', 'fin_transacao', $_POST['SigepeTransacao'], 1, 'fk_fip_id');

        /*
        $RegistroTaxa                   =  $this->model_crud->get_rowSpecific('tb_registro', 'reg_id', $_POST['SigepeRegistroId'], 1, 'fk_ret_id');
        $RegistroTaxaExtra              =  $this->model_crud->get_rowSpecific('tb_registro', 'reg_id', $_POST['SigepeRegistroId'], 1, 'fk_rte_id');
        $RegistroTaxaDesconto           =  $this->model_crud->get_rowSpecific('tb_registro', 'reg_id', $_POST['SigepeRegistroId'], 1, 'fk_rtd_id');
        */

        // Realizando a captura. Nesse momento a transação ja vem autorizada pelo checkout da pagarme
        $ArrMetaData                    = array(
                      #                        "SIGEPE Transacao"            => $SigepeTransacao,
                      #                        "SIGEPE Financeiro"           => $FinanceiroId,
                                              "SIGEPE Fatural Global Id"        => $_POST['FaturaGlobalId'],
                                              "SIGEPE Fatural Simples Id"       => $_POST['FaturaSimplesId'],
                                              "SIGEPE Fatural Global Controle"  => $_POST['FaturaGlobalControle'],
                                              "SIGEPE Fatural Simples Controle" => $_POST['FaturaSimplesControle'],
                                              "SIGEPE Evento ID"                => $_POST['EventoId'],
                                              "SIGEPE Serie Id"                 => $_POST['SerieId'],
                                              "SIGEPE Competidor Id"            => $_POST['AtletaId'],
                                              "SIGEPE Animal Id"                => $_POST['AnimalId']
                                            );
        $Transaction                    =   $this->CapturarTransacao($Token, $Amount, $ArrMetaData);


        // Atualiza financeiro com o ID da transacao do pagarme e outros dados
  #      $this->AtualizarFinanceiro($SigepeTransacao, $Transaction, $POST);

        // Atualizar boleto fatura simples
        $DatasetUpdate      =   array(
                                'frf_boleto'    =>  $Transaction['boleto_url']
                            );
        $this->db->where('frf_controle', $POST['FaturaSimplesControle']);
        $query = $this->db->update('tb_evento_faturaglobal_rel_faturasimples', $DatasetUpdate);



        $Sucesso        =   array(
                                'BoletoUrl'             => $Transaction['boleto_url'],
                                'BoletoCodigoBarras'    => $Transaction['boleto_barcode'],
                                'BoletoVencimento'      => $Transaction['boleto_expiration_date']
                            );
        return $Sucesso;

    }







    /**
    * HandleRegistro
    *
    * @author Gustavo Botega
    */
    public function HandleRegistro($POST) {

        // Carregando modulos externos
        $this->load->module('financeiro/FrontOffice/Transacao');


        // Resgatando dados
        $Token                          =  $_POST['DatasetPagarme']['token'];
        $Amount                         =  $_POST['PagarmeAmount'];
        $SigepeTransacao                =  $_POST['SigepeTransacao'];
        $SigepeNaturezaOperacao         =  $_POST['SigepeNaturezaOperacao'];
        $FinanceiroId                   =  $this->model_crud->get_rowSpecific('tb_financeiro', 'fin_transacao', $_POST['SigepeTransacao'], 1, 'fin_id');
        $TipoPagamento                  =  $this->model_crud->get_rowSpecific('tb_financeiro', 'fin_transacao', $_POST['SigepeTransacao'], 1, 'fk_fip_id');

        $RegistroTaxa                   =  $this->model_crud->get_rowSpecific('tb_registro', 'reg_id', $_POST['SigepeRegistroId'], 1, 'fk_ret_id');
        $RegistroTaxaExtra              =  $this->model_crud->get_rowSpecific('tb_registro', 'reg_id', $_POST['SigepeRegistroId'], 1, 'fk_rte_id');
        $RegistroTaxaDesconto           =  $this->model_crud->get_rowSpecific('tb_registro', 'reg_id', $_POST['SigepeRegistroId'], 1, 'fk_rtd_id');


        // Realizando a captura. Nesse momento a transação ja vem autorizada pelo checkout da pagarme
        $ArrMetaData                    = array(
                                              "SIGEPE Transacao"            => $SigepeTransacao,
                                              "SIGEPE Financeiro"           => $FinanceiroId,
                                              "SIGEPE Registro"             => $POST['SigepeRegistroId'],
                                              "SIGEPE Taxa ( Extra )"       => $RegistroTaxaExtra,
                                              "SIGEPE Taxa ( Registro )"    => $RegistroTaxa,
                                              "SIGEPE Taxa ( Desconto )"    => $RegistroTaxaDesconto
                                            );
        $Transaction                    =   $this->CapturarTransacao($Token, $Amount, $ArrMetaData);


        // Atualiza financeiro com o ID da transacao do pagarme e outros dados
        $this->AtualizarFinanceiro($SigepeTransacao, $Transaction, $POST);


        // Se o pagamento for do tipo 2 ( CARTAO DE CREDITO (Pagar.me ) executa o bloco abaixo.
        if($TipoPagamento == '2'):


            //  Setando Transacao como Paga
            $DatasetFaturaPago              =   array(
                                                    'Historico'     =>  array(
                                                                            'tipo'      =>  2, // Atualizacao automatica gateway
                                                                            'historico' =>  'Pagamento confirmado via cartão de credito.'
                                                                        )
                                                );
            $this->transacao->SetTransacaoPago(NULL, $SigepeTransacao, $DatasetFaturaPago);


            // Atualizando o Status como ativo.
            $this->load->module('registro/FrontOffice/Registro');
            $this->registro->SetStatusAtivo( $POST['SigepeRegistroId'] );

        endif;



        $Sucesso        =   array(
                                'BoletoUrl'             => $Transaction['boleto_url'],
                                'BoletoCodigoBarras'    => $Transaction['boleto_barcode'],
                                'BoletoVencimento'      => $Transaction['boleto_expiration_date']
                            );
        return $Sucesso;

    }




    /**
    * CapturarTransacao
    *
    * @author Gustavo Botega
    */
    public function CapturarTransacao($Token, $Amount, $Metadata) {

        $transaction = PagarMe_Transaction::findById( $Token );

        // Realizando a capture
        $transaction->capture(
                                  array(
                                      "amount" => $Amount,
                                      "boleto_instructions" => 'Instrucsto bolets',
                                      "metadata" => $Metadata
                                  )
                               );

        return $transaction;

    }



    /**
    * AtualizarFinanceiro
    *
    * @author Gustavo Botega
    */
    public function AtualizarFinanceiro($SigepeTransacao, $Transaction, $POST) {

        $DatasetUpdate      =   array(
                                'fk_fib_id'                     =>  $this->GetBandeiraCartao( $Transaction['card_brand'] ), // Obtendo o id da bandeira do cartao. Se for boleto resultado e NULL.
                                'fin_pagarme_token'             =>  $POST['DatasetPagarme']['token'],
                                'fin_transacao_pagarme'         =>  $Transaction['tid'],
                                'fin_cartao_titular'            =>  $Transaction['card_holder_name'],
                                'fin_cartao_primeiros_digitos'  =>  $Transaction['card_first_digits'],
                                'fin_cartao_ultimos_digitos'    =>  $Transaction['card_last_digits'],
                                'fin_cartao_data_expiracao'     =>  $Transaction->card['expiration_date'],
                                'fin_quantidade_parcela'        =>  $Transaction['installments'],
                                'fin_boleto'                    =>  $Transaction['boleto_url'],
                                'fin_boleto_codigo_barras'      =>  $Transaction['boleto_barcode'],
                                'fin_vencimento'                =>  $Transaction['boleto_expiration_date'],
                                'fin_ip'                        =>  $Transaction['ip']
                            );

        $this->db->where('fin_transacao', $SigepeTransacao);
        $query = $this->db->update('tb_financeiro', $DatasetUpdate);

        return $query;

    }






    /**
    * GetBandeiraCartao
    *
    * @author Gustavo Botega
    */
    public function GetBandeiraCartao($Bandeira) {

        switch ($Bandeira):
            case 'mastercard':
                $BandeiraId     =   1;
                break;

            case 'visa':
                $BandeiraId     =   2;
                break;

            case 'amex':
                $BandeiraId     =   3;
                break;

            case 'jcb':
                $BandeiraId     =   4;
                break;

            case 'elo':
                $BandeiraId     =   5;
                break;

            case 'hipercard':
                $BandeiraId     =   6;
                break;

            case 'diners':
                $BandeiraId     =   7;
                break;

            case 'aura':
                $BandeiraId     =   8;
                break;

            case 'discover':
                $BandeiraId     =   9;
                break;

            default:
                $BandeiraId     =   NULL; // bandeira nao identificada
                break;
        endswitch;

        return $BandeiraId;

    }






}

/* End of file */
