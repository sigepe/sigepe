<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';
class Inscricao extends MY_FrontOffice {

    function __construct() {
        parent::__construct();

        $this->data['DatasetPagarmeCheckout']   =   TRUE;
        $PessoaId                               =   $this->session->userdata('PessoaId');
        $this->data['PessoaId']                 =   $PessoaId;

		$this->ThemeComponent();
		$this->SigepeAsset();

    $this->db->reconnect();

    }

    public function index() {
		$this->gear();

    }

    public function dashboard(){

        /*PEGAR A HORA LOCAL DA PESSOA. QUEM ACESSAR POR NY VAI TER UM HORARIO DIFERENTE DE QUEM ACESSAR POR BR */
        $Settings['PageHeadTitle']      = 'Bom dia Gustavo, tudo bem? '; /* */
        $Settings['PageHeadSubtitle']   = 'Seu último acesso foi às 18h40 em 16/04/2017.';


        $this->loadTemplateUser('template/user/tiles', $Settings);
    }




    public function EventoInscricaoAberta(){

        $Settings['PageHeadTitle']      = 'Bom dia Gustavo, tudo bem? ';
        $Settings['PageHeadSubtitle']   = 'Seu último acesso foi às 18h40 em 16/04/2017.';

        $this->LoadTemplateInscricao(
                'template/user/sistema/inscricao/evento/InscricaoAberta',
                $this->data
        );
    }



    public function Formulario( $EventoId = NULL ){

        $Settings['PageHeadTitle']      = 'Bom dia Gustavo, tudo bem? ';
        $Settings['PageHeadSubtitle']   = 'Seu último acesso foi às 18h40 em 16/04/2017.';






        /*  EVENTO */
        $SqlEvento                      = '
                                            SELECT * FROM tb_evento
                                            WHERE
                                                eve_id = '.$EventoId.'
                                            LIMIT 1
                                            ';
        $this->data['DatasetEvento']   = $this->db->query($SqlEvento)->result();


        $DataLimiteSemAcrescimo = new DateTime($this->data['DatasetEvento'][0]->eve_data_limite_sem_acrescimo);
        $DataAtual = new DateTime();

        if ($DataLimiteSemAcrescimo > $DataAtual)
        { // promocional
          $this->data['PagarmeVencimentoBoleto']   = $this->data['DatasetEvento'][0]->eve_data_limite_sem_acrescimo;
          $this->data['PoliticaPreco']             = 'promocional';
        }else{ // cheio
          $this->data['PagarmeVencimentoBoleto']   = $this->data['DatasetEvento'][0]->eve_site_inscricao_fim;
          $this->data['PoliticaPreco']             = 'cheio';
        }





        $this->data['EventoId']        = $this->data['DatasetEvento'][0]->eve_id;


        $SqlSerie                      = '
                                            SELECT * FROM tb_evento_rel_serie
                                            WHERE
                                                fk_eve_id = '.$EventoId.'
                                            ORDER BY fk_evs_id DESC
                                            ';
        $this->data['Series']   = $this->db->query($SqlSerie)->result();

        $SqlProvas                      = '
                                            SELECT * FROM tb_evento_rel_serie
                                            WHERE
                                                fk_eve_id = '.$EventoId.'
                                            ORDER BY fk_evs_id DESC
                                            ';
        $this->data['Provas']   = $this->db->query($SqlProvas)->result();
        $this->data['DatasetProvasJson']   = json_encode($this->retornarProvasDoEventoJson($EventoId));




        $sqlArrayProvas = "
          SELECT prv.srp_id
          , concat(prv.srp_dia, ' ', prv.srp_hora) as srp_data_hora
          , prv.srp_numero_prova
          , prv.srp_nome
          , prv.fk_spc_id /*caracteristicas*/
          , caracter.spc_caracteristica
          , prv.fk_spp_id /*fk_pista*/
          , pista.spp_pista /*pista*/
          , prv.srp_valor_promocional, prv.srp_valor
          
          , null as categorias
          , prv.fk_ers_id /*fk_da série*/
          , serie.fk_eve_id /*fk_evento*/
          from tb_evento_rel_serie_rel_prova as prv
          join tb_evento_rel_serie as serie on prv.fk_ers_id = serie.ers_id
          join tb_evento_serie_prova_pista as pista on prv.fk_spp_id = pista.spp_id
          join tb_evento_serie_prova_caracteristica as caracter on prv.fk_spc_id = caracter.spc_id
          where serie.fk_eve_id = $EventoId

          order by prv.srp_numero_prova asc
          ";
      $arrayProvas = $this->db->query($sqlArrayProvas)->result();









      foreach ($arrayProvas as $prv) {
        $sqlCategoriasDaProva = "
            SELECT prc_id, fk_srp_id, fk_evc_id, srp_id, srp_nome, evc_sigla, evc_categoria
            from tb_evento_rel_serie_rel_prova_rel_categoria as prv_rel_cat
            join tb_evento_categoria as cat on prv_rel_cat.fk_evc_id = cat.evc_id
            join tb_evento_rel_serie_rel_prova as prv on prv_rel_cat.fk_srp_id = prv.srp_id
            where srp_id = $prv->srp_id ORDER by evc_sigla asc";
        $prv->categorias = $this->db->query($sqlCategoriasDaProva)->result();
      }

      $this->data['ProvasCategorias']   =  $arrayProvas;






        $SqlCategoriasDaSerie            = '
                                              SELECT evc_id as CategoriaId, evc_sigla as CategoriaSigla, evc_categoria as Categoria, fk_ers_id as SerieId, ESERIE.fk_eve_id
                                              FROM ( SELECT EVC_ID, EVC_SIGLA, EVC_CATEGORIA, FK_ERS_ID FROM tb_evento_categoria as evcat
                                                    JOIN tb_evento_rel_serie_rel_categoria as ersrc ON ersrc.fk_evc_id = evcat.evc_id
                                                    GROUP BY ersrc.fk_ers_id, ersrc.fk_evc_id) CAT_SERIE
                                              JOIN tb_evento_rel_serie  ESERIE ON ESERIE.ers_id = CAT_SERIE.FK_ERS_ID WHERE ESERIE.fk_eve_id = '.$EventoId.'

                                              GROUP BY EVC_SIGLA
                                              ORDER BY EVC_SIGLA

                                            ';
        $CategoriasDaSerie               = $this->db->query($SqlCategoriasDaSerie)->result();

        $this->data['CategoriasDaSerie']      = $CategoriasDaSerie;
        $this->data['JsonCategoriasDaSerie']  = json_encode($CategoriasDaSerie);


        $SqlCategoriasDaProva            = '
                                            select pro_cat.*, prova.fk_ers_id from (select rel_pro_cat.fk_srp_id, evcat.evc_id, evcat.evc_sigla, evcat.evc_categoria from tb_evento_categoria evcat join tb_evento_rel_serie_rel_prova_rel_categoria as rel_pro_cat on rel_pro_cat.fk_evc_id = evcat.evc_id ) pro_cat
                                            join tb_evento_rel_serie_rel_prova as prova on prova.srp_id = pro_cat.fk_srp_id

                                            join tb_evento_rel_serie as serie on serie.ers_id = prova.fk_ers_id
                                            WHERE serie.fk_eve_id = '.$EventoId.'
                                            group by evc_sigla
                                            order by evc_sigla ASC
                                            ';
        $CategoriasDaProva               = $this->db->query($SqlCategoriasDaProva)->result();

        $this->data['CategoriasDaProva'] = $CategoriasDaProva;




        $SqlSeriesEvento            = '
                                              SELECT * FROM tb_evento_rel_serie as ers
                                              WHERE ers.fk_eve_id = '.$EventoId.'
                                              ORDER BY ers_nome ASC
                                            ';
        $SeriesDoEvento               = $this->db->query($SqlSeriesEvento)->result();
        $this->data['SeriesDoEvento']      = $SeriesDoEvento;


        $this->data['GetSeriePorCategoria']   = json_encode($this->GetSeriePorCategoria($EventoId));





        $SqlProvasDoEvento            = '
                                            SELECT * FROM tb_evento_rel_serie_rel_prova as srp

                                            INNER JOIN tb_evento_rel_serie as ers
                                            ON srp.fk_ers_id = ers.ers_id

                                            WHERE ers.fk_eve_id = '.$EventoId.'
                                            ORDER BY srp.srp_nome ASC
                                            ';
        $ProvasDoEvento                    = $this->db->query($SqlProvasDoEvento)->result();
        $this->data['ProvasDoEvento']      = $ProvasDoEvento;


        $this->data['GetSeriePorCategoria']   = json_encode($this->GetSeriePorCategoria($EventoId));







        $this->DatasetPagarme();  // Carrega dados para serem processados no gateway


        $this->LoadTemplateInscricao(
                                        array(
                                          'Template/FrontOffice/sistema/Inscricao/Conjunto',
                                          'Template/FrontOffice/sistema/Inscricao/Serie',
                                          'Template/FrontOffice/sistema/Inscricao/Prova',
                                          'Template/FrontOffice/sistema/Inscricao/Opcionais',
                                          'Template/FrontOffice/sistema/Inscricao/Resumo'
                                        ), $this->data
                                    );

    }





      public function retornarProvasDoEventoJson($EventoId){

        $sqlArrayProvas = "
                 SELECT prv.srp_id
                 , prv.srp_numero_prova /* número da prova */
                 , prv.srp_nome /* nome da prova */
                 , null as categorias /* array categorias */
                 , evento.eve_data_limite_sem_acrescimo as data_limite /*data sem acrescimo do evento*/
                 , case when DATE_FORMAT(now(), '%Y-%m-%d') <= DATE_FORMAT(evento.eve_data_limite_sem_acrescimo, '%Y-%m-%d')
                 then prv.srp_valor_promocional else prv.srp_valor end as valor
                 from tb_evento_rel_serie_rel_prova as prv
                 join tb_evento_rel_serie as serie on prv.fk_ers_id = serie.ers_id
                 join tb_evento as evento on evento.eve_id = serie.fk_eve_id
                 where serie.fk_eve_id = $EventoId";





        $arrayJsonProvas = $this->db->query($sqlArrayProvas)->result();


        foreach ($arrayJsonProvas as $prv) {
          $sqlCategoriasDaProva = "
            SELECT fk_evc_id
            from tb_evento_rel_serie_rel_prova_rel_categoria prv_rel_cat
            join tb_evento_rel_serie_rel_prova prv on prv_rel_cat.fk_srp_id = prv.srp_id
            where srp_id = $prv->srp_id order by fk_evc_id";
            $categorias = $this->db->query($sqlCategoriasDaProva)->result();
            $jsonCat = array();
            foreach ($categorias as $cat) {
              array_push($jsonCat, $cat->fk_evc_id);
            }
            $prv->categorias = $jsonCat;
        }

        $json = array();
        foreach ($arrayJsonProvas as $prv) {
          $json[$prv->srp_id] = $prv;
        }

        return $json;
      } // end of method




      /**
      * GetSeriePorCategoria
      *
      * @author Gustavo Botega
      */
      public function GetSeriePorCategoria($EventoId){
          $SqlSeries                  =   "
                                              SELECT * FROM tb_evento_rel_serie
                                              WHERE
                                                  fk_eve_id = '".$EventoId."'
                                              ORDER BY fk_evs_Id ASC

                                          ";
          $QuerySerie                      =   $this->db->query($SqlSeries)->result();


          $ArrSerie  = array();
          foreach ($QuerySerie as $KeySerie => $ValueSerie) {

              $SqlCategoriasDaSerie                  =   "
                                                            SELECT * FROM tb_evento_rel_serie_rel_categoria
                                                            WHERE
                                                                fk_ers_id = '".$ValueSerie->ers_id."'

                                                         ";
              $QueryCategoriasDaSerie               =   $this->db->query($SqlCategoriasDaSerie)->result();


              $SqlProvasDaSerie                  =   "
                                                            SELECT * FROM tb_evento_rel_serie_rel_prova
                                                            WHERE
                                                                fk_ers_id = '".$ValueSerie->ers_id."'
                                                            ORDER BY srp_numero_prova
                                                         ";
              $QueryProvasDaSerie               =   $this->db->query($SqlProvasDaSerie)->result();


              $SerieId  = $ValueSerie->ers_id;

              $ArrSerie[$SerieId]['SerieId']                = $ValueSerie->ers_id;
              $ArrSerie[$SerieId]['SerieNome']              = $ValueSerie->ers_nome;
              $ArrSerie[$SerieId]['SeriePrecoPromocional']  = $ValueSerie->ers_valor_ate_inscricao_definitiva;
              $ArrSerie[$SerieId]['SeriePrecoCheio']        = $ValueSerie->ers_valor_apos_inscricao_definitiva;
              $ArrSerie[$SerieId]['SerieCategorias']        = array();
              $ArrSerie[$SerieId]['SerieProvas']            = array();

              foreach ($QueryCategoriasDaSerie as $key => $value) {
                $ArrSerie[$SerieId]['SerieCategorias'][]        = $value->fk_evc_id;
              }

              foreach ($QueryProvasDaSerie as $key => $value) {
                $ArrSerie[$SerieId]['SerieProvas'][]        = $value->srp_id;
              }


          }


          return $ArrSerie;


      }





      /**
      * AjaxProcessar
      *
      * @author Gustavo Botega
      */
      public function AjaxProcessar(){


        $AtletaId               =   $_POST['AtletaId'];
        $AtletaCategoriaId      =   $_POST['AtletaCategoriaId'];
        $AnimalId               =   $_POST['AnimalId'];
        $SerieId                =   $_POST['SerieId'];
        $EventoId               =   $_POST['EventoId'];


        var_dump($SerieId);
        return true;


        $PrecoPromocional                       =   $this->model_crud->get_rowSpecific('tb_evento_rel_serie', 'ers_id', $SerieId, 1, 'ers_valor_ate_inscricao_definitiva');
        $PrecoCheio                             =   $this->model_crud->get_rowSpecific('tb_evento_rel_serie', 'ers_id', $SerieId, 1, 'ers_valor_apos_inscricao_definitiva');


        // GRAVAR FATURA GLOBAL
        $Dataset       =  array(

            'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
            'fk_eve_id'                         =>    $EventoId,

            'fk_pes_id'                         =>    $AtletaId,
            'fk_ani_id'                         =>    $AnimalId,
            'fk_sta_id'                         =>    1,

            'evf_valor'                         =>    $PrecoPromocional,
            'evf_controle'                      =>    $this->GerarTimestamp('tb_evento_faturaglobal', 'evf_controle'),
            'criado'                            =>    date("Y-m-d H:i:s")

        );
        $Query = $this->db->insert('tb_evento_faturaglobal', $Dataset);

        $FaturaGlobalId                         = $this->db->insert_id();




        // GRAVAR FATURA SIMPLES
        $DatasetFaturaSimples       =  array(

            'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
            'fk_evf_id'                         =>    $FaturaGlobalId,

            'fk_pes_id'                         =>    $AtletaId,
            'fk_ers_id'                         =>    $SerieId,
            'fk_evc_id'                         =>    $AtletaCategoriaId,
            'fk_ani_id'                         =>    $AnimalId,
            'fk_sta_id'                         =>    1,

            'frf_valor'                         =>    $PrecoPromocional,
            'frf_controle'                      =>    $this->GerarTimestamp('tb_evento_faturaglobal_rel_faturasimples', 'frf_controle') + 1,

            'frf_valor_promocional_serie'       =>    $PrecoPromocional,
            'frf_valor_serie'                   =>    $PrecoCheio,

            'criado'                            =>    date("Y-m-d H:i:s")

        );
        $Query = $this->db->insert('tb_evento_faturaglobal_rel_faturasimples', $DatasetFaturaSimples);

        echo json_encode(true);


      }



      public function GerarTimestamp($Entidade, $Coluna){

        $Time   = time();

        $Sql  = "select * from ".$Entidade." where ".$Coluna." = ".$Time." ";
        $Query              =   $this->db->query($Sql)->result();
        if(empty($Query)){
          return $Time;
        }else{
          $this->GerarTimestamp();
        }

      }





        /**
        * JsonProprietario
        *
        * @author Gustavo Botega
        */
        public function JsonAtleta(){


            $QueryJson         =   "
                                            SELECT * FROM tb_pessoa as pes

                                            WHERE
                                                pes.pes_natureza    IN ('PF') AND
                                                pes.fk_sta_id       =  1

                                            ORDER BY
                                                FIELD('pes_natureza', 'PF') ASC,
                                                pes_nome_razao_social ASC

                                            ";
            $Query                      =   $this->db->query($QueryJson)->result();


            $array  = array();
            foreach ($Query as $key => $value) {

                $array[]       =  array(
                                    'name'      => strtoupper($value->pes_nome_razao_social),
                                    'id'        => $value->pes_id,
                                ) ;

            }


            echo json_encode($array);

        }






        /**
        * JsonAnimal
        *
        * @author Gustavo Botega
        */
        public function JsonAnimal(){


            $QueryJson         =   "
                                            SELECT * FROM tb_animal as ani

                                            ORDER BY
                                                ani_nome_completo

                                            ";
            $Query                      =   $this->db->query($QueryJson)->result();


            $array  = array();
            foreach ($Query as $key => $value) {

                $array[]       =  array(
                                    'name'      => strtoupper($value->ani_nome_completo),
                                    'id'        => $value->ani_id,
                                ) ;

            }


            echo json_encode($array);

        }
































































       /********************** HERANCA DO MODULO DE REGISTRO ******************************/

           /**
           * Processar
           *
           *
           * @author Gustavo Botega
           */
           public function Processar(){

               // Inicializando dados. Prevent errors.
               $NaturezaOperacao                                =  NULL;

               // Carregando modulos externos
               $this->load->module('financeiro/FrontOffice/Transacao'); // Carregando a classe transacao do modulo financeiro do ambiente FrontOffice
               $this->load->module('financeiro/FrontOffice/FinanceiroInscricao'); // Carregando a classe inscricao do modulo financeiro do ambiente FrontOffice


               // Resgatando valores da view
               $DadosUnserialized['AtletaId']              =   $_POST['AtletaId'];
               $DadosUnserialized['AtletaCategoriaId']     =  $_POST['AtletaCategoriaId'];
               $DadosUnserialized['AnimalId']              =   $_POST['AnimalId'];
               $DadosUnserialized['SerieId']               =   $_POST['SerieId'];
               $DadosUnserialized['EventoId']              =   $_POST['EventoId'];


               // Obtendo valores de taxas do registro a partir das informacoes enviadas na view
               $DatasetValores                         =       array('valor-promocional', 'valor-cheio', 'valor');


               // Gravando a Transacao
               $DatasetComplementar                    =   array(
                                                               'responsavel-financeiro'    =>  $this->session->userdata('PessoaId')
                                                           );
               $Transacao                              =    $this->GravarTransacao($DatasetValores, $DatasetComplementar );


               // Gravanda a Inscricao
               $Inscricao                              =    $this->Gravar($DatasetValores, $Transacao, $DadosUnserialized);
               


               // Gravando Registro no Financeiro
              # $this->financeiroregistro->Gravar( $Transacao['FinanceiroId'],  $Registro['Id'], $DatasetValores );


               // Gravando Relacionamento Financeiro - Registro
            #  $this->financeiroinscricao->GravarRelacionamento( $Transacao['FinanceiroId'],  $Inscricao['Id'] );


               // Tratando situacoes de erro.
               echo json_encode(
                               array(
                                   #'Transacao'         =>  $Transacao['Transacao'],
                                   #'FinanceiroId'      =>  $Transacao['FinanceiroId'],
                                   'FaturaSimplesId'          =>  $Inscricao['FaturaSimplesId'],
                                   'FaturaGlobalId'           =>  $Inscricao['FaturaGlobalId'],
                                   'FaturaSimplesControle'    =>  $Inscricao['FaturaSimplesControle'],
                                   'FaturaGlobalControle'     =>  $Inscricao['FaturaGlobalControle']
                               )
                           );

           }




           /**
           * Gravar
           *
           * @author Gustavo Botega
           */
           public function Gravar($DatasetValores = NULL, $Transacao = NULL, $DadosUnserialized){

               // Iniciando variavel. Prevent error.
               $DatasetResultado   =   array();



               $AtletaId               =   $_POST['AtletaId'];
               $AtletaCategoriaId      =   $_POST['AtletaCategoriaId'];
               $AnimalId               =   $_POST['AnimalId'];
               $SerieId                =   $_POST['SerieId'];
               $EventoId               =   $_POST['EventoId'];



               if(empty($SerieId))
                  $SerieId = NULL;
               

               $FlagSerie              =   $_POST['FlagSerie'];   
               $FlagProva              =   $_POST['FlagProva'];   





               if(!isset($FlagSerie) || $FlagSerie == '2') 
                  $FlagSerie           =  NULL;



              if(isset($_POST['ValorBaia']) && isset($_POST['QtdeBaia'])){
                 $ValorBaia               =   $_POST['ValorBaia'];
                 $QtdeBaia                =   $_POST['QtdeBaia'];
              }else{
                 $ValorBaia               =   0.00;
                 $QtdeBaia                =   0;
              }


              if(isset($_POST['ValorQs']) && isset($_POST['ValorQs'])){
                 $ValorQs                 =   $_POST['ValorQs'];
                 $QtdeQs                  =   $_POST['QtdeQs'];
              }else{
                 $ValorQs               =   0.00;
                 $QtdeQs                =   0;
              }

               $TotalOpcional           =  ( $ValorBaia * $QtdeBaia ) + ( $ValorQs * $QtdeQs );


               $PrecoPromocional                       =   $this->model_crud->get_rowSpecific('tb_evento_rel_serie', 'ers_id', $SerieId, 1, 'ers_valor_ate_inscricao_definitiva');
               $PrecoCheio                             =   $this->model_crud->get_rowSpecific('tb_evento_rel_serie', 'ers_id', $SerieId, 1, 'ers_valor_apos_inscricao_definitiva');
               $FaturaGlobalControle                   =   $this->GerarTimestamp('tb_evento_faturaglobal', 'evf_controle');



                /*  EVENTO */
                $SqlEvento                      = '
                                                    SELECT * FROM tb_evento
                                                    WHERE
                                                        eve_id = '.$EventoId.'
                                                    LIMIT 1
                                                    ';
                $this->data['DatasetEvento']   = $this->db->query($SqlEvento)->result();



                $DataLimiteSemAcrescimo = new DateTime($this->data['DatasetEvento'][0]->eve_data_limite_sem_acrescimo);
                $DataAtual = new DateTime();

                if ($DataLimiteSemAcrescimo > $DataAtual){ // promocional
                   $ValorSerie      =   $PrecoPromocional;
                }else{ // cheio
                   $ValorSerie      =   $PrecoCheio;
                }

                $TotalFatura        =   $ValorSerie + $TotalOpcional;

               // GRAVAR FATURA GLOBAL
               $Dataset       =  array(

                   'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
                   'fk_eve_id'                         =>    $EventoId,

                   'fk_pes_id'                         =>    $AtletaId,
                   'fk_ani_id'                         =>    $AnimalId,
                   'fk_sta_id'                         =>    1,

                   'evf_valor'                         =>    $TotalFatura,
                   'evf_controle'                      =>    $FaturaGlobalControle,
                   'criado'                            =>    date("Y-m-d H:i:s")

               );
               $Query = $this->db->insert('tb_evento_faturaglobal', $Dataset);
               $FaturaGlobalId                         = $this->db->insert_id();



               $FaturaSimplesControle        =    $this->GerarTimestamp('tb_evento_faturaglobal_rel_faturasimples', 'frf_controle') + 1;

               // GRAVAR FATURA SIMPLES
               $DatasetFaturaSimples       =  array(

                   'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
                   'fk_evf_id'                         =>    $FaturaGlobalId,

                   'fk_pes_id'                         =>    $AtletaId,
                   'fk_ers_id'                         =>    $SerieId,
                   'fk_evc_id'                         =>    $AtletaCategoriaId,
                   'fk_ani_id'                         =>    $AnimalId,
                   'fk_sta_id'                         =>    1,

                   'frf_valor'                         =>    $TotalFatura,
                   'frf_controle'                      =>    $FaturaSimplesControle,

                   'frf_valor_promocional_serie'       =>    $PrecoPromocional,
                   'frf_valor_serie'                   =>    $PrecoCheio,
                    
                   'frf_valor_baia'                    =>    $ValorBaia,
                   'frf_quantidade_baia'               =>    $QtdeBaia,

                   'frf_valor_quarto'                  =>    $ValorQs,
                   'frf_quantidade_quarto'             =>    $QtdeQs,

                   'criado'                            =>    date("Y-m-d H:i:s")

               );
               $Query = $this->db->insert('tb_evento_faturaglobal_rel_faturasimples', $DatasetFaturaSimples);
               $FaturaSimplesId                         = $this->db->insert_id();




               // Gravar na tabela inscricao ( obter todas as provas da SerieId e gravar )
               // Get todas as provas da serie
               if(isset($FlagSerie) && $FlagSerie == '1'):
                  

                   $Sql                     =   "
                                                   SELECT * FROM tb_evento_rel_serie_rel_prova as srp
                                                   WHERE
                                                       srp.fk_ers_id = ".$SerieId."
                                                   ";
                   $Query               =   $this->db->query($Sql)->result();
                   $Valor     =   0.00;


                   foreach ($Query as $key => $value):


                      if ($DataLimiteSemAcrescimo > $DataAtual){ // promocional
                        $Valor =  $value->srp_valor_promocional;
                      }else{ // cheio
                        $Valor =  $value->srp_valor;
                      }


                       // Comparar datas
                       $DatasetInscricao       =  array(
                           'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
                           'fk_frf_id'                         =>    $FaturaSimplesId,
                           'fk_srp_id'                         =>    $value->srp_id,
                           'fk_sta_id'                         =>    2,
                           'fri_valor'                         =>    $Valor,
                           'flag_serie'                        =>    $FlagSerie,
                           'criado'                            =>    date("Y-m-d H:i:s")
                       );
                       $Query     =   $this->db->insert('tb_evento_faturaglobal_rel_faturasimples_rel_inscricao', $DatasetInscricao);

                   endforeach;


               endif;


               if(isset($FlagProva) && $FlagProva == '1'):

                  // Percorrendo o array de provas
                  foreach ($_POST['ArrProvas'] as $key => $ProvaId) {

                     $Sql                     =   "
                                                     SELECT * FROM tb_evento_rel_serie_rel_prova as srp
                                                     WHERE
                                                         srp.srp_id = ".$ProvaId."
                                                     ";
                     $Query               =   $this->db->query($Sql)->result();
                     $Valor     =   0.00;

                     foreach ($Query as $key => $value):

                      if ($DataLimiteSemAcrescimo > $DataAtual){ // promocional
                          $Valor =  $value->srp_valor_promocional;
                      }else{
                          $Valor =  $value->srp_valor;
                      }


                         // Comparar datas
                         $DatasetInscricao       =  array(
                             'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
                             'fk_frf_id'                         =>    $FaturaSimplesId,
                             'fk_srp_id'                         =>    $value->srp_id,
                             'fk_sta_id'                         =>    2,
                             'fri_valor'                         =>    $Valor,
                             'flag_serie'                        =>    NULL,
                             'criado'                            =>    date("Y-m-d H:i:s")
                         );
                         $Query     =   $this->db->insert('tb_evento_faturaglobal_rel_faturasimples_rel_inscricao', $DatasetInscricao);

                     endforeach;



                  }

               endif;


               $Array   =   array(
                              'FaturaGlobalControle'    =>   $FaturaGlobalControle,
                              'FaturaSimplesControle'   =>   $FaturaSimplesControle,
                              'FaturaGlobalId'          =>   $FaturaGlobalId,
                              'FaturaSimplesId'         =>   $FaturaSimplesId
                            );

               return $Array;

           }



           /**
           * GravarHistorico
           *
           * @author Gustavo Botega
           */
           public function GravarHistorico($RegistroId, $Dataset = FALSE, $StatusId) {

              /*
               // Dados
               $Arr       =  array(
                   'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
                   'fk_sta_id'                         =>    $StatusId, // Status
                   'fk_reg_id'                         =>    $RegistroId, // ID do Registro
       #            'reh_historico'                     =>    (isset($Dataset['historico'])) ? $Dataset['historico'] : NULL, //
                   'criado'                            =>    date("Y-m-d H:i:s")
               );
               $Query = $this->db->insert('tb_registro_historico', $Arr);

            //    Verificando operacao
               ($Query) ?  $DatasetResultado['Status']     =   true : $DatasetResultado['Status']  =   false;


               return $DatasetResultado;
               */
           }



           /**
           * GravarTransacao
           *
           * @author Gustavo Botega
           */
           public function GravarTransacao($DatasetValores, $DatasetComplementar) {

               // Inicializando variaveis. Prevent errors.
               $Dataset    =   array();


               // Transacao
               $Dataset['Transacao']              =   array(
                                                               'status-sigepe'                 =>      '100', // [ SIGEPE ] Transação - Aguardando Pagamento
                                                               'status-pagarme'                =>      '200', // [ Pagarme ] Aguardando Pagamento
                                                               'responsavel-financeiro'        =>      $DatasetComplementar['responsavel-financeiro'], // implementar futuramente pelo responsalve-financeiro id. criar vinculos etc
                                                               'tipo-pagamento'                =>      1, // boleto
                                                               'natureza-operacao'             =>      5, // Natureza de operacao de Inscricao no modulo financeiro.
                                                               'valor-bruto'                   =>      0.00,
                                                               'valor-liquido'                 =>      0.00
                                                           );

               // Item da Transacao
               $Dataset['Item']                   =   $this->GetItem($DatasetValores);


               // Desconto na Transacao
               $Dataset['Desconto']               =   $this->GetDesconto($DatasetValores);


               // Historico da Transacao
               $Dataset['Historico']              =   array(
                                                               'tipo'                 =>      '1', // [ SIGEPE ] Transação - Aguardando Pagamento
                                                               'historico'            =>      'Criação da fatura. Motivo: Novo Inscrição ( fatura global ). Metodo GravarTransacao. Class: registro/FrontOffice/Registro.'
                                                           );


               // Criando a transacao e tratamento de erros
               $Transacao                                  =   $this->transacao->Gravar( $Dataset );
               if(!$Transacao['Status'])
                   return false;


               // Nesse momento a transacao foi realizada com sucesso. Recuperando dados da transacao.
               return array(
                               'FinanceiroId'      =>  $Transacao['FinanceiroId'],
                               'Transacao'         =>  $Transacao['Transacao']
                           );
           }


           /**
           * GetItem
           *
           * @author Gustavo Botega
           */
           public function GetItem($DatasetValores) {

               // Inicializando variaveis. Prevent errors.
               $Item    =   array();

               // Inserindo a Taxa Principal como Item no array.
               $Item[0]    =   array(
                                   'item'              =>      'Inscricao',
                                   'descricao'         =>      'Desc da Inscrição. 2193;',
                                   'quantidade'        =>      1,
                                   'valor-unitario'    =>      0.00
                               );


               return $Item;

           }



           /**
           * GetDesconto
           *
           * @author Gustavo Botega
           */
           public function GetDesconto($DatasetValores) {
             $Desconto    =   array();
             /*
               // Inicializando variaveis. Prevent errors.

               // Inserindo um desconto como Item no array.
               if($DatasetValores->Desconto->Flag):

                   $Desconto[0]    =   array(
                                       'tipo'              =>      $DatasetValores->Desconto->Tipo,
                                       'item'              =>      $DatasetValores->Desconto->Titulo,
                                       'descricao'         =>      $DatasetValores->Desconto->Descricao,
                                       'valor'             =>      $DatasetValores->Desconto->Valor
                                   );
               endif;

               */
               return $Desconto;
           }





           /**
           * SetStatusAtivo
           *
           * @author Gustavo Botega
           */
           public function SetStatusAtivo($RegistroId = NULL) {

              /*
               $TipoCompetidorId     =   $this->model_crud->get_rowSpecific('tb_registro', 'reg_id', $RegistroId, 1, 'fk_rec_id');


               if($TipoCompetidorId == '1')
                   $StatusId     =   '301';

               if($TipoCompetidorId == '2')
                   $StatusId     =   '311';


               $Dataset        =   array( 'fk_sta_id'  =>  $StatusId );

               $this->db->where( 'reg_id', $RegistroId);
               $query = $this->db->update('tb_registro', $Dataset);


               // Gravando historico
               $this->GravarHistorico($RegistroId, '', $StatusId);
               */
           }



           /**
           * SetStatusCancelado
           *
           * @author Gustavo Botega
           */
           public function SetStatusCancelado($RegistroId = NULL) {

             /*
               $DatasetRegistro        =   array( 'fk_sta_id'  =>  303 ); // Registro - Cancelado

               $this->db->where( 'reg_id', $RegistroId);
               $query = $this->db->update('tb_registro', $DatasetRegistro);


               // Gravando historico
               $this->GravarHistorico($RegistroId, '', $StatusId);


               if(!$Query)
                   return false;


               // Carregando modulo transacao
               $this->load->module('financeiro/Transacao');


               // Gravando o historico da transacao no modulo transcao.
               $DatasetFinanceiro             =   array(
                                                   'Historico'     =>  array(
                                                                           'tipo'      =>  4, // Tipo 4. Cancelado pelo usuario.
                                                                           'historico' =>  'Janela de pagamento cartão de crédito (pagar.me) fechada pelo usuário. Contexto: Registro de competidor.'
                                                                       )
                                                   );

               // Obtendo o ID financeiro
               $FinanceiroId       =      $this->model_crud->get_rowSpecific('tb_registro', 'reg_id', $RegistroId, 1, 'fk_fin_id');


               // Cancelando a transacao
               $this->transacao->SetTransacaoCancelada( $FinanceiroId, NULL, $DatasetFinanceiro );


               return true;
               */
           }




           /**
           * SetStatusCanceladoJson
           *
           * @author Gustavo Botega
           */
           public function SetStatusCanceladoJson() {

             /*
               $RegistroId     =    $_POST['SigepeRegistroId'];

               $DatasetRegistro        =   array( 'fk_sta_id'  =>  303 ); // Registro - Cancelado
               $this->db->where( 'reg_id', $RegistroId);
               $Query = $this->db->update('tb_registro', $DatasetRegistro);


               if(!$Query)
                   return false;


               // Gravando historico
               $this->GravarHistorico($RegistroId, '', 303);


               // Carregando modulo transacao
               $this->load->module('financeiro/FrontOffice/Transacao');


               // Gravando o historico da transacao no modulo transcao.
               $DatasetFinanceiro             =   array(
                                                   'Historico'     =>  array(
                                                                           'tipo'      =>  4, // Tipo 4. Cancelado pelo usuario.
                                                                           'historico' =>  'Janela de pagamento cartão de crédito (pagar.me) fechada pelo usuário. Contexto: Registro de competidor.'
                                                                       )
                                                   );

               // Obtendo o ID financeiro
               $FinanceiroId       =      $this->model_crud->get_rowSpecific('tb_registro', 'reg_id', $RegistroId, 1, 'fk_fin_id');


               // Cancelando a transacao
               $this->transacao->SetTransacaoCancelada( $FinanceiroId, NULL, $DatasetFinanceiro );


               return true;
              */
           }


















     /**
     * DatasetPagarme ( levar esse bloco pra camada do pagarme )
     *
     * @author Gustavo Botega
     */
     public function DatasetPagarme(){

         $PessoaId                               =       $this->data['PessoaId'];

         $this->data['PagarmeNome']              =       $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $PessoaId, 1, 'pes_nome_razao_social');
         $this->data['PagarmeCpf']               =       $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $PessoaId, 1, 'pes_cpf_cnpj');
         $this->data['PagarmeDataNascimento']    =       $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $PessoaId, 1, 'pes_data_nascimento_fundacao');

         // Obtendo o Email Principal
         $this->data['PagarmeEmail']             =       '';
         $SqlPagarmeEmail         =   "
                                         SELECT * FROM tb_email as ema

                                         WHERE
                                             ema.fk_peo_id    = '".$PessoaId."' AND
                                             ema.flag_deletado IS NULL and
                                             ema.fk_sta_id = 1 AND
                                             ema.fk_aut2_id IS NULL

                                         LIMIT 1
                                         ";
         $QueryPagarmeEmail       =   $this->db->query($SqlPagarmeEmail)->result();
         if(!empty($QueryPagarmeEmail))
             $this->data['PagarmeEmail']             =       $QueryPagarmeEmail[0]->ema_email;



         // Obtendo o Telefone Principal
         $this->data['PagarmeTelefonePrincipal']             =       '';
         $SqlPagarmeTelefone         =   "
                                         SELECT * FROM tb_telefone as tel

                                         WHERE
                                             tel.fk_peo_id    = '".$PessoaId."' AND
                                             tel.flag_deletado IS NULL and
                                             tel.fk_sta_id = 1 AND
                                             tel.fk_aut2_id IS NULL

                                         LIMIT 1
                                         ";
         $QueryPagarmeTelefone       =   $this->db->query($SqlPagarmeTelefone)->result();
         if(!empty($QueryPagarmeTelefone)){
             $Ddd                                                =       (string)$QueryPagarmeTelefone[0]->tel_ddd;
             $Telefone                                           =       str_replace("-", "", $QueryPagarmeTelefone[0]->tel_telefone);
             $this->data['PagarmeTelefonePrincipal']             =       $Ddd . (string)$Telefone ;
         }



         // Obtendo o Endereco Principal
         $this->data['PagarmeEndereco']          =       '';
         $SqlPagarmeEndereco         =   "
                                         SELECT * FROM tb_endereco as end

                                         WHERE
                                             end.fk_peo_id    = '".$PessoaId."' AND
                                             end.flag_deletado IS NULL and
                                             end.fk_sta_id = 1 AND
                                             end.fk_aut2_id IS NULL

                                         LIMIT 1
                                         ";
         $QueryPagarmeEndereco       =   $this->db->query($SqlPagarmeEndereco)->result();
         if(!empty($QueryPagarmeEndereco)){
             $this->data['PagarmeCep']                   =       $QueryPagarmeEndereco[0]->end_cep;
             $this->data['PagarmeEstadoSigla']           =       $this->model_crud->get_rowSpecific('tb_estado', 'est_id', $QueryPagarmeEndereco[0]->fk_est_id, 1, 'est_sigla');
             $this->data['PagarmeCidade']                =       $this->model_crud->get_rowSpecific('tb_cidade', 'cid_id', $QueryPagarmeEndereco[0]->fk_cid_id, 1, 'cid_nome');
             $this->data['PagarmeBairro']                =       $QueryPagarmeEndereco[0]->end_bairro;
             $this->data['PagarmeLogradouro']            =       $QueryPagarmeEndereco[0]->end_logradouro;
             $this->data['PagarmeNumero']                =       $QueryPagarmeEndereco[0]->end_numero;
             $this->data['PagarmeComplemento']           =       $QueryPagarmeEndereco[0]->end_complemento;
         }






     }























    /**
    * PackagesClass
    *
    * @author Gustavo Botega
    */
    public function ThemeComponent(){


        /*
            StylesFile
        */
            /*  Plugins*/
            $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
            $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete']                           = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                    = TRUE;
            $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;

            /*  Syles*/
    //            $this->data['StylesFile']['Styles']['profile']                                = TRUE;



        /*
            ScriptsFile
        */
            /*  Plugins*/
            $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
            $this->data['ScriptsFile']['Plugins']['easy-autocomplete']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

            /*  Scripts*/
            $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                            = TRUE;

    }





    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega
    * @return array
    */
    public function SigepeAsset()
    {
        /* Carregando Estilos */
        $this->data['PackageStyles'][]    =   'Packages/Styles/Inscricao/FrontOffice/Formulario';


        /* Carregando Scripts */
        $this->data['PackageScripts'][]    =   'Packages/Scripts/Inscricao/FrontOffice/Formulario';
        $this->data['PackageScripts'][]    =   'Packages/Scripts/Inscricao/FrontOffice/Inscricao';
        $this->data['PackageScripts'][]    =   'Packages/Scripts/Inscricao/FrontOffice/Gateway';



//        $this->data['PackageScripts'][]   =   'packages/frontoffice/script/animal';


    }






}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
