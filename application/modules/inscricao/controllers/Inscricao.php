<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';
class Inscricao extends MY_FrontOffice {

    function __construct() {
        parent::__construct();
		
		$this->ThemeComponent();
		$this->SigepeAsset();
    }

    public function index() {
		$this->gear();    	
    }

    public function dashboard(){

        /*PEGAR A HORA LOCAL DA PESSOA. QUEM ACESSAR POR NY VAI TER UM HORARIO DIFERENTE DE QUEM ACESSAR POR BR */                    
        $Settings['PageHeadTitle']      = 'Bom dia Gustavo, tudo bem? '; /* */
        $Settings['PageHeadSubtitle']   = 'Seu último acesso foi às 18h40 em 16/04/2017.';


        $this->loadTemplateUser('template/user/tiles', $Settings);
    }




    public function EventoInscricaoAberta(){

        $Settings['PageHeadTitle']      = 'Bom dia Gustavo, tudo bem? '; 
        $Settings['PageHeadSubtitle']   = 'Seu último acesso foi às 18h40 em 16/04/2017.';

        $this->LoadTemplateInscricao(
                'Template/FrontOffice/sistema/Inscricao/Evento/InscricaoAberta',
                $this->data
        );
    }


    public function GerarBoleto(){


        echo "teste";

        require 'pagarme-php/Pagarme.php';

        Pagarme::setApiKey('ak_test_pW6VqWmqmOi3XASgJ2g4kKr9cu0DYE');

        $transaction = new PagarMe_Transaction(array(
            'amount' => 1000,
            'payment_method' => 'boleto',
            'postback_url' => 'http://requestb.in/pkt7pgpk',
            'customer' => array(
                'type' => 'individual',
                'country' => 'br',
                'name' => 'Gustavo Botega',
                'documents' => [
                    [
                        'type' => 'cpf',
                        'number' => '04730815122'
                    ]
                ]
            )
        ));

        $transaction->charge();




        echo "hue";


        
    }


    


    public function Formulario( $EventoId = NULL ){

        $Settings['PageHeadTitle']      = 'Bom dia Gustavo, tudo bem? '; 
        $Settings['PageHeadSubtitle']   = 'Seu último acesso foi às 18h40 em 16/04/2017.';

        $this->LoadTemplateInscricao(
                                        array(
                                            'Template/FrontOffice/sistema/Inscricao/Conjunto',
                                            'Template/FrontOffice/sistema/Inscricao/SerieProva',
                                            'Template/FrontOffice/sistema/Inscricao/Opcionais',
                                            'Template/FrontOffice/sistema/Inscricao/Resumo'
                                        ), $this->data
                                    );
    }


	
	
	
	
	
	
	

    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    */
    public function ThemeComponent(){ 


        /* 
            StylesFile 
        */
            /*  Plugins*/
            $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
            $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete']                           = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                    = TRUE;
            $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;
        
            /*  Syles*/
    //            $this->data['StylesFile']['Styles']['profile']                                = TRUE;



        /* 
            ScriptsFile 
        */
            /*  Plugins*/
            $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
            $this->data['ScriptsFile']['Plugins']['easy-autocomplete']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

            /*  Scripts*/
            $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                            = TRUE;

    }

	
	
    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function SigepeAsset()
    { 
        /* Carregando Estilos */
        $this->data['PackageStyles'][]    =   'packages/frontoffice/style/cadastro-animal';


        /* Carregando Scripts */
        $this->data['PackageScripts'][]   =   'packages/frontoffice/script/animal';
        $this->data['PackageScripts'][]   =   'packages/frontoffice/script/cadastro-animal';
        $this->data['PackageScripts'][]   =   'packages/frontoffice/script/form-wizard-cadastro-animal';


    }

	
	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

