<?php
    $autoload = array(
        'helper'    => array('url', 'form'),
        'libraries' => array( 'MY_Data', 'MY_Moeda', 'MY_Pessoa', 'MY_Pessoa_Telefone', 'MY_Pessoa_Email', 'MY_Pessoa_Fisica'),
        'model' => array( 'model_crud'),
        'core'	=> array('my_controller')
    );
	
?>