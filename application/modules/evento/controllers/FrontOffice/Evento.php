<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';
class Evento extends MY_FrontOffice {

    function __construct() {
        parent::__construct();
		
		$this->ThemeComponent();
		$this->SigepeAsset();

        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');

    }

    public function Dashboard($EventoId){

        $this->data['NavActiveSidebar'] =   'Dashboard';
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Evento';
        $this->data['PageHeadSubtitle']   = 'Visualize as principais informações do evento.';
        $this->data['Breadcrumbs']        = array();


        $this->data['PageHeadTitle']      = 'Eventos '; 
        $this->data['PageHeadSubtitle']   = 'Confira a relação de eventos que estão com as inscrições em aberto.';


        $this->data['EventoId']      =  $EventoId; 


        /*  EVENTO */
        $SqlEvento                     = '
                                            SELECT
                                                eve_id, fk_sta_id, fk_pes_id, eve_nome, eve_controle, eve_site_logotipo, eve_data_inicio, eve_site_inscricao_fim,  UNIX_TIMESTAMP(eve_site_inscricao_fim) AS DATE
                                            FROM
                                                tb_evento
                                            WHERE
                                                eve_id = '.$EventoId.'

                                            LIMIT 1
                                            ';
        $this->data['DatasetEvento']   = $this->db->query($SqlEvento)->result();        

        $this->data['NomeEvento']       = $this->data['DatasetEvento'][0]->eve_nome;        
        $this->data['Logotipo']         = $this->data['DatasetEvento'][0]->eve_site_logotipo;        




        /*  EVENTO */
        $SqlInscricoesQueRealizei       = '
                                            SELECT *
                                            FROM
                                                tb_evento_faturaglobal
                                            WHERE
                                                fk_eve_id = '.$EventoId.' AND
                                                fk_aut_id = '.$this->session->userdata('PessoaId').' 

                                            LIMIT 1
                                            ';
        $this->data['DatasetInscricoesQueRealizei']   = $this->db->query($SqlInscricoesQueRealizei)->result();        
        $this->data['EventoId']   = $EventoId;        


        $this->LoadTemplateProfileEvento(
                'Template/FrontOffice/sistema/Evento/Dashboard',
                $this->data
        );
    }

	

    public function GetTipoDeVendaDaInscricao($EventoId){
        return $this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $EventoId, 1, 'fk_evv_id');
    }
	
	
	

    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    */
    public function ThemeComponent(){ 



        /* 
            StylesFile 
        */
            /*  Plugins*/
            $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
            $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete']                           = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                    = TRUE;
            $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;
        
            /*  Syles*/
               $this->data['StylesFile']['Styles']['profile']                                   = TRUE;



        /* 
            ScriptsFile 
        */
            /*  Plugins*/
            $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
            $this->data['ScriptsFile']['Plugins']['easy-autocomplete']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

            /*  Scripts*/
            $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                            = TRUE;


    }

	
	
    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function SigepeAsset()
    { 
        /* Carregando Estilos */
//        $this->data['PackageStyles'][]    =   'packages/frontoffice/style/cadastro-animal';


        /* Carregando Scripts */
//        $this->data['PackageScripts'][]   =   'packages/frontoffice/script/animal';


    }

	
	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

