<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Caracteristica extends MY_Controller {

    public $tabelaNome = "tb_evento_serie_prova_caracteristica";
    public $columnId = "spc_id";
    public $columnDesc = "spc_caracteristica";

    function __construct() {
        parent::__construct();
        $this->load->model('model_crud');
    }

    public function index(){
        var_dump($this->listar());
        var_dump($this->obter(1));
    }

    public function listar(){
        return $this->db->query("SELECT * from $this->tabelaNome order by $this->columnDesc")->result();
    }

    public function obter($id){
        return $this->model_crud->get_rowSpecificObject($this->tabelaNome, $this->columnId, $id);
    }

}