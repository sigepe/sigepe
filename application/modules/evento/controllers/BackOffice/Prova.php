<?php

if (!defined('BASEPATH')) { exit('No direct script access allowed'); }
    
class Prova extends MY_BackOffice {

    public $data;

    function __construct() {
        parent::__construct();
        $this->MetronicAsset();
        $this->SigepeAsset();
        $this->data['ShowProfileEvento']   =  TRUE;
        $this->data['base_dir'] = 'Template/BackOffice/sistema/Evento/';
        $this->load->module('evento/BackOffice/Evento');
        $this->data['Class']    =   $this->router->fetch_class();
        /* Controladores */
        $this->load->module('status/CommonOffice/Status');
        $this->load->module('evento/BackOffice/TipoPista');
        $this->load->module('evento/BackOffice/TipoSorteio');
        $this->load->module('evento/BackOffice/Caracteristica');
    }



    public function index() {
		$this->Listagem();
    }



    /**
    * prova_browse_bread.
    *
    *
    * @author Nillander
    *
    * @return view
    */
    public function prova_browse_bread($eventoId) {
        $evento = $this->evento->obterEvento($eventoId);
        $this->data['EventoId'] = $evento->eve_id;
        $this->data['evento'] = array($evento);
        
        $queryProvasDoEvento = "
                                    SELECT
                                        srp.*,
                                        ers.ers_id, 
                                        eve.eve_id 
                                    FROM
                                        tb_evento_rel_serie_rel_prova as srp
                                    INNER JOIN tb_evento_rel_serie as ers ON ers.ers_id = srp.fk_ers_id
                                    INNER JOIN tb_evento as eve ON eve.eve_id = ers.fk_eve_id
                                    WHERE fk_eve_id = $evento->eve_id ORDER BY srp.srp_numero_prova ASC
                                ";
        
        $DatasetProva = $this->db->query($queryProvasDoEvento)->result();
        $this->data['DatasetProva'] = $DatasetProva;

        // Obter os Desenhadores de Percurso do Evento
        $SqlDesenhadorPercurso = "SELECT * FROM tb_evento_rel_desenhador as erd
            INNER JOIN tb_pessoa as pes ON pes.pes_id = erd.fk_pes_id
            INNER JOIN tb_vinculo as vin ON pes.pes_id = vin.fk_pes1_id
            WHERE
                vin.fk_per_id = 6 AND
                vin.fk_sta_id = 1 AND
                vin.fk_tip_id IS NULL AND
                pes.fk_sta_id = 1 AND
                erd.fk_eve_id = $evento->eve_id
            ORDER BY pes.pes_nome_razao_social ASC";

        $this->data['DatasetDesenhadorPercurso'] = $this->db->query($SqlDesenhadorPercurso)->result();

        /* Nill Shining */
        $this->data['arrStatus'] = $this->status->obterEntre(410, 413);
        $this->data['arrTipoDePista'] = $this->tipopista->listar();
        $this->data['arrTipoDeSorteio'] = $this->tiposorteio->listar();
        $this->data['arrCaracteristica'] = $this->caracteristica->listar();

        $this->data['QuantidadeDiasEvento'] = DateDifferences($evento->eve_data_fim, $evento->eve_data_inicio, 'd') + 1;
        $this->data['EventoInicio'] = $evento->eve_data_inicio;
        $this->data['EventoFim'] = $evento->eve_data_fim;

        /* View */ $this->LoadTemplate($this->data['base_dir'].__CLASS__."/".__FUNCTION__, $this->data);
    } // fim do método @prova_browse_bread


    
    /**
    * prova_read
    *
    * @author
    *
    * @return 
    */
    public function prova_read() {

    } // fim do método @prova_read



    /**
    * prova_add
    *
    * @author
    *
    * @return 
    */
    public function prova_add($eventoId) {
        $evento = $this->evento->obterEvento($eventoId);
        $this->data['evento'] = array($evento);
        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Cadastro';
        $this->data['PageHeadSubtitle']   = 'Para cadastrar uma prova no SIGEPE preencha o formulário abaixo.';
        $this->data['Breadcrumbs']        = array();
        $this->data['QuantidadeDeProva']  = $this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $evento->eve_id, 1, 'eve_quantidade_prova');
        $this->data['DatasetSerie']       = $this->GetSerieDoEvento($evento->eve_id);


        /*  TIPO DE PISTA */
        $SqlTipoPista = "SELECT * FROM tb_evento_serie_prova_pista as spp  ORDER BY spp.spp_id asc";

        $this->data['DatasetTipoPista']   = $this->db->query($SqlTipoPista)->result();

        /*  PROVAS CADASTRADAS */
        $SqlProvasCadastradas = "
            SELECT srp_numero_prova FROM tb_evento_rel_serie_rel_prova as srp
            INNER JOIN tb_evento_rel_serie as ers ON ers.ers_id = srp.fk_ers_id
            INNER JOIN tb_evento as eve ON eve.eve_id = ers.fk_eve_id
            WHERE eve.eve_id = $evento->eve_id
            ORDER BY srp.srp_numero_prova asc";

        $ProvasCadastradas = $this->db->query($SqlProvasCadastradas)->result();
        $ArrayProvasCadastradas = array();
        foreach ($ProvasCadastradas as $key => $value) {
            $ArrayProvasCadastradas[]  =   $value->srp_numero_prova;
         }

        $this->data['DatasetProvasCadastradas'] = $ArrayProvasCadastradas;

        /*  CARACTERISTICA DA PROVA */
        $SqlCaracteristica = "SELECT * FROM tb_evento_serie_prova_caracteristica as spc ORDER BY spc.spc_caracteristica ASC, spc.spc_regulamento";
        $this->data['DatasetCaracteristica'] = $this->db->query($SqlCaracteristica)->result();

        /*  TIPO DE SORTEIO */
        $SqlSorteio = "SELECT * FROM tb_evento_serie_prova_sorteio as sps ORDER BY sps_id ASC";
        $this->data['DatasetTipoSorteio']   = $this->db->query($SqlSorteio)->result();

        $this->data['QuantidadeDiasEvento'] = DateDifferences($evento->eve_data_fim, $evento->eve_data_inicio, 'd') + 1;
        $this->data['EventoInicio'] = $evento->eve_data_inicio;
        $this->data['EventoFim'] = $evento->eve_data_fim;
        $this->data['EventoId'] = $evento->eve_id;

        /* View */ $this->LoadTemplate($this->data['base_dir'].__CLASS__."/".__FUNCTION__, $this->data);

    } // fim do méotodo @prova_add



    /* # Fim Bread
       # Inicio Middleware
    ====================================================================================================================*/



    /**
    * processar.
    *
    * -
    *
    * @author Gustavo Botega
    *
    * @param
    *
    * @return boolean
    */
    public function processar($dataJson = false) {
        if(isset($_POST['dataJson']) && is_array($_POST['dataJson'])) {
            $dados = $_POST['dataJson'];
        }

        if($dataJson) {
            $dados = $dataJson;
        }

        if(!isset($_POST['dataJson']) && !$dataJson) {
            echo json_encode(false);
        }

        // Validacao Formulario
        $validar = $this->validar($dados);
        if (!$validar) {
            echo json_encode(false);
            return false;
        }

        // Se existir algum ID por convencao entende-se que o metodo a ser direcionado e de atualizacao.
        if(!isset($dados['id'])) {
            $resultadoAcao = $this->gravar($dados);
            echo ($resultadoAcao) ? json_encode(true) : json_encode(false);
        } else {
            $resultadoAlterar = $this->alterar($dados);
            echo ($resultadoAlterar) ? json_encode(true) : json_encode(false);
        }

    } // fim do metodo @processar



    /**
    * Gravar
    *
    * @author Gustavo Botega
    */
    public function gravar($Dados) {

        foreach ($Dados as $key => $value) {
            if(empty($value))
                $Dados[$key]    =   NULL;
        }

        // Prova Dependente
        ($Dados['prova-dependente'] == 2) ? $Dados['prova-dependente'] = NULL : '';


        
        if(!isset($Dados['velocidade'])){
            $Dados['velocidade']    =   null;
        }
        if(!isset($Dados['obstaculo-altura'])){
            $Dados['obstaculo-altura']    =   null;
        }
        if(!isset($Dados['obstaculo-largura'])){
            $Dados['obstaculo-largura']    =   null;
        }


        // Dados
        $Dataset       =  array(
            'fk_aut_id'                                 =>    $this->session->userdata('PessoaId'),
            'fk_sta_id'                                 =>    410, // [ Prova ] a realizar. ID Status. Referencia: tb_status
            'fk_ers_id'                                 =>    $Dados['serie'], // ID da Serie do evento que a prova esta vinculada. Referencia: tb_evento_rel_serie
            'fk_spp_id'                                 =>    $Dados['tipo-pista'], // Pista. Referencia: tb_evento_serie_prova_pista
            'fk_sps_id'                                 =>    $Dados['tipo-sorteio'], // Sorteio - Referencia: tb_evento_serie_prova_sorteio
            'fk_spc_id'                                 =>    $Dados['caracteristica'], // Caracteristica da Prova. Referencia: tb_evento_serie_prova_caracteristica

            'srp_nome'                                  =>    $Dados['nome-prova'], //
            'srp_nome_trofeu'                           =>    $Dados['nome-trofeu'], //
            'srp_numero_prova'                          =>    $Dados['numero-prova'], // Numero Prova
            'srp_dia'                                   =>    $Dados['dia'], //
            'srp_hora'                                  =>    $Dados['hora'], //
            /* Valores e Limites */
            'srp_valor'                                 =>    $this->my_moeda->RemoverPontuacao($Dados['valor-prova']),
            'srp_valor_promocional'                     =>    $this->my_moeda->RemoverPontuacao($Dados['valor-prova-promocional']),
            'srp_limite_inscricao_prova'                =>    $Dados['limite-inscricao-prova'],
            'srp_limite_inscricao_atleta'               =>    $Dados['limite-inscricao-atleta'],
            /* Salto */
            'srp_velocidade'                            =>    $Dados['velocidade'],
            'srp_obstaculo_altura'                      =>    $Dados['obstaculo-altura'],
            'srp_obstaculo_largura'                     =>    $Dados['obstaculo-largura'],
            /* Salto */
            'flag_prova_dependente'                     =>    $Dados['prova-dependente'],
            'criado'                                    =>    date("Y-m-d H:i:s"),

            // TOFIX
            'pro_ordem'                                 =>    1,
            'pro_resultado_link'                        =>    1,
            'pro_resultado_equipe_status'               =>    1,
            'pro_resultado_equipe_pdf'                  =>    1,
            'pro_resultado_campeonato_status'           =>    1,
            'pro_resultado_campeonato_pdf'              =>    1,
            'pro_resultado_pdf_status'                  =>    1,
            'pro_resultado_pdf'                         =>    1,
            'pro_resultado_link_status'                 =>    1,
            'pro_ordem_status'                          =>    1
        );


        /* Query */
        $Query = $this->db->insert('tb_evento_rel_serie_rel_prova', $Dataset);

        if ($Query){
            $provaId = $this->db->insert_id();
            $gravarCategoria = $this->GravarCategoriasDaSerie($provaId, $Dados['serie']);
            return true;
        } else {
            return false;
        }
    } // fim do metodo @gravar



    /**
    * Alterar
    *
    * @author Gustavo Botega
    */
    public function alterar($dados) {

        foreach ($dados as $key => $value) {
            if(empty($value))
                $dados[$key]    =   NULL;
        }

        // Prova Dependente
        //($dados['prova-dependente'] == 2) ? $dados['prova-dependente'] = NULL : '';

        (!isset($dados['velocidade'])) ?  $dados['velocidade'] = null : '';
        (!isset($dados['obstaculo-altura'])) ?  $dados['obstaculo-altura'] = null : '';
        (!isset($dados['obstaculo-largura'])) ?  $dados['obstaculo-largura'] = null : '';

        // Dados
        $dataset       =  array(
            'fk_sta_id'                                 =>    $dados['fk-status'],
            'fk_spp_id'                                 =>    $dados['tipo-pista'], // Pista. Referencia: tb_evento_serie_prova_pista
            'fk_sps_id'                                 =>    $dados['tipo-sorteio'], // Sorteio - Referencia: tb_evento_serie_prova_sorteio
            'fk_spc_id'                                 =>    $dados['caracteristica'], // Caracteristica da Prova. Referencia: tb_evento_serie_prova_caracteristica

            'srp_nome'                                  =>    $dados['nome-prova'], //
            'srp_nome_trofeu'                           =>    $dados['nome-trofeu'], //
            'srp_dia'                                   =>    $dados['dia'], //
            'srp_hora'                                  =>    $dados['hora'], //
            /* Valores e Limites */
            'srp_valor'                                 =>    $this->my_moeda->RemoverPontuacao($dados['valor-prova']),
            'srp_valor_promocional'                     =>    $this->my_moeda->RemoverPontuacao($dados['valor-prova-promocional']),
            'srp_limite_inscricao_prova'                =>    $dados['limite-inscricao-prova'],
            'srp_limite_inscricao_atleta'               =>    $dados['limite-inscricao-atleta'],
            /* Salto */
            'srp_velocidade'                            =>    $dados['velocidade'],
            'srp_obstaculo_altura'                      =>    $dados['obstaculo-altura'],
            'srp_obstaculo_largura'                     =>    $dados['obstaculo-largura'],
            /* Salto */
            // implementar 'flag_prova_dependente'                     =>    $dados['prova-dependente'],
            'modificado'                                    =>    date("Y-m-d H:i:s"),

            // TOFIX
            'pro_ordem'                                 =>    1,
            'pro_resultado_link'                        =>    1,
            'pro_resultado_equipe_status'               =>    1,
            'pro_resultado_equipe_pdf'                  =>    1,
            'pro_resultado_campeonato_status'           =>    1,
            'pro_resultado_campeonato_pdf'              =>    1,
            'pro_resultado_pdf_status'                  =>    1,
            'pro_resultado_pdf'                         =>    1,
            'pro_resultado_link_status'                 =>    1,
            'pro_ordem_status'                          =>    1
        );

        /* Query */
        $query = $this->db->update('tb_evento_rel_serie_rel_prova', $dataset, array('srp_id' => $dados['id'] ));
        return $query;

    } // fim do método @alterar



    /**
    * validar
    *
    * @author Gustavo Botega
    */
    public function validar($dados) {
        return true; // Validar inputs e regras de negocio do formulario. Tratar erro.
    } // fim do método @validar



    /* # Fim Middleware
       # Inicio Acessório
    ====================================================================================================================*/



    /**
    * GetCategoriasDaProva
    *
    * @author Gustavo Botega
    */
    public function GetSerieDoEvento($eventoId) {

        $SqlSerieDoEvento = "SELECT * FROM tb_evento_rel_serie as ers WHERE fk_eve_id  = $eventoId ORDER BY fk_evs_id desc";
        return $this->db->query($SqlSerieDoEvento)->result();

    } // fim do método @getSerieDoEvento



    /**
    * GetCategoriasDaProva
    *
    * @author Gustavo Botega
    */
    public function GetCategoriasDaProva($provaId) {

        $SqlCategoriaProva = "
            SELECT * FROM tb_evento_rel_serie_rel_prova_rel_categoria as prc
            INNER JOIN tb_evento_rel_serie_rel_prova as srp ON srp.srp_id = prc.fk_srp_id
            INNER JOIN tb_evento_categoria as evc ON evc.evc_id = prc.fk_evc_id
            WHERE srp.srp_id = $provaId        
            ORDER BY evc.evc_id asc";
        return $this->db->query($SqlCategoriaProva)->result();

    } // fim do método @getCategoriasDaProva



    /**
    * GetDesenhadorDePercursoDaProva
    *
    * @author Gustavo Botega
    */
    public function GetDesenhadorDePercursoDaProva($provaId) {

        $SqlDesenhadorDePercursoDaProva = "
            SELECT * FROM tb_evento_rel_serie_rel_prova_rel_desenhador as prd
            INNER JOIN tb_evento_rel_serie_rel_prova as srp ON srp.srp_id = prd.fk_srp_id
            INNER JOIN tb_pessoa as pes ON pes.pes_id = prd.fk_pes_id
            INNER JOIN tb_vinculo as vin ON pes.pes_id = vin.fk_pes1_id
            WHERE
                vin.fk_per_id = 6 AND
                vin.fk_sta_id = 1 AND
                vin.fk_tip_id IS NULL AND
                pes.fk_sta_id = 1 AND
                prd.fk_srp_id = $provaId
            ORDER BY prd.flag_assistente asc";
        return $this->db->query($SqlDesenhadorDePercursoDaProva)->result();

    } // fim do método @GetDesenhadorDePercursoDaProva



    /**
    * GetDesenhadorDePercursoExcetoProva
    *
    * Lista todos os desenhadores do evento com a excecao dos que ja estao cadastrados na prova
    *
    *
    * @author Gustavo Botega
    */
    public function GetDesenhadorDePercursoExcetoProva($provaId, $eventoId) {

        $SqlDesenhadorDePercursoDaProva = "
            SELECT * FROM tb_evento_rel_serie_rel_prova_rel_desenhador as prd
            INNER JOIN tb_evento_rel_serie_rel_prova as srp ON srp.srp_id = prd.fk_srp_id
            INNER JOIN tb_pessoa as pes ON pes.pes_id = prd.fk_pes_id
            INNER JOIN tb_vinculo as vin ON pes.pes_id = vin.fk_pes1_id
            WHERE
                vin.fk_per_id = 6 AND
                vin.fk_sta_id = 1 AND
                vin.fk_tip_id IS NULL AND
                pes.fk_sta_id = 1 AND
                prd.fk_srp_id = '.$provaId.'
            ORDER BY prd.flag_assistente asc";

        $DatasetDesenhadoresDePercursoDaProva       =    $this->db->query($SqlDesenhadorDePercursoDaProva)->result();
        $ArrayDesenhadoresDePercursoDaProva         =   array();

        foreach ($DatasetDesenhadoresDePercursoDaProva as $key => $value) {
            $ArrayDesenhadoresDePercursoDaProva[]      =    $value->pes_id;
        }

        $SqlDesenhadorPercursoDoEvento      =  "
            SELECT * FROM tb_evento_rel_desenhador as erd
            INNER JOIN tb_pessoa as pes ON pes.pes_id = erd.fk_pes_id
            INNER JOIN tb_vinculo as vin ON pes.pes_id = vin.fk_pes1_id
            WHERE
                vin.fk_per_id = 6 AND
                vin.fk_sta_id = 1 AND
                vin.fk_tip_id IS NULL AND
                pes.fk_sta_id = 1 AND
                erd.fk_eve_id = $eventoId
            ORDER BY pes.pes_nome_razao_social ASC";

        $DatasetDesenhadorPercursoDoEvento   = $this->db->query($SqlDesenhadorPercursoDoEvento)->result();
        $ArrayDesenhadoresDePercursoDoEvento = array();

        foreach ($DatasetDesenhadorPercursoDoEvento as $key => $value) {
            $ArrayDesenhadoresDePercursoDoEvento[]  =   $value->pes_id;
        }

        return array_diff($ArrayDesenhadoresDePercursoDoEvento, $ArrayDesenhadoresDePercursoDaProva);

    } // fim do método @getDesenhadorDePercursoExcetoProva



    public function GravarCategoriasDaSerie($provaId, $serieId) {

        $categoriasDaProva  =   $this->GetCategoriasDaSerie($serieId);
        foreach ($categoriasDaProva as $key => $value) {
            // Dados
            $Dataset       =  array(
                'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
                'fk_srp_id'                         =>    $provaId,
                'fk_evc_id'                         =>    $value->fk_evc_id,
                'flag_bloquear_delacao'             =>    1,
                'criado'                            =>    date("Y-m-d H:i:s")
            );
            $Query = $this->db->insert('tb_evento_rel_serie_rel_prova_rel_categoria', $Dataset);
        }

    } // fim do método @gravarCategoriasDaCategoria



    /**
    * GetCategoriasDaSerie
    *
    * @author Gustavo Botega
    */
    public function GetCategoriasDaSerie($serieId) {

        $SqlCategoriasDaSerie = "
            SELECT * FROM tb_evento_rel_serie_rel_categoria as src
            WHERE fk_ers_id  = $serieId
            ORDER BY fk_evc_id desc";

       return $this->db->query($SqlCategoriasDaSerie)->result();

    }



    /* # Fim Metodo Acessório
       # Inicio Método Controladores
    =========================================================================*/

    public function obterProva($serieId) {
        return $this->model_crud->get_rowSpecificObject('tb_evento_rel_serie', 'ers_id', $serieId);
    } // fim do método @obterSerie


    /* # Fim Metodo Controladores
       # Inicio Asset 
    =========================================================================*/



    /**
    * PackagesClass
    *
    * @author Gustavo Botega
    */
    public function MetronicAsset() {
        /* STYLES
        -----------------------------------*/

        // PLUGINS  //

        /* {NamePackage} */
        $this->data['StylesFile']['Plugins']['bootstrap-select']                            = TRUE;
        $this->data['StylesFile']['Plugins']['multi-select']                                = TRUE;
        $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
        $this->data['StylesFile']['Plugins']['select2-bootstrap']                           = TRUE;
        $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;
        $this->data['StylesFile']['Plugins']['easy-autocomplete']                           = TRUE;
        $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                    = TRUE;

        /* Date & Time Pickers */
        $this->data['StylesFile']['Plugins']['daterangepicker']                             = TRUE;
        $this->data['StylesFile']['Plugins']['bootstrap-datepicker3']                       = TRUE;
        $this->data['StylesFile']['Plugins']['bootstrap-timepicker']                        = TRUE;
        $this->data['StylesFile']['Plugins']['bootstrap-datetimepicker']                    = TRUE;
        $this->data['StylesFile']['Plugins']['clockface']                                   = TRUE;

        /* Bootstrap File Input */
        $this->data['StylesFile']['Plugins']['bootstrap-fileinput']                         = TRUE;

        /* Markdown & WYSIWYG Editors */
        $this->data['StylesFile']['Plugins']['bootstrap-wysihtml5']                         = TRUE;
        $this->data['StylesFile']['Plugins']['bootstrap-markdown']                          = TRUE;
        $this->data['StylesFile']['Plugins']['bootstrap-summernote']                        = TRUE;


        // STYLES  //
        /* {NamePackage} */
        $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;
        $this->data['StylesFile']['Styles']['profile']                                      = TRUE;


        /* SCRIPTS
        -----------------------------------*/

        // PLUGINS  //

        /* {NamePackage} */
        $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
        $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
        $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
        $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
        $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
        $this->data['ScriptsFile']['Plugins']['easy-autocomplete']                        = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

        /* Date & Time Pickers */
        $this->data['ScriptsFile']['Plugins']['moment']                                   = TRUE;
        $this->data['ScriptsFile']['Plugins']['daterangepicker']                          = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-datepicker']                     = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-timepicker']                     = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-datetimepicker']                 = TRUE;
        $this->data['ScriptsFile']['Plugins']['clockface']                                = TRUE;

        /* Bootstrap File Input */
        $this->data['ScriptsFile']['Plugins']['bootstrap-fileinput']                      = TRUE;

        /* Markdown & WYSIWYG Editors */
        $this->data['ScriptsFile']['Plugins']['wysihtml5']                                = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-wysihtml5']                      = TRUE;
        $this->data['ScriptsFile']['Plugins']['markdown']                                 = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-markdown']                       = TRUE;
        $this->data['ScriptsFile']['Plugins']['summernote']                               = TRUE;

        /* Markdown & WYSIWYG Editors */
        $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
        $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;



        // SCRIPTS //

        /* Bootstrap Sweet Alert */
        $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                  = TRUE;

        /* Date & Time Pickers */
        $this->data['ScriptsFile']['Scripts']['components-date-time-pickers']   = TRUE;

        /* Markdown & WYSIWYG Editors */
        $this->data['ScriptsFile']['Scripts']['components-editors']             = TRUE;
        /* User Profile */

    } // fim do método MetronicAsset



    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega
    * @return array
    */
    public function SigepeAsset() {
    /* Carregando Estilos */
    /* Carregando Scripts */
#        $this->data['PackageScripts'][]   =   'Packages/Scripts/Evento/BackOffice/FormValidationProva';
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Evento/BackOffice/FormValidationProva';
    
    } // fim do método SigepeAsset



} /* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */