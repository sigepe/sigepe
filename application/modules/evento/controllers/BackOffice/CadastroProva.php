<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_BackOffice.php';
class CadastroProva extends MY_BackOffice {

    public $data;

    function __construct() {

        parent::__construct();




        $this->load->module('evento/BackOffice/Evento');
        $this->data     =   $this->evento->InformacoesTemplate();
        $this->data['ShowProfileEvento']   =  TRUE;

        $this->ThemeComponent();
        $this->SigepeAsset();


    }


    /**
    * Index
    *
    * @author Gustavo Botega
    */
    public function index() {
		$this->Formulario();
    }




    /**
    * Formulario
    *
    * @author Gustavo Botega
    */
    public function Formulario($EventoId){

        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Cadastro';
        $this->data['PageHeadSubtitle']   = 'Para cadastrar uma prova no SIGEPE preencha o formulário abaixo.';
        $this->data['Breadcrumbs']        = array();
        $this->data['QuantidadeDeProva']  = $this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $EventoId, 1, 'eve_quantidade_prova');


        $this->data['DatasetSerie']        = $this->GetSerieDoEvento($EventoId);


        /*  TIPO DE PISTA */
        $SqlTipoPista                     = '
                                                SELECT * FROM
                                                    tb_evento_serie_prova_pista as spp

                                                ORDER BY
                                                    spp.spp_id asc
                                            ';
        $this->data['DatasetTipoPista']   = $this->db->query($SqlTipoPista)->result();


        /*  PROVAS CADASTRADAS */
        $SqlProvasCadastradas             = '


                                                SELECT srp_numero_prova FROM
                                                tb_evento_rel_serie_rel_prova as srp

                                                INNER JOIN tb_evento_rel_serie as ers ON
                                                ers.ers_id = srp.fk_ers_id

                                                INNER JOIN tb_evento as eve ON
                                                eve.eve_id = ers.fk_eve_id

                                                WHERE
                                                    eve.eve_id = '.$EventoId.'

                                                ORDER BY
                                                    srp.srp_numero_prova asc

';
        $ProvasCadastradas                = $this->db->query($SqlProvasCadastradas)->result();
        $ArrayProvasCadastradas           = array();
        foreach ($ProvasCadastradas as $key => $value) {
             $ArrayProvasCadastradas[]  =   $value->srp_numero_prova;
         }
        $this->data['DatasetProvasCadastradas']     =   $ArrayProvasCadastradas;



        /*  CARACTERISTICA DA PROVA */
        $SqlCaracteristica                = '
                                                SELECT * FROM
                                                    tb_evento_serie_prova_caracteristica as spc

                                                ORDER BY
                                                    spc.spc_caracteristica ASC, spc.spc_regulamento
                                            ';
        $this->data['DatasetCaracteristica']   = $this->db->query($SqlCaracteristica)->result();




        /*  TIPO DE SORTEIO */
        $SqlSorteio                = '
                                                SELECT * FROM
                                                    tb_evento_serie_prova_sorteio as sps

                                                ORDER BY
                                                    sps_id ASC
                                            ';
        $this->data['DatasetTipoSorteio']   = $this->db->query($SqlSorteio)->result();

        $this->data['QuantidadeDiasEvento'] = $this->evento->GetQuantidadeDiasEvento($EventoId);
        $this->data['EventoInicio']         = $this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $EventoId, 1, 'eve_data_inicio');
        $this->data['EventoFim']            = $this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $EventoId, 1, 'eve_data_fim');


        $this->data['EventoId']                     = $EventoId;



        /* Carrega View */
#        $this->loadTemplateUser('Template/BackOffice/sistema/Evento/FormularioProva', $this->data);
        $this->LoadTemplate('Template/BackOffice/sistema/Evento/FormularioProva', $this->data);

    }



    /**
    * GetSerieDoEvento
    *
    *
    * @author Gustavo Botega
    * @return {type}
    */
    public function GetSerieDoEvento($EventoId){


        $SqlSerieDoEvento                     = '
                                                SELECT * FROM
                                                    tb_evento_rel_serie as ers

                                                WHERE
                                                    fk_eve_id  = '.$EventoId.'

                                                ORDER BY
                                                    fk_evs_id desc
                                            ';

        return $this->db->query($SqlSerieDoEvento)->result();

    }








    /**
    * Processar
    *
    * @author Gustavo Botega
    */
    public function Processar(){


        $Dados  =   $_POST;



        $EventoId   =   $Dados['evento-id'];


        // Validacao Formulario
        $Validar                                    =   $this->Validar($Dados);
        if(!$Validar)
            return false;


        $Gravar                                     =   $this->Gravar($Dados);
        if($Gravar):

            // ID da Prova
            $ProvaId                                =   $Gravar;

            // Gravar Tipo do Evento
            $SerieId                                =   $Dados['serie'];
            $GravarCategoria                        =   $this->GravarCategoriasDaSerie($ProvaId, $SerieId);

        endif;


        if(!$Gravar)
            return false;


        // Cadastro atleta finalizado com sucesso
        redirect(base_url() . 'evento/BackOffice/Serie/Listagem/' . $EventoId);

    }





    /**
    * Validar
    *
    *
    * @author Gustavo Botega
    * @return {type}
    */
    public function Validar($Dados){

        // Validar inputs e regras de negocio do formulario. Tratar erro.

        return true;

    }




    /**
    * Gravar
    *
    * @author Gustavo Botega
    */
    public function Gravar($Dados){


        foreach ($Dados as $key => $value) {
            if(empty($value))
                $Dados[$key]    =   NULL;
        }

        // Prova Dependente
        ($Dados['prova-dependente'] == 2) ? $Dados['prova-dependente'] = NULL : '';


        // Dados
        $Dataset       =  array(


            'fk_aut_id'                                 =>    $this->session->userdata('PessoaId'),
            'fk_sta_id'                                 =>    34, // [ Prova ] a realizar. ID Status. Referencia: tb_status
            'fk_ers_id'                                 =>    $Dados['serie'], // ID da Serie do evento que a prova esta vinculada. Referencia: tb_evento_rel_serie
            'fk_spp_id'                                 =>    $Dados['tipo-pista'], // Pista. Referencia: tb_evento_serie_prova_pista
            'fk_sps_id'                                 =>    $Dados['tipo-sorteio'], // Sorteio - Referencia: tb_evento_serie_prova_sorteio
            'fk_spc_id'                                 =>    $Dados['caracteristica'], // Caracteristica da Prova. Referencia: tb_evento_serie_prova_caracteristica

            'srp_nome'                                  =>    $Dados['nome-prova'], //
            'srp_nome_trofeu'                           =>    $Dados['nome-trofeu'], //
            'srp_numero_prova'                          =>    $Dados['numero-prova'], // Numero Prova

            'srp_dia'                                   =>    $Dados['dia'], //
            'srp_hora'                                  =>    $Dados['hora'], //

            /* Valores e Limites */
            'srp_valor'                                 =>    $Dados['valor-prova'],
            'srp_valor_promocional'                     =>    $Dados['valor-prova-promocional'],
            'srp_limite_inscricao_prova'                =>    $Dados['limite-inscricao-prova'],
            'srp_limite_inscricao_atleta'               =>    $Dados['limite-inscricao-atleta'],

            /* Salto */
            'srp_velocidade'                            =>    $Dados['velocidade'],
            'srp_obstaculo_altura'                     =>    $Dados['obstaculo-altura'],
            'srp_obstaculo_largura'                    =>    $Dados['obstaculo-largura'],

            /* Salto */
            'flag_prova_dependente'                     =>    $Dados['prova-dependente'],

            'criado'                                    =>    date("Y-m-d H:i:s")

        );


        /* Query */
        $Query = $this->db->insert('tb_evento_rel_serie_rel_prova', $Dataset);

        return ($Query) ? $this->db->insert_id() : false;

    }



    /**
    * GravarCategoriasDaSerie
    *
    * Funcao responsavel por gravar na prova as mesmas categorias que eixstem na serie.
    *
    * Exemplo:
    *           Na serie 1.10M existe as categorias PMR, JCA, AMA, MA E CN5. Logo na prova tambem serao
    *           gravados essas categorias.
    *
    * @author Gustavo Botega
    */
    public function GravarCategoriasDaSerie($ProvaId, $SerieId){

        $CategoriasDaSerie  =   $this->GetCategoriasDaSerie($SerieId);

        foreach ($CategoriasDaSerie as $key => $value) {

            // Dados
            $Dataset       =  array(
                'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
                'fk_srp_id'                         =>    $ProvaId,
                'fk_evc_id'                         =>    $value->fk_evc_id,
                'flag_bloquear_delacao'             =>    1,
                'criado'                            =>    date("Y-m-d H:i:s")
            );

            // Query
            $Query = $this->db->insert('tb_evento_rel_serie_rel_prova_rel_categoria', $Dataset);

        }



    }



    /**
    * GetCategoriasDaSerie
    *
    * @author Gustavo Botega
    */
    public function GetCategoriasDaSerie($SerieId){

        $SqlCategoriasDaSerie              = '
                                                SELECT * FROM
                                                    tb_evento_rel_serie_rel_categoria as src

                                                WHERE
                                                    fk_ers_id  = '.$SerieId.'

                                                ORDER BY
                                                    fk_evc_id desc
                                            ';

       return $this->db->query($SqlCategoriasDaSerie)->result();

    }






    /**
    * PackagesClass
    *
    * @author Gustavo Botega
    */
    public function ThemeComponent(){



        /* STYLES
        -----------------------------------*/

            // PLUGINS  //
                $this->data['StylesFile']['Plugins']['bootstrap-select']                            = TRUE;
                $this->data['StylesFile']['Plugins']['multi-select']                                = TRUE;
                $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
                $this->data['StylesFile']['Plugins']['select2-bootstrap']                           = TRUE;



            // STYLES  //

                /* {NamePackage} */
                $this->data['StylesFile']['Styles']['profile']                                      = TRUE;




        /* SCRIPTS
        -----------------------------------*/

            // PLUGINS  //

                /* {NamePackage} */
                $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
                $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
                $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
                $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;

                /* Multiple Select */
                $this->data['ScriptsFile']['Plugins']['jquery-multi-select']                      = TRUE;
                $this->data['ScriptsFile']['Plugins']['select2-full']                             = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-select']                         = TRUE;

                /* User Profile */
                $this->data['ScriptsFile']['Plugins']['jquery-sparkline']                         = TRUE;






            // SCRIPTS //

                /* Bootstrap Sweet Alert */
                $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                  = TRUE;

                /* Multiple Select */
                $this->data['ScriptsFile']['Scripts']['components-multi-select']        = TRUE;

                /* User Profile */
                $this->data['ScriptsFile']['Scripts']['profile']                        = TRUE;

    }



    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega
    * @return array
    */
    public function SigepeAsset()
    {
        /* Carregando Estilos */
//        $this->data['PackageStyles'][]    =   'packages/frontoffice/style/cadastro-serie';


        /* Carregando Scripts */
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Evento/BackOffice/FormValidationCadastroProva';


    }



     /*
             // @@@ Implementar permissões por usuario

        // AUDITORIA:
            [0]=> Slug Module
            [1]=> Table Database
            [2]=> Column Database
            [3]=> Value Deleted
            [4]=> Folder Thumb
            [5]=> Delete Gallery
            [6]=> Table Gallery
            [7]=> Column Table Gallery
            [8]=> Data/hora
            [9]=> IP
            [10]=> ID Usuário
            [11]=> Type of Operation
            */

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
