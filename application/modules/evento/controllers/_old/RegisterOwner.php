<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Authentication extends MX_Controller {

    function __construct() {
        parent::__construct();

        $this->clear_cache();
    }


    public function index() {
		$this->user();    	
    }

    function clear_cache()
    {
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
    }    


    /***********************************************    
      User
    ***********************************************/
    public function user() {

        /* Se já existir uma sessao ativa redireicona para a dashboard  */
        if($this->session->userdata('authenticated')){
            redirect(base_url().'dashboard', 'refresh');
        }
        else{
            $data = array();
            $this->loadAuthentication('user', $data);            
        }

    }



    /**
     * Funcao recebe uma requisicao Ajax.
     * Atencao: a funcao nao recebe nenhum valor, porem sao resgatados por $_POST
     * A funcao recebe o valor do cpf e verifica se existe alguma ocorrencia na tabela tb_pessoa
     * se existir ele retorna com algumas informacoes.
     *
     * @author Gustavo Botega 
     * @param $_POST['cpfWithoutMask'] - recebe valor do CPF com o valor formatado. Valor contem 11 digitos.    
     * @return string JSON
     */      
    public function verifyUser() {


        /* CPF */
        $cpf    =   $_POST['cpfWithoutMask'];

        // Checar se existe na tabela tb_pessoa alguem com o cpf informado
        $query = $this->model_crud->select('tb_pessoa', array('pes_id', 'fk_sta_id', 'pes_nome', 'pes_cpf', 'pes_foto'), array('pes_cpf ='=>$cpf), NULL, 1 );

        /* Verificacao da consulta*/
        if( !empty($query) ){

            $array = array(
                'pes_id'        =>  $query[0]->pes_id,
                'fk_sta_id'     =>  $query[0]->fk_sta_id,
                'pes_nome'      =>  $query[0]->pes_nome,
                'pes_cpf'       =>  $query[0]->pes_cpf,
                'pes_foto'      =>  $query[0]->pes_foto,
                'status'        =>  'processed'
            );

        }else{

            $array = array(
                'status'    =>  'unprocessed'
            );            

        }


        /**
         * Saida
         */
        echo json_encode($array);

    }



    /**
     * Funcao recebe uma requisicao Ajax.
     * Atencao: a funcao nao recebe nenhum valor, porem sao resgatados por $_POST
     * A funcao recebe o valor do cpf e verifica se existe alguma ocorrencia na tabela tb_pessoa
     * se existir ele retorna com algumas informacoes.
     *
     * @author Gustavo Botega 
     * @param $_POST['cpfWithoutMask'] - recebe valor do CPF com o valor formatado. Valor contem 11 digitos.    
     * @return string JSON
     */      
    public function checkPassword() {

        /**
         * Resgatando valores por $_POST e armazenando em variaveis
         */      
        $cpf         =   $_POST['cpfWithoutMask'];
        $password    =   $_POST['passwordEntryUser'];


        /**
         * Setando valor do $token
         */      
        $token       =   't7nr';
        $passwordToken      =   $token . $password;


        /**
         * Encriptando senha
         */      
        $passwordEncrypted  =   md5($passwordToken);


        /**
         * SQL
         */      
        $sql    =   "
                        SELECT p.pes_id, p.pes_cpf, p.pes_nome, p.pes_foto, u.usu_id, u.fk_pes_id, u.fk_sta_id, u.usu_senha
                        FROM tb_usuario as u
                        JOIN tb_pessoa as p ON p.pes_id = u.fk_pes_id
                        WHERE pes_cpf = '".$cpf."'
                        AND usu_senha = '".$passwordEncrypted."'
                        LIMIT 1
                    ";
        $query  =   $this->db->query($sql)->result();


        /**
         * Checa se a query nao retornou vazio
         */      
        if( !empty($query) && $query ){

            /**
             * Apenas e registrado a sessao se o status do USUARIO for ATIVO.
             */      
            if($query[0]->fk_sta_id=='1'){
                $this->setSession(
                    $query[0]->usu_id,
                    $query[0]->pes_id,
                    $query[0]->pes_cpf,
                    $query[0]->pes_foto,
                    $query[0]->pes_nome,
                    $token
                );
            }

            $array = array(
                'fk_sta_id'     =>  $query[0]->fk_sta_id, // status do usuario. NAO EH da pessoa
                'status'        =>  'processed'
            );

        }else{

            $array = array(
                'status'    =>  'unprocessed'
            );            

        }


        /**
         * Saida
         */
        echo json_encode($array);

    }



    public function settings()
    {

        $data           = array();
        $data['token']  =   't7nr';

        return $data;
    }






    /**
     * Set Session   
     *
     *
     * @author Gustavo Botega 
     * @param int $userId -    
     * @return TRUE 
     */      
    public function setSession($userId, $peopleId, $peopleCpf, $peopleThumb, $peopleFirstName, $token)
    {

        /* RETORNAR ARRAY COM OS DADOS */
        $arrData = array(
            'userId'            =>      $userId,
            'peopleId'          =>      $peopleId,
            'peopleCpf'         =>      $peopleCpf,
            'peopleThumb'       =>      $peopleThumb,
            'peopleFirstName'   =>      $peopleFirstName,
            'token'             =>      $token,
            'authenticated'     =>      TRUE
        );

        $this->session->set_userdata($arrData);

        return TRUE;
    }





    public function logout()
    {

        // # resgatando os valores que sao gravados na sessao do usuario quando autenticado.
        $autenticado    = $this->session->userdata('authenticated');
        $token          = $this->session->userdata('token');
            
        if( $autenticado == TRUE && $token == 't7nr' )
        {
            // # destruindo sessao do code Igniter 
            $this->session->sess_destroy(); 

        }
            

        // O usuario seve ser redirecionado pra a tela de login .
        redirect(base_url() . 'authentication/authentication/index', 'refresh');

    }






    /**
     * Mostra a estrutura do template 
     * @param string $pathView
     */
    protected function loadAuthentication( $pathView = 'template/authentication/', $settings = NULL )
    {
        $data = array();

        $settings['base_url']   =   base_url();
        $data['settings']       =   $settings;

        /*Settings variables */
        $this->parser->parse('template/authentication/header', $data['settings']);
        $this->parser->parse( $pathView, $data['settings']);
        $this->parser->parse('template/authentication/footer', $data['settings']);
    }


}
