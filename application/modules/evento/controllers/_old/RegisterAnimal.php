<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_CommonOffice.php';

class RegisterAnimal extends MY_CommonOffice {

    public $data;

    function __construct()
    {
        parent::__construct();
        
        $this->data = array();
        $this->load->module('register');
        $this->load->module('authentication');
        $this->data = $this->register->settings();

        $this->load->module('register/RegisterCompetitor');


        /*
            Camada de Verificacao de Autenticacao de Proprietario
            - A validacao ocorre no Modulo: register  Metodo: OwnerRecordSession
            - Tanto no backoffice quanto no frontoffice passam por essa cada antes de chegar no metodo RegisterAnimal
            -  
        */
//        IsOwnerAuthenticated();


        $this->data['FormIdentification']           =   "FormRegisterAnimal";



        /* Variaveis de Template */
        $this->data['EnvironmentPatchModule']       =   'Register/Animal/';
        $this->data['EnvironmentModule']            =   'Register';
        $this->data['EnvironmentModuleSlug']        =   'register';
        $this->data['EnvironmentClass']             =   'BackOffice';
        $this->data['EnvironmentMethod']            =   '';
        $this->data['EnvironmentSidebarDisplay']    =   'closed';
        $this->data['EnvironmentOffice']            =   'BackOffice';
        $this->data['EnvironmentOfficeSlug']        =   'backoffice';



        /* Package */
        $this->PackagesClass();
        $this->PackagesAssets();

        $this->clear_cache();
    }



    /*
      Atividades
    ----------------------------------------------*/
    /*
        1) Verificar se já existe um cpf e notificar. Bloquear cadastro se já existir.
        2) Se a pessoa for menor de idade retornar aviso e pedir pra informar o CPF do resposável.
    */


    public function index() {
		$this->Form();    	
    }


    /*
      Form
    ----------------------------------------------*/
    public function Form() {


        /* Checando se e um Owner Authenticated */
        IsOwnerAuthenticated();

        /* Flashdata */
        $FlashData  =   $this->session->flashdata('FlashDataRegisterAnimal');
        if(!$FlashData || is_null($FlashData)){
            $this->authentication->DestroyAuthenticateOwner();
         #   redirect(base_url() . 'dashboard');
        }


        /* 
            Selects
        */  
        $this->GetDropdowns();




        $path   =   'Register/Animal/';
        $this->LoadTemplateRegister(
            array(

                $path . 'Header',
                $path . 'Tab1',
                $path . 'Tab2',
                $path . 'Tab3',
                $path . 'Tab4',
                $path . 'Modal',
                $path . 'Footer'

            ), $this->data
        );            
    }




    /**
    * GetDropdowns
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function GetDropdowns(){
        $this->data['dropdownState']            =   $this->register->getDropdownState();
        $this->data['dropdownRace']             =   $this->register->GetDropdownRace();
        $this->data['dropdownFur']              =   $this->register->GetDropdownFur();
        $this->data['dropdownNationality']      =   $this->register->getDropdownNationality();
        $this->data['dropdownFederation']       =   $this->register->getDropdownFederation();
        $this->data['dropdownTypeEntry']        =   $this->registercompetitor->GetDropdownTypeEntry();
        $this->data['dropdownTypeEntryYearly']  =   $this->registercompetitor->DropdownTypeEntrySlug('yearly', 'animal');
        $this->data['dropdownTypeEntryCup']     =   $this->registercompetitor->DropdownTypeEntrySlug('cup', 'animal');
        $this->data['dropdownTypeEntrySingle']  =   $this->registercompetitor->DropdownTypeEntrySlug('single', 'animal');
        return $this->data;
    }


    /**
    * RegisterAnimalRedirect
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function RegisterAnimalRedirect( $OwnerId = NULL ) 
    { 
        if( $OwnerId == NULL )   
         redirect(base_url() . 'dashboard', 'redirect');


        if( $this->session->userdata('OwnerId') == $OwnerId){
            $this->session->set_flashdata('FlashDataRegisterAnimal', TRUE);
            redirect(base_url() . 'register/RegisterAnimal/Form', 'refresh');
            return true;
        }

        redirect(base_url() . 'dashboard', 'redirect');

    }

    /**
    * CheckResponsibleFinance
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function CheckResponsibleFinance( $cpf )
    { 

        $data               =       array();


        /* IsPeople */
        $IsPeople           =       $this->peocom->IsPeople( array( 'DataType' => 'PeopleCpf', 'DataEntry'=>$cpf ), 'boolean' );
        if( $IsPeople ):

            /* IsMajority */
            $IsMajority         =       $this->peocom->IsMajority( array( 'DataType' => 'PeopleCpf', 'DataEntry'=>$cpf ), 'boolean' );
            if( $IsMajority ):

                /* IsOwner */
                $IsOwner         =       $this->peocom->IsOwner( array( 'DataType' => 'PeopleCpf', 'DataEntry'=>$cpf ), 'data' );
                if( $IsOwner ):
                    $this->authentication->AuthenticateOwnerResponsibleFinancial( $IsOwner['OwnerId'] );
                    $data['StatusResponse']         =   TRUE;
                    $data['OwnerId']                =   $IsOwner['OwnerId'];
                    $data['PeopleId']               =   $IsOwner['PeopleId'];
                    $data['PeopleFullName']         =   $IsOwner['PeopleFullName'];
                    $data['PeopleCpf']              =   $IsOwner['PeopleCpf'];
                endif;

                /* Not Owner */
                if( !$IsOwner ):
                    $data['StatusResponse']         =   FALSE;
                    $data['MessageResponse']        =   'NotOwner';
                endif;

            endif;

            /* Not Majority */
            if( !$IsMajority ):
                $data['StatusResponse']         =   FALSE;
                $data['MessageResponse']        =   'NotMajority';
            endif;

        endif;


        /* Not People */
        if( !$IsPeople ):
            $data['StatusResponse']             =   FALSE;
            $data['MessageResponse']            =   'NotPeople';
        endif;

        $data['DateTime']                   =   date("d/m/Y") . " às " . date("H:i:s");
        $data['InternetProtocol']           =   $this->input->ip_address();

        echo json_encode($data);
    }





    /*
     * CheckCPF
     * - Verifica na coluna ani_chip da tabela tb_animal se existe alguma ocorrencia com o valor submetido. 
     * - A funcao e chamada por uma requisicao ajax tipo post. Nao recebe parametros na funcao.
     * - Se ja existir algum CHIP a funcao retorna TRUE se nao FALSE
     * @author Gustavo Botega 
     * @param     
     * @return boolean  retorna TRUE se o CPF existir
    */
    public function CheckChip() {

        $chip    =   $_POST['chip'];
        $query = $this->model_crud->select(
                    'tb_animal',
                    array( 'ani_id', 'ani_fullName', 'ani_chip'),
                    array('ani_chip =' => $chip),
                    array(),
                    1
                );

        if(empty($query)){
            $return = array('status' => 'ChipNotExist');
        }else{
            $firstName      = $query[0]->ani_fullName;
            $return = array(
                'status'    => 'ChipExist',
                'firstName' => $firstName,
            );

        }

        echo( json_encode( $return ) );

    }






    /**
    * PackagesAssets
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function PackagesAssets()
    { 
        $this->data['PackageScripts']     = array('Main', 'FormWizard');
        $this->data['PackageStyles']      = array('Main');
    }


    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function PackagesClass()
    { 
        $this->data['PackageFormWizard']    = TRUE;
        $this->data['PackageSelect2']       = TRUE;
        $this->data['PackageToastr']        = TRUE;
        $this->data['PackageForm']          = TRUE;
        $this->data['PackageFormMask']      = TRUE;

        $this->data['PackageRegister']      = TRUE;
        $this->data['PackageRegisterEntry'] = TRUE;
        $this->data['PackageRegisterBonds'] = TRUE;

    }






    function clear_cache()
    {
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
    }    


}
