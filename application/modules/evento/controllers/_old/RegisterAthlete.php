<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_GuestOffice.php';

class RegisterAthlete extends MY_GuestOffice {

    public $data;

    function __construct()
    {
        parent::__construct();
        
        $this->data = array();


        $this->load->module('register');
        $this->data = $this->register->settings();

        $this->load->module('register/RegisterCompetitor');



        /*
            Camada de Verificacao de Autenticacao de Proprietario
            - A validacao ocorre no Modulo: register  Metodo: OwnerRecordSession
            - Tanto no backoffice quanto no frontoffice passam por essa cada antes de chegar no metodo RegisterAnimal
            -  
        */
        $this->data['FormIdentification']           =   "FormRegisterAthlete";



        /* Variaveis de Template */
        $this->data['templateModule']               =   'register';
        $this->data['templatePatchModule']          =   'Register/Athlete';
        $this->data['templateClass']                =   'RegisterAthlete';
        $this->data['templateMethod']               =   '';
        $this->data['templateSidebarDisplay']       =   TRUE;



        /* Environment */
        $this->data['EnvironmentPatchModule']       =   'Register/Athlete';
        $this->data['EnvironmentModule']            =   'Register';
        $this->data['EnvironmentModuleSlug']        =   'register';
        $this->data['EnvironmentClass']             =   'RegisterAthlete';
        $this->data['EnvironmentMethod']            =   '';
        $this->data['EnvironmentSidebarDisplay']    =   TRUE;
        $this->data['EnvironmentOffice']            =   'GuestOffice';
        $this->data['EnvironmentOfficeSlug']        =   'guestoffice';



        /* Package */
        $this->PackagesClass();
        $this->PackagesAssets();

        $this->clear_cache();
    }



    /*
      Atividades
    ----------------------------------------------*/
    /*
        1) Verificar se já existe um cpf e notificar. Bloquear cadastro se já existir.
        2) Se a pessoa for menor de idade retornar aviso e pedir pra informar o CPF do resposável.
    */


    public function index() {
		$this->Form();    	
    }


    /*
      Form
    ----------------------------------------------*/
    public function Form() {

     //   IsGuest();

        /* 
            Selects
        */  
        $this->GetDropdowns();

        $path   =   'Register/Athlete/';
        $this->LoadTemplateRegister(
            array(

                $path . 'Header',
                $path . 'Tab1',
                $path . 'Tab2',
                $path . 'Tab3',
                $path . 'Tab4',
                $path . 'Modal',
                $path . 'Footer'

            ), $this->data
        );            
    }



    /*
      Form
    ----------------------------------------------*/
    public function RegisterAthleteUpdate() {

        IsAuthenticated();

        /* Se usuario ja estiver registrado nao existe a necessidade de atualizar o registro de atleta. Redireciona pra my-profile */
        $UserAlreadyRegisterComplete       =   $this->model_crud->get_rowSpecific('tb_people', 'peo_id', $this->session->userdata('UserId'), 1, 'flag_registerComplete');
        if($UserAlreadyRegisterComplete)
            redirect(base_url().'my-profile');

        $this->data['PackageScripts']     = array('Main', 'FormWizard', 'MainUpdate', 'Peocom', 'jquery.validationBrasil', 'FormPeople');

        $DateBirth          =   $this->model_crud->get_rowSpecific('tb_people', 'peo_id', $this->session->userdata('UserId'), 1, 'peo_birthDate');
        $DateBirthPTbr      =   $this->my_date->date($DateBirth, 'en', 'convertIsoToPtbr');
        $CPF                =   $this->model_crud->get_rowSpecific('tb_people', 'peo_id', $this->session->userdata('UserId'), 1, 'peo_cpf');

        $this->data['PeopleUpdateRegister']     =   $this->session->userdata('PeopleUpdateRegister');
        $this->data['PeopleDateBirth']          =   $DateBirthPTbr;
        $this->data['PeopleUpdateCpf']          =   MaskCpf($CPF, true);
        $this->data['PeopleUpdateName']         =   $this->model_crud->get_rowSpecific('tb_people', 'peo_id', $this->session->userdata('UserId'), 1, 'peo_fullName');
        $this->data['PeopleUpdateGender']       =   $this->model_crud->get_rowSpecific('tb_people', 'peo_id', $this->session->userdata('UserId'), 1, 'fk_gen_id');


        /* 
            Selects
        */  
        $this->GetDropdowns();


        $path   =   'Register/Athlete/';
        $this->LoadTemplateRegister(
            array(

                $path . 'Header',
                $path . 'Tab1',
                $path . 'Tab2',
                $path . 'Tab3',
                $path . 'Tab4',
                $path . 'Modal',
                $path . 'Footer'

            ), $this->data
        );            
    }



    /*
     * GetDropdowns
     * @author Gustavo Botega 
     * @param     
     * @return  
    */      
    public function GetDropdowns(){
        /* Selects */
        $this->data['dropdownMaritalStatus']    =   $this->register->getDropdownMaritalStatus();
        $this->data['dropdownScholarity']       =   $this->register->getDropdownScholarity();
        $this->data['dropdownBloodType']        =   $this->register->getDropdownBloodType();
        $this->data['dropdownState']            =   $this->register->getDropdownState();
        $this->data['dropdownNationality']      =   $this->register->getDropdownNationality();
        $this->data['dropdownFederation']       =   $this->register->getDropdownFederation();
        $this->data['dropdownTypeTelephone']    =   $this->register->getDropdownTypeTelephone();
       // $this->data['dropdownNaturalness']     =   $this->register->getDropdownNaturalness();
        $this->data['dropdownTypeEntry']        =   $this->registercompetitor->GetDropdownTypeEntry();
        $this->data['dropdownTypeEntryYearly']  =   $this->registercompetitor->DropdownTypeEntrySlug('yearly');
        $this->data['dropdownTypeEntryCup']     =   $this->registercompetitor->DropdownTypeEntrySlug('cup');
        $this->data['dropdownTypeEntrySingle']  =   $this->registercompetitor->DropdownTypeEntrySlug('single');

/*  
        # animal

        $this->data['dropdownState']            =   $this->register->getDropdownState();
        $this->data['dropdownRace']             =   $this->register->GetDropdownRace();
        $this->data['dropdownFur']              =   $this->register->GetDropdownFur();
        $this->data['dropdownNationality']      =   $this->register->getDropdownNationality();
        $this->data['dropdownTypeEntry']        =   $this->getDropdownTypeEntry();
        $this->data['dropdownTypeEntryYearly']  =   $this->dropdownTypeEntrySlug('yearly');
        $this->data['dropdownTypeEntryCup']     =   $this->dropdownTypeEntrySlug('cup');
        $this->data['dropdownTypeEntrySingle']  =   $this->dropdownTypeEntrySlug('single');*/

        return $this->data;
    }



    /**
    * CheckResponsibleFinance
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function CheckResponsibleFinance( $cpf )
    { 

        $data               =       array();


        /* IsPeople */
        $IsPeople           =       $this->peocom->IsPeople( array( 'DataType' => 'PeopleCpf', 'DataEntry'=>$cpf ), 'boolean' );
        if( $IsPeople ):

            /* IsMajority */
            $IsMajority         =       $this->peocom->IsMajority( array( 'DataType' => 'PeopleCpf', 'DataEntry'=>$cpf ), 'boolean' );
            if( $IsMajority ):

                /* IsOwner */
                $IsOwner         =       $this->peocom->IsOwner( array( 'DataType' => 'PeopleCpf', 'DataEntry'=>$cpf ), 'data' );
                if( $IsOwner ):
                    $this->authentication->AuthenticateOwnerResponsibleFinancial( $IsOwner['OwnerId'] );
                    $data['StatusResponse']         =   TRUE;
                    $data['OwnerId']                =   $IsOwner['OwnerId'];
                    $data['PeopleId']               =   $IsOwner['PeopleId'];
                    $data['PeopleFullName']         =   $IsOwner['PeopleFullName'];
                    $data['PeopleCpf']              =   $IsOwner['PeopleCpf'];
                endif;

                /* Not Owner */
                if( !$IsOwner ):
                    $data['StatusResponse']         =   FALSE;
                    $data['MessageResponse']        =   'NotOwner';
                endif;

            endif;

            /* Not Majority */
            if( !$IsMajority ):
                $data['StatusResponse']         =   FALSE;
                $data['MessageResponse']        =   'NotMajority';
            endif;

        endif;


        /* Not People */
        if( !$IsPeople ):
            $data['StatusResponse']             =   FALSE;
            $data['MessageResponse']            =   'NotPeople';
        endif;

        $data['DateTime']                   =   date("d/m/Y") . " às " . date("H:i:s");
        $data['InternetProtocol']           =   $this->input->ip_address();

        echo json_encode($data);
    }








    /**
    * PackagesAssets
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function PackagesAssets()
    { 
        $this->data['PackageScripts']     = array('Main', 'FormWizard', 'Peocom', 'jquery.validationBrasil', 'FormPeople');
        $this->data['PackageStyles']      = array('Main');
    }


    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function PackagesClass()
    { 
        $this->data['PackageFormWizard']    = TRUE;
        $this->data['PackageToastr']        = TRUE;
        $this->data['PackageSelect2']       = TRUE;
        $this->data['PackageForm']          = TRUE;
        $this->data['PackageFormMask']      = TRUE;
    }




    function clear_cache()
    {
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
    }    


}
