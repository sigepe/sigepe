<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_GuestOffice.php';

class RegisterGeneric extends MY_GuestOffice {

    public $data;

    function __construct()
    {
        parent::__construct();
        
        $this->data = array();

        $this->load->module('register');
        $this->data = $this->register->settings();


        $this->data['FormIdentification']           =   "FormRegisterGeneric";


        /* Environment */
        $this->data['EnvironmentPatchModule']       =   'Register/Generic';
        $this->data['EnvironmentModule']            =   'Register';
        $this->data['EnvironmentModuleSlug']        =   'register';
        $this->data['EnvironmentClass']             =   'RegisterGeneric';
        $this->data['EnvironmentMethod']            =   '';
        $this->data['EnvironmentSidebarDisplay']    =   TRUE;
        $this->data['EnvironmentOffice']            =   'GuestOffice';
        $this->data['EnvironmentOfficeSlug']        =   'guestoffice';



        /* Package */
        $this->PackagesClass();
        $this->PackagesAssets();

        $this->clear_cache();
    }


    public function index() {
		$this->Form();    	
    }


    /*
      Form
    ----------------------------------------------*/
    public function Form() {

        IsGuest();

        /* 
            Selects
        */  
        $this->GetDropdowns();

        $path   =   'Register/Generic/';
        $this->LoadTemplateRegister(
            array(

                $path . 'Header',
                $path . 'Tab1',
                $path . 'Tab2',
                $path . 'Tab3',
                $path . 'Modal',
                $path . 'Footer'

            ), $this->data
        );            
    }


    /*
     * GetDropdowns
     * @author Gustavo Botega 
     * @param     
     * @return  
    */      
    public function GetDropdowns(){
        /* Selects */
        $this->data['dropdownMaritalStatus']    =   $this->register->getDropdownMaritalStatus();
        $this->data['dropdownScholarity']       =   $this->register->getDropdownScholarity();
        $this->data['dropdownBloodType']        =   $this->register->getDropdownBloodType();
        $this->data['dropdownState']            =   $this->register->getDropdownState();
        $this->data['dropdownNationality']      =   $this->register->getDropdownNationality();
        $this->data['dropdownFederation']       =   $this->register->getDropdownFederation();
        $this->data['dropdownTypeTelephone']    =   $this->register->getDropdownTypeTelephone();
       // $this->data['dropdownNaturalness']     =   $this->register->getDropdownNaturalness();

        return $this->data;
    }


    /**
    * PackagesAssets
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function PackagesAssets()
    { 
        $this->data['PackageScripts']     = array('FormWizard');
        $this->data['PackageStyles']      = array('Main');
    }


    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function PackagesClass()
    { 
        $this->data['PackageFormWizard']                    = TRUE;
        $this->data['PackageToastr']                        = TRUE;
        $this->data['PackageSelect2']                       = TRUE;
        $this->data['PackageForm']                          = TRUE;
        $this->data['PackageFormMask']                      = TRUE;
        $this->data['PackageRegister']                      = TRUE;
        $this->data['PackageRegisterCpf']                   = TRUE;
        $this->data['PackageRegisterAddress']               = TRUE;
        $this->data['PackageRegisterUnderAge']              = TRUE;
        $this->data['PackageRegisterMethodsValidation']     = TRUE;
    }


    /*
     * SetPeopleRegisterComplete
     *
     * Recebe como parametro POST o ID da pessoa e a seta como registro completo.
     *
     * @author Gustavo Botega 
     * @param     
     * @return boolean  
    */
    public function SetPeopleRegisterComplete() {

        /* PeopleId */
        $PeopleId               =   $_POST['PeopleId'];


        /* Autor - se o usuario estiver autenticado na plataforma o autor do registro e o proprio usuario caso nao e o Guest. */
        if($this->session->userdata('UserAuthenticated')){
            $Author     =   $this->session->userdata('UserId');
        }else{
            $Author     =   $_POST['PeopleId'];
        }

        /* Query */
        $data = array(
                       'flag_registerComplete' => 1
                    );

        $this->db->where('peo_id', $PeopleId);
        $query = $this->db->update('tb_people', $data); 
        
        if($query)
        {
            $array = array('status'    =>  'processed' );
            echo json_encode($array);
        }

        if(!$query){
            $DataBulk                               =   array();
            $DataBulk['Message']                    =      '';
            $DataBulk['Author']                     =      $Author;
            $this->RegisterLog($DataBulk);
        }

    }



    /*
     * SetPeocomTypeGeneric
     *
     * Recebe como parametro POST o ID da pessoa e a registra com o tipo de Generico
     *
     * @author Gustavo Botega 
     * @param     
     * @return boolean  
    */
    public function SetPeocomTypeGeneric() {

        /* PeopleId */
        $PeopleId               =   $_POST['PeopleId'];


        /* Autor - se o usuario estiver autenticado na plataforma o autor do registro e o proprio usuario caso nao e o Guest. */
        if($this->session->userdata('UserAuthenticated')){
            $Author     =   $this->session->userdata('UserId');
        }else{
            $Author     =   $_POST['PeopleId'];
        }

        /* Query */
        $data = array(
                        'fk_aut_id'         => $Author,
                        'fk_peo_id'         => $PeopleId,
                        'fk_com_id'         => NULL,
                        'fk_typ_id'         => 1,
                        'fk_sta_id'         => 1,
                        'rel_comment'       => NULL,
                        'rel_reference'     => NULL,
                        'createdAt'         => date("Y-m-d H:i:s")
                    );

        $query = $this->db->insert('tb_relationship_peocom_type', $data); 
        if($query){

            $data = array(
                            'fk_aut_id'         => $Author,
                            'fk_rel_id'         => $this->db->insert_id(),
                            'fk_sta_id'         => 1,
                            'his_note'          => NULL,
                            'createdAt'         => date("Y-m-d H:i:s")
                        );
            $query = $this->db->insert('tb_historic_relationship_peocom_type', $data);             
            if($query){
                $array = array('status'    =>  'processed' );
                echo json_encode($array);
                return true;
            }else{
                $DataBulk['Message']                    =      'Erro ao gravar historico relacionamento peocom type = generic.';
                $DataBulk['Author']                     =      $Author;
                $this->RegisterLog($DataBulk);
                return false;
            }

        }

        $DataBulk['Message']                    =      'Erro ao gravar relacionamento peocom type = generic.';
        $DataBulk['Author']                     =      $Author;
        $this->RegisterLog($DataBulk);
        return false;
    }


    function clear_cache()
    {
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
    }    


}
