<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Confederacao extends MY_BackOffice {

    public $data;
    public $ambiente = "BackOffice";
    public $modulo = "PessoaJuridica";
    public $tabelaNome = "tb_pessoa_juridica_confederacao";
    public $viewNome = "vw_pj_confederacao";
    public $columnId = "pjc_id";
    public $columnDesc = "pjc_acronimo";

    /* url de acesso*/
    // http://localhost/sigepe/pessoajuridica/BackOffice/Confederacao/

    function __construct() {
        parent::__construct();

        $this->metronicAsset();
        $this->sigepeAsset();

        /* Setando variaveis de ambiente */
        $this->data['PageHeadTitle'] = "Confederacao";
        $this->data['PageHeadSubtitle'] = "PessoaJuridica";


        $modulo = "PessoaJuridica";
        $this->data['ShowSidebar'] = true;
        $this->data['ShowColumnLeft'] = "Profile" . $modulo;
        $this->data['base_dir'] = "Template/BackOffice/sistema/$modulo/";
        $this->data['Class'] = $this->router->fetch_class();

        /* Controladores Externos */
        #$this->load->module('evento/XOffice/Evento');
    }

    public function index() {
        $this->confederacao_browse_bread();
    }

    /**
     * confederacao_browse_bread.
     * @author Nillander
     * @return
     */
    public function confederacao_browse_bread() {
        $sql = "SELECT * FROM $this->viewNome";
        $dataset = $this->db->query($sql)->result();
        $this->data['dataset'] = $dataset;
        /* View */
        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);
    }

// fim do metodo @confederacao_browse_bread

    /**
     * confederacao_read
     * @author Nillander
     * @return
     */
    public function confederacao_read($contextoId) {
        $sql = "SELECT * FROM $this->viewNome where $this->columnId = $contextoId";
        $dataset = $this->db->query($sql)->result();
        $this->data['dataset'] = $dataset;
        /* View */
        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);
    }

// fim do metodo @confederacao_read

    /**
     * confederacao_add
     * @author Nillander
     * @return
     */
    public function confederacao_add() {
        /* View */
        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);
    }

// fim do méotodo @confederacao_add

    /**
     * confederacao_edit
     * @author Nillander
     * @return
     */
    public function confederacao_edit() {
        /* View */
        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);
    }

// fim do méotodo @confederacao_edit



    /* # Fim Bread
      # Inicio Middleware
      ==================================================================================================================== */

    /**
     * processar
     * @author Nillander
     * @param $dataJson
     * @return
     */
    public function processar($dataJson = false) {
        if (isset($_POST['dataJson']) && is_array($_POST['dataJson'])) {
            $dados = $_POST['dataJson'];
        }

        if ($dataJson) {
            $dados = $dataJson;
        }

        if (!isset($_POST['dataJson']) && !$dataJson) {
            echo json_encode(false);
        }

        // Validacao Formulario
        $validar = $this->validar($dados);
        if (!$validar) {
            echo json_encode(false);
            return false;
        }

        // Se existir algum ID por convencao entende-se que o metodo a ser direcionado e de atualizacao.
        if (!isset($dados['id'])) {
            $resultadoAcao = $this->gravar($dados);
            echo ($resultadoAcao) ? json_encode(true) : json_encode(false);
        } else {
            $resultadoAlterar = $this->alterar($dados);
            echo ($resultadoAlterar) ? json_encode(true) : json_encode(false);
        }
    }

// fim do metodo @processar

    /**
     * gravar
     * @author Nillander
     * @param $dados
     * @return
     */
    public function gravar($dados) {
        foreach ($dados as $key => $value) {
            if (empty($value))
                $dados[$key] = NULL;
        }

        // dados
        $dataset = array(
            'fk_aut_id' => $this->session->userdata('PessoaId'),
            'tab_coluna' => gerarTimeStamp("tb_tabela", "tab_coluna"),
            'eve_slug' => gerarSlug($dados['eve-nome-evento']),
            'criado' => date("Y-m-d H:i:s")
        );

        /* query */
        $query = $this->db->insert($this->tabelaNome, $dataset);

        if ($query) {
            $classeId = $this->db->insert_id();
            return true;
        } else {
            return false;
        }
    }

// fim do metodo @gravar

    /**
     * alterar.
     *
     * -
     *
     * @author Nillander
     * @param $dados
     * @return
     */
    public function alterar($dados) {

        (!isset($dados['campo_null'])) ? $dados['campo_null'] = null : '';

        // dados
        $dataset = array(
            $this->columnId => $dados['id'],
            'fk_aut_id' => $this->session->userdata('PessoaId'),
            'tab_coluna' => gerarTimeStamp("tb_tabela", "tab_coluna"),
            'eve_slug' => gerarSlug($dados['name-objeto']),
            'criado' => date("Y-m-d H:i:s")
        );

        /* Query */
        $query = $this->db->update($this->tabelaNome, $dataset, array($this->columnId => $dados['id']));
        return $query;
    }

// fim do metodo @alterar

    /**
     * validar
     * @author Nillander
     * @param $dados
     * @return
     */
    public function validar($dados) {
        return true; // Validar inputs e regras de negocio do formulario. Tratar erro.
    }

// fim do metodo @validar



    /* # Fim Middleware
      # Inicio Acessório
      ==================================================================================================================== */

    /**
     * fazerAlgumaCoisa.
     * @author Nillander
     * @param $contextoId
     * @return
     */
    public function fazerAlgumaCoisa($contextoId) {
        
    }

    /* # Fim Metodo Acessório
      # Inicio Asset
      ========================================================================= */

    /**
     * metronicAsset.
     * @author Nillander
     * @param 
     * @return
     */
    public function metronicAsset() {
        /* STYLES */
        $this->data['StylesFile']['Plugins']['datatables'] = TRUE;
        $this->data['StylesFile']['Plugins']['datatables-bootstrap'] = TRUE;

        /* SCRIPTS */
        $this->data['ScriptsFile']['Plugins']['datatable'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['datatables'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['datatables-bootstrap'] = TRUE;

        $this->data['ScriptsFile']['Scripts']['table-datatables-fixedheader'] = TRUE;
    }

// fim do metodo MetronicAsset

    /**
     * sigepeAsset.
     * @author Nillander
     * @param 
     * @return
     */
    public function sigepeAsset() {
        /* Carregando Estilos */
        $this->data['PackageStyles'][] = "Packages/Styles/$this->modulo/$this->ambiente/". __CLASS__ ."";

        /* Carregando Scripts */
        $this->data['PackageScripts'][] = "Packages/Scripts/$this->modulo/$this->ambiente/". __CLASS__ ."";
    }

// fim do metodo sigepeAsset
}

/* End of file classe.php */
