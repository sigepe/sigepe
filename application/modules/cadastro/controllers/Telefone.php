<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_GuestOffice.php';
class Telefone extends MY_GuestOffice {

    public $data;

    function __construct() {
        
        parent::__construct();

    }



    /**
    * Processar
    *
    * @author Gustavo Botega 
    */
    public function Processar($Dados, $PessoaId){
        
        if($this->Validar($Dados['numero'])){
            if($this->Gravar($Dados, $PessoaId))
                return true;
        }

        return false;

    } 


    /**
    * Validar
    *
    * @author Gustavo Botega 
    */
    public function Validar($Telefone){


        // Checa se tem o DDD
        $Ddd    =    GetStringBetween($Telefone, '(', ')');
        if(strlen($Ddd) != 2)
            return false;


        // Verifica se teo o hifen do numero
        if (strpos($Telefone, '-') == false)
            return false;
        

        // Checar se nao contem letras.
        if(preg_match("/[a-z]/i", $Telefone))
            return false;


        return true;

    } 



    /**
    * Gravar
    *
    * @author Gustavo Botega 
    */
    public function Gravar($Dados, $PessoaId){


        $IdAutor            =   NULL;
        $ExplodeNumero      =   explode(" ", $Dados['numero']);


        $Dataset            =  array(
            'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
            'fk_peo_id'                         =>    $PessoaId,
            'fk_con_id'                         =>    $Dados['numero-tipo'],
            'fk_sta_id'                         =>    1,
            'tel_ddd'                           =>    $this->GetDdd($Dados['numero']),
            'tel_telefone'                      =>    $ExplodeNumero[1],
            'tel_numero'                        =>    $Dados['numero'],
            'tel_principal'                     =>    $Dados['numero-principal'],
            'criado'                            =>    date("Y-m-d H:i:s")
        );


        /* Query */
        $Query = $this->db->insert('tb_telefone', $Dataset);

        return ($Query) ? true : false;
    } 



    /**
     * GetDdd
     *
     * @author Gustavo Botega 
     */
    public function GetDdd($Telefone, $Json = FALSE)
    {

        $Ddd = GetStringBetween($Telefone, '(', ')');

        if($Json)
            echo json_encode($Ddd);

        return $Ddd;
    }



}
