<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_GuestOffice.php';
class Consulta extends MY_GuestOffice {

    public $data;

    function __construct() {
        parent::__construct();


        /* Package */
        $this->PackagesClass(); // TROCAR PARA ThemeComponent();
        $this->PackagesAssets();  // TROCAR PARA SigepeAssets();
        $this->data['TesteClasse']                =   'GuestOffice';



        // Criar classe dentro do modulo cadastro chamado Erro para processar foreachs

    }

    public function index() {
        $this->FormularioPessoaFisica();        
    }



    /**
    * Pessoa
    *
    * {desc}
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function Pessoa(){



        $this->data['PackageScripts'][]   =   'packages/guestoffice/script/consulta-pessoa';


        $this->data['IdPessoa']     =   FALSE;

        



        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Consulta';
        $this->data['PageHeadSubtitle']   = 'Relatório de cadastro de pessoa física e jurídica.';


        /*
            
            NA VIEW SE A PESSOA MARCAR QUE E ISENTO CPF, NO LUGAR DO CAMPO DO CPF COLOCAR O " CODIGO DE ACESSO " E GERAR UM TIMESTAMP JS E GRAVAR NA SESSION CODEIGNITER AJAX.
            APOS CONCLUIR CADASTRO EXCLUIR QUALQUER SESSION QUE TIVER.
        */

        /* Carrega View */
        $this->LoadTemplateGuest('Template/GuestOffice/Cadastro/Consulta/formulario-pessoa', $this->data);

    }

    /**
    * Animal
    *
    * @author Gustavo Botega 
    */
    public function Animal(){



        $this->data['PackageScripts'][]   =   'packages/guestoffice/script/consulta-animal';


        $this->data['AnimalId']     =   FALSE;
        $this->data['Animal']       =   array();

        



        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Consulta';
        $this->data['PageHeadSubtitle']   = 'Relatório de cadastro de animal.';


        /* Carrega View */
        $this->LoadTemplateGuest('template/guest/cadastro/consulta/formulario-animal', $this->data);

    }






    /**
    * Processar
    *
    * {desc}
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function Processar(){


        $IdPessoa                   =   $_POST['id-pessoa'];


        $Query  =   $this->db->get_where('tb_pessoa', array('pes_id' => $IdPessoa))->result();
        if(!empty($Query)):


            /* Pessoa Fisica */
            if($Query[0]->pes_natureza == 'PF'):

                $ConsultarPessoaFisica                  =   $this->db->get_where('tb_pessoa_fisica', array('fk_pes_id' => $IdPessoa))->result();
                $this->data['PessoaFisica']             =   $ConsultarPessoaFisica;

                $IdPessoaFisica                         =   $this->GetIdPessoaFisica($IdPessoa);
                $this->data['IdPessoaFisica']           =   $IdPessoaFisica;

                $IdPessoaFisicaAtleta                   =   $this->GetIdPessoaFisicaAtleta($this->data['IdPessoaFisica']);
                $this->data['IdPessoaFisicaAtleta']     =   $IdPessoaFisicaAtleta;

            endif;
            // Pessoa Natureza = PF



            // Perfis
            $QueryPerfil    =   "
                                    SELECT * FROM tb_vinculo
                                        WHERE fk_pes1_id = ".$IdPessoa." AND
                                              fk_pes2_id IS NULL AND
                                              fk_ani_id IS NULL AND
                                              fk_per_id IS NOT NULL AND
                                              fk_tip_id IS NULL AND
                                              fk_sta_id IS NOT NULL
                                ";
            $Perfis  =   $this->db->query($QueryPerfil)->result();



            // Telefone
            $this->GetTelefone($IdPessoa);

            // Email
            $this->GetEmail($IdPessoa);
            
            // Perfil Atleta
            $this->PerfilAtleta($Perfis, $IdPessoa, $IdPessoaFisicaAtleta);




        endif;
        // Query



        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Consulta';
        $this->data['PageHeadSubtitle']   = 'Relatório de cadastro de pessoa física e jurídica.';
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Pessoa/BackOffice/ConsultaPessoa';


        /* Enviando Variaveis para a view */
        $this->data['IdPessoa']     =   $IdPessoa;
        $this->data['Pessoa']       =   $Query;
        $this->data['Perfis']       =   $Perfis;



        /* Carrega View */
        $this->LoadTemplateGuest('Template/GuestOffice/Cadastro/Consulta/formulario-pessoa', $this->data);


    }




    /**
    * ProcessarAnimal
    *
    * @author Gustavo Botega 
    */
    public function ProcessarAnimal(){


        if(!isset($_POST['animal-id']))
            $_POST['animal-id'] =   NULL;


        $AnimalId                   =   $_POST['animal-id'];


        $Query  =   $this->db->get_where('tb_animal', array('ani_id' => $AnimalId))->result();
        if(!empty($Query)):



            /*

            // Perfis
            $QueryPerfil    =   "
                                    SELECT * FROM tb_vinculo
                                        WHERE fk_pes1_id = ".$IdPessoa." AND
                                              fk_pes2_id IS NULL AND
                                              fk_ani_id IS NULL AND
                                              fk_per_id IS NOT NULL AND
                                              fk_tip_id IS NULL AND
                                              fk_sta_id IS NOT NULL
                                ";
            $Perfis  =   $this->db->query($QueryPerfil)->result();



            // Telefone
            $this->GetTelefone($IdPessoa);

            // Email
            $this->GetEmail($IdPessoa);
            
            // Perfil Atleta
            $this->PerfilAtleta($Perfis, $IdPessoa, $IdPessoaFisicaAtleta);
            
            */

            $this->VinculosAnimal($AnimalId);
            $this->VinculosProprietarioAnimal($AnimalId);


        endif;
        // Query



        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Consulta';
        $this->data['PageHeadSubtitle']   = 'Relatório de cadastro de atleta.';
        $this->data['PackageScripts'][]   =   'packages/guestoffice/script/consulta-animal';


        /* Enviando Variaveis para a view */
        $this->data['AnimalId']     =   $AnimalId;
        $this->data['Animal']       =   $Query;



        /* Carrega View */
        $this->LoadTemplateGuest('template/guest/cadastro/consulta/formulario-animal', $this->data);

    }






    /**
    * GetTelefone
    *
    * {desc}
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function GetTelefone($IdPessoa){


        // Telefones
        $Telefones  =   $this->db->get_where('tb_telefone', array('fk_peo_id' => $IdPessoa))->result();
        $this->data['Telefones']   =   $Telefones;


    }



    /**
    * GetEmail
    *
    * {desc}
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function GetEmail($IdPessoa){

        // Email
        $Emails  =   $this->db->get_where('tb_email', array('fk_peo_id' => $IdPessoa))->result();
        $this->data['Emails']   =   $Emails;

    }




    /**
    * GetIdPessoaFisica
    *
    * {desc}
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function GetIdPessoaFisica($IdPessoa){
        return $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'fk_pes_id', $IdPessoa, 1, 'pef_id');

    }



    /**
    * GetIdPessoaFisicaAtleta
    *
    * {desc}
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function GetIdPessoaFisicaAtleta($IdPessoaFisica){
        return $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'fk_pef_id', $IdPessoaFisica, 1, 'pfa_id');

    }


    /**
    * PerfilAtleta
    *
    * {desc}
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function PerfilAtleta($Perfis, $IdPessoa, $IdPessoaFisicaAtleta){



        $PerfilAtleta   =   FALSE;
        foreach ($Perfis as $key => $value) :
            if($value->fk_per_id = 5 && $value->fk_sta_id = 1):
                $PerfilAtleta       =   TRUE;
                $this->data['PerfilAtletaDesde']   =   $value->criado;
                $this->data['StatusPerfilAtleta']  =   $value->fk_sta_id;
            endif;
        endforeach;


        if($PerfilAtleta):

            $this->data['FlagRegistroAtleta']   =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'pfa_id', $IdPessoaFisicaAtleta, 1, 'flag_registro');
            $this->data['AtletaEscola']         =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'pfa_id', $IdPessoaFisicaAtleta, 1, 'pfa_escola');


            // Confederacao
            $QueryConfederacao    =   "
                                    SELECT * FROM tb_vinculo
                                        WHERE fk_pes1_id = ".$IdPessoa." AND
                                              fk_pes2_id IS NOT NULL AND
                                              fk_ani_id IS NULL AND
                                              fk_per_id = 2 AND
                                              fk_tip_id = 9 AND
                                              fk_sta_id IS NOT NULL
                                              ORDER BY vin_id DESC;
                                ";
            $Confederacao  =   $this->db->query($QueryConfederacao)->result();
            $this->data['Confederacao']   =   $Confederacao;



            // Federacao
            $QueryFederacao    =   "
                                    SELECT * FROM tb_vinculo
                                        WHERE fk_pes1_id = ".$IdPessoa." AND
                                              fk_pes2_id IS NOT NULL AND
                                              fk_ani_id IS NULL AND
                                              fk_per_id = 3 AND
                                              fk_tip_id = 9 AND
                                              fk_sta_id IS NOT NULL
                                              ORDER BY vin_id DESC;
                                ";
            $Federacao  =   $this->db->query($QueryFederacao)->result();
            $this->data['Federacao']   =   $Federacao;


            // Entidade 
            $QueryEntidade    =   "
                                    SELECT * FROM tb_vinculo
                                        WHERE fk_pes1_id = ".$IdPessoa." AND
                                              fk_pes2_id IS NOT NULL AND
                                              fk_ani_id IS NULL AND
                                              fk_per_id = 4 AND
                                              fk_tip_id = 9 AND
                                              fk_sta_id IS NOT NULL
                                              ORDER BY vin_id DESC;
                                ";
            $Entidade  =   $this->db->query($QueryEntidade)->result();
            $this->data['Entidade']   =   $Entidade;



            /* Confederacao Atual */
            $QueryConfederacaoAtual    =   "
                                    SELECT * FROM tb_vinculo
                                        WHERE fk_pes1_id = ".$IdPessoa." AND
                                              fk_pes2_id IS NOT NULL AND
                                              fk_ani_id IS NULL AND
                                              fk_per_id = 2 AND
                                              fk_tip_id = 9 AND
                                              fk_sta_id IS NOT NULL
                                              ORDER BY vin_id DESC
                                              LIMIT 1;
                                ";
            $ConfederacaoAtual                  =   $this->db->query($QueryConfederacaoAtual)->result();
            $this->data['ConfederacaoAtual']    =   $ConfederacaoAtual;

            /* Federacao Atual */
            $QueryFederacaoAtual    =   "
                                    SELECT * FROM tb_vinculo
                                        WHERE fk_pes1_id = ".$IdPessoa." AND
                                              fk_pes2_id IS NOT NULL AND
                                              fk_ani_id IS NULL AND
                                              fk_per_id = 3 AND
                                              fk_tip_id = 9 AND
                                              fk_sta_id IS NOT NULL
                                              ORDER BY vin_id DESC
                                              LIMIT 1;
                                ";
            $FederacaoAtual                     =   $this->db->query($QueryFederacaoAtual)->result();
            $this->data['FederacaoAtual']       =   $FederacaoAtual;


            /* Entidade Atual */
            $QueryEntidadeAtual    =   "
                                    SELECT * FROM tb_vinculo
                                        WHERE fk_pes1_id = ".$IdPessoa." AND
                                              fk_pes2_id IS NOT NULL AND
                                              fk_ani_id IS NULL AND
                                              fk_per_id = 4 AND
                                              fk_tip_id = 9 AND
                                              fk_sta_id IS NOT NULL
                                              ORDER BY vin_id DESC
                                              LIMIT 1;
                                ";
            $EntidadeAtual                     =   $this->db->query($QueryEntidadeAtual)->result();
            $this->data['EntidadeAtual']       =   $EntidadeAtual;


        endif;

        $this->data['PerfilAtleta']     =   $PerfilAtleta;

    }


    /**
    * VinculosAnimal
    *
    * {desc}
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function VinculosAnimal($AnimalId){


        // Confederacao
        $QueryConfederacao    =   "
                                SELECT * FROM tb_vinculo
                                    WHERE fk_pes1_id IS NOT NULL AND
                                          fk_pes2_id IS NULL AND
                                          fk_ani_id = ".$AnimalId." AND
                                          fk_per_id = 2 AND
                                          fk_tip_id = 10 AND
                                          fk_sta_id IS NOT NULL
                                          ORDER BY vin_id DESC;
                            ";
        $Confederacao  =   $this->db->query($QueryConfederacao)->result();
        $this->data['Confederacao']   =   $Confederacao;



        // Federacao
        $QueryFederacao    =   "
                                SELECT * FROM tb_vinculo
                                    WHERE fk_pes1_id IS NOT NULL AND
                                          fk_pes2_id IS NULL AND
                                          fk_ani_id = ".$AnimalId." AND
                                          fk_per_id = 3 AND
                                          fk_tip_id = 10 AND
                                          fk_sta_id IS NOT NULL
                                          ORDER BY vin_id DESC;
                            ";
        $Federacao  =   $this->db->query($QueryFederacao)->result();
        $this->data['Federacao']   =   $Federacao;


        // Entidade 
        $QueryEntidade    =   "
                                SELECT * FROM tb_vinculo
                                    WHERE fk_pes1_id IS NOT NULL AND
                                          fk_pes2_id IS NULL AND
                                          fk_ani_id = ".$AnimalId." AND
                                          fk_per_id = 4 AND
                                          fk_tip_id = 10 AND
                                          fk_sta_id IS NOT NULL
                                          ORDER BY vin_id DESC;
                            ";
        $Entidade  =   $this->db->query($QueryEntidade)->result();
        $this->data['Entidade']   =   $Entidade;



        /* Confederacao Atual */
        $QueryConfederacaoAtual    =   "
                                SELECT * FROM tb_vinculo
                                        WHERE fk_pes1_id IS NOT NULL AND
                                          fk_pes2_id IS NULL AND
                                          fk_ani_id = ".$AnimalId." AND
                                          fk_per_id = 2 AND
                                          fk_tip_id = 10 AND
                                          fk_sta_id IS NOT NULL
                                          ORDER BY vin_id DESC
                                          LIMIT 1;
                            ";
        $ConfederacaoAtual                  =   $this->db->query($QueryConfederacaoAtual)->result();
        $this->data['ConfederacaoAtual']    =   $ConfederacaoAtual;

        /* Federacao Atual */
        $QueryFederacaoAtual    =   "
                                SELECT * FROM tb_vinculo
                                         WHERE fk_pes1_id IS NOT NULL AND
                                          fk_pes2_id IS NULL AND
                                          fk_ani_id = ".$AnimalId." AND
                                          fk_per_id = 3 AND
                                          fk_tip_id = 10 AND
                                          fk_sta_id IS NOT NULL
                                          ORDER BY vin_id DESC
                                          LIMIT 1;
                            ";
        $FederacaoAtual                     =   $this->db->query($QueryFederacaoAtual)->result();
        $this->data['FederacaoAtual']       =   $FederacaoAtual;


        /* Entidade Atual */
        $QueryEntidadeAtual    =   "
                                SELECT * FROM tb_vinculo
                                    WHERE fk_pes1_id IS NOT NULL AND
                                          fk_pes2_id IS NULL AND
                                          fk_ani_id = ".$AnimalId." AND
                                          fk_per_id = 4 AND
                                          fk_tip_id = 10 AND
                                          fk_sta_id IS NOT NULL
                                          ORDER BY vin_id DESC
                                          LIMIT 1;
                            ";
        $EntidadeAtual                     =   $this->db->query($QueryEntidadeAtual)->result();
        $this->data['EntidadeAtual']       =   $EntidadeAtual;


    }


    /**
    * VinculosProprietarioAnimal
    *
    * @author Gustavo Botega 
    */
    public function VinculosProprietarioAnimal($AnimalId){




        /* Proprietario */
        $QueryProprietario    =   "
                                SELECT * FROM tb_vinculo
                                    WHERE fk_pes1_id IS NOT NULL AND
                                          fk_pes2_id IS NULL AND
                                          fk_ani_id = ".$AnimalId." AND
                                          fk_per_id IS NULL AND
                                          fk_tip_id = 8 AND
                                          fk_sta_id IS NOT NULL
                                          ORDER BY vin_id DESC
                                          LIMIT 1;
                            ";
        $ProprietarioAnimal                 =   $this->db->query($QueryProprietario)->result();
        $this->data['ProprietarioAnimal']        =   $ProprietarioAnimal;


    }






    /**
    * ConsultarHistoricoVinculo
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function ConsultarHistoricoVinculo( $IdVinculo ) // Trocar nome da function para ThemeComponent()
    { 


        $Html    =  '';
        $SqlHistorico    =   "
                                SELECT * FROM tb_vinculo_historico
                                    WHERE fk_vin_id = ".$IdVinculo." 
                                          ORDER BY vih_id DESC
                            ";
        $Historico                     =   $this->db->query($SqlHistorico)->result();



        if(empty($Historico))
            $Html   =   'Nenhum registro encontrado';


        if(!empty($Historico)):

            foreach($Historico as $key => $value){
                $Html   .=   '<tr>';
                    $Html   .=   '<td> '. $value->vih_id .' </td>';
                    $Html   .=   '<td> '. $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $value->fk_aut_id, 1, 'pes_nome_razao_social') .' </td>';
                    $Html   .=   '<td> '. $this->my_data->ConverterData($value->criado, 'ISO', 'PT-BR') .' </td>';


                    $Html   .=   '<td>';
                        $Html   .=   '<span class="label label-sm tooltips label-'.GetBootstrapColor($value->fk_sta_id).' bold label-vinculo-atual">';
                            $Html   .=   '<i></i>';
                            $Html   .=   GetStatusVinculo($value->fk_sta_id);
                        $Html   .=   '</span>';
                    $Html   .=   '</td>';


                    $Html   .=   '<td><a href="#" title=""><i class="fa fa-plus-circle" aria-hidden="true"></i> Detalhes</a> </td>';
                $Html   .=   '</tr>';
            }

        endif;

        echo json_encode($Html);
/*
                                        <span  data-original-title="<?php echo $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $FederacaoAtual[0]->fk_sta_id, 1, 'sta_status'); ?>" >
                                            <i class="fa <?php echo GetIconVinculo($FederacaoAtual[0]->fk_sta_id); ?>" aria-hidden="true"></i>
                                            <?php
                                                echo GetStatusVinculo($FederacaoAtual[0]->fk_sta_id);
                                            ?>
                                        </span>
*/
    }






    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function PackagesClass() // Trocar nome da function para ThemeComponent()
    { 

     // Trocar nome da function para ThemeComponent()


        /* 
            StylesFile 
        */
            /*  Plugins*/
            $this->data['StylesFile']['Plugins']['select2']                = TRUE;
            $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']   = TRUE;
        
            /*  Syles*/
    //            $this->data['StylesFile']['Styles']['profile']                 = TRUE;



        /* 
            ScriptsFile 
        */
            /*  Plugins*/
            $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

            /*  Scripts*/
            $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                            = TRUE;

    }


    /**
    * PackagesAssets
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function PackagesAssets()
    { 
        /* Carregando Estilos */
        $this->data['PackageStyles'][]    =   'packages/guestoffice/style/cadastro-ordinario';


        /* Carregando Scripts */
        $this->data['PackageScripts'][]   =   'packages/guestoffice/script/cadastro-ordinario';
        $this->data['PackageScripts'][]   =   'packages/guestoffice/script/form-wizard';
        $this->data['PackageScripts'][]   =   'packages/guestoffice/script/pessoa';


    }



     /*


            // Empresas
            $QueryEmpresas    =   "
                                    SELECT * FROM tb_vinculo
                                        WHERE fk_pes1_id = ".$IdPessoa." AND
                                              fk_pes2_id IS NOT NULL AND
                                              fk_ani_id IS NULL AND
                                              fk_per_id IS NULL AND
                                              fk_tip_id IN('12','13','14','15','16','17','18','19') AND
                                              fk_sta_id IS NOT NULL
                                ";
            $Empresas  =   $this->db->query($QueryEmpresas)->result();
            $this->data['Empresas']   =   $Empresas;




             // @@@ Implementar permissões por usuario
        
        // AUDITORIA:
            [0]=> Slug Module
            [1]=> Table Database
            [2]=> Column Database
            [3]=> Value Deleted
            [4]=> Folder Thumb
            [5]=> Delete Gallery
            [6]=> Table Gallery
            [7]=> Column Table Gallery
            [8]=> Data/hora
            [9]=> IP
            [10]=> ID Usuário  
            [11]=> Type of Operation  
            */

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

