<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_GuestOffice.php';
class Email extends MY_GuestOffice {

    public $data;

    function __construct() {
        
        parent::__construct();

    }


    /**
    * Processar
    *
    * @author Gustavo Botega 
    */
    public function Processar($Dados, $PessoaId){
        if($this->Validar($Dados['email'])){
            if($this->Gravar($Dados, $PessoaId))
                return true;
        }

        return false;

    } 


    /**
    * Validar
    *
    * @author Gustavo Botega 
    */
    public function Validar($Email){

        return true;

    } 


    /**
    * Gravar
    *
    * @author Gustavo Botega 
    */
    public function Gravar($Dados, $PessoaId){

        $Dataset        =  array(
            'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
            'fk_peo_id'                         =>    $PessoaId,
            'fk_con_id'                         =>    $Dados['email-tipo'],
            'fk_sta_id'                         =>    1,
            'ema_email'                         =>    $Dados['email'],
            'ema_principal'                     =>    $Dados['email-principal'],
            'criado'                            =>    date("Y-m-d H:i:s")
        );


        /* Query */
        $Query = $this->db->insert('tb_email', $Dataset);

        return ($Query) ? true : false;

    } 

}
