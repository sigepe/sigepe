<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_GuestOffice.php';
class Atleta extends MY_GuestOffice {

    public $data;

    function __construct() {
        parent::__construct();


        /* Package */
        $this->PackagesClass(); // TROCAR PARA ThemeComponent();
        $this->PackagesAssets();  // TROCAR PARA SigepeAssets();
        $this->data['TesteClasse']                =   'GuestOffice';



    }

    public function index() {
		$this->Formulario();    	
    }

    public function Formulario(){

        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Cadastro';
        $this->data['PageHeadSubtitle']   = 'Para se cadastrar como atleta no SIGEPE preencha o formulário abaixo.';
        $this->data['Breadcrumbs']        = array();

        $SqlFederacao                     = '
                                                SELECT * FROM tb_pessoa
                                                INNER JOIN tb_vinculo ON tb_pessoa.pes_id = tb_vinculo.fk_pes1_id
                                                WHERE tb_vinculo.fk_per_id = 3 AND 
                                                      tb_pessoa.fk_sta_id = 1 AND
                                                      tb_vinculo.fk_sta_id = 1
                                                ORDER BY pes_id=141 desc, pes_nome_razao_social asc
                                            ';
        $this->data['DatasetFederacao']   = $this->db->query($SqlFederacao)->result();



        /* Entidade Filiada */
        $SqlEntidadeFiliada                     = '
                                                SELECT * FROM tb_pessoa as pes

                                                INNER JOIN tb_vinculo as vin
                                                ON pes.pes_id = vin.fk_pes1_id

                                                INNER JOIN tb_pessoa_juridica as pej
                                                ON pej.fk_pes_id = pes.pes_id


                                                INNER JOIN tb_pessoa_juridica_entidade as pje
                                                ON pje.fk_pej_id = pej.pej_id


                                                WHERE vin.fk_per_id = 4 AND 
                                                vin.fk_sta_id = 1 AND
                                                pes.fk_sta_id = 1 AND
                                                pje.flag_entidade_filiada = 1

                                                ORDER BY pes.pes_nome_razao_social ASC

                                            ';
        $this->data['DatasetEntidadeFiliada']   = $this->db->query($SqlEntidadeFiliada)->result();;


        /* Escola Equitacao */
        $SqlEscolaEquitacao                     = '
                                                SELECT * FROM tb_pessoa as pes

                                                INNER JOIN tb_vinculo as vin
                                                ON pes.pes_id = vin.fk_pes1_id

                                                INNER JOIN tb_pessoa_juridica as pej
                                                ON pej.fk_pes_id = pes.pes_id


                                                INNER JOIN tb_pessoa_juridica_entidade as pje
                                                ON pje.fk_pej_id = pej.pej_id


                                                WHERE vin.fk_per_id = 4 AND 
                                                vin.fk_sta_id = 1 AND
                                                pes.fk_sta_id = 1 AND
                                                pje.flag_escola_equitacao = 1

                                                ORDER BY pes.pes_nome_razao_social ASC
                                            ';
        $this->data['DatasetEscolaEquitacao']   = $this->db->query($SqlEscolaEquitacao)->result();;

        $this->data['CodigoUsuarioMenorIdade']      =   $this->my_pessoa_fisica->GerarCpf('2');




        /* Carrega View */
        $this->LoadTemplateGuest('template/guest/cadastro/atleta/formulario', $this->data);

    }


    /**
    * Processar
    *
    * @author Gustavo Botega 
    */
    public function Processar(){

        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $DataUnserialized);                       
        $this->load->module('cadastro/PessoaFisica');

        $PessoaFisica       =    $this->pessoafisica->Processar(false, $DataUnserialized);

        if(!isset($PessoaFisica['IdPessoa']))
            return 'Erro!';

        
        $Gravar     =   $this->Gravar($PessoaFisica['IdPessoa'], $DataUnserialized);        


    }



    /**
    * Gravar
    *
    * {desc}
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function Gravar($IdPessoa, $DataUnserialized){


    
    }



    /**
    * ValidaFormularioPessoaFisica
    *
    * {desc}
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function ValidaFormulario($DataUnserialized){


        //        $FlagUsuario        =   $DataUnserialized['flag-usuario']; O flag usuario vai ser apenas usado para PF. 

        // Setando erro 
        $Erro = []; 
    
    }




    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function PackagesClass() // Trocar nome da function para ThemeComponent()
    { 

     // Trocar nome da function para ThemeComponent()


        /*
            
            NO SABADO 22/07 fazer com o que a view processe e imprima os valores abaixo.

        */

        /* 
            StylesFile 
        */
            /*  Plugins*/
            $this->data['StylesFile']['Plugins']['select2']                = TRUE;
            $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']   = TRUE;
        
            /*  Syles*/
    //            $this->data['StylesFile']['Styles']['profile']                 = TRUE;



        /* 
            ScriptsFile 
        */
            /*  Plugins*/
            $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

            /*  Scripts*/
            $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                            = TRUE;

    }



    /**
    * PackagesAssets
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function PackagesAssets()
    { 
        /* Carregando Estilos */
        $this->data['PackageStyles'][]    =   'packages/guestoffice/style/cadastro-atleta';


        /* Carregando Scripts */
        $this->data['PackageScripts'][]   =   'packages/guestoffice/script/cadastro-atleta';
        $this->data['PackageScripts'][]   =   'packages/guestoffice/script/cadastro-ordinario';
        $this->data['PackageScripts'][]   =   'packages/guestoffice/script/form-wizard-cadastro-atleta';
        $this->data['PackageScripts'][]   =   'packages/guestoffice/script/pessoa';


    }



     /*
             // @@@ Implementar permissões por usuario
        
        // AUDITORIA:
            [0]=> Slug Module
            [1]=> Table Database
            [2]=> Column Database
            [3]=> Value Deleted
            [4]=> Folder Thumb
            [5]=> Delete Gallery
            [6]=> Table Gallery
            [7]=> Column Table Gallery
            [8]=> Data/hora
            [9]=> IP
            [10]=> ID Usuário  
            [11]=> Type of Operation  
            */

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

