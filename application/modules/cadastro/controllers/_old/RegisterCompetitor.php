<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_GuestOffice.php';

/*

    # Pre-definicoes
        - Competidor: animal ou atleta
        - Empresa: Confederação, Federação, Entidade / Escola 
    
    # Classe responsavel por:
        1) Realizar a gravação dos dados basicos referentes ao competidor.
        2) Gravar o vinculo do competidor com alguma empresa.  
        3) Gravar o historico do vinculo. 
        4) Gravar o Registro do Competitor
        5) Gravar o Historico do Registro
    
*/

class RegisterCompetitor extends MY_GuestOffice {

    public $data;

    function __construct()
    {
        parent::__construct();


        $this->data = array();
        $this->load->module('register');
        $this->data = $this->register->settings();


        $this->load->module('peocom');
        $this->load->module('company/RelationshipCompetitor');
        $this->load->module('entry');


        /* Deletando Session */
        $this->DestroySessionRegisterCompetitor();
    }


    /*
     * RecordCompetitor
     * @author Gustavo Botega 
     * @param     
     * @return json 
    */
    public function RecordCompetitor() {


        /* Unserialize Data */
        parse_str( $_POST['DataSerialized'] , $DataSet);

        /* Criar camada de validacao BackEnd */
        //$this->ValidationForm($DataSet, $_POST)
 
        /* Setando como NULL caso não houver valor vazio */
        foreach ($DataSet as $key => $value) {
            if(empty($value))
                $value = NULL;
            $DataSet[$key]   =   $value;
        }   


        /* Entity / School */
        $AffiliatedEntity               =   $_POST['AffiliatedEntity'];
        $RidingSchool                   =   $_POST['RidingSchool'];


        if( !empty($AffiliatedEntity) || strlen($AffiliatedEntity) > 3 ){
            $TypeEntrySlugEntitySchool  =   'entity';
            $RidingSchool               =   NULL;
        }   

        if( !empty($RidingSchool) || strlen($RidingSchool) > 3 )   {
            $TypeEntrySlugEntitySchool  =   'school';
            $AffiliatedEntity           =   NULL;
        }


        /* Autor - se o usuario estiver autenticado na plataforma o autor do registro e o proprio usuario caso nao e o Guest. */
        if( $this->session->userdata('UserAuthenticated') ){
            $Author     =   $this->session->userdata('UserId');
        }else{
            $Author     =   $_POST['PeopleId'];
        }

        /* TypeCompetitor */
        $TypeCompetitorSlug     =   $_POST['TypeCompetitor'];
        if( $TypeCompetitorSlug == 'athlete'):
            $TypeCompetitor     =   1;
            $DatabaseTable      =   'tb_people_athlete';

        endif;

        if( $TypeCompetitorSlug == 'animal'):
            $TypeCompetitor     =   2;
            $DatabaseTable      =   'tb_animal';

        endif;

        /* DataBulk */
        $DataBulk                   =   $this->HandlingRecordCompetitor( $TypeCompetitorSlug, $DataSet, $_POST );
        $DataBulk['fk_aut_id']      =   $Author;
        $DataBulk['fk_con_id']      =   '37';
        $DataBulk['fk_fed_id']      =   $_POST['Federation'];
        $DataBulk['fk_ent_id']      =   $AffiliatedEntity;
        $DataBulk['fk_sch_id']      =   $RidingSchool;
        $DataBulk['createdAt']      =   date("Y-m-d H:i:s");


        /* Query */
        $query = $this->db->insert( $DatabaseTable, $DataBulk );
        if($query):

            $LastInsertId                           =   $this->db->insert_id();
            $DataBulk['TypeCompetitor']             =   $TypeCompetitor;
            $DataBulk['TypeCompetitorSlug']         =   $TypeCompetitorSlug;
            $DataBulk['LastInsertId']               =   $LastInsertId;
            $DataBulk['TypeEntrySlugEntitySchool']  =   $TypeEntrySlugEntitySchool;
            $DataBulk['AffiliatedEntityId']         =   $AffiliatedEntity;
            $DataBulk['RidingSchoolId']             =   $RidingSchool;

            $Handling   =   $this->Handling( $DataBulk );
            if($Handling):

                $HandlingEntry  =   $this->HandlingEntry( $DataBulk, $DataSet, $_POST['TypeEntry'] );
                if(!$HandlingEntry){
                    $this->RegisterLog( array('Message'=> 'Erro HandlingEntry' ) );                    
                    return false;
                }

                echo json_encode( array('StatusResponse'    =>  TRUE ) );

            endif;

            if(!$Handling):
                    $DataBulk['Message']    =      'Erro Handling';
                    $this->RegisterLog( $DataBulk );                    
            endif;

        endif;

        if(!$query){
            $DataBulk['Message']                    =      '';
            $DataBulk['Author']                     =      $Author;
            $this->RegisterLog($DataBulk);
        }

    }



    /*
     * Handling
     * @author Gustavo Botega 
     * @param     
     * @return boolean  
    */
    public function Handling( array $DataBulk ) {

        /*
            Confederation
        */
        $RelationshipCompanyConfederation       =   $this->relationshipcompetitor->RecordRelationship( 'confederation', $DataBulk, 23, ''  );
        if(!$RelationshipCompanyConfederation):
            $DataBulk['Message']    =      'Erro gravação confederacao.';
            $this->RegisterLog( $DataBulk );
            return false;
        endif;


        /*
            Federation
        */
        $RelationshipCompanyFederation       =   $this->relationshipcompetitor->RecordRelationship( 'federation', $DataBulk, 23, ''  );
        if(!$RelationshipCompanyFederation):
            $DataBulk   =   array();
            $DataBulk['Message']    =      'Erro gravação federacao';
            $this->RegisterLog($DataBulk);
            return false;
        endif;


        /*
            Entity
        */
        if($DataBulk['TypeEntrySlugEntitySchool'] == 'entity'):
            $RelationshipCompanyEntity              =   $this->relationshipcompetitor->RecordRelationship( 'entity', $DataBulk, 23, ''  );
            if(!$RelationshipCompanyEntity):
                $DataBulk['Message']    =      'Erro gravação entidade';
                $this->RegisterLog($DataBulk);
                return false;
            endif;
        endif;



        /*
            School
        */
        if($DataBulk['TypeEntrySlugEntitySchool'] == 'school'):
            $RelationshipCompanySchool       =   $this->relationshipcompetitor->RecordRelationship( 'school', $DataBulk, 23, ''  );
            if(!$RelationshipCompanySchool){
                $DataBulk['Message']    =      'Erro gravação escola';
                $this->RegisterLog($DataBulk);
                return false;
            }
        endif;



        /*
            Peocom
        */
        if($DataBulk['TypeCompetitorSlug'] == 'athlete'):

            $DataBulkPeocom['fk_aut_id']          =      $DataBulk['fk_aut_id'];
            $DataBulkPeocom['fk_peo_id']          =      $DataBulk['fk_peo_id'];
            $DataBulkPeocom['fk_com_id']          =      NULL;
            $DataBulkPeocom['fk_typ_id']          =      3;
            $DataBulkPeocom['fk_sta_id']          =      18;
            $DataBulkPeocom['rel_reference']      =      $DataBulk['LastInsertId'];

            $Peocom       =   $this->peocom->RecordPeocom( $DataBulkPeocom );
            if(!$Peocom):
                $DataBulk['Message']    =      'Erro gravação Peocom: tb_relationship_peocom_type.';
                $this->RegisterLog($DataBulk);
                return false;
            endif;

        endif;


        /*
            FlagRegisterComplete
            - refatorar para aceitar animal.
        */
        if($DataBulk['TypeCompetitorSlug'] == 'athlete'):
            $FlagRegisterComplete       =   $this->UpdateRegisterComplete( $DataBulk );
            if(!$FlagRegisterComplete){
                $DataBulk   =   array();
                $DataBulk['Message']    =      'Erro alteração do flag register complete.';
                $this->RegisterLog($DataBulk);
                return false;
            }
        endif;

        return true;

    }




    /*
     * HandlingEntry
     * @author Gustavo Botega 
     * @param     
     * @return   
    */
    public function HandlingEntry( array $DataBulk, array $DataSet, $TypeEntry ) {

        /*
            Entry
        */
        if( $TypeEntry == '1' ){ // yearly

            $Entry       =   $this->entry->RecordEntry( $DataBulk, $DataSet['listModalityYearly'], $TypeEntry );
            if(!$Entry):
                $DataBulk['Message']    =      'Erro gravação Entry. ID do Atleta:';
                $this->RegisterLog($DataBulk);
                return false;
            endif;

        }

        if( $TypeEntry == '2' || $TypeEntry == '3'){  // cup || single 

                $Entry       =   $this->RecordEntry( $DataBulk, $EntryPrice );
                if(!$Entry){
                    $DataBulk   =   array();
                    $DataBulk['Message']    =      'Erro gravação Entry. ID do Atleta:';
                    $this->RegisterLog($DataBulk);
                    return false;
                }
                $HistoricEntry      =    $this->RecordHistoricEntry( $DataBulk, $Entry );
                if(!$HistoricEntry){
                    $DataBulk   =   array();
                    $DataBulk['Message']    =      'Erro gravação do historico Entry. ID do Atleta:';
                    $this->RegisterLog($DataBulk);
                    return false;
                }

        }

        return true;
    }




 
    /*
     * HandlingRecordCompetitor
     * @author Gustavo Botega 
     * @param     
     * @return  
    */
     public function HandlingRecordCompetitor( $TypeCompetitor, $DataSet, $Post ){

        /*
            Athlete
        */
        if( $TypeCompetitor == 'athlete' ):
            $DataBulk['fk_peo_id']          =   $DataSet['fk_peo_id']; // ID da Pessoa referente ao atleta
            $DataBulk['fk_sta_id']          =   '18'; // Status - Atleta Ativo
            $DataBulk['fk_eny_id']          =   '21'; // Entry
            $DataBulk['fk_typ_id']          =   $_POST['TypeEntry']; // Type Entry - 1 Anual,  2 Copa ou 3 Unica
            $DataBulk['ath_nameCompetitor'] =   $_POST['nameCompetitor']; // Nome de Competicao do Atleta
            $DataBulk['ath_entryCbh']       =   $_POST['entryCbh']; // N CBH
            $DataBulk['ath_entryFei']       =   $_POST['entryFei']; // N FEI
        endif;


        /*
            Animal
        */
        if( $TypeCompetitor == 'animal' ):


            /* Type Gender */ ( !isset( $DataSet['type-gender'] ) ) ? $TypeGender = NULL : $TypeGender = $DataSet['type-gender'] ;
            /* Nationality */ $NationalityId    =   $this->model_crud->get_rowSpecific('tb_nationality', 'nat_acronym', $DataSet['nationality'], 1, 'nat_id');

            $DataBulk['fk_rac_id']              =   $DataSet['race']; // Raca do Animal
            $DataBulk['fk_fur_id']              =   $DataSet['fur']; // Raca do Animal
            $DataBulk['fk_gen_id']              =   $DataSet['gender']; // Genero 1 - Macho  2 - Femea
            $DataBulk['fk_typ_id']              =   $TypeGender; // Tipo do Genero  1 - Inteiro   2 - Castrado
            $DataBulk['fk_nat_id']              =   $NationalityId; 
            $DataBulk['fk_sta_id']              =   29; // ID Status - 29 Animal Ativo 
            $DataBulk['ani_fullName']           =   $DataSet['full-name']; // Nome Completo do Animal 
            $DataBulk['ani_sponsoredName']      =   $DataSet['sponsored-name']; // Nome Patrocinado 
            $DataBulk['ani_fei']                =   $DataSet['fei']; // N Fei 
            $DataBulk['ani_cbh']                =   $DataSet['cbh']; // N CBH 
            $DataBulk['ani_weight']             =   $DataSet['weight']; // Peso do Animal ( em kg ) 
            $DataBulk['ani_coverFei']           =   $DataSet['cover-fei']; // N Capa FEI 
            $DataBulk['ani_crossHeight']        =   $DataSet['cross-height']; // Altura da Cruz do Animal 
            $DataBulk['ani_nameFather']         =   $DataSet['name-father']; // Nome do Pai
            $DataBulk['ani_nameMother']         =   $DataSet['name-mother']; // Nome da Mãe
            $DataBulk['ani_maternalGrandfather']=   $DataSet['maternal-grand-father']; // Nome do Avo Paterno
            $DataBulk['ani_chip']               =   $Post['Chip']; // N Chip do Animal
            $DataBulk['ani_genealogicalRecord'] =   $DataSet['genealogical-record']; // N Registro Genealogico
            $DataBulk['ani_birthDate']          =   $this->my_date->date( $Post['BirthDate'], 'ptbr', 'convertPtbrToIso' ); // Data de Nascimento
            $DataBulk['ani_note']               =   NULL; // Observacoes

        endif;
        
        return $DataBulk;
    }









    /*
     * UpdateRegisterComplete
     * 
     * 
     *
     * @author Gustavo Botega 
     * @param     
     * @return boolean  retorna TRUE se o CPF existir
    */
    public function UpdateRegisterComplete( array $DataBulk) {



        /* Query */
        $data = array(
                       'flag_registerComplete' => 1
                    );

        $this->db->where('peo_id', $DataBulk['fk_peo_id']);
        $query = $this->db->update('tb_people', $data); 


        return true;
    }

        
    /**
     * DestroyAuthenticateOwner
     *
     *
     * @author Gustavo Botega 
     * @param int $userId -    
     * @return TRUE 
     */      
    public function DestroySessionRegisterCompetitor()
    {
        $DataSession = array(
            'PeopleId'                          =>      NULL,     
        );
        $this->session->set_userdata($DataSession); 

    }



    /*
     * GetDropdownTypeEntry
     * @author Gustavo Botega 
     * @param     
     * @return boolean 
    */      
    public function GetDropdownTypeEntry() {

        $query = $this->model_crud->select(
                    'tb_type_entry',
                    array('typ_id', 'typ_title', 'typ_position', 'typ_slug'),
                    array(),
                    array('typ_position'=>'asc'),
                    NULL
                );

        return $this->mountingDropdownToCodeigniter($query, 'Selecione um Tipo de Participação', array('value'=>'typ_id', 'data-athletetypeentryslug'=>'typ_slug', 'data-identificationFederation'=>'typ_id'), array('typ_title'=>''), 'athleteTypeEntry', 'class="form-control select2" id="athleteTypeEntry" ');        
    }







    /*
     * DropdownTypeEntrySlug
     * @author Gustavo Botega 
     * @param     
     * @return boolean 
    */      
    public function DropdownTypeEntrySlug($typeEntry, $TypeCompetitor = 1) {

        switch ($typeEntry) {
            case 'yearly':
                $idTypeEntry  =   '1';
               break;
            
            case 'cup':
                $idTypeEntry  =   '2';
            break;
            
            case 'single':
                $idTypeEntry  =   '3';
               break;
        }

        switch ($TypeCompetitor) {
            case 'athlete':
                $TypeCompetitor  =   '1';
               break;
            
            case 'animal':
                $TypeCompetitor  =   '2';
            break;
            
            default:
                $TypeCompetitor  =   '1';
               break;
        }



        /*---------------------------------
            QUERY - TYPE ENTRY PRICE
        -----------------------------------*/
       $query = "
            SELECT m.mod_id, m.mod_title, m.mod_slug, m.mod_status,
                   p.pri_id, p.fk_typ_id, p.fk_mod_id, p.pri_before, p.pri_after, p.pri_status, p.fk_com_id,
                   e.typ_id
            FROM tb_type_entry_price              as p
            INNER JOIN tb_modality              as m ON     p.fk_mod_id = m.mod_id    
            INNER JOIN tb_type_entry_competitor   as c ON     p.fk_com_id = c.com_id     
            INNER JOIN tb_type_entry             as e ON     p.fk_typ_id = e.typ_id      
            WHERE 
                p.pri_status = 1 AND
                m.mod_status = 1 AND
                p.fk_typ_id = ".$idTypeEntry." AND
                p.fk_com_id = ".$TypeCompetitor."
        ";        
        $query = $this->db->query($query)->result();


        /*---------------------------------
            QUERY DATE LIMIT
        -----------------------------------*/
        $typeEntryDateLimit = $this->model_crud->select(
                    'tb_type_entry_date_limit',
                    array('*'),
                    array('lim_year =' => date('Y'), 'fk_sta_id =' => '1'),
                    array(),
                    1   
                );
        $dateLimit    =   $typeEntryDateLimit[0]->lim_dateLimit;


        /*---------------------------------
            CHECKING DATE LIMIT 
        -----------------------------------*/
        $dateCurrent     =   date("Y-m-d");
        if( strtotime($dateCurrent) > strtotime($dateLimit) ){
            // passou a data limite
            $dateLimitColumn  =   'pri_after';
        }else{
            // nao passou a data limite
            $dateLimitColumn  =   'pri_before';
        }





        /*---------------------------------
            FOREACH 
        -----------------------------------*/
        foreach ($query as $key => $value) {
            $price  =   '';
           $arrAux[$key]['mod_slug'] =   $value->mod_slug;
           $arrAux[$key]['mod_title'] =   $value->mod_title;
           $arrAux[$key]['pri_id']    =   $value->pri_id;

           /* Environment Variables - Scope Function */
           $idModality          =   $value->mod_id;
           $idTypeEntry         =   $value->typ_id;
           $idTypeCompetitor    =   $value->fk_com_id;


        /*---------------------------------
            QUERY DISCOUNTS
            - Leitura da Query:
            Banco de Dados, traga todos os descontos de acordo com:
            a modalidade, tipo de competidor e tipo de registro. 
            Exiba somente os descontos do mes corrente.
            Exiba somente as modalidades atidadades.
            Exiba somente de competidores tipo atleta
        -----------------------------------*/
       $queryTypeEntryMonthDiscount = "
            SELECT *
            FROM tb_type_entry_month_discount   as d
            INNER JOIN tb_modality              as m    ON d.fk_mod_id = m.mod_id /* modalidade */
            INNER JOIN tb_type_entry_competitor as c    ON d.fk_com_id = c.com_id /* competidor ( pessoa ou atleta ) */
            INNER JOIN tb_type_entry            as e    ON d.fk_typ_id = e.typ_id /* tipo de registro */
            WHERE 
                d.fk_mod_id = '".$idModality."' AND
                m.mod_status = 1 AND
                d.fk_typ_id = '".$idTypeEntry."' AND /* ser dinamico esse fk_typ_id */
                d.fk_com_id = '".$idTypeCompetitor."' AND
                month(d.dis_month) = month(curdate())   /* selecionando apenas os descontos para o mes atual */
        ";
        $queryMonthDiscount = $this->db->query($queryTypeEntryMonthDiscount)->result();

            $discountReal   =   0;
            if(!empty($queryMonthDiscount)){
            /*
                se passar nesse bloco quer dizer que existe algum desconto
                [ nao permitir o admin cadastrar descontos iguais . criterios ( tipo de registro, modalidade e tipo de pessoa )]
                [ se tiver desconto, tratar caso quando o valor do preco for zero ]
                [ nesse bloco de if pode ter apenas um registro ]
            */

            $discountPercentage     =   $queryMonthDiscount[0]->dis_discountPercentage;
            $discountReal         =    ( $discountPercentage / 100 ) * $value->$dateLimitColumn;
        }



        /*---------------------------------
            DISCOUNTS PER DATE ( AFTER, BEFORE )
        -----------------------------------*/
           $arrAux[$key]['pri_price'] =   $value->$dateLimitColumn - $discountReal;
        }

        return $arrAux;        
    }



}
