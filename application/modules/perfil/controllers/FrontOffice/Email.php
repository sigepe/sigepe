<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';
class Email extends MY_FrontOffice {

    public $data;

    function __construct() {

        parent::__construct();

        $this->ThemeComponent(); 
        $this->SigepeAsset(); 

        $this->data['NavActiveSidebar']   = 'Email';
        $this->data['ShowColumnLeft'] = "ProfilePessoa";



    }


    /**
    * Index
    *
    * @author Gustavo Botega 
    */
    public function index() {
		$this->Formulario();    	
    }


    /**
    * Formulario
    *
    * @author Gustavo Botega 
    */
    public function Formulario(){

        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Meu Perfil';
        $this->data['PageHeadSubtitle']   = 'Visualize e gerencie seu perfil. Mantenha seus dados sempre atualizados.';
        $this->data['Breadcrumbs']        = array();


        $PessoaId                           =   $this->session->userdata('PessoaId');
        $PessoaFisicaId                     =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'fk_pes_id', $PessoaId, 1, 'pef_id');
        $this->data['PessoaId']             =   $PessoaId;        
        $this->data['PessoaFisicaId']       =   $PessoaFisicaId;        


        $SqlEmail      =   "
                                    SELECT * FROM tb_email as ema
                                    WHERE fk_peo_id = '".$PessoaId."' AND
                                    flag_deletado IS NULL
                                    ORDER BY ema_principal DESC, ema_id DESC
                                ";
        $this->data['DatasetEmail']      =   $this->db->query($SqlEmail)->result();        


        $SqlTipoEmail      =   "
                                    SELECT * FROM tb_contato 
                                        WHERE con_id IN (1,3)
                                    ORDER BY con_id ASC
                                ";
        $this->data['DatasetTipoEmail']      =   $this->db->query($SqlTipoEmail)->result();        



        $this->data['PackageStyles'][]    =   'Packages/Styles/Perfil/FrontOffice/Email';
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Perfil/FrontOffice/Email';


        /* Carrega View */
        $this->LoadTemplate('Template/FrontOffice/sistema/Perfil/Email', $this->data);

    }


    /**
    * AjaxProcessar
    *
    * @author Gustavo Botega 
    */
    public function AjaxProcessar(){


        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);    

        $ReturnAjax     =   array();

        $PrimeiroEmail   =   $this->VerificaPrimeiroEmail();


        $Processar  =   $this->Processar($Dados);
        if($Processar):

            $ReturnAjax['EmailId']   =   $Processar;
            $ReturnAjax['Criado']       =   date("d/m/Y") . " às " . date("H:i");
            $ReturnAjax['Autor']        =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $this->session->userdata('PessoaId'), 1, 'pes_nome_razao_social' );
            $ReturnAjax['Email']        =   $Dados['email'];
            $ReturnAjax['Tipo']         =   $this->model_crud->get_rowSpecific('tb_contato', 'con_id', $Dados['tipo'], 1, 'con_contato');;
            $TableRow                   =   $this->TableRow($ReturnAjax, $PrimeiroEmail);
            $ReturnAjax['TableRow']     =   $TableRow;

        endif;

        if(!$Processar)
            $ReturnAjax     =   false;


        echo json_encode($ReturnAjax);

    }



    /**
    * Processar
    *
    * @author Gustavo Botega 
    */
    public function Processar($Dados = false){

        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);                       


        // Validacao Formulario
        $Validar                                    =   $this->Validar($Dados);
        if(!$Validar)
            return false;


        $Gravar     =   $this->Gravar($Dados);
        if(!$Gravar)
            return false;



        // Cadastro atleta finalizado com sucesso
        return $Gravar;

    }


    /**
    * Validar
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function Validar($Dados){

        // Email

        // Tipo


        return true;

    }



    /**
    * Gravar
    *
    * @author Gustavo Botega 
    */
    public function Gravar($Dados){

        foreach ($Dados as $key => $value) {
            if(empty($value))
                $Dados[$key]    =   NULL;
        }



        /* Setando Flag */
        $FlagEmailPrincipal  =   NULL;

        /*
            Checa se e o primeiro email a ser cadastrado para marcar com o Flag de Email Principal.
            Se for o primeiro logicamente a ser cadastrado o valor do flag e setado como true para o email ser o principal.
        */
        if( $this->VerificaPrimeiroEmail() )
            $FlagEmailPrincipal = 1;


        // Dados
        $Dataset       =  array(
            'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
            'fk_peo_id'                         =>    $this->session->userdata('PessoaId'),
            'fk_sta_id'                         =>    1,
            'fk_con_id'                         =>    $Dados['tipo'], // tipo do email
            'ema_email'                         =>    $Dados['email'],
            'ema_principal'                     =>    $FlagEmailPrincipal,
            'criado'                            =>    date("Y-m-d H:i:s")
        );

 
       /* Query */
       $Query = $this->db->insert('tb_email', $Dataset);

       return ($Query) ? $this->db->insert_id() : false;
    
    }


    /**
    * TableRow
    *
    * @author Gustavo Botega 
    */
    public function TableRow($Dados, $PrimeiroEmail){


        $EmailId                    =     $Dados['EmailId'];
        $Tipo                       =     $Dados['Tipo'];
        $Email                      =     $Dados['Email'];
        $Criado                     =     $Dados['Criado'];
        $Autor                      =     $Dados['Autor'];

        $FlagEmailPrincipal         =     '';


        if($PrimeiroEmail):
        $FlagEmailPrincipal      =     '
                                            <a href="javascript:;"
                                                class="tooltips badge badge-warning bold flag-email-principal"
                                                title=""
                                                data-original-title="E-mail Principal. Preferencialmente vamos utilizar esse e-mail para entrar em contato com você.">
                                                P
                                            </a>        
                                          ';
        endif;

        $Detalhes       =   "
                                <small> 

                                    <b>Cadastrado em:</b><br>
                                    ".$Criado."
                                    <hr style='margin: 7px 0;'>

                                    <b>Por:</b><br>
                                        ".$Autor." <br>

                                </small> 
        ";


        $TableRow     =   '
                            <tr id="table-row-'.$EmailId.'" data-id="'.$EmailId.'">
                                
                                <td class="text-center">

                                    '.$FlagEmailPrincipal.'

                                    <span class="bold">'.$Tipo.':</span>
                                     '.$Email.'
                                </td>
                                
                                <td class="text-center">

                                    <span class="btn btn-circle btn-sm btn-success btn-gerenciar-conjunto popovers" data-container="body" onclick=" " data-html="true" data-trigger="hover" data-placement="left" data-content="
                                                '.$Detalhes.'
                                        " data-original-title="Detalhes">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        Detalhes
                                    </span>
                            ';


        if(!$PrimeiroEmail):
        $TableRow      .=   '
                                    <a href="javascript:;"
                                        class="btn btn-circle btn-sm btn-warning btn-email-principal tooltips"
                                        data-id="'.$EmailId.'"
                                        data-original-title="Transformar esse email como principal."
                                    >
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                        Email Principal
                                    </a>
                                    
                                    <a href="javascript:;" class="btn btn-circle btn-sm btn-danger btn-deletar-email" data-id="'.$EmailId.'">
                                        <i class="fa fa-close" aria-hidden="true"></i>
                                        Deletar Email
                                    </a>
                            ';
        endif;



        $TableRow      .=   '
                                </td>
                            </tr>
        ';

        return $TableRow;

    }





    /**
    * VerificaEmail
    *
    * Funcao checa se o email que esta sendo requisitado pertence aquela pessoa
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function VerificaEmail($EmailId, $Acao = NULL){


        $PessoaId       =   $this->session->userdata('PessoaId');


        if(is_null($Acao)):
        $SqlEmail       =   "
                                    SELECT * FROM tb_email as ema
                                    WHERE fk_peo_id = '".$PessoaId."' AND
                                    flag_deletado IS NULL AND
                                    ema_id = '".$EmailId."'
                                ";
        endif;


        if($Acao == 'Deletar'):
        $SqlEmail      =   "
                                    SELECT * FROM tb_email as ema
                                    WHERE fk_peo_id = '".$PessoaId."' AND
                                    flag_deletado IS NULL AND
                                    ema_principal IS NULL AND
                                    fk_aut2_id IS NULL AND
                                    ema_id = '".$EmailId."'
                                ";
        endif;


        if($Acao == 'EmailPrincipal'):
        $SqlEmail      =   "
                                    SELECT * FROM tb_email as ema
                                    WHERE fk_peo_id = '".$PessoaId."' AND
                                    flag_deletado IS NULL AND
                                    ema_principal IS NULL AND
                                    fk_aut2_id IS NULL AND
                                    ema_id = '".$EmailId."'
                                ";
        endif;



        $Query              =   $this->db->query($SqlEmail)->result();       


        if(empty($Query))
            return false;

        return true;

    }



    /**
    * VerificaPrimeiroEmail
    *
    * Funcao retorna true se o email for o primeiro a ser cadastrado para a pessoa e false se ja exister
    * outros emails ativos logicamente.
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function VerificaPrimeiroEmail(){


        $PessoaId       =   $this->session->userdata('PessoaId');

        $SqlEmail       =   "
                                    SELECT * FROM tb_email as ema
                                    WHERE fk_peo_id = '".$PessoaId."' AND
                                    flag_deletado IS NULL AND
                                    fk_aut2_id IS NULL
                                ";
        $Query              =   $this->db->query($SqlEmail)->result();       


        if(empty($Query)) // Se nao retornar nenhum registra significa que para o cadastro sera o primeiro registro logico para essa pessoa
            return true;

        return false;

    }





    /**
    * Deletar
    *
    * Funcao responsavel por email
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function Deletar(){

        $PessoaId                   =   $this->session->userdata('PessoaId');
        $EmailId                    =   $_POST['EmailId'];
        $Modificado                 =   date('Y-m-d H:i:s');

        $VerificaEmail              =   $this->VerificaEmail($EmailId, 'Deletar');
        if(!$VerificaEmail){
            echo json_encode(false);
            return false;
        }


        $SqlEmail                   =   "   
                                            UPDATE tb_email
                                            SET fk_aut2_id=".$PessoaId.",
                                                flag_deletado = 1,
                                                modificado = '".$Modificado."'

                                            WHERE ema_id= ".$EmailId." 
                                    ";
        $Query                      =   $this->db->query($SqlEmail);        
        if(!$Query)
            echo json_encode(false);

        echo json_encode(true);

    }




    /**
    * EmailPrincipal
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function EmailPrincipal(){


        $PessoaId                   =   $this->session->userdata('PessoaId');
        $EmailId                    =   $_POST['EmailId'];
        $Modificado                 =   date('Y-m-d H:i:s');

        $VerificaEmail              =   $this->VerificaEmail($EmailId, 'EmailPrincipal');
        if(!$VerificaEmail){
            echo json_encode(false);
            return false;
        }

        $SqlEmail                   =   "   
                                            UPDATE tb_email 
                                                SET ema_principal = NULL

                                            WHERE fk_peo_id= ".$PessoaId." 

                                    ";
        $Query                      =   $this->db->query($SqlEmail);        
        if(!$Query)
            echo json_encode(false);


        $SqlEmail                   =   "   
                                            UPDATE tb_email
                                            SET 
                                                ema_principal = '1'

                                            WHERE ema_id= ".$EmailId." 

                                    ";
        $Query                      =   $this->db->query($SqlEmail);        
        if(!$Query)
            echo json_encode(false);

        echo json_encode(true);

    }







    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    */
    public function ThemeComponent(){ 


        /* 
            StylesFile 
        */
            /*  Plugins*/
            $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
            $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete']                           = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                    = TRUE;
            $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;
        
            /*  Syles*/
               $this->data['StylesFile']['Styles']['profile']                                = TRUE;



        /* 
            ScriptsFile 
        */
            /*  Plugins*/
            $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
            $this->data['ScriptsFile']['Plugins']['easy-autocomplete']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

            /*  Scripts*/
            $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                            = TRUE;

    }



    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function SigepeAsset()
    { 
        /* Carregando Estilos */


        /* Carregando Scripts */

    }



     /*
             // @@@ Implementar permissões por usuario
        
        // AUDITORIA:
            [0]=> Slug Module
            [1]=> Table Database
            [2]=> Column Database
            [3]=> Value Deleted
            [4]=> Folder Thumb
            [5]=> Delete Gallery
            [6]=> Table Gallery
            [7]=> Column Table Gallery
            [8]=> Data/hora
            [9]=> IP
            [10]=> ID Usuário  
            [11]=> Type of Operation  
            */

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

