<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';
//require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
class MeuPerfil extends MY_FrontOffice {

    public $data;

    function __construct() {

        parent::__construct();

        $this->ThemeComponent(); 
        $this->SigepeAsset(); 
        $this->data['ShowColumnLeft'] = "ProfilePessoa";


    }


    /**
    * Index
    *
    * @author Gustavo Botega 
    */
    public function index() {
        $this->MeuPerfil();
    }




    /**
    * MeuPerfil
    *
    * @author Gustavo Botega 
    */
    public function MeuPerfil(){



        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Meu Perfil';
        $this->data['PageHeadSubtitle']   = 'Visualize e gerencie seu perfil. Mantenha seus dados sempre atualizados.';
        $this->data['NavActiveSidebar']   = 'MeuPerfil';

        $this->MeuPerfilDataset();  // Carrega os valores dos selects


        $PessoaId                           =   $this->session->userdata('PessoaId');
        $PessoaFisicaId                     =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'fk_pes_id', $PessoaId, 1, 'pef_id');
        $this->data['PessoaId']             =   $PessoaId;        
        $this->data['PessoaFisicaId']       =   $PessoaFisicaId;        


        $this->data['NomeCompleto']         =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $PessoaId, 1, 'pes_nome_razao_social');
        $this->data['Apelido']              =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'pef_id', $PessoaFisicaId, 1, 'pef_apelido');
        $this->data['DataDeNascimento']     =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $PessoaId, 1, 'pes_data_nascimento_fundacao');
        $this->data['Genero']               =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'pef_id', $PessoaFisicaId, 1, 'fk_gen_id');
        $this->data['GeneroId']             =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'pef_id', $PessoaFisicaId, 1, 'fk_gen_id');
        
        $this->data['EstadoCivilId']        =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'pef_id', $PessoaFisicaId, 1, 'fk_est_id');
        $this->data['EstadoCivil']          =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_estadocivil', 'pfe_id', $this->data['EstadoCivilId'], 1, 'pfe_estado_civil');

        $this->data['NacionalidadeId']      =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'pef_id', $PessoaFisicaId, 1, 'fk_nac_id');
        $this->data['Nacionalidade']        =   $this->model_crud->get_rowSpecific('tb_pais', 'pai_id', $this->data['NacionalidadeId'], 1, 'pai_pais');

        $this->data['Naturalidade']         =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'pef_id', $PessoaFisicaId, 1, 'pef_naturalidade');

        $this->data['TipoSanguineoId']      =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'pef_id', $PessoaFisicaId, 1, 'fk_tip_id');
        $this->data['TipoSanguineo']        =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_tiposanguineo', 'pft_id', $this->data['TipoSanguineoId'], 1, 'pft_tipo_sanguineo');
 
        $this->data['EscolaridadeId']       =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'pef_id', $PessoaFisicaId, 1, 'fk_esc_id');
        $this->data['Escolaridade']         =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_escolaridade', 'pfe_id', $this->data['EscolaridadeId'], 1, 'pfe_escolaridade');

        $this->data['NomeDoPai']            =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'pef_id', $PessoaFisicaId, 1, 'pef_nome_pai');
        $this->data['NomeDaMae']            =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'pef_id', $PessoaFisicaId, 1, 'pef_nome_mae');
        $this->data['CPF']                  =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $PessoaId, 1, 'pes_cpf_cnpj');
//        $this->data['CPFIsento']            =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'pef_id', $PessoaFisicaId, 1, 'flag_cpf_isento');



       $this->data['PackageStyles'][]    =   'Packages/Styles/Perfil/FrontOffice/MeuPerfil';
       $this->data['PackageScripts'][]   =   'Packages/Scripts/Perfil/FrontOffice/MeuPerfil';


        /* Carrega View */
        $this->LoadTemplate('Template/FrontOffice/sistema/Perfil/MeuPerfil', $this->data);

    }





    /**
    * MeuPerfilDataset
    *
    * @author Gustavo Botega 
    */
    public function MeuPerfilDataset(){


        /* Genero */
        $QueryGenero    =   "
                                SELECT * FROM tb_pessoa_fisica_genero
                                    ORDER BY pfg_id ASC 
                            ";
        $this->data['DatasetGenero']  =   $this->db->query($QueryGenero)->result();


        /* Estado Civil */
        $QueryEstadoCivil    =   "
                                SELECT * FROM tb_pessoa_fisica_estadocivil
                                    ORDER BY pfe_posicao ASC 
                            ";
        $this->data['DatasetEstadoCivil']  =   $this->db->query($QueryEstadoCivil)->result();


        /* Nacionalidade */
        $QueryNacionalidade    =   "
                                SELECT * FROM tb_pais
                                    ORDER BY pai_posicao ASC 
                            ";
        $this->data['DatasetNacionalidade']  =   $this->db->query($QueryNacionalidade)->result();


        /* Naturalidade */
        $QueryNaturalidade    =   "
                                SELECT * FROM tb_municipio
                                    ORDER BY mun_id ASC 
                            ";
        $this->data['DatasetNaturalidade']  =   $this->db->query($QueryNaturalidade)->result();


        /* Tipo Sanguineo */
        $QueryTipoSanguineo    =   "
                                SELECT * FROM tb_pessoa_fisica_tiposanguineo
                                    ORDER BY pft_id ASC 
                            ";
        $this->data['DatasetTipoSanguineo']  =   $this->db->query($QueryTipoSanguineo)->result();


        /* Escolaridade */
        $QueryEscolaridade    =   "
                                SELECT * FROM tb_pessoa_fisica_escolaridade
                                    ORDER BY pfe_id ASC 
                            ";
        $this->data['DatasetEscolaridade']  =   $this->db->query($QueryEscolaridade)->result();

    }



    /**
    * AjaxProcessar
    *
    * @author Gustavo Botega 
    */
    public function AjaxProcessar(){


        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);    

        $Processar  =   $this->Processar(true, $Dados);


        echo json_encode($Processar);

    }




    /**
    * Processar
    *
    * @author Gustavo Botega 
    */
    public function Processar($Ajax = false, $AjaxDados = false){

        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);                       

        if($AjaxDados)
            $Dados  =   $AjaxDados;


        // Validacao Formulario
        $Validar                                    =   $this->Validar($Dados);
        if(!$Validar)
            return false;


        $Gravar                                     =   $this->Gravar($Dados);

        if(!$Gravar)
            return false;


        // Cadastro atualizado
        return true;

    }



    /**
    * Validar
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function Validar($Dados){

        // Apelido

        // Estado Civil

        // Nacionalidade

        // Naturalidade

        // Tipo Sanguineo

        // Escolaridade

        // Nome Pai

        // Nome Mae

        return true;

    }




    /**
    * Gravar
    *
    * @author Gustavo Botega 
    */
    public function Gravar($Dados){


        foreach ($Dados as $key => $value) {
            if(empty($value))
                $Dados[$key]    =   NULL;
        }


        // Dados
        $Dataset       =  array(
            'pef_apelido'                       =>    $Dados['apelido'], // Apelido
            'fk_nac_id'                         =>    $Dados['nacionalidade'], // Nacionalidade
            'fk_gen_id'                         =>    $Dados['genero'], // Nacionalidade
            'fk_esc_id'                         =>    $Dados['escolaridade'], // Escolaridade
            'fk_tip_id'                         =>    $Dados['tipo-sanguineo'], // Tipo Sanguineo
            'pef_naturalidade'                  =>    $Dados['naturalidade'], // Naturalidade
            'fk_est_id'                         =>    $Dados['estado-civil'], // Estado Civil
            'pef_nome_pai'                      =>    $Dados['nome-pai'], // Nome Pai
            'pef_nome_mae'                      =>    $Dados['nome-mae'], // Nome Mae
            'modificado'                        =>    date("Y-m-d H:i:s")
        );

 
        $PessoaId                           =   $this->session->userdata('PessoaId');
        $PessoaFisicaId                     =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'fk_pes_id', $PessoaId, 1, 'pef_id');

        /* Query */
        $Query = $this->db->update('tb_pessoa_fisica', $Dataset, "pef_id = " . $PessoaFisicaId);



        // Dados
        $Dataset       =  array(
            'pes_nome_razao_social'             =>    $Dados['nome-completo'], // Nome Completo
            'pes_data_nascimento_fundacao'      =>    $this->my_data->ConverterData($Dados['data-nascimento'], 'PT-BR', 'ISO'), // Nome Completo
            'modificado'                        =>    date("Y-m-d H:i:s")
        );

        $Query = $this->db->update('tb_pessoa', $Dataset, "pes_id = " . $PessoaId);




        return ($Query) ? true : false;

    }





    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    */
    public function ThemeComponent(){ 


        /* 
            StylesFile 
        */
            /*  Plugins*/
            $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
            $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete']                           = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                    = TRUE;
            $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;
        
            /*  Syles*/
               $this->data['StylesFile']['Styles']['profile']                                   = TRUE;



        /* 
            ScriptsFile 
        */
            /*  Plugins*/
            $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
            $this->data['ScriptsFile']['Plugins']['easy-autocomplete']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

            /*  Scripts*/
            $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                            = TRUE;

    }



    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function SigepeAsset()
    { 
        /* Carregando Estilos */
//        $this->data['PackageStyles'][]    =   'Packages/Styles/Animal/FrontOffice/Cadastro';


        /* Carregando Scripts */
//        $this->data['PackageScripts'][]   =   'Packages/Scripts/Animal/FrontOffice/Animal';



        /* Carregando Scripts */
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Perfil/FrontOffice/MeuPerfil';



    }



     /*
             // @@@ Implementar permissões por usuario
        
        // AUDITORIA:
            [0]=> Slug Module
            [1]=> Table Database
            [2]=> Column Database
            [3]=> Value Deleted
            [4]=> Folder Thumb
            [5]=> Delete Gallery
            [6]=> Table Gallery
            [7]=> Column Table Gallery
            [8]=> Data/hora
            [9]=> IP
            [10]=> ID Usuário  
            [11]=> Type of Operation  
            */

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

