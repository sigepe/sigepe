<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';

//require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
class Atleta extends MY_FrontOffice {

    public $data;

    function __construct() {

        parent::__construct();

        $this->ThemeComponent(); 
        $this->SigepeAsset(); 


        $PessoaId                               =   $this->session->userdata('PessoaId');
        $PessoaFisicaId                         =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'fk_pes_id', $PessoaId, 1, 'pef_id');
        $PessoaFisicaAtletaId                   =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'fk_pef_id', $PessoaFisicaId, 1, 'pfa_id');
        $this->data['PessoaId']                 =   $PessoaId;        
        $this->data['PessoaFisicaId']           =   $PessoaFisicaId;        
        $this->data['PessoaFisicaAtletaId']     =   $PessoaFisicaAtletaId;        
        $this->data['DatasetPagarmeCheckout']   =   TRUE;        

        
        
    }


    /**
    * Index
    *
    * @author Gustavo Botega 
    */
    public function index() {
        $this->Dashboard();
    }





    /**
    * Formulario
    *
    * @author Gustavo Botega 
    */
    public function Dashboard(){


        header('Access-Control-Allow-Origin: *');


        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Atleta';
        $this->data['PageHeadSubtitle']   = 'Visualize e gerencie seu perfil. Mantenha seus dados sempre atualizados.';
        $this->data['Breadcrumbs']        = array();
        $this->data['NavActiveSidebar']   = 'Atleta';





        $this->AtletaDatasetVinculos();  // Carrega dados
        $this->DatasetPagarme();  // Carrega dados para serem processados no gateway
        $this->AtletaDatasetRegistrosAtivos();  // Carrega dados
        $this->PermissaoAtletaNovoRegistro();  // Carrega dados
        $this->AtletaDatasetFederacaoEntidade( $this->data['PessoaId'] ,  $this->data['PessoaFisicaId'], $this->data['PessoaFisicaAtletaId']);
        


        $this->data['RegistroCbh']              =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'pfa_id', $this->data['PessoaFisicaAtletaId'], 1, 'pfa_registro_cbh');        
        $this->data['RegistroFei']              =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'pfa_id', $this->data['PessoaFisicaAtletaId'], 1, 'pfa_registro_fei');        
        $this->data['NomeDeCompeticao']         =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'pfa_id', $this->data['PessoaFisicaAtletaId'], 1, 'pfa_nome_competicao');        

        $IdFederacaoAtleta                      =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'pfa_id', $this->data['PessoaFisicaAtletaId'], 1, 'fk_pjf_id');
        $this->data['AtletaFhbr']               =   ($IdFederacaoAtleta == 141) ? true : false;


        // ModalidadesRegistro
        $SqlModalidadesRegistro             =   "SELECT * FROM tb_evento_modalidade ";
        $this->data['ModalidadesRegistro']  =   $this->db->query($SqlModalidadesRegistro)->result();





       $this->data['PackageStyles'][]    =   'Packages/Styles/Perfil/FrontOffice/Atleta';
       $this->data['PackageScripts'][]   =   'Packages/Scripts/Perfil/FrontOffice/Atleta';



        if($this->VerificaAtleta())
            $this->LoadTemplateProfilePessoa('Template/FrontOffice/sistema/Perfil/Atleta/Dashboard', $this->data);

        if(!$this->VerificaAtleta())
            $this->LoadTemplateProfilePessoa('Template/FrontOffice/sistema/Perfil/Atleta/NaoAtleta', $this->data);

    }



    /**
    * GerarBoleto
    *
    * @author Gustavo Botega 
    */
    public function GerarBoleto(){

        var_dump("t");

/*        require("pagarme-php/Pagarme.php");

        Pagarme::setApiKey("SUA_API_KEY");

        $transaction = new PagarMe_Transaction(array(
            "amount" => 100,
            "payment_method" => "boleto",
            "postback_url" => "http://requestb.in/pkt7pgpk",
            "customer" => array(
                "name" => "Aardvark Silva", 
                "document_number" => "18152564000105",
                "email" => "aardvark.silva@pagar.me",
                "address" => array(
                    "street" => "Avenida Brigadeiro Faria Lima", 
                    "street_number" => "1811",
                    "neighborhood" => "Jardim Paulistano",
                    "zipcode" => "01451001"
                ),
                "phone" =>  array(
                    "ddi" => "55"
                    "ddd" => "11",
                    "number" => "99999999" 
                )
            ),
            "metadata" => array(
                "idProduto" => 13933139
            )
        ));

            $transaction->charge();

*/
    }



    /**
    * AtletaDatasetVinculos
    *
    * @author Gustavo Botega 
    */
    public function AtletaDatasetVinculos(){


            $PessoaId                               =   $this->session->userdata('PessoaId');
            $PessoaFisicaId                         =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'fk_pes_id', $PessoaId, 1, 'pef_id');
            $PessoaFisicaAtletaId                   =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'fk_pef_id', $PessoaFisicaId, 1, 'pfa_id');
            $IdPessoa                               =   $PessoaId;
            $IdPessoaFisicaAtleta                   =   $PessoaFisicaAtletaId;


            $this->data['FlagRegistroAtleta']   =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'pfa_id', $IdPessoaFisicaAtleta, 1, 'flag_registro');
            $this->data['AtletaEscola']         =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'pfa_id', $IdPessoaFisicaAtleta, 1, 'pfa_escola');


            // Confederacao
            $QueryConfederacao    =   "
                                    SELECT * FROM tb_vinculo
                                        WHERE fk_pes1_id = ".$IdPessoa." AND
                                              fk_pes2_id IS NOT NULL AND
                                              fk_ani_id IS NULL AND
                                              fk_per_id = 2 AND
                                              fk_tip_id = 9 AND
                                              fk_sta_id IS NOT NULL
                                              ORDER BY vin_id DESC;
                                ";
            $Confederacao  =   $this->db->query($QueryConfederacao)->result();
            $this->data['Confederacao']   =   $Confederacao;



            // Federacao
            $QueryFederacao    =   "
                                    SELECT * FROM tb_vinculo
                                        WHERE fk_pes1_id = ".$IdPessoa." AND
                                              fk_pes2_id IS NOT NULL AND
                                              fk_ani_id IS NULL AND
                                              fk_per_id = 3 AND
                                              fk_tip_id = 9 AND
                                              fk_sta_id IS NOT NULL
                                              ORDER BY vin_id DESC;
                                ";
            $Federacao  =   $this->db->query($QueryFederacao)->result();
            $this->data['Federacao']   =   $Federacao;


            // Entidade 
            $QueryEntidade    =   "
                                    SELECT * FROM tb_vinculo
                                        WHERE fk_pes1_id = ".$IdPessoa." AND
                                              fk_pes2_id IS NOT NULL AND
                                              fk_ani_id IS NULL AND
                                              fk_per_id = 4 AND
                                              fk_tip_id = 9 AND
                                              fk_sta_id IS NOT NULL
                                              ORDER BY vin_id DESC;
                                ";
            $Entidade  =   $this->db->query($QueryEntidade)->result();
            $this->data['Entidade']   =   $Entidade;



            /* Confederacao Atual */
            $QueryConfederacaoAtual    =   "
                                    SELECT * FROM tb_vinculo
                                        WHERE fk_pes1_id = ".$IdPessoa." AND
                                              fk_pes2_id IS NOT NULL AND
                                              fk_ani_id IS NULL AND
                                              fk_per_id = 2 AND
                                              fk_tip_id = 9 AND
                                              fk_sta_id IS NOT NULL
                                              ORDER BY vin_id DESC
                                              LIMIT 1;
                                ";
            $ConfederacaoAtual                  =   $this->db->query($QueryConfederacaoAtual)->result();
            $this->data['ConfederacaoAtual']    =   $ConfederacaoAtual;

            /* Federacao Atual */
            $QueryFederacaoAtual    =   "
                                    SELECT * FROM tb_vinculo
                                        WHERE fk_pes1_id = ".$IdPessoa." AND
                                              fk_pes2_id IS NOT NULL AND
                                              fk_ani_id IS NULL AND
                                              fk_per_id = 3 AND
                                              fk_tip_id = 9 AND
                                              fk_sta_id IS NOT NULL
                                              ORDER BY vin_id DESC
                                              LIMIT 1;
                                ";
            $FederacaoAtual                     =   $this->db->query($QueryFederacaoAtual)->result();
            $this->data['FederacaoAtual']       =   $FederacaoAtual;


            /* Entidade Atual */
            $QueryEntidadeAtual    =   "
                                    SELECT * FROM tb_vinculo
                                        WHERE fk_pes1_id = ".$IdPessoa." AND
                                              fk_pes2_id IS NOT NULL AND
                                              fk_ani_id IS NULL AND
                                              fk_per_id = 4 AND
                                              fk_tip_id = 9 AND
                                              fk_sta_id IS NOT NULL
                                              ORDER BY vin_id DESC
                                              LIMIT 1;
                                ";
            $EntidadeAtual                     =   $this->db->query($QueryEntidadeAtual)->result();
            $this->data['EntidadeAtual']       =   $EntidadeAtual;



    }




    /**
    * AtletaDatasetRegistrosAtivos
    *
    * @author Gustavo Botega 
    */
    public function AtletaDatasetRegistrosAtivos(){


            $PessoaId                               =   $this->session->userdata('PessoaId');
            $PessoaFisicaId                         =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'fk_pes_id', $PessoaId, 1, 'pef_id');
            $PessoaFisicaAtletaId                   =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'fk_pef_id', $PessoaFisicaId, 1, 'pfa_id');


            /* Entidade Atual */
            $QueryRegistrosAtivos    =   "
                                    SELECT * FROM tb_registro
                                        WHERE fk_pes_id = ".$PessoaId." AND
                                              fk_ani_id IS NULL AND
                                              fk_rec_id = 1 AND 
                                              fk_sta_id = 1 
                                              ORDER BY reg_id DESC
                                ";  /* Fazer inner com a tabela financeiro para pegar status id financeiro */
            $this->data['RegistrosAtivos']          =   $this->db->query($QueryRegistrosAtivos)->result();



    }




    /**
    * PermissaoAtletaNovoRegistro
    *
    * Funcao checa se a pessoa possui permissao para realizar o novo registro.
    * A regra e ter pelo menos um telefone e email associado a conta.
    *
    * @author Gustavo Botega 
    */
    public function PermissaoAtletaNovoRegistro(){


            $PessoaId                               =   $this->session->userdata('PessoaId');

            /* Telefone */
            $QueryTelefone    =   "
                                    SELECT * FROM tb_telefone
                                        WHERE fk_peo_id = ".$PessoaId." AND
                                              fk_sta_id = 1 AND
                                              flag_deletado IS NULL AND 
                                              fk_aut2_id IS NULL
                                "; 
            $DatasetTelefone          =   $this->db->query($QueryTelefone)->result();
            $this->data['QtdeTelefone'] =   count($DatasetTelefone);

            /* Email */
            $QueryEmail    =   "
                                    SELECT * FROM tb_email
                                        WHERE fk_peo_id = ".$PessoaId." AND
                                              fk_sta_id = 1 AND
                                              flag_deletado IS NULL AND 
                                              fk_aut2_id IS NULL
                                "; 
            $DatasetEmail          =   $this->db->query($QueryEmail)->result();
            $this->data['QtdeEmail'] =   count($DatasetEmail);



    }





    /**
    * VerificaAtleta
    *
    * Metodo verifica se a pessoa possui perfil atleta. Retorna true se o vinculo existir.
    *
    * @author Gustavo Botega 
    */
    public function VerificaAtleta(){

        // GET ID PESSOA FISICA
        $PessoaFisicaId                           =   $this->session->userdata('PessoaFisicaId');


        $SqlVerificaAtleta      =   "
                                    SELECT * FROM tb_pessoa_fisica_atleta as pfa
                                    WHERE fk_pef_id = '".$PessoaFisicaId."' 
                                ";
        $QueryVerificaAtleta    =   $this->db->query($SqlVerificaAtleta)->result();        


        if(count($QueryVerificaAtleta) === 1)
            return true;

        return false;

    }





    /**
    * GetTipoRegistro
    *
    * @author Gustavo Botega 
    */
    public function GetTipoRegistro(){

        $ModalidadeId   =    $_POST['ModalidadeId'];


        $SqlConsultaTipoRegistro      =   "
                                    SELECT * FROM tb_registro_taxa 
                                    WHERE fk_evm_id = '".$ModalidadeId."'
                                ";
        $Query              =   $this->db->query($SqlConsultaTipoRegistro)->result();        

        $ArrTipoRegistro    =   array();
        foreach ($Query as $key => $value) {        

            if(!in_array($value->fk_ret_id, $ArrTipoRegistro))
                $ArrTipoRegistro[] = $value->fk_ret_id;

        }


        echo json_encode($ArrTipoRegistro);



    }




    /**
    * GetValoresRegistro
    *
    * @author Gustavo Botega 
    */
    public function GetValoresRegistro(){


        $ModalidadeId               =    $_POST['ModalidadeId'];
        $TipoRegistroId             =    $_POST['TipoRegistroId'];


        $AnoAtual                   =   date('Y');
        $AnoId                      =   $this->model_crud->get_rowSpecific('tb_registro_ano', 'rea_ano', $AnoAtual, 1, 'rea_id');

        $SqlConsultaValores         =   "
                                            SELECT * FROM tb_registro_taxa 
                                            WHERE fk_evm_id = '".$ModalidadeId."' AND 
                                                  fk_ret_id = '".$TipoRegistroId."' AND 
                                                  fk_rec_id = 1 AND
                                                  fk_rea_id = '".$AnoId."'
                                        ";
        $Query                      =   $this->db->query($SqlConsultaValores)->result();        

        $ArrTaxa                    =   $this->GetValoresRegistroTaxa($Query[0]);
        $ArrDesconto                =   $this->GetValoresRegistroDesconto($Query[0]);
        $Total                      =   $this->GetValoresRegistroTotal( $ArrTaxa, $ArrDesconto, $ModalidadeId, $TipoRegistroId );


        $Dataset                    =   array(
                                                'Taxa'      => $ArrTaxa,
                                                'Desconto'  => $ArrDesconto,
                                                'Total'     => $Total
                                            );

        echo json_encode($Dataset);

    }





    /**
    * GetValoresRegistroTaxa
    *
    * @author Gustavo Botega 
    */
    public function GetValoresRegistroTaxa($Query){

        $Array  =   array();

        // Get Data Limite Promocional
        $Array['DataLimitePromocional'] =   date("d/m", strtotime($Query->ret_data_promocional));

        // Get Valor Cheio
        $Array['Valor']                 =   $Query->ret_preco;

        // Get Valor
        $Array['ValorPromocional']      =   (!is_null($Query->ret_preco_promocional)) ? $Query->ret_preco_promocional : NULL;

        // Get CBH Atleta Obrigatorio
        $Array['CbhObrigatorio']        =   $this->model_crud->get_rowSpecific('tb_registro_ano', 'rea_ano', date('Y'), 1, 'rea_taxa_atleta_cbh_obrigatorio');

        // Get Flag CBH - Habilitar Venda Atleta
        $Array['CbhHabilitarVenda']     =   $this->model_crud->get_rowSpecific('tb_registro_ano', 'rea_ano', date('Y'), 1, 'rea_habilitar_venda_registro_atleta_cbh');

        // Get Taxa CBH
        $Array['CbhTaxa']               =   $this->model_crud->get_rowSpecific('tb_registro_ano', 'rea_ano', date('Y'), 1, 'rea_taxa_atleta_cbh');

        return $Array;

    }


    /**
    * GetValoresRegistroDesconto
    *
    * @author Gustavo Botega 
    */
    public function GetValoresRegistroDesconto($Query){

        $RegistroTaxaId                 =   $Query->ret_id;

        $Array      =   array();
        $Array['Flag']                  =   false;
        $DataAtual                      =   date("Y-m-d");
        $DataAtualDateTime              =   new DateTime();


        // Checar se data promocional esta ativa. Se sim aplica o desconto e desconsidera qualquer outro.
        $DataPromocional                =   $Query->ret_data_promocional;
        if($DataAtual > $DataPromocional) {
            // passou desconto

        }else{
            // desconto valido
            $Array['Flag']                          =   true;
            $Array['DescontoPromocionalFlag']       =   true;
            $Array['DescontoPromocionalValor']      =   number_format($Query->ret_preco - $Query->ret_preco_promocional, 2, '.', '');

            if( is_null($Query->ret_preco_promocional) ){ // caso participacao unica salto.
                $Array['Flag']                      =   false;
                $Array['DescontoPromocionalFlag']   =   false;
                $Array['DescontoPromocionalValor']  =   0.00;
            }

            return $Array;
        }

        $Array['DescontoPromocionalFlag']           =   false;




        // Faz o laco dentro de cada desconto e checa se a data atual esta dentro da faixa de data. Se encontrar alguma ocorrencia automaticamente para e assume o desconto
        // Obter todos os descontos dessa taxa
        $SqlDescontosParaTaxaSelecionada   =   "
                                            SELECT * FROM tb_registro_desconto 
                                            WHERE fk_ret_id = '".$RegistroTaxaId."' AND 
                                                  fk_sta_id = 1 
                                        ";
        $Query                      =   $this->db->query($SqlDescontosParaTaxaSelecionada)->result();     

        var_dump($Query);
        if(empty($Query))
            return $Array;

        if(!empty($Query)):

            foreach ($Query as $key => $value):

                $DataInicio     =   $value->red_data_inicio;  // Data inicio do desconto
                $DataFim        =   $value->red_data_fim; // Data fim do desconto

//                $DataAtualDateTime = new DateTime('2018-10-12'); // Simular outra data
                $DateBegin = new DateTime($DataInicio);
                $DateEnd  = new DateTime($DataFim);

                if (
                  $DataAtualDateTime->getTimestamp() > $DateBegin->getTimestamp() && 
                  $DataAtualDateTime->getTimestamp() < $DateEnd->getTimestamp()){
                    // Dentro do intervalo de datas do desconto
                      $Array['Flag']          =   true;
                      $Array['Desconto']      =   number_format($value->red_desconto, 2, '.', '');
                      break;
                }else{
                    // Fora do intervalo de datas
                }


            endforeach;

        endif;


        return $Array;

    }


    /**
    * GetValoresRegistroTotal
    *
    * @author Gustavo Botega 
    */
    public function GetValoresRegistroTotal($ArrTaxa, $ArrDesconto, $ModalidadeId, $TipoRegistroId){

        $Array                              =   array();
        $Desconto                           =   0.00; // Setando desconto zerado
        $Taxa                               =   $ArrTaxa['Valor']; 
        $TaxaPromocional                    =   $ArrTaxa['ValorPromocional'];
        $TaxaCbhObrigatorio                 =   $ArrTaxa['CbhObrigatorio'];
        $TaxaCbhHabilitarVenda              =   $ArrTaxa['CbhHabilitarVenda'];
        $TaxaCbh                            =   $ArrTaxa['CbhTaxa'];

        $TotalBruto                         =   $Taxa;

        if($TaxaCbhHabilitarVenda && $TaxaCbhObrigatorio && $ModalidadeId == '1' && $TipoRegistroId == '1')
            $TotalBruto                     =   $Taxa + $TaxaCbh;
        


        /* Descontos Promocional */
        if($ArrDesconto['Flag'] && $ArrDesconto['DescontoPromocionalFlag'] && !is_null($TaxaPromocional))
            $Desconto                       =   $Taxa - $TaxaPromocional;


        /* Descontos Mes a Mes */
        if($ArrDesconto['Flag'] && !$ArrDesconto['DescontoPromocionalFlag'])
            $Desconto                       =   $ArrDesconto['Desconto'];



        $Array['TotalBruto']                =   number_format($TotalBruto, 2, '.', '');
        $Array['TotalLiquido']              =   number_format($TotalBruto - $Desconto, 2, '.', '');

        return $Array;

    }








    /**
    * AjaxDadosAtletaProcessar
    *
    * @author Gustavo Botega 
    */
    public function AjaxDadosAtletaProcessar(){


        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);    

        $Processar  =   $this->ProcessarDadosAtleta(true, $Dados);


        echo json_encode($Processar);

    }



    /**
    * ProcessarDadosAtleta
    *
    * @author Gustavo Botega 
    */
    public function ProcessarDadosAtleta($Ajax = false, $AjaxDados = false){

        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);                       

        if($AjaxDados)
            $Dados  =   $AjaxDados;


        // Validacao Formulario
        $Validar                                    =   $this->ValidarDadosAtleta($Dados);
        if(!$Validar)
            return false;


        $Gravar                                     =   $this->GravarDadosAtleta($Dados);
        if(!$Gravar)
            return false;


        // Cadastro atualizado
        return true;

    }



    /**
    * ValidarDadosAtleta
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function ValidarDadosAtleta($Dados){

        // Nome Competicao

        return true;

    }


    /**
    * GravarDadosAtleta
    *
    * @author Gustavo Botega 
    */
    public function GravarDadosAtleta($Dados){


        foreach ($Dados as $key => $value) {
            if(empty($value))
                $Dados[$key]    =   NULL;
        }

        // Dados
        $Dataset       =  array(
            'pfa_nome_competicao'               =>    $Dados['nome-competicao'], // Nome de Competicao
            'modificado'                        =>    date("Y-m-d H:i:s")
        );

 
        $PessoaId                           =   $this->session->userdata('PessoaId');
        $PessoaFisicaId                     =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'fk_pes_id', $PessoaId, 1, 'pef_id');

        /* Query */
        $Query = $this->db->update('tb_pessoa_fisica_atleta', $Dataset, "fk_pef_id = " . $PessoaFisicaId);

        return ($Query) ? true : false;

    }



    
    
    /*
    ---------------------------------------------------------------------
        
        TROCA DE FEDERACAO
    
    ====================================================================*/
    public function AjaxTrocaFederacao(){

        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);    

        $Processar  =   $this->ProcessarTrocaFederacao(true, $Dados);

        echo json_encode($Processar);        
    }




    /**
    * TrocarFederacao
    *
    * @author Gustavo Botega 
    */
    public function ProcessarTrocaFederacao($Ajax = false, $AjaxDados = false){


        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);                       

        if($AjaxDados)
            $Dados  =   $AjaxDados;


        // Validacao Formulario
        $Validar                                    =   $this->ValidarTrocaFederacao($Dados);
        if(!$Validar)
            return false;


        $Gravar                                     =   $this->GravarTrocaFederacao($Dados);
        if(!$Gravar)
            return false;


        // Cadastro atualizado
        return true;        


    }

    
    /**
    * ValidarDadosAtleta
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function ValidarTrocaFederacao($Dados){

        // Federacao
        
        // Entidade

        return true;

    }


    /**
    * GravarTrocaFederacao
    *
    * @author Gustavo Botega 
    */
    public function GravarTrocaFederacao($Dados){

        $this->load->module('vinculo');

        $FederacaoAtualId              =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'pfa_id', $this->data['PessoaFisicaAtletaId'], 1, 'fk_pjf_id');
        $FederacaoNovaId               =   $Dados['federacao'];
        $PessoaId                      =   $this->data['PessoaId'];
        $NomeCompleto                  =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $PessoaId, 1, 'pes_nome_razao_social');
        
        // Obtendo o vinculo atual
        $SqlVinculoAtualFederacao                     = '
                                                SELECT * FROM
                                                    tb_vinculo as vin
                                            WHERE
                                                vin.fk_pes1_id = '.$PessoaId.' AND
                                                vin.fk_pes2_id = '.$FederacaoAtualId.' AND
                                                vin.fk_per_id = 3 AND
                                                vin.fk_tip_id = 9 AND
                                                vin.fk_sta_id IS NOT NULL
                                            ORDER BY vin.vin_id DESC 
                                            LIMIT 1
                                            ';
        $Query                              = $this->db->query($SqlVinculoAtualFederacao)->result();
        if(empty($Query)) return false;

        $VinculoAtualFederacaoId            = $Query[0]->vin_id;

        
        // Cancelando o vinculo atual
        $Dataset    =   array(
                            'vin_id'            =>  $VinculoAtualFederacaoId,
                            'fk_sta_id'         =>  49, // Vínculo cancelado pelo próprio usuário em virtude de transferencia de federacao.
                            'vih_historico'     =>  'Vinculado com a federaçao foi cancelado pelo usuario. Motivo: Transferencia de federacao. Usuario: ' . $NomeCompleto . ' ID: ' . $PessoaId,
                        );
        $this->vinculo->Atualizar($Dataset);
        

        
        // Inserindo o vinculo da nova federacao
        $Dataset    =   array(
                            'federacao'         =>  $FederacaoNovaId, 
                            'fk_sta_id'         =>  12, // [ Vínculos ] Atleta aguardando aprovação de vínculo pela Federação.
                        );
        $this->vinculo->GravarVinculoAtletaFederacao($Dataset, $PessoaId);

        
        
        // Atualizando vinculo na tabela do atleta. tb_pessoa_fisica_atleta
        $Dataset    =   array(
                            'fk_pjf_id'         =>  $FederacaoNovaId, 
                            'modificado'        =>  date("Y-m-d H:i:s"), 
                        );
        $Query    =   $this->db->update('tb_pessoa_fisica_atleta', $Dataset, array('pfa_id' => $this->data['PessoaFisicaAtletaId'] ));        
        
        
        
        /*
            # MOVIMENTO DE SAIDA 
            A federacao atual e FHBr. Logo o movimento e de saida, ou seja, transferencia de federacao. Saindo de FHBr para outra federacao.
            Nessa situacao o procedimento e cancelar vinculo com ultima entidade valida e cancelar todos os registros
        */
        if($FederacaoAtualId    ==    '141'):  
            
            // Rotina para cancelar a ultima entidade equestre
            // Obtendo o vinculo atual
            $SqlVinculoAtualEntidadeEquestre   =    '
                                                        SELECT * FROM
                                                                tb_vinculo as vin
                                                        WHERE
                                                            vin.fk_pes1_id = '.$PessoaId.' AND
                                                            vin.fk_pes2_id IS NOT NULL AND
                                                            vin.fk_ani_id IS NULL AND
                                                            vin.fk_per_id = 4 AND
                                                            vin.fk_tip_id = 9 AND
                                                            vin.fk_sta_id IS NOT NULL
                                                        ORDER BY vin.vin_id DESC 
                                                        LIMIT 1
                                                    ';
            $Query                              =   $this->db->query($SqlVinculoAtualEntidadeEquestre)->result();                
            $VinculoAtualEntidadeEquestreId     =   $Query[0]->vin_id;
            
            // Cancelando vinculo
            $DatasetCancelarEntidade    =   array(
                                'vin_id'            =>  $VinculoAtualEntidadeEquestreId,
                                'fk_sta_id'         =>  49, // Vínculo cancelado pelo próprio usuário em virtude da transferencia de federacao.
                                'vih_historico'     =>  'Vinculado com a entidade equestre foi cancelado pelo usuario. Motivo: Transferencia de federação. Anula o vinculo com a entidade equestre. Usuario: ' . $NomeCompleto . ' ID: ' . $PessoaId,
                            );
            $this->vinculo->Atualizar($DatasetCancelarEntidade);

        
            // Atualizando vinculo na tabela do atleta. tb_pessoa_fisica_atleta
            $DatasetCancelarAtleta    =   array(
                                'fk_pje_id'         =>  NULL, 
                                'modificado'        =>  date("Y-m-d H:i:s"), 
                            );
            $Query    =   $this->db->update('tb_pessoa_fisica_atleta', $DatasetCancelarAtleta, array('pfa_id' => $this->data['PessoaFisicaAtletaId'] ));        

        
        
            // Rotina para cancelar todos os registros ativos
            // Implementar
            
        endif;
        
        
        
        /*
            # MOVIMENTO DE ENTRADA
            A federacao nova sera a FHBr.
            Nessa situacao o procedimento e realizar o vinculo com a entidade selecionada.
        */
        if($FederacaoNovaId    ==    '141'): // FHBr ID 
            
            $EntidadeEquestreId         =   $Dados['federacao-entidade-equestre'];

            // Inserindo o vinculo da nova federacao
            $Dataset    =   array(
                                'entidade-equestre'         =>  $EntidadeEquestreId, 
                                'fk_sta_id'                 =>  20, // [ Vínculos ] Atleta aguardando aprovação de vínculo pela Entidade Equestre.
                            );
            $this->vinculo->GravarVinculoAtletaEntidade($Dataset, $PessoaId);


            // Atualizando vinculo na tabela do atleta. tb_pessoa_fisica_atleta
            $DatasetAtleta    =   array(
                                'fk_pje_id'         =>  $EntidadeEquestreId, 
                                'modificado'        =>  date("Y-m-d H:i:s"), 
                            );
            $Query    =   $this->db->update('tb_pessoa_fisica_atleta', $DatasetAtleta, array('pfa_id' => $this->data['PessoaFisicaAtletaId'] ));        
        
        endif;
        
        return true;
        
    }



    
    
    
    /*
    ---------------------------------------------------------------------
        
        TROCA DE ENTIDADE
    
    ====================================================================*/
    public function AjaxTrocaEntidade(){

        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);    

        $Processar  =   $this->ProcessarTrocaEntidade(true, $Dados);

        echo json_encode($Processar);        
    }




    /**
    * ProcessarTrocaEntidade
    *
    * @author Gustavo Botega 
    */
    public function ProcessarTrocaEntidade($Ajax = false, $AjaxDados = false){


        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);                       

        if($AjaxDados)
            $Dados  =   $AjaxDados;


        // Validacao Formulario
        $Validar                                    =   $this->ValidarTrocaEntidade($Dados);
        if(!$Validar)
            return false;


        $Gravar                                     =   $this->GravarTrocaEntidade($Dados);
        if(!$Gravar)
            return false;


        // Cadastro atualizado
        return true;        


    }

    
    /**
    * ValidarTrocaEntidade
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function ValidarTrocaEntidade($Dados){

        // Entidade - entidade deve ser diferente da atual, nao pode ser null, deve ser uma que existe.

        return true;

    }


    /**
    * GravarTrocaEntidade
    *
    * @author Gustavo Botega 
    */
    public function GravarTrocaEntidade($Dados){

        $this->load->module('vinculo');

        $EntidadeAtualId               =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'pfa_id', $this->data['PessoaFisicaAtletaId'], 1, 'fk_pje_id');
        $EntidadeNovaId                =   $Dados['entidade-equestre'];
        $PessoaId                      =   $this->data['PessoaId'];
        $NomeCompleto                  =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $PessoaId, 1, 'pes_nome_razao_social');
        
        // Obtendo o vinculo atual
        $SqlVinculoAtualEntidade       = '
                                                SELECT * FROM
                                                    tb_vinculo as vin
                                            WHERE
                                                vin.fk_pes1_id = '.$PessoaId.' AND
                                                vin.fk_pes2_id = '.$EntidadeAtualId.' AND
                                                vin.fk_per_id = 4 AND
                                                vin.fk_tip_id = 9 AND
                                                vin.fk_sta_id IS NOT NULL
                                            ORDER BY vin.vin_id DESC 
                                            LIMIT 1
                                            ';
        $Query                              = $this->db->query($SqlVinculoAtualEntidade)->result();
        if(empty($Query)) return false;

        $VinculoAtualEntidadeId            = $Query[0]->vin_id;

        
        // Cancelando o vinculo atual
        $Dataset    =   array(
                            'vin_id'            =>  $VinculoAtualEntidadeId,
                            'fk_sta_id'         =>  49, // Vínculo cancelado pelo próprio usuário em virtude de transferencia de entidade.
                            'vih_historico'     =>  'Vinculado com a federaçao foi cancelado pelo usuario. Motivo: Transferencia de entidade. Usuario: ' . $NomeCompleto . ' ID: ' . $PessoaId,
                        );
        $this->vinculo->Atualizar($Dataset);
        

        
        // Inserindo o vinculo da nova entidade
        $Dataset    =   array(
                            'entidade-equestre' =>  $EntidadeNovaId, 
                            'fk_sta_id'         =>  20, // [ Vínculos ] Atleta aguardando aprovação de vínculo pela Entidade Equestre.
                        );
        $this->vinculo->GravarVinculoAtletaEntidade($Dataset, $PessoaId);

        
        
        // Atualizando vinculo na tabela do atleta. tb_pessoa_fisica_atleta
        $Dataset    =   array(
                            'fk_pje_id'         =>  $EntidadeNovaId, 
                            'modificado'        =>  date("Y-m-d H:i:s"), 
                        );
        $Query    =   $this->db->update('tb_pessoa_fisica_atleta', $Dataset, array('pfa_id' => $this->data['PessoaFisicaAtletaId'] ));        
        
        
        
        return true;
        
    }
















    /**
    * AjaxNumeroChipExiste
    *
    * @author Gustavo Botega 
    */
    public function AjaxNumeroChipExiste(){

        $NumeroChip         =   $_POST['NumeroChip'];
        $SqlNumeroChip      =   "
                                    SELECT * FROM tb_animal as ani
                                    WHERE ani_chip = '".$NumeroChip."'
                                ";
        $Query              =   $this->db->query($SqlNumeroChip)->result();        
        echo json_encode( (!empty($Query)) ? true : false );

    }


    /**
    * Validar
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function Validar($Dados){

        // Nome Competicao

        // Matricula CBH

        // Matricula FEI

        // Federacao

        // Entidade


        return true;

    }




    /**
    * Gravar
    *
    * @author Gustavo Botega 
    */
    public function Gravar($Dados){

/*        // Genero Tipo
        (empty($Dados['genero-tipo'])) ? $Dados['genero-tipo'] = NULL : '';

        // Associacao
        (empty($Dados['associacao'])) ? $Dados['associacao'] = NULL : '';

        // Pais de Origem
        (empty($Dados['pais-origem'])) ? $Dados['pais-origem'] = NULL : '';

        // Pais de Origem
        (empty($Dados['pais-origem'])) ? $Dados['pais-origem'] = NULL : '';
*/

        foreach ($Dados as $key => $value) {
            if(empty($value))
                $Dados[$key]    =   NULL;
        }


        // Atleta Escola
        $AtletaEscola = NULL;
        if(isset($Dados['escola-equitacao'])){
            (!empty($Dados['escola-equitacao']) && $Dados['escola-equitacao'] > 0) ? $AtletaEscola = 1 : $AtletaEscola = NULL;
        }


        // Genero Tipo
        (!isset($Dados['genero-tipo'])) ? $Dados['genero-tipo'] = NULL : '';

        // Genero 
        (!isset($Dados['genero'])) ? $Dados['genero'] = NULL : '';




        // Dados
        $Dataset       =  array(
            
            'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
            'fk_sta_id'                         =>    1,
            'fk_anr_id'                         =>    $Dados['raca'], // raca do animal
            'fk_anp_id'                         =>    $Dados['pelagem'], // pelagem do animal
            'fk_ang_id'                         =>    $Dados['genero'], // genero do animal
            'fk_agt_id'                         =>    $Dados['genero-tipo'], // genero tipo do animal
            'fk_ass_id'                         =>    $Dados['associacao'], // Associacao de Registro
            'fk_pai_id'                         =>    $Dados['pais-origem'], // Pais Origem
            'ani_chip'                          =>    $Dados['chip'], 
            'ani_data_nascimento'               =>    $this->my_data->ConverterData($Dados['data-nascimento'], 'PT-BR', 'ISO'), 
            'ani_nome_completo'                 =>    $Dados['nome-completo'], 
            'ani_nome_patrocinado'              =>    $Dados['nome-patrocinado'], 
            'ani_peso'                          =>    $Dados['peso'], 
            'ani_altura_cruz'                   =>    $Dados['altura-cruz'], 
            'ani_registro_fei'                  =>    $Dados['registro-fei'], 
            'ani_registro_capa_fei'             =>    $Dados['registro-capa-fei'], 
            'ani_registro_cbh'                  =>    $Dados['registro-cbh'], 
            'ani_registro_genealogico'          =>    $Dados['registro-genealogico'], 
            'ani_nome_pai'                      =>    $Dados['nome-pai'], 
            'ani_nome_mae'                      =>    $Dados['nome-mae'], 
            'ani_nome_avo_materno'              =>    $Dados['nome-avo-materno'], 
            'ani_escola'                        =>    $AtletaEscola, 
            'flag_permissao_autor'              =>    1, 
            'criado'                            =>    date("Y-m-d H:i:s")

        );

 
        /* Query */
        $Query = $this->db->insert('tb_animal', $Dataset);

        return ($Query) ? $this->db->insert_id() : false;

    }





    /**
    * AtletaDatasetFederacaoEntidade
    *
    * @author Gustavo Botega 
    */
    public function AtletaDatasetFederacaoEntidade($PessoaId, $PessoaFisicaId, $PessoaFisicaAtletaId){

        
        $FederacaoAtualId   =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'pfa_id', $PessoaFisicaAtletaId, 1, 'fk_pjf_id');
        $EntidadeAtualId    =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'pfa_id', $PessoaFisicaAtletaId, 1, 'fk_pje_id');
        
        
        
        //  Lista todas as federacoes exceto a que ele esta associado.
        $SqlFederacao                     = '
                                                SELECT * FROM
                                                    tb_pessoa as pes
                                                INNER JOIN
                                                    tb_vinculo as vin
                                                ON
                                                    pes.pes_id = vin.fk_pes1_id
                                            WHERE
                                                    vin.fk_per_id = 3 AND
                                                    vin.fk_tip_id IS NULL AND
                                                    vin.fk_sta_id = 1 AND
                                                    pes.fk_sta_id = 1 AND
                                                    pes.pes_id != "'.$FederacaoAtualId.'"
                                                ORDER BY
                                                    pes_nome_razao_social asc
                                            ';
        $this->data['DatasetFederacao']   = $this->db->query($SqlFederacao)->result();



        /* Entidade Filiada */
        $SqlFederacaoEntidadeFiliada      = '
                                                SELECT * FROM tb_pessoa as pes

                                                INNER JOIN tb_vinculo as vin
                                                ON pes.pes_id = vin.fk_pes1_id

                                                INNER JOIN tb_pessoa_juridica as pej
                                                ON pej.fk_pes_id = pes.pes_id


                                                INNER JOIN tb_pessoa_juridica_entidade as pje
                                                ON pje.fk_pej_id = pej.pej_id


                                                WHERE vin.fk_per_id = 4 AND 
                                                vin.fk_sta_id = 1 AND
                                                vin.fk_tip_id IS NULL AND
                                                pes.fk_sta_id = 1 AND
                                                pje.flag_escola_equitacao = 1

                                                ORDER BY pes.pes_nome_razao_social ASC

                                            ';
        $this->data['DatasetFederacaoEntidadeFiliada']   = $this->db->query($SqlFederacaoEntidadeFiliada)->result();

        
        /* Entidade Filiada */
        if(!is_null($EntidadeAtualId)):
        $SqlEntidadeFiliada                     = '
                                                SELECT * FROM tb_pessoa as pes

                                                INNER JOIN tb_vinculo as vin
                                                ON pes.pes_id = vin.fk_pes1_id

                                                INNER JOIN tb_pessoa_juridica as pej
                                                ON pej.fk_pes_id = pes.pes_id


                                                INNER JOIN tb_pessoa_juridica_entidade as pje
                                                ON pje.fk_pej_id = pej.pej_id


                                                WHERE vin.fk_per_id = 4 AND 
                                                vin.fk_sta_id = 1 AND
                                                vin.fk_tip_id IS NULL AND
                                                pes.fk_sta_id = 1 AND
                                                pje.flag_escola_equitacao = 1 AND
                                                pes.pes_id != '.$EntidadeAtualId.'

                                                ORDER BY pes.pes_nome_razao_social ASC

                                            ';
        $this->data['DatasetEntidadeFiliada']   = $this->db->query($SqlEntidadeFiliada)->result();
        endif;


        
        


    }








    /**
    * JsonProcessar
    *
    * @author Gustavo Botega 
    */
    public function JsonProcessar(){

    }





    /**
    * DatasetPagarme
    *
    * @author Gustavo Botega 
    */
    public function DatasetPagarme(){

        $PessoaId                               =       $this->data['PessoaId'];

        $this->data['PagarmeNome']              =       $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $PessoaId, 1, 'pes_nome_razao_social');
        $this->data['PagarmeCpf']               =       $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $PessoaId, 1, 'pes_cpf_cnpj');
        $this->data['PagarmeDataNascimento']    =       $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $PessoaId, 1, 'pes_data_nascimento_fundacao');

        // Obtendo o Email Principal
        $this->data['PagarmeEmail']             =       '';
        $SqlPagarmeEmail         =   "
                                        SELECT * FROM tb_email as ema

                                        WHERE
                                            ema.fk_peo_id    = '".$PessoaId."' AND 
                                            ema.flag_deletado IS NULL and 
                                            ema.fk_sta_id = 1 AND 
                                            ema.fk_aut2_id IS NULL

                                        LIMIT 1
                                        ";
        $QueryPagarmeEmail       =   $this->db->query($SqlPagarmeEmail)->result();
        if(!empty($QueryPagarmeEmail))
            $this->data['PagarmeEmail']             =       $QueryPagarmeEmail[0]->ema_email;



        // Obtendo o Telefone Principal
        $this->data['PagarmeTelefonePrincipal']             =       '';
        $SqlPagarmeTelefone         =   "
                                        SELECT * FROM tb_telefone as tel

                                        WHERE
                                            tel.fk_peo_id    = '".$PessoaId."' AND 
                                            tel.flag_deletado IS NULL and 
                                            tel.fk_sta_id = 1 AND 
                                            tel.fk_aut2_id IS NULL

                                        LIMIT 1
                                        ";
        $QueryPagarmeTelefone       =   $this->db->query($SqlPagarmeTelefone)->result();
        if(!empty($QueryPagarmeTelefone)){
            $Ddd                                                =       (string)$QueryPagarmeTelefone[0]->tel_ddd;
            $Telefone                                           =       str_replace("-", "", $QueryPagarmeTelefone[0]->tel_telefone);
            $this->data['PagarmeTelefonePrincipal']             =       $Ddd . (string)$Telefone ;
        }



        // Obtendo o Endereco Principal
        $this->data['PagarmeEndereco']          =       '';
        $SqlPagarmeEndereco         =   "
                                        SELECT * FROM tb_endereco as end

                                        WHERE
                                            end.fk_peo_id    = '".$PessoaId."' AND 
                                            end.flag_deletado IS NULL and 
                                            end.fk_sta_id = 1 AND 
                                            end.fk_aut2_id IS NULL

                                        LIMIT 1
                                        ";
        $QueryPagarmeEndereco       =   $this->db->query($SqlPagarmeEndereco)->result();
        if(!empty($QueryPagarmeEndereco)){
            $this->data['PagarmeCep']                   =       $QueryPagarmeEndereco[0]->end_cep;
            $this->data['PagarmeEstadoSigla']           =       $this->model_crud->get_rowSpecific('tb_estado', 'est_id', $QueryPagarmeEndereco[0]->fk_est_id, 1, 'est_sigla');
            $this->data['PagarmeCidade']                =       $this->model_crud->get_rowSpecific('tb_cidade', 'cid_id', $QueryPagarmeEndereco[0]->fk_cid_id, 1, 'cid_nome');
            $this->data['PagarmeBairro']                =       $QueryPagarmeEndereco[0]->end_bairro;
            $this->data['PagarmeLogradouro']            =       $QueryPagarmeEndereco[0]->end_logradouro;
            $this->data['PagarmeNumero']                =       $QueryPagarmeEndereco[0]->end_numero;
            $this->data['PagarmeComplemento']           =       $QueryPagarmeEndereco[0]->end_complemento;
        }




    }



    /**
    * ConsultarProprietario
    *
    * @author Gustavo Botega 
    */
    public function ConsultarProprietario(){


        $CpfCnpjProprietario        =   $_POST['CpfCnpjProprietario'];
        $CpfCnpjProprietario        =   $this->my_pessoa->RemoverPontuacaoCpfCnpj($CpfCnpjProprietario);


        $SqlConsultaCpfCnpj         =   "
                                        SELECT * FROM tb_pessoa as pes

                                        WHERE
                                            pes.pes_cpf_cnpj    = '".$CpfCnpjProprietario."' AND 
                                            pes.fk_sta_id       =  1
                                        ";
        $Query                      =   $this->db->query($SqlConsultaCpfCnpj)->result();
        
        $Dados                      =   array();
        if(!empty($Query)){
            $Dados['Status']                =   true;
            $Dados['StatusSlug']            =   'ProprietarioEncontrado';
            $Dados['PessoaId']              =   $Query[0]->pes_id;
            $Dados['PessoaNomeCompleto']    =   $Query[0]->pes_nome_razao_social;
            $Dados['PessoaCpfCnpj']         =   $this->my_pessoa->InserirPontuacaoCpfCnpj($Query[0]->pes_cpf_cnpj);

            if( $Query[0]->pes_cpf_cnpj == $this->session->userdata('PessoaCpf') )
                $Dados['StatusSlug']            =   'ProprietarioIgualAutor';


        }else{
            $Dados['Status']                =   false;
            $Dados['StatusSlug']            =   'ProprietarioNaoEncontrado';
        }


        echo json_encode($Dados);

    }



    /**
    * JsonProprietario
    *
    * @author Gustavo Botega 
    */
    public function JsonProprietario(){


        $QueryJson         =   "
                                        SELECT * FROM tb_pessoa as pes

                                        WHERE
                                            pes.pes_natureza    =  'PF' AND
                                            pes.fk_sta_id       =  1
                                        ";
        $Query                      =   $this->db->query($QueryJson)->result();
                

        $array  = array();
        foreach ($Query as $key => $value) {

            $array[]       =  array(
                                'name'      => strtoupper($value->pes_nome_razao_social),
                                'id'        => $value->pes_id,
                            ) ;

        }


/*
            $array[]['name']    =   $value->pes_nome_razao_social;
            $array[]['id']      =   $value->pes_id;

        foreach ($Query as $key => $value) {
            array(
                'name'  => $value->pes_nome_razao_social,
                'id'    => $value->pes_id
            );
        }
*/

        echo json_encode($array);

       // echo json_encode($Dados);

    }





    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    */
    public function ThemeComponent(){ 


        /* 
            StylesFile 
        */
            /*  Plugins*/
            $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
            $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete']                           = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                    = TRUE;
            $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;
        
            /*  Syles*/
               $this->data['StylesFile']['Styles']['profile']                                   = TRUE;



        /* 
            ScriptsFile 
        */
            /*  Plugins*/
            $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
            $this->data['ScriptsFile']['Plugins']['easy-autocomplete']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

            /*  Scripts*/
            $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                            = TRUE;

    }



    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function SigepeAsset()
    { 
        /* Carregando Estilos */
//        $this->data['PackageStyles'][]    =   'Packages/Styles/Animal/FrontOffice/Cadastro';


        /* Carregando Scripts */
//        $this->data['PackageScripts'][]   =   'Packages/Scripts/Animal/FrontOffice/Animal';




    }



     /*
             // @@@ Implementar permissões por usuario
        
        // AUDITORIA:
            [0]=> Slug Module
            [1]=> Table Database
            [2]=> Column Database
            [3]=> Value Deleted
            [4]=> Folder Thumb
            [5]=> Delete Gallery
            [6]=> Table Gallery
            [7]=> Column Table Gallery
            [8]=> Data/hora
            [9]=> IP
            [10]=> ID Usuário  
            [11]=> Type of Operation  
            */

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

