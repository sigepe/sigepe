<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';
class Telefone extends MY_FrontOffice {

    public $data;

    function __construct() {

        parent::__construct();

        $this->ThemeComponent(); 
        $this->SigepeAsset(); 

        $this->data['NavActiveSidebar']   = 'Telefone';
        $this->data['ShowColumnLeft'] = "ProfilePessoa";



    }


    /**
    * Index
    *
    * @author Gustavo Botega 
    */
    public function index() {
		$this->Formulario();    	
    }


    /**
    * Formulario
    *
    * @author Gustavo Botega 
    */
    public function Formulario(){

        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Meu Perfil';
        $this->data['PageHeadSubtitle']   = 'Visualize e gerencie seu perfil. Mantenha seus dados sempre atualizados.';
        $this->data['Breadcrumbs']        = array();


        $PessoaId                           =   $this->session->userdata('PessoaId');
        $PessoaFisicaId                     =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'fk_pes_id', $PessoaId, 1, 'pef_id');
        $this->data['PessoaId']             =   $PessoaId;        
        $this->data['PessoaFisicaId']       =   $PessoaFisicaId;        


        $SqlTelefone      =   "
                                    SELECT * FROM tb_telefone as tel
                                    WHERE fk_peo_id = '".$PessoaId."' AND
                                    flag_deletado IS NULL
                                    ORDER BY tel_principal DESC, tel_id DESC
                                ";
        $this->data['DatasetTelefone']      =   $this->db->query($SqlTelefone)->result();        


        $SqlTipoTelefone      =   "
                                    SELECT * FROM tb_contato 
                                    ORDER BY con_id ASC
                                ";
        $this->data['DatasetTipoTelefone']      =   $this->db->query($SqlTipoTelefone)->result();        



        $this->data['PackageStyles'][]    =   'Packages/Styles/Perfil/FrontOffice/Telefone';
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Perfil/FrontOffice/Telefone';


        /* Carrega View */
        $this->LoadTemplate('Template/FrontOffice/sistema/Perfil/Telefone', $this->data);

    }


    /**
    * AjaxProcessar
    *
    * @author Gustavo Botega 
    */
    public function AjaxProcessar(){


        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);    

        $ReturnAjax     =   array();

        $PrimeiroTelefone   =   $this->VerificaPrimeiroTelefone();


        $Processar  =   $this->Processar($Dados);
        if($Processar):

            $ReturnAjax['TelefoneId']   =   $Processar;
            $ReturnAjax['Criado']       =   date("d/m/Y") . " às " . date("H:i");
            $ReturnAjax['Autor']        =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $this->session->userdata('PessoaId'), 1, 'pes_nome_razao_social' );
            $ReturnAjax['Telefone']     =   $Dados['telefone'];
            $ReturnAjax['Tipo']         =   $this->model_crud->get_rowSpecific('tb_contato', 'con_id', $Dados['tipo'], 1, 'con_contato');;
            $TableRow                   =   $this->TableRow($ReturnAjax, $PrimeiroTelefone);
            $ReturnAjax['TableRow']     =   $TableRow;

        endif;

        if(!$Processar)
            $ReturnAjax     =   false;


        echo json_encode($ReturnAjax);

    }



    /**
    * Processar
    *
    * @author Gustavo Botega 
    */
    public function Processar($Dados = false){

        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);                       


        // Validacao Formulario
        $Validar                                    =   $this->Validar($Dados);
        if(!$Validar)
            return false;


        $Gravar     =   $this->Gravar($Dados);
        if(!$Gravar)
            return false;



        // Cadastro atleta finalizado com sucesso
        return $Gravar;

    }


    /**
    * Validar
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function Validar($Dados){

        // Telefone

        // Tipo


        return true;

    }



    /**
    * Gravar
    *
    * @author Gustavo Botega 
    */
    public function Gravar($Dados){

        foreach ($Dados as $key => $value) {
            if(empty($value))
                $Dados[$key]    =   NULL;
        }


        /* DDD */
        preg_match('#\((.*?)\)#', $Dados['telefone'], $match);
        $DDD = $match[1];

        /* Setando Flag */
        $FlagTelefonePrincipal  =   NULL;

        /* Telefone */
        $Telefone   =   explode(")", $Dados['telefone']);
        $Telefone   =   $Telefone[1];

        /*
            Checa se e o primeiro telfeone a ser cadastrado para marcar com o Flag de Telefone Principal.
            Se for o primeiro logicamente a ser cadastrado o valor do flag e setado como true para o telefone ser o principal.
        */
        if( $this->VerificaPrimeiroTelefone() )
            $FlagTelefonePrincipal = 1;


        // Dados
        $Dataset       =  array(
            'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
            'fk_peo_id'                         =>    $this->session->userdata('PessoaId'),
            'fk_sta_id'                         =>    1,
            'fk_con_id'                         =>    $Dados['tipo'], // tipo do telefone
            'tel_ddd'                           =>    $match[1],
            'tel_telefone'                      =>    $Telefone,
            'tel_numero'                        =>    $Dados['telefone'], 
            'tel_principal'                     =>    $FlagTelefonePrincipal,
            'criado'                            =>    date("Y-m-d H:i:s")
        );

 
       /* Query */
       $Query = $this->db->insert('tb_telefone', $Dataset);

       return ($Query) ? $this->db->insert_id() : false;
    
    }


    /**
    * TableRow
    *
    * @author Gustavo Botega 
    */
    public function TableRow($Dados, $PrimeiroTelefone){


        $TelefoneId                 =     $Dados['TelefoneId'];
        $Tipo                       =     $Dados['Tipo'];
        $Telefone                   =     $Dados['Telefone'];
        $Criado                     =     $Dados['Criado'];
        $Autor                      =     $Dados['Autor'];

        $FlagTelefonePrincipal      =     '';


        if($PrimeiroTelefone):
        $FlagTelefonePrincipal      =     '
                                            <a href="javascript:;"
                                                class="tooltips badge badge-warning bold flag-telefone-principal"
                                                title=""
                                                data-original-title="Telefone Principal. Preferencialmente vamos utilizar esse número para entrar em contato com você.">
                                                P
                                            </a>        
                                          ';
        endif;

        $Detalhes       =   "
                                <small> 

                                    <b>Cadastrado em:</b><br>
                                    ".$Criado."
                                    <hr style='margin: 7px 0;'>

                                    <b>Por:</b><br>
                                        ".$Autor." <br>

                                </small> 
        ";


        $TableRow     =   '
                            <tr id="table-row-'.$TelefoneId.'" data-id="'.$TelefoneId.'">
                                
                                <td class="text-center">

                                    '.$FlagTelefonePrincipal.'

                                    <span class="bold">'.$Tipo.':</span>
                                     '.$Telefone.'
                                </td>
                                
                                <td class="text-center">

                                    <span class="btn btn-circle btn-sm btn-success btn-gerenciar-conjunto popovers" data-container="body" onclick=" " data-html="true" data-trigger="hover" data-placement="left" data-content="
                                                '.$Detalhes.'
                                        " data-original-title="Detalhes">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        Detalhes
                                    </span>
                            ';


        if(!$PrimeiroTelefone):
        $TableRow      .=   '
                                    <a href="javascript:;"
                                        class="btn btn-circle btn-sm btn-warning btn-telefone-principal tooltips"
                                        data-id="'.$TelefoneId.'"
                                        data-original-title="Transformar esse telefone como principal."
                                    >
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                        Telefone Principal
                                    </a>
                                    
                                    <a href="javascript:;" class="btn btn-circle btn-sm btn-danger btn-deletar-telefone" data-id="'.$TelefoneId.'">
                                        <i class="fa fa-close" aria-hidden="true"></i>
                                        Deletar Telefone
                                    </a>
                            ';
        endif;



        $TableRow      .=   '
                                </td>
                            </tr>
        ';

        return $TableRow;

    }





    /**
    * VerificaTelefone
    *
    * Funcao checa se o telefone que esta sendo requisitado pertence aquela pessoa
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function VerificaTelefone($TelefoneId, $Acao = NULL){


        $PessoaId       =   $this->session->userdata('PessoaId');


        if(is_null($Acao)):
        $SqlTelefone      =   "
                                    SELECT * FROM tb_telefone as tel
                                    WHERE fk_peo_id = '".$PessoaId."' AND
                                    flag_deletado IS NULL AND
                                    tel_id = '".$TelefoneId."'
                                ";
        endif;


        if($Acao == 'Deletar'):
        $SqlTelefone      =   "
                                    SELECT * FROM tb_telefone as tel
                                    WHERE fk_peo_id = '".$PessoaId."' AND
                                    flag_deletado IS NULL AND
                                    tel_principal IS NULL AND
                                    fk_aut2_id IS NULL AND
                                    tel_id = '".$TelefoneId."'
                                ";
        endif;


        if($Acao == 'TelefonePrincipal'):
        $SqlTelefone      =   "
                                    SELECT * FROM tb_telefone as tel
                                    WHERE fk_peo_id = '".$PessoaId."' AND
                                    flag_deletado IS NULL AND
                                    tel_principal IS NULL AND
                                    fk_aut2_id IS NULL AND
                                    tel_id = '".$TelefoneId."'
                                ";
        endif;



        $Query              =   $this->db->query($SqlTelefone)->result();       


        if(empty($Query))
            return false;

        return true;

    }



    /**
    * VerificaPrimeiroTelefone
    *
    * Funcao retorna true se o telefone for o primeiro a ser cadastrado para a pessoa e false se ja exister
    * outros telefones ativos logicamente.
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function VerificaPrimeiroTelefone(){


        $PessoaId       =   $this->session->userdata('PessoaId');

        $SqlTelefone      =   "
                                    SELECT * FROM tb_telefone as tel
                                    WHERE fk_peo_id = '".$PessoaId."' AND
                                    flag_deletado IS NULL AND
                                    fk_aut2_id IS NULL
                                ";
        $Query              =   $this->db->query($SqlTelefone)->result();       


        if(empty($Query)) // Se nao retornar nenhum registra significa que para o cadastro sera o primeiro registro logico para essa pessoa
            return true;

        return false;

    }





    /**
    * Deletar
    *
    * Funcao responsavel por telefone telefone
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function Deletar(){


        $PessoaId                   =   $this->session->userdata('PessoaId');
        $TelefoneId                 =   $_POST['TelefoneId'];
        $Modificado                 =   date('Y-m-d H:i:s');

        $VerificaTelefone           =   $this->VerificaTelefone($TelefoneId, 'Deletar');
        if(!$VerificaTelefone){
            echo json_encode(false);
            return false;
        }


        $SqlTelefone                =   "   
                                            UPDATE tb_telefone
                                            SET fk_aut2_id=".$PessoaId.",
                                                flag_deletado = 1,
                                                modificado = '".$Modificado."'

                                            WHERE tel_id= ".$TelefoneId." 
                                    ";
        $Query                      =   $this->db->query($SqlTelefone);        
        if(!$Query)
            echo json_encode(false);

        echo json_encode(true);

    }




    /**
    * TelefonePrincipal
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function TelefonePrincipal(){


        $PessoaId                   =   $this->session->userdata('PessoaId');
        $TelefoneId                 =   $_POST['TelefoneId'];
        $Modificado                 =   date('Y-m-d H:i:s');

        $VerificaTelefone           =   $this->VerificaTelefone($TelefoneId, 'TelefonePrincipal');
        if(!$VerificaTelefone){
            echo json_encode(false);
            return false;
        }

        $SqlTelefone                =   "   
                                            UPDATE tb_telefone 
                                                SET tel_principal = NULL

                                            WHERE fk_peo_id= ".$PessoaId." 

                                    ";
        $Query                      =   $this->db->query($SqlTelefone);        
        if(!$Query)
            echo json_encode(false);


        $SqlTelefone                =   "   
                                            UPDATE tb_telefone
                                            SET 
                                                tel_principal = '1'

                                            WHERE tel_id= ".$TelefoneId." 

                                    ";
        $Query                      =   $this->db->query($SqlTelefone);        
        if(!$Query)
            echo json_encode(false);

        echo json_encode(true);

    }







    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    */
    public function ThemeComponent(){ 


        /* 
            StylesFile 
        */
            /*  Plugins*/
            $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
            $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete']                           = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                    = TRUE;
            $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;
        
            /*  Syles*/
               $this->data['StylesFile']['Styles']['profile']                                   = TRUE;



        /* 
            ScriptsFile 
        */
            /*  Plugins*/
            $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
            $this->data['ScriptsFile']['Plugins']['easy-autocomplete']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

            /*  Scripts*/
            $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                            = TRUE;

    }



    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function SigepeAsset()
    { 
        /* Carregando Estilos */


        /* Carregando Scripts */

    }



     /*
             // @@@ Implementar permissões por usuario
        
        // AUDITORIA:
            [0]=> Slug Module
            [1]=> Table Database
            [2]=> Column Database
            [3]=> Value Deleted
            [4]=> Folder Thumb
            [5]=> Delete Gallery
            [6]=> Table Gallery
            [7]=> Column Table Gallery
            [8]=> Data/hora
            [9]=> IP
            [10]=> ID Usuário  
            [11]=> Type of Operation  
            */

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

