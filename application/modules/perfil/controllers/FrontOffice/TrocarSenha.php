<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';
class TrocarSenha extends MY_FrontOffice {

    public $data;

    function __construct() {

        parent::__construct();

        $this->ThemeComponent(); 
        $this->SigepeAsset(); 

        $this->data['NavActiveSidebar']   = 'TrocarSenha';
        $this->data['ShowColumnLeft'] = "ProfilePessoa";



    }


    /**
    * Index
    *
    * @author Gustavo Botega 
    */
    public function index() {
		$this->Formulario();    	
    }


    /**
    * Formulario
    *
    * @author Gustavo Botega 
    */
    public function Formulario(){

        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Meu Perfil';
        $this->data['PageHeadSubtitle']   = 'Visualize e gerencie seu perfil. Mantenha seus dados sempre atualizados.';
        $this->data['Breadcrumbs']        = array();


        $PessoaId                           =   $this->session->userdata('PessoaId');
        $PessoaFisicaId                     =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'fk_pes_id', $PessoaId, 1, 'pef_id');
        $this->data['PessoaId']             =   $PessoaId;        
        $this->data['PessoaFisicaId']       =   $PessoaFisicaId;        





        $this->data['PackageStyles'][]    =   'Packages/Styles/Perfil/FrontOffice/TrocarSenha';
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Perfil/FrontOffice/TrocarSenha';


        /* Carrega View */
        $this->LoadTemplate('Template/FrontOffice/sistema/Perfil/TrocarSenha', $this->data);

    }


    /**
    * AjaxProcessar
    *
    * @author Gustavo Botega 
    */
    public function AjaxProcessar(){


        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);    

        $ReturnAjax     =   array();


        $Processar  =   $this->Processar($Dados);
        if($Processar):

            $ReturnAjax     =   true;

        endif;

        if(!$Processar)
            $ReturnAjax     =   false;


        echo json_encode($ReturnAjax);
    }



    /**
    * Processar
    *
    * @author Gustavo Botega 
    */
    public function Processar($Dados = false){

        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);                       


        // Validacao Formulario
        $Validar                                    =   $this->Validar($Dados);
        if(!$Validar)
            return false;


        $Gravar     =   $this->Gravar($Dados);
        if(!$Gravar)
            return false;


        // Senha alterada com sucesso
        return $Gravar;

    }


    /**
    * Validar
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function Validar($Dados){

        $VerificaSenhaAtual     =   $this->VerificaSenhaAtual($Dados['senha-atual']);


        // Validar se campos obrigatorios foram preenchidos e se seguem o padrao de senha definido ( to implmement )

        // Checar se a senha e o confirmar senha sao iguais ( to implement )

        // A senha nova tem que ser diferente da senha atual
        $SenhaAtual             =   $Dados['senha-atual'];
        $NovaSenha              =   $Dados['nova-senha'];
        $SenhasDiferentes       =   '';
        ($SenhaAtual != $NovaSenha ) ? $SenhasDiferentes = true : $SenhasDiferentes = false;

        return ($VerificaSenhaAtual && $SenhasDiferentes) ? true : false;

    }



    /**
    * Gravar
    *
    * @author Gustavo Botega 
    */
    public function Gravar($Dados){

        $this->load->module('autenticacao');
        $NovaSenha  =   $this->autenticacao->EncriptarSenha($Dados['nova-senha']);


        // Dados
        $Dataset       =  array(
            'pes_senha'                         =>    $NovaSenha,
            'modificado'                        =>    date("Y-m-d H:i:s")
        );

 
       /* Query */
        $PessoaId                           =   $this->session->userdata('PessoaId');
        $Query = $this->db->update('tb_pessoa', $Dataset, "pes_id = " . $PessoaId);



        if(!$Query)
            return false;

        // Destruindo sessao pro usuario realizar login com a nova senha
        $this->session->sess_destroy(); 

        return true;
    
    }





    /**
    * VerificaSenhaAtual
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function VerificaSenhaAtual($SenhaAtual){

        $this->load->module('autenticacao');
        $SenhaEncriptada  =   $this->autenticacao->EncriptarSenha($SenhaAtual);
        $PessoaId                           =   $this->session->userdata('PessoaId');


        // Comparar se senha antiga e a mesma que a atual
        $Sql      =   "
                                    SELECT * FROM tb_pessoa
                                    WHERE pes_senha = '".$SenhaEncriptada."' AND
                                    pes_id = '".$PessoaId."'
                                ";
        $Query      =   $this->db->query($Sql)->result();        


        return ($Query) ? true : false;
    }





    

    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    */
    public function ThemeComponent(){ 


        /* 
            StylesFile 
        */
            /*  Plugins*/
            $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete']                           = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                    = TRUE;
            $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;
        
            /*  Syles*/
               $this->data['StylesFile']['Styles']['profile']                                = TRUE;



        /* 
            ScriptsFile 
        */
            /*  Plugins*/
            $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
            $this->data['ScriptsFile']['Plugins']['easy-autocomplete']                        = TRUE;
            $this->data['ScriptsFile']['Scripts']['sweetalert2']                              = TRUE;

    }



    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function SigepeAsset()
    { 
        /* Carregando Estilos */


        /* Carregando Scripts */

    }



     /*
             // @@@ Implementar permissões por usuario
        
        // AUDITORIA:
            [0]=> Slug Module
            [1]=> Table Database
            [2]=> Column Database
            [3]=> Value Deleted
            [4]=> Folder Thumb
            [5]=> Delete Gallery
            [6]=> Table Gallery
            [7]=> Column Table Gallery
            [8]=> Data/hora
            [9]=> IP
            [10]=> ID Usuário  
            [11]=> Type of Operation  
            */

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

