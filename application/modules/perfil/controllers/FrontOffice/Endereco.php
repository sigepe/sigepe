<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';
class Endereco extends MY_FrontOffice {

    public $data;

    function __construct() {

        parent::__construct();

        $this->ThemeComponent(); 
        $this->SigepeAsset(); 

        $this->data['NavActiveSidebar']   = 'Endereco';
        $this->data['ShowColumnLeft'] = "ProfilePessoa";



    }


    /**
    * Index
    *
    * @author Gustavo Botega 
    */
    public function index() {
		$this->Formulario();    	
    }


    /**
    * Formulario
    *
    * @author Gustavo Botega 
    */
    public function Formulario(){

        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Meu Perfil';
        $this->data['PageHeadSubtitle']   = 'Visualize e gerencie seu perfil. Mantenha seus dados sempre atualizados.';
        $this->data['Breadcrumbs']        = array();


        $PessoaId                           =   $this->session->userdata('PessoaId');
        $PessoaFisicaId                     =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'fk_pes_id', $PessoaId, 1, 'pef_id');
        $this->data['PessoaId']             =   $PessoaId;        
        $this->data['PessoaFisicaId']       =   $PessoaFisicaId;        


        $SqlEndereco      =   "
                                    SELECT * FROM tb_endereco as end
                                    WHERE fk_peo_id = '".$PessoaId."' AND
                                    flag_deletado IS NULL
                                    ORDER BY end_principal DESC, end_id DESC
                                ";
        $this->data['DatasetEndereco']      =   $this->db->query($SqlEndereco)->result();        


        $SqlTipoEndereco      =   "
                                    SELECT * FROM tb_contato 
                                        WHERE con_id IN (2,3)
                                    ORDER BY con_id ASC
                                ";
        $this->data['DatasetTipoEndereco']      =   $this->db->query($SqlTipoEndereco)->result();        


        $SqlEstado            =   "
                                    SELECT * FROM tb_estado 
                                    ORDER BY est_nome ASC
                                ";
        $this->data['DatasetEstado']      =   $this->db->query($SqlEstado)->result();        



        $this->data['PackageStyles'][]    =   'Packages/Styles/Perfil/FrontOffice/Endereco';
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Perfil/FrontOffice/Endereco';


        /* Carrega View */
        $this->LoadTemplate('Template/FrontOffice/sistema/Perfil/Endereco', $this->data);

    }


    /**
    * AjaxProcessar
    *
    * @author Gustavo Botega 
    */
    public function AjaxProcessar(){


        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);    

        $ReturnAjax     =   array();

        $PrimeiroEndereco   =   $this->VerificaPrimeiroEndereco();


        $Processar  =   $this->Processar($Dados);
        if($Processar):

            $ReturnAjax['EnderecoId']   =   $Processar;
            $ReturnAjax['Criado']       =   date("d/m/Y") . " às " . date("H:i");
            $ReturnAjax['Autor']        =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $this->session->userdata('PessoaId'), 1, 'pes_nome_razao_social' );
            $ReturnAjax['Cep']          =   $Dados['cep'];
            $ReturnAjax['Estado']       =   $Dados['estado'];
            $ReturnAjax['Cidade']       =   $Dados['cidade'];
            $ReturnAjax['Bairro']       =   $Dados['bairro'];
            $ReturnAjax['Logradouro']   =   $Dados['logradouro'];
            $ReturnAjax['Numero']       =   $Dados['numero'];
            $ReturnAjax['Complemento']  =   $Dados['complemento'];
            $ReturnAjax['Tipo']         =   $this->model_crud->get_rowSpecific('tb_contato', 'con_id', $Dados['tipo'], 1, 'con_contato');


            $TableRow                   =   $this->TableRow($ReturnAjax, $PrimeiroEndereco);
            $ReturnAjax['TableRow']     =   $TableRow;

        endif;

        if(!$Processar)
            $ReturnAjax     =   false;


        echo json_encode($ReturnAjax);

    }



    /**
    * Processar
    *
    * @author Gustavo Botega 
    */
    public function Processar($Dados = false){

        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);                       


        // Validacao Formulario
        $Validar                                    =   $this->Validar($Dados);
        if(!$Validar)
            return false;


        $Gravar     =   $this->Gravar($Dados);
        if(!$Gravar)
            return false;



        // Cadastro atleta finalizado com sucesso
        return $Gravar;

    }


    /**
    * Validar
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function Validar($Dados){

        // Endereco

        // Tipo


        return true;

    }



    /**
    * Gravar
    *
    * @author Gustavo Botega 
    */
    public function Gravar($Dados){

        foreach ($Dados as $key => $value) {
            if(empty($value))
                $Dados[$key]    =   NULL;
        }



        /* Setando Flag */
        $FlagEnderecoPrincipal  =   NULL;

        /*
            Checa se e o primeiro endereco a ser cadastrado para marcar com o Flag de Endereco Principal.
            Se for o primeiro logicamente a ser cadastrado o valor do flag e setado como true para o endereco ser o principal.
        */
        if( $this->VerificaPrimeiroEndereco() )
            $FlagEnderecoPrincipal = 1;


        // Removendo o hifen do CEP pra gravar no banco
        $Cep    =   str_replace(["-"], '', $Dados['cep']);


        // Dados
        $Dataset       =  array(
            'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
            'fk_peo_id'                         =>    $this->session->userdata('PessoaId'),
            'fk_sta_id'                         =>    1,
            'fk_con_id'                         =>    $Dados['tipo'], // tipo do endereco
            'fk_est_id'                         =>    $Dados['estado'],
            'fk_cid_id'                         =>    $Dados['cidade'],
            'end_cep'                           =>    $Cep,
            'end_bairro'                        =>    $Dados['bairro'],
            'end_logradouro'                    =>    $Dados['logradouro'],
            'end_numero'                        =>    $Dados['numero'],
            'end_complemento'                   =>    $Dados['complemento'],

            'end_principal'                     =>    $FlagEnderecoPrincipal,
            'criado'                            =>    date("Y-m-d H:i:s")
        );


 
       /* Query */
       $Query = $this->db->insert('tb_endereco', $Dataset);

       return ($Query) ? $this->db->insert_id() : false;
    
    }


    /**
    * TableRow
    *
    * @author Gustavo Botega 
    */
    public function TableRow($Dados, $PrimeiroEndereco){


        $EnderecoId                 =     $Dados['EnderecoId'];


        $Cep                        =     $Dados['Cep'];
        $EstadoId                   =     $Dados['Estado'];
        $Estado                     =     $this->model_crud->get_rowSpecific('tb_estado', 'est_id', $Dados['Estado'], 1, 'est_nome');
        $CidadeId                   =     $Dados['Cidade'];
        $Cidade                     =     $this->model_crud->get_rowSpecific('tb_cidade', 'cid_id', $Dados['Cidade'], 1, 'cid_nome');
        $Bairro                     =     $Dados['Bairro'];
        $Logradouro                 =     $Dados['Logradouro'];
        $Numero                     =     $Dados['Numero'];
        $Complemento                =     $Dados['Complemento'];
        $Tipo                       =     $Dados['Tipo'];



        $Criado                     =     $Dados['Criado'];
        $Autor                      =     $Dados['Autor'];

        $FlagEnderecoPrincipal         =     '';


        if($PrimeiroEndereco):
        $FlagEnderecoPrincipal      =     '
                                            <a href="javascript:;"
                                                class="tooltips badge badge-warning bold flag-endereco-principal"
                                                title=""
                                                data-original-title="E-mail Principal. Preferencialmente vamos utilizar esse e-mail para entrar em contato com você.">
                                                P
                                            </a>        
                                          ';
        endif;

        $Detalhes       =   "
                                <small> 

                                    <b>Cadastrado em:</b><br>
                                    ".$Criado."
                                    <hr style='margin: 7px 0;'>

                                    <b>Por:</b><br>
                                        ".$Autor." <br>

                                </small> 
        ";


        $TableRow     =   '
                            <tr id="table-row-'.$EnderecoId.'" data-id="'.$EnderecoId.'">
                                
                                <td class="text-left">

                                    '.$FlagEnderecoPrincipal.'

                                    <span class="bold">Endereço '.$Tipo.':</span><br>

                                     <small>
                                        <b>CEP:</b> '.$Cep.' <br>
                                        '.$Estado.' / '.$Cidade.' <br>
                                        '.$Bairro.' - '.$Logradouro.' - '.$Numero.' <br>
                                        '.$Complemento.' 
                                     </small>

                                </td>
                                

                                <td class="text-center">

                                    <span class="btn btn-circle btn-sm btn-success btn-gerenciar-conjunto popovers" data-container="body" onclick=" " data-html="true" data-trigger="hover" data-placement="left" data-content="
                                                '.$Detalhes.'
                                        " data-original-title="Detalhes">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        Detalhes
                                    </span>
                            ';


        if(!$PrimeiroEndereco):
        $TableRow      .=   '
                                    <a href="javascript:;"
                                        class="btn btn-circle btn-sm btn-warning btn-endereco-principal tooltips"
                                        data-id="'.$EnderecoId.'"
                                        data-original-title="Transformar esse endereço como principal."
                                    >
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                        Endereço Principal
                                    </a>
                                    
                                    <a href="javascript:;" class="btn btn-circle btn-sm btn-danger btn-deletar-endereco" data-id="'.$EnderecoId.'">
                                        <i class="fa fa-close" aria-hidden="true"></i>
                                        Deletar Endereço
                                    </a>
                            ';
        endif;



        $TableRow      .=   '
                                </td>
                            </tr>
        ';

        return $TableRow;

    }





    /**
    * VerificaEndereco
    *
    * Funcao checa se o endereço que esta sendo requisitado pertence aquela pessoa
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function VerificaEndereco($EnderecoId, $Acao = NULL){


        $PessoaId       =   $this->session->userdata('PessoaId');


        if(is_null($Acao)):
        $SqlEndereco       =   "
                                    SELECT * FROM tb_endereco as end
                                    WHERE fk_peo_id = '".$PessoaId."' AND
                                    flag_deletado IS NULL AND
                                    end_id = '".$EnderecoId."'
                                ";
        endif;


        if($Acao == 'Deletar'):
        $SqlEndereco      =   "
                                    SELECT * FROM tb_endereco as end
                                    WHERE fk_peo_id = '".$PessoaId."' AND
                                    flag_deletado IS NULL AND
                                    end_principal IS NULL AND
                                    fk_aut2_id IS NULL AND
                                    end_id = '".$EnderecoId."'
                                ";
        endif;


        if($Acao == 'EnderecoPrincipal'):
        $SqlEndereco      =   "
                                    SELECT * FROM tb_endereco as end
                                    WHERE fk_peo_id = '".$PessoaId."' AND
                                    flag_deletado IS NULL AND
                                    end_principal IS NULL AND
                                    fk_aut2_id IS NULL AND
                                    end_id = '".$EnderecoId."'
                                ";
        endif;



        $Query              =   $this->db->query($SqlEndereco)->result();       


        if(empty($Query))
            return false;

        return true;

    }



    /**
    * VerificaPrimeiroEndereco
    *
    * Funcao retorna true se o endereco for o primeiro a ser cadastrado para a pessoa e false se ja exister
    * outros endereco ativos logicamente.
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function VerificaPrimeiroEndereco(){


        $PessoaId       =   $this->session->userdata('PessoaId');

        $SqlEndereco    =   "
                                    SELECT * FROM tb_endereco as end
                                    WHERE fk_peo_id = '".$PessoaId."' AND
                                    flag_deletado IS NULL AND
                                    fk_aut2_id IS NULL
                                ";
        $Query              =   $this->db->query($SqlEndereco)->result();       


        if(empty($Query)) // Se nao retornar nenhum registra significa que para o cadastro sera o primeiro registro logico para essa pessoa
            return true;

        return false;

    }





    /**
    * Deletar
    *
    * Funcao responsavel por endereco
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function Deletar(){

        $PessoaId                   =   $this->session->userdata('PessoaId');
        $EnderecoId                    =   $_POST['EnderecoId'];
        $Modificado                 =   date('Y-m-d H:i:s');

        $VerificaEndereco              =   $this->VerificaEndereco($EnderecoId, 'Deletar');
        if(!$VerificaEndereco){
            echo json_encode(false);
            return false;
        }


        $SqlEndereco                   =   "   
                                            UPDATE tb_endereco
                                            SET fk_aut2_id=".$PessoaId.",
                                                flag_deletado = 1,
                                                modificado = '".$Modificado."'

                                            WHERE end_id= ".$EnderecoId." 
                                    ";
        $Query                      =   $this->db->query($SqlEndereco);        
        if(!$Query)
            echo json_encode(false);

        echo json_encode(true);

    }




    /**
    * EnderecoPrincipal
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function EnderecoPrincipal(){


        $PessoaId                   =   $this->session->userdata('PessoaId');
        $EnderecoId                 =   $_POST['EnderecoId'];
        $Modificado                 =   date('Y-m-d H:i:s');

        $VerificaEndereco           =   $this->VerificaEndereco($EnderecoId, 'EnderecoPrincipal');
        if(!$VerificaEndereco){
            echo json_encode(false);
            return false;
        }

        $SqlEndereco                   =   "   
                                            UPDATE tb_endereco 
                                                SET end_principal = NULL

                                            WHERE fk_peo_id= ".$PessoaId." 

                                    ";
        $Query                      =   $this->db->query($SqlEndereco);        
        if(!$Query)
            echo json_encode(false);


        $SqlEndereco                   =   "   
                                            UPDATE tb_endereco
                                            SET 
                                                end_principal = '1'

                                            WHERE end_id = ".$EnderecoId." 

                                    ";
        $Query                      =   $this->db->query($SqlEndereco);        
        if(!$Query)
            echo json_encode(false);

        echo json_encode(true);

    }






    /**
    * ConsultaCidadeAjax
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function ConsultaCidadeAjax(){

        header( 'Cache-Control: no-cache' );
        header( 'Content-type: application/xml; charset="utf-8"', true );


        $EstadoId   =   $_POST['EstadoId'];
        $Cidades    =   array();

        $SqlCidades = "SELECT *
            FROM tb_cidade
            WHERE fk_est_id=$EstadoId
            ORDER BY cid_nome";
        $Query                      =   $this->db->query($SqlCidades)->result();        

        foreach ($Query as $key => $value) {
          $Cidades[] = array(
            'CidadeId'  => $value->cid_id,
            'CidadeNome'  => $value->cid_nome,
          );
        }

        echo json_encode($Cidades);

    }







    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    */
    public function ThemeComponent(){ 


        /* 
            StylesFile 
        */
            /*  Plugins*/
            $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
            $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete']                           = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                    = TRUE;
            $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;
        
            /*  Syles*/
               $this->data['StylesFile']['Styles']['profile']                                = TRUE;



        /* 
            ScriptsFile 
        */
            /*  Plugins*/
            $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
            $this->data['ScriptsFile']['Plugins']['easy-autocomplete']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

            /*  Scripts*/
            $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                            = TRUE;

    }



    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function SigepeAsset()
    { 
        /* Carregando Estilos */


        /* Carregando Scripts */

    }



     /*
             // @@@ Implementar permissões por usuario
        
        // AUDITORIA:
            [0]=> Slug Module
            [1]=> Table Database
            [2]=> Column Database
            [3]=> Value Deleted
            [4]=> Folder Thumb
            [5]=> Delete Gallery
            [6]=> Table Gallery
            [7]=> Column Table Gallery
            [8]=> Data/hora
            [9]=> IP
            [10]=> ID Usuário  
            [11]=> Type of Operation  
            */

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

