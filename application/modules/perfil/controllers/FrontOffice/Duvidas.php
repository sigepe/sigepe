<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';
class Duvidas extends MY_FrontOffice {

    public $data;

    function __construct() {

        parent::__construct();

        $this->ThemeComponent(); 
        $this->SigepeAsset(); 

        $this->data['NavActiveSidebar']   = 'Duvidas';
        $this->data['ShowColumnLeft'] = "ProfilePessoa";



    }


    /**
    * Index
    *
    * @author Gustavo Botega 
    */
    public function index() {


        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Meu Perfil';
        $this->data['PageHeadSubtitle']   = 'Visualize e gerencie seu perfil. Mantenha seus dados sempre atualizados.';
        $this->data['Breadcrumbs']        = array();


        $PessoaId                           =   $this->session->userdata('PessoaId');
        $PessoaFisicaId                     =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'fk_pes_id', $PessoaId, 1, 'pef_id');
        $this->data['PessoaId']             =   $PessoaId;        
        $this->data['PessoaFisicaId']       =   $PessoaFisicaId;        





        $this->data['PackageStyles'][]    =   'Packages/Styles/Perfil/FrontOffice/TrocarSenha';
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Perfil/FrontOffice/TrocarSenha';


        /* Carrega View */
        $this->LoadTemplate('Template/FrontOffice/sistema/Perfil/Duvidas', $this->data);

    }


    

    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    */
    public function ThemeComponent(){ 


        /* 
            StylesFile 
        */
            /*  Plugins*/
            $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete']                           = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                    = TRUE;
            $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;
        
            /*  Syles*/
               $this->data['StylesFile']['Styles']['profile']                                = TRUE;



        /* 
            ScriptsFile 
        */
            /*  Plugins*/
            $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
            $this->data['ScriptsFile']['Plugins']['easy-autocomplete']                        = TRUE;
            $this->data['ScriptsFile']['Scripts']['sweetalert2']                              = TRUE;

    }



    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function SigepeAsset()
    { 
        /* Carregando Estilos */


        /* Carregando Scripts */

    }



     /*
             // @@@ Implementar permissões por usuario
        
        // AUDITORIA:
            [0]=> Slug Module
            [1]=> Table Database
            [2]=> Column Database
            [3]=> Value Deleted
            [4]=> Folder Thumb
            [5]=> Delete Gallery
            [6]=> Table Gallery
            [7]=> Column Table Gallery
            [8]=> Data/hora
            [9]=> IP
            [10]=> ID Usuário  
            [11]=> Type of Operation  
            */

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

