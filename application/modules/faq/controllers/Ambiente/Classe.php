<?php

if (!defined('BASEPATH')) { exit('No direct script access allowed'); }
    
class Classe extends MY_BackOffice {

    public $data;
    public $ambiente =  "Ambiente" . "Office";
    public $modulo = "modulo";
    public $tabelaNome = "tb_tabela";
    public $viewNome = "vw_tabela";
    public $columnId = "tab_id";
    public $columnDesc = "tab_desc";    

    function __construct() {

        parent::__construct();

        $this->metronicAsset();
        $this->sigepeAsset();

        $this->data['ShowColumnLeft']   =  "Profile" . "x";
        $this->data['base_dir'] = "Template/BackOffice/sistema/$this->$modulo/";
        $this->data['Class']    =   $this->router->fetch_class();

        /* Controladores Externos */
#        $this->load->module('evento/BackOffice/Evento');

    }



    public function index() {
        $this->Listagem();
    }



    /**
    * classe_browse_bread.
    *
    *
    * @author {Author}
    *
    * @return view
    */
    public function classe_browse_bread($contextoId) {
        
        $sql = "SELECT * FROM $this->tabelaNome";
        $dataset = $this->db->query($sql)->result();

        $this->data['dataset'] = $dataset;

        /* View */
        $this->LoadTemplate($this->data['base_dir'].__CLASS__."/".__FUNCTION__, $this->data);

    } // fim do metodo @classe_browse_bread


    
    /**
    * classe_read
    *
    * @author {Author}
    *
    * @return view
    */
    public function classe_read($contextoId) {

        $sql = "";
        $dataset = $this->db->query($sql)->result();

        $this->data['dataset'] = $dataset;

        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);

    } // fim do metodo @classe_read



    /**
    * classe_add
    *
    * @author {Author}
    *
    * @return 
    */
    public function classe_add($eventoId) {

        /* Setando variaveis de ambiente */
        $this->data['PageHeadTitle']      = 'Cadastro';
        $this->data['PageHeadSubtitle']   = 'Para cadastrar uma classe no SIGEPE preencha o formulário abaixo.';

        /* View */ $this->LoadTemplate($this->data['base_dir'].__CLASS__."/".__FUNCTION__, $this->data);

    } // fim do méotodo @classe_add



    /* # Fim Bread
       # Inicio Middleware
    ====================================================================================================================*/



    /**
    * processar.
    *
    * -
    *
    * @author {Author}
    *
    * @param $dataJson
    *
    * @return boolean
    */
    public function processar($dataJson = false) {

        if(isset($_POST['dataJson']) && is_array($_POST['dataJson'])) {
            $dados = $_POST['dataJson'];
        }

        if($dataJson) {
            $dados = $dataJson;
        }

        if(!isset($_POST['dataJson']) && !$dataJson) {
            echo json_encode(false);
        }

        // Validacao Formulario
        $validar = $this->validar($dados);
        if (!$validar) {
            echo json_encode(false);
            return false;
        }

        // Se existir algum ID por convencao entende-se que o metodo a ser direcionado e de atualizacao.
        if(!isset($dados['id'])) {
            $resultadoAcao = $this->gravar($dados);
            echo ($resultadoAcao) ? json_encode(true) : json_encode(false);
        } else {
            $resultadoAlterar = $this->alterar($dados);
            echo ($resultadoAlterar) ? json_encode(true) : json_encode(false);
        }

    } // fim do metodo @processar




    /**
    * gravar.
    *
    * -
    *
    * @author {Author}
    *
    * @param $dados
    *
    * @return boolean
    */
    public function gravar($dados) {

        foreach ($dados as $key => $value) {
            if(empty($value))
                $dados[$key]    =   NULL;
        }

        // dados
        $dataset       =  array(
            'fk_aut_id'             => $this->session->userdata('PessoaId'),
            'tab_coluna'            => gerarTimeStamp("tb_tabela", "tab_coluna"),
            'eve_slug'              => gerarSlug($dados['eve-nome-evento']),
            'criado'                => date("Y-m-d H:i:s")
        );


        /* query */
        $query = $this->db->insert('tb_tabela', $dataset);

        if ($query){
            $classeId = $this->db->insert_id();
            return true;
        } else {
            return false;
        }

    } // fim do metodo @gravar



    /**
    * alterar.
    *
    * -
    *
    * @author {Author}
    *
    * @param $dados
    *
    * @return boolean
    */
    public function alterar($dados) {

        (!isset($dados['campo_null'])) ?  $dados['campo_null'] = null : '';

        // dados
        $dataset       =  array(
            'tb_id'                 => $dados['id'],
            'fk_aut_id'             => $this->session->userdata('PessoaId'),
            'tab_coluna'            => gerarTimeStamp("tb_tabela", "tab_coluna"),
            'eve_slug'              => gerarSlug($dados['name-objeto']),
            'criado'                => date("Y-m-d H:i:s")
        );

        /* Query */
        $query = $this->db->update('tb_tabela', $dataset, array('tb_id' => $dados['id'] ));
        return $query;

    } // fim do metodo @alterar



    /**
    * validar.
    *
    * -
    *
    * @author {Author}
    *
    * @param $dados
    *
    * @return boolean
    */
    public function validar($dados) {
        return true; // Validar inputs e regras de negocio do formulario. Tratar erro.
    } // fim do metodo @validar



    /* # Fim Middleware
       # Inicio Acessório
    ====================================================================================================================*/



    /**
    * getAlgumaCoisa.
    *
    * -
    *
    * @author {Author}
    *
    * @param $contextoId
    *
    * @return boolean
    */
    public function getAlgumaCoisa($contextoId) {

        $SqlCategoriasDaSerie = "
            SELECT * FROM tb_evento_rel_serie_rel_categoria as src
            WHERE fk_ers_id  = $serieId
            ORDER BY fk_evc_id desc";

       return $this->db->query($SqlCategoriasDaSerie)->result();

    }



    /* # Fim Metodo Acessório
       # Inicio metodo Controladores
    =========================================================================*/

    public function obterClasse($contextoId) {
        return $this->model_crud->get_rowSpecificObject('tb_tabela', 'tb_id', $contextoId);
    } // fim do metodo @obterSerie


    /* # Fim Metodo Controladores
       # Inicio Asset 
    =========================================================================*/
    /**
    * metronicAsset.
    *
    * -
    *
    * @author {Author}
    *
    * @param 
    *
    * @return
    */
    public function metronicAsset() {

        /* STYLES
        -----------------------------------*/
        // PLUGINS  //
        /* {NamePackage} */
        $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                  = TRUE;

        // STYLES  //
        /* {NamePackage} */
        $this->data['StylesFile']['Styles']['todo-2']                                     = TRUE;


        /* SCRIPTS
        -----------------------------------*/
        // PLUGINS  //
        /* {NamePackage} */
        $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;

        // SCRIPTS //
        $this->data['ScriptsFile']['Scripts']['components-editors']                       = TRUE;

    } // fim do metodo MetronicAsset



    /**
    * sigepeAsset.
    *
    * -
    *
    * @author {Author}
    *
    * @param 
    *
    * @return
    */
    public function sigepeAsset() {
    
        /* Carregando Estilos */
        $this->data['PackageStyles'][]   =   "Packages/Styles/$this->modulo/$this->ambiente/Arquivo";
        
        /* Carregando Scripts */
        $this->data['PackageScripts'][]   =   "Packages/Scripts/$this->modulo/$this->ambiente/Arquivo";
    
    } // fim do metodo sigepeAsset


} /* End of file classe.php */
