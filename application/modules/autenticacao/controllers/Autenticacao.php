<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Autenticacao extends MX_Controller {

    public $data;

    function __construct() {
        parent::__construct();

        $this->db->reconnect();

        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
    }


    public function index() {
		$this->Login();
    }


    /***********************************************
      Login
    ***********************************************/
    public function Login($idEvento=null) {

        if(!is_null($idEvento)){
            // Registrar sessao
        }

        $this->UsuarioAutenticado($idEvento);
        $this->LoadAuthentication('Template/GuestOffice/Autenticacao/Formulario', $this->data);
    }



    /***********************************************
      UsuarioAutenticado
    ***********************************************/
    public function UsuarioAutenticado($idEvento=null) {



        if($this->session->userdata('Autenticado')){

            // tratar caso id evento
            if($idEvento == null){
                redirect(base_url().'dashboard');
            }else{
                redirect(base_url().'evento/FrontOffice/FaturaGlobal/Dashboard/'.$idEvento);
            }


        }

    }






    /***********************************************
      GerarSenhaCriptografada
    ***********************************************/
    public function GerarSenhaCriptografada($Senha) {

        /**
         * Setando valor do $token
         */
        $Token              =   '7f6e1612-cf50-4394-8445-235f99392fca'; // Criar module seguranca e puxar valor do token de la > 7f6e1612-cf50-4394-8445-235f99392fca
        $SenhaToken         =   $Token . $Senha;


        /**
         * Encriptando senha
         */
        $SenhaEncriptada  =   md5($SenhaToken);
        echo $SenhaEncriptada;

    }



    /**
     * Funcao recebe uma requisicao Ajax.
     * Atencao: a funcao nao recebe nenhum valor, porem sao resgatados por $_POST
     * A funcao recebe o valor do cpf e verifica se existe alguma ocorrencia na tabela tb_pessoa
     * se existir ele retorna com algumas informacoes.
     *
     * @author Gustavo Botega
     * @param $_POST['cpfWithoutMask'] - recebe valor do CPF com o valor formatado. Valor contem 11 digitos.
     * @return string JSON
     */
    public function verifyUser() {


        /* CPF */
        $cpf    =   $_POST['cpfWithoutMask'];

        // Checar se existe na tabela tb_pessoa alguem com o cpf informado
        $query = $this->model_crud->select('tb_pessoa', array('pes_id', 'fk_sta_id', 'pes_nome', 'pes_cpf', 'pes_foto'), array('pes_cpf ='=>$cpf), NULL, 1 );

        /* Verificacao da consulta*/
        if( !empty($query) ){

            $array = array(
                'pes_id'        =>  $query[0]->pes_id,
                'fk_sta_id'     =>  $query[0]->fk_sta_id,
                'pes_nome'      =>  $query[0]->pes_nome,
                'pes_cpf'       =>  $query[0]->pes_cpf,
                'pes_foto'      =>  $query[0]->pes_foto,
                'status'        =>  'processed'
            );

        }else{

            $array = array(
                'status'    =>  'unprocessed'
            );

        }


        /**
         * Saida
         */
        echo json_encode($array);

    }



    /**
     * Funcao recebe uma requisicao Ajax.
     * Atencao: a funcao nao recebe nenhum valor, porem sao resgatados por $_POST
     * A funcao recebe o valor do cpf e verifica se existe alguma ocorrencia na tabela tb_pessoa
     * se existir ele retorna com algumas informacoes.
     *
     * @author Gustavo Botega
     * @param $_POST['cpfWithoutMask'] - recebe valor do CPF com o valor formatado. Valor contem 11 digitos.
     * @return string JSON
     */
    public function Processar() {

        $ArrayDataSerialized        = $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $DataUnserialized);


        if( !isset($DataUnserialized['cpf'] ) )
            $DataUnserialized['cpf'] = $DataUnserialized['usuario'];


        /**
         * Resgatando valores por $_POST e armazenando em variaveis
         */
        $Cpf         =   $this->my_pessoa->RemoverPontuacaoCpfCnpj($DataUnserialized['cpf']);
        $Senha       =   $DataUnserialized['senha'];



        /**
         * Setando valor do $token
         */
        $Token              =   '7f6e1612-cf50-4394-8445-235f99392fca'; // Criar module seguranca e puxar valor do token de la > 7f6e1612-cf50-4394-8445-235f99392fca
        $SenhaToken         =   $Token . $Senha;


        /**
         * Encriptando senha
         */
        $SenhaEncriptada  =   md5($SenhaToken);


        /**
         * SQL
         */
        $Sql    =   "
                        SELECT * FROM tb_pessoa
                        WHERE pes_cpf_cnpj = '".$Cpf."'
                        AND pes_senha = '".$SenhaEncriptada."'
                        LIMIT 1
                    ";
        $Query  =   $this->db->query($Sql)->result();



        /**
         * Checa se a query nao retornou vazio
         */
        if( !empty($Query) && $Query ){

            /**
             * Apenas e registrado a sessao se o status do USUARIO for ATIVO.
             */
            if($Query[0]->fk_sta_id=='1' && $Query[0]->flag_usuario == '1'){
                $this->SetSession(
                    $Query[0]->pes_id,
                    $Query[0]->pes_cpf_cnpj,
                    $Query[0]->pes_foto,
                    $Query[0]->pes_nome_razao_social,
                    $Query[0]->flag_usuario
                );
            }

            $array = array(
                'fk_sta_id'     =>  $Query[0]->fk_sta_id, // status do usuario.
                'Status'        =>  TRUE,
                'FlagUsuario'   =>  $Query[0]->flag_usuario
            );

        }else{

            $array = array(
                'Status'        =>  FALSE
            );

        }


        /**
         * Saida
         */
        echo json_encode($array);

    }



    public function settings()
    {

        $data           = array();
        $data['token']  =   't7nr';

        return $data;
    }



    public function GetToken(){

        return  '7f6e1612-cf50-4394-8445-235f99392fca';
    }






    /**
     * Se CPF/CNPJ existir retorna true. Se não false.
     *
     * @author Gustavo Botega
     * @param $_POST['cpfWithoutMask'] - recebe valor do CPF com o valor formatado. Valor contem 11 digitos.
     * @return string JSON
     */
    public function CpfCnpjExiste( $CpfCnpj = NULL ) {


        $Cpf    =   $this->my_pessoa->RemoverPontuacaoCpfCnpj($CpfCnpj);

        $Sql    =   "
                                SELECT pes_id, pes_cpf_cnpj FROM tb_pessoa
                                    WHERE pes_cpf_cnpj = ".$CpfCnpj."
                            ";
        $Query  =   $this->db->query($Sql)->result();


        if( count($Query) == '1' )
            return true;

        if( count($Query) > '1' ) // Criar log para o administrador do banco notificando varias ocorrencias na coluna pes_cpf_cnpj
            return false;

        if( count($Query) == '0' || empty($Query) )
            return false;


    }




    /**
     *
     * @author Gustavo Botega
     * @param
     * @return string JSON
     */
    public function GetUsuarioPossuiPermissao( $CpfCnpj = NULL ) {


        $CpfCnpj        =   $this->my_pessoa->RemoverPontuacaoCpfCnpj($CpfCnpj);
        $FlagUsuario    =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_cpf_cnpj', $CpfCnpj, 1, 'flag_usuario');



        return $FlagUsuario;

    }





    /**
     * Set Session
     *
     *
     * @author Gustavo Botega
     * @param int $userId -
     * @return TRUE
     */
    public function SetSession($PessoaId, $PessoaCpf, $PessoaFoto, $PessoaNomeCompleto, $FlagUsuario)
    {


        $this->load->module('pessoa');
        $Perfis                 =   $this->pessoa->GetPerfil($PessoaId);
        $Empresas               =   $this->pessoa->GetEmpresas($PessoaId);
        $PessoaFisicaId         =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'fk_pes_id', $PessoaId, 1, 'pef_id');


        /* RETORNAR ARRAY COM OS DADOS */
        $Array = array(
            'PessoaId'            =>      $PessoaId,
            'PessoaFisicaId'      =>      $PessoaFisicaId,
            'PessoaCpf'           =>      $PessoaCpf,
            'PessoaFoto'          =>      $PessoaFoto,
            'PessoaPrimeiroNome'  =>      $this->my_pessoa_fisica->GetPrimeiroNome($PessoaNomeCompleto),
            'PessoaFlagUsuario'   =>      $FlagUsuario,
            'TokenAutenticacao'   =>      $this->TokenAutenticacao(),
            'Perfis'              =>      $Perfis,
            'Empresas'            =>      $Empresas,
//            'Vinculos'            =>      $this->GetVinculos(),
            'Autenticado'         =>      TRUE
        );

        $this->session->set_userdata($Array);

        return TRUE;
    }









    public function Logout()
    {

        // # destruindo sessao do code Igniter
        $this->session->sess_destroy();

        // O usuario seve ser redirecionado pra a tela de login .
        redirect(base_url() . 'login', 'refresh');

    }




    /**
    * EncriptarSenha
    *
    *
    * @author Gustavo Botega
    */
    public function EncriptarSenha($Senha)
    {

        $Token          =   $this->GetToken();
        return md5($Token.$Senha);

    }






    /**
     * Carrega a estrutura da pagina de login
     *
     * @param string $View
     */
    public function LoadAuthentication( $View )
    {
        $data = array();

        $this->data['base_url']   =   base_url();

        $this->parser->parse('Template/GuestOffice/Autenticacao/Header', $this->data);
        $this->parser->parse( $View, $this->data);
        $this->parser->parse('Template/GuestOffice/Autenticacao/Footer', $this->data);
    }


    public function TokenAutenticacao(){
        return 'aPQZryakzHoTh6JLCL2E';
    }



}
