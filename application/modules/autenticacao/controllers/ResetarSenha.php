<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ResetarSenha extends MX_Controller {

    public $data;

    function __construct() {
        parent::__construct();

        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
    }


    public function index() {
		$this->Login();    	
    }


    /***********************************************    
      Login
    ***********************************************/
    public function Processar() {

        // Resgatando o CPF informado na pagina de resgatar senha
        $CpfCnpj    =   $_POST['CpfCnpj'];
        $CpfCnpj    =   $this->my_pessoa->RemoverPontuacaoCpfCnpj($CpfCnpj);


        // Carregando o modulo de autenticacao
        $this->load->module('autenticacao/Autenticacao');


        // Checar se CPF/CNPJ existe
        if( !$this->autenticacao->CpfCnpjExiste( $CpfCnpj ) ){
            echo json_encode(array('Status' => false, 'Erro' => 'CPF/CNPJ não cadastrado no sistema.' ));
            return false;
        }


        // Obtendo o ID da pessoa
        $PessoaId   =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_cpf_cnpj', $CpfCnpj, 1, 'pes_id');


        // Checar se o flag_usuario esta ativo
        $FlagUsuario    =   $this->autenticacao->GetUsuarioPossuiPermissao( $CpfCnpj );
        if(is_null($FlagUsuario)){
            echo json_encode(array('Status' => false, 'Erro' => 'CPF/CNPJ Não possui usuário ativo.' ));
            return false;            
        }


        // Checar Status do usuario esta bloqueado ou outro que nao permita acesso
        /*
        # Implementar essa funcao. GMB2018#CFI
        $GetStatusUsuario    =   $this->autenticacao->GetStatusUsuario( $Cpf );
        if(is_null($GetStatusUsuario))
            return json_encode(array('Status' => false, 'Erro' => 'CPF/CNPJ Não possui usuário ativo.' ));
        */


        // Obtendo Email
        $EmailPrincipal  =   $this->GetEmailPrincipal( $PessoaId );
        if(empty($EmailPrincipal)){
            echo json_encode(array('Status' => false, 'Erro' => 'Não existe nenhum email associado a sua conta para que possamos te enviar uma nova senha. Entre em contato com a secretaria da federação.' ));
            return false;
        }



        // Gerando nova senha
        $NovaSenha              =   date("dHi");
        $NovaSenhaEncriptada    =   $this->autenticacao->EncriptarSenha($NovaSenha);


        // Atualizando a nova senha no banco
        $Sql       =   "
                                UPDATE tb_pessoa set pes_senha = '".$NovaSenhaEncriptada."'
                                    WHERE   pes_id = ".$PessoaId." 
                        ";
        $this->db->query($Sql);


        // Atualizando a nova senha
        $this->SendEmail($EmailPrincipal[0]->ema_email, $NovaSenha);


        echo json_encode( array('Status'=> true, 'Email' => $this->obfuscate_email($EmailPrincipal[0]->ema_email) ) );

    }


    function obfuscate_email($email)
    {
        $em   = explode("@",$email);
        $name = implode(array_slice($em, 0, count($em)-1), '@');
        $len  = floor(strlen($name)/2);

        return substr($name,0, $len) . str_repeat('*', $len) . "@" . end($em);   
    }



    public function GetEmailPrincipal( $PessoaId ){


        $Sql       =   "
                                SELECT * FROM tb_email
                                    WHERE   fk_aut2_id IS NULL AND 
                                            fk_sta_id = 1 AND
                                            ema_principal = 1 AND
                                            flag_deletado IS NULL AND
                                            fk_peo_id = '".$PessoaId."'
                        ";
        return $this->db->query($Sql)->result();

    }


    public function SendEmail( $EmailPrincipal, $NovaSenha ){


        $Dataset    =   array(
                            'NovaSenha'  =>   $NovaSenha
                        );


        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'sigepebrasil@gmail.com',
            'smtp_pass' => 't730shgg',
            'mailtype'  => 'html', 
            'charset'   => 'utf-8'
        );
        $this->load->library('email', $config);

        $this->email->set_newline("\r\n");

        $this->email->from("sigepebrasil@gmail.com", "FHBr - Federação Hípica de Brasília ");
        $this->email->to($EmailPrincipal);
        $this->email->subject('Pedido de redefinição de senha para sua Conta do SIGEPE' . date("s") );
        $mesg = $this->load->view('EmailTransacional/ResetarSenha', $Dataset, true);
        $this->email->message($mesg);
        $this->email->send();

    }



}
