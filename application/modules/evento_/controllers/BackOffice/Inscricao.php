<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';
class Inscricao extends MY_FrontOffice {

    public $data;
    /*
 select
	fri.fri_id, frf.fk_evc_id, fri.fri_lista, fri.fk_srp_id, fri.fk_pes_id, fri.fk_ani_id,
	pes.pes_nome_razao_social as competidor,
	ani.ani_nome_completo,
	evc.evc_sigla,
	pef.pef_id,
	pfa.pfa_id, pfa.fk_pje_id,
    pes_2.pes_nome_razao_social
	,pej.pej_id
	,pje.pje_acronimo


from tb_evento_faturaglobal_rel_faturasimples_rel_inscricao as fri

join tb_pessoa as pes ON pes.pes_id = fri.fk_pes_id


join tb_animal as ani ON fri.fk_ani_id = ani.ani_id
join tb_evento_faturaglobal_rel_faturasimples as frf ON frf.frf_id = fri.fk_frf_id
join tb_evento_categoria as evc ON evc.evc_id = frf.fk_evc_id

join tb_pessoa_fisica as pef ON pef.fk_pes_id = pes.pes_id
join tb_pessoa_fisica_atleta as pfa ON pfa.fk_pef_id = pef.pef_id

#join tb_pessoa_juridica_entidade as pje ON pje.fk_pej_id = --


#join tb_pessoa_juridica_entidade as pfe ON pfe.fk_pej_id = pej.pej_id




join tb_pessoa as pes_2 ON pes_2.pes_id = pfa.fk_pje_id
join tb_pessoa_juridica as pej on pej.fk_pes_id = pfa.fk_pje_id
join tb_pessoa_juridica_entidade as pje on pje.fk_pej_id = pej.pej_id



#join tb_evento_faturaglobal_rel_faturasimples as frf ON frf.
*/

    function __construct() {

        parent::__construct();

        $this->load->module('evento/BackOffice/Evento');
        $this->data     =   $this->evento->InformacoesTemplate();

        $this->ThemeComponent();
        $this->SigepeAsset();

    }


    /**
    * Index
    *
    * @author Gustavo Botega
    */
    public function index() {
		$this->Listagem();
    }



    /**
    * Listagem
    *
    * @author Gustavo Botega
    */
    public function Dashboard($EventoId){


        $this->data['Serie']       =   $this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $EventoId, 1, 'eve_nome');




        // Obter as Provas das Series do Evento
        $SqlProva                       = '
                                                SELECT * FROM
                                                    tb_evento_rel_serie_rel_prova as srp

                                                INNER JOIN tb_evento_rel_serie as ers
                                                ON ers.ers_id = srp.fk_ers_id

                                                INNER JOIN tb_evento as eve
                                                ON eve.eve_id = ers.fk_eve_id

                                                WHERE fk_eve_id = '.$EventoId.'

                                                ORDER BY srp.srp_numero_prova ASC;
                                            ';
        $DatasetProva                   =   $this->db->query($SqlProva)->result();
        $this->data['DatasetProva']     =   $DatasetProva;






        $SqlSerie                       = '
                                                SELECT * FROM
                                                    tb_evento_rel_serie as ers

                                                INNER JOIN tb_evento as eve
                                                ON eve.eve_id = ers.fk_eve_id

                                                INNER JOIN tb_evento_serie as evs
                                                ON evs.evs_id = ers.fk_evs_id

                                                WHERE fk_eve_id = '.$EventoId.'

                                                ORDER BY
                                                    evs_altura asc
                                            ';
        $this->data['DatasetSerie']     = $this->db->query($SqlSerie)->result();

//                                                                  frf.frf_id, frf.fk_evf_id, frf.fk_pes_id, frf.fk_evc_id, frf.fk_ani_id, frf.fk_sta_id, frf.fk_ers_id, frf.fk_evc_id, frf.frf_controle from tb_evento_faturaglobal_rel_faturasimples as frf
        $SqlInscricoesPorSerie                       = '
                                                          select  pes.pes_id, pes.pes_nome_razao_social,
                                                                  ani.ani_id, ani.ani_nome_completo, ani.fk_pje_id as AnimalEntidadeId,
                                                                  evf.fk_eve_id,
                                                                  pef.pef_id, pef.fk_pes_id,
                                                                  pfa.pfa_id, pfa.fk_pef_id, pfa.fk_pjf_id, pfa.fk_pje_id,
                                                                  evc.evc_id, evc.evc_sigla, evc.evc_categoria,
                                                                  frf.frf_id, frf.fk_evf_id, frf.fk_pes_id, frf.fk_evc_id, frf.fk_ani_id, frf.fk_sta_id, frf.fk_ers_id, frf.fk_evc_id, frf.frf_controle, frf.frf_quantidade_quarto, frf.frf_valor_quarto, frf.frf_quantidade_baia, frf.frf_valor_baia, frf.frf_valor, frf.frf_valor_promocional_serie  from tb_evento_faturaglobal_rel_faturasimples as frf


                                                          join tb_evento_faturaglobal as evf on evf.evf_id = frf.fk_evf_id
                                                          join tb_pessoa as pes on pes.pes_id = frf.fk_pes_id
                                                          join tb_pessoa_fisica as pef on pef.fk_pes_id = pes.pes_id
                                                          join tb_pessoa_fisica_atleta as pfa on pef.pef_id = pfa.fk_pef_id
                                                          join tb_animal as ani on ani.ani_id = frf.fk_ani_id
                                                          join tb_evento_categoria as evc on evc.evc_id = frf.fk_evc_id


                                                          where evf.fk_eve_id = '.$EventoId.'
                                                      ';
        $this->data['DatasetInscricoesPorSerie']     = $this->db->query($SqlInscricoesPorSerie)->result();


        /* Carrega View */
        $this->LoadTemplateEvento('Template/BackOffice/sistema/Evento/Inscricao', $this->data);

    }




    /**
    * GetCategoriasDaSerie
    *
    * @author Gustavo Botega
    */
    public function GetCategoriasDaSerie($SerieId){

        $SqlCategoriaSerie            = '
                                                SELECT * FROM
                                                    tb_evento_rel_serie_rel_categoria as src

                                                INNER JOIN tb_evento_rel_serie as ers
                                                ON ers.ers_id = src.fk_ers_id

                                                INNER JOIN tb_evento_categoria as evc
                                                ON evc.evc_id = src.fk_evc_id

                                                WHERE ers.ers_id = '.$SerieId.'


                                                ORDER BY
                                                    evc.evc_id asc
                                            ';
        return $this->db->query($SqlCategoriaSerie)->result();

    }




    /**
    * GetProvasDaSerie
    *
    * @author Gustavo Botega
    */
    public function GetProvasDaSerie($SerieId){


        $SqlProvasDaSerie            = '
                                                SELECT * FROM
                                                    tb_evento_rel_serie_rel_prova as srp

                                                WHERE srp.fk_ers_id = '.$SerieId.'

                                                ORDER BY
                                                    srp.srp_numero_prova ASC
                                            ';
        return $this->db->query($SqlProvasDaSerie)->result();
    }











    /**
    * PackagesClass
    *
    * @author Gustavo Botega
    */
    public function ThemeComponent(){



        /* STYLES
        -----------------------------------*/

            // PLUGINS  //

                /* {NamePackage} */
                $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;
                $this->data['StylesFile']['Plugins']['datatables']                        = TRUE;
                $this->data['StylesFile']['Plugins']['datatables-bootstrap']                        = TRUE;



            // STYLES  //

                /* {NamePackage} */
                $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;
                $this->data['StylesFile']['Styles']['profile']                                      = TRUE;







        /* SCRIPTS
        -----------------------------------*/

            // PLUGINS  //

                /* {NamePackage} */
                $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
                $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
                $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;
                $this->data['ScriptsFile']['Plugins']['datatable']                               = TRUE;
                $this->data['ScriptsFile']['Plugins']['datatables']                               = TRUE;
                $this->data['ScriptsFile']['Plugins']['datatables-bootstrap']                     = TRUE;


            // SCRIPTS //

                /* Bootstrap Sweet Alert */
                $this->data['ScriptsFile']['Scripts']['table-datatables-fixedheader']                  = TRUE;


    }



    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega
    * @return array
    */
    public function SigepeAsset()
    {
        /* Carregando Estilos */
#        $this->data['PackageStyles'][]    =   'packages/frontoffice/style/cadastro-animal';


        /* Carregando Scripts */
#        $this->data['PackageScripts'][]   =   'packages/frontoffice/script/animal';
#        $this->data['PackageScripts'][]   =   'packages/frontoffice/script/cadastro-animal';
#        $this->data['PackageScripts'][]   =   'packages/frontoffice/script/form-validation-cadastro-evento';


    }





}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
