<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_BackOffice.php';
class CadastroEvento extends MY_BackOffice {

    public $data;

    function __construct() {

        parent::__construct();


        $this->ThemeComponent(); 
        $this->SigepeAsset(); 
    }


    /**
    * Index
    *
    * @author Gustavo Botega 
    */
    public function index() {
		$this->Formulario();    	
    }




    /**
    * Formulario
    *
    * @author Gustavo Botega 
    */
    public function Formulario(){

        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Cadastro';
        $this->data['PageHeadSubtitle']   = 'Para cadastrar um animal no SIGEPE preencha o formulário abaixo.';

        $this->data['ShowSidebar']        = TRUE;
        $this->data['Breadcrumbs']        = array();


        $SqlModalidade                     = '
                                                SELECT * FROM
                                                    tb_evento_modalidade as evm
                                                ORDER BY
                                                    evm_id asc
                                            ';
        $this->data['DatasetModalidade']   = $this->db->query($SqlModalidade)->result();


        $SqlTipoEvento                     = '
                                                SELECT * FROM
                                                    tb_evento_tipo as evt
                                                ORDER BY
                                                    evt_id asc
                                            ';
        $this->data['DatasetTipoEvento']   = $this->db->query($SqlTipoEvento)->result();


        $SqlTipoInscricao                     = '
                                                SELECT * FROM
                                                    tb_evento_venda as evv
                                                ORDER BY
                                                    evv_id asc
                                            ';
        $this->data['DatasetTipoInscricao']   = $this->db->query($SqlTipoInscricao)->result();



        /* Local */
        $SqlLocal                       = '
                                                SELECT * FROM tb_pessoa as pes

                                                INNER JOIN tb_vinculo as vin
                                                ON pes.pes_id = vin.fk_pes1_id

                                                INNER JOIN tb_pessoa_juridica as pej
                                                ON pej.fk_pes_id = pes.pes_id


                                                INNER JOIN tb_pessoa_juridica_entidade as pje
                                                ON pje.fk_pej_id = pej.pej_id


                                                WHERE vin.fk_per_id = 4 AND 
                                                vin.fk_sta_id = 1 AND
                                                vin.fk_tip_id IS NULL AND
                                                pes.fk_sta_id = 1 AND
                                                pje.flag_entidade_filiada = 1

                                                ORDER BY pes.pes_nome_razao_social ASC

                                            ';
        $this->data['DatasetLocal']     = $this->db->query($SqlLocal)->result();


        /* Desenhador */
        $SqlDesenhador             = '
                                                SELECT * FROM tb_pessoa as pes

                                                INNER JOIN tb_vinculo as vin
                                                ON pes.pes_id = vin.fk_pes1_id


                                                WHERE vin.fk_per_id = 6 AND 
                                                vin.fk_sta_id = 1 AND
                                                vin.fk_tip_id IS NULL AND
                                                pes.fk_sta_id = 1 

                                                ORDER BY pes.pes_nome_razao_social ASC
                                            ';
        $this->data['DatasetDesenhador']   = $this->db->query($SqlDesenhador)->result();


        /* Carrega View */
        //$this->loadTemplateUser('', $this->data);
        $this->LoadTemplate('Template/BackOffice/sistema/Evento/FormularioEvento', $this->data);


    }


    /**
    * Processar
    *
    * @author Gustavo Botega 
    */
    public function Processar($Ajax = false, $AjaxDados = false){


        $Dados  =   $_POST;
        $this->load->module('evento');


        // Setando Variaveis
        (!isset($Dados['descricao'])) ? $Dados['descricao'] = NULL : '';


        // Validacao Formulario
        $Validar                                    =   $this->Validar($Dados);
        if(!$Validar)
            return false;

        $Gravar                                     =   $this->Gravar($Dados);
        if($Gravar):        

            // ID do evento Gerado
            $EventoId                               =   $Gravar;

            // Gravar Tipo do Evento
            $GravarTipoEvento                       =   $this->evento->GravarTipoEvento($Dados['tipo-evento'], $EventoId);

            // Gravar Desenhador de Percurso
            $GravarDesenhadorPercurso               =   $this->evento->GravarDesenhadorPercurso($Dados['desenhador-percurso'], $EventoId);


        endif;


        if(!$Gravar)
            return false;


        // Cadastro atleta finalizado com sucesso
        redirect(base_url() . 'evento/BackOffice/Evento/Dashboard/' . $EventoId);
        

    }


    /**
    * Validar
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function Validar($Dados){

        // Validar inputs e regras de negocio do formulario. Tratar erro. 

        return true;

    }




    /**
    * Gravar
    *
    * @author Gustavo Botega 
    */
    public function Gravar($Dados){

/*        // Genero Tipo
        (empty($Dados['genero-tipo'])) ? $Dados['genero-tipo'] = NULL : '';

        // Associacao
        (empty($Dados['associacao'])) ? $Dados['associacao'] = NULL : '';

        // Pais de Origem
        (empty($Dados['pais-origem'])) ? $Dados['pais-origem'] = NULL : '';

        // Pais de Origem
        (empty($Dados['pais-origem'])) ? $Dados['pais-origem'] = NULL : '';
*/

        foreach ($Dados as $key => $value) {
            if(empty($value))
                $Dados[$key]    =   NULL;
        }


        // Flag Baia
        if($Dados['baia']=='2')
            $Dados['baia'] = NULL;


        // Flag Quarto de Sela
        if($Dados['quarto-de-sela']=='2')
            $Dados['quarto-de-sela'] = NULL;



        // Genero Tipo
        (!isset($Dados['genero-tipo'])) ? $Dados['genero-tipo'] = NULL : '';


        // Dados
        $Dataset       =  array(
            
            'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
            'fk_sta_id'                         =>    400, // [ Evento ] Inscrições em breve
            'fk_evm_id'                         =>    $Dados['modalidade'], // Modalidade
            'fk_pes_id'                         =>    $Dados['local'], // Entidade Filiada na qual vai sediar o evento
            'fk_evv_id'                         =>    $Dados['tipo-venda'], // Venda de Inscricao por Tipo ( Serie / Prova / Serie + Prova )
            'eve_nome'                          =>    $Dados['nome-evento'], // Nome do Evento
            'eve_data_inicio'                   =>    $this->my_data->ConverterData($Dados['data-inicio'], 'PT-BR', 'ISO'), //
            'eve_data_fim'                      =>    $this->my_data->ConverterData($Dados['data-fim'], 'PT-BR', 'ISO'), //
            'eve_data_limite_sem_acrescimo'     =>    $this->my_data->ConverterData($Dados['data-limite-sem-acrescimo'], 'PT-BR', 'ISO'), //
            'eve_quantidade_prova'              =>    $Dados['quantidade-prova'], // Quantidade provas permitidas
            'flag_baia'                         =>    $Dados['baia'], //
            'flag_quarto'                       =>    $Dados['quarto-de-sela'], //
            'flag_site'                         =>    $Dados['ativar-site'], //
            'eve_site_inscricao_inicio'         =>    $this->my_data->ConverterData($Dados['abertura-inscricoes'], 'PT-BR', 'ISO'), //
            'eve_site_inscricao_fim'            =>    $this->my_data->ConverterData($Dados['encerramento-inscricoes'], 'PT-BR', 'ISO'), //
            'eve_descricao'                     =>    $Dados['descricao'], //

            'criado'                            =>    date("Y-m-d H:i:s")

        );


        /* Query */
        $Query = $this->db->insert('tb_evento', $Dataset);

        return ($Query) ? $this->db->insert_id() : false;

    }




    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    */
    public function ThemeComponent(){ 



        /* STYLES
        -----------------------------------*/

            // PLUGINS  //

                /* {NamePackage} */
                $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;
                $this->data['StylesFile']['Plugins']['easy-autocomplete']                           = TRUE;
                $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                    = TRUE;
        
                /* Date & Time Pickers */
                $this->data['StylesFile']['Plugins']['daterangepicker']                             = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-datepicker3']                       = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-timepicker']                        = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-datetimepicker']                    = TRUE;
                $this->data['StylesFile']['Plugins']['clockface']                                   = TRUE;

                /* Bootstrap File Input */
                $this->data['StylesFile']['Plugins']['bootstrap-fileinput']                         = TRUE;

                /* Markdown & WYSIWYG Editors */
                $this->data['StylesFile']['Plugins']['bootstrap-wysihtml5']                         = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-markdown']                          = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-summernote']                        = TRUE;


            // STYLES  //

                /* {NamePackage} */
                $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;







        /* SCRIPTS
        -----------------------------------*/

            // PLUGINS  //

                /* {NamePackage} */
                $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
                $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
                $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
                $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
                $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
                $this->data['ScriptsFile']['Plugins']['easy-autocomplete']                        = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;
                
                /* Date & Time Pickers */
                $this->data['ScriptsFile']['Plugins']['moment']                                   = TRUE;
                $this->data['ScriptsFile']['Plugins']['daterangepicker']                          = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-datepicker']                     = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-timepicker']                     = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-datetimepicker']                 = TRUE;
                $this->data['ScriptsFile']['Plugins']['clockface']                                = TRUE;

                /* Bootstrap File Input */
                $this->data['ScriptsFile']['Plugins']['bootstrap-fileinput']                      = TRUE;

                /* Markdown & WYSIWYG Editors */
                $this->data['ScriptsFile']['Plugins']['wysihtml5']                                = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-wysihtml5']                      = TRUE;
                $this->data['ScriptsFile']['Plugins']['markdown']                                 = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-markdown']                       = TRUE;
                $this->data['ScriptsFile']['Plugins']['summernote']                               = TRUE;

                /* Markdown & WYSIWYG Editors */
                $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
                $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;



            // SCRIPTS // 

                /* Bootstrap Sweet Alert */
                $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                  = TRUE;
                
                /* Date & Time Pickers */
                $this->data['ScriptsFile']['Scripts']['components-date-time-pickers']   = TRUE;

                /* Markdown & WYSIWYG Editors */
                $this->data['ScriptsFile']['Scripts']['components-editors']             = TRUE;

    }



    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function SigepeAsset()
    { 
        /* Carregando Estilos */
        $this->data['PackageStyles'][]    =   'Packages/frontoffice/style/cadastro-animal';


        /* Carregando Scripts */
        $this->data['PackageScripts'][]   =   'Packages/Evento/script/animal';
        $this->data['PackageScripts'][]   =   'Packages/Evento/script/cadastro-animal';
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Evento/BackOffice/FormValidationCadastroEvento';
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Evento/BackOffice/HandleCadastroEvento';


    }



     /*
             // @@@ Implementar permissões por usuario
        
        // AUDITORIA:
            [0]=> Slug Module
            [1]=> Table Database
            [2]=> Column Database
            [3]=> Value Deleted
            [4]=> Folder Thumb
            [5]=> Delete Gallery
            [6]=> Table Gallery
            [7]=> Column Table Gallery
            [8]=> Data/hora
            [9]=> IP
            [10]=> ID Usuário  
            [11]=> Type of Operation  
            */

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

