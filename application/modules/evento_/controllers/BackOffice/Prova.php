<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_BackOffice.php';
class Prova extends MY_BackOffice {

    public $data;

    function __construct() {

        parent::__construct();

        $this->load->module('evento/BackOffice/Evento');
        $this->data     =   $this->evento->InformacoesTemplate();
        $this->data['Class']    =   $this->router->fetch_class();
        $this->data['ShowProfileEvento']   =  TRUE;

        $this->ThemeComponent();
        $this->SigepeAsset();

    }


    /**
    * Index
    *
    * @author Gustavo Botega
    */
    public function index() {
		$this->Listagem();
    }



    /**
    * Listagem
    *
    * @author Gustavo Botega
    */
    public function Listagem($EventoId){



        // Obter as Provas das Series do Evento
        $SqlProva                       = '
                                                SELECT * FROM
                                                    tb_evento_rel_serie_rel_prova as srp

                                                INNER JOIN tb_evento_rel_serie as ers
                                                ON ers.ers_id = srp.fk_ers_id

                                                INNER JOIN tb_evento as eve
                                                ON eve.eve_id = ers.fk_eve_id

                                                WHERE fk_eve_id = '.$EventoId.'

                                                ORDER BY srp.srp_numero_prova ASC;
                                            ';
        $DatasetProva                   =   $this->db->query($SqlProva)->result();
        $this->data['DatasetProva']     =   $DatasetProva;



        // Obter os Desenhadores de Percurso do Evento
        $SqlDesenhadorPercurso             = '
                                                SELECT * FROM tb_evento_rel_desenhador as erd

                                                INNER JOIN tb_pessoa as pes
                                                ON pes.pes_id = erd.fk_pes_id


                                                INNER JOIN tb_vinculo as vin
                                                ON pes.pes_id = vin.fk_pes1_id

                                                WHERE
                                                    vin.fk_per_id = 6 AND
                                                    vin.fk_sta_id = 1 AND
                                                    vin.fk_tip_id IS NULL AND
                                                    pes.fk_sta_id = 1 AND
                                                    erd.fk_eve_id = '.$EventoId.'

                                                ORDER BY pes.pes_nome_razao_social ASC
                                            ';
        $this->data['DatasetDesenhadorPercurso']   = $this->db->query($SqlDesenhadorPercurso)->result();



        $this->data['EventoId']         =   $EventoId;



        /*


        $SqlProva                       = '
                                                SELECT * FROM
                                                    tb_evento_rel_serie as ers

                                                INNER JOIN tb_evento as eve
                                                ON eve.eve_id = ers.fk_eve_id

                                                INNER JOIN tb_evento_serie as evs
                                                ON evs.evs_id = ers.fk_evs_id

                                                WHERE fk_eve_id = '.$EventoId.'

                                                ORDER BY
                                                    evs_altura asc
                                            ';
        $this->data['DatasetProva']     = $this->db->query($SqlProva)->result();
        $this->data['EventoId']         = $EventoId;

*/

        $this->LoadTemplate('Template/BackOffice/sistema/Evento/Listagem', $this->data);


    }




    /**
    * GetCategoriasDaProva
    *
    * @author Gustavo Botega
    */
    public function GetCategoriasDaProva($ProvaId){


        $SqlCategoriaProva            = '
                                                SELECT * FROM
                                                    tb_evento_rel_serie_rel_prova_rel_categoria as prc

                                                INNER JOIN tb_evento_rel_serie_rel_prova as srp
                                                ON srp.srp_id = prc.fk_srp_id

                                                INNER JOIN tb_evento_categoria as evc
                                                ON evc.evc_id = prc.fk_evc_id

                                                WHERE srp.srp_id = '.$ProvaId.'

                                                ORDER BY
                                                    evc.evc_id asc
                                            ';
        return $this->db->query($SqlCategoriaProva)->result();

    }



    /**
    * GetDesenhadorDePercursoDaProva
    *
    * @author Gustavo Botega
    */
    public function GetDesenhadorDePercursoDaProva($ProvaId){


        $SqlDesenhadorDePercursoDaProva     = '
                                                SELECT * FROM
                                                    tb_evento_rel_serie_rel_prova_rel_desenhador as prd


                                                INNER JOIN tb_evento_rel_serie_rel_prova as srp
                                                ON srp.srp_id = prd.fk_srp_id


                                                INNER JOIN tb_pessoa as pes
                                                ON pes.pes_id = prd.fk_pes_id


                                                INNER JOIN tb_vinculo as vin
                                                ON pes.pes_id = vin.fk_pes1_id


                                                WHERE
                                                    vin.fk_per_id = 6 AND
                                                    vin.fk_sta_id = 1 AND
                                                    vin.fk_tip_id IS NULL AND
                                                    pes.fk_sta_id = 1 AND
                                                    prd.fk_srp_id = '.$ProvaId.'

                                                ORDER BY
                                                    prd.flag_assistente asc
                                            ';
        return $this->db->query($SqlDesenhadorDePercursoDaProva)->result();

    }



    /**
    * GetDesenhadorDePercursoExcetoProva
    *
    * Lista todos os desenhadores do evento com a excecao dos que ja estao cadastrados na prova
    *
    *
    * @author Gustavo Botega
    */
    public function GetDesenhadorDePercursoExcetoProva($ProvaId, $EventoId){


        $SqlDesenhadorDePercursoDaProva     = '
                                                SELECT * FROM
                                                    tb_evento_rel_serie_rel_prova_rel_desenhador as prd


                                                INNER JOIN tb_evento_rel_serie_rel_prova as srp
                                                ON srp.srp_id = prd.fk_srp_id


                                                INNER JOIN tb_pessoa as pes
                                                ON pes.pes_id = prd.fk_pes_id


                                                INNER JOIN tb_vinculo as vin
                                                ON pes.pes_id = vin.fk_pes1_id


                                                WHERE
                                                    vin.fk_per_id = 6 AND
                                                    vin.fk_sta_id = 1 AND
                                                    vin.fk_tip_id IS NULL AND
                                                    pes.fk_sta_id = 1 AND
                                                    prd.fk_srp_id = '.$ProvaId.'

                                                ORDER BY
                                                    prd.flag_assistente asc
                                            ';
        $DatasetDesenhadoresDePercursoDaProva       =    $this->db->query($SqlDesenhadorDePercursoDaProva)->result();
        $ArrayDesenhadoresDePercursoDaProva         =   array();

        foreach ($DatasetDesenhadoresDePercursoDaProva as $key => $value) {
            $ArrayDesenhadoresDePercursoDaProva[]      =    $value->pes_id;
        }



        $SqlDesenhadorPercursoDoEvento      = '
                                                SELECT * FROM tb_evento_rel_desenhador as erd

                                                INNER JOIN tb_pessoa as pes
                                                ON pes.pes_id = erd.fk_pes_id


                                                INNER JOIN tb_vinculo as vin
                                                ON pes.pes_id = vin.fk_pes1_id

                                                WHERE
                                                    vin.fk_per_id = 6 AND
                                                    vin.fk_sta_id = 1 AND
                                                    vin.fk_tip_id IS NULL AND
                                                    pes.fk_sta_id = 1 AND
                                                    erd.fk_eve_id = '.$EventoId.'

                                                ORDER BY pes.pes_nome_razao_social ASC
                                            ';
        $DatasetDesenhadorPercursoDoEvento   = $this->db->query($SqlDesenhadorPercursoDoEvento)->result();
        $ArrayDesenhadoresDePercursoDoEvento = array();

        foreach ($DatasetDesenhadorPercursoDoEvento as $key => $value) {
            $ArrayDesenhadoresDePercursoDoEvento[]  =   $value->pes_id;
        }


        return array_diff($ArrayDesenhadoresDePercursoDoEvento, $ArrayDesenhadoresDePercursoDaProva);

    }













    /**
    * PackagesClass
    *
    * @author Gustavo Botega
    */
    public function ThemeComponent(){



        /* STYLES
        -----------------------------------*/

            // PLUGINS  //

                /* {NamePackage} */
                $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;
                $this->data['StylesFile']['Plugins']['easy-autocomplete']                           = TRUE;
                $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                    = TRUE;

                /* Date & Time Pickers */
                $this->data['StylesFile']['Plugins']['daterangepicker']                             = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-datepicker3']                       = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-timepicker']                        = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-datetimepicker']                    = TRUE;
                $this->data['StylesFile']['Plugins']['clockface']                                   = TRUE;

                /* Bootstrap File Input */
                $this->data['StylesFile']['Plugins']['bootstrap-fileinput']                         = TRUE;

                /* Markdown & WYSIWYG Editors */
                $this->data['StylesFile']['Plugins']['bootstrap-wysihtml5']                         = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-markdown']                          = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-summernote']                        = TRUE;


            // STYLES  //

                /* {NamePackage} */
                $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;
                $this->data['StylesFile']['Styles']['profile']                                      = TRUE;







        /* SCRIPTS
        -----------------------------------*/

            // PLUGINS  //

                /* {NamePackage} */
                $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
                $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
                $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
                $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
                $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
                $this->data['ScriptsFile']['Plugins']['easy-autocomplete']                        = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

                /* Date & Time Pickers */
                $this->data['ScriptsFile']['Plugins']['moment']                                   = TRUE;
                $this->data['ScriptsFile']['Plugins']['daterangepicker']                          = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-datepicker']                     = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-timepicker']                     = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-datetimepicker']                 = TRUE;
                $this->data['ScriptsFile']['Plugins']['clockface']                                = TRUE;

                /* Bootstrap File Input */
                $this->data['ScriptsFile']['Plugins']['bootstrap-fileinput']                      = TRUE;

                /* Markdown & WYSIWYG Editors */
                $this->data['ScriptsFile']['Plugins']['wysihtml5']                                = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-wysihtml5']                      = TRUE;
                $this->data['ScriptsFile']['Plugins']['markdown']                                 = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-markdown']                       = TRUE;
                $this->data['ScriptsFile']['Plugins']['summernote']                               = TRUE;

                /* Markdown & WYSIWYG Editors */
                $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
                $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;



            // SCRIPTS //

                /* Bootstrap Sweet Alert */
                $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                  = TRUE;

                /* Date & Time Pickers */
                $this->data['ScriptsFile']['Scripts']['components-date-time-pickers']   = TRUE;

                /* Markdown & WYSIWYG Editors */
                $this->data['ScriptsFile']['Scripts']['components-editors']             = TRUE;

                /* User Profile */
/*                $this->data['ScriptsFile']['Scripts']['profile']                        = TRUE;
                $this->data['ScriptsFile']['Scripts']['timeline']                       = TRUE;*/

    }



    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega
    * @return array
    */
    public function SigepeAsset()
    {
        /* Carregando Estilos */
        /* Carregando Scripts */
    }





}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
