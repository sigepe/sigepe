<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_BackOffice.php';
class Serie extends MY_BackOffice {

    public $data;

    function __construct() {

        parent::__construct();

        $this->load->module('evento/BackOffice/Evento');
        $this->data     =   $this->evento->InformacoesTemplate();
        $this->data['ShowProfileEvento']   =  TRUE;

        $this->ThemeComponent();
        $this->SigepeAsset();

    }


    /**
    * Index
    *
    * @author Gustavo Botega
    */
    public function index() {
		$this->Listagem();
    }



    /**
    * Listagem
    *
    * @author Gustavo Botega
    */
    public function Listagem($EventoId){


        $this->data['Serie']       =   $this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $EventoId, 1, 'eve_nome');


        $SqlSerie                       = '
                                                SELECT * FROM
                                                    tb_evento_rel_serie as ers

                                                INNER JOIN tb_evento as eve
                                                ON eve.eve_id = ers.fk_eve_id

                                                INNER JOIN tb_evento_serie as evs
                                                ON evs.evs_id = ers.fk_evs_id

                                                WHERE fk_eve_id = '.$EventoId.'

                                                ORDER BY
                                                    evs_altura asc
                                            ';
        $this->data['DatasetSerie']     = $this->db->query($SqlSerie)->result();
        $this->data['EventoId']         = $EventoId;



        /* Carrega View */
        $this->LoadTemplate('Template/BackOffice/sistema/Evento/Serie', $this->data);
        /* Unable to load the requested file: Template/BackOffice/Sistema/Evento/Serie.php */

    }




    /**
    * GetCategoriasDaSerie
    *
    * @author Gustavo Botega
    */
    public function GetCategoriasDaSerie($SerieId){

        $SqlCategoriaSerie            = '
                                                SELECT * FROM
                                                    tb_evento_rel_serie_rel_categoria as src

                                                INNER JOIN tb_evento_rel_serie as ers
                                                ON ers.ers_id = src.fk_ers_id

                                                INNER JOIN tb_evento_categoria as evc
                                                ON evc.evc_id = src.fk_evc_id

                                                WHERE ers.ers_id = '.$SerieId.'


                                                ORDER BY
                                                    evc.evc_id asc
                                            ';
        return $this->db->query($SqlCategoriaSerie)->result();

    }




    /**
    * GetProvasDaSerie
    *
    * @author Gustavo Botega
    */
    public function GetProvasDaSerie($SerieId){


        $SqlProvasDaSerie            = '
                                                SELECT * FROM
                                                    tb_evento_rel_serie_rel_prova as srp

                                                WHERE srp.fk_ers_id = '.$SerieId.'

                                                ORDER BY
                                                    srp.srp_numero_prova ASC
                                            ';
        return $this->db->query($SqlProvasDaSerie)->result();
    }











    /**
    * PackagesClass
    *
    * @author Gustavo Botega
    */
    public function ThemeComponent(){



        /* STYLES
        -----------------------------------*/

            // PLUGINS  //

                /* {NamePackage} */
                $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;
                $this->data['StylesFile']['Plugins']['easy-autocomplete']                           = TRUE;
                $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                    = TRUE;

                /* Date & Time Pickers */
                $this->data['StylesFile']['Plugins']['daterangepicker']                             = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-datepicker3']                       = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-timepicker']                        = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-datetimepicker']                    = TRUE;
                $this->data['StylesFile']['Plugins']['clockface']                                   = TRUE;

                /* Bootstrap File Input */
                $this->data['StylesFile']['Plugins']['bootstrap-fileinput']                         = TRUE;

                /* Markdown & WYSIWYG Editors */
                $this->data['StylesFile']['Plugins']['bootstrap-wysihtml5']                         = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-markdown']                          = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-summernote']                        = TRUE;


            // STYLES  //

                /* {NamePackage} */
                $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;
                $this->data['StylesFile']['Styles']['profile']                                      = TRUE;







        /* SCRIPTS
        -----------------------------------*/

            // PLUGINS  //

                /* {NamePackage} */
                $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
                $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
                $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
                $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
                $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
                $this->data['ScriptsFile']['Plugins']['easy-autocomplete']                        = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

                /* Date & Time Pickers */
                $this->data['ScriptsFile']['Plugins']['moment']                                   = TRUE;
                $this->data['ScriptsFile']['Plugins']['daterangepicker']                          = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-datepicker']                     = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-timepicker']                     = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-datetimepicker']                 = TRUE;
                $this->data['ScriptsFile']['Plugins']['clockface']                                = TRUE;

                /* Bootstrap File Input */
                $this->data['ScriptsFile']['Plugins']['bootstrap-fileinput']                      = TRUE;

                /* Markdown & WYSIWYG Editors */
                $this->data['ScriptsFile']['Plugins']['wysihtml5']                                = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-wysihtml5']                      = TRUE;
                $this->data['ScriptsFile']['Plugins']['markdown']                                 = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-markdown']                       = TRUE;
                $this->data['ScriptsFile']['Plugins']['summernote']                               = TRUE;

                /* Markdown & WYSIWYG Editors */
                $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
                $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;



            // SCRIPTS //

                /* Bootstrap Sweet Alert */
                $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                  = TRUE;

                /* Date & Time Pickers */
                $this->data['ScriptsFile']['Scripts']['components-date-time-pickers']   = TRUE;

                /* Markdown & WYSIWYG Editors */
                $this->data['ScriptsFile']['Scripts']['components-editors']             = TRUE;

                /* User Profile */
                $this->data['ScriptsFile']['Scripts']['profile']                        = TRUE;
                $this->data['ScriptsFile']['Scripts']['timeline']                       = TRUE;

    }



    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega
    * @return array
    */
    public function SigepeAsset() {
        /* Carregando Estilos */
    
        /* Carregando Scripts */
    }





}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
