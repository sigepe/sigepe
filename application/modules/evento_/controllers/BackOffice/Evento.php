<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_BackOffice.php';
class Evento extends MY_BackOffice {

    public $data;

    function __construct() {

        parent::__construct();


        $this->InformacoesTemplate();
        $this->data['Class']    =   $this->router->fetch_class();

    }


    /**
    * Index
    *
    * @author Gustavo Botega
    */
    public function index() {
		$this->Formulario();
    }



    /**
    * Dashboard
    *
    * @author Gustavo Botega
    */
    public function ObterTipoEventoPorModalidade($ModalidadeId, $Ajax = true){


        $Sql                             = '
                                                SELECT * FROM tb_evento_tipo
                                                WHERE fk_evm_id = '.$ModalidadeId.'
                                            ';
        $Query                           = $this->db->query($Sql)->result();

        if($Ajax){
            echo json_encode($Query);
        }else{
            return $Ajax;
        }

    }





    /**
    * Dashboard
    *
    * @author Gustavo Botega
    */
    public function Dashboard($EventoId){
        $this->ThemeComponent();
        $this->SigepeAsset();


        $ModalidadeId                   =   $this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $EventoId, 1, 'fk_evm_id');

        $this->data['EventoId']   =   $EventoId;
        $this->data['Method']   =   $this->router->fetch_method();
        $this->data['ShowProfileEvento']   =  TRUE;




        $SqlEvento                       = '
                                                SELECT * FROM
                                                    tb_evento as eve

                                                WHERE eve_id = '.$EventoId.'
                                            ';
        $this->data['DatasetEvento']     = $this->db->query($SqlEvento)->result();


        $SqlTipoEvento                       = '
                                                SELECT * FROM
                                                    tb_evento_rel_tipo as ert

                                                WHERE fk_eve_id = '.$EventoId.'
                                            ';
        $this->data['DatasetTipoEvento']     = $this->db->query($SqlTipoEvento)->result();
        $this->data['QuantidadeDiasEvento']  = $this->GetQuantidadeDiasEvento($EventoId);




        /* Carrega View */
        $this->LoadTemplate('Template/BackOffice/sistema/Evento/Dashboard', $this->data);
#        $this->LoadTemplate('Template/BackOffice/sistema/Evento/Dashboard', $this->data);

    }



    /**
    * InformacoesTemplate
    *
    * Retorna pra view as informacoes que compoe o template do Evento
    *
    *
    * @author Gustavo Botega
    */
    public function InformacoesTemplate(){

        $EventoId                       =   $this->uri->segment(5);
        $ModalidadeId                   =   $this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $EventoId, 1, 'fk_evm_id');

        $this->data['NomeEvento']       =   $this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $EventoId, 1, 'eve_nome');
        $this->data['Modalidade']       =   $this->model_crud->get_rowSpecific('tb_evento_modalidade', 'evm_id', $ModalidadeId, 1, 'evm_modalidade');

        $this->data['Logotipo']         =   $this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $EventoId, 1, 'eve_site_logotipo');
        $this->data['FolderId']         =   $this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $EventoId, 1, 'eve_controle');

        return $this->data;

    }




    /**
    * GravarTipoEvento
    *
    * @author Gustavo Botega
    */
    public function GravarTipoEvento($TipoEvento, $EventoId){


        foreach ($TipoEvento as $key => $value) {

            // Dados
            $Dataset       =  array(
                'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
                'fk_evt_id'                         =>    $value,
                'fk_eve_id'                         =>    $EventoId,
                'criado'                            =>    date("Y-m-d H:i:s")
            );

            /* Query */
            $Query = $this->db->insert('tb_evento_rel_tipo', $Dataset);

        }

    }




    /**
    * GetQuantidadeDiasEvento
    *
    *
    * @author Gustavo Botega
    * @return {type}
    */
    public function GetQuantidadeDiasEvento($EventoId){


        $DataInicio     =   $this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $EventoId, 1, 'eve_data_inicio');
        $DataFim        =   $this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $EventoId, 1, 'eve_data_fim');

        $QuantidadeDias =   DateDifferences($DataFim, $DataInicio, 'd') + 1;
        return $QuantidadeDias;

    }



    /**
    * GravarDesenhadorPercurso
    *
    * @author Gustavo Botega
    */
    public function GravarDesenhadorPercurso($DesenhadorPercurso, $EventoId){


        foreach ($DesenhadorPercurso as $key => $value) {

            // Dados
            $Dataset       =  array(
                'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
                'fk_pes_id'                         =>    $value,
                'fk_eve_id'                         =>    $EventoId,
                'criado'                            =>    date("Y-m-d H:i:s")
            );

            /* Query */
            $Query = $this->db->insert('tb_evento_rel_desenhador', $Dataset);

        }

    }



    /**
    * PackagesClass
    *
    * @author Gustavo Botega
    */
    public function ThemeComponent(){



        /* STYLES
        -----------------------------------*/

            // PLUGINS  //

                /* {NamePackage} */
                $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;
                $this->data['StylesFile']['Plugins']['easy-autocomplete']                           = TRUE;
                $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                    = TRUE;

                /* Date & Time Pickers */
                $this->data['StylesFile']['Plugins']['daterangepicker']                             = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-datepicker3']                       = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-timepicker']                        = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-datetimepicker']                    = TRUE;
                $this->data['StylesFile']['Plugins']['clockface']                                   = TRUE;

                /* Bootstrap File Input */
                $this->data['StylesFile']['Plugins']['bootstrap-fileinput']                         = TRUE;

                /* Markdown & WYSIWYG Editors */
                $this->data['StylesFile']['Plugins']['bootstrap-wysihtml5']                         = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-markdown']                          = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-summernote']                        = TRUE;


            // STYLES  //

                /* {NamePackage} */
                $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;
                $this->data['StylesFile']['Styles']['profile']                                      = TRUE;







        /* SCRIPTS
        -----------------------------------*/

            // PLUGINS  //

                /* {NamePackage} */
                $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
                $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
                $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
                $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
                $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
                $this->data['ScriptsFile']['Plugins']['easy-autocomplete']                        = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

                /* Date & Time Pickers */
                $this->data['ScriptsFile']['Plugins']['moment']                                   = TRUE;
                $this->data['ScriptsFile']['Plugins']['daterangepicker']                          = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-datepicker']                     = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-timepicker']                     = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-datetimepicker']                 = TRUE;
                $this->data['ScriptsFile']['Plugins']['clockface']                                = TRUE;

                /* Bootstrap File Input */
                $this->data['ScriptsFile']['Plugins']['bootstrap-fileinput']                      = TRUE;

                /* Markdown & WYSIWYG Editors */
                $this->data['ScriptsFile']['Plugins']['wysihtml5']                                = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-wysihtml5']                      = TRUE;
                $this->data['ScriptsFile']['Plugins']['markdown']                                 = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-markdown']                       = TRUE;
                $this->data['ScriptsFile']['Plugins']['summernote']                               = TRUE;

                /* Markdown & WYSIWYG Editors */
                $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
                $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;



            // SCRIPTS //

                /* Bootstrap Sweet Alert */
                $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                  = TRUE;

                /* Date & Time Pickers */
                $this->data['ScriptsFile']['Scripts']['components-date-time-pickers']   = TRUE;

                /* Markdown & WYSIWYG Editors */
                $this->data['ScriptsFile']['Scripts']['components-editors']             = TRUE;

                /* User Profile */
                $this->data['ScriptsFile']['Scripts']['profile']                        = TRUE;
                $this->data['ScriptsFile']['Scripts']['timeline']                       = TRUE;

    }



    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega
    * @return array
    */
    public function SigepeAsset()
    {
        /* Carregando Estilos */
        $this->data['PackageStyles'][]    =   'packages/frontoffice/style/cadastro-animal';


        /* Carregando Scripts */
        $this->data['PackageScripts'][]   =   'packages/frontoffice/script/animal';
        $this->data['PackageScripts'][]   =   'packages/frontoffice/script/cadastro-animal';
        $this->data['PackageScripts'][]   =   'packages/frontoffice/script/form-validation-cadastro-evento';


    }





}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
