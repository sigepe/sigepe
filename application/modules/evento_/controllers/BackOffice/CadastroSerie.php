<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_BackOffice.php';
class CadastroSerie extends MY_BackOffice {

    public $data;

    function __construct() {

        parent::__construct();




        $this->load->module('evento/BackOffice/Evento');
        $this->data     =   $this->evento->InformacoesTemplate();
        $this->data['ShowProfileEvento']   =  TRUE;

        $this->ThemeComponent();
        $this->SigepeAsset();


    }


    /**
    * Index
    *
    * @author Gustavo Botega
    */
    public function index() {
		$this->Formulario();
    }




    /**
    * Formulario
    *
    * @author Gustavo Botega
    */
    public function Formulario($EventoId){

        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Cadastro';
        $this->data['PageHeadSubtitle']   = 'Para cadastrar um animal no SIGEPE preencha o formulário abaixo.';
        $this->data['Breadcrumbs']        = array();


        // Query abaixo so exibe as series que ainda nao foram preenchidas
        /*
        $SqlSerie                         = '
                                                SELECT evs_id, evs_altura
                                                FROM tb_evento_serie as evs
                                                WHERE not exists(
                                                    SELECT ers.fk_evs_id
                                                    FROM tb_evento_rel_serie as ers
                                                    WHERE ers.fk_eve_id = '.$EventoId.'
                                                    AND evs.evs_id = ers.fk_evs_id
                                                );
                                            ';
        */


        $SqlSerie                         = '
                                                SELECT evs_id, evs_altura
                                                FROM tb_evento_serie as evs
                                            ';
        $this->data['DatasetSerie']        = $this->db->query($SqlSerie)->result();


        $SqlSerieCategoria                         = '
                                                SELECT * FROM
                                                    tb_evento_categoria as evc
                                                ORDER BY
                                                    evc_sigla asc
                                            ';
        $this->data['DatasetSerieCategoria']        = $this->db->query($SqlSerieCategoria)->result();


        $this->data['EventoId']                     = $EventoId;



        /* Carrega View */
        $this->LoadTemplate('Template/BackOffice/sistema/Evento/FormularioSerie', $this->data);

    }


    /**
    * Processar
    *
    * @author Gustavo Botega
    */
    public function Processar(){


        $Dados  =   $_POST;

        $EventoId   =   $Dados['evento-id'];

        var_dump($SerieId);
        var_dump($Dados['serie-categoria']);


        // Validacao Formulario
        $Validar                                    =   $this->Validar($Dados);
        if(!$Validar)
            return false;


        $Gravar                                     =   $this->Gravar($Dados);
        if($Gravar):

            // ID da Serie
            $SerieId                               =   $Gravar;

            // Gravar Tipo do Evento
            $GravarCategoria                        =   $this->GravarCategoriasDaSerie($Dados['serie-categoria'], $SerieId);

        endif;

        var_dump($Gravar);

        if(!$Gravar)
            return false;
        echo "t";


        // Cadastro atleta finalizado com sucesso
        echo "Sucesso. Série Cadastrado";
        redirect(base_url() . 'evento/BackOffice/Serie/Listagem/' . $EventoId);

    }


    /**
    * Validar
    *
    *
    * @author Gustavo Botega
    * @return {type}
    */
    public function Validar($Dados){

        // Validar inputs e regras de negocio do formulario. Tratar erro.

        return true;

    }




    /**
    * Gravar
    *
    * @author Gustavo Botega
    */
    public function Gravar($Dados){


        foreach ($Dados as $key => $value) {
            if(empty($value))
                $Dados[$key]    =   NULL;
        }

        // Dados
        $Dataset       =  array(

            'fk_aut_id'                                 =>    $this->session->userdata('PessoaId'),
            'fk_eve_id'                                 =>    $Dados['evento-id'],
            'fk_evs_id'                                 =>    $Dados['altura'],
            'ers_nome'                                  =>    $Dados['nome-serie'],
            'ers_valor_ate_inscricao_definitiva'        =>    str_replace(',','.',str_replace('.','',$Dados['valor-ate-inscricao-definitiva'])),
            'ers_valor_apos_inscricao_definitiva'       =>    str_replace(',','.',str_replace('.','',$Dados['valor-apos-inscricao-definitiva'])),
            'criado'                                    =>    date("Y-m-d H:i:s")

        );


        /* Query */
        $Query = $this->db->insert('tb_evento_rel_serie', $Dataset);

        return ($Query) ? $this->db->insert_id() : false;

    }



    /**
    * GravarCategoriasDaSerie
    *
    * @author Gustavo Botega
    */
    public function GravarCategoriasDaSerie($Categorias, $SerieId){


        foreach ($Categorias as $key => $value) {

            // Dados
            $Dataset       =  array(
                'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
                'fk_ers_id'                         =>    $SerieId,
                'fk_evc_id'                         =>    $value,
                'criado'                            =>    date("Y-m-d H:i:s")
            );

            /* Query */
            $Query = $this->db->insert('tb_evento_rel_serie_rel_categoria', $Dataset);

        }

    }






    /**
    * PackagesClass
    *
    * @author Gustavo Botega
    */
    public function ThemeComponent(){



        /* STYLES
        -----------------------------------*/

            // PLUGINS  //
                $this->data['StylesFile']['Plugins']['bootstrap-select']                            = TRUE;
                $this->data['StylesFile']['Plugins']['multi-select']                                = TRUE;
                $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
                $this->data['StylesFile']['Plugins']['select2-bootstrap']                           = TRUE;



            // STYLES  //

                /* {NamePackage} */
                $this->data['StylesFile']['Styles']['profile']                                      = TRUE;




        /* SCRIPTS
        -----------------------------------*/

            // PLUGINS  //

                /* {NamePackage} */
                $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
                $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
                $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
                $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;

                /* Multiple Select */
                $this->data['ScriptsFile']['Plugins']['jquery-multi-select']                      = TRUE;
                $this->data['ScriptsFile']['Plugins']['select2-full']                             = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-select']                         = TRUE;

                /* User Profile */
                $this->data['ScriptsFile']['Plugins']['jquery-sparkline']                         = TRUE;






            // SCRIPTS //

                /* Bootstrap Sweet Alert */
                $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                  = TRUE;

                /* Multiple Select */
                $this->data['ScriptsFile']['Scripts']['components-multi-select']        = TRUE;

                /* User Profile */
                $this->data['ScriptsFile']['Scripts']['profile']                        = TRUE;

    }



    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega
    * @return array
    */
    public function SigepeAsset()
    {
        /* Carregando Estilos */
//        $this->data['PackageStyles'][]    =   'packages/frontoffice/style/cadastro-serie';


        /* Carregando Scripts */
//        $this->data['PackageScripts'][]   =   'packages/frontoffice/script/cadastro-serie';
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Evento/BackOffice/FormValidationCadastroSerie';


    }



     /*
             // @@@ Implementar permissões por usuario

        // AUDITORIA:
            [0]=> Slug Module
            [1]=> Table Database
            [2]=> Column Database
            [3]=> Value Deleted
            [4]=> Folder Thumb
            [5]=> Delete Gallery
            [6]=> Table Gallery
            [7]=> Column Table Gallery
            [8]=> Data/hora
            [9]=> IP
            [10]=> ID Usuário
            [11]=> Type of Operation
            */

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
