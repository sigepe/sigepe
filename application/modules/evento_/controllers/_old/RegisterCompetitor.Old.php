<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_GuestOffice.php';

/*

    # Pre-definicoes
        - Competidor: animal ou atleta
        - Empresa: Confederação, Federação, Entidade / Escola 
    
    # Classe responsavel por:
        1) Realizar a gravação dos dados basicos referentes ao competidor.
        2) Gravar o vinculo do competidor com alguma empresa.  
        3) Gravar o historico do vinculo. 
        4) Gravar o Registro do Competitor
        5) Gravar o Historico do Registro
    
*/

class RegisterCompetitor extends MY_GuestOffice {

    public $data;

    function __construct()
    {
        parent::__construct();


        $this->data = array();
        $this->load->module('register');
        $this->data = $this->register->settings();

        /* Deletando Session */
        $this->DestroySessionRegisterCompetitor();
    }


    /*
     * RecordAthlete
     * @author Gustavo Botega 
     * @param     
     * @return boolean  
    */
    public function RecordAthlete() {

        /* Unserialize Data */
        parse_str( $_POST['DataSerialized'] , $DataSet);

        /* Setando como NULL caso não houver valor vazio */
        foreach ($DataSet as $key => $value) {
            if(empty($value))
                $value = NULL;
            $DataSet[$key]   =   $value;
        }   

        /* Entity / School */
        $AffiliatedEntity               =   $_POST['AffiliatedEntity'];
        $RidingSchool                   =   $_POST['RidingSchool'];

        if( !empty($AffiliatedEntity) || strlen($AffiliatedEntity) > 3 ){
            $TypeEntrySlugEntitySchool  =   'entity';
            $RidingSchool               =   NULL;
        }   

        if( !empty($RidingSchool) || strlen($RidingSchool) > 3 )   {
            $TypeEntrySlugEntitySchool  =   'school';
            $AffiliatedEntity           =   NULL;
        }


        /* Autor - se o usuario estiver autenticado na plataforma o autor do registro e o proprio usuario caso nao e o Guest. */
        if($this->session->userdata('UserAuthenticated')){
            $Author     =   $this->session->userdata('UserId');
        }else{
            $Author     =   $_POST['PeopleId'];
        }



        /* Tupla de Dados */
        $DataBulk       =  array(
                'fk_con_id'                 =>    '37', // Confederacao
                'fk_fed_id'                 =>    $DataSet['federation'], // Federacao
                'fk_ent_id'                 =>    $AffiliatedEntity, // Entidade Filiada
                'fk_sch_id'                 =>    $RidingSchool, // Escola de Equitacao
                'fk_aut_id'                 =>    $Author, // Autor
                'fk_peo_id'                 =>    $_POST['PeopleId'], // Pessoa
                'fk_sta_id'                 =>    '18', // Status - Atleta Ativo
                'fk_eny_id'                 =>    '21', // Entry
                'fk_typ_id'                 =>    $_POST['TypeEntry'], // Type Entry - 1 Anual,  2 Copa ou 3 Unica
                'ath_nameCompetitor'        =>    $DataSet['nameCompetitor'], // Nome de Competicao
                'ath_entryCbh'              =>    $DataSet['entryCbh'], // N Registro CBH
                'ath_entryFei'              =>    $DataSet['entryFei'], // N Registro FEI
                'createdAt'                 =>    date("Y-m-d H:i:s")
        );

        /* Query */
        $query = $this->db->insert( 'tb_people_athlete', $DataBulk );
        if($query)
        {
            $DataBulk['TypeCompetitor']             =   '1';
            $DataBulk['TypeCompetitorSlug']         =   'athlete';
            $DataBulk['LastInsertId']               =   $this->db->insert_id();
            $DataBulk['TypeEntrySlugEntitySchool']  =   $TypeEntrySlugEntitySchool;
            $DataBulk['AffiliatedEntityId']         =   $AffiliatedEntity;
            $DataBulk['RidingSchoolId']             =   $RidingSchool;
            $Handling   =   $this->Handling( $DataBulk );
            if($Handling){

                $HandlingEntry  =   $this->HandlingEntry( $DataBulk, $DataSet, $_POST['TypeEntry'] );
                if($HandlingEntry){
                    $array = array('status'    =>  'processed' );
                    echo json_encode($array);
                }
                else{
                    $DataBulk['Message']    =      'Erro HandlingEntry';
                    $this->RegisterLog( $DataBulk );                    
                }

            }
        }

        if(!$query){
            $DataBulk                               =   array();
            $DataBulk['Message']                    =      '';
            $DataBulk['Author']                     =      $Author;
            $this->RegisterLog($DataBulk);
        }

    }






    /*
     * RecordAnimal
     * @author Gustavo Botega 
     * @param     
     * @return boolean  
    */
    public function RecordAnimal() {

        /* Unserialize Data */
        parse_str( $_POST['DataSerialized'] , $DataSet);


        /* Setando como NULL caso não houver valor vazio */
        foreach ($DataSet as $key => $value) {
            if(empty($value))
                $value = NULL;
            $DataSet[$key]   =   $value;
        }   


        /* Entity / School */
        $AffiliatedEntity               =   $_POST['AffiliatedEntity'];
        $RidingSchool                   =   $_POST['RidingSchool'];
        $Federation                     =   $_POST['Federation'];

        if( !empty($AffiliatedEntity) || strlen($AffiliatedEntity) > 3 ){
            $TypeEntrySlugEntitySchool  =   'entity';
            $RidingSchool               =   NULL;
        }   

        if( !empty($RidingSchool) || strlen($RidingSchool) > 3 )   {
            $TypeEntrySlugEntitySchool  =   'school';
            $AffiliatedEntity           =   NULL;
        }


        /* Autor - se o usuario estiver autenticado na plataforma o autor do registro e o proprio usuario caso nao e o Guest. */
        if($this->session->userdata('UserAuthenticated')){
            $Author     =   $this->session->userdata('UserId');
        }else{
            $Author     =   $_POST['PeopleId'];
        }


        /* Type Gender */
        ( !isset( $DataSet['type-gender'] ) ) ? $TypeGender = NULL : $TypeGender = $DataSet['type-gender'] ;

        /* Nationality */
        $Nationality    =   $this->model_crud->get_rowSpecific('tb_nationality', 'nat_acronym', $DataSet['nationality'], 1, 'nat_id');

        /* Chip */
        $Chip           =   $_POST['Chip'];

        /* Tupla de Dados */
        $DataBulk       =  array(
            'fk_aut_id'                 =>    $Author,
            'fk_rac_id'                 =>    $DataSet['race'], // Raca do Animal
            'fk_fur_id'                 =>    $DataSet['fur'], // Pelagem do Animal
            'fk_gen_id'                 =>    $DataSet['gender'], // Genero 1 - Macho  2 - Femea
            'fk_typ_id'                 =>    $TypeGender, // Tipo do Genero  1 - Inteiro   2 - Castrado
            'fk_nat_id'                 =>    $Nationality,

            'fk_con_id'                 =>    '37', // Confederacao
            'fk_fed_id'                 =>    $Federation, // Federacao
            'fk_ent_id'                 =>    $AffiliatedEntity, // Entidade Filiada
            'fk_sch_id'                 =>    $RidingSchool, // Escola de Equitacao


            'fk_peo_id'                 =>    NULL, // ID Pessoa - Proprietario
            'fk_com_id'                 =>    NULL, // ID Company - Proprietario
            'fk_own_id'                 =>    NULL, // Tipo do Proprietario  1 - People  2 - Company

            'fk_pas_id'                 =>    NULL, // ID Passaporte
            'fk_sta_id'                 =>    29, // ID Status - 29 Animal Ativo

            'ani_fullName'              =>    $DataSet['full-name'],
            'ani_sponsoredName'         =>    $DataSet['sponsored-name'],
            'ani_fei'                   =>    $DataSet['fei'],
            'ani_cbh'                   =>    $DataSet['cbh'],
            'ani_weight'                =>    $DataSet['weight'],
            'ani_coverFei'              =>    $DataSet['cover-fei'],
            'ani_crossHeight'           =>    $DataSet['cross-height'],
            'ani_nameFather'            =>    $DataSet['name-father'],
            'ani_nameMother'            =>    $DataSet['name-mother'],
            'ani_maternalGrandfather'   =>    $DataSet['maternal-grand-father'],
            'ani_chip'                  =>    $Chip,
            'ani_genealogicalRecord'    =>    $DataSet['genealogical-record'],
            'ani_note'                  =>    NULL,
            'createdAt'                 =>    date("Y-m-d H:i:s")

        );

        /* Query */
        $query = $this->db->insert( 'tb_animal', $DataBulk );
        if($query)
        {
            $DataBulk['TypeCompetitor']             =   '2';
            $DataBulk['TypeCompetitorSlug']         =   'animal';
            $DataBulk['LastInsertId']               =   $this->db->insert_id();
            $DataBulk['TypeEntrySlugEntitySchool']  =   $TypeEntrySlugEntitySchool;
            $DataBulk['AffiliatedEntityId']         =   $AffiliatedEntity;
            $DataBulk['RidingSchoolId']             =   $RidingSchool;
            $Handling   =   $this->Handling( $DataBulk );
            if($Handling){

                $HandlingEntry  =   $this->HandlingEntry( $DataBulk, $DataSet, $_POST['TypeEntry'] );
                if($HandlingEntry){
                    $array = array('StatusResponse'    =>  TRUE );
                    echo json_encode($array);
                }
                else{
                    $DataBulk['Message']    =      'Erro HandlingEntry';
                    $this->RegisterLog( $DataBulk );                    
                }

            }
        }

        if(!$query){
            $DataBulk                               =   array();
            $DataBulk['Message']                    =      '';
            $DataBulk['Author']                     =      $Author;
            $this->RegisterLog($DataBulk);
        }

    }




    /*
     * Handling
     * @author Gustavo Botega 
     * @param     
     * @return boolean  
    */
    public function Handling( array $DataBulk ) {

        /*
            Confederation
        */
        $RelationshipCompanyConfederation       =   $this->RecordRelationshipCompetitorCompany( 'confederation', $DataBulk );
        if(!$RelationshipCompanyConfederation){
            $DataBulk['Message']    =      'Erro gravação confederacao';
            $this->RegisterLog( $DataBulk );
            return false;
        }
        $HistoricRelationshipConfederation      =    $this->RecordHistoricRelationship( $DataBulk, $RelationshipCompanyConfederation );
        if(!$HistoricRelationshipConfederation){
            $DataBulk   =   array();
            $DataBulk['Message']    =      'Erro gravação historico confederacao';
            $this->RegisterLog( $DataBulk );
            return false;
        }


        /*
            Federation
        */
        $RelationshipCompanyFederation       =   $this->RecordRelationshipCompetitorCompany( 'federation', $DataBulk );
        if(!$RelationshipCompanyFederation){
            $DataBulk   =   array();
            $DataBulk['Message']    =      'Erro gravação federacao';
            $this->RegisterLog($DataBulk);
            return false;
        }
        $HistoricRelationshipFederation      =    $this->RecordHistoricRelationship( $DataBulk, $RelationshipCompanyFederation );
        if(!$HistoricRelationshipFederation){
            $DataBulk   =   array();
            $DataBulk['Message']    =      'Erro gravação historico federacao';
            $this->RegisterLog($DataBulk);
            return false;
        }



        /*
            Entity
        */
        if($DataBulk['TypeEntrySlugEntitySchool'] == 'entity'):
            $RelationshipCompanyEntity              =   $this->RecordRelationshipCompetitorCompany( 'entity', $DataBulk );
            if(!$RelationshipCompanyFederation){
                $DataBulk   =   array();
                $DataBulk['Message']    =      'Erro gravação entidade';
                $this->RegisterLog($DataBulk);
                return false;
            }
            $HistoricRelationshipEntity      =    $this->RecordHistoricRelationship( $DataBulk, $RelationshipCompanyEntity );
            if(!$HistoricRelationshipEntity){
                $DataBulk   =   array();
                $DataBulk['Message']    =      'Erro gravação do Historico Entidade. Erro ';
                $this->RegisterLog($DataBulk);
                return false;
            }
        endif;



        /*
            School
        */
        if($DataBulk['TypeEntrySlugEntitySchool'] == 'school'):
            $RelationshipCompanySchool       =   $this->RecordRelationshipCompetitorCompany( 'school', $DataBulk );
            if(!$RelationshipCompanySchool){
                $DataBulk   =   array();
                $DataBulk['Message']    =      'Erro gravação escola';
                $this->RegisterLog($DataBulk);
                return false;
            }
            $HistoricRelationshipSchool      =    $this->RecordHistoricRelationship( $DataBulk, $RelationshipCompanySchool );
            if(!$HistoricRelationshipSchool){
                $DataBulk   =   array();
                $DataBulk['Message']    =      'Erro gravação historico escola';
                $this->RegisterLog($DataBulk);
                return false;
            }
        endif;



        /*
            Peocom
        */
        if($DataBulk['TypeCompetitorSlug'] == 'athlete'):
            $Peocom       =   $this->RecordPeocom( $DataBulk );
            if(!$Peocom){
                $DataBulk   =   array();
                $DataBulk['Message']    =      'Erro gravação Peocom: tb_relationship_peocom_type.';
                $this->RegisterLog($DataBulk);
                return false;
            }
            $HistoricEntry      =    $this->RecordHistoricPeocom( $DataBulk, $Peocom );
            if(!$HistoricEntry){
                $DataBulk   =   array();
                $DataBulk['Message']    =      'Erro gravação do historico Entry. ID do Atleta:';
                $this->RegisterLog($DataBulk);
                return false;
            }        
        endif;

        /*
            FlagRegisterComplete
        */

        $FlagRegisterComplete       =   $this->UpdateRegisterComplete( $DataBulk );
        if(!$FlagRegisterComplete){
            $DataBulk   =   array();
            $DataBulk['Message']    =      'Erro alteração do flag register complete.';
            $this->RegisterLog($DataBulk);
            return false;
        }

        return true;

    }




    /*
     * HandlingEntry
     * @author Gustavo Botega 
     * @param     
     * @return   
    */
    public function HandlingEntry( array $DataBulk, array $DataSet, $TypeEntry ) {

        /*
            Entry
        */
        if( $TypeEntry == '1' ){ // yearly

            $Entry       =   $this->RecordEntry( $DataBulk, $DataSet['listModalityYearly'] );
            if(!$Entry):
                $DataBulk   =   array();
                $DataBulk['Message']    =      'Erro gravação Entry. ID do Atleta:';
                $this->RegisterLog($DataBulk);
                return false;
            endif;

            $HistoricEntry      =    $this->RecordHistoricEntry( $DataBulk, $Entry );
            if(!$HistoricEntry):
                $DataBulk   =   array();
                $DataBulk['Message']    =      'Erro gravação do historico Entry. ID do Atleta:';
                $this->RegisterLog($DataBulk);
                return false;
            endif;

        }

        if( $TypeEntry == '2' || $TypeEntry == '3'){  // cup || single 


                $Entry       =   $this->RecordEntry( $DataBulk, $EntryPrice );
                if(!$Entry){
                    $DataBulk   =   array();
                    $DataBulk['Message']    =      'Erro gravação Entry. ID do Atleta:';
                    $this->RegisterLog($DataBulk);
                    return false;
                }
                $HistoricEntry      =    $this->RecordHistoricEntry( $DataBulk, $Entry );
                if(!$HistoricEntry){
                    $DataBulk   =   array();
                    $DataBulk['Message']    =      'Erro gravação do historico Entry. ID do Atleta:';
                    $this->RegisterLog($DataBulk);
                    return false;
                }

        }

        return true;
    }


    /*
     * RecordRelationshipCompetitorCompany
     * 
     * Grava o relacionamento do competidor ( atleta ou animal ) com alguma empresa ( confederacao, federacao, entidade / escola )
     *
     * @author Gustavo Botega 
     * @param     
     * @return boolean  retorna TRUE se o CPF existir
    */
    public function RecordRelationshipCompetitorCompany( $TypePeocom, array $DataBulk) {


        /* Type Competitor */
        switch ($DataBulk['TypeCompetitorSlug']) {
            case 'athlete':
                $TypeCompetitor     =   1;                       
                $AnimalId           =   NULL;
                $AthleteId          =   $DataBulk['LastInsertId'];
                break;
            
            case 'animal':
                $TypeCompetitor     =   2;                       
                $AthleteId          =   NULL;
                $AnimalId           =   $DataBulk['LastInsertId'];
                break;
        }

        /* Type Peocom */
        switch ($TypePeocom) {
            case 'confederation':
                $CompanyId          =   '37';
                $TypePeocom         =   13;                       
                break;
            
            case 'federation':
                $CompanyId          =   $DataBulk['fk_fed_id'];
                $TypePeocom         =   14;                       
                break;
            
            case 'entity':
                $CompanyId          =   $DataBulk['AffiliatedEntityId'];
                $TypePeocom         =   15;                       
                break;
            
            case 'school':
                $CompanyId          =   $DataBulk['RidingSchoolId'];;
                $TypePeocom         =   16;                       
                break;
        }

        /* Tupla de Dados */
        $DataSet       =  array(
                'fk_aut_id'                 =>  $DataBulk['fk_aut_id'],
                'fk_ath_id'                 =>  $AthleteId,
                'fk_ani_id'                 =>  $AnimalId,
                'fk_com_id'                 =>  $CompanyId,
                'fk_typ_id'                 =>  $TypePeocom,
                'fk_sta_id'                 =>  23,
                'rel_note'                  =>  NULL,
                'rel_startBond'             =>  date("Y-m-d H:i:s"),
                'creadtedAt'                =>  date("Y-m-d H:i:s")
        );

        /* Query */
        $query = $this->db->insert( 'tb_relationship_competitor_company', $DataSet );
        if(!$query)
            return false;

        return $this->db->insert_id();
    }




    /*
     * RecordHistoricRelationship
     * 
     * {desc}
     *
     * @author Gustavo Botega 
     * @param     
     * @return boolean 
    */
    public function RecordHistoricRelationship( array $DataBulk, $RelationshipId ) {

        /* Tupla de Dados */
        $DataSet       =  array(
                'fk_aut_id'                 =>  $DataBulk['fk_aut_id'],  
                'fk_rel_id'                 =>  $RelationshipId,
                'fk_sta_id'                 =>  23,
                'his_note'                  =>  '',
                'createdAt'                 =>  date("Y-m-d H:i:s")
        );

        /* Query */
        $query = $this->db->insert( 'tb_historic_relationship_competitor_company', $DataSet );
        if(!$query)
            return false;
    
        return true;
    }




    /*
     * RecordEntry
     * 
     * 
     *
     * @author Gustavo Botega 
     * @param     
     * @return  
    */
    public function RecordEntry( $DataBulk, $EntryPrice ) {


        /* Type Competitor */
        switch ($DataBulk['TypeCompetitorSlug']) {
            case 'athlete':                 
                $CompetitorType     =   1;
                $AnimalId           =   NULL;
                $AthleteId          =   $DataBulk['LastInsertId'];
                $StatusId           =   '21'; //  Registro Atleta - Aguardando Pagamento
                break;
            
            case 'animal':                  
                $CompetitorType     =   2;
                $AthleteId          =   NULL;
                $AnimalId           =   $DataBulk['LastInsertId'];
                $StatusId           =   '22'; //  Registro Animal - Aguardando Pagamento
                break;
        }



        /* EntryPrice */
        $DataBulkEntryPrice     =   $this->m_crud->get_allWhere('tb_type_entry_price', 'pri_id', $EntryPrice);
        $DataBulkEntryPrice     =   $DataBulkEntryPrice[0];


        /* Lim Year */
        $query = "
            SELECT * FROM tb_type_entry_date_limit 
            WHERE lim_year = ".date('Y')." AND
                  fk_sta_id = 1            
            ORDER BY lim_id DESC LIMIT 1
        ";        
        $DataBulkLimitYear = $this->db->query($query)->result();
        $DataBulkLimitYear = $DataBulkLimitYear[0];



        /* Modality */
        $DataBulkModality     =   $this->m_crud->get_allWhere('tb_modality', 'mod_id', $DataBulkEntryPrice->fk_mod_id);
        $DataBulkModality     =   $DataBulkModality[0];


        /* Discount */
        $query = "
            SELECT * FROM tb_type_entry_month_discount 
            WHERE   fk_mod_id = ".$DataBulkEntryPrice->fk_mod_id." AND
                    fk_sta_id = 1  AND
                    fk_com_id= ".$CompetitorType." AND
                    MONTH(dis_month)  =  MONTH(NOW())
                    ORDER BY dis_id DESC LIMIT 1
        ";        
        $DataBulkDiscount = $this->db->query($query)->result();

        if(!empty($DataBulkDiscount)){
            $DiscountId                     =   $DataBulkDiscount[0]->dis_id;
            $DiscountMonth                  =   $DataBulkDiscount[0]->dis_month;   
            $DiscountPercentageDiscount     =   $DataBulkDiscount[0]->dis_discountPercentage;               
        }else{
            $DiscountId                     =   NULL;
            $DiscountMonth                  =   NULL;   
            $DiscountPercentageDiscount     =   NULL;               
        }




        /* Tupla de Dados */
        $DataSet       =  array(
            'fk_aut_id'                     =>      $DataBulk['fk_aut_id'],
            'fk_ath_id'                     =>      $AthleteId, // ID Atleta
            'fk_ani_id'                     =>      $AnimalId, // ID Animal
            'fk_mod_id'                     =>      $DataBulkEntryPrice->fk_mod_id, // ID Modalidade
            'fk_typ_id'                     =>      $DataBulk['fk_typ_id'], // Tipo do Registro - Anual, Copa ou Unica
            'fk_sta_id'                     =>      $StatusId, // Status do Registro - Aguardando Pagamento
            'fk_com_id'                     =>      $DataBulk['TypeCompetitor'], // Tipo do Competidor - 1 Atleta ou 2 Animal
            'fk_pri_id'                     =>      $DataBulkEntryPrice->pri_id, // Preco - Referencia tabela tb_type_entry_price
            'fk_dis_id'                     =>      $DiscountId, // Desconto - Referencia tabela: tb_type_entry_month_discount
            'fk_lim_id'                     =>      $DataBulkLimitYear->lim_id, // Data Limite - Referencia tabela: tb_type_entry_date_limit
            'ent_dis_month'                 =>      $DiscountMonth, // Desconto - Mes se aplica o desconto
            'ent_dis_percentageDiscount'    =>      $DiscountPercentageDiscount, // Porcentagem de Desconto
            'ent_pri_before'                =>      $DataBulkEntryPrice->pri_before, // Preco Antes do desconto da data limite
            'ent_pri_after'                 =>      $DataBulkEntryPrice->pri_after, // Preco Depois do desconto da data limite
            'ent_lim_dateLimit'             =>      $DataBulkLimitYear->lim_dateLimit, // Data Limite de Desconto. Registros realizados antes ou nessa data entram no preco antes ( pri_before ) e depois dessa data no preco depois ( pri_after ) 
            'ent_lim_year'                  =>      $DataBulkLimitYear->lim_year, // Ano da Data Limite
            'ent_mod_title'                 =>      $DataBulkModality->mod_title, // Modalidade Titulo
            'ent_mod_slug'                  =>      $DataBulkModality->mod_slug, // Modalidade Slug
            'createdAt'                     =>      date("Y-m-d H:i:s")
        );

        /* Query */
        $query = $this->db->insert( 'tb_entry', $DataSet );
        if(!$query)
            return false;

        return array( 'EntryId' => $this->db->insert_id(), 'StatusId' => $StatusId );
    }


    

    /*
     * RecordHistoricEntry
     * 
     * {desc}
     *
     * @author Gustavo Botega 
     * @param     
     * @return  
    */
    public function RecordHistoricEntry( $DataBulk, $Entry ) {

        /* Tupla de Dados */
        $DataSet       =  array(
                
                'fk_ent_id'     =>  $Entry['EntryId'],
                'fk_aut_id'     =>  $DataBulk['fk_aut_id'],
                'fk_sta_id'     =>  $Entry['StatusId'],
                'his_note'      =>  '',
                'createdAt'     =>  date("Y-m-d H:i:s")

        );

        /* Query */
        $query = $this->db->insert( 'tb_historic_entry', $DataSet );
        if(!$query)
            return false;
    
        return true;
    }





    /*
     * RecordPeocom
     * 
     * 
     *
     * @author Gustavo Botega 
     * @param     
     * @return boolean  retorna TRUE se o CPF existir
    */
    public function RecordPeocom( array $DataBulk) {


        /* Tupla de Dados */
        $DataSet       =  array(
                'fk_aut_id'                 =>  $DataBulk['fk_aut_id'],
                'fk_peo_id'                 =>  $DataBulk['fk_peo_id'],
                'fk_com_id'                 =>  NULL,
                'fk_typ_id'                 =>  3, // Atleta
                'fk_sta_id'                 =>  18,
                'rel_reference'             =>  $DataBulk['LastInsertId'],
                'createdAt'     =>  date("Y-m-d H:i:s")
        );


        /* Query */
        $query = $this->db->insert( 'tb_relationship_peocom_type', $DataSet );
        if(!$query)
            return false;

        return $this->db->insert_id();
    }


    /*
     * RecordPeocom
     * 
     * 
     *
     * @author Gustavo Botega 
     * @param     
     * @return boolean  retorna TRUE se o CPF existir
    */
    public function RecordHistoricPeocom( array $DataBulk, $PeocomId) {


        /* Tupla de Dados */
        $DataSet       =  array(
                'fk_aut_id'                 =>  $DataBulk['fk_aut_id'],
                'fk_rel_id'                 =>  $PeocomId,
                'fk_sta_id'                 =>  18,
                'createdAt'                 =>  date("Y-m-d H:i:s")
        );


        /* Query */
        $query = $this->db->insert( 'tb_historic_relationship_peocom_type', $DataSet );
        if(!$query)
            return false;

        return $this->db->insert_id();
    }


    /*
     * UpdateRegisterComplete
     * 
     * 
     *
     * @author Gustavo Botega 
     * @param     
     * @return boolean  retorna TRUE se o CPF existir
    */
    public function UpdateRegisterComplete( array $DataBulk) {



        /* Query */
        $data = array(
                       'flag_registerComplete' => 1
                    );

        $this->db->where('peo_id', $DataBulk['fk_peo_id']);
        $query = $this->db->update('tb_people', $data); 


        return true;
    }

        
    /**
     * DestroyAuthenticateOwner
     *
     *
     * @author Gustavo Botega 
     * @param int $userId -    
     * @return TRUE 
     */      
    public function DestroySessionRegisterCompetitor()
    {
        $DataSession = array(
            'PeopleId'                          =>      NULL,     
        );
        $this->session->set_userdata($DataSession); 

    }



    /*
     * GetDropdownTypeEntry
     * @author Gustavo Botega 
     * @param     
     * @return boolean 
    */      
    public function GetDropdownTypeEntry() {

        $query = $this->model_crud->select(
                    'tb_type_entry',
                    array('typ_id', 'typ_title', 'typ_position', 'typ_slug'),
                    array(),
                    array('typ_position'=>'asc'),
                    NULL
                );

        return $this->mountingDropdownToCodeigniter($query, 'Selecione um Tipo de Participação', array('value'=>'typ_id', 'data-athletetypeentryslug'=>'typ_slug', 'data-identificationFederation'=>'typ_id'), array('typ_title'=>''), 'athleteTypeEntry', 'class="form-control select2" id="athleteTypeEntry" ');        
    }







    /*
     * DropdownTypeEntrySlug
     * @author Gustavo Botega 
     * @param     
     * @return boolean 
    */      
    public function DropdownTypeEntrySlug($typeEntry, $TypeCompetitor = 1) {

        switch ($typeEntry) {
            case 'yearly':
                $idTypeEntry  =   '1';
               break;
            
            case 'cup':
                $idTypeEntry  =   '2';
            break;
            
            case 'single':
                $idTypeEntry  =   '3';
               break;
        }

        switch ($TypeCompetitor) {
            case 'athlete':
                $TypeCompetitor  =   '1';
               break;
            
            case 'animal':
                $TypeCompetitor  =   '2';
            break;
            
            default:
                $TypeCompetitor  =   '1';
               break;
        }



        /*---------------------------------
            QUERY - TYPE ENTRY PRICE
        -----------------------------------*/
       $query = "
            SELECT m.mod_id, m.mod_title, m.mod_slug, m.mod_status,
                   p.pri_id, p.fk_typ_id, p.fk_mod_id, p.pri_before, p.pri_after, p.pri_status, p.fk_com_id,
                   e.typ_id
            FROM tb_type_entry_price              as p
            INNER JOIN tb_modality              as m ON     p.fk_mod_id = m.mod_id    
            INNER JOIN tb_type_entry_competitor   as c ON     p.fk_com_id = c.com_id     
            INNER JOIN tb_type_entry             as e ON     p.fk_typ_id = e.typ_id      
            WHERE 
                p.pri_status = 1 AND
                m.mod_status = 1 AND
                p.fk_typ_id = ".$idTypeEntry." AND
                p.fk_com_id = ".$TypeCompetitor."
        ";        
        $query = $this->db->query($query)->result();


        /*---------------------------------
            QUERY DATE LIMIT
        -----------------------------------*/
        $typeEntryDateLimit = $this->model_crud->select(
                    'tb_type_entry_date_limit',
                    array('*'),
                    array('lim_year =' => date('Y'), 'fk_sta_id =' => '1'),
                    array(),
                    1   
                );
        $dateLimit    =   $typeEntryDateLimit[0]->lim_dateLimit;


        /*---------------------------------
            CHECKING DATE LIMIT 
        -----------------------------------*/
        $dateCurrent     =   date("Y-m-d");
        if( strtotime($dateCurrent) > strtotime($dateLimit) ){
            // passou a data limite
            $dateLimitColumn  =   'pri_after';
        }else{
            // nao passou a data limite
            $dateLimitColumn  =   'pri_before';
        }





        /*---------------------------------
            FOREACH 
        -----------------------------------*/
        foreach ($query as $key => $value) {
            $price  =   '';
           $arrAux[$key]['mod_slug'] =   $value->mod_slug;
           $arrAux[$key]['mod_title'] =   $value->mod_title;
           $arrAux[$key]['pri_id']    =   $value->pri_id;

           /* Environment Variables - Scope Function */
           $idModality          =   $value->mod_id;
           $idTypeEntry         =   $value->typ_id;
           $idTypeCompetitor    =   $value->fk_com_id;


        /*---------------------------------
            QUERY DISCOUNTS
            - Leitura da Query:
            Banco de Dados, traga todos os descontos de acordo com:
            a modalidade, tipo de competidor e tipo de registro. 
            Exiba somente os descontos do mes corrente.
            Exiba somente as modalidades atidadades.
            Exiba somente de competidores tipo atleta
        -----------------------------------*/
       $queryTypeEntryMonthDiscount = "
            SELECT *
            FROM tb_type_entry_month_discount   as d
            INNER JOIN tb_modality              as m    ON d.fk_mod_id = m.mod_id /* modalidade */
            INNER JOIN tb_type_entry_competitor as c    ON d.fk_com_id = c.com_id /* competidor ( pessoa ou atleta ) */
            INNER JOIN tb_type_entry            as e    ON d.fk_typ_id = e.typ_id /* tipo de registro */
            WHERE 
                d.fk_mod_id = '".$idModality."' AND
                m.mod_status = 1 AND
                d.fk_typ_id = '".$idTypeEntry."' AND /* ser dinamico esse fk_typ_id */
                d.fk_com_id = '".$idTypeCompetitor."' AND
                month(d.dis_month) = month(curdate())   /* selecionando apenas os descontos para o mes atual */
        ";
        $queryMonthDiscount = $this->db->query($queryTypeEntryMonthDiscount)->result();

            $discountReal   =   0;
            if(!empty($queryMonthDiscount)){
            /*
                se passar nesse bloco quer dizer que existe algum desconto
                [ nao permitir o admin cadastrar descontos iguais . criterios ( tipo de registro, modalidade e tipo de pessoa )]
                [ se tiver desconto, tratar caso quando o valor do preco for zero ]
                [ nesse bloco de if pode ter apenas um registro ]
            */

            $discountPercentage     =   $queryMonthDiscount[0]->dis_discountPercentage;
            $discountReal         =    ( $discountPercentage / 100 ) * $value->$dateLimitColumn;
        }



        /*---------------------------------
            DISCOUNTS PER DATE ( AFTER, BEFORE )
        -----------------------------------*/
           $arrAux[$key]['pri_price'] =   $value->$dateLimitColumn - $discountReal;
        }

        return $arrAux;        
    }



}
