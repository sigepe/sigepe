<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_CommonOffice.php';

class AthleteEntryBackOffice extends MY_CommonOffice {

    public $data;

    function __construct()
    {
        parent::__construct();
        
        $this->data = array();
        $this->load->module('register');
        $this->data = $this->register->settings();
        $this->data['use_formPeocom']               =   TRUE;
        $this->data['use_formPeople']               =   TRUE;
        $this->data['use_templateRegisterAthlete']  =   TRUE;
        $this->data['use_formWizardAthlete']        =   TRUE;
        $this->data['FormIdentification']           =   "formAthlete";

        $this->clear_cache();
    }



    /*
      Atividades
    ----------------------------------------------*/
    /*
        1) Verificar se já existe um cpf e notificar. Bloquear cadastro se já existir.
        2) Se a pessoa for menor de idade retornar aviso e pedir pra informar o CPF do resposável.
    */


    public function index() {
		$this->form();    	
    }


    /*
      Form
    ----------------------------------------------*/
    public function form() {

 
        /* Selects */
        $this->data['dropdownMaritalStatus']    =   $this->register->getDropdownMaritalStatus();
        $this->data['dropdownScholarity']       =   $this->register->getDropdownScholarity();
        $this->data['dropdownBloodType']        =   $this->register->getDropdownBloodType();
        $this->data['dropdownState']            =   $this->register->getDropdownState();
        $this->data['dropdownNationality']      =   $this->register->getDropdownNationality();
        $this->data['dropdownFederation']       =   $this->register->getDropdownFederation();
        $this->data['dropdownTypeTelephone']    =   $this->register->getDropdownTypeTelephone();
       // $this->data['dropdownNaturalness']     =   $this->register->getDropdownNaturalness();
        $this->data['dropdownTypeEntry']        =   $this->getDropdownTypeEntry();
        $this->data['dropdownTypeEntryYearly']  =   $this->dropdownTypeEntrySlug('yearly');
        $this->data['dropdownTypeEntryCup']     =   $this->dropdownTypeEntrySlug('cup');
        $this->data['dropdownTypeEntrySingle']  =   $this->dropdownTypeEntrySlug('single');



        /* Settings */
        $this->settings();

        $path   =   'Register/Athlete/';
        $this->loadTemplateRegister(
            array(

                $path . 'Header',
                $path . 'Tab1',
                $path . 'Tab2',
                $path . 'Tab3',
                $path . 'Tab4',
                $path . 'Modal',
                $path . 'Footer'

            ), $this->data
        );            
    }




    /**
     * Settings   
     *
     *
     * @author Gustavo Botega 
     * @param     
     * @return  
     */      
    public function settings() {

        $this->data['databaseTable']                =   '';
        $this->data['databaseColumnPrimaryKey']     =   '';

        /* Packages */
        $this->data['use_formWizard']     =   TRUE;

        return $this->data;
    }





    /***********************************************    
      getDropdownTypeEntry
    ***********************************************/
    public function getDropdownTypeEntry() {

        $query = $this->model_crud->select(
                    'tb_typeEntry',
                    array('typ_id', 'typ_title', 'typ_position', 'typ_slug'),
                    array(),
                    array('typ_position'=>'asc'),
                    NULL
                );

        return $this->mountingDropdownToCodeigniter($query, 'Selecione um Tipo de Participação', array('value'=>'typ_id', 'data-athletetypeentryslug'=>'typ_slug', 'data-identificationFederation'=>'typ_id'), array('typ_title'=>''), 'athleteTypeEntry', 'class="form-control select2" id="athleteTypeEntry" ');        
    }

    /***********************************************    
      dropdownTypeEntryYearly
    ***********************************************/
    public function dropdownTypeEntrySlug($typeEntry) {

        switch ($typeEntry) {
            case 'yearly':
                $idTypeEntry  =   '1';
               break;
            
            case 'cup':
                $idTypeEntry  =   '2';
            break;
            
            case 'single':
                $idTypeEntry  =   '3';
               break;
        }



        /*---------------------------------
            QUERY - TYPE ENTRY PRICE
        -----------------------------------*/
       $query = "
            SELECT m.mod_id, m.mod_title, m.mod_slug, m.mod_status,
                   p.pri_id, p.fk_typ_id, p.fk_mod_id, p.pri_before, p.pri_after, p.pri_status, p.fk_com_id,
                   e.typ_id
            FROM tb_typeEntryPrice              as p
            INNER JOIN tb_modality              as m ON     p.fk_mod_id = m.mod_id    
            INNER JOIN tb_typeEntryCompetitor   as c ON     p.fk_com_id = c.com_id     
            INNER JOIN tb_typeEntry             as e ON     p.fk_typ_id = e.typ_id      
            WHERE 
                p.pri_status = 1 AND
                m.mod_status = 1 AND
                p.fk_typ_id = ".$idTypeEntry." AND
                p.fk_com_id = 1
        ";        
        $query = $this->db->query($query)->result();


        /*---------------------------------
            QUERY DATE LIMIT
        -----------------------------------*/
        $typeEntryDateLimit = $this->model_crud->select(
                    'tb_typeEntryDateLimit',
                    array('*'),
                    array('lim_year =' => date('Y'), 'lim_status =' => '1'),
                    array(),
                    1   
                );
        $dateLimit    =   $typeEntryDateLimit[0]->lim_dateLimit;


        /*---------------------------------
            CHECKING DATE LIMIT 
        -----------------------------------*/
        $dateCurrent     =   date("Y-m-d");
        if( strtotime($dateCurrent) > strtotime($dateLimit) ){
            // passou a data limite
            $dateLimitColumn  =   'pri_after';
        }else{
            // nao passou a data limite
            $dateLimitColumn  =   'pri_before';
        }





        /*---------------------------------
            FOREACH 
        -----------------------------------*/
        foreach ($query as $key => $value) {
            $price  =   '';
           $arrAux[$key]['mod_title'] =   $value->mod_title;
           $arrAux[$key]['pri_id']    =   $value->pri_id;

           /* Environment Variables - Scope Function */
           $idModality          =   $value->mod_id;
           $idTypeEntry         =   $value->typ_id;
           $idTypeCompetitor    =   $value->fk_com_id;


        /*---------------------------------
            QUERY DISCOUNTS
            - Leitura da Query:
            Banco de Dados, traga todos os descontos de acordo com:
            a modalidade, tipo de competidor e tipo de registro. 
            Exiba somente os descontos do mes corrente.
            Exiba somente as modalidades atidadades.
            Exiba somente de competidores tipo atleta
        -----------------------------------*/
       $queryTypeEntryMonthDiscount = "
            SELECT *
            FROM tb_typeEntryMonthDiscount      as d
            INNER JOIN tb_modality              as m    ON d.fk_mod_id = m.mod_id /* modalidade */
            INNER JOIN tb_typeEntryCompetitor   as c    ON d.fk_com_id = c.com_id /* competidor ( pessoa ou atleta ) */
            INNER JOIN tb_typeEntry             as e    ON d.fk_typ_id = e.typ_id /* tipo de registro */
            WHERE 
                d.fk_mod_id = '".$idModality."' AND
                m.mod_status = 1 AND
                d.fk_typ_id = '".$idTypeEntry."' AND /* ser dinamico esse fk_typ_id */
                d.fk_com_id = '".$idTypeCompetitor."' AND
                month(d.dis_month) = month(curdate())   /* selecionando apenas os descontos para o mes atual */
        ";
        $queryMonthDiscount = $this->db->query($queryTypeEntryMonthDiscount)->result();

            $discountReal   =   0;
            if(!empty($queryMonthDiscount)){
            /*
                se passar nesse bloco quer dizer que existe algum desconto
                [ nao permitir o admin cadastrar descontos iguais . criterios ( tipo de registro, modalidade e tipo de pessoa )]
                [ se tiver desconto, tratar caso quando o valor do preco for zero ]
                [ nesse bloco de if pode ter apenas um registro ]
            */

            $discountPercentage     =   $queryMonthDiscount[0]->dis_discountPercentage;
            $discountReal         =    ( $discountPercentage / 100 ) * $value->$dateLimitColumn;
        }



        /*---------------------------------
            DISCOUNTS PER DATE ( AFTER, BEFORE )
        -----------------------------------*/
           $arrAux[$key]['pri_price'] =   $value->$dateLimitColumn - $discountReal;
        }

        return $arrAux;        
    }





    function clear_cache()
    {
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
    }    


}
