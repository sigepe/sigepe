<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';
class FaturaSimples extends MY_FrontOffice {

    function __construct() {
        parent::__construct();

		$this->ThemeComponent();
		$this->SigepeAsset();

        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');

    }




    public function Dashboard($EventoId, $FaturaSimplesControle){


        /*  EVENTO */
        $SqlEvento                     = '
                                            SELECT
                                                eve_id, fk_sta_id, fk_pes_id, eve_nome, eve_controle, eve_site_logotipo, eve_data_inicio, eve_site_inscricao_fim,  UNIX_TIMESTAMP(eve_site_inscricao_fim) AS DATE
                                            FROM
                                                tb_evento
                                            WHERE
                                                eve_id = '.$EventoId.'

                                            LIMIT 1
                                            ';
        $this->data['DatasetEvento']   = $this->db->query($SqlEvento)->result();



        /*  EVENTO */
        $SqlFaturaSimples                     = "
		                                            SELECT *
		                                            FROM
		                                                tb_evento_faturaglobal_rel_faturasimples
		                                            WHERE
		                                                frf_controle = ".$FaturaSimplesControle."
		                                            LIMIT 1
                                            ";

        $this->data['FaturaSimples']          = $this->db->query($SqlFaturaSimples)->result();
        $this->data['FaturaSimplesControle']  = $FaturaSimplesControle;
        $this->data['EventoId']               = $EventoId;
        $this->data['TipoDeVendaDaInscricao'] = $this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $EventoId, 1, 'fk_evv_id');


        /* ---------------- POLITICA DE PRECO ---------------- */

        $DataLimiteSemAcrescimo               = $this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $EventoId, 1, 'eve_data_limite_sem_acrescimo');



/*
//        Manter bloco pra testes
        $date = strtotime("+5 day", strtotime(date("Y-m-d")));
        $DataLimiteSemAcrescimo     = date("Y-m-d", $date);
*/

        $this->data['DataLimiteSemAcrescimo'] =	$DataLimiteSemAcrescimo;

        $DataHoje                             = date("Y-m-d");
        if($DataLimiteSemAcrescimo >= $DataHoje){// Nao Passou da Data Limite Sem Acrescimo. Utilizar Preco promocional.
            $this->data['PoliticaPreco']        =   1; // Preço Promocional
        } else if ( $DataLimiteSemAcrescimo < $DataHoje ){
            $this->data['PoliticaPreco']        =   2; // Preço Cheio
        }


        // Ultimo dia
//        $DataHoje = $DataLimiteSemAcrescimo; // COMNETA ISSO
        if($DataLimiteSemAcrescimo == $DataHoje)
            $this->data['AvisoEncerramentoDataLimiteSemAcrescimo'] =   TRUE;



        /* --------------------------------------------------- */


        $this->data['PageHeadTitle']      = 'Eventos ';
        $this->data['PageHeadSubtitle']   = 'Confira a relação de eventos que estão com as inscrições em aberto.';



        $this->Resumo($FaturaSimplesControle);


        $this->LoadTemplateUser(
            array(
                'Template/FrontOffice/sistema/Evento/FaturaSimples/Header',
                'Template/FrontOffice/sistema/Evento/FaturaSimples/Resumo',
            ),
                $this->data
        );





    }



    /*
    Manipulador responsavel por chamar os blocos do resumo
    **********************************************/
    public function Resumo($FaturaSimplesControle){

        $this->ResumoValores($FaturaSimplesControle);

    }

    public function ResumoLinhaSerie($FaturaSimplesControle){
        $SqlLinhaSerie                     = "
                                                    SELECT *

                                                    FROM
                                                        tb_evento_faturaglobal_rel_faturasimples

                                                    WHERE
                                                        fk_ers_id IS NOT NULL AND
                                                        frf_controle = ".$FaturaSimplesControle."

                                                    LIMIT 1
                                            ";
        $Query  =   $this->db->query($SqlLinhaSerie)->result();
        if(!is_null($Query) && !empty($Query)){


            $this->data['DatasetLinhaSerie']    =   $Query;
            $this->data['NomeDaSerie']          =   $this->model_crud->get_rowSpecific('tb_evento_rel_serie', 'ers_id', $Query[0]->fk_ers_id, 1, 'ers_nome' );
            $SerieId                            =   $Query[0]->fk_ers_id;

          #  $this->load->module('evento/Serie');


                    $SqlProvasDaSerie            = '
                                                            SELECT * FROM
                                                                tb_evento_rel_serie_rel_prova as srp

                                                            WHERE srp.fk_ers_id = '.$SerieId.'

                                                            ORDER BY
                                                                srp.srp_numero_prova ASC
                                                        ';
                    #$this->data['ProvasDaSerie']    =   $this->serie->GetProvasDaSerie($SerieId);
                    $this->data['ProvasDaSerie']    =  $this->db->query($SqlProvasDaSerie)->result();


            $this->data['ValorPromocionalSerie'] =   $this->model_crud->get_rowSpecific('tb_evento_faturaglobal_rel_faturasimples', 'frf_controle', $FaturaSimplesControle, 1, 'frf_valor_promocional_serie' );
            $this->data['ValorSerie']            =   $this->model_crud->get_rowSpecific('tb_evento_faturaglobal_rel_faturasimples', 'frf_controle', $FaturaSimplesControle, 1, 'frf_valor_serie' );


            return $this->parser->parse('Template/FrontOffice/sistema/Evento/FaturaSimples/Resumo/LinhaSerie', $this->data, true); // bloco de cabecalho
        }else{
            return false;
        }

    }

    public function ResumoLinhaProvaAvulsa($FaturaSimplesControle){



        $FaturaSimplesId                        =   $this->model_crud->get_rowSpecific('tb_evento_faturaglobal_rel_faturasimples', 'frf_controle', $FaturaSimplesControle, 1, 'frf_id' );
        $SqlLinhaProvaAvulsa                     = "
                                                    SELECT *

                                                    FROM
                                                        tb_evento_faturaglobal_rel_faturasimples_rel_inscricao as fri

                                                    INNER JOIN tb_evento_rel_serie_rel_prova as srp
                                                    ON srp.srp_id = fri.fk_srp_id

                                                    WHERE
                                                        fri.flag_deletado IS NULL AND
                                                        fri.flag_serie IS NULL AND
                                                        fri.fk_frf_id = ".$FaturaSimplesId."

                                            ";
        $Query  =   $this->db->query($SqlLinhaProvaAvulsa)->result();
        if(!is_null($Query) && !empty($Query)){


            $this->data['DatasetLinhaProvaAvulsa']    =   $Query;
            return $this->parser->parse('Template/FrontOffice/sistema/Evento/FaturaSimples/Resumo/LinhaProva', $this->data, true);

        }else{
            return false;
        }

    }

    public function ResumoLinhaBaia($FaturaSimplesControle){



        $FaturaSimplesId                        =   $this->model_crud->get_rowSpecific('tb_evento_faturaglobal_rel_faturasimples', 'frf_controle', $FaturaSimplesControle, 1, 'frf_id' );

        $SqlLinhaBaia                     = "
                                                    SELECT *

                                                    FROM
                                                        tb_evento_faturaglobal_rel_faturasimples_rel_baia as frb

                                                    WHERE
                                                        frb.flag_deletado IS NULL AND
                                                        frb.fk_frf_id = '".$FaturaSimplesId."'
                                            ";
        $Query  =   $this->db->query($SqlLinhaBaia)->result();


        if(!is_null($Query) && !empty($Query)){

            $this->data['DatasetLinhaBaia']             =   $Query;
            $this->data['DatasetLinhaBaiaQuantidade']   =   count($Query);


            $SqlSomaValorBaia                     = "

                                                        SELECT *, SUM(frb_valor_promocional) AS ValorPromocionalBaia, SUM(frb_valor) AS ValorBaia
                                                        FROM tb_evento_faturaglobal_rel_faturasimples_rel_baia as frb

                                                        WHERE
                                                            frb.flag_deletado IS NULL AND
                                                            frb.fk_frf_id = '".$FaturaSimplesId."'
                                                ";
            $Query  =   $this->db->query($SqlSomaValorBaia)->result();

            $this->data['DatasetLinhaBaiaValor']             =   $Query[0]->ValorBaia;
            $this->data['DatasetLinhaBaiaValorPromocional']  =   $Query[0]->ValorPromocionalBaia;




            return $this->parser->parse('Template/FrontOffice/sistema/Evento/FaturaSimples/Resumo/LinhaBaia', $this->data, true);

        }else{
            return false;
        }


    }

    public function ResumoLinhaQuartoDeSela($FaturaSimplesControle){



        $FaturaSimplesId                        =   $this->model_crud->get_rowSpecific('tb_evento_faturaglobal_rel_faturasimples', 'frf_controle', $FaturaSimplesControle, 1, 'frf_id' );

        $SqlLinhaQuartoDeSela                     = "
                                                    SELECT *

                                                    FROM
                                                        tb_evento_faturaglobal_rel_faturasimples_rel_quarto as frq

                                                    WHERE
                                                        frq.flag_deletado IS NULL AND
                                                        frq.fk_frf_id = '".$FaturaSimplesId."'
                                            ";
        $Query  =   $this->db->query($SqlLinhaQuartoDeSela)->result();

        if(!is_null($Query) && !empty($Query)){

            $this->data['DatasetLinhaQuarto']             =   $Query;
            $this->data['DatasetLinhaQuartoQuantidade']   =   count($Query);


            $SqlSomaValorQuarto                     = "

                                                        SELECT *, SUM(frq_valor_promocional) AS ValorPromocionalQuarto, SUM(frq_valor) AS ValorQuarto
                                                        FROM tb_evento_faturaglobal_rel_faturasimples_rel_quarto as frq

                                                        WHERE
                                                            frq.flag_deletado IS NULL AND
                                                            frq.fk_frf_id = '".$FaturaSimplesId."'
                                                ";
            $Query  =   $this->db->query($SqlSomaValorQuarto)->result();

            $this->data['DatasetLinhaQuartoValor']             =   $Query[0]->ValorQuarto;
            $this->data['DatasetLinhaQuartoValorPromocional']  =   $Query[0]->ValorPromocionalQuarto;




            return $this->parser->parse('Template/FrontOffice/sistema/Evento/FaturaSimples/Resumo/LinhaQuartoDeSela', $this->data, true);

        }else{
            return false;
        }


    }




    public function ResumoValores($FaturaSimplesControle){


        $SerieValor                             =   0.00;
        $SerieValorPromocional                  =   0.00;
        $ProvaAvulsaValor                       =   0.00;
        $ProvaAvulsaValorPromocional            =   0.00;
        $BaiaValor                              =   0.00;
        $BaiaValorPromocional                   =   0.00;
        $QuartoDeSelaValor                      =   0.00;
        $QuartoDeSelaValorPromocional           =   0.00;
        $ValorTotal                             =   0.00;
        $ValorTotalPromocional                  =   0.00;


        $FaturaSimplesId                        =   $this->model_crud->get_rowSpecific('tb_evento_faturaglobal_rel_faturasimples', 'frf_controle', $FaturaSimplesControle, 1, 'frf_id' );



        /*
             SERIE
        -----------------------------------------------*/
        $SqlValorSerie                     = "
                                                    SELECT *
                                                    FROM
                                                        tb_evento_faturaglobal_rel_faturasimples
                                                    WHERE
                                                        fk_ers_id IS NOT NULL AND
                                                        frf_controle = ".$FaturaSimplesControle."
                                                    LIMIT 1
                                            ";
        $Query  =   $this->db->query($SqlValorSerie)->result();

        if(!empty($Query)):
            if(!is_null($Query[0]->fk_ers_id)){
                $SerieValor                 =   $Query[0]->frf_valor_serie;
                $SerieValorPromocional      =   $Query[0]->frf_valor_promocional_serie;
            }
        endif;



        /*
             PROVAS AVULSAS
        -----------------------------------------------*/
        $SqlValorProvaAvulsa                     = "
                                                    SELECT * FROM
                                                        tb_evento_faturaglobal_rel_faturasimples_rel_inscricao as fri

                                                    WHERE
                                                        fri.flag_deletado IS NULL AND
                                                        fri.flag_serie IS NULL AND
                                                        fri.fk_frf_id = ".$FaturaSimplesId."

                                            ";
        $Query  =   $this->db->query($SqlValorProvaAvulsa)->result();


        if(!is_null($Query)):

            $ProvaAvulsaValor   = 0;
            foreach ($Query as $key => $value) {
                //$ProvaAvulsaValor               += $value->fri_valor;
                /*
                if(!is_null($value->fri_valor_promocional)){
                    $ProvaAvulsaValorPromocional    += $value->fri_valor_promocional;
                }else{
                    $ProvaAvulsaValorPromocional    += $value->fri_valor;
                }*/
                $ProvaAvulsaValor    += $value->fri_valor;

            }


        endif;





        /*
             BAIA
        -----------------------------------------------*/
        $SqlSomaValorBaia                     = "

                                                    SELECT * FROM tb_evento_faturaglobal_rel_faturasimples_rel_baia as frb

                                                    WHERE
                                                        frb.flag_deletado IS NULL AND
                                                        frb.fk_frf_id = '".$FaturaSimplesId."'
                                            ";
        $Query  =   $this->db->query($SqlSomaValorBaia)->result();
        if(!is_null($Query)){

            foreach ($Query as $key => $value) {
                $BaiaValor               += $value->frb_valor;

                if(!is_null($value->frb_valor_promocional)){
                    $BaiaValorPromocional    += $value->frb_valor_promocional;
                }else{
                    $BaiaValorPromocional    += $value->frb_valor;
                }
            }

        }




        /*
             QUARTO DE SELA
        -----------------------------------------------*/
        $SqlSomaValorQuarto                     = "

                                                    SELECT * FROM tb_evento_faturaglobal_rel_faturasimples_rel_quarto as frq

                                                    WHERE
                                                        frq.flag_deletado IS NULL AND
                                                        frq.fk_frf_id = '".$FaturaSimplesId."'
                                            ";
        $Query  =   $this->db->query($SqlSomaValorQuarto)->result();
        if(!is_null($Query)){

            foreach ($Query as $key => $value) {
                $QuartoDeSelaValor               += $value->frq_valor;

                if(!is_null($value->frq_valor_promocional)){
                    $QuartoDeSelaValorPromocional    += $value->frq_valor_promocional;
                }else{
                    $QuartoDeSelaValorPromocional    += $value->frq_valor;
                }
            }

        }





        $ValorTotal                           =      $SerieValor + $ProvaAvulsaValor + $BaiaValor + $QuartoDeSelaValor;


        $ValorTotalPromocional                =      floatval($SerieValorPromocional) + floatval($ProvaAvulsaValor) + floatval($BaiaValorPromocional) + floatval($QuartoDeSelaValorPromocional);

/*
        echo "<br>";
        echo " SERIE VLAOR PROMOCIONAL " . $SerieValorPromocional . "<br>";
        echo "PROVA AVULSA  PROMOCIONAL " . $ProvaAvulsaValorPromocional . "<br>";
        echo "BAIA  PROMOCIONAL " . $BaiaValorPromocional . "<br>";
        echo "QUARTO VALOR PROMOCIONAL " . $QuartoDeSelaValorPromocional . "<br>";

        echo "VALOR TOTAL" . */


        $this->data['ResumoValorTotal']                 =   $ValorTotal;
        $this->data['ResumoValorTotalPromocional']      =   $ValorTotalPromocional;


//        var_dump(floatval($SerieValorPromocional));

    }





    public function GetFaturaSimples($FaturaGlobalId){


        /*  EVENTO */
        $SqlFaturaSimples                     = "
		                                            SELECT *

		                                            FROM
		                                                tb_evento_faturaglobal_rel_faturasimples

		                                            WHERE
		                                                fk_evf_id = ".$FaturaGlobalId."

		                                            ORDER BY frf_id DESC;
                                            ";
        return $this->db->query($SqlFaturaSimples)->result();


    }






    /**
     *
     * GetInscricoes()
     *
     * Funcao recebe como parametro o ID da fatura simples e
     * retorna todas as inscricoes vinculadas
     *
     * @author Gustavo Botega
     * @param $FaturaSimplesId
     * @return array
     */
    public function GetInscricoes($FaturaSimplesId){


        $SqlInscricoes                     = "
                                                    SELECT *

                                                    FROM
                                                        tb_evento_faturaglobal_rel_faturasimples_rel_inscricao

                                                    WHERE
                                                        fk_frf_id = ".$FaturaSimplesId."

                                                    ORDER BY fk_srp_id DESC;
                                            ";

        return $this->db->query($SqlInscricoes)->result();
    }





    /**
     *
     * GetInscricoesDaSerie()
     *
     * Funcao recebe como parametro o ID da fatura simples e
     * retorna todas as inscricoes vinculadas a serie da Fatura Simples
     *
     * @author Gustavo Botega
     * @param $FaturaSimplesId
     * @return array
     */
    public function GetInscricoesDaSerie($FaturaSimplesId){


        $SqlInscricoesDaSerie                     = "
                                                    SELECT *

                                                    FROM
                                                        tb_evento_faturaglobal_rel_faturasimples_rel_inscricao as fri

                                                    INNER JOIN tb_evento_rel_serie_rel_prova as srp
                                                    ON srp.srp_id = fri.fk_srp_id

                                                    WHERE
                                                        fri.flag_serie IS NOT NULL AND
                                                        fri.fk_frf_id = ".$FaturaSimplesId." AND
                                                        flag_deletado IS NULL

                                                    ORDER BY srp.srp_numero_prova ASC;
                                            ";

        return $this->db->query($SqlInscricoesDaSerie)->result();
    }




    /**
     *
     * GetInscricoesDaProva()
     *
     * Funcao recebe como parametro o ID da fatura simples e
     * retorna todas as inscricoes vinculadas a Fatura Simples que nao fazem parte de Serie
     *
     * @author Gustavo Botega
     * @param $FaturaSimplesId
     * @return array
     */
    public function GetInscricoesDaProva($FaturaSimplesId){


        $SqlInscricoesDaSerie                     = "
                                                    SELECT *

                                                    FROM
                                                        tb_evento_faturaglobal_rel_faturasimples_rel_inscricao as fri

                                                    INNER JOIN tb_evento_rel_serie_rel_prova as srp
                                                    ON srp.srp_id = fri.fk_srp_id

                                                    WHERE
                                                        fri.flag_serie IS NULL AND
                                                        fri.fk_frf_id = ".$FaturaSimplesId." AND
                                                        flag_deletado IS NULL

                                                    ORDER BY srp.srp_numero_prova ASC;
                                            ";

        return $this->db->query($SqlInscricoesDaSerie)->result();
    }



    /**
     *
     * GetBaia()
     *
     * @author Gustavo Botega
     * @param $FaturaSimplesId
     * @return array
     */
    public function GetBaia($FaturaSimplesId){


        $SqlInscricoes                     = "
                                                    SELECT *

                                                    FROM
                                                        tb_evento_faturaglobal_rel_faturasimples_rel_inscricao

                                                    WHERE
                                                        fk_frf_id = ".$FaturaSimplesId."

                                                    ORDER BY fk_srp_id DESC;
                                            ";

        return $this->db->query($SqlInscricoes)->result();
    }


    /**
     *
     * GetQuartoDeSela()
     *
     * @author Gustavo Botega
     * @param $FaturaSimplesId
     * @return array
     */
    public function GetQuartoDeSela($FaturaSimplesId){


        $SqlInscricoes                     = "
                                                    SELECT *

                                                    FROM
                                                        tb_evento_faturaglobal_rel_faturasimples_rel_inscricao

                                                    WHERE
                                                        fk_frf_id = ".$FaturaSimplesId."

                                                    ORDER BY fk_srp_id DESC;
                                            ";

        return $this->db->query($SqlInscricoes)->result();
    }





    /**
    * PackagesClass
    *
    * @author Gustavo Botega
    */
    public function ThemeComponent(){


        /*
            StylesFile
        */
            /*  Plugins*/
            $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
            $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete']                           = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                    = TRUE;
            $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;

            /*  Syles*/
    //            $this->data['StylesFile']['Styles']['profile']                                = TRUE;



        /*
            ScriptsFile
        */
            /*  Plugins*/
            $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
            $this->data['ScriptsFile']['Plugins']['easy-autocomplete']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

            /*  Scripts*/
            $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                            = TRUE;

    }



    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega
    * @return array
    */
    public function SigepeAsset()
    {
        /* Carregando Estilos */
        $this->data['PackageStyles'][]    =   'Packages/Styles/Evento/FrontOffice/FaturaSimples';


        /* Carregando Scripts */
//        $this->data['PackageScripts'][]   =   'packages/frontoffice/script/animal';


    }




}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
