<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';
class Baia extends MY_FrontOffice {

    function __construct() {
        parent::__construct();
		
		$this->ThemeComponent();
		$this->SigepeAsset();

        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');

    }




    public function GetBaias($FaturaSimplesId){
	

        /*  EVENTO */
        $SqlBaias                     = '
                                            SELECT
                                                *
                                            FROM
                                                tb_evento_faturaglobal_rel_faturasimples_rel_baia
                                            WHERE
                                                fk_frf_id = '.$FaturaSimplesId.' AND
                                                flag_deletado IS NULL

                                            LIMIT 1
                                            ';
        return $this->db->query($SqlBaias)->result();        


	


    }


    public function GetFaturaSimples($FaturaGlobalId){
	

        /*  EVENTO */
        $SqlFaturaSimples                     = "
		                                            SELECT *

		                                            FROM
		                                                tb_evento_faturaglobal_rel_faturasimples

		                                            WHERE
		                                                fk_evf_id = ".$FaturaGlobalId."

		                                            ORDER BY frf_id DESC;
                                            ";
        return $this->db->query($SqlFaturaSimples)->result();        


    }






    /**
     *
     * GetInscricoes()
     *
     * Funcao recebe como parametro o ID da fatura simples e
     * retorna todas as inscricoes vinculadas
     *
     * @author Gustavo Botega 
     * @param $FaturaSimplesId    
     * @return array
     */      
    public function GetInscricoes($FaturaSimplesId){
    

        $SqlInscricoes                     = "
                                                    SELECT *

                                                    FROM
                                                        tb_evento_faturaglobal_rel_faturasimples_rel_inscricao

                                                    WHERE
                                                        fk_frf_id = ".$FaturaSimplesId."

                                                    ORDER BY fk_srp_id DESC;
                                            ";

        return $this->db->query($SqlInscricoes)->result();        
    }





    /**
     *
     * GetInscricoesDaSerie()
     *
     * Funcao recebe como parametro o ID da fatura simples e
     * retorna todas as inscricoes vinculadas a serie da Fatura Simples
     *
     * @author Gustavo Botega 
     * @param $FaturaSimplesId    
     * @return array
     */      
    public function GetInscricoesDaSerie($FaturaSimplesId){
    

        $SqlInscricoesDaSerie                     = "
                                                    SELECT *

                                                    FROM
                                                        tb_evento_faturaglobal_rel_faturasimples_rel_inscricao as fri

                                                    INNER JOIN tb_evento_rel_serie_rel_prova as srp
                                                    ON srp.srp_id = fri.fk_srp_id

                                                    WHERE
                                                        fri.flag_serie IS NOT NULL AND 
                                                        fri.fk_frf_id = ".$FaturaSimplesId." AND
                                                        flag_deletado IS NULL

                                                    ORDER BY srp.srp_numero_prova ASC;
                                            ";

        return $this->db->query($SqlInscricoesDaSerie)->result();        
    }




    /**
     *
     * GetInscricoesDaProva()
     *
     * Funcao recebe como parametro o ID da fatura simples e
     * retorna todas as inscricoes vinculadas a Fatura Simples que nao fazem parte de Serie
     *
     * @author Gustavo Botega 
     * @param $FaturaSimplesId    
     * @return array
     */      
    public function GetInscricoesDaProva($FaturaSimplesId){
    

        $SqlInscricoesDaSerie                     = "
                                                    SELECT *

                                                    FROM
                                                        tb_evento_faturaglobal_rel_faturasimples_rel_inscricao as fri

                                                    INNER JOIN tb_evento_rel_serie_rel_prova as srp
                                                    ON srp.srp_id = fri.fk_srp_id

                                                    WHERE
                                                        fri.flag_serie IS NULL AND 
                                                        fri.fk_frf_id = ".$FaturaSimplesId." AND
                                                        flag_deletado IS NULL

                                                    ORDER BY srp.srp_numero_prova ASC;
                                            ";

        return $this->db->query($SqlInscricoesDaSerie)->result();        
    }



    /**
     *
     * GetBaia()
     *
     * @author Gustavo Botega 
     * @param $FaturaSimplesId    
     * @return array
     */      
    public function GetBaia($FaturaSimplesId){
    

        $SqlInscricoes                     = "
                                                    SELECT *

                                                    FROM
                                                        tb_evento_faturaglobal_rel_faturasimples_rel_inscricao

                                                    WHERE
                                                        fk_frf_id = ".$FaturaSimplesId."

                                                    ORDER BY fk_srp_id DESC;
                                            ";

        return $this->db->query($SqlInscricoes)->result();        
    }


    /**
     *
     * GetQuartoDeSela()
     *
     * @author Gustavo Botega 
     * @param $FaturaSimplesId    
     * @return array
     */      
    public function GetQuartoDeSela($FaturaSimplesId){
    

        $SqlInscricoes                     = "
                                                    SELECT *

                                                    FROM
                                                        tb_evento_faturaglobal_rel_faturasimples_rel_inscricao

                                                    WHERE
                                                        fk_frf_id = ".$FaturaSimplesId."

                                                    ORDER BY fk_srp_id DESC;
                                            ";

        return $this->db->query($SqlInscricoes)->result();        
    }





    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    */
    public function ThemeComponent(){ 


        /* 
            StylesFile 
        */
            /*  Plugins*/
            $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
            $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete']                           = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                    = TRUE;
            $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;
        
            /*  Syles*/
    //            $this->data['StylesFile']['Styles']['profile']                                = TRUE;



        /* 
            ScriptsFile 
        */
            /*  Plugins*/
            $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
            $this->data['ScriptsFile']['Plugins']['easy-autocomplete']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

            /*  Scripts*/
            $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                            = TRUE;

    }

	
	
    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function SigepeAsset()
    { 
        /* Carregando Estilos */
//        $this->data['PackageStyles'][]    =   'packages/frontoffice/style/cadastro-animal';


        /* Carregando Scripts */
//        $this->data['PackageScripts'][]   =   'packages/frontoffice/script/animal';


    }

	
	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

