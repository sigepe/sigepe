<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';
class QuartoDeSela extends MY_FrontOffice {

    function __construct() {
        parent::__construct();
		
        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');

    }




    public function GetQuartosDeSela($FaturaSimplesId){
	

        /*  EVENTO */
        $SqlQuartosDeSela               = '
                                            SELECT
                                                *
                                            FROM
                                                tb_evento_faturaglobal_rel_faturasimples_rel_quarto
                                            WHERE
                                                fk_frf_id = '.$FaturaSimplesId.' AND
                                                flag_deletado IS NULL

                                            LIMIT 1
                                            ';
        return $this->db->query($SqlQuartosDeSela)->result();        

    }



}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

