<?php

/**
 * CodeIgniter CRUD Model 2
 * 
 * @package		CodeIgniter
 * @author		Gustavo Botega
 * @copyright	Copyright (c) 2013, Gustavo Botega
 * @link		http://gustavobotega.com
 * 
 */
class Model_crud extends CI_Model {

	/*
	tratar casos quando o valor recebido for nulo, 
	não popular a string $query;
	*/
	/*
	$table - tabela - entrada - string 
	$data - coluna da tabela - array 
	$where - clausula where - array
	$orderby - ordenar
	$limit - limit
	*/
	public function select($table, $data, $where, $orderby, $limit){
	#	var_dump("-");
	#	var_dump("-");

		// Fields
		$fieldsDatatable = '';
		$counter = 1;
		$countArr = count($data);

		if($data[0]=='*')
		{
			$fieldsDatatable	=	'*';
		}
		else{
			foreach ($data as $item) {
				$fieldsDatatable .= $item;
				if($counter!=$countArr){
					$fieldsDatatable .= ', ';
				}
				$counter++;
			}
		}
		$query = "SELECT $fieldsDatatable FROM ";
		
		// Table
		$query .= $table;

		// Where
		if(!is_null($where)){
			if(!empty($where)){
				$query .= ' WHERE ';
				$counterWhere = 1;
				foreach ($where as $key => $whereItem) {
					$query .= $key . ' ' . $whereItem;
					if($counterWhere!=count($where)){
						$query .= ' AND ';
					}
					$counterWhere++;
				}			
			}
		}

		// Orderby
		if(!is_null($orderby)){
			if(!empty($orderby)){
				$query .= ' ORDER BY ';
				$counterOrderby = 1; 
				foreach ($orderby as $orderbyKey => $order) {
					$query .= $orderbyKey . ' ' . strtoupper($order);
					if($counterOrderby!=count($orderby)){
						$query .= ', ';
					}
					$counterOrderby++;
				}			
			}
		}

		// Limit
		if(!is_null($limit)){
			$query .= ' LIMIT ' . $limit;
		}


		// Return Function Query Processed
		return $this->db->query($query)->result();

	}


	public function query($query){
		return $this->db->query($query)->result();
	}



	public function get_rowSpecific($table, $referenceValue, $entryValue, $limit, $tablefield)
	{
		$query = $this->db->get_where($table, array($referenceValue=>$entryValue), $limit);
		$query_rows = $query->row();

#		var_dump($query_rows->$tableField);

		if($query_rows!=NULL)
		{
			return $query_rows->$tablefield;
		}
		else
		{
			return NULL;
		}
	}	

	public function get_rowSpecificObject($table, $referenceValue, $entryValue) {
		$query = $this->db->get_where($table, array($referenceValue=>$entryValue), 1);
		$query_rows = $query->row();
		if($query_rows != NULL) {
			return $query_rows;
		} else {
			return NULL;
		}
	} // end of method

	/**
	 * @ FUNÇÕES - COUNT
	 */
	/*public function count($table, array $values){
		$this->db->from($table);
		foreach ($values as $item):
			$this->db->where($item[0], $item[1]);
		endforeach;
		return $this->db->count_all_results();
	}
*/

	public function count($table, array $item){
		$this->db->from($table);
		$this->db->where($item[0], $item[1]);
		return $this->db->count_all_results();
	}



	public function delete($table, $columnReference, $valueReference){
		$query = $this->db->delete($table, array($columnReference => $valueReference)); 
		if($query){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function update($table, $colunaReferencia, $valorDeComparacao, $data){
		$this->db->where($colunaReferencia, $valorDeComparacao);
		$query = $this->db->update($table, $data); 
		if ($query) {
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function insert($table, $data){
		$this->db->insert($table, $data);
	}



	/**
	 * Returns a formatted var_dump for debugging purposes
	 * you may wish to highlight they are "optional" in their description)
	 * @author Gustavo Botega
	 * @return int|string could be an int, could be a string
	 * @param string $table variable to display with var_dump()
	 * @param string $columnReference string to display
	 * @param int $valueReference,... unlimited OPTIONAL number of additional variables to display with var_dump()
	 */
    function deleteDataAndFile($table, $columnReference, $valueReference, $tableField, $directory){
		$idFile 	= $this->db->get_where($table, array($columnReference=>$valueReference), 1);
		$row 		= $idFile->row();

		$nameFile 	=	NULL;
		if(isset($row->$tableField)){
			$nameFile 	= $row->$tableField;
		}

		/* Primeiro deleta o registro do banco para depois excluir a imagem. */
		$query 		= $this->db->delete($table, array($columnReference => $valueReference)); 

		if($query):
			if(!is_null($nameFile)){
				$currentFolder 	= $directory . $nameFile;
				$trashFolder 	= "trash/" . $nameFile;
				$rename = rename(FCPATH.$currentFolder, FCPATH.$trashFolder);
				if($rename):
	               return TRUE;
				endif;
			}else{
				// nesse momento gravar em um arquivo de .log o erro.
				return TRUE;
			}
		endif;
		
		/* Se a quer nao acontecer eh retornado falso */
		return FALSE;
	}



	public function deleteDataAndFileAndGallery(){}
	public function deleteGallery(){}




// Produces:
// DELETE FROM mytable 
// WHERE id = $id



/*
	GET
	LIMIT
	ORDERBY
	--
	WHERE
	--	
	COUNT
	--
	SPECIFICROW
*/	
}

?>