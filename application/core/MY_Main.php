<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Classe base do sistema. 
 * 
 * @author Rafael Barreto 
 *
 */
class MY_Main extends CI_Controller
{
	function __construct() {
		parent::__construct();
		$this->load->library('inputconstructor');
	}
}