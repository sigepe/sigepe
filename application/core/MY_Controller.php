<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Controlador principal do sistema
 *
 */
class MY_Controller extends MX_Controller
{
	
	/**
	 * Array de dados utilizado nas views 
	 * @var Array
	 */

    public $data;

	function __construct()
	{
		parent::__construct();
		
        $this->data = array();
        
        // Camada de Autenticacao
       // is_auth();

	}





    /**
     * Packages
     *
     * @author Gustavo Botega 
     * @return array
     */
	public function packages()
	{	
		$data = array(
			'use_datepicker'	=>	FALSE,
			'use_dropdown'		=>	FALSE,
			'use_cft'			=>	FALSE,
			'use_toastr'		=>	FALSE,
			'use_datatable'		=>	FALSE,
			'use_fancybox'		=>	FALSE,
			'use_modal'			=>	FALSE,
			'use_slug'			=>	FALSE,
			'use_jfum'			=>	FALSE,
			'use_jmask'			=>	FALSE,
			'use_jcrop'			=>	FALSE,
			'use_jmap'			=>	FALSE,
			'use_portfolio'		=>	FALSE,
		);
		return $data;		
	}
	

}
