<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Classe base do Sistema
 *
 */
include_once APPPATH . '/core/MY_CommonOffice.php';

class MY_FrontOffice extends MY_CommonOffice {

    /**
     * Array de dados utilizado nas views
     * @var Array
     */
    public $data;
    public $settings;

    function __construct() {
        parent::__construct();

        $this->data = array();


        // Camada de Autenticacao
        IsAuthenticated();
    }

    /**
     * LoadTemplateUser e um alias.
     *
     * @author Gustavo Botega
     * @return void
     */
    public function LoadTemplateUser($pathView = NULL, $settings = NULL, $external = array()) {
        $this->LoadTemplate($pathView, $settings, $external);
    }

    /**
     * Mostra a estrutura do template do Usuario
     *
     * @author Gustavo Botega
     * @return void
     */
    public function LoadTemplate($pathView = NULL, $settings = NULL, $external = array()) {

        $data = array();

        $settings['base_url'] = base_url();
        $settings['ShowHeaderNavigation'] = TRUE;
        $settings['ShowTopMenu'] = TRUE;
        $settings['ShowPreFooter'] = FALSE;
        $settings['ShowQuickNav'] = FALSE;



        /*
          $settings['peopleFirstName']    =   $this->session->userdata('peopleFirstName');
          $settings['peopleId']           =   $this->session->userdata('peopleId');
          $settings['userId']             =   $this->session->userdata('userId');
          $settings['peopleThumb']        =   $this->session->userdata('peopleThumb');
         */


        $packages = $this->packages();

        $external['PerfilId'] = $this->session->userdata('PessoaId');
        $external['PrimeiroNome'] = ucfirst(strtolower(FirstWord($this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $external['PerfilId'], 1, 'pes_nome_razao_social'))));
        $external['UltimoNome'] = ucfirst(strtolower(LastWord($this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $external['PerfilId'], 1, 'pes_nome_razao_social'))));


        $data['settings'] = $settings;
        $data['external'] = $external;


        /* Settings variables */
        $merge = array_merge($packages, $settings, $external);


        $this->Template('html-begin', $merge);

        $this->Head($merge);

        $this->Template('body-begin', $merge);

        $this->Template('content-begin', $merge);


//                  $this->PageHead();
//                  $this->Breadcrumbs();
//                  $this->InnerContent();


        $this->Header($merge);

        $this->Environment($merge, $pathView);

        $this->ContentFooter($merge);


        $this->Template('content-end', $merge);

        $this->Footer($merge);

        $this->Template('body-end', $merge);

        $this->Template('html-end', $merge);
    }

    /**
     * LoadTemplateProfileEvento
     *
     * @author Gustavo Botega
     * @return void
     */
    public function LoadTemplateProfileEvento($pathView = NULL, $settings = NULL, $external = array()) {

        $data = array();

        $settings['base_url'] = base_url();
        $settings['ShowHeaderNavigation'] = TRUE;
        $settings['ShowTopMenu'] = TRUE;
        $settings['ShowPreFooter'] = FALSE;
        $settings['ShowQuickNav'] = FALSE;



        /*
          $settings['peopleFirstName']    =   $this->session->userdata('peopleFirstName');
          $settings['peopleId']           =   $this->session->userdata('peopleId');
          $settings['userId']             =   $this->session->userdata('userId');
          $settings['peopleThumb']        =   $this->session->userdata('peopleThumb');
         */

        $external['PerfilId'] = $this->session->userdata('PessoaId');
        $external['PrimeiroNome'] = ucfirst(strtolower(FirstWord($this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $external['PerfilId'], 1, 'pes_nome_razao_social'))));
        $external['UltimoNome'] = ucfirst(strtolower(LastWord($this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $external['PerfilId'], 1, 'pes_nome_razao_social'))));



        $packages = $this->packages();
        $data['settings'] = $settings;
        $data['external'] = $external;



        /* Settings variables */
        $merge = array_merge($packages, $settings, $external);


        $this->Template('html-begin', $merge);

        $this->Head($merge);

        $this->Template('body-begin', $merge);

        $this->Template('content-begin', $merge);


//                  $this->PageHead();
//                  $this->Breadcrumbs();
//                  $this->InnerContent();


        $this->Header($merge);



        $this->parser->parse('Template/FrontOffice/content/environment/begin', $merge);
        $this->parser->parse('Template/FrontOffice/content/environment/page-head/page-head', $merge); // estilos externos
        $this->parser->parse('Template/FrontOffice/content/environment/page-content-body/begin', $merge); // estilos externos
        /* Breadcrumbs */ $this->parser->parse('Template/FrontOffice/content/environment/page-content-body/breadcrumbs/breadcrumbs', $merge); // estilos externos
        $this->parser->parse('Template/FrontOffice/content/environment/page-content-body/content-inner/begin', $merge); // estilos externos

        $this->parser->parse('Template/FrontOffice/content/environment/page-content-body/content-inner/ProfileEvento/Topbar', $merge); // estilos externos
        $this->parser->parse('Template/FrontOffice/content/environment/page-content-body/content-inner/ProfileEvento/Sidebar', $merge); // estilos externos


        if (is_array($pathView)) {
            foreach ($pathView as $view) {
                $this->parser->parse($view, $merge);
            }
        } else {
            $this->parser->parse($pathView, $merge);
        }

        $this->parser->parse('Template/FrontOffice/content/environment/page-content-body/content-inner/end', $merge); // estilos externos
        $this->parser->parse('Template/FrontOffice/content/environment/page-content-body/end', $merge); // estilos externos
        $this->parser->parse('Template/FrontOffice/content/environment/end', $merge);


        $this->ContentFooter($merge);


        $this->Template('content-end', $merge);

        $this->Footer($merge);

        $this->Template('body-end', $merge);

        $this->Template('html-end', $merge);
    }

    /**
     * LoadTemplateInscricao
     *
     * @author Gustavo Botega
     * @return void
     */
    protected function LoadTemplateInscricao($pathView = NULL, $settings = NULL, $external = array()) {

        $data = array();

        $settings['base_url'] = base_url();
        $settings['ShowHeaderNavigation'] = FALSE;
        $settings['ShowTopMenu'] = TRUE;
        $settings['ShowPreFooter'] = TRUE;
        $settings['ShowQuickNav'] = TRUE;
        $settings['ShowPreFooter'] = FALSE;

        $packages = $this->packages();
        $data['settings'] = $settings;
        $data['external'] = $external;


        /* Settings variables */
        $merge = array_merge($packages, $settings, $external);


        $this->Template('html-begin', $merge);

        $this->Head($merge);

        $this->Template('body-begin', $merge);

        $this->Template('content-begin', $merge);

        $this->parser->parse('Template/FrontOffice/content/header/begin', $merge); // bloco de cabecalho
        $this->parser->parse('Template/FrontOffice/content/header/inscricao/header', $merge); // bloco de cabecalho
        $this->parser->parse('Template/FrontOffice/content/header/end', $merge); // bloco de cabecalho


        /* ENVIRONMENT */
        $this->parser->parse('Template/FrontOffice/content/environment/begin', $merge);
        $this->parser->parse('Template/FrontOffice/content/environment/page-content-body/begin', $merge); // estilos externos

        $this->parser->parse('Template/FrontOffice/content/environment/page-content-body/content-inner/begin', $merge); // estilos externos
        if (is_array($pathView)) {
            foreach ($pathView as $view) {
                $this->parser->parse($view, $merge);
            }
        } else {
            $this->parser->parse($pathView, $merge);
        }
        $this->parser->parse('Template/FrontOffice/content/environment/page-content-body/content-inner/end', $merge); // estilos externos
        $this->parser->parse('Template/FrontOffice/content/environment/page-content-body/end', $merge); // estilos externos
        $this->parser->parse('Template/FrontOffice/content/environment/end', $merge);


        $this->ContentFooter($merge);


        $this->Template('content-end', $merge);

        $this->Footer($merge);

        $this->Template('body-end', $merge);

        $this->Template('html-end', $merge);
    }

    /**
     * Mostra a estrutura do template do Usuario
     *
     * @author Gustavo Botega
     * @return void
     */
    protected function LoadTemplateEvento($pathView = NULL, $settings = NULL, $external = array()) {

        $data = array();

        $settings['base_url'] = base_url();
        $settings['ShowHeaderNavigation'] = FALSE;
        $settings['ShowTopMenu'] = TRUE;
        $settings['ShowPreFooter'] = TRUE;
        $settings['ShowQuickNav'] = TRUE;

        /*
          $settings['peopleFirstName']    =   $this->session->userdata('peopleFirstName');
          $settings['peopleId']           =   $this->session->userdata('peopleId');
          $settings['userId']             =   $this->session->userdata('userId');
          $settings['peopleThumb']        =   $this->session->userdata('peopleThumb');
         */


        $packages = $this->packages();
        $data['settings'] = $settings;
        $data['external'] = $external;


        /* Settings variables */
        $merge = array_merge($packages, $settings, $external);


        $this->Template('html-begin', $merge);

        $this->Head($merge);

        $this->Template('body-begin', $merge);

        $this->Template('content-begin', $merge);


//                  $this->PageHead();
//                  $this->Breadcrumbs();
//                  $this->InnerContent();


        $this->Header($merge);








        $this->parser->parse('Template/FrontOffice/content/environment/begin', $merge);
        $this->parser->parse('Template/FrontOffice/content/environment/page-head/page-head', $merge); // estilos externos
        $this->parser->parse('Template/FrontOffice/content/environment/page-content-body/begin', $merge); // estilos externos
        $this->parser->parse('Template/FrontOffice/content/environment/page-content-body/breadcrumbs/breadcrumbs', $merge); // estilos externos
        $this->parser->parse('Template/FrontOffice/content/environment/page-content-body/content-inner/begin', $merge); // estilos externos


        $this->parser->parse('Template/FrontOffice/sistema/Evento/Estrutura/Header', $merge);
        $this->parser->parse('Template/FrontOffice/sistema/Evento/Estrutura/Sidebar', $merge);
        if (is_array($pathView)) {
            foreach ($pathView as $view) {
                $this->parser->parse($view, $merge);
            }
        } else {
            $this->parser->parse($pathView, $merge);
        }
        $this->parser->parse('Template/FrontOffice/sistema/Evento/Estrutura/Footer', $merge);


        $this->parser->parse('Template/FrontOffice/content/environment/page-content-body/content-inner/end', $merge); // estilos externos
        $this->parser->parse('Template/FrontOffice/content/environment/page-content-body/end', $merge); // estilos externos
        $this->parser->parse('Template/FrontOffice/content/environment/end', $merge);















        $this->ContentFooter($merge);


        $this->Template('content-end', $merge);

        $this->Footer($merge);

        $this->Template('body-end', $merge);

        $this->Template('html-end', $merge);
    }

    /**
     * Environment
     *
     * @author Gustavo Botega
     * @return void
     */
    public function Environment($merge, $pathView) {

        /* INICIO */
        $this->parser->parse('Template/FrontOffice/content/environment/begin', $merge);

        /* PAGE-HEAD */
        $this->parser->parse('Template/FrontOffice/content/environment/page-head/page-head', $merge); // estilos externos


        /* PAGE-CONTENT-BODY */
        $this->parser->parse('Template/FrontOffice/content/environment/page-content-body/begin', $merge); // estilos externos

        /* Breadcrumbs */ $this->parser->parse('Template/FrontOffice/content/environment/page-content-body/breadcrumbs/breadcrumbs', $merge); // estilos externos

        /* Content Inner */
        if (!isset($merge['ShowProfileAnimal']) && !isset($merge['ShowProfileEvento']) && !isset($merge['ShowProfilePessoa'])) :
            if (is_array($pathView)) {
                foreach ($pathView as $view) {
                    $this->parser->parse($view, $merge);
                }
            } else {
                $this->parser->parse($pathView, $merge);
            }
        endif;


        /*         * ****************************************** */
        if (isset($merge['ShowProfileAnimal'])):
            $this->parser->parse('Template/FrontOffice/content/environment/page-content-body/content-inner/ProfileAnimal/Sidebar', $merge); // estilos externos
            if (is_array($pathView)) {
                foreach ($pathView as $view) {
                    $this->parser->parse($view, $merge);
                }
            } else {
                $this->parser->parse($pathView, $merge);
            }
            $this->parser->parse('Template/FrontOffice/sistema/Evento/Estrutura/Footer', $merge);
        endif;
        /*         * ****************************************** */
        if (isset($merge['ShowProfilePessoa'])):
            $this->parser->parse('Template/FrontOffice/content/environment/page-content-body/content-inner/ProfilePessoa/Sidebar', $merge); // estilos externos

            if (is_array($pathView)) {
                foreach ($pathView as $view) {
                    $this->parser->parse($view, $merge);
                }
            } else {
                $this->parser->parse($pathView, $merge);
            }
            $this->parser->parse('Template/FrontOffice/sistema/Evento/Estrutura/Footer', $merge);
        endif;
        /*         * ****************************************** */




        $this->parser->parse('Template/FrontOffice/content/environment/page-content-body/end', $merge); // estilos externos


        /* FIM */
        $this->parser->parse('Template/FrontOffice/content/environment/end', $merge);
    }

    /**
     * Head
     *
     * @author Gustavo Botega
     * @return void
     */
    public function Head($merge) {

        /* INICIO HEAD */
        $this->parser->parse('Template/FrontOffice/head/begin', $merge);


        /* METATAGS */
        $this->parser->parse('Template/FrontOffice/head/metatags', $merge);  // metatags da head

        /* MANDATORY / PLUGINS */
        $this->parser->parse('Template/FrontOffice/head/theme/global-mandatory-styles', $merge); // estilos requeridos do tema
        $this->parser->parse('Template/FrontOffice/head/page-level/plugins', $merge); // estilos requeridos do tema

        /* GLOBAL / PLUGINS */
        $this->parser->parse('Template/FrontOffice/head/theme/theme-global-styles', $merge); // estilos externos
        $this->parser->parse('Template/FrontOffice/head/page-level/styles', $merge); // estilos requeridos do tema

        /* THEME LAYOUT STYLES */
        $this->parser->parse('Template/FrontOffice/head/theme/theme-layout-styles', $merge); // estilos requeridos do tema

        /* SIGEPE */
        $this->parser->parse('Template/FrontOffice/head/sigepe/sigepe-global', $merge); // estilos requeridos do tema
        $this->parser->parse('Template/FrontOffice/head/sigepe/sigepe-modules', $merge); // estilos requeridos do tema


        /* FIM HEAD */
        $this->parser->parse('Template/FrontOffice/head/end', $merge);
    }

    /**
     * Header
     *
     * @author Gustavo Botega
     * @return void
     */
    public function Header($merge) {


        /* header */
        $this->parser->parse('Template/FrontOffice/content/header/begin', $merge); // bloco de cabecalho



        $this->HeaderTop($merge);
        ($merge['ShowHeaderNavigation'] == TRUE) ? $this->HeaderNavigation($merge) : '';

        $this->parser->parse('Template/FrontOffice/content/header/end', $merge); // bloco de cabecalho
    }

    /**
     * HeaderTop
     *
     * @author Gustavo Botega
     * @return void
     */
    public function HeaderTop($merge) {


        $this->parser->parse('Template/FrontOffice/content/header/top/begin', $merge); // bloco de cabecalho

        /* logo */
        $this->parser->parse('Template/FrontOffice/content/header/top/logo', $merge); // bloco de cabecalho

        /* topmenu */
        if ($merge['ShowTopMenu']):
            $this->parser->parse('Template/FrontOffice/content/header/top/topmenu/begin', $merge); // bloco de cabecalho
            // $this->parser->parse('Template/FrontOffice/content/header/top/topmenu/avisos', $merge); // bloco de cabecalho
            // $this->parser->parse('Template/FrontOffice/content/header/top/topmenu/eventos', $merge); // bloco de cabecalho
            // $this->parser->parse('Template/FrontOffice/content/header/top/topmenu/helpdesk', $merge); // bloco de cabecalho
            $this->parser->parse('Template/FrontOffice/content/header/top/topmenu/perfil', $merge); // bloco de cabecalho
            $this->parser->parse('Template/FrontOffice/content/header/top/topmenu/administracao', $merge); // bloco de cabecalho
            $this->parser->parse('Template/FrontOffice/content/header/top/topmenu/logout', $merge); // bloco de cabecalho
            $this->parser->parse('Template/FrontOffice/content/header/top/topmenu/end', $merge); // bloco de cabecalho
        endif;

        $this->parser->parse('Template/FrontOffice/content/header/top/end', $merge); // bloco de cabecalho
    }

    /**
     * HeaderNavigation
     *
     * @author Gustavo Botega
     * @return void
     */
    public function HeaderNavigation($merge) {


        /* navigation */
        $this->parser->parse('Template/FrontOffice/content/header/navigation/begin', $merge); // bloco de cabecalho
        $this->parser->parse('Template/FrontOffice/content/header/navigation/navbar/begin', $merge); // bloco de cabecalho
        $this->parser->parse('Template/FrontOffice/content/header/navigation/navbar/inicio', $merge); // bloco de cabecalho
        $this->parser->parse('Template/FrontOffice/content/header/navigation/navbar/meu-perfil', $merge); // bloco de cabecalho
        $this->parser->parse('Template/FrontOffice/content/header/navigation/navbar/calendario', $merge); // bloco de cabecalho
        $this->parser->parse('Template/FrontOffice/content/header/navigation/navbar/inscricao', $merge); // bloco de cabecalho
        $this->parser->parse('Template/FrontOffice/content/header/navigation/navbar/meus-animais', $merge); // bloco de cabecalho
        $this->parser->parse('Template/FrontOffice/content/header/navigation/navbar/empresas', $merge); // bloco de cabecalho
        #    $this->parser->parse('Template/FrontOffice/content/header/navigation/navbar/financeiro', $merge); // bloco de cabecalho
        $this->parser->parse('Template/FrontOffice/content/header/navigation/navbar/duvidas', $merge); // bloco de cabecalho
        //        $this->parser->parse('Template/FrontOffice/content/header/navigation/navbar/suporte', $merge); // bloco de cabecalho
        $this->parser->parse('Template/FrontOffice/content/header/navigation/navbar/chat', $merge); // bloco de cabecalho
        $this->parser->parse('Template/FrontOffice/content/header/navigation/navbar/end', $merge); // bloco de cabecalho
        $this->parser->parse('Template/FrontOffice/content/header/navigation/end', $merge); // bloco de cabecalho
    }

    /**
     * ContentFooter
     *
     * @author Gustavo Botega
     * @return void
     */
    public function ContentFooter($merge) {

        $this->parser->parse('Template/FrontOffice/content/footer/begin', $merge);

        if ($merge['ShowPreFooter'] == TRUE)
            $this->parser->parse('Template/FrontOffice/content/footer/pre-footer', $merge);

        $this->parser->parse('Template/FrontOffice/content/footer/inner-footer', $merge);

        $this->parser->parse('Template/FrontOffice/content/footer/end', $merge);
    }

    /**
     * Footer
     *
     * @author Gustavo Botega
     * @return void
     */
    public function Footer($merge) {

        /* quicknav */
        if ($merge['ShowQuickNav'] == TRUE)
            $this->parser->parse('Template/FrontOffice/footer/quicknav', $merge);

        /* core-plugins */
        $this->parser->parse('Template/FrontOffice/footer/sigepe/environment-variable', $merge);

        /* core-plugins */
        $this->parser->parse('Template/FrontOffice/footer/theme/core-plugins', $merge);
        $this->parser->parse('Template/FrontOffice/footer/page-level/plugins', $merge);

        /* theme-global-scripts */
        $this->parser->parse('Template/FrontOffice/footer/theme/theme-global-scripts', $merge); /* app.min.js */
        $this->parser->parse('Template/FrontOffice/footer/page-level/scripts', $merge);

        /* theme-layouts-scripts */
        $this->parser->parse('Template/FrontOffice/footer/theme/theme-layouts-scripts', $merge);
        $this->parser->parse('Template/FrontOffice/footer/page-level/others', $merge);

        /* sigepe */
        $this->parser->parse('Template/FrontOffice/footer/sigepe/sigepe-global', $merge);
        $this->parser->parse('Template/FrontOffice/footer/sigepe/sigepe-modules', $merge);
    }

    public function Template($type, $merge) {

        switch ($type) {
            case 'html-begin':
                $this->parser->parse('Template/FrontOffice/html-begin', $merge); // abertura tag html
                break;

            case 'body-begin':
                $this->parser->parse('Template/FrontOffice/body-begin', $merge); // abertura tag body
                break;

            case 'content-begin':
                $this->parser->parse('Template/FrontOffice/content/begin', $merge);  // abertura tag page-wrapper
                break;

            case 'content-end':
                $this->parser->parse('Template/FrontOffice/content/end', $merge); // fechamento tag page-wrapper
                break;

            case 'body-end':
                $this->parser->parse('Template/FrontOffice/body-end', $merge);  // abertura tag page-wrapper
                break;

            case 'html-end':
                $this->parser->parse('Template/FrontOffice/end-html', $merge); // fechamento tag html
                break;
        }
    }

    /**
     * Mostra a estrutura do template do Usuario
     *
     * @author Gustavo Botega
     * @return void
     */
    protected function loadBlankPage($pathView = 'template/content', $settings = NULL, $external = array()) {

        $data = array();

        $settings['base_url'] = base_url();

        /* Settings variables */
        $this->parser->parse('Template/FrontOffice/blank-page', $settings);
    }

}
