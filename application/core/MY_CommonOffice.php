<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Classe base do Sistema
 *
 */

class MY_CommonOffice extends MY_Controller
{
	
	/**
	 * Array de dados utilizado nas views 
	 * @var Array
	 */

    public $data;
    public $settings;

	function __construct()
	{
		parent::__construct();
		
        $this->data = array();

//        IsAuthenticated();
//        $this->IsPeopleRegisterCompleted();

	}






    /**
     * Mostra a estrutura do template 
     *
     *  Modulos que usam esse metodo: dashboard
     * 
     * @author Gustavo Botega 
     * @return void
     */
    public function LoadTemplateRegister( $pathView = 'template/content', $settings = NULL, $external = array() )
    {

		$merge          =	array_merge ( $settings, $external );
		$this->LoadParser( $pathView, $merge, 'Register', 'CommonOffice' );
	}






    /**
     * IsPeopleRegisterCompleted 
     *
     * Função verifica se o usuário logado está com o registro completo.
     *
     * Checa na tabela tb_people a coluna flag_registerComplete se possui valor = 1. Se sim o cadastro esta completo, usuario 
     * é marcado com a session: IsPeopleRegisterCompleted como True, fluxo de carregamento do sistema continua. 
     * Se campo estiver NULL ou diferente de 1 usuário é marcado com a session: IsPeopleRegisterCompleted como False e 
     * redirecionado para tela de completar o registro.
     * diferente de 1 usuário  
     * 
     * @author Gustavo Botega 
     * @return Boolean
     */
    public function IsPeopleRegisterCompleted( $pathView = 'template/content', $settings = NULL, $external = array() )
    {
    	/* Declarando de variaveis que serao utilizadas no escopo da funcao */
    	$UserId 						=	$this->session->userdata('UserId');
    	$IsPeopleRegisterCompleted		=	$this->session->userdata('IsPeopleRegisterCompleted');
        $this->load->module('people');
        $CountTelephone 	=	$this->people->CountTelephone( $UserId );

    	/* Usuario ja esta logado, retorna true e o fluxo do sistema continua */
    	if(isset($IsPeopleRegisterCompleted) && $IsPeopleRegisterCompleted)
    		return true;

    	/*
			1 Verificar se usuario esta com registro completo se sim Setar IS PEOPLE REGISTER COMPLETED e retornar true para o fluxo continuar.
			2 Se não checar se email, telefone ou endereco esta vazio pro id. Mandar array com os itens incompletos pra construir
				o formulario. Salvar na session. So trabalhar com Session.
			3 Redirecionar Usuario
    	*/
        // Usuario esta com cadastro COMPLETO
        $FlagRegister   =   $this->model_crud->get_rowSpecific('tb_people', 'peo_id', $this->session->userdata('UserId'), 1, 'flag_registerComplete');
        if(!is_null($FlagRegister) && $FlagRegister == '1'):

	        $DataSession = array( 'IsPeopleRegisterCompleted'	=>	TRUE );
	        $this->session->set_userdata($DataSession); 
	        return TRUE;

        endif;


        // Usuario esta com cadastro INCOMPLETO
        if(is_null($FlagRegister) || empty($FlagRegister)):

	        $DataSession = array( 'IsPeopleRegisterCompleted'	=>	FALSE );
	        $this->session->set_userdata($DataSession); 

	        // Count Qty E-mail
	        $this->load->module('people');
	        $CountTelephone 	=	$this->people->CountTelephone( $UserId );
#	        $CountEmail 		=	$this->people->CountEmail( $UserId );
#	        $CountAddress 		=	$this->people->CountAddress( $UserId );

#            redirect(base_url().'register/RegisterAthlete/RegisterAthleteUpdate/');
	        var_dump("redirecionar!");
        endif;        

	}





}