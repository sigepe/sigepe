<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Data {



    /**
    * CalcularIdade
    *
    * {desc}
    *
    * @author Gustavo Botega 
    * @return {type}
    */
	public function CalcularIdade($DataNascimentoFundacao)
	{
		

		// Checar se a data esta em formato PTBR se nao converter para.
		switch ($this->ChecarFormatoData($DataNascimentoFundacao)) {
			case 'pt-br':
			    list($Dia, $Mes, $Ano) = explode('/', $DataNascimentoFundacao);
				break;

			case 'en':
			    list($Ano, $Mes, $Dia) = explode('/', $DataNascimentoFundacao);
				break;

			case 'iso':
			    list($Ano, $Mes, $Dia ) = explode('-', $DataNascimentoFundacao);
				break;
			
			default:
				return NULL;
				break;
		}


	   
	    // Descobre que dia é hoje e retorna a unix timestamp
	    $Hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));

	    // Descobre a unix timestamp da data de nascimento do fulano
	    $Nascimento = mktime( 0, 0, 0, $Mes, $Dia, $Ano);
	   
	    // Depois apenas fazemos o cálculo já citado :)
	    $Idade = floor((((($Hoje - $Nascimento) / 60) / 60) / 24) / 365.25);

	    return $Idade;

	}




    /**
    * ChecarFormatoData
    *
    * {desc}
    *
    * @author Gustavo Botega 
    * @return {type}
    */
	public function ChecarFormatoData($Data)
	{

		// pt-br	29/07/2017
		// en 		2017/07/29
		// iso 		2017-07-29
		$Formato = NULL;


		if (preg_match('/-/',$Data)):
		    $Formato = 'iso';

		else:

			$ExplodeData 	=	 explode("/", $Data);
			if( strlen($ExplodeData[0]) == 4 ):
				$Formato 	=	'en';
			else:
				$Formato 	=	'pt-br';
			endif;

		endif;


		return $Formato;

	}





	/* 
	GetData
	*/
	public function GetDateTimeIso($Data, $Output, $Json = FALSE)
	{


		if(empty($Data) || is_null($Data) || $Data == "0000-00-00 00:00:00"):
			if($Json)
				echo json_encode('-');

			if(!$Json)
				return '-';
		endif;



		$ExplodeData 	=	explode(' ', $Data);			

		$Date 	=		$ExplodeData[0];
		$Time 	=		$ExplodeData[1];


		$ExplodeDate 	=	explode('-', $Date);
		$ExplodeTime 	=	explode(':', $Time);

		if($Output=='Date'):

			if($Json)
				echo json_encode($ExplodeDate[0] . '-' . $ExplodeDate[1] . '-' . $ExplodeDate[2]);

			if(!$Json)
				return ($ExplodeDate[0] . '-' . $ExplodeDate[1] . '-' . $ExplodeDate[2]);

		endif;

		if($Output=='Time'):

			if($Json)
				echo json_encode($ExplodeTime[0] . ':' . $ExplodeTime[1] . ':' . $ExplodeTime[2]);

			if(!$Json)
				return ($ExplodeTime[0] . ':' . $ExplodeTime[1] . ':' . $ExplodeTime[2]);

		endif;



	}






	/* 
	@ USO
	$this->my_date->datetime('data', 'justDate');
	*/
	public function ConverterData($Data, $TipoDataOriginal, $TipoDataFormatada, $Json = FALSE)
	{

		if(empty($Data) || is_null($Data) || $Data == "0000-00-00 00:00:00" || $Data == '-'):
			if($Json)
				echo json_encode('-');

			if(!$Json)
				return '-';
		endif;



		if (strpos($Data, ':') !== false){
			$DateTime 	=	TRUE;
		}else{
			$DateTime 	=	FALSE;
		}



		// NAO DATETIME
		if(!$DateTime):

			if($TipoDataOriginal=='ISO')
				$ExplodeData 	=	explode('-', $Data);			

			if($TipoDataOriginal=='PT-BR')
				$ExplodeData 	=	explode('/', $Data);			


			/* PT-BR */
			if($TipoDataFormatada=='PT-BR'):

				if($Json)
					echo json_encode($ExplodeData[2] . '/' . $ExplodeData[1] . '/' . $ExplodeData[0]);

				if(!$Json)
					return $ExplodeData[2] . '/' . $ExplodeData[1] . '/' . $ExplodeData[0];

			endif;


			/* ISO */
			if($TipoDataFormatada=='ISO'):

				if($Json)
					echo json_encode($ExplodeData[2] . '-' . $ExplodeData[1] . '-' . $ExplodeData[0]);

				if(!$Json)
					return $ExplodeData[2] . '-' . $ExplodeData[1] . '-' . $ExplodeData[0];

			endif;

		endif;



		// DATETIME
		if($DateTime):


			if($TipoDataOriginal=='ISO'){
				$ExplodeData 	=	explode(' ', $Data);			

				$Date 	=		$ExplodeData[0];
				$Time 	=		$ExplodeData[1];


				$ExplodeDate 	=	explode('-', $Date);
				$ExplodeTime 	=	explode(':', $Time);

			}



			/* PT-BR */
			if($TipoDataFormatada=='PT-BR'):

				if($Json)
					echo json_encode( $ExplodeDate[2] . '/' . $ExplodeDate[1] . '/' . $ExplodeDate[0] . ' ' . $ExplodeTime[0] . ':' . $ExplodeTime[1] . ':' . $ExplodeTime[2] );

				if(!$Json)
					return $ExplodeDate[2] . '/' . $ExplodeDate[1] . '/' . $ExplodeDate[0] . ' ' . $ExplodeTime[0] . ':' . $ExplodeTime[1] . ':' . $ExplodeTime[2];

			endif;




		endif;





	}






	/* 
	@ USO
	$this->my_date->datetime('data', 'justDate');
	*/
	public function datetime($datetime, $action)
	{

		if(empty($datetime) || $datetime == "0000-00-00 00:00:00")
		{
			return "-";
		}
		else{

			$datetime = explode(' ', $datetime);

			$datetime[0]; # DATE
			$date = explode('-', $datetime[0]); # $date[0] = AAAA  /  $date[1] = MM  /  $date[2] = DD  

			if(isset($datetime[1]) && !is_null($datetime[1]))
			{
				$time = explode(':', $datetime[1]); # $time[0] = HH  /  $time[1] = MM  /  $time[2] = SS 
			}

			switch ($action) {
				case 'justDate':
					return $date[2] . '/' . $date[1] . '/' . $date[0];
					break;	

				case 'justTime':
					return $time[0] . ':' . $time[1] . ':' . $time[2];
					break;
				
				case 'justDay':
					return $date[2];
					break;
				
				case 'justDayAndMonth':
					return $date[2] . '/' . $date[1];
					break;
				
				case 'justMonth':
					return $date[1];
					break;
				
				case 'justYear':
					return $date[0];
					break;
				
				case 'defaultDatetime':
					return $date[2] . '/' . $date[1] . '/' . $date[0] . ' ' . $time[0] . ':' . $time[1] . ':' . $time[2];
					break;

				case 'datetimeApart':
					return $date[2] . '/' . $date[1] . '/' . $date[0] . ' às ' . $time[0] . ':' . $time[1] . ':' . $time[2];
					break;

				case 'datetime_untilMinuts':
					return $date[2] . '/' . $date[1] . '/' . $date[0] . ' às ' . $time[0] . ':' . $time[1] ;
					break;

			}

		}



	}



	function diasemana($data)
	{  // Traz o dia da semana para qualquer data informada
		$ano =  substr($data,0,4);
		$mes =  substr($data,5,2);
		$dia =  substr($data,8,2);
		$diasemana = date("w", mktime(0,0,0,$mes,$dia,$ano) );
		switch($diasemana){  
						case"0": $diasemana = "Domingo";	   break;  
						case"1": $diasemana = "Segunda-Feira"; break;  
						case"2": $diasemana = "Terça-Feira";   break;  
						case"3": $diasemana = "Quarta-Feira";  break;  
						case"4": $diasemana = "Quinta-Feira";  break;  
						case"5": $diasemana = "Sexta-Feira";   break;  
						case"6": $diasemana = "Sábado";		break;  
					 }
		return $diasemana;
	}

	function mes_extenso($data)
	{  // Traz o dia da semana para qualquer data informada
		$mes_extenso =  substr($data,5,2);
		switch($mes_extenso){  
						case"01": $mes_extenso = "Janeiro";	   break;  
						case"02": $mes_extenso = "Fevereiro"; break;  
						case"03": $mes_extenso = "Março";   break;  
						case"04": $mes_extenso = "Abril";  break;  
						case"05": $mes_extenso = "Maio";  break;  
						case"06": $mes_extenso = "Junho";   break;  
						case"07": $mes_extenso = "Julho";		break;  
						case"08": $mes_extenso = "Agosto";		break;  
						case"09": $mes_extenso = "Setembro";		break;  
						case"10": $mes_extenso = "Outubro";		break;  
						case"11": $mes_extenso = "Novembro";		break;  
						case"12": $mes_extenso = "Dezembro";		break;  
					 }
		return $mes_extenso;
	}



}



?>