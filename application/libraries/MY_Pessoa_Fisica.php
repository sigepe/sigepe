<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Pessoa_Fisica {




    /* Criar funcao que recebe como parametro ID da Pessoa, ID da Pessoa Fisica ou CPF e o nome do campo que se quer na tb_pessoa_fisica */



    /**
     * Gravar
     *
     *
     * @author Gustavo Botega 
     * @return boolean
     */
    public function Gravar($IdPessoa, $Dados, $Json = FALSE)
    {
        $ci =& get_instance();

        if(isset( $Dados['cpf-isento'] )){
            $FlagCpfIsento         =   '1';
        }else{
            $FlagCpfIsento         =   NULL;
        }


        $Dados       =  array(
            'fk_pes_id'                         =>    $IdPessoa,
            'flag_cpf_isento'                   =>    $FlagCpfIsento,
            'pef_primeiro_nome'                 =>    $this->GetPrimeiroNome($Dados['nome-completo']),
            'pef_sobrenome'                     =>    $this->GetUltimoNome($Dados['nome-completo']),
            'pef_data_nascimento'               =>    $ci->my_data->ConverterData($Dados['data-nascimento'], 'PT-BR', 'ISO'),
            'criado'                            =>    date("Y-m-d H:i:s")
        );




        /* Query */
        $Query = $ci->db->insert('tb_pessoa_fisica', $Dados);
        if($Query)
        {

            $Array = array(
                'Status'            =>  TRUE,
                'datetime'          =>  date("d/m/Y") . " às " . date("H:i:s"),
                'ip'                =>  $ci->input->ip_address(),
                'PessoaFisicaId'    =>  $ci->db->insert_id()
            );
        }
        else
        {
            $Erro[] = '76748987';
        }        

        if(!empty($Erro)){

            if(!$Json)
                return $Array;

            if($Json)
                echo json_encode($Array);
        }

        if($Json){
            echo json_encode( $Array );
        }else{
            return $Array;
        }

    }





    /**
     * GetPrimeiroNome
     *
     *
     * @author Gustavo Botega 
     * @return boolean
     */
    public function GetPrimeiroNome($Nome)
    {
        $Explode    =   explode(" ", $Nome);
        return $Explode[0];
    }


    /**
     * GetUltimoNome
     *
     *
     * @author Gustavo Botega 
     * @return boolean
     */
    public function GetUltimoNome($Nome)
    {
        $Explode    =   explode(" ", $Nome);
        return end($Explode);
    }


    /**
     * CpfIsento
     *
     * Verifica se o flag CPF Isento da tabela tb_pessoa_fisica esta marcado. Se sim retorna true se 
     * nao false.
     *
     * @author Gustavo Botega 
     * @return boolean
     */
	public function CpfIsento($Cpf)
	{

	}



    /**
     * ValidaSexo
     *
     * Verifica se a pessoa fisica esta com o flag de usuario ativo. Se sim retorna true se nao false.
     *
     * @author Gustavo Botega 
     * @return boolean
     */
	public function ValidaSexo($IdSexo)
	{

	}



    /**
     * GerarCpf
     *
     *
     * @author Gustavo Botega 
     * @return boolean
     */
    public static function GerarCpf($mascara = "1") {

        $n1 = rand(0, 9);
        $n2 = rand(0, 9);
        $n3 = rand(0, 9);
        $n4 = rand(0, 9);
        $n5 = rand(0, 9);
        $n6 = rand(0, 9);
        $n7 = rand(0, 9);
        $n8 = rand(0, 9);
//        $n9 = rand(0, 9);
        $n9 = 2; // 2 = Ṕará, Amazonas, Acre, Amapá, Rondônia e Roraima
        $d1 = $n9 * 2 + $n8 * 3 + $n7 * 4 + $n6 * 5 + $n5 * 6 + $n4 * 7 + $n3 * 8 + $n2 * 9 + $n1 * 10;
        $d1 = 11 - (self::mod($d1, 11) );
        if ($d1 >= 10) {
            $d1 = 0;
        }
        $d2 = $d1 * 2 + $n9 * 3 + $n8 * 4 + $n7 * 5 + $n6 * 6 + $n5 * 7 + $n4 * 8 + $n3 * 9 + $n2 * 10 + $n1 * 11;
        $d2 = 11 - (self::mod($d2, 11) );
        if ($d2 >= 10) {
            $d2 = 0;
        }
        $retorno = '';
        if ($mascara == 1) {
            $retorno = '' . $n1 . $n2 . $n3 . "." . $n4 . $n5 . $n6 . "." . $n7 . $n8 . $n9 . "-" . $d1 . $d2;
        } else {
            $retorno = '' . $n1 . $n2 . $n3 . $n4 . $n5 . $n6 . $n7 . $n8 . $n9 . $d1 . $d2;
        }
        return $retorno;

    }


    /**
     * @param type $dividendo
     * @param type $divisor
     * @return type
     */
    private static function mod($dividendo, $divisor) {
        return round($dividendo - (floor($dividendo / $divisor) * $divisor));
    }







}



?>