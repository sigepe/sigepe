<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_Vinculo {




    public function Gravar( $Dados = NULL, $Json = NULL ) {

        $ci =& get_instance();

      if(is_null($Dados))
        return NULL;


      /* Dados */
      $ArrayDados       =  array(
          'fk_aut_id'                         =>    $Dados['fk_aut_id'],
          'fk_pes1_id'                        =>    $Dados['fk_pes1_id'],
          'fk_pes2_id'                        =>    $Dados['fk_pes2_id'],
          'fk_ani_id'                         =>    $Dados['fk_ani_id'],
          'fk_per_id'                         =>    $Dados['fk_per_id'],
          'fk_tip_id'                         =>    $Dados['fk_tip_id'],
          'fk_sta_id'                         =>    $Dados['fk_sta_id'],
          'criado'                            =>    date("Y-m-d H:i:s")
      );


      /* Query */
      $Query = $ci->db->insert('tb_vinculo', $ArrayDados);

      if($Query){

        $this->GravarHistorico($ci->db->insert_id(), $Dados);

      }else{

      }


    }

    public function GravarHistorico($IdVinculo = NULL, $Dados) {
        $ci =& get_instance();

          if(!isset($Dados['vih_historico']))
              $Dados['vih_historico']   = '';

            
          /* Dados */
          $ArrayDados       =  array(
              'fk_aut_id'                         =>    $Dados['fk_aut_id'],
              'fk_vin_id'                         =>    $IdVinculo,
              'fk_sta_id'                         =>    $Dados['fk_sta_id'],
              'vih_historico'                     =>    $Dados['vih_historico'],
              'criado'                            =>    date("Y-m-d H:i:s")
          );


          /* Query */
          $Query = $ci->db->insert('tb_vinculo_historico', $ArrayDados);

    }










}



?>