<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Pessoa_Fisica_Atleta {




    /* Criar funcao que recebe como parametro ID da Pessoa, ID da Pessoa Fisica ou CPF e o nome do campo que se quer na tb_pessoa_fisica */



    /**
     * Gravar
     *
     *
     * @author Gustavo Botega 
     * @return boolean
     */
    public function Gravar($IdPessoa, $IdPessoaFisica, $Dados, $Json = FALSE)
    {
        $ci =& get_instance();


        $Dados       =  array(
            'flag_registro'                     =>    NULL,
            'fk_pef_id'                         =>    $IdPessoaFisica,
            'fk_aut_id'                         =>    $IdPessoa,
            'pfa_nome_competicao'               =>    $Dados['nome-competicao'],
            'pfa_matricula_cbh'                 =>    $Dados['registro-cbh'],
            'pfa_matricula_fei'                 =>    $Dados['registro-fei'],
            'criado'                            =>    date("Y-m-d H:i:s")
        );


        /* Query */
        $Query = $ci->db->insert('tb_pessoa_fisica_atleta', $Dados);
        if($Query)
        {

            $Array = array(
                'Status'                =>  TRUE,
                'datetime'              =>  date("d/m/Y") . " às " . date("H:i:s"),
                'ip'                    =>  $ci->input->ip_address(),
                'PessoaFisicaAtletaId'  =>  $ci->db->insert_id()
            );

            $IdAtleta   =   $ci->db->insert_id();


            // Gravar Perfil Atleta
            $this->GravarPerfilAtleta($IdPessoa, $Dados);
/*
            // Confederacao
            $this->GravarVinculoAtletaEmpresa($IdPessoa, $IdPessoaFisica, $Dados, $IdAtleta,  2);

            // Federacao
            $this->GravarVinculoAtletaEmpresa($IdPessoa, $IdPessoaFisica, $Dados, $IdAtleta, 3);

            // Se tiver entidade/escola gravar. 
            $this->GravarVinculoAtletaEmpresa($IdPessoa, $IdPessoaFisica, $Dados, $IdAtleta, 4);
*/
        }
        else
        {
            $Erro[] = '74472147';
        }        

        if(!empty($Erro)){

            if(!$Json)
                return $Array;

            if($Json)
                echo json_encode($Array);
        }

        if($Json){
            echo json_encode( $Array );
        }else{
            return $Array;
        }

    }





}



?>