<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Pessoa {


    /**
     * PessoaAtiva
     *
     * @author Gustavo Botega 
     * @return boolean
     */
	public function PessoaAtiva($Id)
	{

	}


    /**
     * StatusDaPessoa
     *
     * @author Gustavo Botega 
     * @return integer
     */
	public function StatusDaPessoa($Id)
	{

	}



    /**
     * Identifica se é CPF ou CNPJ
     * 
     * Se for CPF tem 11 caracteres, CNPJ tem 14
     * 
     * @access protected
     * @return string CPF, CNPJ ou false
     */
    protected function IdentificarCpfCnpj ( $CpfCnpj, $Json = FALSE ) {

        $Identificador = FALSE;


        // Com pontuacao
        if(!ctype_digit($CpfCnpj)):

            if ( strlen( $CpfCnpj ) === 14 ) {
                $Identificador ='CPF';
            } 
            // Verifica CNPJ
            elseif ( strlen( $CpfCnpj ) === 18 ) {
                $Identificador = 'CNPJ';
            } 
            // Não retorna nada
            else {
                $Identificador = FALSE;
            }

        endif;


        // Sem pontuacao
        if(ctype_digit($CpfCnpj)):

            if ( strlen( $CpfCnpj ) === 11 ) {
                $Identificador = 'CPF';
            } 
            // Verifica CNPJ
            elseif ( strlen( $CpfCnpj ) === 14 ) {
                $Identificador = 'CNPJ';
            } 
            // Não retorna nada
            else {
                $Identificador = FALSE;
            }

        endif;


        if($Json)
            echo json_encode($Identificador);

        if(!$Json)
            return $Identificador;

    }




    /*
     * CpfCnpjExiste
     * - Verifica na coluna peo_cpf da tabela tb_people se existe alguma ocorrencia com o valor submetido. 
     * - A funcao e chamada por uma requisicao ajax tipo post. Nao recebe parametros na funcao.
     * - Se ja existir algum CPF a funcao retorna TRUE se nao FALSE
     * @author Gustavo Botega 
     * @param     
     * @return boolean  retorna TRUE se o CPF existir
    */
    public function CpfCnpjExiste($CpfCnpj = FALSE, $Json = FALSE) {

        if(!$CpfCnpj)
            $CpfCnpj    =   $_POST['CpfCnpj'];

        $CpfCnpj    =   $this->RemoverPontuacaoCpfCnpj($CpfCnpj);

        $ci     =   get_instance();
        $Query = $ci->model_crud->select(
                    'tb_pessoa',
                    array( '*'),
                    array('pes_cpf_cnpj =' => $CpfCnpj),
                    array(),
                    1
                );

        if( is_null($Query) || empty($Query) ) {
            $RetornoDeDados     =   false;            
        }else{
            $RetornoDeDados     =   true;            
        }

        if($Json)
            echo json_encode($RetornoDeDados); 

        if(!$Json)
            return $RetornoDeDados;

    }


    /**
     * ValidarCpfCnpj
     *
     * @author Gustavo Botega 
     * @return void
     */
	public function ValidarCpfCnpj($CpfCnpj)
    {


		if(!ctype_digit($CpfCnpj))
			$CpfCnpj = $this->FormatarCpfCnpj($CpfCnpj); // apenas numeros


		switch (strlen($CpfCnpj)) {
			case '11': // CPF
				$CpfCnpjValido 	=	$this->ValidarCpf();
				break;
			
			case '14': // CNPJ
				$CpfCnpjValido 	=	$this->ValidarCnpj();
				break;
			
		}

		return ($CpfCnpjValido) ? true : false;

	}












    /**
     * ValidarCPF
     *
     * @author                Luiz Otávio Miranda <contato@tutsup.com>
     * @access protected
     * @param  string    $cpf O CPF com ou sem pontos e traço
     * @return bool           True para CPF correto - False para CPF incorreto
     */
    public function ValidarCpf($Cpf, $Json = FALSE) {
   
        // Remover pontuacao aqui
        if(!ctype_digit($Cpf))
            $Cpf = $this->RemoverPontuacaoCpfCnpj($Cpf);


        // Captura os 9 primeiros dígitos do CPF
        // Ex.: 02546288423 = 025462884
        $digitos = substr($Cpf, 0, 9);


        // Faz o cálculo dos 9 primeiros dígitos do CPF para obter o primeiro dígito
        $novo_cpf = $this->CalcularDigitosPosicoes( $digitos );


        // Faz o cálculo dos 10 dígitos do CPF para obter o último dígito
        $novo_cpf = $this->CalcularDigitosPosicoes( $novo_cpf, 11 );
        

        // Verifica se todos os números são iguais
        if ( $this->VerificarIgualdade($Cpf) ) {
            return false;
        }


        // Verifica se o novo CPF gerado é idêntico ao CPF enviado
        if ( $novo_cpf === $Cpf ) {
            // CPF válido
            $Resultado = true;
        } else {
            // CPF inválido
            $Resultado = false;
        }

        if($Json)
            echo json_encode($Resultado);

        if(!$Json)
            return $Resultado;

    }



    /**
     * ValidarCnpj
     *
     * @author Gustavo Botega 
     * @return void
     */
	public function ValidarCnpj($datetime, $JsonEncode = FALSE)
	{

	}






    /**
     * Recebe o CPF/CNPJ como parametro. CPF/CNPJ se vier com pontuacao e retirado e vier sem 
     * pontuacao
     * e inserido a pontuacao.
     *
     * @access public
     * @return string  CPF ou CNPJ formatado
     */
    public function FormatarCpfCnpj($CpfCnpj) {
        // O valor formatado
        $formatado = false;

        // Valida CPF
        if ( $this->IdentificarCpfCnpj($CpfCnpj) === 'CPF' ) {
            // Verifica se o CPF é válido
            if ( $this->ValidarCpf($CpfCnpj) ) {
                // Formata o CPF ###.###.###-##
                $formatado  = substr( $CpfCnpj, 0, 3 ) . '.';
                $formatado .= substr( $CpfCnpj, 3, 3 ) . '.';
                $formatado .= substr( $CpfCnpj, 6, 3 ) . '-';
                $formatado .= substr( $CpfCnpj, 9, 2 ) . '';
            }
        } 
        // Valida CNPJ
        elseif ( $this->IdentificarCpfCnpj($CpfCnpj) === 'CNPJ' ) {
            // Verifica se o CPF é válido
            if ( $this->ValidarCpf($CpfCnpj) ) {
                // Formata o CNPJ ##.###.###/####-##
                $formatado  = substr( $CpfCnpj,  0,  2 ) . '.';
                $formatado .= substr( $CpfCnpj,  2,  3 ) . '.';
                $formatado .= substr( $CpfCnpj,  5,  3 ) . '/';
                $formatado .= substr( $CpfCnpj,  8,  4 ) . '-';
                $formatado .= substr( $CpfCnpj, 12, 14 ) . '';
            }
        } 

        // Retorna o valor 
        return $formatado;
    }






    /**
     * FormatarCpf
     *
     * Recebe o CPF como parametro. CPF se vier com pontuacao e retirado e vier sem pontuacao
     * e inserido a pontuacao.
     *
     * @author Gustavo Botega 
     * @return string
     */
	public function FormatarCpf($Cpf)
	{

	}


    /**
     * FormatarCnpj
     *
     *
     * Recebe o CNPJ como parametro. CNPJ se vier com pontuacao e retirado e vier sem pontuacao
     * e inserido a pontuacao.
     *
     * @author Gustavo Botega 
     * @return string
     */
    public function FormatarCnpj($Cnpj)
    {

    }


    


    /**
     * InserirPontuacaoCpfCnpj
     *
     *
     * Recebe o CNPJ como parametro. CNPJ se vier com pontuacao e retirado e vier sem pontuacao
     * e inserido a pontuacao.
     *
     * @author Gustavo Botega 
     * @return string
     */
    public function InserirPontuacaoCpfCnpj($CpfCnpj, $Json = FALSE)
    {  

        $CpfCnpjFormatado  =   '';

        $CpfCnpj = (string)$CpfCnpj;

        switch ($this->IdentificarCpfCnpj($CpfCnpj)) {
            case 'CPF':
                $CpfCnpjFormatado  = substr( $CpfCnpj, 0, 3 ) . '.';
                $CpfCnpjFormatado .= substr( $CpfCnpj, 3, 3 ) . '.';
                $CpfCnpjFormatado .= substr( $CpfCnpj, 6, 3 ) . '-';
                $CpfCnpjFormatado .= substr( $CpfCnpj, 9, 2 ) . '';
                break;
            
            case 'CNPJ':
                $CpfCnpjFormatado  = substr( $CpfCnpj,  0,  2 ) . '.';
                $CpfCnpjFormatado .= substr( $CpfCnpj,  2,  3 ) . '.';
                $CpfCnpjFormatado .= substr( $CpfCnpj,  5,  3 ) . '/';
                $CpfCnpjFormatado .= substr( $CpfCnpj,  8,  4 ) . '-';
                $CpfCnpjFormatado .= substr( $CpfCnpj, 12, 14 ) . '';
                break;
        }


        if($Json)
            echo json_encode($CpfCnpjFormatado);

        if(!$Json)
            return $CpfCnpjFormatado;

    }



    /**
     * RemoverPontuacaoCpfCnpj
     *
     *
     * Recebe o CNPJ como parametro. CNPJ se vier com pontuacao e retirado e vier sem pontuacao
     * e inserido a pontuacao.
     *
     * @author Gustavo Botega 
     * @return string
     */
	public function RemoverPontuacaoCpfCnpj($CpfCnpj, $Json = FALSE)
	{  

        $CpfCnpj = (string)$CpfCnpj;

        $CpfCnpj = str_replace(".", "", $CpfCnpj);
        $CpfCnpj = str_replace("-", "", $CpfCnpj);
        $CpfCnpj = str_replace("/", "", $CpfCnpj);

        if($Json)
            echo json_encode($CpfCnpj);

        if(!$Json)
            return $CpfCnpj;

	}



    /**
     * Multiplica dígitos vezes posições
     *
     * @access protected
     * @param  string    $digitos      Os digitos desejados
     * @param  int       $posicoes     A posição que vai iniciar a regressão
     * @param  int       $soma_digitos A soma das multiplicações entre posições e dígitos
     * @return int                     Os dígitos enviados concatenados com o último dígito
     */
    protected function CalcularDigitosPosicoes( $digitos, $posicoes = 10, $soma_digitos = 0 ) {
      
        // Faz a soma dos dígitos com a posição
        // Ex. para 10 posições:
        //   0    2    5    4    6    2    8    8   4
        // x10   x9   x8   x7   x6   x5   x4   x3  x2
        //   0 + 18 + 40 + 28 + 36 + 10 + 32 + 24 + 8 = 196
        for ( $i = 0; $i < strlen( $digitos ); $i++  ) {
            // Preenche a soma com o dígito vezes a posição
            $soma_digitos = $soma_digitos + ( $digitos[$i] * $posicoes );

            // Subtrai 1 da posição
            $posicoes--;

            // Parte específica para CNPJ
            // Ex.: 5-4-3-2-9-8-7-6-5-4-3-2
            if ( $posicoes < 2 ) {
                // Retorno a posição para 9
                $posicoes = 9;
            }
        }

        // Captura o resto da divisão entre $soma_digitos dividido por 11
        // Ex.: 196 % 11 = 9
        $soma_digitos = $soma_digitos % 11;

        // Verifica se $soma_digitos é menor que 2
        if ( $soma_digitos < 2 ) {
            // $soma_digitos agora será zero
            $soma_digitos = 0;
        } else {
            // Se for maior que 2, o resultado é 11 menos $soma_digitos
            // Ex.: 11 - 9 = 2
            // Nosso dígito procurado é 2
            $soma_digitos = 11 - $soma_digitos;
        }

        // Concatena mais um dígito aos primeiro nove dígitos
        // Ex.: 025462884 + 2 = 0254628842
        $cpf = $digitos . $soma_digitos;

        // Retorna
        return $cpf;
    }




    /**
     * Verifica se todos os números são iguais
     *   * 
     * @access protected
     * @return bool true para todos iguais, false para números que podem ser válidos
     */
    protected function VerificarIgualdade($CpfCnpj) {

        // Todos os caracteres em um array
        $caracteres = str_split( $CpfCnpj );
        
        // Considera que todos os números são iguais
        $todos_iguais = true;
        
        // Primeiro caractere
        $last_val = $caracteres[0];
        
        // Verifica todos os caracteres para detectar diferença
        foreach( $caracteres as $val ) {
            
            // Se o último valor for diferente do anterior, já temos
            // um número diferente no CPF ou CNPJ
            if ( $last_val != $val ) {
               $todos_iguais = false; 
            }
            
            // Grava o último número checado
            $last_val = $val;
        }
        
        // Retorna true para todos os números iguais
        // ou falso para todos os números diferentes
        return $todos_iguais;
    }





    /**
     * ValidarNome
     *
     * @author Gustavo Botega 
     * @return void
     */
    public function ValidarNome($Nome, $Json = FALSE)
    {

        $Erro       =   array();

        $Explode    =   explode(" ", $Nome);
        count($Explode);

        if($Explode < 2)
            $Erro[] =   '93647989';

        if(strlen($Nome) < 10)
            $Erro[] =   '56880307';


        if($Json)
            echo (!empty($Erro)) ? json_encode($Erro) : json_encode(true);


        if(!$Json && empty($Erro))
            return true;

    }









}



?>