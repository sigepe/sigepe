<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Pessoa_Email {


    /**
     * Gravar
     *
     * @author Gustavo Botega 
     * @return mixed
     */
    public function Gravar($IdPessoa, $Dados, $Json = FALSE)
    {

        $IdAutor    =   NULL;

        $ArrayInsert        =  array(
            'fk_aut_id'                         =>    $IdPessoa,
            'fk_peo_id'                         =>    $IdPessoa,
            'fk_con_id'                         =>    $Dados['email-tipo'],
            'fk_sta_id'                         =>    1,
            'ema_email'                         =>    $Dados['email'],
            'ema_principal'                     =>    $Dados['email-principal'],
            'criado'                            =>    date("Y-m-d H:i:s")
        );


        /* Query */
        $ci =& get_instance();
        $Query = $ci->db->insert('tb_email', $ArrayInsert);
        if($Query)
        {

            $Array = array(
                'Status'    =>  TRUE,
                'datetime'  =>  date("d/m/Y") . " às " . date("H:i:s"),
                'ip'        =>  $ci->input->ip_address(),
                'EmailId'  =>  $ci->db->insert_id()
            );

            if(!$Json)
                return $Array;

            if($Json)
               echo json_encode( $Array );

        }else{
            $Erro[]     =      '37584576';
        }

    }




    /**
     * ValidarEmail
     *
     * @author Gustavo Botega 
     * @return mixed
     */
	public function ValidarEmail($Email, $Json = FALSE)
	{
        $Erro   =   array();


	}







}



?>