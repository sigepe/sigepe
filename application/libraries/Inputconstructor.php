<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
	A classe InputConstruct e responsavel por tratar os dados recebidos e retornar em um array de dados com as configuracoes input. 

	Enquanto a classe inputconstructor monta os array para serem enviadas para a view
*/

class Inputconstructor {


    /***********************************************    
    TEXT
    ***********************************************/
    public function text(array $data){

        /* Display */ (is_null($data[0][8]) || empty($data[0][8])) ? $data[0][8] = TRUE : '';
        /* Icon */ (empty($data[1][1])) ? $data[1][1] = NULL : '';
        /* Helper */ (empty($data[0][2])) ? $data[0][2] = NULL : '';
        /* Style */ (empty($data[0][6])) ? $data[0][6] = NULL : '';
        /* Validation Frontend */ (empty($data[0][10])) ? $data[0][10] = NULL : '';
        /* Validation Backend */ (empty($data[0][11])) ? $data[0][11] = NULL : '';

        $array  = array(
            /* Common Features */
            array(
                'input-type'                  =>   'text',
                'bootstrap-grid'              =>   $data[0][0],
                'input-label'                 =>   $data[0][1],
                'input-helper'                =>   $data[0][2],
                'input-value'                 =>   $data[0][3],
                'input-id'                    =>   $data[0][4],
                'input-class'                 =>   $data[0][5],
                'input-style'                 =>   $data[0][6],
                'input-attributes'            =>   $data[0][7],
                'input-display'               =>   $data[0][8],
                'input-disabled'              =>   $data[0][9],
                'validation-frontend'         =>   $data[0][10],
                'validation-backend'          =>   $data[0][11]
            ),
            /* Specif Input */
            array(
                'input-placeholder'           =>   $data[1][0],
                'input-icon'                  =>   $data[1][1],
                'input-size'                  =>   $data[1][2]
            ),
            /* Update */
            array(
                'input-display'               =>   $data[2][0],
                'input-disabled'              =>   $data[2][1]
            ),
            /* Database */
            array(
                'database-column'             =>   $data[3][0],
                'database-value'              =>   $data[3][1]
            )
        );


        /* Rename Arrays */
        $array['common']   =   $array[0];
        unset($array[0]);

        $array['specif']   =   $array[1];
        unset($array[1]);

        $array['update']   =   $array[2];
        unset($array[2]);

        $array['database']   =   $array[3];
        unset($array[3]);


    return $array;
    }


    /***********************************************    
    TEXT
    ***********************************************/
    public function filemanager(array $data){

        /* Display */ (is_null($data[0][8]) || empty($data[0][8])) ? $data[0][8] = TRUE : '';
        /* Icon */ (empty($data[1][1])) ? $data[1][1] = NULL : '';
        /* Helper */ (empty($data[0][2])) ? $data[0][2] = NULL : '';
        /* Style */ (empty($data[0][6])) ? $data[0][6] = NULL : '';
        /* Validation Frontend */ (empty($data[0][10])) ? $data[0][10] = NULL : '';
        /* Validation Backend */ (empty($data[0][11])) ? $data[0][11] = NULL : '';

        $array  = array(
            /* Common Features */
            array(
                'input-type'                  =>   'filemanager',
                'bootstrap-grid'              =>   $data[0][0],
                'input-label'                 =>   $data[0][1],
                'input-helper'                =>   $data[0][2],
                'input-value'                 =>   $data[0][3],
                'input-id'                    =>   $data[0][4],
                'input-class'                 =>   $data[0][5],
                'input-style'                 =>   $data[0][6],
                'input-attributes'            =>   $data[0][7],
                'input-display'               =>   $data[0][8],
                'input-disabled'              =>   $data[0][9],
                'validation-frontend'         =>   $data[0][10],
                'validation-backend'          =>   $data[0][11]
            ),
            /* Specif Input */
            array(
                'input-placeholder'           =>   $data[1][0],
                'input-icon'                  =>   $data[1][1],
                'input-size'                  =>   $data[1][2]
            ),
            /* Update */
            array(
                'input-display'               =>   $data[2][0],
                'input-disabled'              =>   $data[2][1]
            ),
            /* Database */
            array(
                'database-column'             =>   $data[3][0],
                'database-value'              =>   $data[3][1]
            )
        );


        /* Rename Arrays */
        $array['common']   =   $array[0];
        unset($array[0]);

        $array['specif']   =   $array[1];
        unset($array[1]);

        $array['update']   =   $array[2];
        unset($array[2]);

        $array['database']   =   $array[3];
        unset($array[3]);


    return $array;
    }










    /***********************************************    
    MAP
    ***********************************************/
    public function map(array $data){

        /* Display */ (is_null($data[0][8]) || empty($data[0][8])) ? $data[0][8] = FALSE : '';
        /* Icon */ (empty($data[1][1])) ? $data[1][1] = NULL : '';
        /* Helper */ (empty($data[0][2])) ? $data[0][2] = NULL : '';
        /* Style */ (empty($data[0][6])) ? $data[0][6] = NULL : '';
        /* Validation Frontend */ (empty($data[0][10])) ? $data[0][10] = NULL : '';
        /* Validation Backend */ (empty($data[0][11])) ? $data[0][11] = NULL : '';

        $array  = array(
            /* Common Features */
            array(
                'input-type'                  =>   'map',
                'bootstrap-grid'              =>   $data[0][0],
                'input-label'                 =>   $data[0][1],
                'input-helper'                =>   $data[0][2],
                'input-value'                 =>   $data[0][3],
                'input-id'                    =>   $data[0][4],
                'input-class'                 =>   $data[0][5],
                'input-style'                 =>   $data[0][6],
                'input-attributes'            =>   $data[0][7],
                'input-display'               =>   $data[0][8],
                'input-disabled'              =>   $data[0][9],
                'validation-frontend'         =>   $data[0][10],
                'validation-backend'          =>   $data[0][11]
            ),
            /* Specif Input */
            array(
                'input-placeholder'           =>   $data[1][0],
                'input-icon'                  =>   $data[1][1],
                'input-size'                  =>   $data[1][2]
            ),
            /* Update */
            array(
                'input-display'               =>   $data[2][0],
                'input-disabled'              =>   $data[2][1]
            ),
            /* Database */
            array(
                'database-column'             =>   $data[3][0],
                'database-value'              =>   $data[3][1]
            )
        );


        /* Rename Arrays */
        $array['common']   =   $array[0];
        unset($array[0]);

        $array['specif']   =   $array[1];
        unset($array[1]);

        $array['update']   =   $array[2];
        unset($array[2]);

        $array['database']   =   $array[3];
        unset($array[3]);


    return $array;
    }


  /***********************************************  
    BTN
  ***********************************************/
  public function btn(array $data){
        $array                  =       array(
            'input-type'        =>   'btn',
            'input-label'       =>   $data[0],
            'input-size'        =>   $data[1],
            'input-block'       =>   $data[2],
            'input-color'       =>   $data[3],
            'input-icon'        =>   $data[4],
            'input-disabled'    =>   $data[5],
            'input-id'          =>   $data[6],
            'input-class'       =>   $data[7],
            'input-link'        =>   $data[8],
        );
        return $array;
  }





    /***********************************************  
      SELECT
    ***********************************************/
    public function select(array $data){

        /* Helper */ (is_null($data[0][2]) || empty($data[0][2])) ? $data[0][2] = 'Slug' : '';

        $array  = array(
            /* Common Features */
            array(
                'input-type'                  =>   'select',
                'bootstrap-grid'              =>   $data[0][0],
                'input-label'                 =>   $data[0][1],
                'input-helper'                =>   $data[0][2],
                'input-value'                 =>   $data[0][3],
                'input-id'                    =>   $data[0][4],
                'input-class'                 =>   $data[0][5],
                'input-style'                 =>   $data[0][6],
                'input-attributes'            =>   $data[0][7],
                'input-display'               =>   $data[0][8],
                'input-disabled'              =>   $data[0][9],
                'validation-frontend'         =>   $data[0][10],
                'validation-backend'          =>   $data[0][11]
            ),
            /* Specif Input */
            array(
                'input-optionInstruction'     =>   $data[1][0],
                'input-table'                 =>   $data[1][1],
                'input-columns'               =>   $data[1][2],
                'input-where'                 =>   $data[1][3],
                'input-orderby'               =>   $data[1][4],
                'input-index'                 =>   $data[1][5],
                'input-value'                 =>   $data[1][6]
            ),
            /* Update */
            array(
                'input-display'               =>   $data[2][0],
                'input-disabled'              =>   $data[2][1]
            ),
            /* Database */
            array(
                'database-column'             =>   $data[3][0],
                'database-value'              =>   $data[3][1]
            )
        );


        /* Rename Arrays */
        $array['common']   =   $array[0];
        unset($array[0]);

        $array['specif']   =   $array[1];
        unset($array[1]);

        $array['update']   =   $array[2];
        unset($array[2]);

        $array['database']   =   $array[3];
        unset($array[3]);

    return $array;
    }      



    /***********************************************  
      SELECT
    ***********************************************/
    public function selectDependence(array $data){


        /* First select */
        if($data[1][0]==FALSE){ // first select
            $data[1][2][0] = NULL; // select-optionInstruction
            $data[1][2][1] = NULL; // select-table
            $data[1][2][2] = NULL; // select-columns
            $data[1][2][3] = NULL; // select-where
            $data[1][2][4] = NULL; // select-orderby
            $data[1][2][5] = NULL; // select-index
            $data[1][2][6] = NULL; // select-value
        }

        /* Have dependencies */
        if($data[1][1]==FALSE){ // have dependencies
            $data[1][3][0] = NULL; // selectDependence-Identification
            $data[1][3][1] = NULL; // selectDependence-optionInstruction
            $data[1][3][2] = NULL; // selectDependence-Table
            $data[1][3][3] = NULL; // selectDependence-Columns
            $data[1][3][4] = NULL; // selectDependence-Where
            $data[1][3][5] = NULL; // selectDependence-Orderby
            $data[1][3][6] = NULL; // selectDependence-Index
            $data[1][3][7] = NULL; // selectDependence-Value
            $data[1][4]    = NULL; // arrays of dependencies 
        }

        $array  = array(
            /* Common Features */
            array(
                'input-type'                                =>   'selectDependence',
                'bootstrap-grid'                            =>   $data[0][0],
                'input-label'                               =>   $data[0][1],
                'input-helper'                              =>   $data[0][2],
                'input-value'                               =>   $data[0][3],
                'input-id'                                  =>   $data[0][4],
                'input-class'                               =>   $data[0][5],
                'input-style'                               =>   $data[0][6],
                'input-attributes'                          =>   $data[0][7],
                'input-display'                             =>   $data[0][8],
                'input-disabled'                            =>   $data[0][9],
                'validation-frontend'                       =>   $data[0][10],
                'validation-backend'                        =>   $data[0][11]
            ),
            /* Specif Input */
            array(

                /* Flags */      
                'select-firstSelect'                        =>   $data[1][0],
                'select-haveDependencies'                   =>   $data[1][1],

                /* First Select */      
                'select-optionInstruction'                  =>   $data[1][2][0],
                'select-table'                              =>   $data[1][2][1],
                'select-columns'                            =>   $data[1][2][2],
                'select-where'                              =>   $data[1][2][3],
                'select-orderby'                            =>   $data[1][2][4],
                'select-index'                              =>   $data[1][2][5],
                'select-value'                              =>   $data[1][2][6],
            
                /* Dependence */      
                'selectDependence-identification'           =>   $data[1][3][0],
                'selectDependence-optionInstruction'        =>   $data[1][3][1],
                'selectDependence-table'                    =>   $data[1][3][2],
                'selectDependence-columns'                  =>   $data[1][3][3],
                'selectDependence-where'                    =>   $data[1][3][4],
                'selectDependence-orderby'                  =>   $data[1][3][5],
                'selectDependence-index'                    =>   $data[1][3][6],
                'selectDependence-value'                    =>   $data[1][3][7],
                'select-dependencies'                       =>   $data[1][4],
          
            ),
            /* Update */
            array(
                'input-display'                             =>   $data[2][0],
                'input-disabled'                            =>   $data[2][1]
            ),
            /* Database */
            array(
                'database-column'                           =>   $data[3][0],
                'database-value'                            =>   $data[3][1]
            )
        );


        /* Rename Arrays */
        $array['common']   =   $array[0];
        unset($array[0]);

        $array['specif']   =   $array[1];
        unset($array[1]);

        $array['update']   =   $array[2];
        unset($array[2]);

        $array['database']   =   $array[3];
        unset($array[3]);



    return $array;
    }      





    /***********************************************    
    DATE
    ***********************************************/
    public function date(array $data){

        /* Display */ (is_null($data[0][8]) || empty($data[0][8])) ? $data[0][8] = TRUE : '';
        /* Icon */ (empty($data[1][1])) ? $data[1][1] = NULL : '';
        /* Helper */ (empty($data[0][2])) ? $data[0][2] = NULL : '';
        /* Style */ (empty($data[0][6])) ? $data[0][6] = NULL : '';
        /* Validation Frontend */ (empty($data[0][10])) ? $data[0][10] = NULL : '';
        /* Validation Backend */ (empty($data[0][11])) ? $data[0][11] = NULL : '';

        $array  = array(
            /* Common Features */
            array(
                'input-type'                  =>   'date',
                'bootstrap-grid'              =>   $data[0][0],
                'input-label'                 =>   $data[0][1],
                'input-helper'                =>   $data[0][2],
                'input-value'                 =>   $data[0][3],
                'input-id'                    =>   $data[0][4],
                'input-class'                 =>   $data[0][5],
                'input-style'                 =>   $data[0][6],
                'input-attributes'            =>   $data[0][7],
                'input-display'               =>   $data[0][8],
                'input-disabled'              =>   $data[0][9],
                'validation-frontend'         =>   $data[0][10],
                'validation-backend'          =>   $data[0][11]
            ),
            /* Specif Input */
            array(
                'input-placeholder'           =>   $data[1][0],
                'input-icon'                  =>   $data[1][1],
                'input-size'                  =>   $data[1][2]
            ),
            /* Update */
            array(
                'input-display'               =>   $data[2][0],
                'input-disabled'              =>   $data[2][1]
            ),
            /* Database */
            array(
                'database-column'             =>   $data[3][0],
                'database-value'              =>   $data[3][1]
            )
        );


        /* Rename Arrays */
        $array['common']   =   $array[0];
        unset($array[0]);

        $array['specif']   =   $array[1];
        unset($array[1]);

        $array['update']   =   $array[2];
        unset($array[2]);

        $array['database']   =   $array[3];
        unset($array[3]);

    return $array;
    }






    /***********************************************    
    BOOLEAN
    ***********************************************/
    public function boolean(array $data){

        /* Display */ (is_null($data[0][8]) || empty($data[0][8])) ? $data[0][8] = TRUE : '';
        /* Helper */ (empty($data[0][2])) ? $data[0][2] = NULL : '';
        /* Style */ (empty($data[0][6])) ? $data[0][6] = NULL : '';
        /* Validation Frontend */ (empty($data[0][10])) ? $data[0][10] = NULL : '';
        /* Validation Backend */ (empty($data[0][11])) ? $data[0][11] = NULL : '';

        $array  = array(
            /* Common Features */
            array(
                'input-type'                  =>   'boolean',
                'bootstrap-grid'              =>   $data[0][0],
                'input-label'                 =>   $data[0][1],
                'input-helper'                =>   $data[0][2],
                'input-value'                 =>   $data[0][3],
                'input-id'                    =>   $data[0][4],
                'input-class'                 =>   $data[0][5],
                'input-style'                 =>   $data[0][6],
                'input-attributes'            =>   $data[0][7],
                'input-display'               =>   $data[0][8],
                'input-disabled'              =>   $data[0][9],
                'validation-frontend'         =>   $data[0][10],
                'validation-backend'          =>   $data[0][11]
            ),
            /* Specif Input */
            array(
                'input-on'                    =>   $data[1][0],
                'input-off'                   =>   $data[1][1]
            ),
            /* Update */
            array(
                'input-display'               =>   $data[2][0],
                'input-disabled'              =>   $data[2][1]
            ),
            /* Database */
            array(
                'database-column'             =>   $data[3][0],
                'database-value'              =>   $data[3][1]
            )
        );


        /* Rename Arrays */
        $array['common']   =   $array[0];
        unset($array[0]);

        $array['specif']   =   $array[1];
        unset($array[1]);

        $array['update']   =   $array[2];
        unset($array[2]);

        $array['database']   =   $array[3];
        unset($array[3]);


    return $array;
    }




    /***********************************************    
    NUMBER
    ***********************************************/
    public function number(array $data){

        /* Display */ (is_null($data[0][8]) || empty($data[0][8])) ? $data[0][8] = TRUE : '';
        /* Helper */ (empty($data[0][2])) ? $data[0][2] = NULL : '';
        /* Style */ (empty($data[0][6])) ? $data[0][6] = NULL : '';
        /* Validation Frontend */ (empty($data[0][10])) ? $data[0][10] = NULL : '';
        /* Validation Backend */ (empty($data[0][11])) ? $data[0][11] = NULL : '';
        /* Step */ (empty($data[1][2])) ? $data[1][2] = NULL : '';

        $array  = array(
            /* Common Features */
            array(
                'input-type'                  =>   'number',
                'bootstrap-grid'              =>   $data[0][0],
                'input-label'                 =>   $data[0][1],
                'input-helper'                =>   $data[0][2],
                'input-value'                 =>   $data[0][3],
                'input-id'                    =>   $data[0][4],
                'input-class'                 =>   $data[0][5],
                'input-style'                 =>   $data[0][6],
                'input-attributes'            =>   $data[0][7],
                'input-display'               =>   $data[0][8],
                'input-disabled'              =>   $data[0][9],
                'validation-frontend'         =>   $data[0][10],
                'validation-backend'          =>   $data[0][11]
            ),
            /* Specif Input */
            array(
                'number-length'               =>   $data[1][0],
                'number-readOnly'             =>   $data[1][1],
                'number-step'                 =>   $data[1][2],
            ),
            /* Update */
            array(
                'input-display'               =>   $data[2][0],
                'input-disabled'              =>   $data[2][1]
            ),
            /* Database */
            array(
                'database-column'             =>   $data[3][0],
                'database-value'              =>   $data[3][1]
            )
        );


        /* Rename Arrays */
        $array['common']   =   $array[0];
        unset($array[0]);

        $array['specif']   =   $array[1];
        unset($array[1]);

        $array['update']   =   $array[2];
        unset($array[2]);

        $array['database']   =   $array[3];
        unset($array[3]);


    return $array;
    }



    /***********************************************    
    WYSIH5ML5
    ***********************************************/
    public function wysihtml5(array $data){

        /* Display */ (is_null($data[0][8]) || empty($data[0][8])) ? $data[0][8] = TRUE : '';
        /* Helper */ (empty($data[0][2])) ? $data[0][2] = NULL : '';
        /* Style */ (empty($data[0][6])) ? $data[0][6] = NULL : '';
        /* Validation Frontend */ (empty($data[0][10])) ? $data[0][10] = NULL : '';
        /* Validation Backend */ (empty($data[0][11])) ? $data[0][11] = NULL : '';

        $array  = array(
            /* Common Features */
            array(
                'input-type'                  =>   'wysihtml5',
                'bootstrap-grid'              =>   $data[0][0],
                'input-label'                 =>   $data[0][1],
                'input-helper'                =>   $data[0][2],
                'input-value'                 =>   $data[0][3],
                'input-id'                    =>   $data[0][4],
                'input-class'                 =>   $data[0][5],
                'input-style'                 =>   $data[0][6],
                'input-attributes'            =>   $data[0][7],
                'input-display'               =>   $data[0][8],
                'input-disabled'              =>   $data[0][9],
                'validation-frontend'         =>   $data[0][10],
                'validation-backend'          =>   $data[0][11]
            ),
            /* Specif Input */
            array(
                'wysihtml5-rows'              =>   $data[1][0],
            ),
            /* Update */
            array(
                'input-display'               =>   $data[2][0],
                'input-disabled'              =>   $data[2][1]
            ),
            /* Database */
            array(
                'database-column'             =>   $data[3][0],
                'database-value'              =>   $data[3][1]
            )
        );


        /* Rename Arrays */
        $array['common']   =   $array[0];
        unset($array[0]);

        $array['specif']   =   $array[1];
        unset($array[1]);

        $array['update']   =   $array[2];
        unset($array[2]);

        $array['database']   =   $array[3];
        unset($array[3]);

    return $array;
    }





    /***********************************************    
    CKEDITOR
    ***********************************************/
    public function ckeditor(array $data){

        /* Display */ (is_null($data[0][8]) || empty($data[0][8])) ? $data[0][8] = TRUE : '';
        /* Helper */ (empty($data[0][2])) ? $data[0][2] = NULL : '';
        /* Style */ (empty($data[0][6])) ? $data[0][6] = NULL : '';
        /* Validation Frontend */ (empty($data[0][10])) ? $data[0][10] = NULL : '';
        /* Validation Backend */ (empty($data[0][11])) ? $data[0][11] = NULL : '';

        $array  = array(
            /* Common Features */
            array(
                'input-type'                  =>   'ckeditor',
                'bootstrap-grid'              =>   $data[0][0],
                'input-label'                 =>   $data[0][1],
                'input-helper'                =>   $data[0][2],
                'input-value'                 =>   $data[0][3],
                'input-id'                    =>   $data[0][4],
                'input-class'                 =>   $data[0][5],
                'input-style'                 =>   $data[0][6],
                'input-attributes'            =>   $data[0][7],
                'input-display'               =>   $data[0][8],
                'input-disabled'              =>   $data[0][9],
                'validation-frontend'         =>   $data[0][10],
                'validation-backend'          =>   $data[0][11]
            ),
            /* Specif Input */
            array(
                'ckeditor-rows'              =>   $data[1][0],
            ),
            /* Update */
            array(
                'input-display'               =>   $data[2][0],
                'input-disabled'              =>   $data[2][1]
            ),
            /* Database */
            array(
                'database-column'             =>   $data[3][0],
                'database-value'              =>   $data[3][1]
            )
        );


        /* Rename Arrays */
        $array['common']   =   $array[0];
        unset($array[0]);

        $array['specif']   =   $array[1];
        unset($array[1]);

        $array['update']   =   $array[2];
        unset($array[2]);

        $array['database']   =   $array[3];
        unset($array[3]);

    return $array;
    }













    /***********************************************  
      STATIC
    ***********************************************/
  public function staticControl(array $data){

    /*
      <div class="form-group last">
        <label class="col-md-3 control-label">Static Control</label>
        <div class="col-md-4">
          <span class="form-control-static">
          email@example.com </span>
        </div>
    */

    return $data;
  }


    /***********************************************	
      RADIOBUTTON
    ***********************************************/
	public function radiobutton(array $data){
		return $data;
	}


    /***********************************************	
      CHECKBOX
    ***********************************************/
	public function checkbox(array $data){
		return $data;
	}


    /***********************************************    
    TEXTAREA
    ***********************************************/
    public function textarea(array $data){

        /* Display */ (is_null($data[0][8]) || empty($data[0][8])) ? $data[0][8] = TRUE : '';
        /* Icon */ (empty($data[1][1])) ? $data[1][1] = NULL : '';
        /* Helper */ (empty($data[0][2])) ? $data[0][2] = NULL : '';
        /* Style */ (empty($data[0][6])) ? $data[0][6] = NULL : '';
        /* Validation Frontend */ (empty($data[0][10])) ? $data[0][10] = NULL : '';
        /* Validation Backend */ (empty($data[0][11])) ? $data[0][11] = NULL : '';

        $array  = array(
            /* Common Features */
            array(
                'input-type'                  =>   'textarea',
                'bootstrap-grid'              =>   $data[0][0],
                'input-label'                 =>   $data[0][1],
                'input-helper'                =>   $data[0][2],
                'input-value'                 =>   $data[0][3],
                'input-id'                    =>   $data[0][4],
                'input-class'                 =>   $data[0][5],
                'input-style'                 =>   $data[0][6],
                'input-attributes'            =>   $data[0][7],
                'input-display'               =>   $data[0][8],
                'input-disabled'              =>   $data[0][9],
                'validation-frontend'         =>   $data[0][10],
                'validation-backend'          =>   $data[0][11]
            ),
            /* Specif Input */
            array(
                'input-placeholder'           =>   $data[1][0],
            ),
            /* Update */
            array(
                'input-display'               =>   $data[2][0],
                'input-disabled'              =>   $data[2][1]
            ),
            /* Database */
            array(
                'database-column'             =>   $data[3][0],
                'database-value'              =>   $data[3][1]
            )
        );


        /* Rename Arrays */
        $array['common']   =   $array[0];
        unset($array[0]);

        $array['specif']   =   $array[1];
        unset($array[1]);

        $array['update']   =   $array[2];
        unset($array[2]);

        $array['database']   =   $array[3];
        unset($array[3]);


    return $array;
    }



    /****************************
    /***********************************************	
      SLUG
    ***********************************************/
	public function slug(array $data){


        /* Display */ (is_null($data[0][8]) || empty($data[0][8])) ? $data[0][8] = TRUE : '';
        /* Icon */ (empty($data[1][1])) ? $data[1][1] = NULL : '';
        /* Helper */ (empty($data[0][2])) ? $data[0][2] = NULL : '';
        /* Style */ (empty($data[0][6])) ? $data[0][6] = NULL : '';
        /* Validation Frontend */ (empty($data[0][10])) ? $data[0][10] = NULL : '';
        /* Validation Backend */ (empty($data[0][11])) ? $data[0][11] = NULL : '';

        $array  = array(
            /* Common Features */
            array(
                'input-type'                  =>   'slug',
                'bootstrap-grid'              =>   $data[0][0],
                'input-label'                 =>   $data[0][1],
                'input-helper'                =>   $data[0][2],
                'input-value'                 =>   $data[0][3],
                'input-id'                    =>   $data[0][4],
                'input-class'                 =>   $data[0][5],
                'input-style'                 =>   $data[0][6],
                'input-attributes'            =>   $data[0][7],
                'input-display'               =>   $data[0][8],
                'input-disabled'              =>   $data[0][9],
                'validation-frontend'         =>   $data[0][10],
                'validation-backend'          =>   $data[0][11]
            ),
            /* Specif Input */
            array(
                'input-reference'             =>   $data[1][0]
            ),
            /* Update */
            array(
                'input-display'               =>   $data[2][0],
                'input-disabled'              =>   $data[2][1]
            ),
            /* Database */
            array(
                'database-column'             =>   $data[3][0],
                'database-value'              =>   $data[3][1]
            )
        );


        /* Rename Arrays */
        $array['common']   =   $array[0];
        unset($array[0]);

        $array['specif']   =   $array[1];
        unset($array[1]);

        $array['update']   =   $array[2];
        unset($array[2]);

        $array['database']   =   $array[3];
        unset($array[3]);


        return $array;
	}


    /***********************************************	
      PASSWORD
    ***********************************************/
	public function password(array $data){
		return $data;
	}



}
/* End class*/