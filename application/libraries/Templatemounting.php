<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
	A classe InputConstruct e responsavel por tratar os dados recebidos e retornar em um array de dados com as configuracoes input. 
	Enquanto a classe inputconstructor monta os array para serem enviadas para a view
*/

class TemplateMounting extends MY_Controller{

    /*-- Mounting
    =============================================*/
    public function mounting($crudOperation, $method, $moduleSlug){

      $this->$method($crudOperation, $moduleSlug);

    }


    /*-- Porlet Title
    =============================================*/
    public function portletTitle($crudOperation, $moduleSlug){

      $this->load->module($moduleSlug);

      $settings         =   $this->$moduleSlug->settings($crudOperation); // ATEEENCAO - ESSE EH O CREATE, DUPLICAR PRO UPDATE
      $portletTitle     =   $settings['templateMounting'][0];
      $portletSubtitle  =   $settings['templateMounting'][1];


      $inputAction = '';

      /*--  [ CREATE | RETRIEVE ]  Printing on the user's screen
      =============================================*/
      if ($crudOperation == 'create' || $crudOperation == 'retrieve'):
    
        foreach ($settings['templateMounting']['portletButton'] as $key => $value) {
          $constructorBtn = $this->inputconstructor->btn($value);
          $mountingBtn    = $this->inputmounting->btn($constructorBtn);
          $inputAction    .= $mountingBtn;
        }

        echo '
          <div class="portlet-title">

            <div class="caption">
              <i class="icon-equalizer font-blue-hoki"></i>
              <span class="caption-subject font-blue-hoki bold uppercase">'.$portletTitle.'</span>
              <span class="caption-helper">'.$portletSubtitle.'</span>
            </div>

            <div class="actions">

              '.$inputAction.'

              <a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen" data-original-title="" title="Tela Cheia"></a>
            </div>

          </div>
          ';

      endif;


      /*--  [ UPDATE ]  Printing on the user's screen
      =============================================*/
      if ($crudOperation == 'update'):

        echo '
          <div class="caption">
            <i class="icon-pin font-yellow-lemon"></i>
            <span class="caption-subject bold font-yellow-lemon uppercase">
            '.$portletTitle.' </span>
            <span class="caption-helper">'.$portletSubtitle.'</span>
          </div>

        ';
        
      endif;

      return TRUE;
    }



    /*-- Breadcrumbs
    =============================================*/
    public function breadcrumb($crudOperation, $moduleSlug){

      $this->load->module($moduleSlug);
      $settings   = $this->$moduleSlug->settings($crudOperation);


      /*--  [ CREATE ]  Printing on the user's screen
      =============================================*/
      if ($crudOperation == 'create' || $crudOperation == 'retrieve' || $crudOperation == 'update'):

        #$inputAction = '';

        $breadcrumb = $settings['templateMounting']['breadcrumb'];

        $contador = 1;
        $countBreadcrumb = count($breadcrumb);

        foreach ($breadcrumb as $key => $value):

            echo '
            <li>
              <a href="'.$value.'" class="loadModalLoading">'.$key.'</a>
              ';
              if($contador!=$countBreadcrumb):
                echo '<i class="fa fa-circle"></i>';
              endif;
            echo '
             </li>';
        
          $contador++;
        
        endforeach;

      endif;

/*


*/


    }


}
/* End class*/