<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Moeda {


    /**
    * InserirPontuacao
    *
    * @author Gustavo Botega 
    * @return {type}
    */
	public function InserirPontuacao($Valor)
	{
        return number_format($Valor, 2, ',' , '.');
	}


    /**
    * RemoverPontuacao
    *
    * @author Gustavo Botega 
    * @return {type}
    */
	public function RemoverPontuacao($Valor)
	{
        return str_replace( ',' ,'.', str_replace('.','',$Valor) );
	}



}
?>