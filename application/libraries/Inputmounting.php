<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
	A classe InputConstruct e responsavel por tratar os dados recebidos e retornar em um array de dados com as configuracoes input. 

	Enquanto a classe inputconstructor monta os array para serem enviadas para a view
*/

class Inputmounting {


    public function mounting($arrData){
      //return $arrData;
      foreach ($arrData as $key => $value) {

        // Identificar cada tipo de input e encaminhar para a funcao que ira 
        // imprimir na tela o respecto conteudo html.

      }
    }


    public function html($crud = NULL, $input){
        
      switch ($input['common']['input-type']) {
        case 'text':
        $this->text($crud, $input);
        break;

        case 'filemanager':
        $this->filemanager($crud, $input);
        break;

        case 'slug':
        $this->slug($crud, $input);
        break;

        case 'select':
        $this->select($crud, $input);
        break;

        case 'selectDependence':
        $this->selectDependence($crud, $input);
        break;

        case 'date':
        $this->date($crud, $input);
        break;

        case 'boolean':
        $this->boolean($crud, $input);
        break;

        case 'textarea':
        $this->textarea($crud, $input);
        break;

        case 'number':
        $this->number($crud, $input);
        break;

        case 'map':
        $this->map($crud, $input);
        break;

        case 'wysihtml5':
        $this->wysihtml5($crud, $input);
        break;

        case 'ckeditor':
        $this->ckeditor($crud, $input);
        break;
       
       default:
         # code...
         break;
     }

    }


  /***********************************************  
  TEXT
  ***********************************************/
  public function text($crud, $input){

    /* Common */
    $inputType            = $input['common']['input-type'];
    $bootstrapGrid        = $input['common']['bootstrap-grid'];
    $inputLabel           = $input['common']['input-label'];
    $inputValue           = $input['common']['input-value'];
    $inputId              = $input['common']['input-id'];
    $inputClass           = $input['common']['input-class'];
    $inputStyle           = $input['common']['input-style'];
    $inputHelper          = $input['common']['input-helper'];
    $inputDisabled        = $input['common']['input-disabled'];
    $inputDisplay         = $input['common']['input-display'];
    $validationFrontend   = $input['common']['validation-frontend'];
    $validationBackend    = $input['common']['validation-backend'];


    /* Specif */
    $inputIcon            = $input['specif']['input-icon'];
    $inputSize            = $input['specif']['input-size'];
    $inputPlaceholder     = $input['specif']['input-placeholder'];

    /* Update */
    $inputDisplay         = $input['update']['input-display'];
    $inputDisabled        = $input['update']['input-disabled'];

    /* Database */
    $databaseColumn       = $input['database']['database-column'];
    $databaseValue        = $input['database']['database-value'];


    /*-- Setting Variables
    =============================================*/
    $iconPositionLeft       = NULL;
    $iconPositionRight      = NULL;
    $iconPositionTypeRight  = NULL;
    $beginTagIcon           = NULL;
    $endTagIcon             = NULL;
    $requiredTag            = NULL;


    /*-- Validation Javascript
    =============================================*/
    if(is_array($validationFrontend) && !is_null($validationFrontend)){
      
      // Required
      if(!is_null ($validationFrontend[0]) && $validationFrontend[0]==TRUE){
        $requiredTag  =  '<span class="required"> * </span>';
      }

    }


    /*-- Disabled
    =============================================*/
    switch ($inputDisabled) {
      case NULL:
        $inputDisabled = '';
        break;
      
      case TRUE:
        $inputDisabled = 'disabled';
        break;
    }

   
    /*-- Helper
    =============================================*/
    if(!is_null($inputHelper)):
      $inputHelper  = '<span class="help-block">'.$inputHelper.' </span>';
    endif;

   
    /*-- Icon ( tratar typeIcon ??? )
    =============================================*/
    $inputIconType = 'group'; 
    if(is_array($inputIcon) && !empty($inputIcon)):

      switch ($inputIcon[0]) {
        case 'inside': // dentro da caixa cinza
          $inputIconType = 'group';
          switch ($inputIcon[1]) {
            case 'left':
              $iconPositionLeft = '<span class="input-group-addon"><i class="'.$inputIcon[2].'"></i></span>';
              break;
            case 'right':
              $iconPositionRight = '<span class="input-group-addon"><i class="'.$inputIcon[2].'"></i></span>';
              break;
          }
          break;
        case 'outside': // fora da caixa cinza - dentro do input
          $inputIconType = 'icon';
          switch ($inputIcon[1]) {
            case 'left':
              $iconPositionLeft = ' <i class="'.$inputIcon[2].'"></i> ';
              break;
            case 'right':
              $iconPositionTypeRight = 'right';
              $iconPositionLeft = ' <i class="'.$inputIcon[2].'"></i>';
              break;
          }
          break;
      }

      $beginTagIcon =  '<div class="input-'.$inputIconType.' '.$iconPositionTypeRight.'">';
      $endTagIcon   =  '</div>';

    endif;


    /*-- Style
    =============================================*/
    switch ($inputStyle) {
      case NULL:
        $style = NULL;
        break;

      case !empty($inputStyle):
        $style = 'style="'.$inputStyle.'"';
        break;
    }


    /*--  [ CREATE ]  Printing on the user's screen
    =============================================*/
    if ($crud=='create'):
      echo '
      <div class="'.$bootstrapGrid.'">
        <div class="form-group">
          <label class="control-label" for="'.$inputId.'">'.$inputLabel.' '.$requiredTag.' </label>

          '.$beginTagIcon.'

            '.$iconPositionLeft.'
             
            <input type="text" id="'.$inputId.'" name="'.$inputId.'" value="'.$inputValue.'" class="form-control '.$inputSize.' '.$inputClass.'" placeholder="'.$inputPlaceholder.'" '.$style.'  '.$inputDisabled.' />

            '.$iconPositionRight.'

          '.$endTagIcon.'

          '.$inputHelper.'

        </div>
      </div>
      ';
      return TRUE;
    endif;


    /*--  [ UPDATE ]  Printing on the user's screen
    =============================================*/
    if ($crud=='update'):
      echo '
      <div class="'.$bootstrapGrid.'">
        <div class="form-group">
          <label class="control-label" for="'.$inputId.'">'.$inputLabel.' '.$requiredTag.' </label>

          '.$beginTagIcon.'

            '.$iconPositionLeft.'
             
            <input type="text" id="'.$inputId.'" name="'.$inputId.'" value="'.$databaseValue.'" class="form-control '.$inputSize.' '.$inputClass.'" placeholder="'.$inputPlaceholder.'" '.$style.'  '.$inputDisabled.' />

            '.$iconPositionRight.'

          '.$endTagIcon.'

          '.$inputHelper.'

        </div>
      </div>
      ';
      return TRUE;
    endif;

  }










  /***********************************************  
  FILEMANAGER
  ***********************************************/
  public function filemanager($crud, $input){

    /* Common */
    $inputType            = $input['common']['input-type'];
    $bootstrapGrid        = $input['common']['bootstrap-grid'];
    $inputLabel           = $input['common']['input-label'];
    $inputValue           = $input['common']['input-value'];
    $inputId              = $input['common']['input-id'];
    $inputClass           = $input['common']['input-class'];
    $inputStyle           = $input['common']['input-style'];
    $inputHelper          = $input['common']['input-helper'];
    $inputDisabled        = $input['common']['input-disabled'];
    $inputDisplay         = $input['common']['input-display'];
    $validationFrontend   = $input['common']['validation-frontend'];
    $validationBackend    = $input['common']['validation-backend'];


    /* Specif */
    $inputIcon            = $input['specif']['input-icon'];
    $inputSize            = $input['specif']['input-size'];
    $inputPlaceholder     = $input['specif']['input-placeholder'];

    /* Update */
    $inputDisplay         = $input['update']['input-display'];
    $inputDisabled        = $input['update']['input-disabled'];

    /* Database */
    $databaseColumn       = $input['database']['database-column'];
    $databaseValue        = $input['database']['database-value'];


    /*-- Setting Variables
    =============================================*/
    $iconPositionLeft       = NULL;
    $iconPositionRight      = NULL;
    $iconPositionTypeRight  = NULL;
    $beginTagIcon           = NULL;
    $endTagIcon             = NULL;
    $requiredTag            = NULL;


    /*-- Validation Javascript
    =============================================*/
    if(is_array($validationFrontend) && !is_null($validationFrontend)){
      
      // Required
      if(!is_null ($validationFrontend[0]) && $validationFrontend[0]==TRUE){
        $requiredTag  =  '<span class="required"> * </span>';
      }

    }


    /*-- Disabled
    =============================================*/
    switch ($inputDisabled) {
      case NULL:
        $inputDisabled = '';
        break;
      
      case TRUE:
        $inputDisabled = 'disabled';
        break;
    }

   
    /*-- Helper
    =============================================*/
    if(!is_null($inputHelper)):
      $inputHelper  = '<span class="help-block">'.$inputHelper.' </span>';
    endif;

   
    /*-- Icon ( tratar typeIcon ??? )
    =============================================*/
    $inputIconType = 'group'; 
    if(is_array($inputIcon) && !empty($inputIcon)):

      switch ($inputIcon[0]) {
        case 'inside': // dentro da caixa cinza
          $inputIconType = 'group';
          switch ($inputIcon[1]) {
            case 'left':
              $iconPositionLeft = '<span class="input-group-addon"><i class="'.$inputIcon[2].'"></i></span>';
              break;
            case 'right':
              $iconPositionRight = '<span class="input-group-addon"><i class="'.$inputIcon[2].'"></i></span>';
              break;
          }
          break;
        case 'outside': // fora da caixa cinza - dentro do input
          $inputIconType = 'icon';
          switch ($inputIcon[1]) {
            case 'left':
              $iconPositionLeft = ' <i class="'.$inputIcon[2].'"></i> ';
              break;
            case 'right':
              $iconPositionTypeRight = 'right';
              $iconPositionLeft = ' <i class="'.$inputIcon[2].'"></i>';
              break;
          }
          break;
      }

      $beginTagIcon =  '<div class="input-'.$inputIconType.' '.$iconPositionTypeRight.'">';
      $endTagIcon   =  '</div>';

    endif;


    /*-- Style
    =============================================*/
    switch ($inputStyle) {
      case NULL:
        $style = NULL;
        break;

      case !empty($inputStyle):
        $style = 'style="'.$inputStyle.'"';
        break;
    }


    /*--  [ CREATE ]  Printing on the user's screen
    =============================================*/
    if ($crud=='create'):
      echo '
      <div class="'.$bootstrapGrid.'">
        <div class="form-group">
          <label class="control-label" for="'.$inputId.'">'.$inputLabel.' '.$requiredTag.' </label>

          '.$beginTagIcon.'

            '.$iconPositionLeft.'
             
            <input type="text" id="'.$inputId.'" name="'.$inputId.'" value="'.$inputValue.'" class="form-control '.$inputSize.' '.$inputClass.'" placeholder="'.$inputPlaceholder.'" '.$style.'  '.$inputDisabled.' />
            <a href="http://fhbr.com.br/manager/filemanager/dialog.php?type=2&field_id='.$inputId.'&crossdomain=1" class="btn btn-success btn-block iframe-btn" type="button">Selecionar Arquivo</a>


            '.$iconPositionRight.'

          '.$endTagIcon.'

          '.$inputHelper.'

        </div>
      </div>
      ';
      return TRUE;
    endif;


    /*--  [ UPDATE ]  Printing on the user's screen
    =============================================*/
    if ($crud=='update'):
      echo '
      <div class="'.$bootstrapGrid.'">
        <div class="form-group" style="margin-bottom:0px;">
          <label class="control-label" for="'.$inputId.'">'.$inputLabel.' '.$requiredTag.' </label>

          '.$beginTagIcon.'

            '.$iconPositionLeft.'
             
            <input type="text" id="'.$inputId.'" name="'.$inputId.'" value="'.$databaseValue.'" class="form-control '.$inputSize.' '.$inputClass.'" placeholder="'.$inputPlaceholder.'" '.$style.'  '.$inputDisabled.' />
            <a href="http://fhbr.com.br/manager/filemanager/dialog.php?type=2&field_id='.$inputId.'&crossdomain=1" class="btn btn-success btn-block iframe-btn filemanager-stand-alone" type="button">Selecionar Arquivo</a>

            '.$iconPositionRight.'

          '.$endTagIcon.'

          '.$inputHelper.'

        </div>
      </div>
      ';
      return TRUE;
    endif;

  }










  /***********************************************  
  MAP
  ***********************************************/
  public function map($crud, $input){

    /* Common */
    $inputType            = $input['common']['input-type'];
    $bootstrapGrid        = $input['common']['bootstrap-grid'];
    $inputLabel           = $input['common']['input-label'];
    $inputValue           = $input['common']['input-value'];
    $inputId              = $input['common']['input-id'];
    $inputClass           = $input['common']['input-class'];
    $inputStyle           = $input['common']['input-style'];
    $inputHelper          = $input['common']['input-helper'];
    $inputDisabled        = $input['common']['input-disabled'];
    $inputDisplay         = $input['common']['input-display'];
    $validationFrontend   = $input['common']['validation-frontend'];
    $validationBackend    = $input['common']['validation-backend'];


    /* Specif */
    $inputIcon            = $input['specif']['input-icon'];
    $inputSize            = $input['specif']['input-size'];
    $inputPlaceholder     = $input['specif']['input-placeholder'];

    /* Update */
    $inputDisplay         = $input['update']['input-display'];
    $inputDisabled        = $input['update']['input-disabled'];

    /* Database */
    $databaseColumn       = $input['database']['database-column'];
    $databaseValue        = $input['database']['database-value'];


    /*-- Setting Variables
    =============================================*/
    $iconPositionLeft       = NULL;
    $iconPositionRight      = NULL;
    $iconPositionTypeRight  = NULL;
    $beginTagIcon           = NULL;
    $endTagIcon             = NULL;
    $requiredTag            = NULL;


    /*-- Validation Javascript
    =============================================*/
    if(is_array($validationFrontend) && !is_null($validationFrontend)){
      
      // Required
      if(!is_null ($validationFrontend[0]) && $validationFrontend[0]==TRUE){
        $requiredTag  =  '<span class="required"> * </span>';
      }

    }


    /*-- Disabled
    =============================================*/
    switch ($inputDisabled) {
      case NULL:
        $inputDisabled = '';
        break;
      
      case TRUE:
        $inputDisabled = 'disabled';
        break;
    }

   
    /*-- Helper
    =============================================*/
    if(!is_null($inputHelper)):
      $inputHelper  = '<span class="help-block">'.$inputHelper.' </span>';
    endif;

   
    /*-- Icon ( tratar typeIcon ??? )
    =============================================*/
    $inputIconType = 'group'; 
    if(is_array($inputIcon) && !empty($inputIcon)):

      switch ($inputIcon[0]) {
        case 'inside': // dentro da caixa cinza
          $inputIconType = 'group';
          switch ($inputIcon[1]) {
            case 'left':
              $iconPositionLeft = '<span class="input-group-addon"><i class="'.$inputIcon[2].'"></i></span>';
              break;
            case 'right':
              $iconPositionRight = '<span class="input-group-addon"><i class="'.$inputIcon[2].'"></i></span>';
              break;
          }
          break;
        case 'outside': // fora da caixa cinza - dentro do input
          $inputIconType = 'icon';
          switch ($inputIcon[1]) {
            case 'left':
              $iconPositionLeft = ' <i class="'.$inputIcon[2].'"></i> ';
              break;
            case 'right':
              $iconPositionTypeRight = 'right';
              $iconPositionLeft = ' <i class="'.$inputIcon[2].'"></i>';
              break;
          }
          break;
      }

      $beginTagIcon =  '<div class="input-'.$inputIconType.' '.$iconPositionTypeRight.'">';
      $endTagIcon   =  '</div>';

    endif;


    /*-- Style
    =============================================*/
    switch ($inputStyle) {
      case NULL:
        $style = NULL;
        break;

      case !empty($inputStyle):
        $style = 'style="'.$inputStyle.'"';
        break;
    }


    /*--  [ CREATE ]  Printing on the user's screen
    =============================================*/
    if ($crud=='create'):
      echo '
      <div class="'.$bootstrapGrid.'">
        <div class="form-group">
          <label class="control-label" for="'.$inputId.'">'.$inputLabel.' '.$requiredTag.' </label>

          '.$beginTagIcon.'

            '.$iconPositionLeft.'
             
            <input type="text" id="'.$inputId.'" name="'.$inputId.'" value="'.$inputValue.'" class="form-control '.$inputSize.' '.$inputClass.'" placeholder="'.$inputPlaceholder.'" '.$style.'  '.$inputDisabled.' />

            '.$iconPositionRight.'

          '.$endTagIcon.'

          '.$inputHelper.'

        </div>
      </div>
      ';
      return TRUE;
    endif;


    /*--  [ UPDATE ]  Printing on the user's screen
    =============================================*/
    if ($crud=='update'):
      echo '
      <div class="'.$bootstrapGrid.'">
        <div class="form-group">
          <label class="control-label" for="'.$inputId.'">'.$inputLabel.' '.$requiredTag.' </label>

          '.$beginTagIcon.'

            '.$iconPositionLeft.'
             
            <input type="text" id="'.$inputId.'" name="'.$inputId.'" value="'.$databaseValue.'" class="form-control '.$inputSize.' '.$inputClass.'" placeholder="'.$inputPlaceholder.'" '.$style.'  '.$inputDisabled.' />

            '.$iconPositionRight.'

          '.$endTagIcon.'

          '.$inputHelper.'

        </div>
      </div>
      ';
      return TRUE;
    endif;

  }











  /***********************************************  
  SLUG
  ***********************************************/
  public function slug($crud, $input){

    /* Common */
    $inputType            = $input['common']['input-type'];
    $bootstrapGrid        = $input['common']['bootstrap-grid'];
    $inputLabel           = $input['common']['input-label'];
    $inputId              = $input['common']['input-id'];
    $inputClass           = $input['common']['input-class'];
    $inputStyle           = $input['common']['input-style'];
    $inputHelper          = $input['common']['input-helper'];
    $inputDisplay         = $input['common']['input-display'];

    /* Specif */
    $reference            = $input['specif']['input-reference'];

    /* Update */
    $inputDisplay         = $input['update']['input-display'];
    $inputDisabled        = $input['update']['input-disabled'];

    /* Database */
    $databaseColumn       = $input['database']['database-column'];
    $databaseValue        = $input['database']['database-value'];


    /*-- Style
    =============================================*/
    switch ($inputStyle) {
      case NULL:
        $style = NULL;
        break;

      case !empty($inputStyle):
        $style = 'style="'.$inputStyle.'"';
        break;
    }

   
    /*-- Helper
    =============================================*/
    if(!is_null($inputHelper)):
      $inputHelper  = '<span class="help-block">'.$inputHelper.' </span>';
    endif;


    /*--  [ CREATE ]  Printing on the user's screen
    =============================================*/
    if ($crud=='create'):
      echo '
        <div class="'.$bootstrapGrid.'">

          <div class="form-group">
            <label class="control-label">'.$inputLabel.'</label>
              <input type="text" id="'.$inputId.'" name="'.$inputId.'" class="form-control '.$inputClass.'" '.$inputStyle.' readOnly />

            '.$inputHelper.'

          </div>
        </div>
      ';
    endif;


    /*--  [ UPDATE ]  Printing on the user's screen
    =============================================*/
    if ($crud=='update'):
      echo '
        <div class="'.$bootstrapGrid.'">

          <div class="form-group">
            <label class="control-label">'.$inputLabel.'</label>
              <input type="text" id="'.$inputId.'" placeholder="'.$databaseValue.'" name="'.$inputId.'" class="form-control '.$inputClass.'" '.$inputStyle.' readOnly />

            '.$inputHelper.'

          </div>
        </div>
      ';
    endif;



    return TRUE;
  }




  /***********************************************  
  DATE
  ***********************************************/
  public function date($crud, $input){

    /* Common */
    $inputType            = $input['common']['input-type'];
    $bootstrapGrid        = $input['common']['bootstrap-grid'];
    $inputLabel           = $input['common']['input-label'];
    $inputValue           = $input['common']['input-value'];
    $inputId              = $input['common']['input-id'];
    $inputClass           = $input['common']['input-class'];
    $inputStyle           = $input['common']['input-style'];
    $inputHelper          = $input['common']['input-helper'];
    $inputDisabled        = $input['common']['input-disabled'];
    $inputDisplay         = $input['common']['input-display'];
    $validationFrontend   = $input['common']['validation-frontend'];
    $validationBackend    = $input['common']['validation-backend'];

    /* Specif */
    $inputIcon            = $input['specif']['input-icon'];
    $inputSize            = $input['specif']['input-size'];
    $inputPlaceholder     = $input['specif']['input-placeholder'];

    /* Update */
    $inputDisplay         = $input['update']['input-display'];
    $inputDisabled        = $input['update']['input-disabled'];

    /* Database */
    $databaseColumn       = $input['database']['database-column'];
    $databaseValue        = $input['database']['database-value'];


    /*-- Setting Variables
    =============================================*/
    $iconPositionLeft       = NULL;
    $iconPositionRight      = NULL;
    $iconPositionTypeRight  = NULL;
    $beginTagIcon           = NULL;
    $endTagIcon             = NULL;
    $requiredTag            = NULL;



    /*-- Mask Date
    =============================================*/
    $iconPositionLeft       = NULL;
    $ci =& get_instance();
    $ci->load->library('My_date');
    $dateMask = $ci->my_date->datetime($databaseValue, 'justDate');




    /*-- Validation Javascript
    =============================================*/
    if(is_array($validationFrontend) && !is_null($validationFrontend)){
      
      // Required
      if(!is_null ($validationFrontend[0]) && $validationFrontend[0]==TRUE){
        $requiredTag  =  '<span class="required"> * </span>';
      }

    }


    /*-- Disabled
    =============================================*/
    switch ($inputDisabled) {
      case NULL:
        $inputDisabled = '';
        break;
      
      case TRUE:
        $inputDisabled = 'disabled';
        break;
    }

   
    /*-- Helper
    =============================================*/
    if(!is_null($inputHelper)):
      $inputHelper  = '<span class="help-block">'.$inputHelper.' </span>';
    endif;

   
    /*-- Icon ( tratar typeIcon ??? )
    =============================================*/
    $inputIconType = 'group'; 
    if(is_array($inputIcon) && !empty($inputIcon)):

      switch ($inputIcon[0]) {
        case 'inside': // dentro da caixa cinza
          $inputIconType = 'group';
          switch ($inputIcon[1]) {
            case 'left':
              $iconPositionLeft = '<span class="input-group-addon"><i class="'.$inputIcon[2].'"></i></span>';
              break;
            case 'right':
              $iconPositionRight = '<span class="input-group-addon"><i class="'.$inputIcon[2].'"></i></span>';
              break;
          }
          break;
        case 'outside': // fora da caixa cinza - dentro do input
          $inputIconType = 'icon';
          switch ($inputIcon[1]) {
            case 'left':
              $iconPositionLeft = ' <i class="'.$inputIcon[2].'"></i> ';
              break;
            case 'right':
              $iconPositionTypeRight = 'right';
              $iconPositionLeft = ' <i class="'.$inputIcon[2].'"></i>';
              break;
          }
          break;
      }

      $beginTagIcon =  '<div class="input-'.$inputIconType.' '.$iconPositionTypeRight.'">';
      $endTagIcon   =  '</div>';

    endif;


    /*-- Style
    =============================================*/
    switch ($inputStyle) {
      case NULL:
        $style = NULL;
        break;

      case !empty($inputStyle):
        $style = 'style="'.$inputStyle.'"';
        break;
    }


    /*--  [ CREATE ]  Printing on the user's screen
    =============================================*/
    if ($crud=='create'):
      echo '
      <div class="'.$bootstrapGrid.'">
        <div class="form-group">
          <label class="control-label" for="'.$inputId.'">'.$inputLabel.' '.$requiredTag.' </label>

          '.$beginTagIcon.'

            '.$iconPositionLeft.'
             
            <input type="text" id="'.$inputId.'" class="form-control form-control-inline date-picker maskDate hiddenTrigger '.$inputSize.' '.$inputClass.'" placeholder="'.$inputPlaceholder.'" '.$style.'  '.$inputDisabled.' readOnly />

            '.$iconPositionRight.'

          '.$endTagIcon.'

          '.$inputHelper.'

          <input type="hidden" name="'.$inputId.'" value=""  />

        </div>
      </div>


      ';
    endif;


    /*--  [ UPDATE ]  Printing on the user's screen
    =============================================*/
    if ($crud=='update'):

      echo '
      <div class="'.$bootstrapGrid.'">
        <div class="form-group">
          <label class="control-label" for="'.$inputId.'">'.$inputLabel.' '.$requiredTag.' </label>

          '.$beginTagIcon.'

            '.$iconPositionLeft.'
             
             <input type="text" id="'.$inputId.'" name="'.$inputId.'" value="'.$dateMask.'" class="form-control form-control-inline date-picker maskDate hiddenTrigger'.$inputSize.' '.$inputClass.'" placeholder="'.$inputPlaceholder.'" '.$style.'  '.$inputDisabled.' readOnly />

            '.$iconPositionRight.'

          '.$endTagIcon.'

          '.$inputHelper.'

          <input type="hidden" name="'.$inputId.'" value="'.$databaseValue.'"  />

        </div>
      </div>
      ';
    endif;

    return TRUE;
  }





  /***********************************************  
  BOOLEAN
  ***********************************************/
  public function boolean($crud, $input){

    /* Common */
    $inputType            = $input['common']['input-type'];
    $bootstrapGrid        = $input['common']['bootstrap-grid'];
    $inputLabel           = $input['common']['input-label'];
    $inputValue           = $input['common']['input-value'];
    $inputId              = $input['common']['input-id'];
    $inputClass           = $input['common']['input-class'];
    $inputStyle           = $input['common']['input-style'];
    $inputHelper          = $input['common']['input-helper'];
    $inputDisabled        = $input['common']['input-disabled'];
    $inputDisplay         = $input['common']['input-display'];
    $validationFrontend   = $input['common']['validation-frontend'];
    $validationBackend    = $input['common']['validation-backend'];

    /* Specif */
    $inputOn              = $input['specif']['input-on'];
    $inputOff             = $input['specif']['input-off'];

    /* Update */
    $inputDisplay         = $input['update']['input-display'];
    $inputDisabled        = $input['update']['input-disabled'];

    /* Database */
    $databaseColumn       = $input['database']['database-column'];
    $databaseValue        = $input['database']['database-value'];


    /*-- Helper
    =============================================*/
    if(!is_null($inputHelper)):
      $inputHelper  = '<span class="help-block">'.$inputHelper.' </span>';
    endif;



    /*--  [ CREATE ]  Printing on the user's screen
    =============================================*/
    if ($crud=='create'):
      echo '
        <div class="'.$bootstrapGrid.'">
            <div class="form-group">

              <input type="hidden" name="'.$inputId.'" value="bootstrapSwitchEmpty"  />

              <label class="control-label">'.$inputLabel.'</label>
              <div class="radio-list">
                <input type="checkbox"  id="'.$inputId.'" name="'.$inputId.'" class="make-switch" data-trigger="boolean" data-on-text="'.$inputOn.'" data-off-text="'.$inputOff.'" >
              </div>

              '.$inputHelper.'


            </div>
          </div>
      ';
    endif;


    /*--  [ UPDATE ]  Printing on the user's screen
    =============================================*/
    if ($crud=='update'):

      $checked = '';
      if($databaseValue=='1'){
        $checked = 'checked';
      }


      echo '
        <div class="'.$bootstrapGrid.'">
            <div class="form-group">

              <input type="hidden" name="'.$inputId.'" value="bootstrapSwitchEmpty"  />

              <label class="control-label">'.$inputLabel.'</label>

              <div class="radio-list">
                <input type="checkbox"  id="'.$inputId.'" name="'.$inputId.'" class="make-switch" data-on-text="'.$inputOn.'" data-off-text="'.$inputOff.'" '.$checked.' >
              </div>

              '.$inputHelper.'

            </div>
          </div>
      ';
    endif;


    return TRUE;
  }



  /***********************************************  
  NUMBER
  - Não funciona minLength e MaxLength - integrar com JS
  - Implementar STEP Number. step by step
  ***********************************************/
  public function number($crud, $input){

    /* Common */
    $inputType            = $input['common']['input-type'];
    $bootstrapGrid        = $input['common']['bootstrap-grid'];
    $inputLabel           = $input['common']['input-label'];
    $inputValue           = $input['common']['input-value'];
    $inputId              = $input['common']['input-id'];
    $inputClass           = $input['common']['input-class'];
    $inputStyle           = $input['common']['input-style'];
    $inputHelper          = $input['common']['input-helper'];
    $inputDisabled        = $input['common']['input-disabled'];
    $inputDisplay         = $input['common']['input-display'];
    $validationFrontend   = $input['common']['validation-frontend'];
    $validationBackend    = $input['common']['validation-backend'];

    /* Specif */
    $numberLength         = $input['specif']['number-length'];
    $numberReadOnly       = $input['specif']['number-readOnly'];
    $numberStep           = $input['specif']['number-step'];

    /* Update */
    $inputDisplay         = $input['update']['input-display'];
    $inputDisabled        = $input['update']['input-disabled'];

    /* Database */
    $databaseColumn       = $input['database']['database-column'];
    $databaseValue        = $input['database']['database-value'];

    /*-- Helper
    =============================================*/
    if(!is_null($inputHelper)):
      $inputHelper  = '<span class="help-block">'.$inputHelper.' </span>';
    endif;


    /*-- Read Only
    =============================================*/
    $readOnly = '';
    if(!empty($numberReadOnly) && $numberReadOnly == TRUE):
      $readOnly = 'readonly';
    endif;


    /*-- Length
    =============================================*/
    $minLength = '';
    $maxLength = '';

    if(!is_null($numberLength)):

      if (!empty($numberLength[0])) {
        $minLength =  'minlength="' . $numberLength[0] . '"';
      }
      if (!empty($numberLength[1])) {
        $maxLength =  'maxlength="' . $numberLength[1] . '"';
      }

    endif;


    /*--  [ CREATE ]  Printing on the user's screen
    =============================================*/
    if ($crud=='create'):
    echo '
    <div class="'.$bootstrapGrid.'">

      <div class="form-group">
       
        <label class="control-label"> '.$inputLabel.'</label>
       
        <div id="spinner3">
         
          <div class="input-group" style="width:150px;">
           
            <input type="text" id="'.$inputId.'" name="'.$inputId.'" class="spinner-input form-control '.$inputClass.'" '.$minLength.'  '.$maxLength.'  '.$readOnly.'>
           
            <div class="spinner-buttons input-group-btn">
            
              <button type="button" class="btn spinner-up default">
                <i class="fa fa-angle-up"></i>
              </button>

              <button type="button" class="btn spinner-down default">
                <i class="fa fa-angle-down"></i>
              </button>

            </div>

          </div>

            '.$inputHelper.'

        </div>
      </div>
    </div>
    ';
    endif;


    /*--  [ UPDATE ]  Printing on the user's screen
    =============================================*/
    if ($crud=='update'):
    echo '
    <div class="'.$bootstrapGrid.'">

      <div class="form-group">
       
        <label class="control-label"> '.$inputLabel.'</label>
       
        <div id="spinner3">
         
          <div class="input-group" style="width:150px;">
           
            <input type="text" id="'.$inputId.'" name="'.$inputId.'" value="'.$databaseValue.'" class="spinner-input form-control '.$inputClass.'" '.$minLength.'  '.$maxLength.'  '.$readOnly.'>
           
            <div class="spinner-buttons input-group-btn">
            
              <button type="button" class="btn spinner-up default">
                <i class="fa fa-angle-up"></i>
              </button>

              <button type="button" class="btn spinner-down default">
                <i class="fa fa-angle-down"></i>
              </button>

            </div>

          </div>

            '.$inputHelper.'

        </div>
      </div>
    </div>
    ';
    endif;

    return TRUE;
  }





  /***********************************************  
  WYSIHTML5
  ***********************************************/
  public function wysihtml5($crud, $input){

    /* Common */
    $inputType            = $input['common']['input-type'];
    $bootstrapGrid        = $input['common']['bootstrap-grid'];
    $inputLabel           = $input['common']['input-label'];
    $inputValue           = $input['common']['input-value'];
    $inputId              = $input['common']['input-id'];
    $inputClass           = $input['common']['input-class'];
    $inputStyle           = $input['common']['input-style'];
    $inputHelper          = $input['common']['input-helper'];
    $inputDisabled        = $input['common']['input-disabled'];
    $inputDisplay         = $input['common']['input-display'];
    $validationFrontend   = $input['common']['validation-frontend'];
    $validationBackend    = $input['common']['validation-backend'];

    /* Specif */
    $wysihtml5Rows        = $input['specif']['wysihtml5-rows'];

    /* Update */
    $inputDisplay         = $input['update']['input-display'];
    $inputDisabled        = $input['update']['input-disabled'];

    /* Database */
    $databaseColumn       = $input['database']['database-column'];
    $databaseValue        = $input['database']['database-value'];


    /*-- Helper
    =============================================*/
    if(!is_null($inputHelper)):
      $inputHelper  = '<span class="help-block">'.$inputHelper.' </span>';
    endif;


    /*--  [ CREATE ]  Printing on the user's screen
    =============================================*/
    if ($crud=='create'):
    echo '
    <div class="'.$bootstrapGrid.'">

      <div class="form-group">
       
        <label class="control-label"> '.$inputLabel.'</label>
       
         <textarea class="wysihtml5 form-control" name="'.$databaseColumn.'" rows="'.$wysihtml5Rows.'"></textarea>

          '.$inputHelper.'

      </div>
    </div>
    ';
    endif;


    /*--  [ UPDATE ]  Printing on the user's screen
    =============================================*/
    if ($crud=='update'):
    echo '
    <div class="'.$bootstrapGrid.'">

      <div class="form-group">
       
        <label class="control-label"> '.$inputLabel.'</label>
       
         <textarea class="wysihtml5 form-control" name="'.$databaseColumn.'" rows="'.$wysihtml5Rows.'">'.$databaseValue.'</textarea>

          '.$inputHelper.'

      </div>
    </div>
    ';
    endif;


    return TRUE;
  }






  /***********************************************  
  CKEDITOR
  ***********************************************/
  public function ckeditor($crud, $input){

    /* Common */
    $inputType            = $input['common']['input-type'];
    $bootstrapGrid        = $input['common']['bootstrap-grid'];
    $inputLabel           = $input['common']['input-label'];
    $inputValue           = $input['common']['input-value'];
    $inputId              = $input['common']['input-id'];
    $inputClass           = $input['common']['input-class'];
    $inputStyle           = $input['common']['input-style'];
    $inputHelper          = $input['common']['input-helper'];
    $inputDisabled        = $input['common']['input-disabled'];
    $inputDisplay         = $input['common']['input-display'];
    $validationFrontend   = $input['common']['validation-frontend'];
    $validationBackend    = $input['common']['validation-backend'];

    /* Specif */
    $ckeditorRows         = $input['specif']['ckeditor-rows'];

    /* Update */
    $inputDisplay         = $input['update']['input-display'];
    $inputDisabled        = $input['update']['input-disabled'];

    /* Database */
    $databaseColumn       = $input['database']['database-column'];
    $databaseValue        = $input['database']['database-value'];


    /*-- Helper
    =============================================*/
    if(!is_null($inputHelper)):
      $inputHelper  = '<span class="help-block">'.$inputHelper.' </span>';
    endif;


    /*--  [ CREATE ]  Printing on the user's screen
    =============================================*/
    if ($crud=='create'):
    echo '
    <div class="'.$bootstrapGrid.'">

      <div class="form-group">
       
        <label class="control-label"> '.$inputLabel.'</label>
       
         <textarea class="ckeditor form-control" name="'.$databaseColumn.'" rows="'.$ckeditorRows.'"></textarea>

          '.$inputHelper.'

      </div>
    </div>
    ';
    endif;


    /*--  [ UPDATE ]  Printing on the user's screen
    =============================================*/
    if ($crud=='update'):
    echo '
    <div class="'.$bootstrapGrid.'">

      <div class="form-group">
       
        <label class="control-label"> '.$inputLabel.'</label>
       
         <textarea class="ckeditor form-control" name="'.$databaseColumn.'" rows="'.$ckeditorRows.'">'.$databaseValue.'</textarea>

          '.$inputHelper.'

      </div>
    </div>
    ';
    endif;
    return TRUE;
  }






  /***********************************************  
  BTN
  ------------------------------------------------
  array (size=10)
  'input-type' => string 'btn' (length=3)
  'input-label' => string 
  'input-link' => string 
  'input-size' => string
  'input-block' => boolean 
  'input-color' => string 
  'input-icon' => string 
  'input-disabled' => boolean 
  'input-id' => string 
  'input-class
  ***********************************************/
  public function btn($data){

    # LINK
    switch ($data['input-link'][0]) {
      case 'linkInternal':
        $link = base_url() . $data['input-link'][1];
        break;
      case 'linkExternal':
        $link = $data['input-link'][1];
        break;
    }


    $echo = ' <a href="'.$link.'" data-link="'.$data['input-link'][1].'" id="'.$data['input-id'].'"  class="btn loadModalLoading ' . $data['input-color'] . ' ' . $data['input-block'] . ' ' . $data['input-size'] . ' ' . $data['input-class'] .  ' ' . $data['input-disabled'] . '" target="'.$data['input-link'][2].'"><i class="'.$data['input-icon'].'"></i> '.$data['input-label'].' </a>';
    return $echo;
  }


    /***********************************************	
      SELECT
    ***********************************************/
	public function select($crud, array $input){

    /* Common */
    $inputType                  = $input['common']['input-type'];
    $bootstrapGrid              = $input['common']['bootstrap-grid'];
    $inputLabel                 = $input['common']['input-label'];
    $inputId                    = $input['common']['input-id'];
    $inputClass                 = $input['common']['input-class'];
    $inputStyle                 = $input['common']['input-style'];
    $inputHelper                = $input['common']['input-helper'];
    $inputDisplay               = $input['common']['input-display'];

    /* Specif */
    $inputOptionInstruction     = $input['specif']['input-optionInstruction'];
    $inputTable                 = $input['specif']['input-table'];
    $inputColumns               = $input['specif']['input-columns'];
    $inputWhere                 = $input['specif']['input-where'];
    $inputOrderby               = $input['specif']['input-orderby'];
    $inputIndex                 = $input['specif']['input-index'];
    $inputValue                 = $input['specif']['input-value'];

    /* Update */
    $inputUpdateDisplay         = $input['update']['input-display'];
    $inputUpdateDisabled        = $input['update']['input-disabled'];

    /* Database */
    $databaseColumn             = $input['database']['database-column'];
    $databaseValue              = $input['database']['database-value'];


    $CI =& get_instance();
    $dataBulk = $CI->model_crud->select(
                                      $inputTable,
                                      $inputColumns,
                                      $inputWhere,
                                      $inputOrderby,
                                      NULL
                                  );

    $arrAux = array(''=>$inputOptionInstruction);
    foreach ($dataBulk as $key => $value) {
      $arrAux[$value->$inputIndex]  = $value->$inputValue;
    }

   
    /*-- Helper
    =============================================*/
    if(!is_null($inputHelper)):
      $inputHelper  = '<span class="help-block">'.$inputHelper.' </span>';
    endif;

   
    /*-- Disabled
    =============================================*/


    /*-- Style
    =============================================*/
    switch ($inputStyle) {
      case NULL:
        $style = NULL;
        break;

      case !empty($inputStyle):
        $style = 'style="'.$inputStyle.'"';
        break;
    }


    /*--  [ CREATE ]  Printing on the user's screen
    =============================================*/
    if ($crud=='create'):
      echo '
         <div class="'.$bootstrapGrid.'">
              <div class="form-group">
                <label class="control-label">'.$inputLabel.'</label>

                '. form_dropdown($databaseColumn, $arrAux, '', " id='".$inputId."' class='select2_category form-control ".$inputClass." ' ".$style." ") .'


                '.$inputHelper.'
              </div>
            </div>
      ';
    endif;


    /*--  [ UPDATE ]  Printing on the user's screen
    =============================================*/
    if ($crud=='update'):
      if($inputUpdateDisplay==TRUE):
      echo '
         <div class="'.$bootstrapGrid.'">
              <div class="form-group">
                <label class="control-label">'.$inputLabel.'</label>

                '. form_dropdown($databaseColumn, $arrAux, $databaseValue, " id='".$inputId."' class='select2_category form-control ".$inputClass." ' ".$style." ") .'

                '.$inputHelper.'
              </div>
            </div>
      ';
      endif;
    endif;

    return true;
	}





  /***********************************************  
    SELECT
  ***********************************************/
  public function selectDependence($crud, array $input){


    /*--  [ COMMON ]
    =============================================*/
    $inputType                  = $input['common']['input-type'];
    $bootstrapGrid              = $input['common']['bootstrap-grid'];
    $inputLabel                 = $input['common']['input-label'];
    $inputId                    = $input['common']['input-id'];
    $inputClass                 = $input['common']['input-class'];
    $inputStyle                 = $input['common']['input-style'];
    $inputHelper                = $input['common']['input-helper'];
    $inputDisplay               = $input['common']['input-display'];

    /*--  [ SPECIF ]
    =============================================*/

    /* Flags */
    $selectFirstSelect          = $input['specif']['select-firstSelect'];
    $selectHaveDependencies     = $input['specif']['select-haveDependencies'];

    /* First Select */
    $inputOptionInstruction     = $input['specif']['select-optionInstruction'];
    $inputTable                 = $input['specif']['select-table'];
    $inputColumns               = $input['specif']['select-columns'];
    $inputWhere                 = $input['specif']['select-where'];
    $inputOrderby               = $input['specif']['select-orderby'];
    $inputIndex                 = $input['specif']['select-index'];
    $inputValue                 = $input['specif']['select-value'];

    /* Dependence */
    $selectDependenceIdentification     = $input['specif']['selectDependence-identification'];
    $selectDependenceOptionInstruction  = $input['specif']['selectDependence-optionInstruction'];
    $selectDependenceTable              = $input['specif']['selectDependence-table'];
    $selectDependenceColumns            = $input['specif']['selectDependence-columns'];
    $selectDependenceWhere              = $input['specif']['selectDependence-where'];
    $selectDependenceOrderby            = $input['specif']['selectDependence-orderby'];
    $selectDependenceIndex              = $input['specif']['selectDependence-index'];
    $selectDependenceValue              = $input['specif']['selectDependence-value'];
    $selectDependencies                 = $input['specif']['select-dependencies'];


    /* Have Dependencies = TRUE */
    if($selectHaveDependencies==TRUE):
      $merge  =  array_merge(
                  array($selectDependenceIdentification), // id
                  array($selectDependenceOptionInstruction), // id
                  array($selectDependenceTable), // selecione uma cidade
                  array($selectDependenceColumns),
                  array($selectDependenceWhere),
                  array($selectDependenceOrderby),
                  array($selectDependenceIndex),
                  array($selectDependenceValue)
                );
      $json   =  base64_encode( json_encode( $merge ) );
    endif;


    /* Have Dependencies = FALSE */
    if($selectHaveDependencies==FALSE):
      $json   =   'empty';
    endif;


    /* Update */
    $inputDisplay               = $input['update']['input-display'];
    $inputDisabled              = $input['update']['input-disabled'];


    /* Database */
    $databaseColumn             = $input['database']['database-column'];
    $databaseValue              = $input['database']['database-value'];


    /* First Select = TRUE */
    if($selectFirstSelect==TRUE):
      $CI =& get_instance();
      $dataBulk = $CI->model_crud->select(
                                        $inputTable,
                                        $inputColumns,
                                        $inputWhere,
                                        $inputOrderby,
                                        NULL
                                    );

      $arrAux = array(''=>$inputOptionInstruction);
      foreach ($dataBulk as $key => $value) {
        $arrAux[$value->$inputIndex]  = $value->$inputValue;
      }
    endif;

     
    /* First Select = FALSE */
    if($selectFirstSelect==FALSE):
      $arrAux = array(''=>'Selecione uma Opção');
    endif;



    /*-- Helper
    =============================================*/
    if(!is_null($inputHelper)):
      $inputHelper  = '<span class="help-block">'.$inputHelper.' </span>';
    endif;


    /*-- Style
    =============================================*/
    switch ($inputStyle) {
      case NULL:
        $style = NULL;
        break;

      case !empty($inputStyle):
        $style = 'style="'.$inputStyle.'"';
        break;
    }


    /*--  [ CREATE ]  Printing on the user's screen
    =============================================*/
    if ($crud=='create'):
      echo '
         <div class="'.$bootstrapGrid.'">
              <div class="form-group">
                <label class="control-label">'.$inputLabel.'</label>

                '. form_dropdown($databaseColumn, $arrAux, '', " id='".$inputId."' data-instruction='".$json."'  data-dependencies='".json_encode($selectDependencies)."' class='selectDependence form-control ".$inputClass." ' ".$style." ") .'


                '.$inputHelper.'
              </div>
            </div>
      ';
    endif;


    /*--  [ UPDATE ]  Printing on the user's screen
    =============================================*/
    if ($crud=='update'):

      // Nao permitir edição quando houver dependencia

    endif;

    return true;
  }



    /***********************************************	
      RADIOBUTTON
    ***********************************************/
	public function radiobutton(array $data){
    return NULL;
	}


    /***********************************************	
      CHECKBOX
    ***********************************************/
	public function checkbox(array $data){
    return NULL;
	}



  /***********************************************  
  TEXTAREA
  ***********************************************/
  public function textarea($crud, $input){

    /* Common */
    $inputType            = $input['common']['input-type'];
    $bootstrapGrid        = $input['common']['bootstrap-grid'];
    $inputLabel           = $input['common']['input-label'];
    $inputValue           = $input['common']['input-value'];
    $inputId              = $input['common']['input-id'];
    $inputClass           = $input['common']['input-class'];
    $inputStyle           = $input['common']['input-style'];
    $inputHelper          = $input['common']['input-helper'];
    $inputDisabled        = $input['common']['input-disabled'];
    $inputDisplay         = $input['common']['input-display'];
    $validationFrontend   = $input['common']['validation-frontend'];
    $validationBackend    = $input['common']['validation-backend'];


    /* Specif */

    /* Update */
    $inputDisplay         = $input['update']['input-display'];
    $inputDisabled        = $input['update']['input-disabled'];

    /* Database */
    $databaseColumn       = $input['database']['database-column'];
    $databaseValue        = $input['database']['database-value'];




    /*-- Validation Javascript
    =============================================*/
    if(is_array($validationFrontend) && !is_null($validationFrontend)){
      
      // Required
      if(!is_null ($validationFrontend[0]) && $validationFrontend[0]==TRUE){
        $requiredTag  =  '<span class="required"> * </span>';
      }

    }


    /*-- Disabled
    =============================================*/
    switch ($inputDisabled) {
      case NULL:
        $inputDisabled = '';
        break;
      
      case TRUE:
        $inputDisabled = 'disabled';
        break;
    }

   
    /*-- Helper
    =============================================*/
    if(!is_null($inputHelper)):
      $inputHelper  = '<span class="help-block">'.$inputHelper.' </span>';
    endif;


    /*-- Style
    =============================================*/
    switch ($inputStyle) {
      case NULL:
        $style = NULL;
        break;

      case !empty($inputStyle):
        $style = 'style="'.$inputStyle.'"';
        break;
    }


    /*--  [ CREATE ]  Printing on the user's screen
    =============================================*/
    if ($crud=='create'):
      echo '
      <div class="'.$bootstrapGrid.'">
        <div class="form-group">
          <label class="control-label" for="'.$inputId.'">'.$inputLabel.'  </label>


             
            <textarea id="'.$inputId.'" name="'.$inputId.'" value="'.$inputValue.'" class="form-control  '.$inputClass.'"  '.$style.'  '.$inputDisabled.'></textarea>



          '.$inputHelper.'

        </div>
      </div>
      ';
      return TRUE;
    endif;


    /*--  [ UPDATE ]  Printing on the user's screen
    =============================================*/
    if ($crud=='update'):
      echo '
      <div class="'.$bootstrapGrid.'">
        <div class="form-group">
          <label class="control-label" for="'.$inputId.'">'.$inputLabel.' </label>


             
            <textarea id="'.$inputId.'" name="'.$inputId.'" class="form-control '.$inputClass.'" '.$style.'  '.$inputDisabled.'> '.$databaseValue.' </textarea>


          '.$inputHelper.'

        </div>
      </div>
      ';
      return TRUE;
    endif;

  }




    /***********************************************	
      PASSWORD
    ***********************************************/
	public function password(array $data){
    return NULL;
	}



}
/* End class*/