<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Classe base do sistema. 
 * 
 * @author Rafael Barreto 
 *
 */
class Generallib extends CI_Controller
{
	
	/**
	 * Array de dados utilizado nas views 
	 * @var Array
	 */

    public $data;

	function __construct()
	{
		parent::__construct();

		$this->load->library('inputconstructor');
		$this->load->library('parser');	
		$this->load->library('my_date');	
		$this->load->helper('url');
		$this->load->model('m_crud');
        $this->data = array();


		/*
		Setando variaveis de footer
		*/	
		$this->data['use_datatable']	=	TRUE;

	}


	/**
	 * Mostra a estrutura do template 
	 * @param string $pathView
	 */
	protected function loadTemplate( $pathView = 'template/content'  )
	{
		$this->data['base_url'] = base_url();

		$this->parser->parse('template/header', $this->data);
		$this->parser->parse('template/sidebar', $this->data);
		$this->parser->parse('template/begin-main', $this->data);
		
			$this->load->view( $pathView, $this->data );
		
		$this->parser->parse('template/end-main', $this->data);
		$this->parser->parse('template/pre-footer', $this->data);
		$this->parser->parse('template/footer', $this->data);
	}
	

}