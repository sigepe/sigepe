<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if (!function_exists('getNav')){
	function getNav($nav_id){
		$CI =& get_instance();
		/*$navSql = " SELECT nav.*, count(dep.nav_title) as dependente from vanguarda_tools.tb_navitem nav left join vanguarda_tools.tb_navitem dep on dep.nav_fk_nav_id = nav.nav_id where nav.nav_id = $nav_id -- where nav.nav_fk_nav_id = 1 group by nav.nav_id order by nav_ordem asc";*/

		$navSql = "SELECT * from vanguarda_tools.vw_navitem where nav_id = $nav_id order by nav_ordem";
		$resultNav = $CI->db->query($navSql)->result();
		return $resultNav[0];
	}
}


if (!function_exists('getSubNavs')){
	function getSubNavs($fk_nav_id){
		$CI =& get_instance();
		/*$subNavSql = " SELECT nav.*, count(dep.nav_title) as dependente from vanguarda_tools.tb_navitem nav left join vanguarda_tools.tb_navitem dep on dep.nav_fk_nav_id = nav.nav_id -- where nav.nav_id = 1 where nav.nav_fk_nav_id = $fk_nav_id group by nav.nav_id order by nav_ordem asc";*/

		$subNavSql = "SELECT * from vanguarda_tools.vw_navitem where nav_fk_nav_id = $fk_nav_id order by nav_ordem";
		$resultSubNav = $CI->db->query($subNavSql)->result();
		return $resultSubNav;
	}
}


if (!function_exists('gerarNav')){
	function gerarNav($nav_id){
		$CI =& get_instance();

		$nav = getNav($nav_id);
		$resultSubNav = getSubNavs($nav_id);
		

		$retorno = "";
		$retorno .= "<li class='nav-item'>";
		$retorno .= "<a href='$nav->nav_link' class='nav-link'>";
		$retorno .= "<i class='icon-diaamond $nav->nav_icon' aria-hidden='true'></i>";
		$retorno .= "<span class='title'>$nav->nav_title</span>";
		$retorno .= ($nav->dependente == 0) ? "" : "<span class='arrow'></span>";
		$retorno .= "</a>";
		$retorno .= ($nav->dependente == 0) ? "" : "<ul class='sub-menu'>";

		if ($nav->dependente > 0){
			foreach ($resultSubNav as $subNav) {
				$retorno .= gerarNav($subNav->nav_id);
			}
		}

		$retorno .= ($nav->dependente == 0) ? "" : "</ul>";

		return $retorno;
	}	
}


if (!function_exists('gerarTimeStamp')){
	function gerarTimeStamp($tabela, $coluna){

		$CI =& get_instance();

		$time = time();
		
		$sql = "select * from ".$tabela." where ".$coluna." = ".$time." ";
		$query = $CI->db->query($sql)->result();
		if(empty($query)){
		  return $time;
		}else{
			gerarTimeStamp($tabela, $coluna);
		}
	}
}


if (!function_exists('gerarSlug')){
	function gerarSlug($text) {
		// replace non letter or digits by -
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);

		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);

		// trim
		$text = trim($text, '-');

		// remove duplicate -
		$text = preg_replace('~-+~', '-', $text);

		// lowercase
		$text = strtolower($text);

		if (empty($text)) {
		    return 'n-a';
		}

		return $text;
	}
}




if ( ! function_exists('LoadTemplateComponent'))
{


    function LoadTemplateComponent($TypeTechnology = NULL, $TypeComponent = NULL, $Component = NULL)
    {	


    	if($TypeTechnology == NULL || $TypeComponent == NULL || $Component == NULL)
    		return NULL;


    	/* 
    		STYLES
    	===================================================
    	*/
    	if($TypeTechnology == 'StylesFile'):

	    	if($TypeComponent == 'PageLevelPlugins'):
		    	switch ($Component):


		    		case 'easy-autocomplete':
		    			return 'assets/global/stretch/EasyAutocomplete-1.3.5/easy-autocomplete.min.css'; 
		    			break;

		    		case 'easy-autocomplete-themes':
		    			return 'assets/global/stretch/EasyAutocomplete-1.3.5/easy-autocomplete.themes.min.css'; 
		    			break;




					/* Bootstrap Sweet Alert
					-----------------------------------*/

		    		case 'datatables':
		    			return 'assets/global/plugins/datatables/datatables.min.css'; 
		    			break;

		    		case 'datatables-bootstrap':
		    			return 'assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css'; 
		    			break;



					/* Bootstrap Sweet Alert
					-----------------------------------*/

		    		case 'bootstrap-sweetalert':
		    			return 'assets/global/plugins/bootstrap-sweetalert/sweetalert.css'; 
		    			break;





					/* Date & Time Pickers
					-----------------------------------*/

		    		case 'daterangepicker':
		    			return 'assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css'; 
		    			break;

		    		case 'bootstrap-datepicker3':
		    			return 'assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css'; 
		    			break;

		    		case 'bootstrap-timepicker':
		    			return 'assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css'; 
		    			break;

		    		case 'bootstrap-datetimepicker':
		    			return 'assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'; 
		    			break;

		    		case 'clockface':
		    			return 'assets/global/plugins/clockface/css/clockface.css'; 
		    			break;



					/* Bootstrap File Input
					-----------------------------------*/

		    		case 'bootstrap-fileinput':
		    			return 'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css'; 
		    			break;



					/* Markdown & WYSIWYG Editors
					-----------------------------------*/

		    		case 'bootstrap-wysihtml5':
		    			return 'assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css'; 
		    			break;

		    		case 'bootstrap-markdown':
		    			return 'assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css'; 
		    			break;

		    		case 'bootstrap-summernote':
		    			return 'assets/global/plugins/bootstrap-summernote/summernote.css'; 
		    			break;



					/* User Profile
					-----------------------------------*/
		    		case 'bootstrap-fileinput':
		    			return 'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css'; 
		    			break;



					/* Multiple Select
					-----------------------------------*/

		    		case 'bootstrap-select':
		    			return 'assets/global/plugins/bootstrap-select/css/bootstrap-select.css'; 
		    			break;

		    		case 'multi-select':
		    			return 'assets/global/plugins/jquery-multi-select/css/multi-select.css'; 
		    			break;

		    		case 'select2':
		    			return 'assets/global/plugins/select2/css/select2.min.css'; 
		    			break;

		    		case 'select2-bootstrap':
		    			return 'assets/global/plugins/select2/css/select2-bootstrap.min.css'; 
		    			break;





		    		default:
		    			return NULL;
		    			break;

		    	endswitch;

	    	endif;

	    	if($TypeComponent == 'PageLevelStyles'):

		    	switch ($Component):

		    		case 'todo-2':
		    			return 'assets/apps/css/todo-2.min.css';			 
		    			break;


					/* User Profile
					-----------------------------------*/
		    		case 'profile':
		    			return 'assets/pages/css/profile.min.css'; 
		    			break;


					/* Invoice
					-----------------------------------*/
		    		case 'invoice':
		    			return 'assets/pages/css/invoice.min.css'; 
		    			break;

        
		    		default:
		    			return NULL;
		    			break;

		    	endswitch;

			endif;


		endif;



    	/* 
    		SCRIPTS
    	===================================================
    	*/
    	if($TypeTechnology == 'ScriptsFile'):

	    	if($TypeComponent == 'PageLevelPlugins'):

		    	switch ($Component):
		    		case 'select2':
		    			return 'assets/global/plugins/select2/js/select2.full.min.js'; 
		    			break;

		    		case 'input-mask':
		    			return 'assets/global/stretch/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js'; 
		    			break;

		    		case 'input-mask-trigger':
		    			return 'assets/global/stretch/jQuery-Mask-Plugin-master/dist/trigger.js'; 
		    			break;

		    		case 'easy-autocomplete':
		    			return 'assets/global/stretch/EasyAutocomplete-1.3.5/jquery.easy-autocomplete.min.js'; 
		    			break;

		    		case 'bootstrap-wizard':
		    			return 'assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js'; 
		    			break;




					/* Bootstrap Form Validation
					-----------------------------------*/
		    		case 'datatable':
		    			return 'assets/global/scripts/datatable.js'; 
		    			break;

		    		case 'datatables':
		    			return 'assets/global/plugins/datatables/datatables.min.js'; 
		    			break;

		    		case 'datatables-bootstrap':
		    			return 'assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'; 
		    			break;



					/* Bootstrap Form Validation
					-----------------------------------*/
		    		case 'jquery-validation':
		    			return 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js'; 
		    			break;

		    		case 'jquery-validation-additional-methods':
		    			return 'assets/global/plugins/jquery-validation/js/additional-methods.js'; 
		    			break;



					/* Bootstrap Sweet Alert
					-----------------------------------*/

		    		case 'bootstrap-sweetalert':
		    			return 'assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js'; 
		    			break;



					/* Date & Time Pickers
					-----------------------------------*/

		    		case 'moment':
		    			return 'assets/global/plugins/moment.min.js'; 
		    			break;

		    		case 'daterangepicker':
		    			return 'assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js'; 
		    			break;

		    		case 'bootstrap-datepicker':
		    			return 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js'; 
		    			break;

		    		case 'bootstrap-timepicker':
		    			return 'assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js'; 
		    			break;

		    		case 'bootstrap-datetimepicker':
		    			return 'assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'; 
		    			break;

		    		case 'clockface':
		    			return 'assets/global/plugins/clockface/js/clockface.js'; 
		    			break;



					/* Bootstrap File Input
					-----------------------------------*/

		    		case 'bootstrap-fileinput':
		    			return 'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js'; 
		    			break;



					/* Markdown & WYSIWYG Editors
					-----------------------------------*/

		    		case 'wysihtml5':
		    			return 'assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js'; 
		    			break;


		    		case 'bootstrap-wysihtml5':
		    			return 'assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js'; 
		    			break;

		    		case 'markdown':
		    			return 'assets/global/plugins/bootstrap-markdown/lib/markdown.js'; 
		    			break;

		    		case 'bootstrap-markdown':
		    			return 'assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js'; 
		    			break;

		    		case 'summernote':
		    			return 'assets/global/plugins/bootstrap-summernote/summernote.min.js'; 
		    			break;



					/* User Profile
					-----------------------------------*/

		    		case 'bootstrap-fileinput':
		    			return 'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js'; 
		    			break;

		    		case 'jquery-sparkline':
		    			return 'assets/global/plugins/jquery.sparkline.min.js'; 
		    			break;

		    		case 'gmaps':
		    			return 'assets/global/plugins/gmaps/gmaps.min.js'; 
		    			break;



					/* Multiple Select
					-----------------------------------*/

		    		case 'bootstrap-select':
		    			return 'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js'; 
		    			break;

		    		case 'jquery-multi-select':
		    			return 'assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js'; 
		    			break;

		    		case 'select2-full':
		    			return 'assets/global/plugins/select2/js/select2.full.min.js'; 
		    			break;



		    		default:
		    			return NULL;
		    			break;

		    	endswitch;

	    	endif;

	    	if($TypeComponent == 'PageLevelScripts'):

		    	switch ($Component):
		    		case 'select2':
		    			return 'assets/global/plugins/select2/js/select2.full.min.js'; 
		    			break;



					/* Bootstrap Sweet Alert
					-----------------------------------*/
		    		case 'table-datatables-fixedheader':
		    			return 'assets/pages/scripts/table-datatables-fixedheader.min.js'; 
		    			break;



					/* Datatable Buttons
					-----------------------------------*/
		    		case 'table-datatables-buttons':
		    			return 'assets/pages/scripts/table-datatables-buttons.min.js'; 
		    			break;




					/* Bootstrap Sweet Alert
					-----------------------------------*/
		    		case 'ui-sweetalert':
		    			return 'assets/pages/scripts/ui-sweetalert.min.js'; 
		    			break;



					/* Date & Time Pickers
					-----------------------------------*/

		    		case 'components-date-time-pickers':
		    			return 'assets/pages/scripts/components-date-time-pickers.min.js'; 
		    			break;



					/* Markdown & WYSIWYG Editors
					-----------------------------------*/

		    		case 'components-editors':
		    			return 'assets/pages/scripts/components-editors.min.js'; 
		    			break;

		    		

					/* User Profile
					-----------------------------------*/

		    		case 'profile':
		    			return 'assets/pages/scripts/profile.min.js'; 
		    			break;

		    		case 'timeline':
		    			return 'assets/pages/scripts/timeline.min.js'; 
		    			break;



					/* Multiple Select
					-----------------------------------*/

		    		case 'components-multi-select':
		    			return 'assets/pages/scripts/components-multi-select.min.js'; 
		    			break;


					/* SweetAlert2
					-----------------------------------*/
		    		case 'sweetalert2':
		    			return 'assets/global/stretch/sweetalert2-master/dist/sweetalert2.all.js'; 
		    			break;




		    		default:
		    			return NULL;
		    			break;

		    	endswitch;

			endif;

		endif;

    }   



	function GetStringBetween($string, $start, $end){
	    $string = ' ' . $string;
	    $ini = strpos($string, $start);
	    if ($ini == 0) return '';
	    $ini += strlen($start);
	    $len = strpos($string, $end, $ini) - $ini;
	    return substr($string, $ini, $len);
	}




	function GetBootstrapColor($StatusId){

		$BootstrapColor 	=	'';


		// Default ( cinza )
		if(
			$StatusId ==  3 ||
			$StatusId ==  7 ||
			$StatusId ==  12 ||
			$StatusId ==  16 ||
			$StatusId ==  20 ||
			$StatusId ==  24 
		)
			$BootstrapColor 	=	'default';


		// Success ( verde )
		if(
			$StatusId ==  1 ||
			$StatusId ==  4 ||
			$StatusId ==  8 ||
			$StatusId ==  13 ||
			$StatusId ==  17 ||
			$StatusId ==  21 ||
			$StatusId ==  25 
		)
			$BootstrapColor 	=	'success';


		// Warning ( laranja )
		if(
			$StatusId ==  2 ||
			$StatusId ==  5 ||
			$StatusId ==  9 ||
			$StatusId ==  14 ||
			$StatusId ==  18 ||
			$StatusId ==  22 ||
			$StatusId ==  26 
		)
			$BootstrapColor 	=	'danger';


		// Dangerous ( vermelho )
		if(
			$StatusId ==  6 ||
			$StatusId ==  11 ||
			$StatusId ==  15 ||
			$StatusId ==  19 ||
			$StatusId ==  23 ||
			$StatusId ==  27 ||
			$StatusId ==  49 
		)
			$BootstrapColor 	=	'warning';



		return $BootstrapColor;

	}



	function GetStatusVinculo($StatusId){

		$NameStatus 	=	'';


		// Success ( verde )
		if(
			$StatusId ==  1  
		)
			$NameStatus 	=	'Vínculo Ativo';



		// Dangerous ( vermelho )
		if(
			$StatusId ==  2
		)
			$NameStatus 	=	'Vínculo Inativo/Cancelado';



		// Default ( cinza )
		if(
			$StatusId ==  3 ||
			$StatusId ==  7 ||
			$StatusId ==  12 ||
			$StatusId ==  16 ||
			$StatusId ==  20 ||
			$StatusId ==  24 
		)
			$NameStatus 	=	'Aguardando Validação';


		// Success ( verde )
		if(
			$StatusId ==  4 ||
			$StatusId ==  8 ||
			$StatusId ==  13 ||
			$StatusId ==  17 ||
			$StatusId ==  21 ||
			$StatusId ==  25 
		)
			$NameStatus 	=	'Vínculo Validado';


		// Warning ( laranja )
		if(
			$StatusId ==  5 ||
			$StatusId ==  9 ||
			$StatusId ==  14 ||
			$StatusId ==  18 ||
			$StatusId ==  22 ||
			$StatusId ==  26 ||
			$StatusId ==  49 
		)
			$NameStatus 	=	'Vínculo Cancelado';


		// Dangerous ( vermelho )
		if(
			$StatusId ==  6 ||
			$StatusId ==  11 ||
			$StatusId ==  15 ||
			$StatusId ==  19 ||
			$StatusId ==  23 ||
			$StatusId ==  27 
		)
			$NameStatus 	=	'Vínculo Rejeitado';



		return $NameStatus;

	}


	function GetIconVinculo($StatusId){

		$Icon 	=	'';

		// Default ( cinza )
		if(
			$StatusId ==  3 ||
			$StatusId ==  7 ||
			$StatusId ==  12 ||
			$StatusId ==  16 ||
			$StatusId ==  20 ||
			$StatusId ==  24 
		)
			$Icon 	=	'fa-clock-o';


		// Success ( verde )
		if(
			$StatusId ==  4 ||
			$StatusId ==  8 ||
			$StatusId ==  13 ||
			$StatusId ==  17 ||
			$StatusId ==  21 ||
			$StatusId ==  25 
		)
			$Icon 	=	'fa-check';


		// Warning ( laranja )
		if(
			$StatusId ==  5 ||
			$StatusId ==  9 ||
			$StatusId ==  14 ||
			$StatusId ==  18 ||
			$StatusId ==  22 ||
			$StatusId ==  26 ||
			$StatusId ==  49 
		)
			$Icon 	=	'fa-ban';


		// Dangerous ( vermelho )
		if(
			$StatusId ==  6 ||
			$StatusId ==  11 ||
			$StatusId ==  15 ||
			$StatusId ==  19 ||
			$StatusId ==  23 ||
			$StatusId ==  27 
		)
			$Icon 	=	'fa-close';



		return $Icon;

	}



	function GetStatusTitle($StatusId){

		$NameStatus 	=	'';

		switch ($StatusId) {
			case '1':
				$NameStatus 	=	'Ativo';
				break;

			case '2':
				$NameStatus 	=	'Inativo';
				break;
		}

		return $NameStatus;
	}









}